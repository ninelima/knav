TARGET = knav-win32-qt.exe
INSTALLER = Setup.exe Update.exe

all:
    @echo #
    @echo # Build the Qt version
    @echo #
    cd qt
    nmake -nologo -f makefile.msvc
    cd ..
    copy qt\qtnav.exe .\knav-win32-qt.exe


ins: $(INSTALLER)

$(INSTALLER): $(TARGET)
    makensis setup.nsi
    makensis update.nsi


#all: $(TARGET)

$(TARGET):
    @echo #
    @echo # Build the Qt version
    @echo #
    cd qt
    nmake -nologo -f makefile.msvc
    cd ..
    copy qt\qtnav.exe .\knav-win32-qt.exe

#    @echo #
#    @echo # Build the Java version
#    @echo #
#    cd java
#    nmake -f makefile.msvc
#    cd ..
#    copy java\KNav.jar


clean:
    @echo #
    @echo # Clean the Qt version
    @echo #
    cd qt
    nmake -nologo -f makefile.msvc clean
    cd ..

#    @echo #
#    @echo # Clean the Java version
#    @echo #
#    cd java
#    nmake -f makefile.msvc clean
#    cd ..

help:
	@echo #
	@echo # Usage of this makefile
	@echo #
    rem  Usage: nmake -f makefile.msvc [switch]
    rem
    rem  where switch is one of the following:
    rem
    rem   all (default)
    rem   clean
    rem   help
    rem   ins
 

