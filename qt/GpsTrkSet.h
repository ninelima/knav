#ifndef __GPSTRKSET_H
#define __GPSTRKSET_H

#include "DelimRecordSet.h"
#include <string>

// GpsTrkSet.h : header file
//

#define _NO_GPSTRK_CACHE_

#if _MOTIF_
#define _NO_GPSTRK_CACHE_
#endif

		      
//#ifdef _MOTIF_
#ifdef _NO_GPSTRK_CACHE_

#pragma message ("fix cp for no cache")
	       
//-----------------------------------------------------------------------------
// CGpsTrkSet Delimited recordset
//
class CGpsTrkSet : public CDelimRecordSet
{
public:
  CGpsTrkSet();

  void Bind();
                
  unsigned long  m_DTG;
  double  m_rLat;
  double  m_rLong;
	unsigned long   m_Date; 
	unsigned long   m_Time; 

	unsigned long   m_Gs; 
	unsigned long   m_Trk; 
	unsigned long   m_Alt; 

  //long   m_nFields;
};


#else //_NO_GPSTRK_CACHE_
--> todo

//-----------------------------------------------------------------------------

#define WPT_TOTAL 24000


class WptRec
{
public:

  long  m_ID;
  std::string m_ICAO;
  std::string m_INFO;
  std::string m_LAT;
  std::string m_LONG;
  double m_rElev;
  double m_rLat;
  double m_rLong;
  long   m_TYPE;
  long   m_nFields;

  WptRec()
  {
    // Field initialisation
    m_ID    = 0;
    m_ICAO  = "";
    m_INFO  = "";
    //m_LAT   = "";
    //m_LONG  = "";
    m_rElev = 0.0;
    m_rLat  = 0.0;
    m_rLong = 0.0;
    m_TYPE  = 0;

    // Number of fields
    m_nFields = 9;
  }
};


class CGpsTrkSet : public CDelimRecordSet
{

public:
  // We can use vectors, lists or whatever
  // but speed is king. A simple static array
  // is the best.
  WptRec tbl[WPT_TOTAL];

  int cp;
  int num;


public:
  long   m_ID;
  std::string m_ICAO;
  std::string m_INFO;
  std::string m_LAT;
  std::string m_LONG;
  double m_rElev;
  double m_rLat;
  double m_rLong;
  long   m_TYPE;

  CGpsTrkSet();
  //~CGpsTrkSet();
  void GpsTrkSetInit();
  bool Open(const char *sFileName);
  bool MoveFirst();
  bool MoveNext();
  bool Move(int idx);
  bool IsEOF();
  void Clear();
  void Bind();
  void Store();
  WptRec GetRecord();
};


#endif //_MOTIF_


#endif // __GPSTRKSET_H
