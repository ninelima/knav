//-----------------------------------------------------------------------------
//
// Copyright (c) Player One Inc 2001
//
// Module name: WPTSet
// Module type: Implementation source file
// Compiler   : MSVC++ 6.0
// Environment: Microsoft Windows NT
//
// Description:
//  This class implements the a CSV file handler derived from CDelimRecordSet
//  It's primary function is to define and bind member variables mapped to the
//  TXT database
//
// Note:
//  The pure virtual Bind(..) is implemented in this class
//
// Contents:                 
//
// Revision history:
//  24 Aug 2001 B2  Created
//  24 Aug 2001 B2  Add comment block
//
//-----------------------------------------------------------------------------

#include <qstring.h>               
#include "TxtSet.h"


#ifdef _NO_TXT_CACHE_
#pragma message ("#\n# CTxtSet - caching disabled\n#")

//-----------------------------------------------------------------------------
// CTxtSet
//
CTxtSet::CTxtSet()
  : CDelimRecordSet()
{
  m_NAME = "";
  m_COUNTRY = "";
  m_rLat = 0.0;
  m_rLon = 0.0;
  m_TYPE = 0;
  m_nFields = 5;
}

CTxtSet::~CTxtSet()
{
}

void CTxtSet::Bind()                     
{
  CDelimRecordSet::Bind();

  m_NAME    = m_sField[0];
  m_COUNTRY = m_sField[1];
  m_rLat  = atof(m_sField[2]);
  m_rLon  = atof(m_sField[3]);
  m_TYPE  = atoi(m_sField[4]);
}


#else //-----------------------------------------------------------------------------
#pragma message ("#\n# CTxtSet - caching enabled\n#")
  

#include <qlist.h>
#include <qstring.h>


CTxtSet::CTxtSet()
{
  cp = 0;
  num = 0;
  TxtSetInit();
}

CTxtSet::~CTxtSet()
{
}


void CTxtSet::TxtSetInit()
{
}

//-----------------------------------------------------------------------------
//  Open and read the entire .csv file, storing it in vector class tbl
//
bool CTxtSet::Open(const char *sFileName)
{
  CDelimRecordSet rs;// = new CDelimRecordSet();

  if (!rs.Open(sFileName))
    return false;

  rs.MoveFirst();
  //rs.MoveNext(); // only if we use the first line for field names
  while (rs.IsEOF() != true) {
    if (rs.m_nFields > 4) {
        TxtRec rec;

        rec.m_NAME    = QString(rs.m_sField[0]);
        rec.m_rLat    = QString(rs.m_sField[2]).toDouble();
        rec.m_rLon    = QString(rs.m_sField[3]).toDouble();
        rec.m_TYPE    = QString(rs.m_sField[4]).toLong();
				rec.m_nFields = 4;

        tbl[num] = rec;
        num++;
    }
    if (num > TXT_TOTAL-10)
      break; // throw 1;
    
    rs.MoveNext();
  }
  rs.Close();
  return true;
}

//-----------------------------------------------------------------------------
//  Move to the first entry in the set
//
bool CTxtSet::MoveFirst()
{
  cp = 0;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
// Move to the next entry in the set
//
bool CTxtSet::MoveNext()
{
  cp++;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
// Move to a specified in the set
//
bool CTxtSet::Move(int idx)
{
  cp = idx;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
//  Check for the end of the set
//  returns true is the end is reached and
//  false if not
//
bool CTxtSet::IsEOF()
{
  int size = num;// tbl.size();
  //if (cp >= size - 1) if we use the first line as headings
  if (cp >= size)
    return true;
  else
    return false;
}

//-----------------------------------------------------------------------------
//  Clear out the contents of the set
//
void CTxtSet::Clear()
{
  cp = 0;
  num = 0;
}

void CTxtSet::Bind()
{
  m_NAME    = tbl[cp].m_NAME;
  //m_COUNTRY = tbl[cp].m_COUNTRY;
  m_rLat    = tbl[cp].m_rLat;
  m_rLon    = tbl[cp].m_rLon;
  m_TYPE    = tbl[cp].m_TYPE;
  
  m_nCurRec = cp;
}

void CTxtSet::Store() 
{
  tbl[cp].m_NAME     = m_NAME;   
  //tbl[cp].m_COUNTRY  = m_COUNTRY;
  tbl[cp].m_rLat     = m_rLat;   
  tbl[cp].m_rLon     = m_rLon;   
  tbl[cp].m_TYPE     = m_TYPE;   
}

TxtRec CTxtSet::GetRecord()
{
  TxtRec rec;
                                   
  rec.m_NAME    =  m_NAME;    
  //rec.m_COUNTRY =  m_COUNTRY; 
  rec.m_rLat    =  m_rLat;    
  rec.m_rLon    =  m_rLon;    
  rec.m_TYPE    =  m_TYPE;    

  return rec;
}

long CTxtSet::GetCP()
{
  return cp;
}


#endif
