//
// $Id: DiGadget.h,v 1.2 2005/07/08 07:40:14 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//

#include <qwidget.h>
#include <qpixmap.h>

#ifndef __GRAPHICPAINTER_H
  #include "GraphicPainter.h"
#endif // __GRAPHICPAINTER_H

#include "PanelGadget.h"

class DiGadget : public PanelGadget //QWidget //PanelGadget
{
public:
	int m_Hdg;
  int hotspot; /** 80 sets hotspot to the arrow */
  QRect rect;

  DiGadget(QWidget *parent=0, const char *name=0, WFlags f=0);

  //void paintEvent(QPaintEvent* e);

	void drawBlankFace(GraphicPainter *gc, int x, int y, int size);
  void drawFace(GraphicPainter *gc);

  void setHdg(int Hdg);

  
  void mousePressEvent(QMouseEvent *e);
	//void mouseReleaseEvent(QMouseEvent *e);
private:
	/*
  QPixmap pixbuffer;
	int m_size;
	double m_scale;

  bool m_bMouseLeftButtonDown;
  bool m_bMouseRightButtonDown;
	*/
};
