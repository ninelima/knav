#ifdef _MOTIF_

#include <stdio.h>
#include <Xm/PushB.h>
#include <Xm/Form.h>
#include <Xm/Text.h>

#include "ToolbarWidget.h"
#include "toolbar.h"
#include "MapWnd.h"


extern CMapWnd MapWnd;




/*-----------------------------------------------------------------------------*/
void pushB1Callback(Widget widget, XtPointer client_data, XtPointer call_data);

void SetPixmap(Widget, char *);

void SetPixmap(Widget w, char *filename)
{
  Pixmap bitmap;
  Pixel bg, fg;
 
  XtVaGetValues(w,
                  XmNbackground, &bg,
                  XmNforeground, &fg,
                  NULL);
 
  bitmap = XmGetPixmap(XtScreen(w), filename, fg, bg);
 
  if(bitmap)
  XtVaSetValues(w,
          XmNlabelType, XmPIXMAP,
          XmNlabelPixmap, bitmap,
          NULL);
}


Widget create_toolbar(Widget parent)
{
  Widget /*topLevel,*/ newid;
  Widget form;
  Widget pb;
 
  //form = XtVaCreateWidget("Form", xmFormWidgetClass, parent, NULL);
  newid = XtVaCreateWidget("Toolbar", xmToolbarWidgetClass, /*form*/parent,
                  //XmNtipPosition, XmTIP_BOTTOM_RIGHT,
                  XtVaTypedArg, XmNbackground, XmRString, "#667788", 8,
                  XtVaTypedArg, XmNtipForeground,XmRString, "black", 6,
                  XtVaTypedArg, XmNtipBackground,XmRString, "yellow", 7,
                  NULL);
 
  pb = XtVaCreateManagedWidget("PushB1", xmPushButtonWidgetClass, newid,
                  XtVaTypedArg, XmNtipLabel,XmRString, "X Logo", 7,
                  NULL);
  SetPixmap(pb, "xlogo32");
  XtAddCallback(pb, XmNactivateCallback, pushB1Callback, NULL);
 

  XtVaCreateManagedWidget("text", xmTextWidgetClass, newid,
                  XmNconfigurable, False,
                  XtVaTypedArg, XmNtipLabel,XmRString,"Text", 5,
                  XmNcolumns, 10,
                  NULL);
 
  pb = XtVaCreateManagedWidget("PushB2", xmPushButtonWidgetClass, newid,
                  XtVaTypedArg, XmNtipLabel,XmRString, "Left", 5,
                  NULL);
   SetPixmap(pb, "Left");
 
  pb = XtVaCreateManagedWidget("PushB3", xmPushButtonWidgetClass, newid,
                  XtVaTypedArg, XmNtipLabel,XmRString,"Right", 6,
                  NULL);
   SetPixmap(pb, "Right");
 
  pb = XtVaCreateManagedWidget("PushB4", xmPushButtonWidgetClass, newid,
               XtVaTypedArg, XmNtipLabel,XmRString,"Down", 5,
               NULL);
   SetPixmap(pb, "Down");
 
  pb = XtVaCreateManagedWidget("PushB4", xmPushButtonWidgetClass, newid,
               XtVaTypedArg, XmNtipLabel,XmRString,"Up", 3, NULL);
   SetPixmap(pb, "Up");
 
  pb = XtVaCreateManagedWidget("PushB5", xmPushButtonWidgetClass, newid,
               XtVaTypedArg, XmNtipLabel,XmRString,"Rotate Left", 11,
               XmNnewGroup, True, NULL);
   SetPixmap(pb, "RotateLeft");
 
  pb = XtVaCreateManagedWidget("PushB6", xmPushButtonWidgetClass, newid,
               XtVaTypedArg, XmNtipLabel,XmRString,"Rotate Right", 12, NULL);
   SetPixmap(pb, "RotateRight");
 
  pb = XtVaCreateManagedWidget("PushB5", xmPushButtonWidgetClass, newid,
               XtVaTypedArg, XmNtipLabel,XmRString,"Flip Horizontal", 16,
               XmNnewGroup, True, NULL);
   SetPixmap(pb, "FlipHoriz");
 
  pb = XtVaCreateManagedWidget("PushB6", xmPushButtonWidgetClass, newid,
               XtVaTypedArg, XmNtipLabel,XmRString,"Flip Vertical", 14, NULL);
 SetPixmap(pb, "FlipVert");
 
 XtManageChild(newid);

 //return form;
 return newid;
}

void pushB1Callback(Widget widget, XtPointer client_data, XtPointer call_data)
{
  printf("pushB1Callback\n");
  MapWnd.bAddRteActive = !MapWnd.bAddRteActive;

}


#endif //_MOTIF_
