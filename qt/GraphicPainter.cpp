
#include <stdio.h>
#include <stdlib.h>
#include <math.h>


#include "GraphicPainter.h"
#include "umath.h"
#include "utrig.h"


//-----------------------------------------------------------------------------
// Construction/Destruction
//
GraphicPainter::GraphicPainter()
{
}

GraphicPainter::~GraphicPainter()
{
  printf("GraphicPainter::~GraphicPainter\n");
}


//-----------------------------------------------------------------------------
// SetupFont
//
// Setup up a Arial font - we will use as default
//
#ifdef XWIN32
BOOL GraphicPainter::SetupFont(CFont *Font, int height)
{
  return Font->CreateFont(height,      // nheight default = 0
                  0,                   // nwidth default  based on aspect ratio
                  0,                   // nEscapement
                  0,                   // nOrientation
                  FW_NORMAL,           // nWeight,
                  FALSE,               // bItalic,
                  FALSE,               // bUnderline,
                  FALSE,               // cStrikeOut,
                  ANSI_CHARSET,        // nCharSet,
                  OUT_DEFAULT_PRECIS,  // nOutPrecision,
                  CLIP_DEFAULT_PRECIS, // nClipPrecision,
                  DEFAULT_QUALITY,     // nQuality,
                  DEFAULT_PITCH,       // nPitchAndFamily,
                  //"Arial");            // lpszFacename);  see also: EnumFontFamilies
                  "Small");            // lpszFacename);  see also: EnumFontFamilies
}
#else
BOOL GraphicPainter::SetupFont(unsigned long *Font, int height)
{
  return FALSE;
}
#endif

// Stubs
void GraphicPainter::setColor(COLORREF crColor)
{
}

void GraphicPainter::setStroke(int width)
{
}

BOOL GraphicPainter::drawString(const char *str, int x, int y)
{
  return TextOut(x, y, str);
}

BOOL GraphicPainter::drawRectangle(int x, int y, int width, int height)
{
#ifdef _MOTIF_
  return XDrawRectangle(m_Display, m_Window, m_GC, x, y, width, height);
#else
  drawRect(x, y, width, height);
  return true;
#endif
}


#ifdef _MOTIF_
POINT GraphicPainter::moveTo(int x, int y)
{
  return MoveTo(x, y);
}

BOOL GraphicPainter::lineTo(int x, int y)
{
  return LineTo(x, y);
}
#endif //_MOTIF_

void GraphicPainter::radLine(int x, int y, int l, int phi)
{
  int a,b;

  a = uround(l*icos(phi));
  b = uround(l*isin(phi));

  moveTo(x, y);
  lineTo(x+a, y-b);
}

void GraphicPainter::radMove(int x, int y, int l, int phi)
{
  int a,b;

  a = uround(l*icos(phi));
  b = uround(l*isin(phi));

  moveTo(x+a, y-b);
}


void GraphicPainter::radLineArrow(int x, int y, int l, int phi)
{
  int a, b, i, j;

  // The basic line
  a = uround(l*icos(phi));
  b = uround(l*isin(phi));

  moveTo(x, y);
  lineTo(x+a, y-b);

  // The triangle
  i = uround(15*icos(phi));
  j = uround(15*isin(phi));
  moveTo(x+a+i, y-b-j);

  i = uround(05*icos(phi-90));
  j = uround(05*isin(phi-90));
  lineTo(x+a+i, y-b-j);

  i = uround(05*icos(phi+90));
  j = uround(05*isin(phi+90));
  lineTo(x+a+i, y-b-j);

  i = uround(15*icos(phi));
  j = uround(15*isin(phi));
  lineTo(x+a+i, y-b-j);
}

/*
  void radLineArrow(int x, int y, int l, int phi) {
    int a, b, i, j;

    l -= 5;

    a = (int) Math.uround(l*Math.cos(phi));
    b = (int) Math.uround(l*Math.sin(phi));

    // The basic line
    moveTo(x, y);
    lineTo(x+a, y-b);
    //todo pDC->SetPixel(x+a, y-b, RGB_BLACK); // plug the 1 pixel gap between the line an triangle

    // The triangle
    i = uround(10*icos(phi));
    j = uround(10*isin(phi));
    moveTo(x+a+i, y-b-j);

    i = uround(03*icos(phi-90));
    j = uround(03*isin(phi-90));
    lineTo(x+a+i, y-b-j);

    i = uround(03*icos(phi+90));
    j = uround(03*isin(phi+90));
    lineTo(x+a+i, y-b-j);

    i = uround(10*icos(phi));
    j = uround(10*isin(phi));
    lineTo(x+a+i, y-b-j);
  }

*/


void GraphicPainter::setLineAttributes(int width, int style, int cap, int join)
{
#ifdef _MOTIF_
  XSetLineAttributes(m_Display, m_GC, width, style, cap, join);
#else
  //  m_Pen.Create(style, width, color?!!
#endif
}

//-----------------------------------------------------------------------------
//#ifndef XWIN32
#if 1

int GraphicPainter::SetROP2(int nDrawMode)
{
#ifdef _MOTIF_
  XSetFunction(m_Display, m_GC, nDrawMode);
#else
  setRasterOp((Qt::RasterOp) nDrawMode); //XorROP
#endif
  return 0;
}


COLORREF GraphicPainter::GetPixel(int x, int y)
{
  return 1;
}

void GraphicPainter::SetPixel(int x, int y, COLORREF color)
{
  drawPoint(x, y);
}


//BOOL GraphicPainter::TextOut(int x, int y, const CString& str)
BOOL GraphicPainter::TextOut(int x, int y, const char *str)
{
#if _MOTIF_
  XDrawString(m_Display, m_Window, m_GC, x, y, str, strlen(str));
#else
  drawText(x, y, str);
#endif
return TRUE;
}

#ifdef _MOTIF_
POINT cp;
POINT GraphicPainter::MoveTo(int x, int y)
{
  cp.x = x; cp.y = y;
  return cp;
}

POINT GraphicPainter::MoveTo(POINT point)
{
  cp = point;
  return point;
}

BOOL GraphicPainter::LineTo(int x, int y)
{
  XDrawLine(m_Display, m_Window, m_GC, cp.x, cp.y, x, y);
  cp.x = x; cp.y = y;
  return TRUE;
}

BOOL GraphicPainter::LineTo(POINT point)
{
  // todo
  return FALSE;
}
#endif //_MOTIF_

BOOL GraphicPainter::Ellipse(int x1, int y1, int x2, int y2)
{
  int width = abs(x2 - x1);
  int height = abs(y2 - y1);

#if _MOTIF_
  int angle1 = 0;//180 * 64;
  /* 180 degrees */
  int angle2 = 360 * 64;//90 * 64;
  /* 90 degrees */

  XDrawArc(m_Display, m_Window, m_GC, x1, y1, width, height, angle1, angle2);
#else
  drawEllipse(x1, y1, width, height);
#endif
  return TRUE;
}

BOOL GraphicPainter::Ellipse(LPCRECT lpRect)
{
  return FALSE;
}

BOOL GraphicPainter::Rectangle(int x1, int y1, int x2, int y2)
{
  return FALSE;
}

BOOL GraphicPainter::Rectangle(LPCRECT lpRect)
{
  return FALSE;
}

UINT GraphicPainter::SetTextAlign(UINT nFlags)
{
  return FALSE;
}

BOOL GraphicPainter::Polygon(LPPOINT lpPoints, int nCount)
{
  return FALSE;
}

//virtual COLORREF SetTextColor(COLORREF crColor)
COLORREF GraphicPainter::SetTextColor(COLORREF crColor)
{
  return 0;
}

int GraphicPainter::SetBkMode(int nBkMode)
{
  return 0;
}


// Not to be
/*
int GraphicPainter::GetDeviceCaps(int nIndex) const
{
  return 0;
}

CPen* GraphicPainter::SelectObject( CPen* pPen )
{
  return NULL;
}

CBrush* GraphicPainter::SelectObject( CBrush* pBrush )
{
  return NULL;
}

CFont* GraphicPainter::SelectObject( CFont* pFont )
{
  return NULL;
}

CBitmap* GraphicPainter::SelectObject( CBitmap* pBitmap )
{
  return NULL;
}

int GraphicPainter::SelectObject( CRgn* pRgn )
{
  return NULL;
}

CGdiObject* GraphicPainter::SelectObject( CGdiObject* pObject )
{
  return NULL;
}
//BOOL CreateCompatibleDC( CDC* pDC )

BOOL GraphicPainter::CreateCompatibleDC(GraphicPainter* pDC )
{
  return NULL;
}
//BOOL BitBlt ( int x, int y, int nWidth, int nHeight, CDC* pSrcDC, int xSrc, int ySrc, DWORD dwRop )

BOOL GraphicPainter::BitBlt ( int x, int y, int nWidth, int nHeight, GraphicPainter*  pSrcDC, int xSrc, int ySrc, DWORD dwRop )
{
  return NULL;
}

CGdiObject* GraphicPainter::SelectStockObject (int nIndex )
{
  return NULL;
}

int GraphicPainter::GetClipBox( LPRECT lpRect ) const
{
  return NULL;
}

BOOL GraphicPainter::PatBlt( int x, int y, int nWidth, int nHeight, DWORD dwRop )
{
  return NULL;
}
*/

#endif //WIN32

