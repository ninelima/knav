//-----------------------------------------------------------------------------
// $Id: MapWnd.cpp,v 1.76 2007/06/23 06:55:09 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786


//
// Extension of GenMapPanel for use as the main  work area of KNav
//


  #include <qimage.h>
  #include <qfile.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "utypes.h"
#include "GenMapWnd.h"
#include "MapWnd.h"
#include "math.h"
#include "ustring.h"
#include "math.h"
#include "ucolor.h"
#include "umath.h"
#include "ufile.h"
#include "uvalues.h"
#include "WindTriangle.h"
#include "GreatCircle.h"
#include "utrig.h"
#include "iniFile.h"
#include "version.h"
#include "umisc.h"
#include "misc.h"

#include "AltGadget.h"
#include "AsiGadget.h"
#include "HudGadget.h"
#include "DiGadget.h"
#include "TsiGadget.h"
#include "VsiGadget.h"
#include "PlayControl.uih"


#include <qcolor.h>
#include <qmessagebox.h>
#include <qfile.h>
#include <qpainter.h>
#include <qlabel.h> //#include <qlcdnumber.h>
#include <qlineedit.h>
#include <qslider.h>
#include <qpushbutton.h>
#include <qsqldatabase.h>
#include <qdatatable.h>
#include <qsqlselectcursor.h>
#include <qdatetime.h>


extern int g_CurAct;
extern USelect g_Select;
unsigned short g_wptMask = W_APT | W_VOR | W_NDB | W_USR | W_IFR | W_SLD;

extern QSqlSelectCursor *g_AllMvm;

extern QSqlSelectCursor *g_Mvm;
extern QSqlSelectCursor *g_Device;
extern QSqlSelectCursor *g_Asset;


//-----------------------------------------------------------------------------
// TableItem Constructor
//
TableItem::TableItem(QTable *t, EditType et, const QString &txt)
      : QTableItem(t, et, txt)
{
}


//-----------------------------------------------------------------------------
// TableItem paint
//
void TableItem::paint(QPainter *p, const QColorGroup &cg, const QRect &cr, bool selected)
{
    QColorGroup g(cg);
    // last row is the sum row - we want to make it better visible by
    // using a red background
    //if ( row() == table()->numRows() - 1 )
    //g.setColor(QColorGroup::Base, red);
    g.setColor(QColorGroup::Text, g.dark());
    QTableItem::paint(p, g, cr, selected);
}


//-----------------------------------------------------------------------------
// CMapWnd Constructor
//
CMapWnd::CMapWnd()
{
  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  g_wptMask = iniFile.GetValueI("Prefs", "wptMask", 0);
  g_wptMask |= W_USR;  // User waypoints are always on
  bGpsTrkActive = iniFile.GetValueI("Prefs", "bGpsTrkActive", 0);
  bAirActive = iniFile.GetValueI("Prefs", "bAirActive", 0);
  bAirPenWide = iniFile.GetValueI("Prefs", "bAirPenWide", 1);
  bNodeRoseActive = iniFile.GetValueI("Prefs", "bNodeRoseActive", 1);

  m_bPrnActive = false;
  m_bTextActive = true;
  bAddRteActive = false;
  bRouteSaved = true;
  gpsTrkAnimate = false;
  gpsTrkFollowMe = false;

  nLineCtr = 0;
  RouteWpt = 0;
  _RouteWpt = 0;
  iInsertAt = 0;
  memset(RouteLst, 0x0, sizeof(RouteLst));
  bDibFileLoaded = false;
  wptReady = false;
  airReady = false;
  gpsWptReady = false;
  bRadarActive = false;
 
  m_bLeftButtonDown = false;
  _bLbuttonDown = false;

  InitTrig();
  loadAircraftDefaults();
  for (int i = 0; i < NR_LEGS; i++) {
    WindLst[i] = 36000;
  }

  // Open and read the text point database
  loadTxt();

  // Open and read the waypoint database
  loadWpt();

  // Open and read the airspace database
  // loadAir(); - use Esri instead

  // Load the last selected Gps tracks
  // loadGpsAllTrack();

  // Load the Gps waypoints
  loadGpsWpt();

  playControlDlg = NULL;

}


//-----------------------------------------------------------------------------
// Destructor
//
CMapWnd::~CMapWnd()
{
  printf("CMapWnd::~CMapWnd\n");

  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  iniFile.SetValueI("Prefs", "wptMask", g_wptMask);
  iniFile.SetValueI("Prefs", "bGpsTrkActive", bGpsTrkActive);
  iniFile.SetValueI("Prefs", "bAirActive", bAirActive);
  iniFile.SetValueI("Prefs", "bNodeRoseActive", bNodeRoseActive);

  iniFile.WriteFile();

  // Close the waypoint database
  if (wptReady)
    wpt.Close();
}


//-----------------------------------------------------------------------------
// MapPanel initialisation
//
void CMapWnd::mapPanelInit()
{
}

//-----------------------------------------------------------------------------
// Play Control Dialog initialisation
//
void CMapWnd::playControlDlgInit()
{
  //if (!playControlDlg)
  //  playControlDlg = new PlayControl(NULL, "playcontrol", false);  // create as modeless
}


//-----------------------------------------------------------------------------
// Load the default aircraft params
//
void CMapWnd::loadAircraftDefaults()
{
  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
 
  std::string sLastAd = iniFile.GetValue ("Prefs", "LastUsedAd", "default.ad");
  CIniFile adIniFile(sLastAd); adIniFile.ReadFile();

  m_TAS = adIniFile.GetValueI("Performance", "TAS", 999);
  m_FuelFlow = adIniFile.GetValueI("Performance", "FuelFlow", 999);
  m_Units = adIniFile.GetValueI("Options", "Units", 1);

  for (int i = 0; i < NR_LEGS; i++) {
    TasLst[i] = m_TAS;
    //WindLst[i] = 36000;
    FuelFlowLst[i] = m_FuelFlow;
  }
}


//-----------------------------------------------------------------------------
// Open the airport/navaid waypoint information
// and set the flag as ready on success
//
void CMapWnd::loadTxt()
{
  // Open and read the streetnames text database
  streetTxt.Open("streetnames.csv");

  // Open and read the places text database
  if (txt.Open("txt.csv"))
    txtReady = true;
  else
    txtReady = false;
}


//-----------------------------------------------------------------------------
// Open the airport/navaid waypoint information
// and set the flag as ready on success
//
void CMapWnd::loadWpt()
{
  // Open and read the waypoint database
  if (wpt.Open("wpt.csv"))
    wptReady = true;
  else
    wptReady = false;
}


//-----------------------------------------------------------------------------
// Open the airspace information
// and set the flag as ready on success
//
void CMapWnd::loadAir()
{
/*
#pragma message (" TODO: fixup air.Open after data is complete")
#pragma message ("       while we debug just do it in draw")
airReady = true;
*/

        /* 
        --- Defunct --- 

  // Open and read the waypoint database
  if (air.Open("air.csv"))
    airReady = true;

        */
}


//-----------------------------------------------------------------------------
// Open the download waypoint information
// and set the flag as ready on success
//
void CMapWnd::loadGpsWpt()
{
  if (gpsWpt.Open("gps-wpt.csv"))
    gpsWptReady = true;
}


//-----------------------------------------------------------------------------
// Draw the selected waypint on the map
//
void CMapWnd::drawGpsWpt(GraphicPainter *gc)
{
  // Local vars
  int x, y;
  int isize, isizex2;
  QString s;
  //int iVar;
  QString sVar;

  if (bShadeActive)
    gc->setPen(Qt::white);
  else
    gc->setPen(Qt::black);

  gc->setPen(QPen(Qt::darkMagenta, WIDE_PEN));

  isize = 4;
  isizex2 = isize*2;

  gpsWpt.MoveFirst();
  while (gpsWpt.IsEOF() == false) {
    if (LtoC(gpsWpt.m_rLon, gpsWpt.m_rLat, x, y)) {
      gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
    }
    gpsWpt.MoveNext();
  }
}


//-----------------------------------------------------------------------------
// Open the selected track information
// and set the flag as ready on success
//
void CMapWnd::loadGpsTrack()
{
        /*
        --- Defunct ---

  gpsTrk.Close();
  gpsTrkReady = false;

  if (gpsTrk.Open("gps-trk-tmp.csv")) {
    g_Select.m_minLat = +999;
    g_Select.m_maxLat = -999;
    g_Select.m_minLon = +999;
    g_Select.m_maxLon = -999;

    gpsTrkReady = true;
    gpsTrk.MoveFirst();
    while (gpsTrk.IsEOF() == false) {
      double Lat = gpsTrk.m_rLat;
      double Lon = gpsTrk.m_rLong;
 
      if (Lat < g_Select.m_minLat) g_Select.m_minLat = Lat;
      if (Lat > g_Select.m_maxLat) g_Select.m_maxLat = Lat;
      if (Lon < g_Select.m_minLon) g_Select.m_minLon = Lon;
      if (Lon > g_Select.m_maxLon) g_Select.m_maxLon = Lon;

      gpsTrk.MoveNext();
    }
    ZoomToRect(g_Select.m_minLat, g_Select.m_maxLat,
               g_Select.m_minLon, g_Select.m_maxLon);
  }
        */
}


void CMapWnd::loadDatabaseTrack()
{  
  g_Select.m_minLat = +999;
  g_Select.m_maxLat = -999;
  g_Select.m_minLon = +999;
  g_Select.m_maxLon = -999;

  while (g_Mvm->next()) {
    double Lat = g_Mvm->value("rlat").toDouble();
    double Lon = g_Mvm->value("rlon").toDouble();

    if (Lat < g_Select.m_minLat) g_Select.m_minLat = Lat;
    if (Lat > g_Select.m_maxLat) g_Select.m_maxLat = Lat;
    if (Lon < g_Select.m_minLon) g_Select.m_minLon = Lon;
    if (Lon > g_Select.m_maxLon) g_Select.m_maxLon = Lon;

  }
  ZoomToRect(g_Select.m_minLat, g_Select.m_maxLat,
             g_Select.m_minLon, g_Select.m_maxLon);
}


/*
void CMapWnd::reLoadGpsTrack()
{
  gpsTrk.Close();
  gpsTrkReady = false;
  if (gpsTrk.Open("gps-trk-tmp.csv"))
    gpsTrkReady = true;
}
*/



//-----------------------------------------------------------------------------
// PaintMain - the main paint method
//
void CMapWnd::PaintMain(GraphicPainter *gc)
{   
  CGenMapWnd::PaintMain(gc);

  //
  // Draw the Bom website radar images
  //
  if (bRadarActive) {
    drawAllRadar(gc);
  }

  if (bShadeActive)
    gc->setPen(QPen(Qt::white, 1));
  else
    gc->setPen(QPen(Qt::black, 1));

  //
  // We need to draw the "all" tracks before our highligted
  // tracks other wise we see nothing, zip , nada
  //
  if (bGpsTrkActive)
    drawDatabaseAllTrk(gc);

  // We rather use the Esri data
  //if (airReady)
  //  if (bAirActive)
  //    drawAirspace(gc); 

  //
  // We can only draw datasets that have sucessfully loaded
  //
  if (wptReady)
    drawAllWpt(gc);

  if (txtReady)
    drawTxt(gc);

  //
  // Draw the gps waypoints, if any
  //
  if (gpsWptReady)
    if (bGpsTrkActive)  // use the same as for tracks
      drawGpsWpt(gc);

  //
  // Draw the selected tracks, if any
  //
  if (/*(gpsTrkReady) &&*/ (!gpsTrkAnimate) && (!gpsTrkFollowMe))
    drawDatabaseTrk(gc);

  drawRoute(gc);

  // todo
  if (bCrossHairActive) {
    drawCrossHair(gc);
  }

  if (bRoseActive) {
    drawRose(gc);
  }

}


//-----------------------------------------------------------------------------
// Draw the Bom website radar images
//
void CMapWnd::drawAllRadar(GraphicPainter *gc)
{
    //nSpan = 2.304;  //128 km = 2.304 deg lat
    //--------//
    // 256 km //
    //--------//
    
    //QString("%1/%2.png").arg(getenv("TEMP")).arg("IDR482")
    //Kalgoorlie / Boulder,1203,-30.78,121.46,1
    drawShadeT(gc, "temp/IDR482.png", -30.78, 121.46, 2.304*2);
    // Learmonth,19,-22.23,114.08,1
    drawShadeT(gc, "temp/IDR292.png", -22.23, 114.08, 2.304*2);
    // Albany,233,-34.95,117.8,1
    drawShadeT(gc, "temp/IDR312.png", -34.95, 117.80, 2.304*2);
    // Alice-Springs,1789,-23.81,133.9,1
    drawShadeT(gc, "temp/IDR252.png", -23.81, 133.90, 2.304*2);
  
    // Carnarvon,13,-24.88,113.66,1
    drawShadeT(gc, "temp/IDR052.png", -24.88, 113.66, 2.304*2);
    // Geraldton,121,-28.8,114.7,1
    drawShadeT(gc, "temp/IDR062.png", -28.80, 114.70, 2.304*2);
    // Wyndham,14,-15.51,128.15,1
    drawShadeT(gc, "temp/IDR072.png", -15.51, 128.15, 2.304*2);
    //Gove,178,-12.28,136.81,1
    drawShadeT(gc, "temp/IDR092.png", -12.28, 136.81, 2.304*2);
    // Perth   -31.93, 115.95  +- 1.1520 approx
    drawShadeT(gc, "temp/IDR122.png", -31.93, 115.95, 2.304*2);
    // Dampier*Karratha  29,-20.71,116.76
    //--drawShadeT(gc, -top, left, -bottom, right, "temp/IDR152.png", -20.71, 116.76, 2.304*2);
    // Port Hedland,33,-20.38,118.63
    drawShadeT(gc, "temp/IDR162.png", -20.38, 118.63, 2.304*2);
    // Broome,57,-17.95,122.23,1
    drawShadeT(gc, "temp/IDR172.png", -17.95, 122.23, 2.304*2);
    // Weipa,63,-12.68,141.91,1
    drawShadeT(gc, "temp/IDR182.png", -12.68, 141.91, 2.304*2);
    // Cairns,10,-16.88,145.75,1
    drawShadeT(gc, "temp/IDR192.png", -16.88, 145.75, 2.304*2);
    // Townsville,18,-19.25,146.76,1
    drawShadeT(gc, "temp/IDR212.png", -19.25, 146.76, 2.304*2);
    // Esperance,470,-33.68,121.81,1
    drawShadeT(gc, "temp/IDR322.png", -33.68, 121.81, 2.304*2);
    // Ceduna,77,-32.13,133.71,1
    drawShadeT(gc, "temp/IDR332.png", -32.13, 133.71, 2.304*2);
    // Mornington Island,13,-16.66,139.16,1
    drawShadeT(gc, "temp/IDR362.png", -16.66, 139.16, 2.304*2);
    // Eucla,-1,-31.71,128.88,1
    drawShadeT(gc, "temp/IDR452.png", -31.71, 128.88, 2.304*2);
    // Adelaide*  20  -34.95  138.53
    drawShadeT(gc, "temp/IDR462.png", -35.30, 138.53, 2.304*2);
    // Darwin,102,-12.41,130.88,1
    drawShadeT(gc, "temp/IDR632.png", -12.41, 130.88, 2.304*2);


    /*
    //--------//
    // 128 km //
    //--------//
    // Carnarvon,13,-24.88,113.66,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR053.png", -24.88, 113.66, 2.304);
    // Geraldton,121,-28.8,114.7,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR063.png", -28.80, 114.70, 2.304);
    // Wyndham,14,-15.51,128.15,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR073.png", -15.51, 128.15, 2.304);
    // Gove,178,-12.28,136.81,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR093.png", -12.28, 136.81, 2.304);
    // Perth   -31.93, 115.95  +- 1.1520 approx
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR123.png", -31.93, 115.95, 2.304);
    // Dampier*Karratha  29,-20.71,116.76
    //--drawShadeT(gc, -top, left, -bottom, right, "temp/IDR153.png", -20.71, 116.76, 2.304);
    // Port Hedland,33,-20.38,118.63
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR163.png", -20.38, 118.63, 2.304);
    // Broome,57,-17.95,122.23,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR173.png", -17.95, 122.23, 2.304);
    // Weipa,63,-12.68,141.91,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR183.png", -12.68, 141.91, 2.304);
    // Cairns,10,-16.88,145.75,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR193.png", -16.88, 145.75, 2.304);
    // Townsville,18,-19.25,146.76,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR213.png", -19.25, 146.76, 2.304);
    // Learmonth,19,-22.23,114.08,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR293.png", -22.23, 114.08, 2.304);
    // Albany,233,-34.95,117.8,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR313.png", -34.95, 117.80, 2.304);
    // Esperance,470,-33.68,121.81,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR323.png", -33.68, 121.81, 2.304);
    // Mornington Island,13,-16.66,139.16,1
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR363.png", -16.66, 139.16, 2.304);
    // Adelaide*  20  -34.95  138.53
    drawShadeT(gc, -top, left, -bottom, right, "temp/IDR463.png", -35.30, 138.53, 2.304);
    */
}


//-----------------------------------------------------------------------------
// Draw the selected track on the map
//
void CMapWnd::drawGpsTrk(GraphicPainter *gc)
{
  int x, y;
  int _x, _y;
  int isize, isizex2;
  QString s;
  QString sVar;

  if (bShadeActive)
    gc->setPen(Qt::white);
  else
    gc->setPen(Qt::black);

  gc->setPen(QPen(Qt::darkMagenta, THIN_PEN));

  isize = 2; //1;
  isizex2 = isize*2;
  _x = 0;
  _y = 0;

  gpsTrk.MoveFirst();
  gpsTrk.MoveNext();
  LtoC(gpsTrk.m_rLong, gpsTrk.m_rLat, x, y);
  gc->moveTo(x, y);
  while (gpsTrk.IsEOF() == false) {
    LtoC(gpsTrk.m_rLong, gpsTrk.m_rLat, x, y);
    gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
    gc->lineTo(x, y);
    gpsTrk.MoveNext();
  }
}
 

//-----------------------------------------------------------------------------
// Animate the selected track on the map
//
void CMapWnd::animateGpsTrk(GraphicPainter *gc)
{
  /*

  if (playControlDlg == NULL) {
    playControlDlg = new PlayControl(NULL, "playcontrol", false);  // create as modeless
    playControlDlg->show();
  } 

  // sanity check

  // simple hack to make an animation of sorts
  static int animCp = 0;
  int x, y;
  int _x, _y;
  int isize, isizex2;
  QString s;
  QString sVar;
  uint32_t dtg = 0;

  if (bShadeActive)
    gc->setPen(Qt::white);
  else
    gc->setPen(Qt::black);

  gc->setPen(QPen(Qt::darkMagenta, THIN_PEN));

  isize = 2; //1;
  isizex2 = isize*2;
  _x = 0;
  _y = 0;

  // Use a static guard to stop multiple animations
  // we use this to paint the "catch up" bit if there 
  // was a screen event as well
  static bool bGuard = false;
  if (bGuard) {
    // catch up to where we were (ie up to animCp)
    // normally there was just a return here
    gpsTrk.MoveFirst();
    gpsTrk.MoveNext();
    for (int i = 0; i < animCp; i++) {
      LtoC(gpsTrk.m_rLong, gpsTrk.m_rLat, x, y);
      gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
      gpsTrk.MoveNext();
    }
    // The guard's work is done
    return;
  }
  bGuard = true;

  gpsTrk.MoveFirst();
  gpsTrk.MoveNext();

  while (gpsTrk.IsEOF() == false) {
  
    LtoC(gpsTrk.m_rLong, gpsTrk.m_rLat, x, y);

    int ispeed = avg_speed(gpsTrk.m_rLat, gpsTrk.m_rLong, gpsTrk.m_DTG);
    int iheading = avg_hdg(gpsTrk.m_rLat, gpsTrk.m_rLong, gpsTrk.m_DTG);

    int ibank = 0;
    int irot = 0;
    if (iheading != 999)  {
      ibank = avg_bank(iheading, gpsTrk.m_DTG);
      irot = avg_rot(iheading, gpsTrk.m_DTG);
    }


    playControlDlg->AltGadget1->setAlt(gpsTrk.m_Alt);
    playControlDlg->HudGadget1->setAlt(gpsTrk.m_Alt);

    if (ispeed) {
      playControlDlg->AsiGadget1->setGs(ispeed);
      playControlDlg->HudGadget1->setGs(ispeed);
    }

    if (iheading != 999) {
      playControlDlg->diGadget4->setHdg(iheading);
      playControlDlg->HudGadget1->setHdg(iheading);
      playControlDlg->HudGadget1->setBank(ibank);
      playControlDlg->tsiGadget1->setRot(irot);
    }

    if (dtg != 0) {
      int a = playControlDlg->slider1->value();
      // the slider range is 100
      uint32_t d = (gpsTrk.m_DTG - dtg) * playControlDlg->slider1->value() / 50;
      if (d > 10) d = 10;

      clock_t wait = d*100; // 10x
      clock_t goal;
      goal = 1 + wait + clock();
      while (goal > clock()) {
        // flashing animation
        gc->setPen(QPen(Qt::white, THIN_PEN));
        gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
        sleep(100);

        gc->setPen(QPen(Qt::black, THIN_PEN));
        gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
        sleep(100);
      } 
      gc->setPen(QPen(Qt::darkMagenta, THIN_PEN));
      gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);

      // check off screen
      if ((x-40 < gc->window().left()) ||
          (x+40 > gc->window().right()) ||
          (y-40 < gc->window().top()) ||            
          (y+40 > gc->window().bottom())) {

        POINT p;
        p.x = x;
        p.y = y;
        PanCenter(p);
        
        //gc->setPen(Qt::gray);
        gc->setBrush(gc->backgroundColor());

        gc->drawRect(gc->window());
        PaintMain(gc);  // the whole echilada
      }
    }
    dtg = gpsTrk.m_DTG;
    
    //
    // Controller actions
    //
    if (playControlDlg->m_Back == 1) {
      return;
    } 

    if (playControlDlg->m_Play == 1) {
      animCp++;
      gpsTrk.MoveNext();
    }

    if (playControlDlg->m_Fwd == 1) {
      animCp++;
      gpsTrk.MoveNext();
      playControlDlg->m_Fwd = 0;
    }
    
    if (playControlDlg->m_Rewind == 1) {
      animCp=0;
      gpsTrk.MoveFirst();
      animCp++;
      gpsTrk.MoveNext();
      playControlDlg->m_Rewind = 0;
      bGuard = false;
    }

    if (playControlDlg->m_Closed == 1) {
      break;
    }
  }
  
  animCp = 0;
  gpsTrkAnimate = false;
  bGuard = false;

  delete playControlDlg;
  playControlDlg = NULL;
  */
}


// #include "MapWnd_animateDatabaseTrk.hpp"
//-----------------------------------------------------------------------------
// Animate the selected track on the map
//
void CMapWnd::animateDatabaseTrk(GraphicPainter *gc)
{
  static int animCp = 0;
  static int x, y;
  int isize, isizex2;
  QString s;
  QString sVar;
  int dtg = 0;

  if (bShadeActive)
    gc->setPen(Qt::white);
  else
    gc->setPen(Qt::black);

  gc->setPen(QPen(Qt::darkMagenta, THIN_PEN));

  isize = 2; //1;
  isizex2 = isize*2;

  //
  // catch up to where we were (ie up to animCp)
  //
  g_Mvm->first();
  LtoC(g_Mvm->value("rlon").toDouble(), g_Mvm->value("rlat").toDouble(), x, y);
  gc->moveTo(x, y);
  for (int i = 0; i <= animCp; i++) {
    g_Mvm->next();
    LtoC(g_Mvm->value("rlon").toDouble(), g_Mvm->value("rlat").toDouble(), x, y);
    gc->lineTo(x, y);
    gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
  }
  
  //
  // Include the common shared code
  //
  #include "MapWnd_PlayControlHandler.hpp"
                                                   
  //
  // Controller actions
  //
  if (playControlDlg->m_Back == 1) {
    g_Mvm->prev();
    animCp--;
    playControlDlg->m_Back = 0;
  } 

  if (playControlDlg->m_Play == 1) {
    g_Mvm->next();
    animCp++;
  }

  if (playControlDlg->m_Fwd == 1) {
    g_Mvm->next();
    animCp++;
    playControlDlg->m_Fwd = 0;
  }
  
  if (playControlDlg->m_Rewind == 1) {
    g_Mvm->first();
    animCp=0;
    playControlDlg->m_Rewind = 0;
  }

  if (playControlDlg->m_Closed == 1) {
    gpsTrkFollowMe = false;
    gpsTrkAnimate = false;
    if (playControlDlg) playControlDlg->hide(); 
    playControlDlg->m_Closed = 0;
  } 
}



//-----------------------------------------------------------------------------
// Follow me - 
// Returns true if polt fit on screen
// false if a pan was forced
//
bool CMapWnd::flash(GraphicPainter *gc, QColor color, double rLat, double rLon, bool bKeepOnscreen)
{
  int x, y;
  int isize, isizex2;

  isize = 2;
  isizex2 = isize*2;

  LtoC(rLon, rLat, x, y);
  gc->setPen(QPen(color, THIN_PEN));
  gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);

  if (bKeepOnscreen) {
    // check off screen
    if ((x-40 < gc->window().left()) ||
        (x+40 > gc->window().right()) ||
        (y-40 < gc->window().top()) ||
        (y+40 > gc->window().bottom())) {

      POINT p;
      p.x = x;
      p.y = y;
      PanCenter(p);
 
      //gc->setBrush(gc->backgroundColor());
      //gc->drawRect(gc->window());
      //PaintMain(gc);  // the whole echilada
      return false;
    }
  }
  return true;
}

void CMapWnd::flashPoint(GraphicPainter *gc, double rLat, double rLon, bool bKeepOnscreen)
{
  int x, y;
  int _x, _y;
  int isize, isizex2;

  isize = 2; //1;
  isizex2 = isize*2;
  _x = 0;
  _y = 0;

  LtoC(rLon, rLat, x, y);

  clock_t wait = 300; // 500 ms
  clock_t goal;
  goal = 1 + wait + clock();
  while (goal > clock()) {
    // flashing animation
    flash(gc, Qt::white, rLat, rLon);
    sleep(100);

    flash(gc, Qt::black, rLat, rLon);
    sleep(100);
  }
  flash(gc, Qt::darkMagenta, rLat, rLon);

  if (bKeepOnscreen) {
    // check off screen
    if ((x-40 < gc->window().left()) ||
        (x+40 > gc->window().right()) ||
        (y-40 < gc->window().top()) ||
        (y+40 > gc->window().bottom())) {

      POINT p;
      p.x = x;
      p.y = y;
      PanCenter(p);
 
      //gc->setPen(Qt::gray);
      gc->setBrush(gc->backgroundColor());

      gc->drawRect(gc->window());
      PaintMain(gc);  // the whole echilada
    }
  }
}

//-----------------------------------------------------------------------------
// set the local playControlDlg is supplied (non null) otherwise
// create a new one
void CMapWnd::InitPlayControl(PlayControl *dlg)
{
  if (dlg == NULL) {
    if (playControlDlg == NULL) {
      //playControlDlg = new PlayControl(NULL, "playcontrol", false);  // create as modeless
      // create as modeless - always on top
      playControlDlg = new PlayControl(NULL, "playcontrol", false, Qt::WStyle_Customize | 
                                                                   Qt::WStyle_Title |
                                                                   Qt::WStyle_NormalBorder |
                                                                   Qt::WStyle_Tool |
                                                                   //Qt::WStyle_SysMenu |
                                                                   Qt::WStyle_StaysOnTop);  
      playControlDlg->hideButtons();
      playControlDlg->show();
      playControlDlg->m_Closed = 0;
    } 
  }
  else {
    playControlDlg = dlg;
  }
}


void CMapWnd::followGpsTrk(GraphicPainter *gc)
{
  /*
  // sanity check
  if (!playControlDlg)
    InitPlayControl();

  // simple hack to make an animation of sorts
  static int animCp = 0;
  static int x, y;
  int _x, _y;
  int isize, isizex2;
  QString s;
  QString sVar;
  static uint32_t dtg = 0;

  if (bShadeActive)
    gc->setPen(Qt::white);
  else
    gc->setPen(Qt::black);

  gc->setPen(QPen(Qt::darkMagenta, THIN_PEN));

  isize = 2; //1;
  isizex2 = isize*2;
  _x = 0;
  _y = 0;

  gpsTrk.MoveLast();
  gpsTrk.Move(gpsTrk.m_nCurRec - 5);
  for (int i = 0; i < 5; i++) {
    LtoC(gpsTrk.m_rLong, gpsTrk.m_rLat, x, y);
    gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
    gpsTrk.MoveNext();
  }

  int ispeed = avg_speed(gpsTrk.m_rLat, gpsTrk.m_rLong, gpsTrk.m_DTG);
  int iheading = avg_hdg(gpsTrk.m_rLat, gpsTrk.m_rLong, gpsTrk.m_DTG);

  int ibank = 0;
  int irot  = 0;
  if (iheading != 999)  {
    ibank = avg_bank(iheading, gpsTrk.m_DTG);
    irot = avg_rot(iheading, gpsTrk.m_DTG);
  }
  
  playControlDlg->LCDInfo->display(QString().sprintf("20%06d %04d - %05.2f, %06.2f", gpsTrk.m_Date, 
                                                                   gpsTrk.m_Time/100,
                                                                   gpsTrk.m_rLat,
                                                                   gpsTrk.m_rLong));
  playControlDlg->AltGadget1->setAlt(gpsTrk.m_Alt);
  playControlDlg->HudGadget1->setAlt(gpsTrk.m_Alt);

  if (ispeed) {
    playControlDlg->AsiGadget1->setGs(ispeed);
    playControlDlg->HudGadget1->setGs(ispeed);
  }

  if (iheading != 999) {
    playControlDlg->diGadget4->setHdg(iheading);
    playControlDlg->HudGadget1->setHdg(iheading);
    playControlDlg->HudGadget1->setBank(ibank);
    playControlDlg->tsiGadget1->setRot(irot);
  } 
  
  if (playControlDlg->m_Closed == 1) {
    gpsTrkFollowMe = false;
    delete playControlDlg;
    playControlDlg = NULL;
  }
  */
}

void CMapWnd::followDatabaseTrk(GraphicPainter *gc)
{
  static int animCp = 0;
  static int x, y;
  int _x, _y;
  int isize, isizex2;
  QString s;
  QString sVar;
  static uint32_t dtg = 0;

  if (bShadeActive)
    gc->setPen(Qt::white);
  else
    gc->setPen(Qt::black);

  gc->setPen(QPen(Qt::darkMagenta, THIN_PEN));

  isize = 2; //1;
  isizex2 = isize*2;
  _x = 0;
  _y = 0;

  g_Mvm->first();

  //
  // Include the common shared code
  //
  #include "MapWnd_PlayControlHandler.hpp"
 
  //
  // Controller actions
  //
  if (playControlDlg->m_Closed == 1) {
    gpsTrkFollowMe = false;
    playControlDlg->hide();
  }

  //
  // draw the little tail
  //
  LtoC(g_Mvm->value("rlon").toDouble(), g_Mvm->value("rlat").toDouble(), x, y);
  gc->moveTo(x, y);
  do {
    LtoC(g_Mvm->value("rlon").toDouble(), g_Mvm->value("rlat").toDouble(), x, y);
    gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
    gc->lineTo(x, y);
  } while (g_Mvm->next());

  g_Mvm->first();
}


//-----------------------------------------------------------------------------
// Draw the selected track on the map
//
void CMapWnd::drawDatabaseTrk(GraphicPainter *gc)
{
  //static int x, y;
  int x, y;
  int _x, _y;
  int isize, isizex2;
  
  isize = 2; 
  isizex2 = isize*2;
  _x = 0;
  _y = 0;

  gc->setPen(QPen(Qt::darkMagenta, THIN_PEN));

  g_Mvm->first();       
  LtoC(g_Mvm->value("rlon").toDouble(), g_Mvm->value("rlat").toDouble(), x, y);
  gc->moveTo(x, y);
  while (g_Mvm->next()) {
    if (LtoC(g_Mvm->value("rlon").toDouble(), g_Mvm->value("rlat").toDouble(), x, y)) {
      gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
      gc->lineTo(x, y);
    }
  }
}


//-----------------------------------------------------------------------------
// Draw the selected track on the map
//
void CMapWnd::drawDatabaseAllTrk(GraphicPainter *gc)
{
  static int x, y;
  int isize, isizex2;
  isize = 2; //1;
  isizex2 = isize*2;

  gc->setPen(QPen(Qt::gray, THIN_PEN));

  g_AllMvm->first();
  while (g_AllMvm->next()) {
    if (LtoC(g_AllMvm->value("rlon").toDouble(), g_AllMvm->value("rlat").toDouble(), x, y))
      gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
  }
}


//-----------------------------------------------------------------------------
// Open the master (all) track information
// and set the flag as ready on success
//
void CMapWnd::loadGpsAllTrack()
{
  if (gpsAllTrk.Open("gps-trk.csv"))
    gpsAllTrkReady = true;
}

void CMapWnd::unloadGpsAllTrack()
{
  if (gpsAllTrk.Close())
    gpsAllTrkReady = false;
}


//-----------------------------------------------------------------------------
// Draw the master track db track on the map
//
void CMapWnd::drawGpsAllTrk(GraphicPainter *gc)
{
  int x, y;
  int _x, _y;
  int isize, isizex2;
  QString s;
  QString sVar;
  bool move_flag = 0;

  gc->setPen(QPen(Qt::gray, THIN_PEN));

  isize = 2; //1;
  isizex2 = isize*2;
  _x = 0;
  _y = 0;

  gpsAllTrk.MoveFirst();
  gpsAllTrk.MoveNext();
  LtoC(gpsAllTrk.m_rLong, gpsAllTrk.m_rLat, x, y);
  gc->moveTo(x, y);
  while (gpsAllTrk.IsEOF() == false) {
    LtoC(gpsAllTrk.m_rLong, gpsAllTrk.m_rLat, x, y);
    if ((gpsAllTrk.m_rLat == 0) && (gpsAllTrk.m_rLong == 0) && (gpsAllTrk.m_DTG ==0)) {
      move_flag = true;
      gpsAllTrk.MoveNext();
      continue;
    }

    if (move_flag) {
      gc->moveTo(x, y);
      move_flag = false;
    }

    gc->drawRectangle(x - isize, y - isize, isizex2, isizex2);
    gc->lineTo(x, y);
    gpsAllTrk.MoveNext();
  }
}


//-----------------------------------------------------------------------------
// Draw the current waypoint on the map
//
void CMapWnd::drawWpt(GraphicPainter *gc)
{
  int x, y;
  int isize;
  int a, b;
  char s[256];
  int iVar;

  int fmHeight = 0.60 * gc->fontMetrics().height();
  isize = fmHeight * 0.75;
  if (LtoC(wpt.m_rLon, wpt.m_rLat, x, y) || (m_bPrnActive && flatWorld)) {
    
    //g_wptMask = W_APT | W_VOR | W_NDB | W_USR | W_IFR | W_SLD; // to be removed
    
    // Todo: mask lower 4 bits like ApplicationWindow::createDatabaseTree()
    unsigned short control = (unsigned short) wpt.m_TYPE & g_wptMask;
    switch (control) {
      case W_APT | W_IFR | W_SLD: // Hack it for now
      case W_APT | W_SLD: // Hack it for now
      case W_IFR | W_SLD: 
      case W_IFR: 
      case W_SLD:
      case W_APT: {
        //-----------------------------
        // base sizing on font metrics
        //
        // Normal airports
        bool bNormalApt = true;
        isize = (int)(fmHeight * 0.6);

        // Smaller airports
        if (wpt.m_ICAO.length() < 4) {
          isize = (int)(fmHeight * 0.4);
          bNormalApt = false; 
        }

        //QRegExp rx( "\\d\\d\\d$" );      // match integers 000 to 999 at the end of a string
        //int rv = rx.search(wpt.m_ICAO);  // returns -1 (no match)

        gc->Ellipse(x - isize, y - isize, x + isize, y + isize);
        gc->moveTo(x - isize, y); gc->lineTo(x - 1.5*isize, y);
        gc->moveTo(x + isize, y); gc->lineTo(x + 1.5*isize, y);
        gc->moveTo(x, y - isize); gc->lineTo(x, y - 1.5*isize);
        gc->moveTo(x, y + isize); gc->lineTo(x, y + 1.5*isize);

        if (m_bTextActive) {
          if ((mag > 4) && (bNormalApt))
              gc->TextOut(x + isize, y + isize, wpt.m_ICAO);
          if (mag > 6) 
            gc->TextOut(x + isize, y + isize + fmHeight, wpt.m_INFO);
        }
        break;
      }
      case W_VOR : {
        gc->drawPoint(x, y);  // so that it will "tag"
        //gc->setPen(Qt::darkCyan);  //Qt::blue
        //isize = mag * 3;
        isize = fmHeight * 3;

        sprintf(s, "%s %s", wpt.m_ICAO.latin1(), wpt.m_INFO.latin1());
        gc->Ellipse(x - isize, y - isize, x + isize, y + isize);

        //
        // Legacy - If we really want, use
        // CGenMapWnd::getMagVar(double lat, double lon)
        //
        iVar = 0;

        for (int i = 0; i < 360; i += 30){
          int phi = 360 + 90 + i - iVar;
          if (i % 90 == 0) {
            a = uround(2*fmHeight*icos(phi));
            b = uround(2*fmHeight*isin(phi));
            gc->radLine(x+a, y-b, fmHeight, phi);
          }
          else {
            a = uround(2.5*fmHeight*icos(phi));
            b = uround(2.5*fmHeight*isin(phi));
            gc->radLine(x+a, y-b, 0.5*fmHeight, phi);
          }
        }
        if (m_bTextActive) {
          if (mag > 4)
            gc->TextOut(x + isize, y - isize, s);
        }
        break;
      }
      case W_NDB : {
        gc->drawPoint(x, y);
        //gc->setPen(QPen(Qt::darkCyan, 0, Qt::DotLine));
        //isize = mag * 0.75;
        isize = fmHeight * 0.75;

        sprintf(s, "%s %s", wpt.m_ICAO.latin1(), wpt.m_INFO.latin1());
        gc->Ellipse(x - isize, y - isize, x + isize, y + isize);
        isize = mag;
        gc->Ellipse(x - isize, y - isize, x + isize, y + isize);
        if (m_bTextActive) {
          if (mag > 4)
            gc->TextOut(x + isize, y - 2*isize, s);
        }
        break;
      }
      case W_USR : {
        gc->setPen(Qt::darkMagenta/*Qt::darkGray*/);  //Qt::blue
        //isize = mag * 0.50;
        isize = fmHeight * 0.50;
        gc->drawRectangle(x - isize, y - isize, isize, isize);
        if (m_bTextActive) {
          if (mag > 4)
            gc->TextOut(x + isize, y + isize, wpt.m_INFO);
        }
 
        break;
      }
      /*
      default : {
        //if (mag > 6) {
        {
          //isize = mag * 0.50;
          isize = fmHeight * 0.50;
          gc->drawRectangle(x - isize, y - isize, isize, isize);
          if (mag > 4)
            gc->TextOut(x + isize, y + isize, wpt.m_INFO.c_str());
        }
        break;
      }
      */
    }
  }
}
                                      

//-----------------------------------------------------------------------------
//
//
void CMapWnd::drawTxt(GraphicPainter *gc)
{
  int x, y;
  int isize;
  int FontHeight;

  FontHeight = (int) (2.5 * mag * FONT_SCALER);
  if (FontHeight <= MINFONTSIZE) FontHeight = 1;

  QFont font("Arial", FontHeight);
  gc->setFont(font);

  if (bShadeActive)
    gc->setPen(Qt::lightGray);
  else
    gc->setPen(Qt::gray);

  txt.MoveFirst();
  while (txt.IsEOF() == false) {
    // first 100 = country names
    if (txt.m_nCurRec == 100) {
      FontHeight = (int) (2.0 * mag * FONT_SCALER);
      if (FontHeight <= MINFONTSIZE) break; 

      QFont font("Arial", FontHeight);
      gc->setFont(font);
    }

    // 100 - 500 = city names
    if (txt.m_nCurRec == 500) {
      FontHeight = (int) (1.0 * mag * FONT_SCALER);
      if (FontHeight <= MINFONTSIZE) break; 

      QFont font("Arial", FontHeight);
      gc->setFont(font);
    }

    // 500 - 1000 = town names
    if (txt.m_nCurRec == 10000) {
      FontHeight = (int) (0.75 * mag * FONT_SCALER);
      if (FontHeight <= MINFONTSIZE) break; 

      QFont font("Arial", FontHeight);
      gc->setFont(font);
    } 

    //if (txt.m_nCurRec > 90000)
    //  break;

    if (LtoC(txt.m_rLon, txt.m_rLat, x, y) || (m_bPrnActive && flatWorld)) {
      //int fmHeight = 1.0 * gc->fontMetrics().height();
      //isize = fmHeight * 0.50;
      isize = 0.50 * gc->fontMetrics().height();
      gc->TextOut(x + isize, y + isize, txt.m_NAME);
    }
    txt.MoveNext();
  }


  streetTxt.MoveFirst();
  while (streetTxt.IsEOF() == false) {
    // street names
    if (streetTxt.m_nCurRec == 0) { // hack
      FontHeight = (int) (0.375 * mag * FONT_SCALER);
      if (1.1*FontHeight <= MINFONTSIZE) break;  //1.025*

      QFont font("Arial", FontHeight);
      gc->setFont(font);
    } 

    if (LtoC(streetTxt.m_rLon, streetTxt.m_rLat, x, y) || (m_bPrnActive && flatWorld)) {
      isize = 0.50 * gc->fontMetrics().height();
      gc->TextOut(x + isize, y + isize, streetTxt.m_NAME);
    }
    streetTxt.MoveNext();
  }
}


//-----------------------------------------------------------------------------
//
//
void CMapWnd::choosePen(GraphicPainter *gc, long iType)
{
  if ((iType & W_APT) == W_APT)
    gc->setPen(Qt::darkGreen);  

  if ((iType & W_SLD) == W_SLD)
    gc->setPen(Qt::black);  

  if ((iType & W_IFR) == W_IFR)
    gc->setPen(Qt::blue);  


  if ((iType & W_VOR) == W_VOR)
    gc->setPen(Qt::darkCyan);  

  if ((iType & W_NDB) == W_NDB)
    gc->setPen(QPen(Qt::darkCyan, 0, Qt::DotLine));

/*  
  switch (iType) {
    case W_IFR | W_SLD: 
    case W_IFR: 
    case W_SLD:
    case W_APT: 
      gc->setPen(Qt::darkGreen);  

      if ((iType & W_SLD) == W_SLD)
        gc->setPen(Qt::black);  
      
      if ((iType & W_IFR) == W_IFR)
        gc->setPen(Qt::blue);  

      // need to make provision for unsealed IFR ??

      //if (bShadeActive)
      //  gc->setPen(Qt::white); 

      break;
    
    case W_VOR: 
      gc->setPen(Qt::darkCyan);  
      break;

    case W_NDB: 
      gc->setPen(QPen(Qt::darkCyan, 0, Qt::DotLine));
      break;
  }
  */
}

//-----------------------------------------------------------------------------
//
//
void CMapWnd::drawAllWpt(GraphicPainter *gc)
{
  int FontHeight;

  FontHeight = (int) (mag * FONT_SCALER);
  if (FontHeight <= MINFONTSIZE) FontHeight = 1;
  if (FontHeight > MAXFONTSIZE)  FontHeight = MAXFONTSIZE;

  QFont font("Arial", FontHeight);
  gc->setFont(font);

  // debug
  int ps = font.pointSize();
  int fmHeight = 0.75 * gc->fontMetrics().height();
  // rebug

  if (bShadeActive)
    gc->setPen(Qt::white);
  else
    gc->setPen(Qt::black);

  wpt.MoveFirst();
  while (wpt.IsEOF() == false) {
    choosePen(gc, wpt.m_TYPE);
    drawWpt(gc);
    wpt.MoveNext();
  }
}

 
//-----------------------------------------------------------------------------
//
//
void CMapWnd::drawAirspace(GraphicPainter *gc)
{
  /*
  int FontHeight;
  FontHeight = (int) (0.8 * mag * FONT_SCALER);
  if (FontHeight <= MINFONTSIZE) FontHeight = 1;
  if (FontHeight > MAXFONTSIZE)  FontHeight = MAXFONTSIZE;
  QFont font("Arial", FontHeight);
  */

  int fmHeight = 0.50 * gc->fontMetrics().height();
  QFont font("Arial", fmHeight);

  gc->setFont(font);
  int isize = gc->fontMetrics().height();

  air.MoveFirst();

  // If set use a wide pen for airspace.
  // Note: This incurrs a sigificant performace penalty
  if (bAirPenWide) {
    // go with the wide pen (3 pixels) - slow
    gc->setPen(QPen(Qt::blue, WIDE_PEN));
  }
  else {
    // go with the thin (1 pixel) pen - much faster
    gc->setPen(QPen(Qt::blue, THIN_PEN));
  }

  while (air.IsEOF() == false) {
    // we must at least have a valid lat2/lon2 before
    // we can make anything of the entry, otherwise
    // move along ... nothing to see here.
    if ((air.m_rLat2 != 0) && (air.m_rLon2 != 0)) {
      // for arcs lat1/lon1 must be zero
      if ((air.m_rLat1 == 0) && (air.m_rLon1 == 0)) {
        // an arc it is we will be drawing
        drawArc(gc, air.m_rLat2, air.m_rLon2, air.m_radius, air.m_a1, air.m_a2);
 
        //
        // Attempt a label for LL and UL
        //
        if (mag > 6 /*7*/) {
          // if LL == UL == 0 we interpret it to mean
          // that we don not want to display anything
          if ((air.m_alt1 != 0) && (air.m_alt2 != 0)) {
            // convert from nm and use the "magic" number 1.05
            // "magic" number to work around a error, rounding perhaps?
            int x, y;
            double cor = 1.05/3600.0;
            double Lat=0, Lon=0;
            int phi;
 
            //
            // Choose a "radial" for our label as
            // midway between the start and end radials of
            // an arc
            //
            if (air.m_a2 > air.m_a1)
              phi = air.m_a1 + (air.m_a2-air.m_a1)/2;
            else
              phi = air.m_a1 + (air.m_a2-air.m_a1+360)/2;
 
            GreatCircle::getRadialDmePoint(&Lat, &Lon, air.m_rLat2, air.m_rLon2, cor*air.m_radius, -phi);
            if (LtoC(Lon, Lat, x, y)) {
              gc->drawText(x - 300, y         - 200, 600, 400, Qt::AlignCenter, air.m_INFO.c_str());
              gc->drawText(x - 300, y+isize   - 200, 600, 400, Qt::AlignCenter, QString("UL %1").arg(air.m_alt2));
              gc->drawText(x - 300, y+isize*2 - 200, 600, 400, Qt::AlignCenter, QString("LL %1").arg(air.m_alt1));
            }
          }
        }
      }
      else {
        // a line we will be drawing
        drawLine(gc, air.m_rLat1, air.m_rLon1, air.m_rLat2, air.m_rLon2);
      }
    }
    air.MoveNext();
  }
}


//-----------------------------------------------------------------------------
// Draw the route on the map. Firstly 'cleaning'
// a swath around old tracks and update the workheet.
// 

//
// As a rhumb line
//
/*
#if 0
void CMapWnd::drawRouteRhumbLine(GraphicPainter *gc)
{
  int i;
  int x, y;

  //
  // Clear a 'swath' around our plotted track.
  // It also serves to get rid of most of the
  // overshoot construction lines created during
  // drag and drop.
  //
  gc->setPen(QPen(gc->backgroundColor(), FAT_PEN));

  wpt.MoveFirst();
  wpt.Move(RouteLst[0]);
  LtoC(wpt.m_rLong, wpt.m_rLat, x, y);
  gc->moveTo(x ,y);
  for (i = 0; i < RouteWpt; i++) {
    if (RouteLst[i] != 0) {
      wpt.MoveFirst();
      wpt.Move(RouteLst[i]);
      LtoC(wpt.m_rLong, wpt.m_rLat, x, y);
      gc->lineTo(x ,y);
    }
    else {
      wpt.MoveFirst();
      wpt.Move(RouteLst[i+1]);
      LtoC(wpt.m_rLong, wpt.m_rLat, x, y);
      gc->moveTo(x ,y);
    }
  }

  //
  // Plot the route legs
  //
  wpt.MoveFirst();
  wpt.Move(RouteLst[0]);
  LtoC(wpt.m_rLong, wpt.m_rLat, x, y);
  gc->moveTo(x ,y);
  for (i = 0; i < RouteWpt; i++) {
    if (RouteLst[i] != 0) {
      wpt.MoveFirst();
      wpt.Move(RouteLst[i]);
      LtoC(wpt.m_rLong, wpt.m_rLat, x, y);
      //
      // choose a wide pen to display the route legs
      //
      gc->setPen(QPen(Qt::black, WIDE_PEN));

      gc->lineTo(x, y);
      //
      // reset the pen back to our default of a thin pen
      //
      gc->setPen(QPen(Qt::black, THIN_PEN));
      drawWpt(gc);
      gc->moveTo(x, y);  // reset the cp clobbered by drawWpt
    }
    else {
      wpt.MoveFirst();
      wpt.Move(RouteLst[i+1]);
      LtoC(wpt.m_rLong, wpt.m_rLat, x, y);
      gc->moveTo(x ,y);
    }
  }
  
  //
  // Update our worksheets
  //
  EnterMovement();
  populateMagnetic();
}
#endif
*/

//
// As a great circle
//
void CMapWnd::drawRoute(GraphicPainter *gc)
{
  int i;
  int x, y;
  int x1, y1, x2, y2;
  double minr = 10000;
  double maxr = -1;
  double lat1, lon1, lat2, lon2;

  int FontHeight;
  FontHeight = (int) (mag * FONT_SCALER);
  if (FontHeight <= MINFONTSIZE) FontHeight = 1;
  if (FontHeight > MAXFONTSIZE)  FontHeight = MAXFONTSIZE;
  QFont font("Arial", FontHeight);
  gc->setFont(font);

 
  // 60 nm radius
  LtoC(left, bottom, x1, y1);
  LtoC(left, bottom - 1.0, x2, y2);
  int nm60 = abs(y2 - y1);   // 1 deg lat = 60nm

  //
  // Clear a 'swath' around our plotted track.
  // It also serves to get rid of most of the
  // overshoot construction lines created during
  // drag and drop.
  //
  
  //~~
  //gc->setPen(QPen(gc->backgroundColor(), FAT_PEN, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
  gc->setPen(QPen(QColor(255, 255, 196), FAT_PEN, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

  wpt.MoveFirst();
  wpt.Move(RouteLst[0]);
  LtoC(wpt.m_rLon, wpt.m_rLat, x, y);
  gc->moveTo(x ,y);
  lat1 = wpt.m_rLat;
  lon1 = wpt.m_rLon;

  for (i = 0; i < RouteWpt; i++) {
    if (RouteLst[i] != 0) {
      wpt.MoveFirst();
      wpt.Move(RouteLst[i]);
      lat2 = wpt.m_rLat;
      lon2 = wpt.m_rLon;

      // record non zero min and max r while we are at it
      // we use this to scale the node rose in the
      // next section
      double rr = hypotenuse(lat1, lon1, lat2, lon2);
      if (rr > 0.0) {
        if (rr > maxr) maxr = rr;
        if (rr < minr) minr = rr;
      }

      drawLine(gc, lat1, lon1, lat2, lon2);
      lat1 = lat2;
      lon1 = lon2;
    }
  }

  int nmul = floor(minr);

  //
  // Plot the route legs
  //
  wpt.MoveFirst();
  wpt.Move(RouteLst[0]);
  LtoC(wpt.m_rLon, wpt.m_rLat, x, y);
  gc->moveTo(x ,y);
  lat1 = wpt.m_rLat;
  lon1 = wpt.m_rLon;
  for (i = 0; i < RouteWpt; i++) {
    if (RouteLst[i] != 0) {
      wpt.MoveFirst();
      wpt.Move(RouteLst[i]);

      lat2 = wpt.m_rLat;
      lon2 = wpt.m_rLon;

      if (bNodeRoseActive) {
        LtoC(lon2, lat2, x2, y2);

        drawRose(gc, x2, y2, nm60 / 2 /* * nmul*/);  // fixed at 60 nm diameter
      }

      // choose a wide pen to display the route legs
      gc->setPen(QPen(Qt::black, WIDE_PEN, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
      drawLine(gc, lat1, lon1, lat2, lon2);

      // reset the pen back to our default of a thin pen
      gc->setPen(QPen(Qt::black, THIN_PEN));
      drawWpt(gc);

      lat1 = lat2;
      lon1 = lon2;
    }
  }
 
  //
  // Update our worksheets
  //
  for (int row = 0; row < NR_LEGS + 2; row++) {
    for (int col = 0; col < MAXCOLOM; col++) {
      trueTable->clearCell(row, col);
      magneticTable->clearCell(row, col);
    }
  }
 
  populateTrue();
  populateMagnetic();
}


//-----------------------------------------------------------------------------
// Clear the current route from a little endian file
//
void CMapWnd::clearRoute()
{
  int iRow, iCol;

  for (iRow = 0; iRow < RouteWpt+3; iRow++) {
    for (iCol = 0; iCol < MAXCOLOM; iCol++) {
      trueTable->setText(iRow, iCol, NULL);
      magneticTable->setText(iRow, iCol, NULL);
    }
  }

  _bLbuttonDown = false;
  //leave it alone  mapPane.bAddRteActive = false;
  // mapPane.nLineCtr = 0;
  RouteWpt = 0;
  memset(RouteLst, 0x0, sizeof(RouteLst));
  iInsertAt = 0;
  iSeg = 0;
  lastSeg = 0;
  bRouteSaved = true;
}


//-----------------------------------------------------------------------------
// Save the current route to a little endian file
//
void CMapWnd::saveRoute(const char *szFileName)
{
  try {
    QFile fout(szFileName);
    fout.open(IO_WriteOnly);
    QDataStream fdata(&fout);

    fdata << (long) RouteWpt;
    for (int i = 0; i < RouteWpt; i++) {
      wpt.Move(RouteLst[i]);
      fdata << (long) wpt.m_ID; //fdata.writeLong(wpt.m_ID);
    }
    bRouteSaved = true;
  }
  catch (...) {
    printf("CMapWnd::saveRoute - %s could not be saved\n", szFileName);
  }
}


//-----------------------------------------------------------------------------
// Load the current route from a little endian file
//
bool CMapWnd::loadRoute(const char *szFileName)
{
  // Load the new file
  QFile fin(szFileName);
  if (fin.open(IO_ReadOnly)) {
    QDataStream fdata(&fin);

    fdata >> RouteWpt;

    //
    // Perform some sanity checks on the number of legs
    //
    if ((RouteWpt < 0) ||
        (RouteWpt > NR_LEGS)) {
      RouteWpt = 0;
      QMessageBox::information(NULL, SZ_PRGNAME,
                             QString("File corrupt - %1").arg(szFileName));
      printf("CMapWnd::loadRoute - %s File corrupt\n", szFileName);
      return false;
    }

    //
    // Read each of the waypoint id's
    //
    // The assumption is that the wpt database id's are unique and
    // unchanged.
    //
    for (int i = 0; i < RouteWpt; i++) {
      Q_INT32 wptId;
      fdata >> wptId;

      wpt.MoveFirst();
      while (wpt.IsEOF() == false) {
        if (wpt.m_ID == wptId) {
          //RouteLst[i] = wpt.GetCP(); //wpt.cp;
          RouteLst[i] = wpt.m_nCurRec; // for no_cache ??
          
          break;
        }
        wpt.MoveNext();
      }
    }
    bRouteSaved = true;
    return true;
  }
  else {
    printf("CMapWnd::loadRoute - %s could not be read\n", szFileName);
    return false;
  }
}

//-----------------------------------------------------------------------------
// Look up the waypoints for each entry in the RouteLst
// and update trueTable accordingly
//
//void CMapWnd::EnterMovement()
void CMapWnd::populateTrue()
{
  int i;
  int iTrack = 0;
  int iDist = 0;
  int iTas = 0;  
  int iFFlo = 999;
  int iWind = 36000;          
  int iHeading = 0;
  int iGS = 0;
  double Heading = 0;
  double GS = 0;
  int iTotDist = 0;
  double lat1 = 9999;
  double lon1 = 9999;
  double lat2, lon2;
  double rvar = 9999;
  int row = -1;

  //todo trueTable.removeAll();
  wpt.Move(RouteLst[0]);
  for (i = 0; i < RouteWpt; i++) {
    wpt.Move(RouteLst[i]);
    if (RouteLst[i] != 0) {
      lat2 = wpt.m_rLat;   //2 = aid 1 = gps
      lon2 = wpt.m_rLon;
      if (lat1 != 9999) {
        // get the magnetic variation midway
        rvar = getMagVar((lat1+lat2)/2, (lon1+lon2)/2);

        // Approximated Rumb Line navigation
        double depy = lat2 - lat1;
        double depx = (lon2 - lon1)*cos((lat1 + lat2)*M_PI/180.0/2.0);

        // true track
        if (depy < 0)
          iTrack = (int) ((atan(depx/depy)*180/M_PI) + 180);
        else
          iTrack = (int) ((atan(depx/depy)*180/M_PI));

        // true heading & ground speed
        iTas  = TasLst[i];
        iWind = WindLst[i];
        iFFlo = FuelFlowLst[i];
        WindTriangle::calcHdGs(iTrack, iTas, iWind, &Heading, &GS);
        iHeading = Heading;
        iGS = GS;

        //iHeading = (int) WindTriangle.m_HD;
        //iGS = (int) WindTriangle.m_GS;

        iDist = (int) (sqrt(sqr(depy) + sqr(depx))*60);
        while (iTrack < 0) iTrack += 360;

        /*  maybe later ?
        // great circle navigation
        rDist  = GreatCircle.getDistance(lat1, lon1, lat2, lon2);
        rT12 = GreatCircle.getCrs12(lat1, lon1, lat2, lon2) + rvar;
        rT21 = GreatCircle.getCrs21(lat1, lon1, lat2, lon2) + rvar;
        while (rT12 < 0) rT12 += 360.0;
        while (rT21 < 0) rT21 += 360.0;
        System.out.println("t12= " + rT12 + "t21= " + rT21 + " d= " + rDist);
        */
      }
      row++;
      //
      // Note the use of our very own TableItem. This is done so that we
      // can display the entries as "greyed out"
      //
      trueTable->setText(row, 0, wpt.m_ICAO);
      //trueTable->setItem(row, 1, new TableItem(trueTable, TableItem::Never, wpt.m_INFO.c_str()));
      trueTable->setText(row, 1, wpt.m_INFO);
      trueTable->setItem(row, 2, new TableItem(trueTable, TableItem::Never, QString().sprintf("%04.2f", wpt.m_rLat)));  
      trueTable->setItem(row, 3, new TableItem(trueTable, TableItem::Never, QString().sprintf("%05.2f", wpt.m_rLon)));  
      trueTable->setItem(row, 4, new TableItem(trueTable, TableItem::Never, QString("%1").arg(wpt.m_rElev)));  
      if (rvar != 9999)
        trueTable->setItem(row, 5, new TableItem(trueTable, TableItem::Never, QString("%1").arg(rvar)));  

      if (iDist != 0) {
        trueTable->setItem(row, 6, new TableItem(trueTable, TableItem::Never, QString("%1").arg(iTrack)));  
        trueTable->setItem(row, 7, new TableItem(trueTable, TableItem::Never, QString("%1").arg(iDist)));  
        trueTable->setText(row, 8, QString("%1").arg(iTas));
        trueTable->setText(row, 9, QString().sprintf("%04i", iWind));
        trueTable->setItem(row, 10, new TableItem(trueTable,  TableItem::Never, QString("%1").arg(iHeading)));  
        trueTable->setItem(row, 11, new TableItem(trueTable, TableItem::Never, QString("%1").arg(iGS)));  
        trueTable->setText(row, 12, QString("%1").arg(iFFlo));  
        iTotDist += iDist;
      }
      lat1 = wpt.m_rLat;
      lon1 = wpt.m_rLon;
    }
  }

  row++;
  //trueTable.setValueAt(" ", row, 0); //- we leave this one out, it makes manual entry easier
  trueTable->setText(row, 0, NULL); 
  trueTable->clearCell(row, 0);
  trueTable->setText(row, 1, NULL);
  trueTable->clearCell(row, 1);

  for (i = 2; i < MAXCOLOM; i++) {
    //trueTable->setItem(row, i, new TableItem(magneticTable, TableItem::Never, QString("---")));
    trueTable->setText(row, i, "----");  // maybe better ?

    // clear the next two rows as well (sub totals and totals)
    trueTable->setText(row + 1, i, NULL);
    trueTable->clearCell(row + 1, i);

    trueTable->setText(row + 2, i, NULL);
    trueTable->clearCell(row + 2, i);
  }

  row++;
  //trueTable->setText(row, 6, QString("%1").arg(iTotDist));
  trueTable->setItem(row, 7, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(iTotDist)));

  row++;
}

//                          0       1       2       3         4      5        6          7      8       9          10
//  tableColumnNames =    {"ICAO", "Info", "Lat",  "Long",   "Var", "Track", "Dist",    "TAS", "Wind", "Heading", "GS"};
//  magneticColumnNames = {"ICAO", "Info", "Track","Dist",   "TAS", "Wind",  "Heading", "GS",  "Time", "Fuel",    " "};

//-----------------------------------------------------------------------------
// Look up the waypoints for each entry in the RouteLst
// and update magneticTable accordingly
//
void CMapWnd::populateMagnetic()
{
  int row;
  int iTotTime = 0;
  //int iTotFuel = 0;
  double rTotFuel = 0;
  QString s; 

  // The first row is the starting point and does
  // not display any leg information
  row = 0;
  
  magneticTable->setItem(row, 0, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(trueTable->text(row, 0))));
  magneticTable->setItem(row, 1, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(trueTable->text(row, 1))));
  
  // The rest of the rows describe the legs
  for (row = 1; row < RouteWpt; row++) {
    int iVar = 999;
    int iTrk = 999;
    int iDist = 999;
    int iTas = 999;
    int iWind = 36000;
    int iFFlo = 999;
  
    iVar = trueTable->text(row, 5).toInt();
    iTrk = trueTable->text(row, 6).toInt();
    iDist = trueTable->text(row, 7).toInt();
    iTas  = trueTable->text(row, 8).toInt();
    iWind = trueTable->text(row, 9).toInt();
    iFFlo = trueTable->text(row, 12).toInt();
  
    double Hdg;
    double Gs;
    WindTriangle::calcHdGs(iTrk, iTas, iWind, &Hdg, &Gs);
    int iHdg = Hdg;

    // make sure we do not div zero
    int iGs;   
    if (Gs > 0) 
      iGs = Gs;
    else
      iGs = 1;

    int iTime = 60 * iDist / iGs;
    double rFuel = double (iFFlo * iTime) / 60.0;
    iTotTime += iTime;
    rTotFuel += rFuel;
  
    magneticTable->setItem(row, 0, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(trueTable->text(row, 0))));
    magneticTable->setItem(row, 1, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(trueTable->text(row, 1))));
    magneticTable->setItem(row, 2, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(iTrk + iVar)));
    magneticTable->setItem(row, 3, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(trueTable->text(row, 7))));
    magneticTable->setItem(row, 4, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(trueTable->text(row, 8))));
    magneticTable->setItem(row, 5, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(trueTable->text(row, 9))));
    magneticTable->setItem(row, 6, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(iHdg + iVar)));
    magneticTable->setItem(row, 7, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(trueTable->text(row, 11))));
    s.sprintf("%02d:%02d", iTime/60, iTime%60);
    magneticTable->setItem(row, 8, new TableItem(magneticTable, TableItem::Never, s)); 
    //magneticTable->setItem(row, 9, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(iFuel))); 
    //s.sprintf("%02f", rFuel);
    magneticTable->setItem(row, 9, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(rFuel, 3, 'f', 1))); 
  }
  
  //row++;    
  
  for (int i = 0; i < MAXCOLOM; i++) {
    // memleak> magneticTable->setText(row, i, "----");
    magneticTable->setItem(row, i, new TableItem(magneticTable, TableItem::Never, QString("---")));

    // clear the next two rows as well
    magneticTable->setText(row + 1, i, " ");
    magneticTable->clearCell(row + 1, i);

    magneticTable->setText(row + 2, i, " ");
    magneticTable->clearCell(row + 2, i);
  }
  
  row++;
  magneticTable->setItem(row, 3, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(trueTable->text(row, 7))));
  s.sprintf("%02d:%02d", iTotTime/60, iTotTime%60);
  magneticTable->setItem(row, 8, new TableItem(magneticTable, TableItem::Never, s));
  magneticTable->setItem(row, 9, new TableItem(magneticTable, TableItem::Never, QString("%1").arg(rTotFuel, 3, 'f', 1)));

 
  row++;
}

//-----------------------------------------------------------------------------
// Allow to set alteration to the column widths.
//
// @param witdh the desired width of the columns -1 will 'autofit'. <br>
// note: default system column width is 64
//
void CMapWnd::autoFitTableColums(int width)
{
  for (int i = 0; i < MAXCOLOM; i++) {
    if (width < 0) {
      magneticTable->adjustColumn(i);
      trueTable->adjustColumn(i);
    }
    else {
      magneticTable->setColumnWidth(i, width);
      trueTable->setColumnWidth(i, width);
    }
  }
}


//-----------------------------------------------------------------------------
// Save RouteWpt and RouteLst for the undo buffer
//
void CMapWnd::pushUndoBuffer()
{
  _RouteWpt = RouteWpt;
  memcpy(_RouteLst, RouteLst, sizeof(_RouteLst));
}


//-----------------------------------------------------------------------------
// Handle a change to entries in the route table
//   - handle a manual TAS and/or Wind entry
//
// @param row the row that was edited
// @param col the column that was edited
//
void CMapWnd::trueTableEdited(int row, int col)
{
  int iTrk, iVar, iDist, iTas, iFF;
  int iWind = 36000;
  int iGs, iHdg;
  int iTime; // in minutes
  int iFuel; // in minutes

  if ((row < 0) || (col < 0))
    return;

  //
  // Check and handle an ICAO designator typed in
  //
  if ((col == 0) || (col == 1)) {
    int iWpt;

    if (col == 0) {
      printf("ICAO (wpt) entry detected\n");
      iWpt = findIcaoWaypoint(trueTable->text(row, 0).upper().latin1());
    }
    if (col == 1) {
      printf("Info (wpt) entry detected\n");
      iWpt = findInfoWaypoint(trueTable->text(row, 1).upper().latin1());
    }

    if (iWpt < 0) {
      ZoomToTrack();
      return;
    }
    // Add this Wpt to our route list
    if (iInsertAt == 0) {
      // Save RouteWpt and RouteLst for the undo buffer
    //_RouteWpt = RouteWpt;
      //memcpy(_RouteLst, RouteLst, sizeof(_RouteLst));
    pushUndoBuffer();

      // Make sure we do not add multiples
      if (row == 0) {
        RouteLst[row] = iWpt;
        if (RouteWpt == 0)
          RouteWpt++;
      }
      else {
        RouteLst[row] = iWpt;
        if (row >= RouteWpt) {
          // fill the empty one(s) with the current wpt
          for (int i = RouteWpt; i <= row; i++) {
            RouteLst[i] = iWpt;
          }
          RouteWpt = row + 1;
        }
      }
      bRouteSaved = false;
    }
    ZoomToTrack();
    return;
  }

  //
  // Check and handle a TAS typed in
  //
  if ((row > 0) && (col == 8)) {
    iTas = trueTable->text(row, 8).toInt();
    TasLst[row] = iTas;
    printf("TAS entry detected: %d", iTas);
  }

  if ((row > 0) && (col == 9)) {
    iWind = trueTable->text(row, 9).toInt();
    WindLst[row] = iWind;
    printf("Wind entry detected: %05d", iWind);
  }
  if ((row > 0) && (col == 12)) {
    iFF = trueTable->text(row, 12).toInt();
    FuelFlowLst[row] = iFF;
    printf("Fuel Flow entry detected: %05d", iFF);
  }

  // true heading & ground speed
  // read the required fields and update
  iVar  = trueTable->text(row, 5).toInt();
  iTrk  = trueTable->text(row, 6).toInt();
  iDist = trueTable->text(row, 7).toInt();
  iTas  = trueTable->text(row, 8).toInt();

  double dHdg, dGs;
  WindTriangle::calcHdGs(iTrk, iTas, iWind, &dHdg, &dGs);
  iHdg = dHdg; //(int) WindTriangle.m_HD;
  iGs  = dGs;  //(int) WindTriangle.m_GS;
  if (iGs == 0)
    iTime = 99999;
  else
    iTime = 60 * iDist / iGs;
  iFuel = m_FuelFlow * iTime / 60;

  // populate the trueTable
  trueTable->setText(row,  8, QString("%1").arg(iTas));
  trueTable->setText(row,  9, QString("%1").arg(iWind));
  trueTable->setText(row, 10, QString("%1").arg(iHdg));
  trueTable->setText(row, 11, QString("%1").arg(iGs));

  // populate the magneticTable
  populateMagnetic();
}


//-----------------------------------------------------------------------------
// Look up the waypoints for each entry in the RouteLst
//  and print out a plan
//
/*
void printMovement() {
  int i;
  int x, y;
  int nItem;
  String sType;
  String s;
  int iTrack = 0;
  int iDist = 0;
  int iTotDist = 0;
  double lat1 = 9999;
  double lon1 = 9999;
  double lat2, lon2;
  double rvar = 9999;
  double d, tc1;
  double departure;
  int iRow = -1;

  trueTable.removeAll();
  wpt.Move(RouteLst[0]);
  for (i = 0; i < RouteWpt; i++) {
    wpt.Move(RouteLst[i]);
    if (RouteLst[i] != 0) {
      lat2 = wpt.m_rLat;   //2 = aid 1 = gps
      lon2 = wpt.m_rLong;
      if (lat1 != 9999) {
        rvar = getMagVar((lat1+lat2)/2, (lon1+lon2)/2);
        double depy = lat2 - lat1;
        double depx = (lon2 - lon1)*Math.cos((lat1 + lat2)*Math.PI/180.0/2.0);
        if (depy < 0)
          iTrack = (int) ((Math.atan(depx/depy)*180/Math.PI) + 180 + rvar);
        else
          iTrack = (int) ((Math.atan(depx/depy)*180/Math.PI) + rvar);

        iDist = (int) (Math.sqrt(UMath.sqr(depy) + UMath.sqr(depx))*60);
        while (iTrack < 0) iTrack += 360;
      }
      iRow++;
      System.out.print(wpt.m_ICAO + "\t");
      System.out.print(wpt.m_INFO + "\t");
      System.out.print(new Double(wpt.m_rLat) + "\t");
      System.out.print(new Double(wpt.m_rLong) + "\t");
      if (rvar != 9999)
        //System.out.print(new Double(rvar) + "\t");
        trueTable.setValueAt(new Long(Math.uround(rvar)),   iRow, 4);
      if (iDist != 0) {
        System.out.print(new Integer(iTrack) + "\t");
        System.out.print(new Integer(iDist) + "\t");
        iTotDist += iDist;
      }
      lat1 = wpt.m_rLat;
      lon1 = wpt.m_rLong;
      System.out.println();
    }
  }

  System.out.println();
  iRow++;
  System.out.print("----");
  System.out.print("----");
  System.out.print("----");
  System.out.print("----");
  System.out.print("----");
  System.out.print("----");
  System.out.print("----");
}
*/


//---------------------------------------------------------------------------
// Respond to paint messages by redrawing the entire map
//

/*void CMapWnd::OnPaint()
{
  CPaintDC gc(this); // device context for painting
  int FontHeight = (int) mag * 3;//2;
  CFont Font, *OldFont;

  if (FontHeight <= 4) FontHeight = 1;
  if (FontHeight > 12) FontHeight = 12;
  SetupFont(&Font, FontHeight);

  OldFont = (CFont*) gc.SelectObject(&Font);
 
  bIsBusy = TRUE;

  //if (!bAddRteActive)
    PaintMain(&gc);  // the whole echilada

  DrawNewNames(&gc);

  bIsBusy = FALSE;

  gc.SelectObject(GetStockObject(NULL_BRUSH));
  if (iBackGround || bShadeActive)
    gc.SetTextColor(RGB_WHITE);
  else
    gc.SetTextColor(RGB_BLACK);

  gc.SetBkMode(TRANSPARENT);

  DrawWpt(&gc);
  DrawRoute(&gc);
  DrawMovements(&gc);
  // Do not call CWnd::OnPaint() for painting messages
}*/


//---------------------------------------------------------------------------
// Zoom to the complete current plan
//
void CMapWnd::ZoomToTrack()
{
  int i;
  //int x, y;
  //double Lat, Lon;
  double minLat =  999;
  double maxLat = -999;
  double minLon =  999;
  double maxLon = -999;

  wpt.MoveFirst();
  wpt.Move(RouteLst[0]);
  for (i = 0; i < RouteWpt; i++) {
    wpt.MoveFirst();
    wpt.Move(RouteLst[i]);

    if (RouteLst[i] != 0) {
      if (wpt.m_rLat > maxLat) maxLat = wpt.m_rLat;
      if (wpt.m_rLat < minLat) minLat = wpt.m_rLat;
      if (wpt.m_rLon > maxLon) maxLon = wpt.m_rLon;
      if (wpt.m_rLon < minLon) minLon = wpt.m_rLon;
    }
  }

  ZoomToRect(minLat, maxLat, minLon, maxLon);

  /*
  ZoomToWorld();
  Lat = (minLat + maxLat) / 2;
  Lon = (minLon + maxLon) / 2;
  LtoC(Lon, +Lat, x, y);
  POINT p = {x, y};
  PanCenter(p);

  while ((left < minLon) &&
         (right > maxLon) &&
         (bottom < minLat) && 
         (top > maxLat) &&
         (mag < 16)) {

    ZoomIn();
  }
  ZoomOut();
  */
}


//-----------------------------------------------------------------------------
// Return the offset in the wpt database corresponding to the first entry
// indentified by the ICAO designator sIcao
//
int CMapWnd::findIcaoWaypoint(const char *sIcao)
{
  int iWpt = 0; 

  wpt.MoveFirst();
  while (wpt.IsEOF() == false) {
    //if (wpt.m_ICAO.equals(sIcao.toUpperCase())) {
    //if (strcmp(wpt.m_ICAO, sIcao) == 0) {
    if (wpt.m_ICAO.compare(sIcao) == 0) { 
      return iWpt;
    }
    iWpt++;
    wpt.MoveNext();
  }
  return -1;
}


//-----------------------------------------------------------------------------
// Return the offset in the wpt database corresponding to the first entry
// containing the letters of sInfo
//
int CMapWnd::findInfoWaypoint(const char *sInfo)
{
  int iWpt = 0; 

  wpt.MoveFirst();
  while (wpt.IsEOF() == false) {
    //if (wpt.m_ICAO.equals(sIcao.toUpperCase())) {
    //??if (QString(wpt.m_INFO).contains(sInfo, false)){
    if (wpt.m_INFO.contains(sInfo, false)) {
      return iWpt;
    }
    iWpt++;
    wpt.MoveNext();
  }
  return -1;
}


//-----------------------------------------------------------------------------
//
//
int CMapWnd::findNearestWaypoint(double rLat, double rLon)
{
  //double rLat, rLong;
  int i;
  //  int iNav;
  ///int iApt;
  int iWpt = 0; // hack - java ??
  double r;
  double rNav = 10000;
  double rApt = 10000;
  double rWpt = 10000;

  //rLong = m_rlon; rLat = m_rlat;
  // Find the nearest wpt
  i = 0;
  wpt.MoveFirst();
  while (wpt.IsEOF() == false) {
    r = sqrt(sqr(rLat - wpt.m_rLat) + sqr(rLon - wpt.m_rLon));
    if (r < rWpt) {
      rWpt = r;
      iWpt = i;
    }
    i++;
    wpt.MoveNext();
  }

  if (rWpt > 0.025) {
    iInsertAt = iSeg;
  }
  else {
    // we are not near enough to anything
    iInsertAt = 0;
  }

  return iWpt;
}


//drawShadeT(gc, -top, left, -bottom, right, "temp/IDR482.png", -30.78, 121.46, 2.304*2);

//-----------------------------------------------------------------------------
// Draw a shaded relief tile.
// A specialised helper function to assist with the experimental weather radar
// displays
//
void CMapWnd::drawShadeT(GraphicPainter *pDC, const char *szFileName, const double cLat, const double cLon, const double rSpan)
{
//  char szFileName[256];
  int x, y;
  int width, height;
  //double Lat, Long;
  //double nSpan;
  //double rMag;
  int idx = 0;
  QImage sci;

  if (!flatWorld)
    return;  // shade is only available for flatworld

  if (mag > 12)
    return;  // not sensible anymore we do not have
             // data for this level of zoom

  /*
  nSpan = 90.00;
  if (mag > span1000) nSpan = 10.00;
  if (mag > span0100) nSpan =  1.00;
  if (mag > span0025) nSpan =  0.25;
  */

  //nSpan = 1.0;  //dummy
  //nSpan = 4.6126;  //256 km = 4.6126 deg lat
  //nSpan = 2.304;  //128 km = 2.304 deg lat

  width = screenWidth;
  double pixperdeg = width / (right - left);
  //rMag = pixperdeg / (BM_HEIGHT / nSpan);

  //printf("CGenMapWnd::drawShadeR - rMag=%f\n", rMag);

  // some stats for our cache
  int chit = 0;
  int cmiss = 0;
  int creq = 1;

  // Set up a blank image
  QImage image;
  QImage blankImage("blank.png");

  width = -1;
  height = -1;
  y = -2;


  //for (Lat = (utrunc(rtop / nSpan)) * nSpan - nSpan; Lat < rbottom; Lat = Lat + nSpan) {
  {
    //if (Lat > -bottom) continue;
    y++;
    x = -1;
    width = -1;
    //for (Long = (utrunc(rleft / nSpan)) * nSpan - nSpan; Long < rright; Long = Long + nSpan) {
    {
      //if (Long > right) continue;
      x++;

      // - supplied - GetFileName(szFileName, Lat, Long, nSpan);

      int ileft  = (int)((x  - ufract(left / rSpan)) * pixperdeg * rSpan);
      int itop   = (int)((y  - ufract(top / rSpan)) * pixperdeg * rSpan);
      int iwidth  = (int)(pixperdeg * rSpan + 1);
      int iheight = (int)(pixperdeg * rSpan + 1);

      LtoC(cLon - rSpan/2 /*113.6437*/, cLat + rSpan/2  /*-29.6237*/, ileft, itop);
 
      // Check and see if the image is cached
      sci = ImageCache(szFileName, iwidth, iheight, &chit, &cmiss, &creq);
      
      pDC->drawImage(ileft, itop, sci);
    }
  }
 
  // display the cache performance info
  //printf("img cache - requests: %d hit: %d miss: %d effect: %d%%\n", creq, chit, cmiss, 100*chit/creq);
}


//-----------------------------------------------
// Mouse Button handlers
//

//-----------------------------------------------------------------------------
// Capture the left mouse button down event, if we are in 'route adding' mode
// prime for append the nearest wpt to the route list when button goes up.
//
void CMapWnd::mouseLeftButtonDown(POINT point)
{
  double rLat, rLon;
  int iWpt = 0; // hack - java ??
  double rNav = 10000;
  double rApt = 10000;
  double rWpt = 10000;

  _bLbuttonDown = true;
  if (bAddRteActive) {
    CtoL(point.x, point.y, rLon, rLat);
    _iWpt = findNearestWaypoint(rLat, rLon);
    return;
  }
  CGenMapWnd::mouseLeftButtonDown(point);
}

void CMapWnd::appendRouteWpt(int iWpt)
{
  if (iInsertAt == 0) {
    // Save RouteWpt and RouteLst for the undo buffer
    //_RouteWpt = RouteWpt;
    //memcpy(_RouteLst, RouteLst, sizeof(_RouteLst));
  pushUndoBuffer();

    // Make sure we do not add multiples
    if (RouteWpt == 0) {
      RouteLst[RouteWpt] = iWpt;
      RouteWpt++;
    }
    else {
      if (RouteLst[RouteWpt-1] != iWpt) {
        RouteLst[RouteWpt] = iWpt;
        RouteWpt++;
      }
    }
    bRouteSaved = false;
  }
}

void CMapWnd::insertRouteWpt(int iWpt, int iAt)
{
  int i;

  // Save RouteWpt and RouteLst for the undo buffer
  //_RouteWpt = RouteWpt;
  //memcpy(_RouteLst, RouteLst, sizeof(_RouteLst));
  pushUndoBuffer();

  // Shift the last lot up one to make space
  for (i = RouteWpt; i > iAt; i--) {
    RouteLst[i] = RouteLst[i-1];
  }
  RouteLst[iAt] = iWpt;
  RouteWpt++;
  bRouteSaved = false;
}


void CMapWnd::removeRouteWpt(int iAt)
{
  int i;

  pushUndoBuffer();
  //
  // We 'delete' an entry by overwriting
  // that particular entry by shifting all
  // subsequent entries down one place
  //
  for (i = iAt; i < RouteWpt; i++)
    RouteLst[i] = RouteLst[i+1];

  // There is now one less waypoint
  RouteWpt--;
}



//-----------------------------------------------------------------------------
// Capture the left mouse button up event
//
void CMapWnd::mouseLeftButtonUp(POINT point)
{
  double rLat, rLon;
  int i;
  //  int iNav;
  int iWpt = 0; // hack java ??
  double r;
  double rNav = 10000;
  double rApt = 10000;
  double rWpt = 10000;

  _bLbuttonDown = false;
  mousepoint.x = -1;  // to stop the overdrawing in drag


  // Check to see if we dragged - if so insert the new wpt
  if (_bDragged) {
    CtoL(point.x, point.y, rLon, rLat);
    //rLong = m_rlon; rLat = m_rlat;
    // Find the nearest Wpt
    i = 0;
    wpt.MoveFirst();
    while (wpt.IsEOF() == false) {
      r = sqrt(sqr(rLat - wpt.m_rLat) + sqr(rLon - wpt.m_rLon));
      if (r < rWpt) {
        rWpt = r;
        iWpt = i;
      }
      i++;
      wpt.MoveNext();
    }
    if (rWpt > 0.1) {
      // Missed the drop - abandon all hope
      // and force a complete redraw to
      // get rid of 'contruction' lines
      iInsertAt = 0;
      bIsDirty = true;
    }

    if (iInsertAt != 0) {
      insertRouteWpt(iWpt, iInsertAt);
    }
    iInsertAt = 0;  // reset the iInsertAt action
  }
  else {
    // if it was not a drag, we may be clicking
    // in a route
    if (bAddRteActive) {
      iWpt = _iWpt;  // use the nearest waypoint at time of mouse down
 
      // Add this Wpt to our route list
      appendRouteWpt(iWpt);
    }
  }

  if (RouteWpt > NR_LEGS) {
    RouteWpt--;
    QMessageBox::information(NULL, SZ_PRGNAME,
                             QString("Maximum number of route legs - %1").arg(NR_LEGS-1));
  }

  _bDragged = false;
  CGenMapWnd::mouseLeftButtonUp(point);
}


//-----------------------------------------------------------------------------
// Capture the right mouse button down event
//
void CMapWnd::mouseRightButtonDown(POINT point)
{
  if (bZoomActive) {
    CGenMapWnd::mouseRightButtonDown(point);
  }
  else {
    double rLat, rLon;
    int iWpt = 0;

    CtoL(point.x, point.y, rLon, rLat);
    iWpt = findNearestWaypoint(rLat, rLon);

    wpt.MoveFirst();
    wpt.Move(iWpt);
    inf_id = wpt.m_ID;
    inf_name = QString(wpt.m_INFO);
    inf_icao = QString(wpt.m_ICAO);
    //fireChangedEvent(icao);

    CGenMapWnd::mouseRightButtonDown(point);
  }
}


//-----------------------------------------------
// Mouse event handlers
//
void CMapWnd::mouseDragged(GraphicPainter *gc, POINT point)
{
  _bDragged = true;

  CGenMapWnd::mouseDragged(gc, point);

  if (!bAddRteActive)
    return;

  double rLat, rLong;
  double lat1, lon1, lat2, lon2;

  if (!CtoL(point.x, point.y, rLong, rLat))
    return;

  gc->SetROP2(Qt::NotXorROP);
  gc->setPen(QPen(Qt::red, WIDE_PEN));

  if (_bLbuttonDown) {
    if (iInsertAt != 0) {
      if (mousepoint.x != -1) {
        // There *is* some draglines to erase

        // 'Erase' the current segment by
        // overdrawing the segment with the background color
        gc->SetROP2(Qt::CopyROP);
        
        //~~
        //gc->setPen(QPen(gc->backgroundColor(), 6)); //g2d.setStroke(wideStroke);
        gc->setPen(QPen(QColor(255, 255, 196), 6)); //g2d.setStroke(wideStroke);

        wpt.MoveFirst();
        wpt.Move(RouteLst[iInsertAt-1]);
        lat1 = wpt.m_rLat;
        lon1 = wpt.m_rLon;

        wpt.MoveFirst();
        wpt.Move(RouteLst[iInsertAt]);
        lat2 = wpt.m_rLat;
        lon2 = wpt.m_rLon;
        drawLine(gc, lat1, lon1, lat2, lon2);

        // Most of the segment is now 'erased'

        // User XOR to rubberband the draglines
        gc->SetROP2(Qt::NotXorROP);
        gc->setPen(QPen(Qt::red, WIDE_PEN)); //g2d.setStroke(wideStroke);

        wpt.MoveFirst();
        wpt.Move(RouteLst[iInsertAt-1]);
 
        lat1 = wpt.m_rLat;
        lon1 = wpt.m_rLon;
        CtoL(mousepoint.x, mousepoint.y, lon2, lat2);
        drawLine(gc, lat1, lon1, lat2, lon2);

        wpt.MoveFirst();
        wpt.Move(RouteLst[iInsertAt]);
        lat1 = wpt.m_rLat;
        lon1 = wpt.m_rLon;
        drawLine(gc, lat1, lon1, lat2, lon2);
      }

      wpt.MoveFirst();
      wpt.Move(RouteLst[iInsertAt-1]);
      lat1 = wpt.m_rLat;
      lon1 = wpt.m_rLon;
      CtoL(point.x, point.y, lon2, lat2);
      drawLine(gc, lat1, lon1, lat2, lon2);

      wpt.MoveFirst();
      wpt.Move(RouteLst[iInsertAt]);
      lat1 = wpt.m_rLat;
      lon1 = wpt.m_rLon;
      drawLine(gc, lat1, lon1, lat2, lon2);

      mousepoint = point;
    }
    else
      mousepoint.x = -1;
  }
}


//---------------------------------------------------------------------------
// higlightNearestWaypoint
//
// The previous highlight (snap)
int _iWpt = -255;

void CMapWnd::higlightNearestWaypoint(GraphicPainter *gc, double rLat, double rLong)
{
  int x, y;
  int iWpt;

  //g2d.setStroke(basicStroke);
  gc->setPen(Qt::black);
  // Highlight the target snap to
  //Rectangle2D r2d = new Rectangle2D.Double();
  int isize = (int)(mag * 0.75);

  int FontHeight = (int) (mag * FONT_SCALER); //1.50
  if (FontHeight <= MINFONTSIZE) FontHeight = 1;
  if (FontHeight > MAXFONTSIZE ) FontHeight = MAXFONTSIZE;

  QFont font("Arial", FontHeight);
  gc->setFont(font);

  // Find and highlight the current nearest one
  iWpt = findNearestWaypoint(rLat, rLong);

  // Erase the previous highlight
  if (iWpt != _iWpt) {
    if (_iWpt >= 0) {
      wpt.Move(_iWpt);
      

      LtoC(wpt.m_rLon, wpt.m_rLat, x, y);
      choosePen(gc, wpt.m_TYPE);
      drawWpt(gc);
    }
  }

  // Highlight the nearest one
  wpt.Move(iWpt);
  gc->setPen(Qt::red); //g2d.setColor(Color.red);
  LtoC(wpt.m_rLon, wpt.m_rLat, x, y);
  drawWpt(gc);
  _iWpt = iWpt;
}



//-----------------------------------------------------------------------------
// Get the closest segment to the point
//
int CMapWnd::findNearestRouteSegment(double rLat, double rLon)
{
  int i;
  double r, dx, dy;

  double lat1, lon1, lat2, lon2;

  for (i = 0; i < RouteWpt-1; i++) {
    if (RouteLst[i] != 0) {
      wpt.MoveFirst();
      wpt.Move(RouteLst[i]);
      lat1 = wpt.m_rLat;
      lon1 = wpt.m_rLon;

      wpt.MoveFirst();
      wpt.Move(RouteLst[i+1]);
      lat2 = wpt.m_rLat;
      lon2 = wpt.m_rLon;

      double tlat;
      double tlon = rLon;
      GreatCircle::getIntermediatePoint(&tlat, &tlon, lat1, lon1, lat2, lon2);

      if (
        (((rLon > lon1) && (rLon < lon2)) || ((rLon < lon1) && (rLon > lon2)))
        ) {

        dy = rLat - tlat;
        dx = rLon - tlon;
        r = fabs((dx*dy) / (sqrt(sqr(dx) + sqr(dy))));
        r = fabs(dy);

        if (r < 0.25) { /*1.0*/
          iSeg = i+1;
          break;
        }
        else
          iSeg = 0;
 
      }
    }
  }
  
  return iSeg; // change to local var later and m_iSeg
}

//-----------------------------------------------------------------------------
// Highlight the current nearest segment (iSeg) and un-highlight the previous 
// one (lastSeg)
//
int CMapWnd::higlightNearestSegment(GraphicPainter *gc)
{
  double lat1, lon1, lat2, lon2;
  if (iSeg != 0) {
    //
    // there is a valid close segment
    // so we highlight it
    //
    gc->setPen(QPen(Qt::red, WIDE_PEN));
    wpt.MoveFirst();
    wpt.Move(RouteLst[iSeg-1]);
    lat1 = wpt.m_rLat;
    lon1 = wpt.m_rLon;

    wpt.MoveFirst();
    wpt.Move(RouteLst[iSeg]);
    lat2 = wpt.m_rLat;
    lon2 = wpt.m_rLon;

    drawLine(gc, lat1, lon1, lat2, lon2);
  }

  //
  // If the active segment has changed
  // clear the previous segment.
  //
  if (iSeg != lastSeg) {
    //
    // undo any previous highlight
    //
    if (lastSeg != 0) {
      if (bShadeActive)
        gc->setPen(QPen(Qt::white, WIDE_PEN));
      else
        gc->setPen(QPen(Qt::black, WIDE_PEN));

      wpt.MoveFirst();
      wpt.Move(RouteLst[lastSeg-1]);
      lat1 = wpt.m_rLat;
      lon1 = wpt.m_rLon;

      wpt.MoveFirst();
      wpt.Move(RouteLst[lastSeg]);
      lat2 = wpt.m_rLat;
      lon2 = wpt.m_rLon;

      drawLine(gc, lat1, lon1, lat2, lon2);
    }
  }
  return -1; // temp
}


//---------------------------------------------------------------------------
// mouseMoved
//
void CMapWnd::mouseMoved(GraphicPainter *gc, POINT point)
{
  double rLat, rLon;

  if (!CtoL(point.x, point.y, rLon, rLat))
    return;

  findNearestRouteSegment(rLat, rLon);  // will set iSeg

  // Produce some debug output
  if (lastSeg != iSeg) {
    printf("> iSeg      = %d\n", iSeg);
    printf("> iInsertAt = %d\n", iInsertAt);
  }

  if (bAddRteActive) {
    higlightNearestWaypoint(gc, rLat, rLon);
    higlightNearestSegment(gc);
  } 

  if (!bZoomActive) {
    higlightNearestWaypoint(gc, rLat, rLon);
  }

  lastSeg = iSeg;
  CGenMapWnd::mouseMoved(point);
}


void CMapWnd::autoSetPen(GraphicPainter *gc)
{
  //
  // Autoset a pen color
  //
  if (gc->pen() == QPen(Qt::darkGreen))
    gc->setPen(Qt::darkMagenta);
 
  else if (gc->pen() == QPen(Qt::darkMagenta))
    gc->setPen(Qt::darkYellow);
 
  else if (gc->pen() == QPen(Qt::darkYellow))
    gc->setPen(Qt::darkBlue);
 
  else if (gc->pen() == QPen(Qt::darkBlue))
    gc->setPen(Qt::darkRed);
 
  else if (gc->pen() == QPen(Qt::darkRed))
    gc->setPen(Qt::darkCyan);
 
  else if (gc->pen() == QPen(Qt::darkCyan))
    gc->setPen(Qt::darkGreen);
}




void CMapWnd::DrawNewNames(GraphicPainter *gc)
{
}




