// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include "msacc9.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// DoCmd properties

/////////////////////////////////////////////////////////////////////////////
// DoCmd operations

void DoCmd::AddMenu(const VARIANT& MenuName, const VARIANT& MenuMacroName, const VARIANT& StatusBarText)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x3e9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &MenuName, &MenuMacroName, &StatusBarText);
}

void DoCmd::ApplyFilter(const VARIANT& FilterName, const VARIANT& WhereCondition)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x3ea, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &FilterName, &WhereCondition);
}

void DoCmd::Beep()
{
  InvokeHelper(0x3eb, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void DoCmd::CancelEvent()
{
  InvokeHelper(0x3ec, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void DoCmd::Close(long ObjectType, const VARIANT& ObjectName, long Save)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_I4;
  InvokeHelper(0x3ed, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ObjectType, &ObjectName, Save);
}

void DoCmd::CopyObject(const VARIANT& DestinationDatabase, const VARIANT& NewName, long SourceObjectType, const VARIANT& SourceObjectName)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT VTS_I4 VTS_VARIANT;
  InvokeHelper(0x3ee, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &DestinationDatabase, &NewName, SourceObjectType, &SourceObjectName);
}

void DoCmd::DoMenuItem(const VARIANT& MenuBar, const VARIANT& MenuName, const VARIANT& Command, const VARIANT& Subcommand, const VARIANT& Version)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x3ef, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &MenuBar, &MenuName, &Command, &Subcommand, &Version);
}

void DoCmd::Echo(const VARIANT& EchoOn, const VARIANT& StatusBarText)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x3f0, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &EchoOn, &StatusBarText);
}

void DoCmd::FindNext()
{
  InvokeHelper(0x3f1, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void DoCmd::FindRecord(const VARIANT& FindWhat, long Match, const VARIANT& MatchCase, long Search, const VARIANT& SearchAsFormatted, long OnlyCurrentField, const VARIANT& FindFirst)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4 VTS_VARIANT VTS_I4 VTS_VARIANT VTS_I4 VTS_VARIANT;
  InvokeHelper(0x3f2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &FindWhat, Match, &MatchCase, Search, &SearchAsFormatted, OnlyCurrentField, &FindFirst);
}

void DoCmd::GoToControl(const VARIANT& ControlName)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x3f3, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ControlName);
}

void DoCmd::GoToPage(const VARIANT& PageNumber, const VARIANT& Right, const VARIANT& Down)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x3f4, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &PageNumber, &Right, &Down);
}

void DoCmd::GoToRecord(long ObjectType, const VARIANT& ObjectName, long Record, const VARIANT& Offset)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_I4 VTS_VARIANT;
  InvokeHelper(0x3f5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ObjectType, &ObjectName, Record, &Offset);
}

void DoCmd::Hourglass(const VARIANT& HourglassOn)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x3f6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &HourglassOn);
}

void DoCmd::Maximize()
{
  InvokeHelper(0x3f7, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void DoCmd::Minimize()
{
  InvokeHelper(0x3f8, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void DoCmd::MoveSize(const VARIANT& Right, const VARIANT& Down, const VARIANT& Width, const VARIANT& Height)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x3f9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &Right, &Down, &Width, &Height);
}

void DoCmd::OpenForm(const VARIANT& FormName, long View, const VARIANT& FilterName, const VARIANT& WhereCondition, long DataMode, long WindowMode, const VARIANT& OpenArgs)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4 VTS_VARIANT VTS_VARIANT VTS_I4 VTS_I4 VTS_VARIANT;
  InvokeHelper(0x3fb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &FormName, View, &FilterName, &WhereCondition, DataMode, WindowMode, &OpenArgs);
}

void DoCmd::OpenQuery(const VARIANT& QueryName, long View, long DataMode)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4 VTS_I4;
  InvokeHelper(0x3fc, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &QueryName, View, DataMode);
}

void DoCmd::OpenTable(const VARIANT& TableName, long View, long DataMode)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4 VTS_I4;
  InvokeHelper(0x3fd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &TableName, View, DataMode);
}

void DoCmd::PrintOut(long PrintRange, const VARIANT& PageFrom, const VARIANT& PageTo, long PrintQuality, const VARIANT& Copies, const VARIANT& CollateCopies)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_VARIANT VTS_I4 VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x3fe, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     PrintRange, &PageFrom, &PageTo, PrintQuality, &Copies, &CollateCopies);
}

void DoCmd::Quit(long Options)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x3ff, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Options);
}

void DoCmd::Requery(const VARIANT& ControlName)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x400, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ControlName);
}

void DoCmd::RepaintObject(long ObjectType, const VARIANT& ObjectName)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT;
  InvokeHelper(0x401, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ObjectType, &ObjectName);
}

void DoCmd::Rename(const VARIANT& NewName, long ObjectType, const VARIANT& OldName)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4 VTS_VARIANT;
  InvokeHelper(0x402, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &NewName, ObjectType, &OldName);
}

void DoCmd::Restore()
{
  InvokeHelper(0x403, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void DoCmd::RunMacro(const VARIANT& MacroName, const VARIANT& RepeatCount, const VARIANT& RepeatExpression)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x406, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &MacroName, &RepeatCount, &RepeatExpression);
}

void DoCmd::RunSQL(const VARIANT& SQLStatement, const VARIANT& UseTransaction)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x407, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &SQLStatement, &UseTransaction);
}

void DoCmd::SelectObject(long ObjectType, const VARIANT& ObjectName, const VARIANT& InDatabaseWindow)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x408, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ObjectType, &ObjectName, &InDatabaseWindow);
}

void DoCmd::SetWarnings(const VARIANT& WarningsOn)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x40b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &WarningsOn);
}

void DoCmd::ShowAllRecords()
{
  InvokeHelper(0x40c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void DoCmd::OpenReport(const VARIANT& ReportName, long View, const VARIANT& FilterName, const VARIANT& WhereCondition)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4 VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x40f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ReportName, View, &FilterName, &WhereCondition);
}

void DoCmd::TransferDatabase(long TransferType, const VARIANT& DatabaseType, const VARIANT& DatabaseName, long ObjectType, const VARIANT& Source, const VARIANT& Destination, const VARIANT& StructureOnly, const VARIANT& StoreLogin)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_VARIANT VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x410, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     TransferType, &DatabaseType, &DatabaseName, ObjectType, &Source, &Destination, &StructureOnly, &StoreLogin);
}

void DoCmd::TransferSpreadsheet(long TransferType, long SpreadsheetType, const VARIANT& TableName, const VARIANT& FileName, const VARIANT& HasFieldNames, const VARIANT& Range, const VARIANT& UseOA)
{
  static BYTE parms[] =
    VTS_I4 VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x411, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     TransferType, SpreadsheetType, &TableName, &FileName, &HasFieldNames, &Range, &UseOA);
}

void DoCmd::TransferText(long TransferType, const VARIANT& SpecificationName, const VARIANT& TableName, const VARIANT& FileName, const VARIANT& HasFieldNames, const VARIANT& HTMLTableName, const VARIANT& CodePage)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x412, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     TransferType, &SpecificationName, &TableName, &FileName, &HasFieldNames, &HTMLTableName, &CodePage);
}

void DoCmd::OutputTo(long ObjectType, const VARIANT& ObjectName, const VARIANT& OutputFormat, const VARIANT& OutputFile, const VARIANT& AutoStart, const VARIANT& TemplateFile)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x53d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ObjectType, &ObjectName, &OutputFormat, &OutputFile, &AutoStart, &TemplateFile);
}

void DoCmd::DeleteObject(long ObjectType, const VARIANT& ObjectName)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT;
  InvokeHelper(0x55c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ObjectType, &ObjectName);
}

void DoCmd::OpenModule(const VARIANT& ModuleName, const VARIANT& ProcedureName)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x55e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ModuleName, &ProcedureName);
}

void DoCmd::SendObject(long ObjectType, const VARIANT& ObjectName, const VARIANT& OutputFormat, const VARIANT& To, const VARIANT& Cc, const VARIANT& Bcc, const VARIANT& Subject, const VARIANT& MessageText, const VARIANT& EditMessage, 
    const VARIANT& TemplateFile)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x561, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ObjectType, &ObjectName, &OutputFormat, &To, &Cc, &Bcc, &Subject, &MessageText, &EditMessage, &TemplateFile);
}

void DoCmd::ShowToolbar(const VARIANT& ToolbarName, long Show)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4;
  InvokeHelper(0x572, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ToolbarName, Show);
}

void DoCmd::Save(long ObjectType, const VARIANT& ObjectName)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT;
  InvokeHelper(0x5ad, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ObjectType, &ObjectName);
}

void DoCmd::SetMenuItem(const VARIANT& MenuIndex, const VARIANT& CommandIndex, const VARIANT& SubcommandIndex, const VARIANT& Flag)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x5ae, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &MenuIndex, &CommandIndex, &SubcommandIndex, &Flag);
}

void DoCmd::RunCommand(long Command)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x642, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Command);
}

void DoCmd::OpenDataAccessPage(const VARIANT& DataAccessPageName, long View)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4;
  InvokeHelper(0x6eb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &DataAccessPageName, View);
}

void DoCmd::OpenView(const VARIANT& ViewName, long View, long DataMode)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4 VTS_I4;
  InvokeHelper(0x701, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ViewName, View, DataMode);
}

void DoCmd::OpenDiagram(const VARIANT& DiagramName)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x702, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &DiagramName);
}

void DoCmd::OpenStoredProcedure(const VARIANT& ProcedureName, long View, long DataMode)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_I4 VTS_I4;
  InvokeHelper(0x703, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ProcedureName, View, DataMode);
}


/////////////////////////////////////////////////////////////////////////////
// _RecordsetEvents properties

/////////////////////////////////////////////////////////////////////////////
// _RecordsetEvents operations

void _RecordsetEvents::WillChangeField(long cFields, const VARIANT& Fields, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0x9, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     cFields, &Fields, adStatus, pRecordset);
}

void _RecordsetEvents::FieldChangeComplete(long cFields, const VARIANT& Fields, LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_UNKNOWN VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0xa, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     cFields, &Fields, pError, adStatus, pRecordset);
}

void _RecordsetEvents::WillChangeRecord(long adReason, long cRecords, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_I4 VTS_I4 VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0xb, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     adReason, cRecords, adStatus, pRecordset);
}

void _RecordsetEvents::RecordChangeComplete(long adReason, long cRecords, LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_I4 VTS_I4 VTS_UNKNOWN VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0xc, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     adReason, cRecords, pError, adStatus, pRecordset);
}

void _RecordsetEvents::WillChangeRecordset(long adReason, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_I4 VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     adReason, adStatus, pRecordset);
}

void _RecordsetEvents::RecordsetChangeComplete(long adReason, LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_I4 VTS_UNKNOWN VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0xe, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     adReason, pError, adStatus, pRecordset);
}

void _RecordsetEvents::WillMove(long adReason, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_I4 VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0xf, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     adReason, adStatus, pRecordset);
}

void _RecordsetEvents::MoveComplete(long adReason, LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_I4 VTS_UNKNOWN VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     adReason, pError, adStatus, pRecordset);
}

void _RecordsetEvents::EndOfRecordset(short* fMoreData, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_PI2 VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0x11, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     fMoreData, adStatus, pRecordset);
}

void _RecordsetEvents::FetchProgress(long Progress, long MaxProgress, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_I4 VTS_I4 VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0x12, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Progress, MaxProgress, adStatus, pRecordset);
}

void _RecordsetEvents::FetchComplete(LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset)
{
  static BYTE parms[] =
    VTS_UNKNOWN VTS_PI4 VTS_UNKNOWN;
  InvokeHelper(0x13, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     pError, adStatus, pRecordset);
}


/////////////////////////////////////////////////////////////////////////////
// _AccessProperty properties

/////////////////////////////////////////////////////////////////////////////
// _AccessProperty operations

LPDISPATCH _AccessProperty::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _AccessProperty::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _AccessProperty::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _AccessProperty::GetName()
{
  CString result;
  InvokeHelper(0x60030002, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _AccessProperty::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x60030002, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _AccessProperty::GetType()
{
  short result;
  InvokeHelper(0x60030004, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _AccessProperty::SetType(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x60030004, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _AccessProperty::GetInherited()
{
  BOOL result;
  InvokeHelper(0x60030006, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

LPDISPATCH _AccessProperty::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _AccessProperty::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

short _AccessProperty::GetCategory()
{
  short result;
  InvokeHelper(0x830, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// Properties properties

/////////////////////////////////////////////////////////////////////////////
// Properties operations

LPDISPATCH Properties::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Properties::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Properties::GetItem(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

long Properties::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _FormatCondition properties

/////////////////////////////////////////////////////////////////////////////
// _FormatCondition operations

long _FormatCondition::GetForeColor()
{
  long result;
  InvokeHelper(DISPID_FORECOLOR, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _FormatCondition::SetForeColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(DISPID_FORECOLOR, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _FormatCondition::GetBackColor()
{
  long result;
  InvokeHelper(DISPID_BACKCOLOR, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _FormatCondition::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(DISPID_BACKCOLOR, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _FormatCondition::GetFontBold()
{
  BOOL result;
  InvokeHelper(0x84f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _FormatCondition::SetFontBold(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x84f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _FormatCondition::GetFontItalic()
{
  BOOL result;
  InvokeHelper(0x850, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _FormatCondition::SetFontItalic(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x850, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _FormatCondition::GetFontUnderline()
{
  BOOL result;
  InvokeHelper(0x853, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _FormatCondition::SetFontUnderline(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x853, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _FormatCondition::GetEnabled()
{
  BOOL result;
  InvokeHelper(DISPID_ENABLED, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _FormatCondition::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(DISPID_ENABLED, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

long _FormatCondition::GetType()
{
  long result;
  InvokeHelper(0x82f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

long _FormatCondition::GetOperator()
{
  long result;
  InvokeHelper(0x897, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

CString _FormatCondition::GetExpression1()
{
  CString result;
  InvokeHelper(0x898, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

CString _FormatCondition::GetExpression2()
{
  CString result;
  InvokeHelper(0x899, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _FormatCondition::Modify(long Type, long Operator, const VARIANT& Expression1, const VARIANT& Expression2)
{
  static BYTE parms[] =
    VTS_I4 VTS_I4 VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x89a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Type, Operator, &Expression1, &Expression2);
}

void _FormatCondition::Delete()
{
  InvokeHelper(0x80f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// FormatConditions properties

/////////////////////////////////////////////////////////////////////////////
// FormatConditions operations

LPDISPATCH FormatConditions::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH FormatConditions::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH FormatConditions::GetItem(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

long FormatConditions::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

LPDISPATCH FormatConditions::Add(long Type, long Operator, const VARIANT& Expression1, const VARIANT& Expression2)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_I4 VTS_I4 VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x88f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    Type, Operator, &Expression1, &Expression2);
  return result;
}

void FormatConditions::Delete()
{
  InvokeHelper(0x80f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// _ItemsSelected properties

/////////////////////////////////////////////////////////////////////////////
// _ItemsSelected operations

long _ItemsSelected::GetItem(const VARIANT& Index)
{
  long result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, parms,
    &Index);
  return result;
}

long _ItemsSelected::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// Children properties

/////////////////////////////////////////////////////////////////////////////
// Children operations

LPDISPATCH Children::GetItem(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

long Children::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _AccessField properties

/////////////////////////////////////////////////////////////////////////////
// _AccessField operations

VARIANT _AccessField::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _AccessField::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}


/////////////////////////////////////////////////////////////////////////////
// _Hyperlink properties

/////////////////////////////////////////////////////////////////////////////
// _Hyperlink operations

CString _Hyperlink::GetSubAddress()
{
  CString result;
  InvokeHelper(0x886, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Hyperlink::SetSubAddress(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x886, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Hyperlink::GetAddress()
{
  CString result;
  InvokeHelper(0x887, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Hyperlink::SetAddress(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x887, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

void _Hyperlink::AddToFavorites()
{
  InvokeHelper(0x888, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Hyperlink::Follow(BOOL NewWindow, BOOL AddHistory, const VARIANT& ExtraInfo, long Method, LPCTSTR HeaderInfo)
{
  static BYTE parms[] =
    VTS_BOOL VTS_BOOL VTS_VARIANT VTS_I4 VTS_BSTR;
  InvokeHelper(0x889, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     NewWindow, AddHistory, &ExtraInfo, Method, HeaderInfo);
}

CString _Hyperlink::GetEmailSubject()
{
  CString result;
  InvokeHelper(0x8a1, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Hyperlink::SetEmailSubject(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x8a1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Hyperlink::GetScreenTip()
{
  CString result;
  InvokeHelper(0x8a3, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Hyperlink::SetScreenTip(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x8a3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Hyperlink::GetTextToDisplay()
{
  CString result;
  InvokeHelper(0x8b7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Hyperlink::SetTextToDisplay(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x8b7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

void _Hyperlink::CreateNewDocument(LPCTSTR FileName, BOOL EditNow, BOOL Overwrite)
{
  static BYTE parms[] =
    VTS_BSTR VTS_BOOL VTS_BOOL;
  InvokeHelper(0x8b8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     FileName, EditNow, Overwrite);
}


/////////////////////////////////////////////////////////////////////////////
// Pages properties

/////////////////////////////////////////////////////////////////////////////
// Pages operations

LPDISPATCH Pages::GetItem(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

long Pages::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

LPDISPATCH Pages::Add(const VARIANT& Before)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x88f, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    &Before);
  return result;
}

void Pages::Remove(const VARIANT& Item)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x890, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &Item);
}


/////////////////////////////////////////////////////////////////////////////
// _Control properties

/////////////////////////////////////////////////////////////////////////////
// _Control operations

LPDISPATCH _Control::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Control::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Control::Undo()
{
  InvokeHelper(0x860, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Control::Dropdown()
{
  InvokeHelper(0x85f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT _Control::GetColumn(long Index, const VARIANT& Row)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT;
  InvokeHelper(0x835, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms,
    Index, &Row);
  return result;
}

long _Control::GetSelected(long lRow)
{
  long result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x841, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, parms,
    lRow);
  return result;
}

void _Control::SetSelected(long lRow, long nNewValue)
{
  static BYTE parms[] =
    VTS_I4 VTS_I4;
  InvokeHelper(0x841, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lRow, nNewValue);
}

VARIANT _Control::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Control::GetForm()
{
  LPDISPATCH result;
  InvokeHelper(0x829, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Control::GetReport()
{
  LPDISPATCH result;
  InvokeHelper(0x831, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _Control::GetItemData(long Index)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x837, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms,
    Index);
  return result;
}

LPDISPATCH _Control::GetObject()
{
  LPDISPATCH result;
  InvokeHelper(0x838, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Control::GetObjectVerbs(long Index)
{
  CString result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x839, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
    Index);
  return result;
}

LPDISPATCH _Control::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Control::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Control::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Control::GetItemsSelected()
{
  LPDISPATCH result;
  InvokeHelper(0x859, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Control::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Control::GetPages()
{
  LPDISPATCH result;
  InvokeHelper(0x884, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Control::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Control::GetHyperlink()
{
  LPDISPATCH result;
  InvokeHelper(0x885, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// Controls properties

/////////////////////////////////////////////////////////////////////////////
// Controls operations

LPDISPATCH Controls::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Controls::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Controls::GetItem(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

long Controls::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _Label properties

/////////////////////////////////////////////////////////////////////////////
// _Label operations

LPDISPATCH _Label::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Label::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Label::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Label::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Label::GetHyperlink()
{
  LPDISPATCH result;
  InvokeHelper(0x885, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Label::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetCaption()
{
  CString result;
  InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetCaption(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetHyperlinkAddress()
{
  CString result;
  InvokeHelper(0x157, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetHyperlinkAddress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x157, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetHyperlinkSubAddress()
{
  CString result;
  InvokeHelper(0x15b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetHyperlinkSubAddress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x15b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Label::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Label::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Label::GetVertical()
{
  BOOL result;
  InvokeHelper(0x163, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Label::SetVertical(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x163, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Label::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Label::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Label::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Label::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Label::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Label::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Label::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Label::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Label::GetForeColor()
{
  long result;
  InvokeHelper(0xcc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Label::SetForeColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xcc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Label::GetFontName()
{
  CString result;
  InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetFontName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x22, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Label::GetFontSize()
{
  short result;
  InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetFontSize(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Label::GetFontWeight()
{
  short result;
  InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetFontWeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Label::GetFontItalic()
{
  BOOL result;
  InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Label::SetFontItalic(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Label::GetFontUnderline()
{
  BOOL result;
  InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Label::SetFontUnderline(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Label::GetFontBold()
{
  short result;
  InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetFontBold(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Label::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Label::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Label::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Label::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Label::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Label::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Label::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Label::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Label::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Label::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Label::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Label::GetLeftMargin()
{
  short result;
  InvokeHelper(0x180, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetLeftMargin(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x180, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Label::GetTopMargin()
{
  short result;
  InvokeHelper(0x181, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetTopMargin(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x181, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Label::GetLineSpacing()
{
  short result;
  InvokeHelper(0x182, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetLineSpacing(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x182, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Label::GetRightMargin()
{
  short result;
  InvokeHelper(0x184, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetRightMargin(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x184, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Label::GetBottomMargin()
{
  short result;
  InvokeHelper(0x185, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Label::SetBottomMargin(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x185, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _Rectangle properties

/////////////////////////////////////////////////////////////////////////////
// _Rectangle operations

LPDISPATCH _Rectangle::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Rectangle::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Rectangle::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Rectangle::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString _Rectangle::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Rectangle::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Rectangle::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Rectangle::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Rectangle::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Rectangle::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Rectangle::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Rectangle::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Rectangle::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Rectangle::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Rectangle::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Rectangle::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Rectangle::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Rectangle::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Rectangle::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Rectangle::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Rectangle::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Rectangle::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Rectangle::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _Line properties

/////////////////////////////////////////////////////////////////////////////
// _Line operations

LPDISPATCH _Line::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Line::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Line::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Line::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString _Line::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Line::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Line::GetLineSlant()
{
  BOOL result;
  InvokeHelper(0x37, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Line::SetLineSlant(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x37, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Line::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Line::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Line::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Line::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Line::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Line::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Line::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Line::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Line::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Line::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Line::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Line::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Line::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Line::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Line::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Line::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Line::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Line::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Line::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Line::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Line::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Line::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _Image properties

/////////////////////////////////////////////////////////////////////////////
// _Image operations

LPDISPATCH _Image::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Image::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _Image::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Image::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Image::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Image::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Image::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Image::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Image::GetHyperlink()
{
  LPDISPATCH result;
  InvokeHelper(0x885, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Image::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Image::GetPicture()
{
  CString result;
  InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetPicture(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _Image::GetPictureData()
{
  VARIANT result;
  InvokeHelper(0xbc, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Image::SetPictureData(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

BOOL _Image::GetPictureTiling()
{
  BOOL result;
  InvokeHelper(0x139, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Image::SetPictureTiling(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x139, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Image::GetHyperlinkAddress()
{
  CString result;
  InvokeHelper(0x157, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetHyperlinkAddress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x157, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Image::GetHyperlinkSubAddress()
{
  CString result;
  InvokeHelper(0x15b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetHyperlinkSubAddress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x15b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Image::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Image::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Image::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Image::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Image::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Image::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Image::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Image::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Image::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Image::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Image::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Image::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Image::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Image::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Image::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Image::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Image::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Image::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Image::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

VARIANT _Image::GetObjectPalette()
{
  VARIANT result;
  InvokeHelper(0x63, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Image::SetObjectPalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x63, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

BOOL _Image::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Image::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Image::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Image::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Image::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Image::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Image::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Image::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Image::GetImageHeight()
{
  long result;
  InvokeHelper(0x133, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Image::SetImageHeight(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x133, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Image::GetImageWidth()
{
  long result;
  InvokeHelper(0x134, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Image::SetImageWidth(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x134, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Image::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Image::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Image::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Image::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Image::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _CommandButton properties

/////////////////////////////////////////////////////////////////////////////
// _CommandButton operations

LPDISPATCH _CommandButton::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CommandButton::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _CommandButton::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CommandButton::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _CommandButton::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _CommandButton::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _CommandButton::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _CommandButton::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CommandButton::GetHyperlink()
{
  LPDISPATCH result;
  InvokeHelper(0x885, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _CommandButton::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetCaption()
{
  CString result;
  InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetCaption(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetPicture()
{
  CString result;
  InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetPicture(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _CommandButton::GetPictureData()
{
  VARIANT result;
  InvokeHelper(0xbc, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetPictureData(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

BOOL _CommandButton::GetTransparent()
{
  BOOL result;
  InvokeHelper(0x2e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetTransparent(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x2e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CommandButton::GetDefault()
{
  BOOL result;
  InvokeHelper(0xdc, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetDefault(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xdc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CommandButton::GetCancel()
{
  BOOL result;
  InvokeHelper(0xdd, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetCancel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xdd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CommandButton::GetAutoRepeat()
{
  BOOL result;
  InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetAutoRepeat(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _CommandButton::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnPush()
{
  CString result;
  InvokeHelper(0x9b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnPush(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x9b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetHyperlinkAddress()
{
  CString result;
  InvokeHelper(0x157, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetHyperlinkAddress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x157, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetHyperlinkSubAddress()
{
  CString result;
  InvokeHelper(0x15b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetHyperlinkSubAddress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x15b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _CommandButton::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CommandButton::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CommandButton::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _CommandButton::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CommandButton::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CommandButton::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CommandButton::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CommandButton::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _CommandButton::GetForeColor()
{
  long result;
  InvokeHelper(0xcc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetForeColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xcc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _CommandButton::GetFontName()
{
  CString result;
  InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetFontName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x22, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _CommandButton::GetFontSize()
{
  short result;
  InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetFontSize(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CommandButton::GetFontWeight()
{
  short result;
  InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetFontWeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _CommandButton::GetFontItalic()
{
  BOOL result;
  InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetFontItalic(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CommandButton::GetFontUnderline()
{
  BOOL result;
  InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetFontUnderline(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _CommandButton::GetFontBold()
{
  short result;
  InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetFontBold(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _CommandButton::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _CommandButton::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _CommandButton::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CommandButton::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _CommandButton::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CommandButton::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CommandButton::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _CommandButton::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _CommandButton::GetObjectPalette()
{
  VARIANT result;
  InvokeHelper(0x63, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetObjectPalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x63, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

BOOL _CommandButton::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CommandButton::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _CommandButton::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CommandButton::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CommandButton::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _OptionButton properties

/////////////////////////////////////////////////////////////////////////////
// _OptionButton operations

LPDISPATCH _OptionButton::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _OptionButton::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _OptionButton::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _OptionButton::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _OptionButton::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _OptionButton::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _OptionButton::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _OptionButton::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _OptionButton::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _OptionButton::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _OptionButton::GetOptionValue()
{
  long result;
  InvokeHelper(0x3a, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOptionValue(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x3a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _OptionButton::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetDefaultValue()
{
  CString result;
  InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetDefaultValue(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetValidationRule()
{
  CString result;
  InvokeHelper(0x91, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetValidationRule(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x91, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetValidationText()
{
  CString result;
  InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetValidationText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _OptionButton::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionButton::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionButton::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionButton::GetTripleState()
{
  BOOL result;
  InvokeHelper(0x126, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetTripleState(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x126, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionButton::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _OptionButton::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _OptionButton::GetHideDuplicates()
{
  BOOL result;
  InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetHideDuplicates(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _OptionButton::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionButton::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionButton::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionButton::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _OptionButton::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _OptionButton::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _OptionButton::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionButton::GetColumnWidth()
{
  short result;
  InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetColumnWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionButton::GetColumnOrder()
{
  short result;
  InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetColumnOrder(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _OptionButton::GetColumnHidden()
{
  BOOL result;
  InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetColumnHidden(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionButton::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionButton::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _OptionButton::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionButton::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionButton::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _OptionButton::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _OptionButton::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionButton::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _OptionButton::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionButton::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionButton::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _Checkbox properties

/////////////////////////////////////////////////////////////////////////////
// _Checkbox operations

LPDISPATCH _Checkbox::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Checkbox::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Checkbox::Undo()
{
  InvokeHelper(0x860, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT _Checkbox::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Checkbox::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Checkbox::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Checkbox::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Checkbox::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Checkbox::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _Checkbox::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Checkbox::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Checkbox::GetOptionValue()
{
  long result;
  InvokeHelper(0x3a, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOptionValue(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x3a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Checkbox::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetDefaultValue()
{
  CString result;
  InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetDefaultValue(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetValidationRule()
{
  CString result;
  InvokeHelper(0x91, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetValidationRule(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x91, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetValidationText()
{
  CString result;
  InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetValidationText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Checkbox::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Checkbox::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Checkbox::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Checkbox::GetTripleState()
{
  BOOL result;
  InvokeHelper(0x126, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetTripleState(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x126, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Checkbox::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Checkbox::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Checkbox::GetHideDuplicates()
{
  BOOL result;
  InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetHideDuplicates(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Checkbox::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Checkbox::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Checkbox::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Checkbox::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Checkbox::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Checkbox::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Checkbox::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Checkbox::GetColumnWidth()
{
  short result;
  InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetColumnWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Checkbox::GetColumnOrder()
{
  short result;
  InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetColumnOrder(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Checkbox::GetColumnHidden()
{
  BOOL result;
  InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetColumnHidden(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Checkbox::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Checkbox::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Checkbox::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Checkbox::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Checkbox::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Checkbox::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Checkbox::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Checkbox::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Checkbox::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Checkbox::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Checkbox::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _OptionGroup properties

/////////////////////////////////////////////////////////////////////////////
// _OptionGroup operations

LPDISPATCH _OptionGroup::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _OptionGroup::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _OptionGroup::Undo()
{
  InvokeHelper(0x860, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT _OptionGroup::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _OptionGroup::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _OptionGroup::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _OptionGroup::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _OptionGroup::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _OptionGroup::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _OptionGroup::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetDefaultValue()
{
  CString result;
  InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetDefaultValue(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetValidationRule()
{
  CString result;
  InvokeHelper(0x91, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetValidationRule(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x91, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetValidationText()
{
  CString result;
  InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetValidationText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _OptionGroup::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionGroup::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionGroup::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionGroup::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _OptionGroup::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _OptionGroup::GetHideDuplicates()
{
  BOOL result;
  InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetHideDuplicates(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _OptionGroup::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionGroup::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionGroup::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionGroup::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _OptionGroup::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _OptionGroup::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _OptionGroup::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _OptionGroup::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionGroup::GetColumnWidth()
{
  short result;
  InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetColumnWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionGroup::GetColumnOrder()
{
  short result;
  InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetColumnOrder(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _OptionGroup::GetColumnHidden()
{
  BOOL result;
  InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetColumnHidden(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionGroup::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionGroup::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _OptionGroup::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionGroup::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _OptionGroup::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _OptionGroup::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _OptionGroup::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _OptionGroup::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _OptionGroup::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _OptionGroup::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _OptionGroup::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _BoundObjectFrame properties

/////////////////////////////////////////////////////////////////////////////
// _BoundObjectFrame operations

LPDISPATCH _BoundObjectFrame::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _BoundObjectFrame::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _BoundObjectFrame::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _BoundObjectFrame::GetObject()
{
  LPDISPATCH result;
  InvokeHelper(0x838, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _BoundObjectFrame::GetObjectVerbs(long Index)
{
  CString result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x839, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
    Index);
  return result;
}

LPDISPATCH _BoundObjectFrame::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _BoundObjectFrame::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _BoundObjectFrame::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _BoundObjectFrame::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _BoundObjectFrame::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _BoundObjectFrame::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetClass()
{
  CString result;
  InvokeHelper(0x112, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetClass(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x112, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetSourceDoc()
{
  CString result;
  InvokeHelper(0x85, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetSourceDoc(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x85, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetSourceItem()
{
  CString result;
  InvokeHelper(0x30, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetSourceItem(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x30, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _BoundObjectFrame::GetAutoActivate()
{
  short result;
  InvokeHelper(0x65, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetAutoActivate(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x65, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _BoundObjectFrame::GetDisplayType()
{
  BOOL result;
  InvokeHelper(0x110, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetDisplayType(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x110, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _BoundObjectFrame::GetUpdateOptions()
{
  short result;
  InvokeHelper(0x67, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetUpdateOptions(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x67, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _BoundObjectFrame::GetVerb()
{
  long result;
  InvokeHelper(0x66, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetVerb(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x66, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _BoundObjectFrame::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _BoundObjectFrame::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _BoundObjectFrame::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _BoundObjectFrame::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _BoundObjectFrame::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _BoundObjectFrame::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _BoundObjectFrame::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _BoundObjectFrame::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _BoundObjectFrame::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _BoundObjectFrame::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _BoundObjectFrame::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _BoundObjectFrame::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _BoundObjectFrame::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _BoundObjectFrame::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _BoundObjectFrame::GetColumnWidth()
{
  short result;
  InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetColumnWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _BoundObjectFrame::GetColumnOrder()
{
  short result;
  InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetColumnOrder(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _BoundObjectFrame::GetColumnHidden()
{
  BOOL result;
  InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetColumnHidden(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _BoundObjectFrame::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _BoundObjectFrame::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _BoundObjectFrame::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _BoundObjectFrame::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _BoundObjectFrame::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _BoundObjectFrame::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _BoundObjectFrame::GetObjectPalette()
{
  VARIANT result;
  InvokeHelper(0x63, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetObjectPalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x63, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

long _BoundObjectFrame::GetLpOleObject()
{
  long result;
  InvokeHelper(0xac, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetLpOleObject(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xac, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _BoundObjectFrame::GetObjectVerbsCount()
{
  long result;
  InvokeHelper(0xad, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetObjectVerbsCount(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xad, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _BoundObjectFrame::GetAction()
{
  short result;
  InvokeHelper(0x111, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetAction(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x111, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _BoundObjectFrame::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _BoundObjectFrame::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _BoundObjectFrame::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnUpdated()
{
  CString result;
  InvokeHelper(0x76, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnUpdated(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x76, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _BoundObjectFrame::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _BoundObjectFrame::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _Textbox properties

/////////////////////////////////////////////////////////////////////////////
// _Textbox operations

LPDISPATCH _Textbox::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Textbox::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Textbox::Undo()
{
  InvokeHelper(0x860, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT _Textbox::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Textbox::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Textbox::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Textbox::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Textbox::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Textbox::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Textbox::GetHyperlink()
{
  LPDISPATCH result;
  InvokeHelper(0x885, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Textbox::GetFormatConditions()
{
  LPDISPATCH result;
  InvokeHelper(0x89b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _Textbox::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Textbox::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Textbox::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetFormat()
{
  CString result;
  InvokeHelper(0x26, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetFormat(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x26, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetInputMask()
{
  CString result;
  InvokeHelper(0x48, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetInputMask(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x48, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetDefaultValue()
{
  CString result;
  InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetDefaultValue(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Textbox::GetIMEHold()
{
  BOOL result;
  InvokeHelper(0x165, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetIMEHold(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x165, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Textbox::GetFuriganaControl()
{
  CString result;
  InvokeHelper(0x167, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetFuriganaControl(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x167, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetPostalAddress()
{
  CString result;
  InvokeHelper(0x168, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetPostalAddress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x168, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetValidationRule()
{
  CString result;
  InvokeHelper(0x91, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetValidationRule(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x91, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetValidationText()
{
  CString result;
  InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetValidationText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Textbox::GetEnterKeyBehavior()
{
  BOOL result;
  InvokeHelper(0x4a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetEnterKeyBehavior(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x4a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetAllowAutoCorrect()
{
  BOOL result;
  InvokeHelper(0x154, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetAllowAutoCorrect(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x154, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetVertical()
{
  BOOL result;
  InvokeHelper(0x163, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetVertical(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x163, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetFELineBreak()
{
  BOOL result;
  InvokeHelper(0x166, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetFELineBreak(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x166, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetAutoTab()
{
  BOOL result;
  InvokeHelper(0x49, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetAutoTab(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x49, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Textbox::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Textbox::GetHideDuplicates()
{
  BOOL result;
  InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetHideDuplicates(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetCanGrow()
{
  BOOL result;
  InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetCanGrow(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetCanShrink()
{
  BOOL result;
  InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetCanShrink(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Textbox::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Textbox::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Textbox::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Textbox::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Textbox::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Textbox::GetForeColor()
{
  long result;
  InvokeHelper(0xcc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Textbox::SetForeColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xcc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Textbox::GetFontName()
{
  CString result;
  InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetFontName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x22, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Textbox::GetFontSize()
{
  short result;
  InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetFontSize(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetFontWeight()
{
  short result;
  InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetFontWeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Textbox::GetFontItalic()
{
  BOOL result;
  InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetFontItalic(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetFontUnderline()
{
  BOOL result;
  InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetFontUnderline(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Textbox::GetFontBold()
{
  short result;
  InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetFontBold(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Textbox::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Textbox::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Textbox::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetColumnWidth()
{
  short result;
  InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetColumnWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetColumnOrder()
{
  short result;
  InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetColumnOrder(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Textbox::GetColumnHidden()
{
  BOOL result;
  InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetColumnHidden(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Textbox::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Textbox::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetText()
{
  CString result;
  InvokeHelper(0x10b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetSelText()
{
  CString result;
  InvokeHelper(0x10f, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetSelText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Textbox::GetSelStart()
{
  short result;
  InvokeHelper(0x10e, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetSelStart(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x10e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetSelLength()
{
  short result;
  InvokeHelper(0x10d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetSelLength(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x10d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Textbox::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Textbox::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Textbox::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnChange()
{
  CString result;
  InvokeHelper(0x72, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnChange(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x72, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Textbox::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Textbox::GetIMEMode()
{
  long result;
  InvokeHelper(0x164, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Textbox::SetIMEMode(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x164, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Textbox::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Textbox::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Textbox::GetIMESentenceMode()
{
  long result;
  InvokeHelper(0x17b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Textbox::SetIMESentenceMode(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x17b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetLeftMargin()
{
  short result;
  InvokeHelper(0x180, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetLeftMargin(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x180, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetTopMargin()
{
  short result;
  InvokeHelper(0x181, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetTopMargin(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x181, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetLineSpacing()
{
  short result;
  InvokeHelper(0x182, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetLineSpacing(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x182, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetRightMargin()
{
  short result;
  InvokeHelper(0x184, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetRightMargin(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x184, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Textbox::GetBottomMargin()
{
  short result;
  InvokeHelper(0x185, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Textbox::SetBottomMargin(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x185, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Textbox::GetIsHyperlink()
{
  BOOL result;
  InvokeHelper(0x18c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Textbox::SetIsHyperlink(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x18c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _ListBox properties

/////////////////////////////////////////////////////////////////////////////
// _ListBox operations

LPDISPATCH _ListBox::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _ListBox::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _ListBox::Undo()
{
  InvokeHelper(0x860, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT _ListBox::GetColumn(long Index, const VARIANT& Row)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT;
  InvokeHelper(0x835, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms,
    Index, &Row);
  return result;
}

long _ListBox::GetSelected(long lRow)
{
  long result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x841, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, parms,
    lRow);
  return result;
}

void _ListBox::SetSelected(long lRow, long nNewValue)
{
  static BYTE parms[] =
    VTS_I4 VTS_I4;
  InvokeHelper(0x841, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lRow, nNewValue);
}

VARIANT _ListBox::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

VARIANT _ListBox::GetItemData(long Index)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x837, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms,
    Index);
  return result;
}

LPDISPATCH _ListBox::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _ListBox::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _ListBox::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _ListBox::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _ListBox::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _ListBox::GetItemsSelected()
{
  LPDISPATCH result;
  InvokeHelper(0x859, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _ListBox::GetHyperlink()
{
  LPDISPATCH result;
  InvokeHelper(0x885, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _ListBox::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _ListBox::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _ListBox::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetRowSourceType()
{
  CString result;
  InvokeHelper(0x5d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetRowSourceType(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x5d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetRowSource()
{
  CString result;
  InvokeHelper(0x5b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetRowSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x5b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _ListBox::GetColumnCount()
{
  short result;
  InvokeHelper(0x46, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetColumnCount(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x46, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ListBox::GetColumnHeads()
{
  BOOL result;
  InvokeHelper(0x83, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetColumnHeads(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x83, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _ListBox::GetColumnWidths()
{
  CString result;
  InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetColumnWidths(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _ListBox::GetBoundColumn()
{
  long result;
  InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ListBox::SetBoundColumn(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ListBox::GetDefaultValue()
{
  CString result;
  InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetDefaultValue(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _ListBox::GetIMEHold()
{
  BOOL result;
  InvokeHelper(0x165, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetIMEHold(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x165, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _ListBox::GetValidationRule()
{
  CString result;
  InvokeHelper(0x91, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetValidationRule(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x91, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetValidationText()
{
  CString result;
  InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetValidationText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _ListBox::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ListBox::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ListBox::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ListBox::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ListBox::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ListBox::GetHideDuplicates()
{
  BOOL result;
  InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetHideDuplicates(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ListBox::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ListBox::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ListBox::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ListBox::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _ListBox::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ListBox::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _ListBox::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ListBox::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _ListBox::GetForeColor()
{
  long result;
  InvokeHelper(0xcc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ListBox::SetForeColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xcc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ListBox::GetFontName()
{
  CString result;
  InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetFontName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x22, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _ListBox::GetFontSize()
{
  short result;
  InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetFontSize(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ListBox::GetFontWeight()
{
  short result;
  InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetFontWeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ListBox::GetFontItalic()
{
  BOOL result;
  InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetFontItalic(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ListBox::GetFontUnderline()
{
  BOOL result;
  InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetFontUnderline(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ListBox::GetFontBold()
{
  short result;
  InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetFontBold(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ListBox::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _ListBox::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ListBox::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ListBox::GetColumnWidth()
{
  short result;
  InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetColumnWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ListBox::GetColumnOrder()
{
  short result;
  InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetColumnOrder(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ListBox::GetColumnHidden()
{
  BOOL result;
  InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetColumnHidden(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ListBox::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ListBox::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ListBox::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ListBox::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ListBox::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ListBox::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ListBox::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _ListBox::GetListCount()
{
  long result;
  InvokeHelper(0xee, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ListBox::SetListCount(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xee, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _ListBox::GetListIndex()
{
  long result;
  InvokeHelper(0xef, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ListBox::SetListIndex(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xef, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ListBox::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ListBox::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ListBox::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _ListBox::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ListBox::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _ListBox::GetIMEMode()
{
  long result;
  InvokeHelper(0x164, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ListBox::SetIMEMode(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x164, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ListBox::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ListBox::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _ListBox::GetIMESentenceMode()
{
  long result;
  InvokeHelper(0x17b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ListBox::SetIMESentenceMode(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x17b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _Combobox properties

/////////////////////////////////////////////////////////////////////////////
// _Combobox operations

LPDISPATCH _Combobox::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Combobox::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Combobox::Undo()
{
  InvokeHelper(0x860, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Combobox::Dropdown()
{
  InvokeHelper(0x85f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT _Combobox::GetColumn(long Index, const VARIANT& Row)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT;
  InvokeHelper(0x835, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms,
    Index, &Row);
  return result;
}

VARIANT _Combobox::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

VARIANT _Combobox::GetItemData(long Index)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x837, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms,
    Index);
  return result;
}

LPDISPATCH _Combobox::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Combobox::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Combobox::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Combobox::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Combobox::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Combobox::GetHyperlink()
{
  LPDISPATCH result;
  InvokeHelper(0x885, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Combobox::GetFormatConditions()
{
  LPDISPATCH result;
  InvokeHelper(0x89b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _Combobox::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Combobox::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Combobox::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetFormat()
{
  CString result;
  InvokeHelper(0x26, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetFormat(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x26, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetInputMask()
{
  CString result;
  InvokeHelper(0x48, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetInputMask(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x48, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetRowSourceType()
{
  CString result;
  InvokeHelper(0x5d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetRowSourceType(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x5d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetRowSource()
{
  CString result;
  InvokeHelper(0x5b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetRowSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x5b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Combobox::GetColumnCount()
{
  short result;
  InvokeHelper(0x46, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetColumnCount(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x46, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Combobox::GetColumnHeads()
{
  BOOL result;
  InvokeHelper(0x83, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetColumnHeads(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x83, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Combobox::GetColumnWidths()
{
  CString result;
  InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetColumnWidths(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x12, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Combobox::GetBoundColumn()
{
  long result;
  InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Combobox::SetBoundColumn(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetListRows()
{
  short result;
  InvokeHelper(0x99, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetListRows(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x99, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Combobox::GetListWidth()
{
  CString result;
  InvokeHelper(0x9a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetListWidth(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x9a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Combobox::GetLimitToList()
{
  BOOL result;
  InvokeHelper(0x43, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetLimitToList(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x43, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Combobox::GetAutoExpand()
{
  BOOL result;
  InvokeHelper(0x44, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetAutoExpand(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x44, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Combobox::GetDefaultValue()
{
  CString result;
  InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetDefaultValue(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Combobox::GetIMEHold()
{
  BOOL result;
  InvokeHelper(0x165, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetIMEHold(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x165, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Combobox::GetValidationRule()
{
  CString result;
  InvokeHelper(0x91, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetValidationRule(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x91, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetValidationText()
{
  CString result;
  InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetValidationText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Combobox::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Combobox::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Combobox::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Combobox::GetAllowAutoCorrect()
{
  BOOL result;
  InvokeHelper(0x154, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetAllowAutoCorrect(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x154, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Combobox::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Combobox::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Combobox::GetHideDuplicates()
{
  BOOL result;
  InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetHideDuplicates(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Combobox::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Combobox::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Combobox::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Combobox::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Combobox::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Combobox::GetForeColor()
{
  long result;
  InvokeHelper(0xcc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Combobox::SetForeColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xcc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Combobox::GetFontName()
{
  CString result;
  InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetFontName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x22, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Combobox::GetFontSize()
{
  short result;
  InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetFontSize(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetFontWeight()
{
  short result;
  InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetFontWeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Combobox::GetFontItalic()
{
  BOOL result;
  InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetFontItalic(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Combobox::GetFontUnderline()
{
  BOOL result;
  InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetFontUnderline(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Combobox::GetFontBold()
{
  short result;
  InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetFontBold(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Combobox::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Combobox::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Combobox::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetColumnWidth()
{
  short result;
  InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetColumnWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetColumnOrder()
{
  short result;
  InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetColumnOrder(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Combobox::GetColumnHidden()
{
  BOOL result;
  InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetColumnHidden(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Combobox::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Combobox::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Combobox::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Combobox::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetText()
{
  CString result;
  InvokeHelper(0x10b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetSelText()
{
  CString result;
  InvokeHelper(0x10f, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetSelText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Combobox::GetSelStart()
{
  short result;
  InvokeHelper(0x10e, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetSelStart(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x10e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Combobox::GetSelLength()
{
  short result;
  InvokeHelper(0x10d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Combobox::SetSelLength(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x10d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Combobox::GetListCount()
{
  long result;
  InvokeHelper(0xee, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Combobox::SetListCount(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xee, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Combobox::GetListIndex()
{
  long result;
  InvokeHelper(0xef, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Combobox::SetListIndex(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xef, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Combobox::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Combobox::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Combobox::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnChange()
{
  CString result;
  InvokeHelper(0x72, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnChange(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x72, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnNotInList()
{
  CString result;
  InvokeHelper(0x78, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnNotInList(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x78, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Combobox::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Combobox::GetIMEMode()
{
  long result;
  InvokeHelper(0x164, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Combobox::SetIMEMode(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x164, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Combobox::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Combobox::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Combobox::GetIMESentenceMode()
{
  long result;
  InvokeHelper(0x17b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Combobox::SetIMESentenceMode(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x17b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Combobox::GetIsHyperlink()
{
  BOOL result;
  InvokeHelper(0x18c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Combobox::SetIsHyperlink(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x18c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _ObjectFrame properties

/////////////////////////////////////////////////////////////////////////////
// _ObjectFrame operations

LPDISPATCH _ObjectFrame::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _ObjectFrame::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _ObjectFrame::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _ObjectFrame::GetObject()
{
  LPDISPATCH result;
  InvokeHelper(0x838, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _ObjectFrame::GetObjectVerbs(long Index)
{
  CString result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x839, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
    Index);
  return result;
}

LPDISPATCH _ObjectFrame::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _ObjectFrame::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _ObjectFrame::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _ObjectFrame::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _ObjectFrame::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOLEClass()
{
  CString result;
  InvokeHelper(0x4c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOLEClass(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x4c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetItem()
{
  CString result;
  InvokeHelper(0x2f, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetItem(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x2f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetRowSourceType()
{
  CString result;
  InvokeHelper(0x5d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetRowSourceType(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x5d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetRowSource()
{
  CString result;
  InvokeHelper(0x5b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetRowSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x5b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetLinkChildFields()
{
  CString result;
  InvokeHelper(0x31, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetLinkChildFields(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x31, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetLinkMasterFields()
{
  CString result;
  InvokeHelper(0x32, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetLinkMasterFields(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x32, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _ObjectFrame::GetAutoActivate()
{
  short result;
  InvokeHelper(0x65, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetAutoActivate(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x65, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ObjectFrame::GetDisplayType()
{
  BOOL result;
  InvokeHelper(0x110, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetDisplayType(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x110, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ObjectFrame::GetUpdateOptions()
{
  short result;
  InvokeHelper(0x67, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetUpdateOptions(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x67, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _ObjectFrame::GetVerb()
{
  long result;
  InvokeHelper(0x66, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetVerb(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x66, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ObjectFrame::GetSourceObject()
{
  CString result;
  InvokeHelper(0x84, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetSourceObject(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x84, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetClass()
{
  CString result;
  InvokeHelper(0x112, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetClass(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x112, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetSourceDoc()
{
  CString result;
  InvokeHelper(0x85, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetSourceDoc(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x85, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetSourceItem()
{
  CString result;
  InvokeHelper(0x30, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetSourceItem(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x30, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _ObjectFrame::GetColumnCount()
{
  short result;
  InvokeHelper(0x46, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetColumnCount(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x46, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ObjectFrame::GetColumnHeads()
{
  BOOL result;
  InvokeHelper(0x83, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetColumnHeads(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x83, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ObjectFrame::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ObjectFrame::GetUpdateMethod()
{
  short result;
  InvokeHelper(0x8e, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetUpdateMethod(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ObjectFrame::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ObjectFrame::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _ObjectFrame::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _ObjectFrame::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ObjectFrame::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ObjectFrame::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ObjectFrame::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ObjectFrame::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ObjectFrame::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _ObjectFrame::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _ObjectFrame::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ObjectFrame::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _ObjectFrame::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ObjectFrame::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ObjectFrame::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _ObjectFrame::GetObjectPalette()
{
  VARIANT result;
  InvokeHelper(0x63, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetObjectPalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x63, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

long _ObjectFrame::GetLpOleObject()
{
  long result;
  InvokeHelper(0xac, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetLpOleObject(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xac, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _ObjectFrame::GetObjectVerbsCount()
{
  long result;
  InvokeHelper(0xad, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetObjectVerbsCount(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xad, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ObjectFrame::GetAction()
{
  short result;
  InvokeHelper(0x111, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetAction(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x111, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ObjectFrame::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ObjectFrame::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _ObjectFrame::GetOnUpdated()
{
  CString result;
  InvokeHelper(0x76, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnUpdated(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x76, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ObjectFrame::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ObjectFrame::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _PageBreak properties

/////////////////////////////////////////////////////////////////////////////
// _PageBreak operations

LPDISPATCH _PageBreak::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _PageBreak::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _PageBreak::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _PageBreak::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString _PageBreak::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PageBreak::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _PageBreak::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PageBreak::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _PageBreak::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PageBreak::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _PageBreak::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PageBreak::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _PageBreak::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PageBreak::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _PageBreak::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PageBreak::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _PageBreak::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PageBreak::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _PageBreak::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PageBreak::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _PageBreak::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PageBreak::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _ToggleButton properties

/////////////////////////////////////////////////////////////////////////////
// _ToggleButton operations

LPDISPATCH _ToggleButton::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _ToggleButton::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _ToggleButton::Undo()
{
  InvokeHelper(0x860, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

VARIANT _ToggleButton::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _ToggleButton::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _ToggleButton::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _ToggleButton::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _ToggleButton::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _ToggleButton::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _ToggleButton::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetCaption()
{
  CString result;
  InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetCaption(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetPicture()
{
  CString result;
  InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetPicture(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _ToggleButton::GetPictureData()
{
  VARIANT result;
  InvokeHelper(0xbc, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetPictureData(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

long _ToggleButton::GetOptionValue()
{
  long result;
  InvokeHelper(0x3a, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOptionValue(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x3a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ToggleButton::GetDefaultValue()
{
  CString result;
  InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetDefaultValue(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetValidationRule()
{
  CString result;
  InvokeHelper(0x91, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetValidationRule(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x91, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetValidationText()
{
  CString result;
  InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetValidationText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _ToggleButton::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ToggleButton::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ToggleButton::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ToggleButton::GetTripleState()
{
  BOOL result;
  InvokeHelper(0x126, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetTripleState(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x126, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ToggleButton::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ToggleButton::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ToggleButton::GetHideDuplicates()
{
  BOOL result;
  InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetHideDuplicates(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ToggleButton::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ToggleButton::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ToggleButton::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ToggleButton::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _ToggleButton::GetForeColor()
{
  long result;
  InvokeHelper(0xcc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetForeColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xcc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ToggleButton::GetFontName()
{
  CString result;
  InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetFontName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x22, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _ToggleButton::GetFontSize()
{
  short result;
  InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetFontSize(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ToggleButton::GetFontWeight()
{
  short result;
  InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetFontWeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ToggleButton::GetFontItalic()
{
  BOOL result;
  InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetFontItalic(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ToggleButton::GetFontUnderline()
{
  BOOL result;
  InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetFontUnderline(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ToggleButton::GetFontBold()
{
  short result;
  InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetFontBold(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ToggleButton::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _ToggleButton::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ToggleButton::GetColumnWidth()
{
  short result;
  InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetColumnWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ToggleButton::GetColumnOrder()
{
  short result;
  InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetColumnOrder(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _ToggleButton::GetColumnHidden()
{
  BOOL result;
  InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetColumnHidden(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ToggleButton::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ToggleButton::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _ToggleButton::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ToggleButton::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _ToggleButton::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _ToggleButton::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _ToggleButton::GetObjectPalette()
{
  VARIANT result;
  InvokeHelper(0x63, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetObjectPalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x63, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

BOOL _ToggleButton::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _ToggleButton::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _ToggleButton::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _ToggleButton::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _ToggleButton::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _PaletteButton properties

/////////////////////////////////////////////////////////////////////////////
// _PaletteButton operations

LPDISPATCH _PaletteButton::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _PaletteButton::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _PaletteButton::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _PaletteButton::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _PaletteButton::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _PaletteButton::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _PaletteButton::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _PaletteButton::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _PaletteButton::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _PaletteButton::GetOptionValue()
{
  long result;
  InvokeHelper(0x3a, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOptionValue(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x3a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _PaletteButton::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetDefaultValue()
{
  CString result;
  InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetDefaultValue(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetValidationRule()
{
  CString result;
  InvokeHelper(0x91, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetValidationRule(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x91, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetValidationText()
{
  CString result;
  InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetValidationText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _PaletteButton::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _PaletteButton::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _PaletteButton::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _PaletteButton::GetTripleState()
{
  BOOL result;
  InvokeHelper(0x126, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetTripleState(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x126, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _PaletteButton::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _PaletteButton::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _PaletteButton::GetHideDuplicates()
{
  BOOL result;
  InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetHideDuplicates(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _PaletteButton::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _PaletteButton::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _PaletteButton::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _PaletteButton::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _PaletteButton::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _PaletteButton::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _PaletteButton::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _PaletteButton::GetColumnWidth()
{
  short result;
  InvokeHelper(0xa6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetColumnWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _PaletteButton::GetColumnOrder()
{
  short result;
  InvokeHelper(0xa5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetColumnOrder(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _PaletteButton::GetColumnHidden()
{
  BOOL result;
  InvokeHelper(0xa7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetColumnHidden(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _PaletteButton::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _PaletteButton::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _PaletteButton::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _PaletteButton::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _PaletteButton::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _PaletteButton::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _PaletteButton::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _PaletteButton::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _PaletteButton::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _PaletteButton::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _PaletteButton::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _SubForm properties

/////////////////////////////////////////////////////////////////////////////
// _SubForm operations

LPDISPATCH _SubForm::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _SubForm::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _SubForm::GetForm()
{
  LPDISPATCH result;
  InvokeHelper(0x829, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _SubForm::GetReport()
{
  LPDISPATCH result;
  InvokeHelper(0x831, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _SubForm::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _SubForm::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _SubForm::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _SubForm::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _SubForm::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _SubForm::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _SubForm::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _SubForm::GetSourceObject()
{
  CString result;
  InvokeHelper(0x84, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _SubForm::SetSourceObject(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x84, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _SubForm::GetLinkChildFields()
{
  CString result;
  InvokeHelper(0x31, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _SubForm::SetLinkChildFields(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x31, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _SubForm::GetLinkMasterFields()
{
  CString result;
  InvokeHelper(0x32, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _SubForm::SetLinkMasterFields(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x32, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _SubForm::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _SubForm::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _SubForm::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _SubForm::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _SubForm::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _SubForm::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _SubForm::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _SubForm::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _SubForm::GetCanGrow()
{
  BOOL result;
  InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetCanGrow(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _SubForm::GetCanShrink()
{
  BOOL result;
  InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetCanShrink(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _SubForm::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _SubForm::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _SubForm::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _SubForm::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _SubForm::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _SubForm::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _SubForm::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _SubForm::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _SubForm::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _SubForm::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _SubForm::GetAutoLabel()
{
  BOOL result;
  InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetAutoLabel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x39, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _SubForm::GetAddColon()
{
  BOOL result;
  InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetAddColon(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _SubForm::GetLabelX()
{
  short result;
  InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _SubForm::SetLabelX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _SubForm::GetLabelY()
{
  short result;
  InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _SubForm::SetLabelY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _SubForm::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _SubForm::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _SubForm::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _SubForm::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _SubForm::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _SubForm::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _SubForm::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _SubForm::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _SubForm::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _SubForm::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _SubForm::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _SubForm::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _SubForm::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _SubReport properties

/////////////////////////////////////////////////////////////////////////////
// _SubReport operations

LPDISPATCH _SubReport::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _SubReport::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _SubReport::GetForm()
{
  LPDISPATCH result;
  InvokeHelper(0x829, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _SubReport::GetReport()
{
  LPDISPATCH result;
  InvokeHelper(0x831, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _CustomControl properties

/////////////////////////////////////////////////////////////////////////////
// _CustomControl operations

LPDISPATCH _CustomControl::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CustomControl::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _CustomControl::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CustomControl::GetObject()
{
  LPDISPATCH result;
  InvokeHelper(0x838, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _CustomControl::GetObjectVerbs(long Index)
{
  CString result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x839, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
    Index);
  return result;
}

LPDISPATCH _CustomControl::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _CustomControl::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _CustomControl::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _CustomControl::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _CustomControl::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _CustomControl::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _CustomControl::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CustomControl::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CustomControl::GetOLEClass()
{
  CString result;
  InvokeHelper(0x4c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetOLEClass(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x4c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _CustomControl::GetVerb()
{
  long result;
  InvokeHelper(0x66, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetVerb(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x66, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _CustomControl::GetClass()
{
  CString result;
  InvokeHelper(0x112, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetClass(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x112, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _CustomControl::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CustomControl::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CustomControl::GetLocked()
{
  BOOL result;
  InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetLocked(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CustomControl::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _CustomControl::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CustomControl::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CustomControl::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CustomControl::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CustomControl::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _CustomControl::GetBorderColor()
{
  long result;
  InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetBorderColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _CustomControl::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _CustomControl::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _CustomControl::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _CustomControl::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _CustomControl::GetObjectPalette()
{
  VARIANT result;
  InvokeHelper(0x63, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetObjectPalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x63, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

long _CustomControl::GetLpOleObject()
{
  long result;
  InvokeHelper(0xac, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetLpOleObject(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xac, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _CustomControl::GetObjectVerbsCount()
{
  long result;
  InvokeHelper(0xad, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetObjectVerbsCount(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xad, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _CustomControl::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CustomControl::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _CustomControl::GetOnUpdated()
{
  CString result;
  InvokeHelper(0x76, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetOnUpdated(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x76, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CustomControl::GetOnEnter()
{
  CString result;
  InvokeHelper(0xde, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetOnEnter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xde, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CustomControl::GetOnExit()
{
  CString result;
  InvokeHelper(0xdf, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetOnExit(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xdf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CustomControl::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CustomControl::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _CustomControl::GetDefault()
{
  BOOL result;
  InvokeHelper(0xdc, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetDefault(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xdc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _CustomControl::GetCancel()
{
  BOOL result;
  InvokeHelper(0xdd, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetCancel(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xdd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _CustomControl::GetCustom()
{
  CString result;
  InvokeHelper(0x128, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetCustom(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x128, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CustomControl::GetAbout()
{
  CString result;
  InvokeHelper(0x129, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetAbout(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x129, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _CustomControl::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _CustomControl::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _TabControl properties

/////////////////////////////////////////////////////////////////////////////
// _TabControl operations

LPDISPATCH _TabControl::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _TabControl::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _TabControl::GetOldValue()
{
  VARIANT result;
  InvokeHelper(0x836, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

LPDISPATCH _TabControl::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _TabControl::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _TabControl::GetPages()
{
  LPDISPATCH result;
  InvokeHelper(0x884, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

VARIANT _TabControl::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _TabControl::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _TabControl::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _TabControl::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _TabControl::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _TabControl::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _TabControl::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _TabControl::GetTabStop()
{
  BOOL result;
  InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _TabControl::SetTabStop(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x106, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _TabControl::GetTabIndex()
{
  short result;
  InvokeHelper(0x105, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetTabIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x105, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _TabControl::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _TabControl::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _TabControl::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _TabControl::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _TabControl::GetMultiRow()
{
  BOOL result;
  InvokeHelper(0x15c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _TabControl::SetMultiRow(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x15c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _TabControl::GetTabFixedHeight()
{
  short result;
  InvokeHelper(0x15e, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetTabFixedHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x15e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _TabControl::GetTabFixedWidth()
{
  short result;
  InvokeHelper(0x15f, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetTabFixedWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x15f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _TabControl::GetFontName()
{
  CString result;
  InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetFontName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x22, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _TabControl::GetFontSize()
{
  short result;
  InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetFontSize(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _TabControl::GetFontWeight()
{
  short result;
  InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetFontWeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x25, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _TabControl::GetFontItalic()
{
  BOOL result;
  InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _TabControl::SetFontItalic(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _TabControl::GetFontUnderline()
{
  BOOL result;
  InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _TabControl::SetFontUnderline(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _TabControl::GetFontBold()
{
  short result;
  InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetFontBold(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _TabControl::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _TabControl::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _TabControl::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _TabControl::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _TabControl::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _TabControl::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _TabControl::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _TabControl::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _TabControl::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _TabControl::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _TabControl::GetOnChange()
{
  CString result;
  InvokeHelper(0x72, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetOnChange(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x72, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _TabControl::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _TabControl::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _Page properties

/////////////////////////////////////////////////////////////////////////////
// _Page operations

LPDISPATCH _Page::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Page::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Page::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Page::SizeToFit()
{
  InvokeHelper(0x867, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Page::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Page::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Page::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Page::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Page::GetCaption()
{
  CString result;
  InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetCaption(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Page::GetPicture()
{
  CString result;
  InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetPicture(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Page::GetPageIndex()
{
  short result;
  InvokeHelper(0x160, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Page::SetPageIndex(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x160, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Page::GetStatusBarText()
{
  CString result;
  InvokeHelper(0x87, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetStatusBarText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x87, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Page::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Page::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Page::GetEnabled()
{
  BOOL result;
  InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Page::SetEnabled(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x19, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Page::GetLeft()
{
  short result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Page::SetLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Page::GetTop()
{
  short result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Page::SetTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Page::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Page::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Page::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Page::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Page::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Page::GetControlTipText()
{
  CString result;
  InvokeHelper(0x13d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetControlTipText(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x13d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Page::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Page::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Page::GetSection()
{
  short result;
  InvokeHelper(0xed, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Page::SetSection(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Page::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Page::GetIsVisible()
{
  BOOL result;
  InvokeHelper(0x8c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Page::SetIsVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Page::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Page::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Page::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Page::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Page::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Page::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Page::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _Page::GetPictureData()
{
  VARIANT result;
  InvokeHelper(0xbc, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Page::SetPictureData(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Page::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Page::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _Section properties

/////////////////////////////////////////////////////////////////////////////
// _Section operations

LPDISPATCH _Section::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Section::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Section::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Section::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x88e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Section::GetEventProcPrefix()
{
  CString result;
  InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetEventProcPrefix(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x16, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Section::GetKeepTogether()
{
  BOOL result;
  InvokeHelper(0x4b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Section::SetKeepTogether(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x4b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Section::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Section::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Section::GetCanGrow()
{
  BOOL result;
  InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Section::SetCanGrow(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Section::GetCanShrink()
{
  BOOL result;
  InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Section::SetCanShrink(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x10, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Section::GetRepeatSection()
{
  BOOL result;
  InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Section::SetRepeatSection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Section::GetHeight()
{
  short result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Section::SetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Section::GetBackColor()
{
  long result;
  InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Section::SetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x1c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Section::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Section::GetOnFormat()
{
  CString result;
  InvokeHelper(0x50, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetOnFormat(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x50, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Section::GetOnPrint()
{
  CString result;
  InvokeHelper(0x52, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetOnPrint(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x52, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Section::GetOnRetreat()
{
  CString result;
  InvokeHelper(0x54, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetOnRetreat(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x54, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Section::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Section::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Section::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Section::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Section::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Section::GetHasContinued()
{
  BOOL result;
  InvokeHelper(0xc8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Section::SetHasContinued(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xc8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Section::GetWillContinue()
{
  BOOL result;
  InvokeHelper(0xc9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Section::SetWillContinue(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xc9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Section::GetInSelection()
{
  BOOL result;
  InvokeHelper(0x118, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Section::SetInSelection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x118, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Section::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Section::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// _GroupLevel properties

/////////////////////////////////////////////////////////////////////////////
// _GroupLevel operations

LPDISPATCH _GroupLevel::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _GroupLevel::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _GroupLevel::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _GroupLevel::GetControlSource()
{
  CString result;
  InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _GroupLevel::SetControlSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _GroupLevel::GetSortOrder()
{
  BOOL result;
  InvokeHelper(0xae, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _GroupLevel::SetSortOrder(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xae, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _GroupLevel::GetGroupHeader()
{
  BOOL result;
  InvokeHelper(0xaf, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _GroupLevel::SetGroupHeader(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xaf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _GroupLevel::GetGroupFooter()
{
  BOOL result;
  InvokeHelper(0xb0, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _GroupLevel::SetGroupFooter(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xb0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _GroupLevel::GetGroupOn()
{
  short result;
  InvokeHelper(0xb1, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _GroupLevel::SetGroupOn(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xb1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _GroupLevel::GetGroupInterval()
{
  long result;
  InvokeHelper(0xb2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _GroupLevel::SetGroupInterval(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xb2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// Module properties

/////////////////////////////////////////////////////////////////////////////
// Module operations

LPDISPATCH Module::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Module::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void Module::InsertText(LPCTSTR Text)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x82d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Text);
}

CString Module::GetName()
{
  CString result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void Module::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

void Module::AddFromString(LPCTSTR String)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x60020005, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     String);
}

void Module::AddFromFile(LPCTSTR FileName)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x60020006, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     FileName);
}

CString Module::GetLines(long Line, long NumLines)
{
  CString result;
  static BYTE parms[] =
    VTS_I4 VTS_I4;
  InvokeHelper(0x60020007, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
    Line, NumLines);
  return result;
}

long Module::GetCountOfLines()
{
  long result;
  InvokeHelper(0x60020008, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void Module::InsertLines(long Line, LPCTSTR String)
{
  static BYTE parms[] =
    VTS_I4 VTS_BSTR;
  InvokeHelper(0x60020009, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Line, String);
}

void Module::DeleteLines(long StartLine, long Count)
{
  static BYTE parms[] =
    VTS_I4 VTS_I4;
  InvokeHelper(0x6002000a, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     StartLine, Count);
}

void Module::ReplaceLine(long Line, LPCTSTR String)
{
  static BYTE parms[] =
    VTS_I4 VTS_BSTR;
  InvokeHelper(0x6002000b, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Line, String);
}

long Module::GetProcStartLine(LPCTSTR ProcName, long ProcKind)
{
  long result;
  static BYTE parms[] =
    VTS_BSTR VTS_I4;
  InvokeHelper(0x6002000c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, parms,
    ProcName, ProcKind);
  return result;
}

long Module::GetProcCountLines(LPCTSTR ProcName, long ProcKind)
{
  long result;
  static BYTE parms[] =
    VTS_BSTR VTS_I4;
  InvokeHelper(0x6002000d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, parms,
    ProcName, ProcKind);
  return result;
}

long Module::GetProcBodyLine(LPCTSTR ProcName, long ProcKind)
{
  long result;
  static BYTE parms[] =
    VTS_BSTR VTS_I4;
  InvokeHelper(0x6002000e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, parms,
    ProcName, ProcKind);
  return result;
}

CString Module::GetProcOfLine(long Line, long* pprockind)
{
  CString result;
  static BYTE parms[] =
    VTS_I4 VTS_PI4;
  InvokeHelper(0x6002000f, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms,
    Line, pprockind);
  return result;
}

long Module::GetCountOfDeclarationLines()
{
  long result;
  InvokeHelper(0x60020010, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

long Module::CreateEventProc(LPCTSTR EventName, LPCTSTR ObjectName)
{
  long result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR;
  InvokeHelper(0x60020011, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
    EventName, ObjectName);
  return result;
}

BOOL Module::Find(LPCTSTR Target, long* StartLine, long* StartColumn, long* EndLine, long* EndColumn, BOOL WholeWord, BOOL MatchCase, BOOL PatternSearch)
{
  BOOL result;
  static BYTE parms[] =
    VTS_BSTR VTS_PI4 VTS_PI4 VTS_PI4 VTS_PI4 VTS_BOOL VTS_BOOL VTS_BOOL;
  InvokeHelper(0x60020012, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
    Target, StartLine, StartColumn, EndLine, EndColumn, WholeWord, MatchCase, PatternSearch);
  return result;
}

long Module::GetType()
{
  long result;
  InvokeHelper(0x60020013, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// Modules properties

/////////////////////////////////////////////////////////////////////////////
// Modules operations

LPDISPATCH Modules::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Modules::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Modules::GetItem(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

long Modules::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _Form properties

/////////////////////////////////////////////////////////////////////////////
// _Form operations

CString _Form::GetRecordSource()
{
  CString result;
  InvokeHelper(0x9c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetRecordSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x9c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetFilter()
{
  CString result;
  InvokeHelper(0xf5, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetFilter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xf5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Form::GetFilterOn()
{
  BOOL result;
  InvokeHelper(0x14c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetFilterOn(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x14c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Form::GetOrderBy()
{
  CString result;
  InvokeHelper(0x14b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOrderBy(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x14b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Form::GetOrderByOn()
{
  BOOL result;
  InvokeHelper(0x150, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetOrderByOn(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x150, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetAllowFilters()
{
  BOOL result;
  InvokeHelper(0x1e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetAllowFilters(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x1e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Form::GetCaption()
{
  CString result;
  InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetCaption(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Form::GetAllowEdits()
{
  BOOL result;
  InvokeHelper(0x153, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetAllowEdits(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x153, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetAllowDeletions()
{
  BOOL result;
  InvokeHelper(0x124, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetAllowDeletions(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x124, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetAllowAdditions()
{
  BOOL result;
  InvokeHelper(0x146, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetAllowAdditions(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x146, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetDataEntry()
{
  BOOL result;
  InvokeHelper(0x152, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetDataEntry(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x152, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetRecordSelectors()
{
  BOOL result;
  InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetRecordSelectors(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x13, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetNavigationButtons()
{
  BOOL result;
  InvokeHelper(0x117, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetNavigationButtons(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x117, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetDividingLines()
{
  BOOL result;
  InvokeHelper(0x13a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetDividingLines(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x13a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetAutoResize()
{
  BOOL result;
  InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetAutoResize(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetAutoCenter()
{
  BOOL result;
  InvokeHelper(0x116, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetAutoCenter(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x116, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetPopUp()
{
  BOOL result;
  InvokeHelper(0x5a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetPopUp(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x5a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetModal()
{
  BOOL result;
  InvokeHelper(0x3e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetModal(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetControlBox()
{
  BOOL result;
  InvokeHelper(0x61, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetControlBox(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x61, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetCloseButton()
{
  BOOL result;
  InvokeHelper(0x12f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetCloseButton(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x12f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetWhatsThisButton()
{
  BOOL result;
  InvokeHelper(0x60, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetWhatsThisButton(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x60, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Form::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Form::GetPicture()
{
  CString result;
  InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetPicture(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Form::GetPictureTiling()
{
  BOOL result;
  InvokeHelper(0x139, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetPictureTiling(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x139, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Form::GetMenuBar()
{
  CString result;
  InvokeHelper(0xf0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xf0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetToolbar()
{
  CString result;
  InvokeHelper(0x161, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetToolbar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x161, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Form::GetShortcutMenu()
{
  BOOL result;
  InvokeHelper(0x1f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetShortcutMenu(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x1f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Form::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Form::GetGridX()
{
  short result;
  InvokeHelper(0x29, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetGridX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x29, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Form::GetGridY()
{
  short result;
  InvokeHelper(0x2a, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetGridY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Form::GetLayoutForPrint()
{
  BOOL result;
  InvokeHelper(0x8f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetLayoutForPrint(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetFastLaserPrinting()
{
  BOOL result;
  InvokeHelper(0x90, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetFastLaserPrinting(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x90, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Form::GetHelpFile()
{
  CString result;
  InvokeHelper(0xda, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetHelpFile(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xda, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Form::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Form::GetRowHeight()
{
  short result;
  InvokeHelper(0xa8, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetRowHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Form::GetDatasheetFontName()
{
  CString result;
  InvokeHelper(0xa0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetDatasheetFontName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xa0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Form::GetDatasheetFontHeight()
{
  short result;
  InvokeHelper(0xa1, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetDatasheetFontHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Form::GetDatasheetFontWeight()
{
  short result;
  InvokeHelper(0xa2, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetDatasheetFontWeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xa2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Form::GetDatasheetFontItalic()
{
  BOOL result;
  InvokeHelper(0xa3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetDatasheetFontItalic(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetDatasheetFontUnderline()
{
  BOOL result;
  InvokeHelper(0xa4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetDatasheetFontUnderline(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xa4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

long _Form::GetDatasheetGridlinesColor()
{
  long result;
  InvokeHelper(0x13f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetDatasheetGridlinesColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x13f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetDatasheetForeColor()
{
  long result;
  InvokeHelper(0x132, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetDatasheetForeColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x132, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetDatasheetBackColor()
{
  long result;
  InvokeHelper(0x141, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetDatasheetBackColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x141, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetHwnd()
{
  long result;
  InvokeHelper(0xc0, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetHwnd(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xc0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Form::GetCount()
{
  short result;
  InvokeHelper(0xc3, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetCount(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xc3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetPage()
{
  long result;
  InvokeHelper(0xc4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetPage(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xc4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Form::GetPages()
{
  short result;
  InvokeHelper(0xc5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetPages(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xc5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Form::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetPainting()
{
  BOOL result;
  InvokeHelper(0xf1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetPainting(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xf1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

VARIANT _Form::GetPrtMip()
{
  VARIANT result;
  InvokeHelper(0xbd, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Form::SetPrtMip(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

VARIANT _Form::GetPrtDevMode()
{
  VARIANT result;
  InvokeHelper(0xbe, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Form::SetPrtDevMode(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

VARIANT _Form::GetPrtDevNames()
{
  VARIANT result;
  InvokeHelper(0xbf, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Form::SetPrtDevNames(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

short _Form::GetFrozenColumns()
{
  short result;
  InvokeHelper(0xaa, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetFrozenColumns(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xaa, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

VARIANT _Form::GetBookmark()
{
  VARIANT result;
  InvokeHelper(0xe6, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Form::SetBookmark(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xe6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Form::GetPaletteSource()
{
  CString result;
  InvokeHelper(0x62, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetPaletteSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x62, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _Form::GetPaintPalette()
{
  VARIANT result;
  InvokeHelper(0x64, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Form::SetPaintPalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x64, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Form::GetOnMenu()
{
  CString result;
  InvokeHelper(0x3c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnMenu(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _Form::GetOpenArgs()
{
  VARIANT result;
  InvokeHelper(0x11c, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Form::SetOpenArgs(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x11c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Form::GetOnCurrent()
{
  CString result;
  InvokeHelper(0x57, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnCurrent(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x57, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnInsert()
{
  CString result;
  InvokeHelper(0x51, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnInsert(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x51, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetBeforeInsert()
{
  CString result;
  InvokeHelper(0x7a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetBeforeInsert(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetAfterInsert()
{
  CString result;
  InvokeHelper(0x7b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetAfterInsert(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetBeforeUpdate()
{
  CString result;
  InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetBeforeUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetAfterUpdate()
{
  CString result;
  InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetAfterUpdate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnDirty()
{
  CString result;
  InvokeHelper(0x177, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnDirty(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x177, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnDelete()
{
  CString result;
  InvokeHelper(0x4f, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnDelete(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x4f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetBeforeDelConfirm()
{
  CString result;
  InvokeHelper(0x7c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetBeforeDelConfirm(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetAfterDelConfirm()
{
  CString result;
  InvokeHelper(0x7d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetAfterDelConfirm(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnOpen()
{
  CString result;
  InvokeHelper(0x4d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnOpen(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x4d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnLoad()
{
  CString result;
  InvokeHelper(0x7f, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnLoad(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnResize()
{
  CString result;
  InvokeHelper(0x75, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnResize(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x75, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnUnload()
{
  CString result;
  InvokeHelper(0x80, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnUnload(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnClose()
{
  CString result;
  InvokeHelper(0x4e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnClose(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x4e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnActivate()
{
  CString result;
  InvokeHelper(0x70, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnActivate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x70, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnDeactivate()
{
  CString result;
  InvokeHelper(0x71, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnDeactivate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x71, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnGotFocus()
{
  CString result;
  InvokeHelper(0x73, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnGotFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x73, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnLostFocus()
{
  CString result;
  InvokeHelper(0x74, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnLostFocus(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x74, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnClick()
{
  CString result;
  InvokeHelper(0x7e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnDblClick()
{
  CString result;
  InvokeHelper(0xe0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnDblClick(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xe0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnMouseDown()
{
  CString result;
  InvokeHelper(0x6b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnMouseDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnMouseMove()
{
  CString result;
  InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnMouseMove(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnMouseUp()
{
  CString result;
  InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnMouseUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnKeyDown()
{
  CString result;
  InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnKeyDown(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x68, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnKeyUp()
{
  CString result;
  InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnKeyUp(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnKeyPress()
{
  CString result;
  InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnKeyPress(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Form::GetKeyPreview()
{
  BOOL result;
  InvokeHelper(0x147, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetKeyPreview(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x147, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Form::GetOnError()
{
  CString result;
  InvokeHelper(0x77, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnError(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x77, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnFilter()
{
  CString result;
  InvokeHelper(0x14d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnFilter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x14d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnApplyFilter()
{
  CString result;
  InvokeHelper(0x14e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnApplyFilter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x14e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetOnTimer()
{
  CString result;
  InvokeHelper(0x6e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetOnTimer(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x6e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Form::GetTimerInterval()
{
  long result;
  InvokeHelper(0x6f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetTimerInterval(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x6f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Form::GetDirty()
{
  BOOL result;
  InvokeHelper(0x11e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetDirty(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x11e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Form::GetWindowWidth()
{
  short result;
  InvokeHelper(0x11f, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetWindowWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x11f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Form::GetWindowHeight()
{
  short result;
  InvokeHelper(0x120, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetWindowHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x120, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Form::GetCurrentView()
{
  short result;
  InvokeHelper(0x121, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetCurrentView(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x121, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Form::GetCurrentSectionTop()
{
  short result;
  InvokeHelper(0x123, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetCurrentSectionTop(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x123, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Form::GetCurrentSectionLeft()
{
  short result;
  InvokeHelper(0x122, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetCurrentSectionLeft(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x122, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetSelLeft()
{
  long result;
  InvokeHelper(0x12a, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetSelLeft(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x12a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetSelTop()
{
  long result;
  InvokeHelper(0x12b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetSelTop(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x12b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetSelWidth()
{
  long result;
  InvokeHelper(0x12c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetSelWidth(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x12c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetSelHeight()
{
  long result;
  InvokeHelper(0x12d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetSelHeight(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x12d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetCurrentRecord()
{
  long result;
  InvokeHelper(0x12e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetCurrentRecord(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x12e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

VARIANT _Form::GetPictureData()
{
  VARIANT result;
  InvokeHelper(0xbc, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Form::SetPictureData(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

long _Form::GetInsideHeight()
{
  long result;
  InvokeHelper(0x13b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetInsideHeight(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x13b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Form::GetInsideWidth()
{
  long result;
  InvokeHelper(0x13c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetInsideWidth(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x13c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

VARIANT _Form::GetPicturePalette()
{
  VARIANT result;
  InvokeHelper(0x137, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Form::SetPicturePalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x137, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

BOOL _Form::GetHasModule()
{
  BOOL result;
  InvokeHelper(0x156, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetHasModule(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x156, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Form::GetAllowDesignChanges()
{
  BOOL result;
  InvokeHelper(0x175, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetAllowDesignChanges(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x175, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Form::GetServerFilter()
{
  CString result;
  InvokeHelper(0xf6, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetServerFilter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xf6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Form::GetServerFilterByForm()
{
  BOOL result;
  InvokeHelper(0xf7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetServerFilterByForm(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xf7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

long _Form::GetMaxRecords()
{
  long result;
  InvokeHelper(0x169, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Form::SetMaxRecords(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x169, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Form::GetUniqueTable()
{
  CString result;
  InvokeHelper(0x41, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetUniqueTable(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x41, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetResyncCommand()
{
  CString result;
  InvokeHelper(0x40, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetResyncCommand(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x40, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Form::GetInputParameters()
{
  CString result;
  InvokeHelper(0x42, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetInputParameters(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x42, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Form::GetMaxRecButton()
{
  BOOL result;
  InvokeHelper(0x3f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetMaxRecButton(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x3f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

LPDISPATCH _Form::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Form::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

short _Form::GetNewRecord()
{
  short result;
  InvokeHelper(0x863, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::Undo()
{
  InvokeHelper(0x861, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Form::GetActiveControl()
{
  LPDISPATCH result;
  InvokeHelper(0x7d6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Form::GetDefaultControl(long ControlType)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x866, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    ControlType);
  return result;
}

LPDISPATCH _Form::GetRecordsetClone()
{
  LPDISPATCH result;
  InvokeHelper(0x826, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Form::GetRecordset()
{
  LPDISPATCH result;
  InvokeHelper(0x894, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Form::SetRefRecordset(LPDISPATCH newValue)
{
  static BYTE parms[] =
    VTS_DISPATCH;
  InvokeHelper(0x894, DISPATCH_PROPERTYPUTREF, VT_EMPTY, NULL, parms,
     newValue);
}

LPDISPATCH _Form::GetSection(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x828, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

LPDISPATCH _Form::GetForm()
{
  LPDISPATCH result;
  InvokeHelper(0x829, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Form::GetModule()
{
  LPDISPATCH result;
  InvokeHelper(0x82a, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Form::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Form::Recalc()
{
  InvokeHelper(0x7e1, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Form::Requery()
{
  InvokeHelper(0x7e2, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Form::Refresh()
{
  InvokeHelper(DISPID_REFRESH, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Form::Repaint()
{
  InvokeHelper(0x7e4, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Form::GoToPage(long PageNumber, long Right, long Down)
{
  static BYTE parms[] =
    VTS_I4 VTS_I4 VTS_I4;
  InvokeHelper(0x7e5, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     PageNumber, Right, Down);
}

void _Form::SetFocus()
{
  InvokeHelper(0x7e6, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Form::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Form::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Form::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Form::GetSubdatasheetHeight()
{
  short result;
  InvokeHelper(0x183, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Form::SetSubdatasheetHeight(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x183, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Form::GetSubdatasheetExpanded()
{
  BOOL result;
  InvokeHelper(0x186, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Form::SetSubdatasheetExpanded(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x186, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// Forms properties

/////////////////////////////////////////////////////////////////////////////
// Forms operations

LPDISPATCH Forms::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Forms::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Forms::GetItem(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

long Forms::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _Report properties

/////////////////////////////////////////////////////////////////////////////
// _Report operations

CString _Report::GetRecordSource()
{
  CString result;
  InvokeHelper(0x9c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetRecordSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x9c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetFilter()
{
  CString result;
  InvokeHelper(0xf5, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetFilter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xf5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Report::GetFilterOn()
{
  BOOL result;
  InvokeHelper(0x14c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetFilterOn(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x14c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Report::GetOrderBy()
{
  CString result;
  InvokeHelper(0x14b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetOrderBy(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x14b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Report::GetOrderByOn()
{
  BOOL result;
  InvokeHelper(0x150, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetOrderByOn(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x150, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Report::GetServerFilter()
{
  CString result;
  InvokeHelper(0xf6, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetServerFilter(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xf6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetCaption()
{
  CString result;
  InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetCaption(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x11, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Report::GetWidth()
{
  short result;
  InvokeHelper(0x96, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x96, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Report::GetPicture()
{
  CString result;
  InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetPicture(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Report::GetPictureTiling()
{
  BOOL result;
  InvokeHelper(0x139, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetPictureTiling(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x139, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Report::GetMenuBar()
{
  CString result;
  InvokeHelper(0xf0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xf0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetToolbar()
{
  CString result;
  InvokeHelper(0x161, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetToolbar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x161, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Report::GetGridX()
{
  short result;
  InvokeHelper(0x29, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetGridX(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x29, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetGridY()
{
  short result;
  InvokeHelper(0x2a, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetGridY(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x2a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Report::GetLayoutForPrint()
{
  BOOL result;
  InvokeHelper(0x8f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetLayoutForPrint(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Report::GetFastLaserPrinting()
{
  BOOL result;
  InvokeHelper(0x90, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetFastLaserPrinting(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x90, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Report::GetHelpFile()
{
  CString result;
  InvokeHelper(0xda, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetHelpFile(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0xda, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Report::GetHelpContextId()
{
  long result;
  InvokeHelper(0xdb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetHelpContextId(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xdb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Report::GetHwnd()
{
  long result;
  InvokeHelper(0xc0, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetHwnd(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xc0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetCount()
{
  short result;
  InvokeHelper(0xc3, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetCount(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xc3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Report::GetPage()
{
  long result;
  InvokeHelper(0xc4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetPage(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xc4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetPages()
{
  short result;
  InvokeHelper(0xc5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetPages(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xc5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Report::GetHasData()
{
  long result;
  InvokeHelper(0xca, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetHasData(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xca, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Report::GetLeft()
{
  long result;
  InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetLeft(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Report::GetTop()
{
  long result;
  InvokeHelper(0x8d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetTop(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Report::GetHeight()
{
  long result;
  InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetHeight(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x2c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Report::GetPrintSection()
{
  BOOL result;
  InvokeHelper(0xe1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetPrintSection(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xe1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Report::GetNextRecord()
{
  BOOL result;
  InvokeHelper(0xe2, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetNextRecord(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xe2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Report::GetMoveLayout()
{
  BOOL result;
  InvokeHelper(0xe3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetMoveLayout(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xe3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

short _Report::GetFormatCount()
{
  short result;
  InvokeHelper(0xe4, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetFormatCount(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xe4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetPrintCount()
{
  short result;
  InvokeHelper(0xe5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetPrintCount(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xe5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _Report::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Report::GetPainting()
{
  BOOL result;
  InvokeHelper(0xf1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetPainting(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0xf1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

VARIANT _Report::GetPrtMip()
{
  VARIANT result;
  InvokeHelper(0xbd, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Report::SetPrtMip(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

VARIANT _Report::GetPrtDevMode()
{
  VARIANT result;
  InvokeHelper(0xbe, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Report::SetPrtDevMode(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

VARIANT _Report::GetPrtDevNames()
{
  VARIANT result;
  InvokeHelper(0xbf, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Report::SetPrtDevNames(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

long _Report::GetForeColor()
{
  long result;
  InvokeHelper(0xcc, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetForeColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xcc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

float _Report::GetCurrentX()
{
  float result;
  InvokeHelper(0xcd, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
  return result;
}

void _Report::SetCurrentX(float newValue)
{
  static BYTE parms[] =
    VTS_R4;
  InvokeHelper(0xcd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     newValue);
}

float _Report::GetCurrentY()
{
  float result;
  InvokeHelper(0xce, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
  return result;
}

void _Report::SetCurrentY(float newValue)
{
  static BYTE parms[] =
    VTS_R4;
  InvokeHelper(0xce, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     newValue);
}

float _Report::GetScaleHeight()
{
  float result;
  InvokeHelper(0xcf, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
  return result;
}

void _Report::SetScaleHeight(float newValue)
{
  static BYTE parms[] =
    VTS_R4;
  InvokeHelper(0xcf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     newValue);
}

float _Report::GetScaleLeft()
{
  float result;
  InvokeHelper(0xd0, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
  return result;
}

void _Report::SetScaleLeft(float newValue)
{
  static BYTE parms[] =
    VTS_R4;
  InvokeHelper(0xd0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     newValue);
}

short _Report::GetScaleMode()
{
  short result;
  InvokeHelper(0xd1, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetScaleMode(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xd1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

float _Report::GetScaleTop()
{
  float result;
  InvokeHelper(0xd2, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
  return result;
}

void _Report::SetScaleTop(float newValue)
{
  static BYTE parms[] =
    VTS_R4;
  InvokeHelper(0xd2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     newValue);
}

float _Report::GetScaleWidth()
{
  float result;
  InvokeHelper(0xd3, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
  return result;
}

void _Report::SetScaleWidth(float newValue)
{
  static BYTE parms[] =
    VTS_R4;
  InvokeHelper(0xd3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     newValue);
}

short _Report::GetFontBold()
{
  short result;
  InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetFontBold(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x20, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetFontItalic()
{
  short result;
  InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetFontItalic(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x21, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Report::GetFontName()
{
  CString result;
  InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetFontName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x22, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

short _Report::GetFontSize()
{
  short result;
  InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetFontSize(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetFontUnderline()
{
  short result;
  InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetFontUnderline(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x24, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetDrawMode()
{
  short result;
  InvokeHelper(0xd4, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetDrawMode(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xd4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetDrawStyle()
{
  short result;
  InvokeHelper(0xd5, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetDrawStyle(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xd5, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetDrawWidth()
{
  short result;
  InvokeHelper(0xd6, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetDrawWidth(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xd6, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _Report::GetFillColor()
{
  long result;
  InvokeHelper(0xd8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetFillColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0xd8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

short _Report::GetFillStyle()
{
  short result;
  InvokeHelper(0xd7, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void _Report::SetFillStyle(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0xd7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _Report::GetPaletteSource()
{
  CString result;
  InvokeHelper(0x62, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetPaletteSource(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x62, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

VARIANT _Report::GetPaintPalette()
{
  VARIANT result;
  InvokeHelper(0x64, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Report::SetPaintPalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x64, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

CString _Report::GetOnMenu()
{
  CString result;
  InvokeHelper(0x3c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetOnMenu(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x3c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetOnOpen()
{
  CString result;
  InvokeHelper(0x4d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetOnOpen(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x4d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetOnClose()
{
  CString result;
  InvokeHelper(0x4e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetOnClose(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x4e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetOnActivate()
{
  CString result;
  InvokeHelper(0x70, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetOnActivate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x70, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetOnDeactivate()
{
  CString result;
  InvokeHelper(0x71, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetOnDeactivate(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x71, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetOnNoData()
{
  CString result;
  InvokeHelper(0x79, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetOnNoData(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x79, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetOnPage()
{
  CString result;
  InvokeHelper(0x53, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetOnPage(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x53, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

CString _Report::GetOnError()
{
  CString result;
  InvokeHelper(0x77, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetOnError(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x77, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Report::GetDirty()
{
  BOOL result;
  InvokeHelper(0x11e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetDirty(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x11e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

long _Report::GetCurrentRecord()
{
  long result;
  InvokeHelper(0x12e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Report::SetCurrentRecord(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x12e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

VARIANT _Report::GetPictureData()
{
  VARIANT result;
  InvokeHelper(0xbc, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Report::SetPictureData(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0xbc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

VARIANT _Report::GetPicturePalette()
{
  VARIANT result;
  InvokeHelper(0x137, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void _Report::SetPicturePalette(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x137, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}

BOOL _Report::GetHasModule()
{
  BOOL result;
  InvokeHelper(0x156, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Report::SetHasModule(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x156, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _Report::GetInputParameters()
{
  CString result;
  InvokeHelper(0x42, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetInputParameters(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x42, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

LPDISPATCH _Report::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Report::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Report::GetActiveControl()
{
  LPDISPATCH result;
  InvokeHelper(0x7d6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Report::GetDefaultControl(long ControlType)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x866, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    ControlType);
  return result;
}

void _Report::Circle(short flags, float X, float Y, float radius, long color, float start, float end, float aspect)
{
  static BYTE parms[] =
    VTS_I2 VTS_R4 VTS_R4 VTS_R4 VTS_I4 VTS_R4 VTS_R4 VTS_R4;
  InvokeHelper(0x846, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     flags, X, Y, radius, color, start, end, aspect);
}

void _Report::Line(short flags, float x1, float y1, float x2, float y2, long color)
{
  static BYTE parms[] =
    VTS_I2 VTS_R4 VTS_R4 VTS_R4 VTS_R4 VTS_I4;
  InvokeHelper(0x868, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     flags, x1, y1, x2, y2, color);
}

void _Report::PSet(short flags, float X, float Y, long color)
{
  static BYTE parms[] =
    VTS_I2 VTS_R4 VTS_R4 VTS_I4;
  InvokeHelper(0x845, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     flags, X, Y, color);
}

void _Report::Scale(short flags, float x1, float y1, float x2, float y2)
{
  static BYTE parms[] =
    VTS_I2 VTS_R4 VTS_R4 VTS_R4 VTS_R4;
  InvokeHelper(0x844, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     flags, x1, y1, x2, y2);
}

float _Report::TextWidth(LPCTSTR Expr)
{
  float result;
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x842, DISPATCH_METHOD, VT_R4, (void*)&result, parms,
    Expr);
  return result;
}

float _Report::TextHeight(LPCTSTR Expr)
{
  float result;
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x843, DISPATCH_METHOD, VT_R4, (void*)&result, parms,
    Expr);
  return result;
}

void _Report::Print(LPCTSTR Expr)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x869, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Expr);
}

LPDISPATCH _Report::GetSection(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x828, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

LPDISPATCH _Report::GetGroupLevel(long Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x832, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    Index);
  return result;
}

LPDISPATCH _Report::GetReport()
{
  LPDISPATCH result;
  InvokeHelper(0x831, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Report::GetModule()
{
  LPDISPATCH result;
  InvokeHelper(0x82a, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Report::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Report::GetControls()
{
  LPDISPATCH result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Report::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Report::SetName(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// Reports properties

/////////////////////////////////////////////////////////////////////////////
// Reports operations

LPDISPATCH Reports::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Reports::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Reports::GetItem(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

long Reports::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// Screen properties

/////////////////////////////////////////////////////////////////////////////
// Screen operations

LPDISPATCH Screen::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Screen::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Screen::GetActiveDatasheet()
{
  LPDISPATCH result;
  InvokeHelper(0x862, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Screen::GetActiveControl()
{
  LPDISPATCH result;
  InvokeHelper(0x7d6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Screen::GetPreviousControl()
{
  LPDISPATCH result;
  InvokeHelper(0x7d7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Screen::GetActiveForm()
{
  LPDISPATCH result;
  InvokeHelper(0x7d8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH Screen::GetActiveReport()
{
  LPDISPATCH result;
  InvokeHelper(0x7d9, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

short Screen::GetMousePointer()
{
  short result;
  InvokeHelper(0x83a, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

void Screen::SetMousePointer(short nNewValue)
{
  static BYTE parms[] =
    VTS_I2;
  InvokeHelper(0x83a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

LPDISPATCH Screen::GetActiveDataAccessPage()
{
  LPDISPATCH result;
  InvokeHelper(0x8a6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _Application properties

/////////////////////////////////////////////////////////////////////////////
// _Application operations

LPDISPATCH _Application::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetCodeContextObject()
{
  LPDISPATCH result;
  InvokeHelper(0x822, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Application::NewCurrentDatabase(LPCTSTR filepath)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x85e, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     filepath);
}

void _Application::OpenCurrentDatabase(LPCTSTR filepath, BOOL Exclusive)
{
  static BYTE parms[] =
    VTS_BSTR VTS_BOOL;
  InvokeHelper(0x85c, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     filepath, Exclusive);
}

CString _Application::GetMenuBar()
{
  CString result;
  InvokeHelper(0x7da, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Application::SetMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7da, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _Application::GetCurrentObjectType()
{
  long result;
  InvokeHelper(0x7db, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

CString _Application::GetCurrentObjectName()
{
  CString result;
  InvokeHelper(0x7dc, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

VARIANT _Application::GetOption(LPCTSTR OptionName)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7dd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    OptionName);
  return result;
}

void _Application::SetOption(LPCTSTR OptionName, const VARIANT& Setting)
{
  static BYTE parms[] =
    VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7de, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     OptionName, &Setting);
}

void _Application::Echo(short EchoOn, LPCTSTR bstrStatusBarText)
{
  static BYTE parms[] =
    VTS_I2 VTS_BSTR;
  InvokeHelper(0x7df, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     EchoOn, bstrStatusBarText);
}

void _Application::CloseCurrentDatabase()
{
  InvokeHelper(0x85d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Application::Quit(long Option)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x7e0, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Option);
}

LPDISPATCH _Application::GetForms()
{
  LPDISPATCH result;
  InvokeHelper(0x7d2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetReports()
{
  LPDISPATCH result;
  InvokeHelper(0x7d3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetScreen()
{
  LPDISPATCH result;
  InvokeHelper(0x7d4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetDoCmd()
{
  LPDISPATCH result;
  InvokeHelper(0x7e9, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Application::GetShortcutMenuBar()
{
  CString result;
  InvokeHelper(0x83b, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _Application::SetShortcutMenuBar(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x83b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

BOOL _Application::GetVisible()
{
  BOOL result;
  InvokeHelper(0x864, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Application::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x864, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _Application::GetUserControl()
{
  BOOL result;
  InvokeHelper(0x865, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Application::SetUserControl(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x865, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

VARIANT _Application::SysCmd(long Action, const VARIANT& Argument2, const VARIANT& Argument3)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_I4 VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x7ec, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Action, &Argument2, &Argument3);
  return result;
}

LPDISPATCH _Application::CreateForm(const VARIANT& Database, const VARIANT& FormTemplate)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x7ed, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    &Database, &FormTemplate);
  return result;
}

LPDISPATCH _Application::CreateReport(const VARIANT& Database, const VARIANT& ReportTemplate)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x7ee, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    &Database, &ReportTemplate);
  return result;
}

LPDISPATCH _Application::CreateControl(LPCTSTR FormName, long ControlType, long Section, const VARIANT& Parent, const VARIANT& ColumnName, const VARIANT& Left, const VARIANT& Top, const VARIANT& Width, const VARIANT& Height)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_BSTR VTS_I4 VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x7ef, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    FormName, ControlType, Section, &Parent, &ColumnName, &Left, &Top, &Width, &Height);
  return result;
}

LPDISPATCH _Application::CreateReportControl(LPCTSTR ReportName, long ControlType, long Section, const VARIANT& Parent, const VARIANT& ColumnName, const VARIANT& Left, const VARIANT& Top, const VARIANT& Width, const VARIANT& Height)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_BSTR VTS_I4 VTS_I4 VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x7f0, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    ReportName, ControlType, Section, &Parent, &ColumnName, &Left, &Top, &Width, &Height);
  return result;
}

void _Application::DeleteControl(LPCTSTR FormName, LPCTSTR ControlName)
{
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR;
  InvokeHelper(0x7f1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     FormName, ControlName);
}

void _Application::DeleteReportControl(LPCTSTR ReportName, LPCTSTR ControlName)
{
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR;
  InvokeHelper(0x7f2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ReportName, ControlName);
}

long _Application::CreateGroupLevel(LPCTSTR ReportName, LPCTSTR Expression, short Header, short Footer)
{
  long result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_I2 VTS_I2;
  InvokeHelper(0x803, DISPATCH_METHOD, VT_I4, (void*)&result, parms,
    ReportName, Expression, Header, Footer);
  return result;
}

VARIANT _Application::DMin(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7f3, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DMax(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7f4, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DSum(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7f5, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DAvg(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7f6, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DLookup(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7f7, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DLast(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7f8, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DVar(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7f9, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DVarP(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7fa, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DStDev(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7fb, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DStDevP(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7fc, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DFirst(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7fd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::DCount(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x7fe, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Expr, Domain, &Criteria);
  return result;
}

VARIANT _Application::Eval(LPCTSTR StringExpr)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x7ff, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    StringExpr);
  return result;
}

CString _Application::CurrentUser()
{
  CString result;
  InvokeHelper(0x800, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
  return result;
}

VARIANT _Application::DDEInitiate(LPCTSTR Application, LPCTSTR Topic)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR;
  InvokeHelper(0x804, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Application, Topic);
  return result;
}

void _Application::DDEExecute(const VARIANT& ChanNum, LPCTSTR Command)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_BSTR;
  InvokeHelper(0x805, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ChanNum, Command);
}

void _Application::DDEPoke(const VARIANT& ChanNum, LPCTSTR Item, LPCTSTR Data)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_BSTR VTS_BSTR;
  InvokeHelper(0x806, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ChanNum, Item, Data);
}

CString _Application::DDERequest(const VARIANT& ChanNum, LPCTSTR Item)
{
  CString result;
  static BYTE parms[] =
    VTS_VARIANT VTS_BSTR;
  InvokeHelper(0x807, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
    &ChanNum, Item);
  return result;
}

void _Application::DDETerminate(const VARIANT& ChanNum)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x808, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &ChanNum);
}

void _Application::DDETerminateAll()
{
  InvokeHelper(0x809, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Application::GetDBEngine()
{
  LPDISPATCH result;
  InvokeHelper(0x83e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::CurrentDb()
{
  LPDISPATCH result;
  InvokeHelper(0x801, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::CodeDb()
{
  LPDISPATCH result;
  InvokeHelper(0x802, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Application::BuildCriteria(LPCTSTR Field, short FieldType, LPCTSTR Expression)
{
  CString result;
  static BYTE parms[] =
    VTS_BSTR VTS_I2 VTS_BSTR;
  InvokeHelper(0x85a, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
    Field, FieldType, Expression);
  return result;
}

LPDISPATCH _Application::DefaultWorkspaceClone()
{
  LPDISPATCH result;
  InvokeHelper(0x86d, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Application::RefreshTitleBar()
{
  InvokeHelper(0x86e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

long _Application::hWndAccessApp()
{
  long result;
  InvokeHelper(0x873, DISPATCH_METHOD, VT_I4, (void*)&result, NULL);
  return result;
}

VARIANT _Application::Run(LPCTSTR Procedure, VARIANT* Arg1, VARIANT* Arg2, VARIANT* Arg3, VARIANT* Arg4, VARIANT* Arg5, VARIANT* Arg6, VARIANT* Arg7, VARIANT* Arg8, VARIANT* Arg9, VARIANT* Arg10, VARIANT* Arg11, VARIANT* Arg12, VARIANT* Arg13, 
    VARIANT* Arg14, VARIANT* Arg15, VARIANT* Arg16, VARIANT* Arg17, VARIANT* Arg18, VARIANT* Arg19, VARIANT* Arg20, VARIANT* Arg21, VARIANT* Arg22, VARIANT* Arg23, VARIANT* Arg24, VARIANT* Arg25, VARIANT* Arg26, VARIANT* Arg27, 
    VARIANT* Arg28, VARIANT* Arg29, VARIANT* Arg30)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_BSTR VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT 
    VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT;
  InvokeHelper(0x856, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    Procedure, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9, Arg10, Arg11, Arg12, Arg13, Arg14, Arg15, Arg16, Arg17, Arg18, Arg19, Arg20, Arg21, Arg22, Arg23, Arg24, Arg25, Arg26, Arg27, Arg28, Arg29, Arg30);
  return result;
}

VARIANT _Application::Nz(const VARIANT& Value, const VARIANT& ValueIfNull)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x857, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    &Value, &ValueIfNull);
  return result;
}

LPDISPATCH _Application::LoadPicture(LPCTSTR FileName)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x876, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    FileName);
  return result;
}

VARIANT _Application::AccessError(const VARIANT& ErrorNumber)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x879, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    &ErrorNumber);
  return result;
}

VARIANT _Application::StringFromGUID(const VARIANT& Guid)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x87b, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    &Guid);
  return result;
}

VARIANT _Application::GUIDFromString(const VARIANT& String)
{
  VARIANT result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x87c, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms,
    &String);
  return result;
}

LPDISPATCH _Application::GetCommandBars()
{
  LPDISPATCH result;
  InvokeHelper(0x87e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetAssistant()
{
  LPDISPATCH result;
  InvokeHelper(0x87f, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Application::FollowHyperlink(LPCTSTR Address, LPCTSTR SubAddress, BOOL NewWindow, BOOL AddHistory, const VARIANT& ExtraInfo, long Method, LPCTSTR HeaderInfo)
{
  static BYTE parms[] =
    VTS_BSTR VTS_BSTR VTS_BOOL VTS_BOOL VTS_VARIANT VTS_I4 VTS_BSTR;
  InvokeHelper(0x880, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Address, SubAddress, NewWindow, AddHistory, &ExtraInfo, Method, HeaderInfo);
}

void _Application::AddToFavorites()
{
  InvokeHelper(0x888, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

void _Application::RefreshDatabaseWindow()
{
  InvokeHelper(0x88a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

LPDISPATCH _Application::GetReferences()
{
  LPDISPATCH result;
  InvokeHelper(0x88b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetModules()
{
  LPDISPATCH result;
  InvokeHelper(0x88c, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetFileSearch()
{
  LPDISPATCH result;
  InvokeHelper(0x88d, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

BOOL _Application::GetIsCompiled()
{
  BOOL result;
  InvokeHelper(0x891, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _Application::RunCommand(long Command)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x892, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Command);
}

CString _Application::HyperlinkPart(const VARIANT& Hyperlink, long Part)
{
  CString result;
  static BYTE parms[] =
    VTS_VARIANT VTS_I4;
  InvokeHelper(0x893, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms,
    &Hyperlink, Part);
  return result;
}

BOOL _Application::GetHiddenAttribute(long ObjectType, LPCTSTR ObjectName)
{
  BOOL result;
  static BYTE parms[] =
    VTS_I4 VTS_BSTR;
  InvokeHelper(0x895, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms,
    ObjectType, ObjectName);
  return result;
}

void _Application::SetHiddenAttribute(long ObjectType, LPCTSTR ObjectName, BOOL fHidden)
{
  static BYTE parms[] =
    VTS_I4 VTS_BSTR VTS_BOOL;
  InvokeHelper(0x896, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ObjectType, ObjectName, fHidden);
}

LPDISPATCH _Application::GetVbe()
{
  LPDISPATCH result;
  InvokeHelper(0x8a2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetDataAccessPages()
{
  LPDISPATCH result;
  InvokeHelper(0x8a4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::CreateDataAccessPage(const VARIANT& FileName, BOOL CreateNewFile)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT VTS_BOOL;
  InvokeHelper(0x8a5, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    &FileName, CreateNewFile);
  return result;
}

LPDISPATCH _Application::GetCurrentProject()
{
  LPDISPATCH result;
  InvokeHelper(0x8a7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetCurrentData()
{
  LPDISPATCH result;
  InvokeHelper(0x8a8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetCodeProject()
{
  LPDISPATCH result;
  InvokeHelper(0x8a9, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetCodeData()
{
  LPDISPATCH result;
  InvokeHelper(0x8aa, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _Application::NewAccessProject(LPCTSTR filepath, const VARIANT& Connect)
{
  static BYTE parms[] =
    VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x8d6, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     filepath, &Connect);
}

void _Application::OpenAccessProject(LPCTSTR filepath, BOOL Exclusive)
{
  static BYTE parms[] =
    VTS_BSTR VTS_BOOL;
  InvokeHelper(0x8d7, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     filepath, Exclusive);
}

void _Application::CreateAccessProject(LPCTSTR filepath, const VARIANT& Connect)
{
  static BYTE parms[] =
    VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x8d8, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     filepath, &Connect);
}

CString _Application::GetProductCode()
{
  CString result;
  InvokeHelper(0x8da, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetCOMAddIns()
{
  LPDISPATCH result;
  InvokeHelper(0x8e5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _Application::GetName()
{
  CString result;
  InvokeHelper(0x82e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetDefaultWebOptions()
{
  LPDISPATCH result;
  InvokeHelper(0x8e6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetLanguageSettings()
{
  LPDISPATCH result;
  InvokeHelper(0x8fd, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _Application::GetAnswerWizard()
{
  LPDISPATCH result;
  InvokeHelper(0x900, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

long _Application::GetFeatureInstall()
{
  long result;
  InvokeHelper(0x912, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _Application::SetFeatureInstall(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x912, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

double _Application::EuroConvert(double Number, LPCTSTR SourceCurrency, LPCTSTR TargetCurrency, const VARIANT& FullPrecision, const VARIANT& TriangulationPrecision)
{
  double result;
  static BYTE parms[] =
    VTS_R8 VTS_BSTR VTS_BSTR VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x915, DISPATCH_METHOD, VT_R8, (void*)&result, parms,
    Number, SourceCurrency, TargetCurrency, &FullPrecision, &TriangulationPrecision);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// Reference properties

/////////////////////////////////////////////////////////////////////////////
// Reference operations

LPDISPATCH Reference::GetCollection()
{
  LPDISPATCH result;
  InvokeHelper(0x60020000, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString Reference::GetName()
{
  CString result;
  InvokeHelper(0x60020001, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

CString Reference::GetGuid()
{
  CString result;
  InvokeHelper(0x60020002, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

long Reference::GetMajor()
{
  long result;
  InvokeHelper(0x60020003, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

long Reference::GetMinor()
{
  long result;
  InvokeHelper(0x60020004, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

CString Reference::GetFullPath()
{
  CString result;
  InvokeHelper(0x60020005, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

BOOL Reference::GetBuiltIn()
{
  BOOL result;
  InvokeHelper(0x60020006, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

BOOL Reference::GetIsBroken()
{
  BOOL result;
  InvokeHelper(0x60020007, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

long Reference::GetKind()
{
  long result;
  InvokeHelper(0x60020008, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _References properties

/////////////////////////////////////////////////////////////////////////////
// _References operations

LPDISPATCH _References::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x60020000, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _References::Item(const VARIANT& var)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    &var);
  return result;
}

long _References::GetCount()
{
  long result;
  InvokeHelper(0x60020002, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

LPUNKNOWN _References::_NewEnum()
{
  LPUNKNOWN result;
  InvokeHelper(0xfffffffc, DISPATCH_METHOD, VT_UNKNOWN, (void*)&result, NULL);
  return result;
}

LPDISPATCH _References::AddFromGuid(LPCTSTR Guid, long Major, long Minor)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_BSTR VTS_I4 VTS_I4;
  InvokeHelper(0x60020004, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    Guid, Major, Minor);
  return result;
}

LPDISPATCH _References::AddFromFile(LPCTSTR FileName)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x60020005, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
    FileName);
  return result;
}

void _References::Remove(LPDISPATCH Reference)
{
  static BYTE parms[] =
    VTS_DISPATCH;
  InvokeHelper(0x60020006, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Reference);
}


/////////////////////////////////////////////////////////////////////////////
// _References_Events properties

/////////////////////////////////////////////////////////////////////////////
// _References_Events operations

void _References_Events::ItemAdded(LPDISPATCH Reference)
{
  static BYTE parms[] =
    VTS_DISPATCH;
  InvokeHelper(0x0, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Reference);
}

void _References_Events::ItemRemoved(LPDISPATCH Reference)
{
  static BYTE parms[] =
    VTS_DISPATCH;
  InvokeHelper(0x1, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     Reference);
}


/////////////////////////////////////////////////////////////////////////////
// _Dummy properties

/////////////////////////////////////////////////////////////////////////////
// _Dummy operations


/////////////////////////////////////////////////////////////////////////////
// _DataAccessPage properties

/////////////////////////////////////////////////////////////////////////////
// _DataAccessPage operations

CString _DataAccessPage::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

BOOL _DataAccessPage::GetVisible()
{
  BOOL result;
  InvokeHelper(0x94, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _DataAccessPage::SetVisible(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x94, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _DataAccessPage::GetTag()
{
  CString result;
  InvokeHelper(0x10a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _DataAccessPage::SetTag(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x10a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _DataAccessPage::GetWindowWidth()
{
  long result;
  InvokeHelper(0x11f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

long _DataAccessPage::GetWindowHeight()
{
  long result;
  InvokeHelper(0x120, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

short _DataAccessPage::GetCurrentView()
{
  short result;
  InvokeHelper(0x121, DISPATCH_PROPERTYGET, VT_I2, (void*)&result, NULL);
  return result;
}

LPDISPATCH _DataAccessPage::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _DataAccessPage::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _DataAccessPage::GetDocument()
{
  LPDISPATCH result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

void _DataAccessPage::ApplyTheme(LPCTSTR ThemeName)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x8d2, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     ThemeName);
}

LPDISPATCH _DataAccessPage::GetWebOptions()
{
  LPDISPATCH result;
  InvokeHelper(0x8f5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString _DataAccessPage::GetConnectionString()
{
  CString result;
  InvokeHelper(0x8dc, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _DataAccessPage::SetConnectionString(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x8dc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}


/////////////////////////////////////////////////////////////////////////////
// DataAccessPages properties

/////////////////////////////////////////////////////////////////////////////
// DataAccessPages operations

LPDISPATCH DataAccessPages::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH DataAccessPages::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH DataAccessPages::GetItem(const VARIANT& var)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &var);
  return result;
}

long DataAccessPages::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// AllObjects properties

/////////////////////////////////////////////////////////////////////////////
// AllObjects operations

LPDISPATCH AllObjects::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH AllObjects::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH AllObjects::GetItem(const VARIANT& var)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &var);
  return result;
}

long AllObjects::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// AccessObjectProperty properties

/////////////////////////////////////////////////////////////////////////////
// AccessObjectProperty operations

CString AccessObjectProperty::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

VARIANT AccessObjectProperty::GetValue()
{
  VARIANT result;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
  return result;
}

void AccessObjectProperty::SetValue(const VARIANT& newValue)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     &newValue);
}


/////////////////////////////////////////////////////////////////////////////
// AccessObjectProperties properties

/////////////////////////////////////////////////////////////////////////////
// AccessObjectProperties operations

LPDISPATCH AccessObjectProperties::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH AccessObjectProperties::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH AccessObjectProperties::GetItem(const VARIANT& Index)
{
  LPDISPATCH result;
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms,
    &Index);
  return result;
}

long AccessObjectProperties::GetCount()
{
  long result;
  InvokeHelper(0x7d1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void AccessObjectProperties::Add(LPCTSTR PropertyName, const VARIANT& Value)
{
  static BYTE parms[] =
    VTS_BSTR VTS_VARIANT;
  InvokeHelper(0x88f, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     PropertyName, &Value);
}

void AccessObjectProperties::Remove(const VARIANT& Item)
{
  static BYTE parms[] =
    VTS_VARIANT;
  InvokeHelper(0x890, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &Item);
}


/////////////////////////////////////////////////////////////////////////////
// _CurrentProject properties

/////////////////////////////////////////////////////////////////////////////
// _CurrentProject operations

LPDISPATCH _CurrentProject::GetAllForms()
{
  LPDISPATCH result;
  InvokeHelper(0x8ab, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentProject::GetAllReports()
{
  LPDISPATCH result;
  InvokeHelper(0x8ac, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentProject::GetAllMacros()
{
  LPDISPATCH result;
  InvokeHelper(0x8ad, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentProject::GetAllModules()
{
  LPDISPATCH result;
  InvokeHelper(0x8ae, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentProject::GetAllDataAccessPages()
{
  LPDISPATCH result;
  InvokeHelper(0x8b0, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

long _CurrentProject::GetProjectType()
{
  long result;
  InvokeHelper(0x8df, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

CString _CurrentProject::GetBaseConnectionString()
{
  CString result;
  InvokeHelper(0x8db, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

BOOL _CurrentProject::GetIsConnected()
{
  BOOL result;
  InvokeHelper(0x8dd, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _CurrentProject::OpenConnection(const VARIANT& BaseConnectionString, const VARIANT& UserID, const VARIANT& Password)
{
  static BYTE parms[] =
    VTS_VARIANT VTS_VARIANT VTS_VARIANT;
  InvokeHelper(0x8e0, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
     &BaseConnectionString, &UserID, &Password);
}

void _CurrentProject::CloseConnection()
{
  InvokeHelper(0x8de, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}

CString _CurrentProject::GetName()
{
  CString result;
  InvokeHelper(0x82e, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

CString _CurrentProject::GetPath()
{
  CString result;
  InvokeHelper(0x8e3, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

CString _CurrentProject::GetFullName()
{
  CString result;
  InvokeHelper(0x8d4, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentProject::GetConnection()
{
  LPDISPATCH result;
  InvokeHelper(0x8e2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentProject::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentProject::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentProject::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _CurrentData properties

/////////////////////////////////////////////////////////////////////////////
// _CurrentData operations

LPDISPATCH _CurrentData::GetAllTables()
{
  LPDISPATCH result;
  InvokeHelper(0x8b1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentData::GetAllQueries()
{
  LPDISPATCH result;
  InvokeHelper(0x8b2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentData::GetAllViews()
{
  LPDISPATCH result;
  InvokeHelper(0x8b4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentData::GetAllStoredProcedures()
{
  LPDISPATCH result;
  InvokeHelper(0x8b5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _CurrentData::GetAllDatabaseDiagrams()
{
  LPDISPATCH result;
  InvokeHelper(0x8b6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// AccessObject properties

/////////////////////////////////////////////////////////////////////////////
// AccessObject operations

LPDISPATCH AccessObject::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

CString AccessObject::GetName()
{
  CString result;
  InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

long AccessObject::GetType()
{
  long result;
  InvokeHelper(0x82f, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

LPDISPATCH AccessObject::GetProperties()
{
  LPDISPATCH result;
  InvokeHelper(0x82b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

BOOL AccessObject::GetIsLoaded()
{
  BOOL result;
  InvokeHelper(0x8e4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

CString AccessObject::GetFullName()
{
  CString result;
  InvokeHelper(0x8d4, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _WizHook properties

/////////////////////////////////////////////////////////////////////////////
// _WizHook operations


/////////////////////////////////////////////////////////////////////////////
// _DefaultWebOptions properties

/////////////////////////////////////////////////////////////////////////////
// _DefaultWebOptions operations

LPDISPATCH _DefaultWebOptions::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _DefaultWebOptions::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

long _DefaultWebOptions::GetHyperlinkColor()
{
  long result;
  InvokeHelper(0x8e7, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetHyperlinkColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8e7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

long _DefaultWebOptions::GetFollowedHyperlinkColor()
{
  long result;
  InvokeHelper(0x8e8, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetFollowedHyperlinkColor(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8e8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _DefaultWebOptions::GetUnderlineHyperlinks()
{
  BOOL result;
  InvokeHelper(0x8e9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetUnderlineHyperlinks(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8e9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _DefaultWebOptions::GetOrganizeInFolder()
{
  BOOL result;
  InvokeHelper(0x8ec, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetOrganizeInFolder(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8ec, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _DefaultWebOptions::GetUseLongFileNames()
{
  BOOL result;
  InvokeHelper(0x8ed, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetUseLongFileNames(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8ed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _DefaultWebOptions::GetCheckIfOfficeIsHTMLEditor()
{
  BOOL result;
  InvokeHelper(0x8ee, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetCheckIfOfficeIsHTMLEditor(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8ee, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _DefaultWebOptions::GetDownloadComponents()
{
  BOOL result;
  InvokeHelper(0x8ef, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetDownloadComponents(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8ef, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _DefaultWebOptions::GetLocationOfComponents()
{
  CString result;
  InvokeHelper(0x8f0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetLocationOfComponents(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x8f0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _DefaultWebOptions::GetEncoding()
{
  long result;
  InvokeHelper(0x8f1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetEncoding(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8f1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

BOOL _DefaultWebOptions::GetAlwaysSaveInDefaultEncoding()
{
  BOOL result;
  InvokeHelper(0x8f2, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _DefaultWebOptions::SetAlwaysSaveInDefaultEncoding(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8f2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _DefaultWebOptions::GetFolderSuffix()
{
  CString result;
  InvokeHelper(0x903, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}


/////////////////////////////////////////////////////////////////////////////
// _WebOptions properties

/////////////////////////////////////////////////////////////////////////////
// _WebOptions operations

LPDISPATCH _WebOptions::GetApplication()
{
  LPDISPATCH result;
  InvokeHelper(0x7d5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

LPDISPATCH _WebOptions::GetParent()
{
  LPDISPATCH result;
  InvokeHelper(0x827, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
  return result;
}

BOOL _WebOptions::GetOrganizeInFolder()
{
  BOOL result;
  InvokeHelper(0x8ec, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _WebOptions::SetOrganizeInFolder(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8ec, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _WebOptions::GetUseLongFileNames()
{
  BOOL result;
  InvokeHelper(0x8ed, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _WebOptions::SetUseLongFileNames(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8ed, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

BOOL _WebOptions::GetDownloadComponents()
{
  BOOL result;
  InvokeHelper(0x8ef, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
  return result;
}

void _WebOptions::SetDownloadComponents(BOOL bNewValue)
{
  static BYTE parms[] =
    VTS_BOOL;
  InvokeHelper(0x8ef, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     bNewValue);
}

CString _WebOptions::GetLocationOfComponents()
{
  CString result;
  InvokeHelper(0x8f0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _WebOptions::SetLocationOfComponents(LPCTSTR lpszNewValue)
{
  static BYTE parms[] =
    VTS_BSTR;
  InvokeHelper(0x8f0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     lpszNewValue);
}

long _WebOptions::GetEncoding()
{
  long result;
  InvokeHelper(0x8f1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
  return result;
}

void _WebOptions::SetEncoding(long nNewValue)
{
  static BYTE parms[] =
    VTS_I4;
  InvokeHelper(0x8f1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms,
     nNewValue);
}

CString _WebOptions::GetFolderSuffix()
{
  CString result;
  InvokeHelper(0x903, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
  return result;
}

void _WebOptions::UseDefaultFolderSuffix()
{
  InvokeHelper(0x904, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
}
