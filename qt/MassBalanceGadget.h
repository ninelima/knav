#ifndef __MASSBALANCEGADGET_H
#define __MASSBALANCEGADGET_H

#include <qvariant.h>
#include <qdialog.h>
#include <qlineedit.h>

#include "EnvelopeWidget.h"
#include "BorderLayout.h"

class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QIconView;
class QIconViewItem;
class QLabel;
class QLineEdit;
class QPushButton;

//
// Subclass of QLineEdit to handle focus lost et al 
//
class QLineEditSubClass : public QLineEdit 
{
		Q_OBJECT

	public:
		QLineEditSubClass(QWidget *parent, const char *name=0);

	protected:
			void focusOutEvent( QFocusEvent *e );
			void focusInEvent( QFocusEvent *e );
			void keyPressEvent( QKeyEvent *e ); 
    
	signals:
		void focusIn();
		void focusOut();
	//  void textFixed();
	//  void textFixed(const QString &);
};


#define MAX_LINE 8

class MassBalanceGadget : public QWidget
{
    Q_OBJECT

public:
    int m_bFuel;
    int m_iNrPoints;
    int m_iUnits;

    MassBalanceGadget(QWidget* parent = 0, const char* name = 0, WFlags f = 0);
    ~MassBalanceGadget();

    QLabel* TextLabel1;
    QLabel* TextLabel2;
    QLabel* TextLabel3;
    QLabel* TextLabel4;
    QLabel* TextLabel5;
    QLabel* TextLabel6;
    QLabel* TextLabel7;
    QLabel* TextLabel8;

    QLabel    *item[MAX_LINE+1];
    QLabel    *arm[MAX_LINE+1];
    QLabel    *max[MAX_LINE+1];
		QLineEditSubClass *weight[MAX_LINE+1]; 

    EnvelopeWidget* m_envelopePanel;


    QPushButton* activeAircraftButton;

//private:
  void ReLoad();
  void ReCalc();


protected:
    //QVBoxLayout* MyDialogLayout;
    BorderLayout* MyDialogLayout;

    QHBoxLayout* Layout22;
    QVBoxLayout* Layout20;
    QGridLayout* Layout17;
    QHBoxLayout* Layout19;
    QHBoxLayout* Layout1;
/*    bool event( QEvent* );*/

private slots:
  void onAircraftButton();
  void _textChanged();
  void test();
  void focusOut();

signals:
  void aircraftChanged();


};

#endif // __MASSBALANCEGADGET_H
