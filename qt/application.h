/****************************************************************************
** $Id: application.h,v 1.24 2004/05/22 03:38:47 player Exp $
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#ifndef __APPLICATION_H
#define __APPLICATION_H

#include <qmainwindow.h>
#include <qlistview.h>
#include <qtextbrowser.h>
#include "utextbrowser.h"

#ifndef __GRAPHICPAINTER_H
#include "GraphicPainter.h"
#endif

#ifndef __MASSBALANCEGADGET_H
#include "MassBalanceGadget.h"
#endif


//-----------------------------------------------------------------------------
// MapWidget definition
//
class MapWidget : public QWidget
{
  Q_OBJECT

public:
  MapWidget(QWidget *parent=0, const char *name=0, WFlags f=0);

  ~MapWidget()
  {}

protected:
  void paintEvent(QPaintEvent *e);
  void resizeEvent(QResizeEvent *e);
  void mouseMoveEvent(QMouseEvent *e);
  void mousePressEvent(QMouseEvent *e);
  void mouseReleaseEvent(QMouseEvent *e);
  void wheelEvent(QWheelEvent *e);


//public:
private slots:

signals:
  void infoChanged();
  void latlonChanged();

//protected:
};



//-----------------------------------------------------------------------------
// MyListView - demo stuff
//
/*
class MyListView : public QListView
{
    Q_OBJECT
  public:

    MyListView( QWidget * parent = 0, const char *name = 0 )
      :QListView( parent, name ), selected(0)
    {}
    ~MyListView()
    {}
  protected:

    void contentsMousePressEvent( QMouseEvent * e )
    {
      selected = selectedItem(); 
      QListView::contentsMousePressEvent( e );
    }
    void contentsMouseReleaseEvent( QMouseEvent * e )
    {
      QListView::contentsMouseReleaseEvent( e );
      if ( selectedItem() != selected ) {
        emit mySelectionChanged( selectedItem() );
        emit mySelectionChanged();
      }
    }

  signals:
    void mySelectionChanged();
    void mySelectionChanged( QListViewItem* );

  private:
    QListViewItem* selected;

};
*/

//-----------------------------------------------------------------------------
// ApplicationWindow definition
//
class QMultiLineEdit;
class QToolBar;
class QPopupMenu;

class ApplicationWindow: public QMainWindow
{
    Q_OBJECT
public:
  MapWidget *pMapWidget;
  UTextBrowser *m_htmlBrowser;

  ApplicationWindow();
  ~ApplicationWindow();

  QPixmap openIcon, saveIcon, printIcon;
  createMenu();
  createToolbar();
  createWorkspace();

 
protected:
  void closeEvent(QCloseEvent *);
  void keyPressEvent(QKeyEvent *e);
  void keyReleaseEvent(QKeyEvent *e);


private:
  int m_reccnt;

  int id_relief;
  int id_rose;
  int id_noderose;
  int id_globe;
  //----------
  int id_apt;
  int id_nav;
  int id_air;
  int id_trk;

  int id_undo;
  int id_redo;

  QPopupMenu *view;// = new QPopupMenu(this);
  QPopupMenu *edit;

  void displayInfoHtml(QString sIcao); 
  void dl_all();

public: // slots:
  void fileLoad(const char *szfileName);

  void helper_print_text(GraphicPainter *p, QString filename);
  //void print_text(GraphicPainter *p);
  void helper_print_graphics(GraphicPainter *p);
  void helper_print_massbalance(GraphicPainter *p);


  //void setTitle(const QString &caption);
  void setTitle();


private slots:
  //
  // Menu slots
  //
  void fileNew();
  void fileOpen();
  //  void fileLoad(const char *fileName);
  void fileSave();
  void saveAs();
  void print();
  //-----------
  void editUndo();
  void editRedo();
  //-----------
  void zoom();
  void route();

  //-----------
  void toolbar_apt();
  void toolbar_nav();
  void toolbar_trk();
  void toolbar_air();

  //-----------
  void rose();
  void noderose();

  void globe();
  void relief();
  void radar();

  void preferences();
  void startup();
  void refresh();

  //-----------
  void zoomall();
  void zoomtrack();

  //-----------
  void massbalance();
  void e6bcomputer();
  void flightlog();
  void planninglog();
  //void flightlogTxt(GraphicPainter g);
  void flightlogTxt(QString fileName);
  void planninglogTxt(QString fileName);

  void flightlogHtml(QString fileName);
  void planninglogHtml(QString fileName);




  //-----------
  void briefing();
  void weather();
  void duats_naips();
  void flightplan();

  //-----------
  void dl_track();
  void dl_route();
  void dl_waypt();
  //-----------


  //-----------
  void about();
  void index();
  void aboutQt();

  //-----------

  // 
  // miscellaneous slots
  //
  void clickCatcher();
  void infoChanged();
  void latlonChanged();
  /*
  void latlonChanged();
  */
  //void routeTablevalueChanged();
  //void trueTablevalueChanged();
  void trueTableValueChanged(int row, int col);
  //void clicked(int row, int col, int button, const QPoint &mousePos)
  void trueTableClicked(int row, int col, int button, const QPoint &mousePos);
  void magneticTableClicked(int row, int col, int button, const QPoint &mousePos);

  //
  // Slots for actions performed on the ListView "tree"
  //
  void lv_mouseButtonClicked( int button, QListViewItem * item, const QPoint & pos, int c );
  //void lv_rightButtonClicked(QListViewItem *, const QPoint &, int);


  //
  // Popup menu slots
  //
  void autofit();
  void fixedfit();
  void popup_info();
  void popup_insert();
  void popup_delete();

  void popup_route();
  void popup_locate();
  void popup_edit();
  void popup_remove();

  void test();

  //connect(massBalanceGadget, SIGNAL(aircraftChanged()), this, SLOT(aircraftChanged()));
  void aircraftChanged();


private:
  QPrinter *printer;
  MassBalanceGadget *massBalanceGadget;

  //QMultiLineEdit *e;
  QWidget *central;

  QToolBar *fileTools;
  QToolBar *actionTools;
  QToolBar *filterTools;
  QString m_filename;

  QToolButton *zoom_button;
  QToolButton *route_button;
  QToolButton *globe_button;
  QToolButton *shade_button;

  QToolButton *radar_button;


  QToolButton *apt_button;
  QToolButton *nav_button;
  QToolButton *gpstrk_button;
  QToolButton *air_button;


  QListView *m_top;// = new QListView();
  QListViewItem *downloaddb;
  QListViewItem *wptdb;

  void createDatabaseTree();
  void createDownloadTree();

  //QListView *createDatabaseTree();
};


#endif // __APPLICATION_H
