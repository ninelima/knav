//
// $Id: TsiGadget.h,v 1.3 2005/08/05 14:37:58 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//

#include <qwidget.h>
#include <qpixmap.h>

#ifndef __GRAPHICPAINTER_H
  #include "GraphicPainter.h"
#endif // __GRAPHICPAINTER_H

#include "PanelGadget.h"

class TsiGadget : public PanelGadget //QWidget //PanelGadget
{
public:
  int m_roll;
  int hotspot; /** 80 sets hotspot to the arrow */
  QRect rect;

  TsiGadget(QWidget *parent=0, const char *name=0, WFlags f=0);

  //void paintEvent(QPaintEvent* e);

  void drawBlankFace(GraphicPainter *gc, int x, int y, int size);
  void drawFace(GraphicPainter *gc);
  
	void setRot(int rot);

  void mousePressEvent(QMouseEvent *e);
  //void mouseReleaseEvent(QMouseEvent *e);
private:
  /*
  QPixmap pixbuffer;
  int m_size;
  double m_scale;

  bool m_bMouseLeftButtonDown;
  bool m_bMouseRightButtonDown;
  */
};
