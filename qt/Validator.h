#ifndef __VALIDATOR_H
#define __VALIDATOR_H

#include <qobject.h>
#include <qwidget.h>
#include <qhttp.h>
#include <qurl.h>


class Validator : public QWidget //QObject  
{
  Q_OBJECT

  public:
    QString m_buffer;
    QString m_serverIP;
 
    Validator(QWidget *parent=0, const char *name=0, WFlags f=0);
    ~Validator();

    void sendStr(QString str);

    /*void Init();
    void Start();
    void Done();
    void setGeometry(QRect r);*/

  private slots:
    void fetchDone(bool error);

  private:
    QSocket *m_socket;
    QHttp *m_articleFetcher;

  signals:
    void done();
};


#endif // __VALIDATOR_H
