#ifndef __GENMAPWND_H
#define __GENMAPWND_H

#ifndef __GRAPHICPAINTER_H
#include "GraphicPainter.h"
#endif

#ifndef _SHAPEFILE_H_INCLUDED
#include "shapefil.h"
#endif


#ifndef _MOTIF_
#include <qstring.h>
#include <qimage.h>
#endif

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


#ifndef _MOTIF_
//
// A very simple brute force cache
//
#define IMG_CACHE 512
class CacheObject
{
public:
  int iwidth;
  int iheight;
  QString filename;
  QImage img;
};


#endif // _MOTIF_


// GenMapWnd.h : header file
//

class Descriptor {
public:
  char FileName[256];
  int MagTH;
  int MagTT;
  int nPenStyle;
  int nWidth;
  int nTxtField;
  double rFontScale;
  int crColor;
  int crFill;
  int bPolygon;
};



#define FONT_SCALER 1.2


#define NR_BUF     24
//#define BM_HEIGHT 200  - SA Data 50 k
// #define BM_HEIGHT 120


#define MINFONTSIZE  6
#define MAXFONTSIZE 14


/* moved to GraphicContext
BOOL SetupFont(CFont *Font, int height);
*/

//#include <qobject.h>

/////////////////////////////////////////////////////////////////////////////
// CGenMapWnd
//
class CGenMapWnd //: public QObject
 
{
public:

  
  int ci;// = 0;
  CacheObject imgCache[IMG_CACHE];
 
 
  char m_sBinPath[256];
  char m_sQuadPath[256];
  char m_sShadePath[256];

  bool bRoseActive;
  bool bTextActive;
  bool bIsDirty;
  bool bIsBusy;
  bool bAnimate;
  bool bShadeActive;
  bool bPolygonActive;
  bool bDisplayDMS;
  int  iBackGround;
  int iAnimInter;
  int screenWidth;
  int screenHeight;
  bool bDibFileLoaded;

  CGenMapWnd();
  void Init();

// Attributes
public:
  bool bZoomActive;
  bool m_bLeftButtonDown;

private:
  double M;
  int m_PlotControl;
  int m_LogPixY;
  int nTargetGate;
  bool bLButtonDblClk;
  POINT OffsetPt;
  double *buff[NR_BUF+1];
  UINT nrpts[NR_BUF+1];
  char *pbuftxt[NR_BUF+1];
  UINT nrpbuftxt[NR_BUF+1];
  Descriptor descriptor[NR_BUF+1];
  
  QString m_old_name;
  Descriptor m_descriptor;
  //int m_tbl[2][256];
  //int m_tblidx;


public:
  // x globals
  bool flatWorld;
  double top;
  double bottom;
  double right;
  double left;
  double mag; //long int mag;
  double mag2; //long int mag2;
  double centerLon;
  double centerLat;

  int span1000;
  int span0100;
  int span0025;


  // Aspect ration for X/Y
  double convY;
  double convX;

  int nZoomMode;

  int iMouseDragX, iMouseDragY;
  int iMouseDownX, iMouseDownY;//, oLx, oLy;


  void StoC(double &x, double &y, double &z, double lat, double lon, double r);
  void StoCe(double &x, double &y, double &z, double lat, double lon);
  void CtoS(double x, double y, double z, double &lat, double &lon, double &r);
  void CtoSe(double x, double y, double z, double &lat, double &lon);
  //void drawBuffer(GraphicPainter* pDC, CPen *grayPen, double *p, long loop);
  void drawBuffer(GraphicPainter *gc, int idx);
  void drawBinPolygon(GraphicPainter *gc, int idx);


  void drawShpPolygon(GraphicPainter *gc, SHPObject *psShape);
  void drawShpMultiPoint(GraphicPainter *gc, SHPObject *psShape);
  void drawShpfile(GraphicPainter *gc, char *szFilename); //, int nField, double rFSize);

  void drawDbfEntry(DBFHandle hDBF, int iRecord, GraphicPainter *gc, SHPObject *psShape);


  //bool drawMessageLoop();
  //void OnViewZoomIn();
  //void OnViewZoomOut();
  //void Redraw();
  void LocationBar();
  void Size(int cx, int cy);
  void OnSize();
  void OnSize(int cx, int cy);
  void OnViewFlatworld();
  void OnViewShade();

  // depreciated - void OnViewBackgroundNone();
  // depreciated - void OnViewBackgroundGreyscale();
  // depreciated - void OnViewBackgroundColor();


  //--void ZoomToFit();
  void ZoomToWorld();
  void ZoomToRect(double minLat, double maxLat, double minLon, double maxLon);


  void fixup(double &x, double &y);

  void Mcalc();
  int LtoC(double xl, double yl, int &xc, int &yc);
  int CtoL(int xc, int yc, double &xl, double &yl);

  void PanCenter(/*CPoint*/ POINT point);
  void ZoomIn2X();
  void ZoomOut2X();
  void ZoomIn();
  void ZoomOut();
  void ProjectGetXY(double Lat, double Long, int *x, int *y);
  void ProjectGetLatLong(int x, int y, double *Lat, double *Long);

  void Utility(GraphicPainter *gc);

  virtual  void PaintMain(GraphicPainter *gc);
  void DrawMapDet(GraphicPainter *gc);
  //void DrawData1(GraphicPainter *gc);
    /* todo - we may still want this
  void DrawData2(GraphicPainter *gc);
  void DrawLargeData(GraphicPainter *gc, char *szFileName, CPen *Pen);
    */
  void DrawLargeData(GraphicPainter *gc, char *szFileName);


  //void DrawNames(GraphicPainter *gc);
  void DrawNames(GraphicPainter *gc, char *p, long loop);
 
  void DrawStreetNames(GraphicPainter *gc, char *p, long loop);
  void DrawRoadNames(GraphicPainter *gc, char *p, long loop);
  //void DrawAreas(GraphicPainter* pDC, CPen *grayPen, double *p, long loop);
  //void DrawAreas(GraphicPainter* gc, COLORREF color, double *p, long loop);
  void drawLargeArea(GraphicPainter *gc, char *szFileName, COLORREF color);
 
  void DrawGeographic(GraphicPainter *gc);
  void drawLine(GraphicPainter *gc, double lat1, double lon1, double lat2, double lon2);
  void drawArc(GraphicPainter *gc, double lat, double lon, double r, int a1, int a2);
  void drawGrid(GraphicPainter *pDC);
  void DrawOnTheFly(GraphicPainter *pDC);

  double HiLat, HiLong;
  void DrawHiLite(GraphicPainter *gc, double Lat, double Long, char *szName);

  char *GetFileName(char *s, double rLat, double rLong, double rSpan);
  QImage ImageCache(const char *szFileName, int iwidth, int iheight, int *chit = NULL, int *cmiss = NULL, int *creq = NULL);

  void drawShadeR(GraphicPainter *pDC, double rtop, double rleft, double rbottom, double rright);
  void drawShadeA(GraphicPainter *pDC, const char *szFileName, const double cLat, const double cLon, const double vSpan, const double hSpan);


  void LoadData();
  void UnLoadData();              

  bool EraseBkgnd(GraphicPainter* pDC);

  double getMagVar(double lat, double lon);

  void drawScaleRuler(GraphicPainter *gc);

  void drawRose(GraphicPainter *gc, int x, int y, int r);
  void drawRose(GraphicPainter *gc);


  void setCrossHair(double lat, double lon);
  void clearCrossHair();
  void drawCrossHair(GraphicPainter *gc);

  virtual  void mouseLeftButtonDown(POINT point);
  virtual  void mouseLeftButtonUp(POINT point);
  virtual  void mouseMiddleButtonDown(POINT point);
  virtual  void mouseMiddleButtonUp(POINT point);
  virtual  void mouseRightButtonDown(POINT point);
  virtual  void mouseRightButtonUp(POINT point);
  virtual  void mouseMoved(POINT point); 
  //virtual  void mouseDragged(POINT point); 
  virtual  void mouseDragged(GraphicPainter *gc, POINT point);


private:
  double dmax;
  bool bAbort;
  //HCURSOR ZoomCursor, PanCursor;
  bool Mready;
  double M11;
  double M12;
  double M13;
  double M21;
  double M22;
  double M23;
  double M31;
  double M32;
  double M33;

 double xhlat;
 double xhlon;


// Operations
public:


// Implementation
public:
  bool bChange;
  virtual ~CGenMapWnd();

#ifndef _MOTIF_
  QString m_sStatusInfo;
#else
    //std::string m_sStatusInfo;
    char *m_sStatusInfo;
#endif
protected:
 bool bCrossHairActive;

};

/////////////////////////////////////////////////////////////////////////////


#endif //__GENMAPWND_H


