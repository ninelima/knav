/****************************************************************************
** Form interface generated from reading ui file 'AptInfoEditor.ui'
**
** Created: Sat Sep 11 12:24:22 2004
**      by: The User Interface Compiler ($Id: AptInfoEditor.h,v 1.1 2004/09/16 11:24:34 player Exp $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef FORM1_H
#define FORM1_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QLineEdit;
class QLabel;
class QMultiLineEdit;
class QListBox;
class QListBoxItem;
class QPushButton;

class AptInfoEditorForm : public QDialog
{
    Q_OBJECT

public:
    AptInfoEditorForm( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~AptInfoEditorForm();

    QLabel *ICAOTextLabel;
    QLabel *NameTextLabel;
    QLabel *CountryTextLabel;
    QLabel *RegionTextLabel;
    QLabel *GeographicTextLabel;
    QLabel *FrequenciesTextLabel;
    QLabel *RunwaysTextLabel;
    QLabel *CommentsTextLabel;
    QLabel *FuelTextLabel;
    QLabel *OperatorTextLabel;
    QLabel *ImageTextLabel;

    QLineEdit *ICAOLineEdit;
    QLineEdit *NameLineEdit;
    QLineEdit *CountryLineEdit;
    QLineEdit *RegionLineEdit;

    QMultiLineEdit *GeographicMultiLineEdit;
    QMultiLineEdit *FreqMultiLineEdit;
    QMultiLineEdit *RwyMultiLineEdit;
    QMultiLineEdit *CommentMultiLineEdit;
    QMultiLineEdit *FuelMultiLineEdit;
    QMultiLineEdit *OprMultiLineEdit;

    QListBox *ImgListBox;

    QPushButton *AddButton;
    QPushButton *RemoveButton;
    QPushButton *CancelButton;
    QPushButton *OKButton;

public:
    void Init(QString sIcao);
    void readHtmlInfo(QString sFilename);


private:
    QString sICAO;
    QString sINFO;
    QString sCOUNTRY;
    QString sREGION;
    QString sSPATIAL;
    QString sFREQ;
    QString sRWY;
    QString sCOMMENT;
    QString sFUEL;
    QString sOPERATOR;
    
    QString m_sFilename;

    QString stripHTMLTags(QString htmlStr);


protected:
    QVBoxLayout * Form1Layout;
    QVBoxLayout * Layout33;
    QHBoxLayout * Layout6;
    QGridLayout * Layout4;
    QHBoxLayout * Layout8;
    QVBoxLayout * Layout2;
    QVBoxLayout * Layout1;
    QHBoxLayout * Layout32;
    QVBoxLayout * Layout12;
    QVBoxLayout * Layout31;
    QSpacerItem * Spacer4;

protected slots:
    virtual void languageChange();
    void add_clicked();
    void remove_clicked();
    void ok_clicked();

};

#endif // FORM1_H
