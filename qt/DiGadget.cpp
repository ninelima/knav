//-----------------------------------------------------------------------------
// $Id: DiGadget.cpp,v 1.2 2005/07/08 07:40:14 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <qrect.h>
#include <math.h>
#include "DiGadget.h"
#include "umath.h"
#include "uvalues.h"

//#define min(a,b)            (((a) < (b)) ? (a) : (b))

//-----------------------------------------------------------------------------
// Default constructor
//
DiGadget::DiGadget(QWidget *parent, const char *name, WFlags f)
    //:QWidget(parent, name, f)
    :PanelGadget(parent, name, f)
{
  // capture mouse movements only if buttons are pressed
  // ie mouse dragged
  //setMouseTracking(false);
  // not!
  setMouseTracking(true);
  m_bMouseLeftButtonDown = false;
  m_bMouseRightButtonDown = false;
}


//-----------------------------------------------------------------------------
// Paint method the panel
//
/*
void DiGadget::paintEvent(QPaintEvent* e)
{
  QPainter p(this);
  QPainter b(&pixbuffer);
  GraphicPainter *gc = (GraphicPainter *) &p;

  rect = p.window();
  int screenWidth  = rect.width();
  int screenHeight = rect.height();
  
  m_size = min(screenWidth, screenHeight)/2;
  m_scale = (double) m_size / 75.0;

  drawFace((GraphicPainter *) &b);
  // copy the image from the buffer pixmap to the window
  bitBlt(this, 0, 0, &pixbuffer);
}
*/


void DiGadget::drawBlankFace(GraphicPainter *gc, int x, int y, int size)
{
  //double scale = (double) size / 75.0;
  double scale = m_scale;

  gc->setPen(Qt::gray);
  gc->setBrush(Qt::gray);
  gc->drawRect((x-75)*scale, (y-75)*scale, 150*scale, 150*scale);

  gc->setPen(Qt::black);
  gc->setBrush(Qt::black);
  gc->drawEllipse((x-75)*scale, (y-75)*scale, 150*scale, 150*scale);

  gc->setPen(Qt::white);
  gc->setBrush(Qt::white);

  // draw the little plane - vectors based on size 75
  QPoint ptTbl[32] = {
    // left
    QPoint(0,   -30), QPoint(  -3, -10),
    QPoint(-3,  -10), QPoint( -25,  -5),
    QPoint(-25,  -5), QPoint( -25,  -0),
    QPoint(-25,  -0), QPoint(  -3,  -0),
    QPoint(-3,   -0), QPoint(  -2, +20),
    QPoint(-2,  +20), QPoint( -10, +22),
    QPoint(-10, +22), QPoint( -10, +26),
    QPoint(-10, +26), QPoint(  -0, +26),

    QPoint(0,   -30), QPoint(  +3, -10),
    QPoint(+3,  -10), QPoint( +25,  -5),
    QPoint(+25,  -5), QPoint( +25,  -0),
    QPoint(+25,  -0), QPoint(  +3,  -0),
    QPoint(+3,   -0), QPoint(  +2, +20),
    QPoint(+2,  +20), QPoint( +10, +22),
    QPoint(+10, +22), QPoint( +10, +26),
    QPoint(+10, +26), QPoint(  +0, +26)
  };

  QPointArray ptArray(32);
  QPoint offs(x, y);

  for (int i = 0; i < 32; i++) {
    ptArray[i] = (ptTbl[i] + offs)*scale;
  }
    
  gc->drawPolygon(ptArray);//, 0, iNrPoints);

  gc->drawLine(x*scale, (y-37)*scale, x*scale, (y-33)*scale);
  gc->drawLine(x*scale, (y+37)*scale, x*scale, (y+29)*scale);

}


void DiGadget::drawFace(GraphicPainter *gc)
{
  QString ts;
  int i;
  int a, b;
  int x = 75;
  int y = 75;

  int size = m_size;//55;//125;//75;
  //double scale = (double) size / 75.0;
  double scale = m_scale;
  //blank(x,y,65);
  //drawBlankFace(gc, x, y, 75);
  //drawBlankFace(gc, x, y, size);
  //hexagon(x,y,65);

  gc->setPen(Qt::white);

  QFont font1("Arial", 8*scale); //, fontsize);
  gc->setFont(font1);

  int fontheight = gc->fontMetrics().height();
  int fontwidth = gc->fontMetrics().width('0');

  for(i = 0; i <= 11 /*359 div 30*/; i++)  {
    ts.sprintf("%02d", 36-i*3);

    if (ts == "36") ts = "N";
    if (ts == "09") ts = "E";
    if (ts == "18") ts = "S";
    if (ts == "27") ts = "W";

    int off = 53;
    int phi = i * 30 + 90 + m_Hdg;

    a = uround(off*cos(phi*M_PI/180));
    b = uround(off*sin(phi*M_PI/180));
    a = a - fontwidth;
    b = b - fontheight/4;

    gc->drawString(ts, (x+a)*scale, (y-b)*scale);
  }

  // Short calib lines
  for (i = 1; i < 360 / 10; i++) {
    int a, b;
    int off =  40;
    int phi = i * 10 + m_Hdg;

    a = uround(off*cos(phi*M_PI/180));
    b = uround(off*sin(phi*M_PI/180));

    gc->radLine((x+a)*scale, (y-b)*scale, scale*5, phi);
  }
}

void DiGadget::setHdg(int Hdg)
{
	if (m_Hdg != Hdg) {
	  m_Hdg = Hdg;
		repaint(false);
	}
}


void DiGadget::mousePressEvent(QMouseEvent *e)
{
  // PanelGadget::mousePressEvent(e);
  
  // play
  //m_Hdg = (m_Hdg+=5) % 360;
  //repaint(false);
}

