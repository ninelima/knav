
/*************************************************************************
 * Version 1.0  on  23-May-1997
 * (c) 1997 Pralay Dakua (pkanti@hotmail.com)
 *
 * This is a free software and permission to use, modify, distribute,
 * selling and using for commercial purpose is hereby granted provided
 * that THE ABOVE COPYRIGHT NOTICE AND THIS PERMISSION NOTICE SHALL BE
 * INCLUDED IN ALL COPIES AND THEIR SUPPORTING DOCUMENTATIONS.
 *
 * There is no warranty for this software. In no event Pralay Dakua
 * will be liable for merchantability and fitness of the software and
 * damages due to this software.
 *
 * Author:
 * Pralay Dakua (pkanti@hotmail.com)
 *
 **************************************************************************/

#ifdef _MOTIF_

#include "ToolbarWidgetP.h"
#include <X11/Shell.h>
#include <Xm/Label.h>

static void Initialize(Widget,Widget,ArgList,Cardinal *);
static void Destroy(Widget);
static void Resize(Widget);
static Boolean SetValues(Widget,Widget, Widget,ArgList,Cardinal *);

static XtGeometryResult QueryGeometry(Widget, XtWidgetGeometry*,
                                    XtWidgetGeometry*);
static XtGeometryResult GeometryManager(Widget, XtWidgetGeometry*,
                                    XtWidgetGeometry*);
static void ChangeManaged(Widget);


static void Constraint_Initialize(Widget,Widget,ArgList,Cardinal *);
static Boolean Constraint_SetValues(Widget,Widget, Widget,ArgList,Cardinal *);
static void Constraint_Destroy(Widget);

static void Layout(XmToolbarWidget);
static void layout_unspec_hor(XmToolbarWidget);
static void layout_spec_hor(XmToolbarWidget);
static void layout_unspec_ver(XmToolbarWidget);
static void layout_spec_ver(XmToolbarWidget);

static void make_tooltip_on(XmToolbarWidget);
static void make_tooltip_off(XmToolbarWidget);
static void post_tooltip(XtPointer);
static void get_size_for_children(XmToolbarWidget, XtWidgetGeometry*);
static void hor_size_spec(XmToolbarWidget, XtWidgetGeometry*);
static void hor_size_unspec(XmToolbarWidget, XtWidgetGeometry*);
static void ver_size_spec(XmToolbarWidget, XtWidgetGeometry*);
static void ver_size_unspec(XmToolbarWidget, XtWidgetGeometry*);
static void replace_tooltip(XmToolbarWidget);
static void search_big(XmToolbarWidget, int, int, Dimension*, Dimension*,
              Dimension*, Dimension*);

#define offset(field) XtOffsetOf(XmToolbarRec, field)
#define ACTIONPROC(proc)  static void proc(Widget,XEvent*,String*,Cardinal*)
#define CHILD_EVENT_MASK (EnterWindowMask|LeaveWindowMask)

ACTIONPROC(EnterAction);
ACTIONPROC(LeaveAction);

static void child_event_handler(Widget, XtPointer, XEvent*);

static XtResource resources[]={
  {
  XmNorientation,
  XmCOrientation,
  XmROrientation,
  sizeof(unsigned char),
  offset(toolbar.orientation),
  XmRImmediate,
  (XtPointer)XmHORIZONTAL
  },
  {
  XmNspacing,
  XmCSpacing,
  XmRDimension,
  sizeof(Dimension),
  offset(toolbar.spacing),
  XmRString,
  "0"
  },
  {
  XmNentryCount,
  XmCEntryCount,
  XmRInt,
  sizeof(int),
  offset(toolbar.entry_count),
  XmRImmediate,
  (XtPointer)XmENTRY_UNSPECIFIED
  },
  {
  XmNgroupSpacing,
  XmCGroupSpacing,
  XmRDimension,
  sizeof(Dimension),
  offset(toolbar.group_spacing),
  XmRString,
  "5"
  },
  {
  XmNmarginHeight,
  XmCMarginHeight,
  XmRDimension,
  sizeof(Dimension),
  offset(toolbar.margin_height),
  XmRString,
  "0"
  },
  {
  XmNmarginWidth,
  XmCMarginWidth,
  XmRDimension,
  sizeof(Dimension),
  offset(toolbar.margin_width),
  XmRString,
  "0"
  },
  {
  XmNtipFontList,
  XmCTipFontList,
  XmRFontList,
  sizeof(XmRFontList),
  offset(toolbar.tip_font_list),
  XmRImmediate,
  (XtPointer)NULL
  },
  {
  XmNtipForeground,
  XmCTipForeground,
  XmRPixel,
  sizeof(Pixel),
  offset(toolbar.tip_foreground),
  XmRCallProc,
  (XtPointer)_XmForegroundColorDefault
  },
  {
  XmNtipBackground,
  XmCTipBackground,
  XmRPixel,
  sizeof(Pixel),
  offset(toolbar.tip_background),
  XmRCallProc,
  (XtPointer)_XmBackgroundColorDefault
  },
  {
  XmNtipBorderWidth,
  XmCTipBorderWidth,
  XmRDimension,
  sizeof(Dimension),
  offset(toolbar.tip_border_width),
  XmRString,
  "1"
  },
  {
  XmNtipPosition,
  XmCTipPosition,
  XmRTipPosition,
  sizeof(unsigned char),
  offset(toolbar.tip_position),
  XmRImmediate,
  (XtPointer)XmTIP_BOTTOM_RIGHT,
  },
  {
  XmNtipDistance,
  XmCTipDistance,
  XmRDimension,
  sizeof(Dimension),
  offset(toolbar.tip_distance),
  XmRString,
  "10"
  },
  {
  XmNtipNotifyDelay,
  XmCTipNotifyDelay,
  XmRInt,
  sizeof(int),
  offset(toolbar.tip_notify_delay),
  XmRString,
  "1000"
  }
};

static XtResource constraint_resources[]={
  {
  XmNtipLabel,
  XmCTipLabel,
  XmRXmString,
  sizeof(XmString),
  XtOffsetOf(XmToolbarConstraintRec, toolbar.tip_label),
  XmRImmediate,
  (XtPointer)NULL
  },
  {
  XmNnotifyable,
  XmCNotifyable,
  XmRBoolean,
  sizeof(Boolean),
  XtOffsetOf(XmToolbarConstraintRec, toolbar.notifyable),
  XmRImmediate,
  (XtPointer)True
  },
  {
  XmNnewGroup,
  XmCNewGroup,
  XmRBoolean,
  sizeof(Boolean),
  XtOffsetOf(XmToolbarConstraintRec, toolbar.new_group),
  XmRImmediate,
  (XtPointer)False
  },
  {
  XmNconfigurable,
  XmCConfigurable,
  XmRBoolean,
  sizeof(Boolean),
  XtOffsetOf(XmToolbarConstraintRec, toolbar.configurable),
  XmRImmediate,
  (XtPointer)True
  }
};

static XtActionsRec actions[] = {
  { "EnterAction", (XtActionProc)EnterAction },
  { "LeaveAction", (XtActionProc)LeaveAction }
};

static char translations[] =
"<Enter>: EnterAction() \n\
<Leave>: LeaveAction()";


XmToolbarClassRec xmToolbarClassRec={
{ /***** core class ******/
(WidgetClass) &xmManagerClassRec, /* super class */
"XmToolbar",        /* class name */
sizeof(XmToolbarRec),     /* widget size */
NULL,         /* class initialize */
NULL,         /* class part initialize */
False,          /* class inited */
(XtInitProc)Initialize,     /* initialize */
NULL,         /* initialize hook */
XtInheritRealize,     /* realize */
actions,        /* actions */
XtNumber(actions),      /* num actions */
resources,        /* resources */
XtNumber(resources),      /* num resources */
NULLQUARK,        /* xrm class */
True,         /* compress motion */
XtExposeCompressMultiple,   /* compress exposure */
False,          /* compress enter leave */
False,          /* visible interest */
(XtWidgetProc)Destroy,      /* destroy */
(XtWidgetProc)Resize,     /* resize */
XtInheritExpose,      /* expose */
(XtSetValuesFunc)SetValues,   /* set values */
NULL,         /* set values hook */
XtInheritSetValuesAlmost,   /* set values almost */
NULL,         /* get values hook */
XtInheritAcceptFocus,     /* accept focus */
XtVersion,        /* version */
NULL,         /* callback private */
translations,       /* tm table */
(XtGeometryHandler)QueryGeometry,   /* query geometry */
XtInheritDisplayAccelerator,    /* display accelerator */
NULL          /* extension */
},
{ /***** composite class part *****/
(XtGeometryHandler)GeometryManager,   /* geomerty manager */
(XtWidgetProc)ChangeManaged,    /* change managed */
XtInheritInsertChild,     /* insert child */
XtInheritDeleteChild,     /* delete child */
NULL          /* extension */
},
{ /**** Constraint Class Part *****/
constraint_resources,     /* constraint resource list */
XtNumber(constraint_resources),   /* number of constraints in list */
sizeof(XmToolbarConstraintRec),   /* size of constraint record     */
(XtInitProc)Constraint_Initialize,  /* constraint initialization   */
(XtWidgetProc)Constraint_Destroy, /* constraint destroy proc     */
(XtSetValuesFunc)Constraint_SetValues,  /* constraint set_values proc   */
NULL          /* pointer to extension record  */
},
{ /****** Manager Class Part *****/
NULL,         /* translations */
NULL,         /* syn_resources  */
0,          /* num_syn_resources */
NULL,         /* syn_constraint_resources */
0,          /* num_syn_constraint_resources */
XmInheritParentProcess,     /* parent_process */
NULL          /* extension */
},
{ /******* Toolbar Class Part ******/
NULL          /* extension */
}
};

WidgetClass xmToolbarWidgetClass=(WidgetClass) &xmToolbarClassRec;


/*********************** Actions *******************************/

static void EnterAction(Widget w, XEvent *event,
        String *params,Cardinal *nparams)
{
XmToolbarWidget wid = (XmToolbarWidget) w;

 switch(event->xcrossing.detail)
  {
  case NotifyVirtual:
    make_tooltip_on(wid);
  break;

  case NotifyInferior:
    make_tooltip_off(wid);
  break;

  case NotifyNonlinearVirtual:
    make_tooltip_on(wid);
  break;
  }
}

static void LeaveAction(Widget w, XEvent *event,
      String *params,Cardinal *nparams)
{
XmToolbarWidget wid = (XmToolbarWidget) w;


  switch(event->xcrossing.detail)
  {
  case NotifyVirtual:
    make_tooltip_off(wid);
  break;

  case NotifyInferior:
    make_tooltip_on(wid);
  break;

  case NotifyNonlinearVirtual:
    make_tooltip_off(wid);
  break;
  }

}

/*********************** end of Actions **************************/

/********************** Core Class Methods **********************/

static void Initialize(Widget treq,Widget tnew,ArgList args,Cardinal *nargs)
{
XmToolbarWidget wid = (XmToolbarWidget) tnew;
Arg temp_args[10];
int i;

wid->toolbar.interval_id_on = False;
wid->toolbar.tooltip_on = False;

wid->manager.traversal_on = False;

i = 0;
XtSetArg(temp_args[i], XmNallowShellResize, True); i++;
XtSetArg(temp_args[i], XmNborderWidth, wid->toolbar.tip_border_width); i++;
XtSetArg(temp_args[i], XmNborderColor, wid->toolbar.tip_foreground); i++;
XtSetArg(temp_args[i], XmNbackground, wid->toolbar.tip_background); i++;

wid->toolbar.tooltip = XtCreatePopupShell("Notifier", overrideShellWidgetClass,
          (Widget) wid, temp_args, i);

i = 0;
XtSetArg(temp_args[i], XmNbackground, wid->toolbar.tip_background); i++;
XtSetArg(temp_args[i], XmNforeground, wid->toolbar.tip_foreground); i++;

if(wid->toolbar.tip_font_list)
XtSetArg(temp_args[i], XmNfontList, wid->toolbar.tip_font_list); i++;

wid->toolbar.label = XtCreateManagedWidget("NotifierLabel",
          xmLabelWidgetClass, 
          wid->toolbar.tooltip,
          temp_args, i);

}

static void Resize(Widget w)
{
XmToolbarWidget wid = (XmToolbarWidget)w;

Layout(wid);
}

static void Destroy(Widget w)
{
XmToolbarWidget wid = (XmToolbarWidget)w;

XtDestroyWidget(wid->toolbar.tooltip);

}

static Boolean SetValues(Widget current,Widget request, Widget tnew,
        ArgList args,Cardinal *nargs)
{
XmToolbarWidget curw = (XmToolbarWidget) current;
XmToolbarWidget reqw = (XmToolbarWidget) request;
XmToolbarWidget neww = (XmToolbarWidget) tnew;
Boolean redraw = False;
Boolean redo_layout = False;
Boolean repost = False;

  if(neww->manager.traversal_on) neww->manager.traversal_on = False;

 if((curw->toolbar.orientation == XmHORIZONTAL 
    && neww->toolbar.orientation == XmVERTICAL)
  || (curw->toolbar.orientation == XmVERTICAL
    && neww->toolbar.orientation == XmHORIZONTAL))
  {
    redo_layout = True;
  }

 if( curw->toolbar.spacing != neww->toolbar.spacing)
  {
    redo_layout = True;
  }

 if( curw->toolbar.group_spacing != neww->toolbar.group_spacing)
  {
    redo_layout = True;
  }

 if( curw->toolbar.margin_height != neww->toolbar.margin_height)
   {
    redo_layout = True;
   }

 if( curw->toolbar.margin_width != neww->toolbar.margin_width)
   {
    redo_layout = True;
   }

 if( curw->toolbar.entry_count != neww->toolbar.entry_count)
   {
      redo_layout = True;
   }

  if( redo_layout) Layout(neww);

  if( curw->toolbar.tip_foreground != neww->toolbar.tip_foreground)
   {
     XtVaSetValues( neww->toolbar.label,
      XmNforeground, neww->toolbar.tip_foreground,
      NULL);
     XtVaSetValues( neww->toolbar.tooltip,
      XmNborderColor, neww->toolbar.tip_foreground,
      NULL);
   }

  if( curw->toolbar.tip_background != neww->toolbar.tip_background)
   {
     XtVaSetValues( neww->toolbar.label,
    XmNbackground, neww->toolbar.tip_background,
    NULL);
     XtVaSetValues( neww->toolbar.tooltip,
    XmNbackground, neww->toolbar.tip_background,
    NULL);
   }

  if( curw->toolbar.tip_border_width != neww->toolbar.tip_border_width)
   {
     XtVaSetValues( neww->toolbar.tooltip,
    XmNborderWidth, neww->toolbar.tip_border_width,
    NULL);
   }

  if( curw->toolbar.tip_font_list != neww->toolbar.tip_font_list)
   {
     XtVaSetValues( neww->toolbar.label,
    XmNfontList, neww->toolbar.tip_font_list,
    NULL);
   }

  if( curw->toolbar.tip_position != neww->toolbar.tip_position)
   {
    repost = True;
   }

  if( curw->toolbar.tip_distance != neww->toolbar.tip_distance)
   {
    repost = True;
   }

  if( repost) replace_tooltip(neww);

return(redraw); 
}

static XtGeometryResult QueryGeometry(Widget w, 
        XtWidgetGeometry *request,
        XtWidgetGeometry *reply)
{
XmToolbarWidget wid = (XmToolbarWidget) w;
XtGeometryResult answer = XtGeometryNo;

  reply->request_mode = (CWWidth|CWHeight);

  reply->request_mode = 0;

  if( request->request_mode & CWX)
  {
    reply->x = request->x;
    reply->request_mode |= CWX;
    answer = XtGeometryYes;
  }

  if( request->request_mode & CWY)
  {
    reply->y = request->y;
    reply->request_mode |= CWY;
    answer = XtGeometryYes;
  }

  if( request->request_mode & CWBorderWidth)
  {
    reply->border_width = request->border_width;
    reply->request_mode |= CWBorderWidth;
    answer = XtGeometryYes;
  }

  if( request->request_mode & CWSibling)
  {
    reply->sibling = request->sibling;
    reply->request_mode |= CWSibling;
    answer = XtGeometryYes;
  }

  if( request->request_mode & CWStackMode)
  {
    reply->stack_mode = request->stack_mode;
    reply->request_mode |= CWStackMode;
    answer = XtGeometryYes;
  }
  
  if( request->request_mode & (CWWidth|CWHeight))
  {
    get_size_for_children(wid, reply);
  }

  if( request->request_mode & CWWidth)
  {
    reply->request_mode |= CWWidth;

    if(request->width == reply->width)
    {
    answer = XtGeometryYes;
    }
    else if(request->width > reply->width)
    {
    reply->width = request->width;
    answer = XtGeometryYes;
    }
    else
    {
    answer = XtGeometryNo;
    }
  }

  if( request->request_mode & CWHeight)
  {
    reply->request_mode |= CWHeight;

    if(request->height == reply->height)
    {
    answer = XtGeometryYes;
    }
    else if(request->height > reply->height)
    {
    reply->width = request->width;
    answer = XtGeometryYes;
    }
    else
    {
    answer = XtGeometryNo;
    }
  }

return(answer);
}

static void get_size_for_children(XmToolbarWidget wid,
         XtWidgetGeometry *geometry)
{

   switch( wid->toolbar.orientation )
  {
   case XmHORIZONTAL:

             if(wid->toolbar.entry_count == XmENTRY_UNSPECIFIED)
               {
      hor_size_unspec(wid, geometry);
    }
    else
    {
      hor_size_spec(wid, geometry);
    }
  break;

    case XmVERTICAL:

             if(wid->toolbar.entry_count == XmENTRY_UNSPECIFIED)
               {
                  ver_size_unspec(wid, geometry);
               }
               else
               {
                  ver_size_spec(wid, geometry);
               }

      break;
  }

}

static void hor_size_spec(XmToolbarWidget wid, XtWidgetGeometry *geometry)
{
int i;
Widget child;
int child_count;
Dimension size, row_height;
int val;
XmToolbarConstraintRec *toolbar_constraints;
int num_row;

  geometry->width = wid->toolbar.margin_width;
  geometry->height = wid->toolbar.margin_height;

      size = wid->toolbar.margin_width;

  row_height = 0;
  num_row = 0;

       for(i=0, child_count=0; i< wid->composite.num_children; i++)
       {

    child = wid->composite.children[i];

    toolbar_constraints =
      (XmToolbarConstraintRec*) child->core.constraints;

    if(!XtIsManaged(child)) continue;

    child_count++;
    val = child_count % wid->toolbar.entry_count;

    if(val == 1 || child->core.height > row_height)
    {
      row_height = child->core.height; 
      num_row++;
    }

    if(val)
    {
                if( toolbar_constraints->toolbar.new_group && val != 1 )
      {
                  size += child->core.width + wid->toolbar.group_spacing;
      }
                else
      {
                  size += child->core.width + wid->toolbar.spacing;
      }
               }
               else
               {
                     size += (child->core.width + wid->toolbar.margin_width);

                        if( size > geometry->width )
                        {
                        geometry->width = size;
                        }

      size = wid->toolbar.margin_width;
      geometry->height += row_height;
              }

      }

  if(num_row > 0)
  geometry->height += (num_row - 1) * wid->toolbar.spacing
        + wid->toolbar.margin_height;
  else
  geometry->height += wid->toolbar.margin_height;
}


static void ver_size_spec(XmToolbarWidget wid, XtWidgetGeometry *geometry)
{
int i;
Widget child;
int child_count;
Dimension size;
int val;
int num_col;
Dimension col_width;
XmToolbarConstraintRec *toolbar_constraints;

geometry->width = wid->toolbar.margin_width;
geometry->height = wid->toolbar.margin_height;

       size = wid->toolbar.margin_height;

  col_width = 0;
  num_col = 0;

       for(i=0, child_count=0; i< wid->composite.num_children; i++)
       {
         child = wid->composite.children[i];

         toolbar_constraints =
                 (XmToolbarConstraintRec*) child->core.constraints;

         if(!XtIsManaged(child)) continue;

           child_count++;

           val = child_count % wid->toolbar.entry_count;

    if(val == 1 || child->core.width > col_width)
    {
      col_width = child->core.width; 
      num_col++;
    }

            if( val )
            {
              if( toolbar_constraints->toolbar.new_group && val != 1 )
                  size += child->core.height + wid->toolbar.group_spacing;
              else
                 size += child->core.height + wid->toolbar.spacing;
            }
            else
            {
               size += (child->core.height + wid->toolbar.margin_height);

                  if( size > geometry->height )
                     {
                         geometry->height = size;
                     }

              size = wid->toolbar.margin_height;
    geometry->width += col_width;
            }

     }

  if(num_col > 0)
  geometry->width += (num_col - 1) * wid->toolbar.spacing
        + wid->toolbar.margin_width;
  else
  geometry->width += wid->toolbar.margin_width;

}

static void hor_size_unspec(XmToolbarWidget wid, XtWidgetGeometry *geometry)
{
int i;
Widget child;
int child_count;
XmToolbarConstraintRec *toolbar_constraints;

geometry->width = wid->toolbar.margin_width;
geometry->height = wid->toolbar.margin_height;

       for(i=0, child_count=0; i< wid->composite.num_children; i++)
       {
         child = wid->composite.children[i];

         toolbar_constraints =
                 (XmToolbarConstraintRec*) child->core.constraints;

            if(XtIsManaged(child))
            {
               child_count++;

              if( (child->core.height + 2* wid->toolbar.margin_height)
                                                   > geometry->height)
               {
               geometry->height = 
      (child->core.height + 2* wid->toolbar.margin_height);
               }

               if( toolbar_constraints->toolbar.new_group && child_count != 1 )
                    geometry->width +=
                       (child->core.width + wid->toolbar.group_spacing);
               else
                    geometry->width +=
                       (child->core.width + wid->toolbar.spacing);

           }
       }
   geometry->width += wid->toolbar.margin_width;

}

static void ver_size_unspec(XmToolbarWidget wid, XtWidgetGeometry *geometry)
{
int i;
Widget child;
int child_count;
XmToolbarConstraintRec *toolbar_constraints;

geometry->width = wid->toolbar.margin_width;
geometry->height = wid->toolbar.margin_height;

       for(i=0, child_count=0; i< wid->composite.num_children; i++)
       {
         child = wid->composite.children[i];

         toolbar_constraints =
                 (XmToolbarConstraintRec*) child->core.constraints;

            if(XtIsManaged(child))
            {
               child_count++;

              if( (child->core.width + 2* wid->toolbar.margin_width)
                                                   > geometry->width)
               {
               geometry->width = 
      (child->core.width + 2* wid->toolbar.margin_width);
               }

               if( toolbar_constraints->toolbar.new_group && child_count != 1 )
                    geometry->height +=
                       (child->core.height + wid->toolbar.group_spacing);
               else
                    geometry->height +=
                        (child->core.height + wid->toolbar.spacing);

           }
       }
  geometry->height += wid->toolbar.margin_height;

}

static void search_big(XmToolbarWidget wid, 
    int first_pos, int to_check,
    Dimension *std_width, Dimension *std_height,
    Dimension *unstd_width, Dimension *unstd_height)
{
XtWidgetGeometry rep;
int i;
Widget child;
XmToolbarConstraintRec *toolbar_constraints;
int last_pos;
int checking_done;

*std_width = *std_height = *unstd_width = *unstd_height = 0;

   checking_done = 0;

 for(i=first_pos; i< wid->composite.num_children; i++)
  {
    child = wid->composite.children[i];

         toolbar_constraints =
                 (XmToolbarConstraintRec*) child->core.constraints;
   
  if(!XtIsManaged(child)) continue;

  checking_done++;

      if(toolbar_constraints->toolbar.configurable) 
      {
        XtQueryGeometry(child, NULL, &rep);

        if( rep.request_mode & (CWWidth|CWHeight))
        {
          if(rep.width > *std_width) 
            *std_width = rep.width;
          if(rep.height > *std_height) 
            *std_height = rep.height;
        }
        else
        {
          if(child->core.width > *std_width) 
            *std_width = child->core.width;
          if(child->core.height > *std_height) 
            *std_height = child->core.height;
        }
      }
      else
      {
        if(child->core.width > *unstd_width) 
          *unstd_width = child->core.width;
        if(child->core.height > *unstd_height) 
          *unstd_height = child->core.height;
      }

     if(checking_done == to_check && to_check != 0) break;
  }
 
if( *std_width > *std_height) *std_height = *std_width;
else if( *std_width < *std_height) *std_width = *std_height;
}

static void layout_unspec_hor(XmToolbarWidget wid)
{
Dimension std_width, std_height;
Dimension unstd_width, unstd_height;
Position x, std_y, unstd_y;
int i;
int c_count;
Widget child;
XmToolbarConstraintRec *toolbar_constraints;
Dimension height_space;

search_big(wid, 0, 0, &std_width, &std_height, &unstd_width, &unstd_height);

 x = wid->toolbar.margin_width;

  if( std_height >= unstd_height ) 
    {
    std_y = wid->toolbar.margin_height;
      height_space = std_height;
    }
  else
    {
     std_y = wid->toolbar.margin_height 
        + (Position)((unstd_height - std_height)/2); 
      height_space = unstd_height;
    }
    
 for(i=0, c_count=0; i< wid->composite.num_children; i++)
   {
      child = wid->composite.children[i];

         toolbar_constraints =
                 (XmToolbarConstraintRec*) child->core.constraints;
   
     if(XtIsManaged(child))
      {
     c_count++;

       if(c_count != 1)
      {
        if(toolbar_constraints->toolbar.new_group)
         x += wid->toolbar.group_spacing;
      else
         x += wid->toolbar.spacing;
      }

         if(toolbar_constraints->toolbar.configurable)
         {
    XtConfigureWidget(child, x, std_y, std_width, std_height, 
        child->core.border_width);
         }
         else
         {
    unstd_y = wid->toolbar.margin_height 
      + (Position)((height_space - child->core.height)/2);
    XtConfigureWidget(child, x, unstd_y, child->core.width, 
      child->core.height, child->core.border_width);
         }
      x += child->core.width; 
      }
   }
}


static void layout_unspec_ver(XmToolbarWidget wid)
{
Dimension std_width, std_height;
Dimension unstd_width, unstd_height;
Position y, std_x, unstd_x;
int i;
int c_count;
Widget child;
XmToolbarConstraintRec *toolbar_constraints;
Dimension width_space;

search_big(wid, 0, 0, &std_width, &std_height, &unstd_width, &unstd_height);

 y = wid->toolbar.margin_height;

  if( std_width >= unstd_width ) 
  {
  std_x = wid->toolbar.margin_width;
   width_space = std_width;
  }
  else 
  {
  std_x = wid->toolbar.margin_width
    + (Position)((unstd_width - std_width)/2); 
    width_space = unstd_width;
  }
    
 for(i=0, c_count=0; i< wid->composite.num_children; i++)
   {
      child = wid->composite.children[i];

        toolbar_constraints =
                 (XmToolbarConstraintRec*) child->core.constraints;
   
     if(XtIsManaged(child))
      {
     c_count++;

       if(c_count != 1)
      {
        if(toolbar_constraints->toolbar.new_group)
           y += wid->toolbar.group_spacing;
        else
           y += wid->toolbar.spacing;
      }

         if(toolbar_constraints->toolbar.configurable)
         {
    XtConfigureWidget(child, std_x, y, std_width, std_height, 
      child->core.border_width);
         }
         else
         {
    unstd_x = wid->toolbar.margin_width 
      + (Position)((width_space - child->core.width)/2);
    XtConfigureWidget(child, unstd_x, y, child->core.width, 
      child->core.height, child->core.border_width);
         }
      y += child->core.height; 
      }
   }
}

static void layout_spec_hor(XmToolbarWidget wid)
{
Dimension std_width, std_height;
Dimension unstd_width, unstd_height;
Position x, std_y, unstd_y;
Dimension dummy;
int i;
int c_count;
Widget child;
int val;
XmToolbarConstraintRec *toolbar_constraints;
Position YY;

search_big(wid, 0, 0, &std_width, &std_height, &dummy, &dummy);

x = wid->toolbar.margin_width;
YY = wid->toolbar.margin_height;

  for(i=0, c_count=0; i< wid->composite.num_children; i++)
  {
  child = wid->composite.children[i];

  toolbar_constraints = (XmToolbarConstraintRec*) child->core.constraints;

  if(!XtIsManaged(child)) continue;

  c_count++;

  val = c_count % wid->toolbar.entry_count;

  if( val == 1) 
  { 
    search_big(wid, i, wid->toolbar.entry_count,
         &dummy, &dummy, &unstd_width, &unstd_height);

    if( std_height >= unstd_height) 
    {
      std_y = YY;
    }
    else
    {
      std_y = YY + (Position)((unstd_height - std_height)/2);
    }
  }
  else
  {
    if(toolbar_constraints->toolbar.new_group)
      x += wid->toolbar.group_spacing;
    else
      x += wid->toolbar.spacing;
  }

         if(toolbar_constraints->toolbar.configurable)
         {
    XtConfigureWidget(child, x, std_y, std_width, std_height, 
        child->core.border_width);
         }
         else
         {
    unstd_y = std_y + (Position)((std_height - child->core.height)/2);
    XtConfigureWidget(child, x, unstd_y, child->core.width, 
        child->core.height, child->core.border_width);
         }
      x += child->core.width; 

         if(val == 0)
         {
    if(unstd_height > std_height)
      YY += unstd_height + wid->toolbar.spacing;

    else
      YY += std_height + wid->toolbar.spacing;

                x = wid->toolbar.margin_width;
         }
   }
}

static void layout_spec_ver(XmToolbarWidget wid)
{
Dimension std_width, std_height;
Dimension unstd_width, unstd_height;
Position y, std_x, unstd_x;
Dimension dummy;
int i;
int c_count;
Widget child;
int val;
XmToolbarConstraintRec *toolbar_constraints;
Position XX;

search_big(wid, 0, 0, &std_width, &std_height, &dummy, &dummy);

y = wid->toolbar.margin_height;
XX = wid->toolbar.margin_width;

  for(i=0, c_count=0; i< wid->composite.num_children; i++)
  {
  child = wid->composite.children[i];

  toolbar_constraints = (XmToolbarConstraintRec*) child->core.constraints;

  if(!XtIsManaged(child)) continue;

  c_count++;

  val = c_count % wid->toolbar.entry_count;

  if( val == 1) 
  { 
    search_big(wid, i, wid->toolbar.entry_count,
         &dummy, &dummy, &unstd_width, &unstd_height);

    if( std_width >= unstd_width) 
    {
      std_x = XX;
    }
    else
    {
      std_x = XX + (Position)((unstd_width - std_width)/2);
    }
  }
  else
  {
    if(toolbar_constraints->toolbar.new_group)
      y += wid->toolbar.group_spacing;
    else
      y += wid->toolbar.spacing;
  }

         if(toolbar_constraints->toolbar.configurable)
         {
    XtConfigureWidget(child, std_x, y, std_width, std_height, 
        child->core.border_width);
         }
         else
         {
    unstd_x = std_x + (Position)((std_width - child->core.width)/2);
    XtConfigureWidget(child, unstd_x, y, child->core.width, 
        child->core.height, child->core.border_width);
         }
      y += child->core.height; 

         if(val == 0)
         {
    if(unstd_width > std_width)
      XX += unstd_width + wid->toolbar.spacing;

    else
      XX += std_width + wid->toolbar.spacing;

                y = wid->toolbar.margin_height;
         }
   }
}

/********************** end of Core Class Methods ***************/

/********************** Composite Methods ***********************/

static XtGeometryResult GeometryManager(Widget w, 
        XtWidgetGeometry *request,
        XtWidgetGeometry *reply)
{
XmToolbarWidget wid = (XmToolbarWidget)w->core.parent;
XmToolbarConstraintRec *constraints;
XtGeometryResult answer = XtGeometryNo;
Dimension width, height, border;

  reply->request_mode = 0;

  constraints =
    (XmToolbarConstraintRec*) w->core.constraints;

  if(constraints->toolbar.configurable)
      return(answer);

  width = w->core.width;
  height = w->core.height;
  border = w->core.border_width;

  if(request->request_mode & CWBorderWidth)
  {
    border = reply->border_width = request->border_width;
    reply->request_mode |= CWBorderWidth;
    answer = XtGeometryYes;
  }

  if(request->request_mode & CWSibling)
  {
    reply->sibling = request->sibling;
    reply->request_mode |= CWSibling;
    answer = XtGeometryYes;
  }

  if(request->request_mode & CWStackMode)
  {
    reply->stack_mode = request->stack_mode;
    reply->request_mode |= CWStackMode;
    answer = XtGeometryYes;
  }

  if(request->request_mode & CWWidth)
  {
    width = reply->width = request->width;
    reply->request_mode |= CWWidth;
    answer = XtGeometryYes;
  }

  if(request->request_mode & CWHeight)
  {
    height = reply->height = request->height;
    reply->request_mode |= CWHeight;
    answer = XtGeometryYes;
  }

  if( request->request_mode & (CWWidth|CWHeight|CWBorderWidth))
  {
    XtResizeWidget(w, width, height, border);
    Layout(wid);
  }

  if(request->request_mode & (CWX | CWY))
  {
    answer = XtGeometryNo;
  }

return(answer);
}

static void ChangeManaged(Widget w)
{
XmToolbarWidget wid = (XmToolbarWidget)w;

 Layout(wid);

}

static void Layout(XmToolbarWidget wid)
{
XtWidgetGeometry geometry;

  switch(wid->toolbar.orientation)
  {
  case XmHORIZONTAL:
    if( wid->toolbar.entry_count == XmENTRY_UNSPECIFIED)
      layout_unspec_hor(wid);
    else
      layout_spec_hor(wid);
  break;

  case XmVERTICAL:
    if( wid->toolbar.entry_count == XmENTRY_UNSPECIFIED)
      layout_unspec_ver(wid);
    else
      layout_spec_ver(wid);
  break;
  }

      get_size_for_children(wid,&geometry);

     XtMakeResizeRequest((Widget)wid, geometry.width, geometry.height,
      NULL, NULL);
}

/********************** end of Composite Methods ***************/

/****************** Constraint Class Methods *******************/

static void Constraint_Initialize(Widget treq,Widget tnew,
      ArgList args,Cardinal *nargs)
{
XmToolbarConstraintRec *new_constraints;
XmString temp_str;


   new_constraints =
              (XmToolbarConstraintRec*) tnew->core.constraints;

  if(new_constraints->toolbar.tip_label)
  {
    temp_str = new_constraints->toolbar.tip_label;
    new_constraints->toolbar.tip_label = XmStringCopy(temp_str);
  }

  XtAddEventHandler(tnew, CHILD_EVENT_MASK, False,
          (XtEventHandler)child_event_handler,
          (XtPointer)XtParent(tnew));
        
}


static void Constraint_Destroy(Widget w)
{
XmToolbarWidget wid = (XmToolbarWidget)XtParent(w);
XmToolbarConstraintRec *toolbar_constraints;
XmString tip_label;

   toolbar_constraints =
              (XmToolbarConstraintRec*) w->core.constraints;

      tip_label = toolbar_constraints->toolbar.tip_label;

   if(tip_label)
   {
  XmStringFree(tip_label);
   }

  XtRemoveEventHandler(w, CHILD_EVENT_MASK, False,
          (XtEventHandler)child_event_handler,
          (XtPointer)XtParent(w));

}

static Boolean Constraint_SetValues(Widget current,Widget request, Widget newwid,
                                          ArgList args,Cardinal *nargs)
{
XmToolbarConstraintRec *new_constraints;
XmToolbarConstraintRec *cur_constraints;
Boolean redraw = False; 
XmString temp_str;


   new_constraints =
              (XmToolbarConstraintRec*) newwid->core.constraints;
   cur_constraints =
              (XmToolbarConstraintRec*) current->core.constraints;

  if(!XmStringCompare(new_constraints->toolbar.tip_label,
        cur_constraints->toolbar.tip_label))
  {
    XmStringFree(cur_constraints->toolbar.tip_label);
    temp_str = new_constraints->toolbar.tip_label;
    new_constraints->toolbar.tip_label = XmStringCopy(temp_str);
  }

return(redraw);
}

/****************** end of Constraint Class Methods ************/

/*************** local processing functions *****************/

static void make_tooltip_on(XmToolbarWidget wid)
{
 if( wid->toolbar.tooltip_on ) 
  {
    XtPopdown(wid->toolbar.tooltip);
    wid->toolbar.tooltip_on = False;
  }
 if( wid->toolbar.interval_id_on )
  {
    XtRemoveTimeOut( wid->toolbar.interval_id );
  }

wid->toolbar.interval_id = XtAppAddTimeOut(
       XtWidgetToApplicationContext( (Widget)wid),
       wid->toolbar.tip_notify_delay, 
      (XtTimerCallbackProc) post_tooltip, (XtPointer) wid);

wid->toolbar.interval_id_on = True;
}

static void make_tooltip_off(XmToolbarWidget wid)
{
 if( wid->toolbar.tooltip_on )
   {
      XtPopdown(wid->toolbar.tooltip);
      wid->toolbar.tooltip_on = False;
   }

 if( wid->toolbar.interval_id_on )
   {
      XtRemoveTimeOut( wid->toolbar.interval_id );
    wid->toolbar.interval_id_on = False;
   }

}

static void replace_tooltip(XmToolbarWidget wid)
{
XtPopdown(wid->toolbar.tooltip);
post_tooltip((XtPointer) wid);
}

static void post_tooltip(XtPointer client_data)
{
XmToolbarWidget wid = (XmToolbarWidget) client_data;
Position x, y;
unsigned int width, height;
Bool ptr_status;
Window window, root_window;
int root_x, root_y, win_x, win_y;
unsigned int mask;
Status status;
unsigned int border_width, depth;
Widget widget;
int diff_x, diff_y;
int child_x, child_y;
Boolean notifyable;
XmString tip_label;
XmToolbarConstraintRec *toolbar_constraints;
Dimension label_dimension;

 window = None;

 ptr_status = XQueryPointer(XtDisplay(wid), wid->core.window, 
        &root_window, &window,
          &root_x, &root_y, &win_x, &win_y,
          &mask);

if(ptr_status && window)
  {
    diff_x = root_x - win_x;
    diff_y = root_y - win_y;

    widget = XtWindowToWidget(XtDisplay(wid), window);

  if(widget && XtParent(widget) == (Widget) wid)
    {
    toolbar_constraints = 
    (XmToolbarConstraintRec*) widget->core.constraints;

    notifyable = toolbar_constraints->toolbar.notifyable;
    tip_label = toolbar_constraints->toolbar.tip_label;

    status = XGetGeometry(XtDisplay(wid), window,
          &root_window,
          &win_x, &win_y,
          &width, &height,
          &border_width, &depth);               

    if(notifyable && tip_label && status)
      {
      XtVaSetValues(wid->toolbar.label,
          XmNlabelString, tip_label,
          NULL);

      child_x = diff_x + win_x;
      child_y = diff_y + win_y;

      switch( wid->toolbar.orientation)
      {
      case XmHORIZONTAL:
        switch(wid->toolbar.tip_position)
        {
        case XmTIP_BOTTOM_RIGHT:
          x = (Position)(child_x + (width/2)); 
          y = (Position)(child_y + height 
          + wid->toolbar.tip_distance); 
        break;

        case XmTIP_BOTTOM_LEFT:
          XtVaGetValues(wid->toolbar.label,
                                XmNwidth, &label_dimension,
                                NULL);
  
          x = (Position)(child_x + (width/2) 
            - label_dimension); 
          y = (Position)(child_y + height 
          + wid->toolbar.tip_distance); 
        break;

                     case XmTIP_TOP_RIGHT:
                
                        XtVaGetValues(wid->toolbar.label,
                           XmNheight, &label_dimension,
                           NULL);

                        x = (Position)(child_x + (width/2));
                        y = (Position)(child_y - label_dimension 
                                          - wid->toolbar.tip_distance);
                     break;

                     case XmTIP_TOP_LEFT:
                        XtVaGetValues(wid->toolbar.label,
                           XmNwidth, &label_dimension,
                           NULL);
  
                        x = (Position)(child_x + (width/2) - label_dimension);

                        XtVaGetValues(wid->toolbar.label,
                           XmNheight, &label_dimension,
                           NULL);

                        y = (Position)(child_y - label_dimension
                                          - wid->toolbar.tip_distance);
                     break;
            }
          break;

               case XmVERTICAL:
                  switch(wid->toolbar.tip_position)
                  {
                     case XmTIP_BOTTOM_RIGHT:
                        x = (Position)(child_x + width + wid->toolbar.tip_distance);
                        y = (Position)(child_y + (height/2));
                     break;

                     case XmTIP_BOTTOM_LEFT:
                        XtVaGetValues(wid->toolbar.label,
                           XmNwidth, &label_dimension,
                           NULL);

                        x = (Position)(child_x - wid->toolbar.tip_distance 
        - label_dimension);
                        y = (Position)(child_y + (height/2));
                     break;

                     case XmTIP_TOP_RIGHT:

                        XtVaGetValues(wid->toolbar.label,
                           XmNheight, &label_dimension,
                           NULL);

                        x = (Position)(child_x + width 
        + wid->toolbar.tip_distance);
                        y = (Position)(child_y + (height/2) 
        - label_dimension);
                     break;

                     case XmTIP_TOP_LEFT:
                        XtVaGetValues(wid->toolbar.label,
                           XmNwidth, &label_dimension,
                           NULL);

                        x = (Position)(child_x - wid->toolbar.tip_distance 
        - label_dimension);

                        XtVaGetValues(wid->toolbar.label,
                           XmNheight, &label_dimension,
                           NULL);

                        y = (Position)(child_y + (height/2) - label_dimension);
                     break;
                  }
               break;
      }

      XtVaSetValues(wid->toolbar.tooltip,
        XmNx, x,
          XmNy, y,
        NULL);

      XtPopup(wid->toolbar.tooltip, XtGrabNone);
        wid->toolbar.tooltip_on = True;
        wid->toolbar.interval_id_on = False;

      }
    }
  }
}

static void child_event_handler(Widget widget, XtPointer client, XEvent *event)
{
XmToolbarWidget wid = (XmToolbarWidget) client;
XmToolbarConstraintRec *toolbar_constraints;

if( event->xcrossing.type == EnterNotify)
{
  toolbar_constraints =
     (XmToolbarConstraintRec*) widget->core.constraints;

  switch(event->xcrossing.detail)
  {
  case NotifyNonlinear:
  case NotifyNonlinearVirtual:

    if( toolbar_constraints->toolbar.notifyable)
      replace_tooltip(wid);
    else
      make_tooltip_off(wid);
  break;

  }
}
else if( event->xcrossing.type == LeaveNotify)
{

  toolbar_constraints =
     (XmToolbarConstraintRec*) widget->core.constraints;

  switch(event->xcrossing.detail)
  {
  case NotifyNonlinear:
  case NotifyNonlinearVirtual:

    if( toolbar_constraints->toolbar.notifyable == False)
    {
      make_tooltip_off(wid);
    }
  break;
  }
}

}

#endif //_MOTIF_
/************************* end of file **********************************/

