//-----------------------------------------------------------------------------
// $Id: demo_MainWindow.cpp,v 1.5 2005/02/15 07:46:22 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786


#ifdef WIN32
  //
  // Include the windows headers on the windows
  // platform so we can do some windows stuff.
  //
  #include <windows.h>
  #include <direct.h>

#endif //WIN32

#include "MapWnd.h"
#include "MainWindow.h"

#include <qimage.h>
#include <qpixmap.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qpopupmenu.h>
#include <qmenubar.h>
#include <qkeycode.h>
#include <qmultilineedit.h>
#include <qfile.h>
#include <qfiledialog.h>
#include <qstatusbar.h>
#include <qmessagebox.h>
#include <qprinter.h>
#include <qapplication.h>
#include <qaccel.h>
#include <qtextstream.h>
#include <qpainter.h>
#include <qpaintdevicemetrics.h>
#include <qwhatsthis.h>
#include <qlabel.h>

#include <qlayout.h>
#include <qsplitter.h>
#include <qtooltip.h>
#include <qtextview.h>
#include <qcheckbox.h>

#include <qtable.h>
#include <qtabdialog.h>
#include <qtabbar.h>
#include <qtabwidget.h>
#include <qsettings.h>
#include <qpushbutton.h>


#include "tripkit.h"
#include "massbalancedlg.h"
#include "tabdialog.h"
#include "e6bdlg.h"
#include "iniFile.h"
#include "aptInfoeditor.h"

#include "InfoHtml.h"
#include "version.h"
#include "ufile.h"
#include "ustring.h"
#include "misc.h"
#include "utypes.h"

#include "ui_FlightNotification.h"

#include "filesave.xpm"
#include "fileopen.xpm"
#include "fileprint.xpm"

#include "zoom.xpm"
#include "route.xpm"
#include "globe.xpm"
#include "shade.xpm"

#include "apt.xpm"
#include "nav.xpm"
#include "hangten_16.xpm"
#include "wings.xpm"
	
	
/* TBD
#include "sat.xpm"
*/

//
// Macro definitions
//
#define MIN_WIDGET_SIZE 1
#define SZ_TRACK   "Track"
#define SZ_AIRPORT "Airport"

#define SZ_GPSERROR "Download from GPS failed.\n\n" \
                    "Please ensure that your GPS is connected\n\n" \
                    "to COM1 and set to GARMIN/GARMIN"

extern int g_MouseWheel;
extern unsigned short g_wptMask;// = W_APT | W_VOR | W_NDB;

//-------------------------------------
const char *fileOpenText =
  "<img source=\"fileopen\"> "
  "Click this button to open a <em>new file</em>. <br><br>"
  "You can also select the <b>Open command</b> from the File menu.";
const char *fileSaveText =
  "Click this button to save the file you are "
  "editing.  You will be prompted for a file name.\n\n"
  "You can also select the Save command from the File menu.\n\n"
  "Note that implementing this function is left as an exercise for the reader.";
const char * filePrintText =
  "Click this button to print the file you "
  "are editing.\n\n"
  "You can also select the Print command from the File menu.";

const char *editUndoText =
  "<img source=\"fileopen\"> "
  "Click this button to open a <em>new file</em>. <br><br>"
  "You can also select the <b>Open command</b> from the File menu.";

//-------------------------------------
const char *zoom_buttonText =
  "Click this button to toggle between zoom and pan "
  "mode.\n\n"
  "Left mouse button drag to zoom in and right mouse button to zoom out.";
const char *route_buttonText =
  "Click this button to activate routing "
  "mode.\n\n"
  "Left mouse button click to add the highligted waypoint and "
  "right mouse button click to query the waypoint.";
const char *globe_buttonText =
  "Click this button to toggle between globe and and flat "
  "view mode.\n\n"
  "Note :\n"
  "  All routes are depiced as great circles";
const char *shade_buttonText =
  "Click this button to enable shaded relief display.\n\n"
  "Note :\n"
  "  Relief is only avaliable in flat view mode.";

//-------------------------------------
const char *apt_buttonText =
  "Click this button to enable airports.\n\n"
  "Note :\n"
  "  none.";
const char *nav_buttonText =
  "Click this button to enable navigation aids.\n\n"
  "Note :\n"
  "  At this time only NDB's and VOR's are displayed.";
const char *gpstrk_buttonText =
  "Click this button to display GPS tracks that were\n"
  "downloaded from a GPS unit.\n\n"
  "Note :\n"
  "  For Garmin and compatible series GPS receivers.";
const char *air_buttonText =
  "Click this button to display airspace.\n\n"
  "Note :\n"
  "  At this time only class C displayed.";

USelect g_Select;

//
// http://qdbttabular.sourceforge.net/doc/
// may be an alternative to QTable?
//

//-----------------------------------------------------------------------------
// MapWidget - (�QWidget�*�parent=0, const�char�*�name=0, WFlags�f=0�)
//

CMapWnd g_MapWnd;


MapWidget::MapWidget(QWidget *parent, const char *name, WFlags f)
    :QWidget(parent, name, f)
{
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  g_MouseWheel = iniFile.GetValueI("Prefs", "MouseWheel", 0);

  // Set up the default cursor. At the moment the application
  // wil never start in add route mode, so we can safely set
  // the cursor to the pointing hand.
  setCursor(pointingHandCursor);
}


void MapWidget::paintEvent(QPaintEvent* e)
{

  QPainter p(this);
  p.setClipRect(e->rect());
  g_MapWnd.PaintMain((GraphicPainter *) &p);  // the whole echilada
}

void MapWidget::resizeEvent(QResizeEvent *e)
{
  int width = e->size().width();
  int height = e->size().height();

  g_MapWnd.OnSize(width, height);

  QWidget::resizeEvent(e);
}


void MapWidget::mouseMoveEvent(QMouseEvent *e)
{
  POINT point = {e->x(), e->y()};
  QPainter p(this);
  
  if (g_MapWnd._bLbuttonDown)
    g_MapWnd.mouseDragged((GraphicPainter *) &p, point);
  else
    g_MapWnd.mouseMoved((GraphicPainter *) &p, point);

  emit latlonChanged();
  QWidget::mouseMoveEvent(e);
}

void MapWidget::mousePressEvent(QMouseEvent *e)
{
  POINT point = {e->x(), e->y()};

  switch (e->button()) {
    case LeftButton:
      g_MapWnd.mouseLeftButtonDown(point);
      break;
    case RightButton:
      g_MapWnd.mouseRightButtonDown(point);
      break;
    case MidButton:
      g_MapWnd.mouseMiddleButtonDown(point);
      break;
    case NoButton:
      break;
  }
  QWidget::mousePressEvent(e);
}

void MapWidget::mouseReleaseEvent(QMouseEvent *e)
{
  POINT point = {e->x(), e->y()};

  switch (e->button()) {
    case LeftButton:
      g_MapWnd.mouseLeftButtonUp(point);
      break;
    case RightButton:
      g_MapWnd.mouseRightButtonUp(point);
      emit infoChanged();
      break;
    case MidButton:
      g_MapWnd.mouseMiddleButtonUp(point);
      break;
    case NoButton:
      break;
  }

  //
  // a bit of convoluted logic here but it seems to work
  // 
  if (g_MapWnd.bIsDirty) {
    if (g_MapWnd.bShadeActive)
      repaint(false); // do not erase
    else
      repaint(true);  // erase
  }
  else {
    if (g_MapWnd.bAddRteActive)
      repaint(false); // do not erase
  }
 
  QWidget::mouseReleaseEvent(e);
}


void MapWidget::wheelEvent(QWheelEvent *e)
{
  //
  // There appears to be a bug of sorts
  // with QWheelEvent.pos and globalPos
  // since both appears to return the same
  // coordinates :-(
  //

  //
  // Accepting the wheelevent will stop all
  // other widgets from responding paticularly
  // the two tables (Stops them scolling up
  // and down with zooming)
  //
  e->accept();

  //
  // Use mapFromGlobal to hack
  // the coordinates to be relative to the window
  //
  QPoint pt = mapFromGlobal(e->globalPos());
  POINT point = {pt.x(), pt.y()};
 
  if (e->delta() > 0) {
    // See if we want to Pan as well as ...
    if (g_MouseWheel == 2) {
      g_MapWnd.PanCenter(point);
 
      QPoint cpt = mapToGlobal(QPoint(g_MapWnd.screenWidth/2, g_MapWnd.screenHeight/2));
      QCursor::setPos(cpt);
    }
    // ... Zoom
    g_MapWnd.ZoomIn();
  }
  else {
    g_MapWnd.ZoomOut();
  }

  repaint(true);  // erase background first
}


//-----------------------------------------------------------------------------
// ApplicationWindow
//

#define PANEL_BORDER 0

ApplicationWindow::ApplicationWindow()
    :QMainWindow(0, "knav application main window", WDestructiveClose)
{
  // zero the recurse counter
  m_reccnt = 0;

  printer = new QPrinter;
  printer->setPageSize(QPrinter::A4);

  openIcon = QPixmap(fileopen);
  saveIcon = QPixmap(filesave);
  printIcon = QPixmap(fileprint);

  zoomIcon = QPixmap(zoom_xpm);
  routeIcon = QPixmap(route_xpm);

  globeIcon = QPixmap(globe_xpm);
  shadeIcon = QPixmap(shade_xpm);

  aptIcon = QPixmap(apt_xpm);
  navIcon = QPixmap(nav_xpm);
  airIcon = QPixmap(air_xpm);
  gpsTrkIcon = QPixmap(hangten_16_xpm);

	wingsIcon = QPixmap(wings);
	setIcon(wingsIcon);

  createMenu();
  createToolbar();
  createWorkspace();


  //-----------------------------------
  //
  //
  view->setItemChecked(id_globe, !g_MapWnd.flatWorld);
  globe_button->setOn(!g_MapWnd.flatWorld);

  view->setItemChecked(id_relief, g_MapWnd.bShadeActive);
  shade_button->setOn(g_MapWnd.bShadeActive);

  statusBar()->message("Ready", 20000);
  resize(800, 600);
}


ApplicationWindow::~ApplicationWindow()
{
  printf("ApplicationWindow::~ApplicationWindow\n");
  delete printer;
}


//-----------------------------------------------------------------------------
// Set up the menu bar and menu items
//
ApplicationWindow::createMenu()
{
  //int id;

  //
  // File menu
  //
  QPopupMenu *file = new QPopupMenu(this);
  menuBar()->insertItem("&File", file);
    file->insertItem("&New", this, SLOT(fileNew()), CTRL+Key_N);
    id_open = file->insertItem(openIcon, "&Open", this, SLOT(fileOpen()), CTRL+Key_O);
              file->setWhatsThis(id_open, fileOpenText);
    id_save = file->insertItem(saveIcon, "&Save", this, SLOT(fileSave()), CTRL+Key_S);
              file->setWhatsThis(id_save, fileSaveText);
    id_save_as = file->insertItem("Save &as...", this, SLOT(saveAs()));
                 file->setWhatsThis(id_save_as, fileSaveText);
    file->insertSeparator();
    id_print = file->insertItem(printIcon, "&Print", this, SLOT(print()), CTRL+Key_P);
               file->setWhatsThis(id_print, filePrintText);
    file->insertSeparator();
    file->insertItem("&Close", this, SLOT(close()), CTRL+Key_W);
    file->insertItem("&Quit", qApp, SLOT(closeAllWindows()), CTRL+Key_Q);

  //
  // Edit menu
  //
  edit = new QPopupMenu(this);
  menuBar()->insertItem("&Edit", edit);
    id_undo = edit->insertItem(/*openIcon,*/ "&Undo", this, SLOT(editUndo()), CTRL+Key_Z);
              //edit->setWhatsThis(id, editUndoText);
    id_redo = edit->insertItem(/*openIcon,*/ "&Redo", this, SLOT(editRedo()), CTRL+Key_Y);
              //edit->setWhatsThis(id, editUndoText);

    edit->insertSeparator();
    edit->insertItem("&Preferences", this, SLOT(preferences())/*, CTRL+Key_?*/);

    // deprecated
    //edit->insertItem("&Startup", this, SLOT(startup())/*, CTRL+Key_?*/);

    edit->setItemEnabled(id_redo, false);

  //
  // View menu
  //
  view = new QPopupMenu(this);
  menuBar()->insertItem("&View", view);
    id_rose = view->insertItem("Ro&se", this, SLOT(rose()), CTRL+Key_R);
              view->setItemChecked(id_rose, g_MapWnd.bRoseActive);

    id_noderose = view->insertItem("No&de Rose", this, SLOT(noderose()), CTRL+Key_D);
                 view->setItemChecked(id_noderose, g_MapWnd.bNodeRoseActive);

    id_globe = view->insertItem(/*globeIcon,*/ "Glo&be", this, SLOT(globe()), CTRL+Key_B);
               view->setItemChecked(id_globe, !g_MapWnd.flatWorld);
    id_relief = view->insertItem("Relie&f", this, SLOT(relief()), CTRL+Key_F);
                view->setItemChecked(id_relief, g_MapWnd.bShadeActive);
    view->insertSeparator();
 
    id_apt = view->insertItem(aptIcon, "Airport", this, SLOT(toolbar_apt())/*, CTRL+Key_?*/);
    if ((g_wptMask & W_APT) == W_APT)
      view->setItemChecked(id_apt, true);
    else
      view->setItemChecked(id_apt, false);

    id_nav = view->insertItem(navIcon, "NavAids", this, SLOT(toolbar_nav())/*, CTRL+Key_?*/);
    if ((g_wptMask & (W_VOR | W_NDB)) == (W_VOR | W_NDB))
      view->setItemChecked(id_nav, true);
    else
      view->setItemChecked(id_nav, false);

    id_air = view->insertItem(airIcon, "Airspace", this, SLOT(toolbar_air())/*, CTRL+Key_?*/);
    view->setItemChecked(id_apt, g_MapWnd.bAirActive);

    id_trk = view->insertItem(gpsTrkIcon, "Track", this, SLOT(toolbar_trk())/*, CTRL+Key_?*/);
    view->setItemChecked(id_trk, g_MapWnd.bGpsTrkActive);

    view->insertSeparator();
    view->insertItem("&Refresh", this, SLOT(refresh()), Key_F5);

  //
  // Action menu
  //
  QPopupMenu *action = new QPopupMenu(this);
  menuBar()->insertItem("&Action", action);
    action->insertItem("Zoom &All", this, SLOT(zoomall()), CTRL+Key_L);
    action->insertItem("Zoom &Track", this, SLOT(zoomtrack()), CTRL+Key_T);
    action->insertSeparator();
    action->insertItem("Expand Database", this, SLOT(expanddatabase()), CTRL+Key_X);
    action->insertItem("Collapse Database", this, SLOT(collapsedatabase()), CTRL+Key_E);

  //
  // Tools menu
  //
  QPopupMenu *tools = new QPopupMenu(this);
  menuBar()->insertItem("&Tools", tools);
    tools->insertItem("Mass&Balance", this, SLOT(massbalance()), Key_F2);
    tools->insertItem("E6B Computer", this, SLOT(e6bcomputer()), Key_F3);
 
    /*
    // Flight and Planning Logs as a popup
    QPopupMenu *output = new QPopupMenu(tools);
      output->insertItem("Flight Log", this, SLOT(flightlog()), Key_F4);
      output->insertItem("Planning Log", this, SLOT(planninglog()), Key_F5);
    tools->insertItem("Output", output);
    */
    // Flight and Planning Logs between seperators
    tools->insertSeparator();
    tools->insertItem("Flight Log", this, SLOT(flightlog()), Key_F7);
    tools->insertItem("Planning Log", this, SLOT(planninglog()), Key_F8);

    tools->insertSeparator();
    tools->insertItem("Briefing", this, SLOT(briefing()));
    tools->insertItem("Weather", this, SLOT(weather()));
    tools->insertItem("Duats/Naips", this, SLOT(duats_naips()));
    tools->insertItem("Flight Plan", this, SLOT(flightplan()));
    tools->insertSeparator();
    QPopupMenu *download = new QPopupMenu(tools);
      download->insertItem("Track", this, SLOT(dl_track()));
      download->insertItem("Route", this, SLOT(dl_route()));
      download->insertItem("Waypoint", this, SLOT(dl_waypt()));
    tools->insertItem("Download", download);

  //
  // Help menu
  //
  QPopupMenu * help = new QPopupMenu(this);
  menuBar()->insertSeparator();
  menuBar()->insertItem("&Help", help);
    help->insertItem("&Index", this, SLOT(index()), Key_F1);
    help->insertItem("&About", this, SLOT(about()));
    help->insertSeparator();
    // help->insertItem("&Qt", this, SLOT(aboutQt()));
    help->insertItem("What's &This", this, SLOT(whatsThis()), SHIFT+Key_F1);

}

//-----------------------------------------------------------------------------
// Create our tool bar
//
ApplicationWindow::createToolbar()
{
  //-----------------------------------
  // Set up a "standard" toolbar
  //
  fileTools = new QToolBar(this, "file operations");
  fileTools->setLabel(tr("File Operations") );

  QToolButton *fileOpen = new QToolButton(openIcon, "Open File", QString::null,
                                          this, SLOT(fileOpen()), fileTools, "open file");

  QToolButton *fileSave = new QToolButton(saveIcon, "Save File", QString::null,
                                          this, SLOT(fileSave()), fileTools, "save file");

  QToolButton * filePrint = new QToolButton(printIcon, "Print File", QString::null,
                                            this, SLOT(print()), fileTools, "print file");

  (void)QWhatsThis::whatsThisButton(fileTools);

  QWhatsThis::add(fileOpen, fileOpenText);
  QWhatsThis::add(fileSave, fileSaveText);
  QWhatsThis::add(filePrint, filePrintText);

  QMimeSourceFactory::defaultFactory()->setPixmap("fileopen", openIcon);

  //-----------------------------------
  // Set up a "action" toolbar
  //
  actionTools = new QToolBar(this, "user operations");
  actionTools->setLabel(tr("User Operations") );

  zoom_button = new QToolButton(zoomIcon, "Zoom", QString::null, this, SLOT(zoom()), actionTools, "toggle zoom/pan");
  zoom_button->setToggleButton(true);

  route_button = new QToolButton(routeIcon, "Route", QString::null, this, SLOT(route()), actionTools, "toggle add route");
  route_button->setToggleButton(true);

  globe_button = new QToolButton(globeIcon, "Globe", QString::null, this, SLOT(globe()), actionTools, "toggle globe");
  globe_button->setToggleButton(true);

  shade_button = new QToolButton(shadeIcon, "Shade", QString::null, this, SLOT(relief()), actionTools, "toggle shade");
  shade_button->setToggleButton(true);

  /* TBD
  QPixmap radarIcon = QPixmap(sat_xpm);
  radar_button = new QToolButton(radarIcon, "shade", QString::null, this, SLOT(radar()), actionTools, "toggle shade");
  radar_button->setToggleButton(true);
  */


  QWhatsThis::add(zoom_button,  zoom_buttonText);
  QWhatsThis::add(route_button, route_buttonText);
  QWhatsThis::add(globe_button, globe_buttonText);
  QWhatsThis::add(shade_button, shade_buttonText);


  //-----------------------------------
  // Set up a "filter" toolbar
  //
  filterTools = new QToolBar(this, "user operations");
  filterTools->setLabel(tr("User Operations") );

  apt_button = new QToolButton(aptIcon, "Apt", QString::null, this, SLOT(toolbar_apt()), filterTools, "toggle Apt on/off");
  apt_button->setToggleButton(true);
  if ((g_wptMask & W_APT) == W_APT) 
    apt_button->setOn(true);  //apt_button->toggle();

  nav_button = new QToolButton(navIcon, "Nav", QString::null, this, SLOT(toolbar_nav()), filterTools, "toggle Nav on/off");
  nav_button->setToggleButton(true);
  int a = W_VOR | W_NDB;
  int b = g_wptMask & a;
  if ((g_wptMask & (W_VOR | W_NDB)) == (W_VOR | W_NDB))
    nav_button->setOn(true);  // nav_button->toggle();  

  air_button = new QToolButton(airIcon, "Airspace", QString::null, this, SLOT(toolbar_air()), filterTools, "toggle Gps track on/off");
  air_button->setToggleButton(true);
  air_button->setOn(g_MapWnd.bAirActive);

  gpstrk_button = new QToolButton(gpsTrkIcon, "GpsTrk", QString::null, this, SLOT(toolbar_trk()), filterTools, "toggle Gps track on/off");
  gpstrk_button->setToggleButton(true);
  gpstrk_button->setOn(g_MapWnd.bGpsTrkActive);


  QWhatsThis::add(apt_button, apt_buttonText);
  QWhatsThis::add(nav_button, nav_buttonText);
  QWhatsThis::add(gpstrk_button, gpstrk_buttonText);
  QWhatsThis::add(air_button, air_buttonText);

  ////QPixmap shadeIcon = QPixmap(shade_xpm);
  //shade_button = new QToolButton(shadeIcon, "shade", QString::null, this, SLOT(relief()), filterTools, "toggle shade");
  //shade_button->setToggleButton(true);

  //QWhatsThis::add(zoom_button,  zoom_buttonText);
  //QWhatsThis::add(route_button, route_buttonText);
  //QWhatsThis::add(globe_button, globe_buttonText);
  //QWhatsThis::add(shade_button, shade_buttonText);
}

//-----------------------------------------------------------------------------
// Create our basic workspace consisting of all the panes and various
// splitters
//
ApplicationWindow::createWorkspace()
{

  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  int iLayout = iniFile.GetValueI("Prefs", "Layout", 0);

  //
  // Make a central widget to contain the other widgets that will make up
  // the "work area" of the application
  //
  central = new QWidget(this);
  setCentralWidget(central);

  // Create a layout to position the widgets
  QHBoxLayout *topLayout = new QHBoxLayout(central, PANEL_BORDER);

  //-----------------------------------
  // Horizontal splitter
  //
  QSplitter *splitH = new QSplitter(QSplitter::Horizontal, central, "splitter");
  //splitH->setOpaqueResize(TRUE);
  topLayout->addWidget(splitH, 1);
  QWidget *leftPanel = new QWidget(splitH);
  leftPanel->setMinimumSize(MIN_WIDGET_SIZE, MIN_WIDGET_SIZE);

  QVBoxLayout *leftLayout = new QVBoxLayout(leftPanel, PANEL_BORDER);
  QSplitter *splitV0 = new QSplitter(QSplitter::Vertical, leftPanel, "splitter");
  leftLayout->addWidget(splitV0, 1);

  //
  // Set up the positions of the browser and tree
  // based on the setting of Layout in the knav.ini
  //
  if (iLayout == 0) {
    m_htmlBrowser = new UTextBrowser(splitV0);
    m_top = new QListView(splitV0);
  }
  else {
    m_top = new QListView(splitV0);
    m_htmlBrowser = new UTextBrowser(splitV0);
  }

  //-----------------------------------
  // Text Browser
  //
  m_htmlBrowser->setSource("default.html");

  //--------------
  // Build the "tree"
  //
  m_top->setRootIsDecorated(true);
  //top->setSorting(-1, true);
  createDatabaseTree();
  createDownloadTree();

  m_top->setMinimumSize(MIN_WIDGET_SIZE, MIN_WIDGET_SIZE);
  m_top->addColumn("One");  //m_top->addColumn("Two");
  m_top->setAllColumnsShowFocus(TRUE);
  m_top->setSelectionMode(QListView::NoSelection);

  QToolTip::add(m_top, "list view");
  QWhatsThis::add(m_top, "This is the <b>Waypoint Database</b> and <b>Garmin GPS Download</b> tree lists");
  connect(m_top, SIGNAL(mouseButtonClicked(int, QListViewItem *, const QPoint &, int)), this, SLOT(lv_mouseButtonClicked(int, QListViewItem *, const QPoint &, int)));

  //-----------------------------------
  // Vertical splitter
  //
  QWidget *rightPanel = new QWidget(splitH);
  QHBoxLayout *rightLayout = new QHBoxLayout(rightPanel, PANEL_BORDER); //10
  QSplitter *splitV1 = new QSplitter(QSplitter::Vertical, rightPanel, "splitter");
 
  //
  // hardcode it to false for now
  //
  bool bOpaqueResize = false;
  if (bOpaqueResize)
    splitV1->setOpaqueResize(true);
  else
    splitV1->setOpaqueResize(false);

  rightLayout->addWidget(splitV1, 1);

  //--------------
  pMapWidget = new MapWidget(splitV1);
  //pMapWidget->resize(640, 480);

  // +-------+--------------------+
  // |       |                    |
  // |       |                    |
  // |       +--------V1----------+
  // +--V0---+                    |   
  // |       |                    |
  // |       H                    |
  // |       |                    |
  // +-------+--------------------+
  //
  // All the splitters are now created and
  // we can set the initial sizes of the contained widgets.
  // This has the effect of effectively setting the
  // "initial" positions of the splitter sashes
  //
  QValueList <int> sizes;
  sizes.clear();
  sizes << 200 << 800;
  splitH->setSizes(sizes);

  sizes.clear();
  sizes << 300 << 300;
  splitV0->setSizes(sizes);

  sizes.clear();
  sizes << 480 << 300;
  splitV1->setSizes(sizes);


  //
  // Register interest in the infoChanged() signal. The signal
  // is emitted when a right mouse click on a wpt is detected which
  // is handled by displaying the relevant html page for that wpt
  //
  connect(pMapWidget, SIGNAL(infoChanged()), this, SLOT(infoChanged()));
  connect(pMapWidget, SIGNAL(latlonChanged()), this, SLOT(latlonChanged()));

  // capture mouse movements even if no buttons are pressed
  pMapWidget->setMouseTracking(true);

  // Allow the map widget to grab the focus. This is particularly useful
  // when the focus has been on one of the tables
 
  //pMapWidget->setFocusPolicy(QWidget::ClickFocus); //QWidget::StrongFocus);// | QWidget::WheelFocus);

  //
  // This is pretty bizarre - I suppose it is
  // somewhat :-) of a hack to get around the fact
  // that we need to connect the table to the tabs
  // and the whole lot to the messages.
  //
  QTabWidget* tabs;

  tabs = new QTabWidget(splitV1);
  tabs->setTabPosition( QTabWidget::Bottom );
  tabs->setMargin(4);
  tabs->setMinimumSize(MIN_WIDGET_SIZE, MIN_WIDGET_SIZE);

  QTable *pMagneticTable = new QTable(NR_LEGS + 2, MAXCOLOM, tabs, "mag table");
  QTable *pTrueTable     = new QTable(NR_LEGS + 2, MAXCOLOM, tabs, "true table");

  pTrueTable->setFocusPolicy(QWidget::NoFocus);

  g_MapWnd.trueTable = pTrueTable;
  g_MapWnd.trueTable->horizontalHeader()->setLabel(0, "ICAO");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(1, "Info");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(2, "Lat");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(3, "Lon");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(4, "Elev '");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(5, "Var �");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(6, "Trk �T");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(7, "Dist nm");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(8, "TAS kts");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(9, "Wind ");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(10, "Hdg �T");
  g_MapWnd.trueTable->horizontalHeader()->setLabel(11, "GS kts");
  if (g_MapWnd.m_Units == 0)
    g_MapWnd.trueTable->horizontalHeader()->setLabel(12, "FF lit/hr");
  else
    g_MapWnd.trueTable->horizontalHeader()->setLabel(12, "FF usg/hr");
 
  connect(pTrueTable, SIGNAL(valueChanged(int, int)), this, SLOT(trueTableValueChanged(int, int)));
  connect(pTrueTable, SIGNAL(clicked(int, int, int, const QPoint&)), this, SLOT(trueTableClicked(int, int, int, const QPoint&)));
  tabs->addTab(pTrueTable, "WorkSheet - True");

  g_MapWnd.magneticTable = pMagneticTable;
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(0, "ICAO");
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(1, "Info");
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(2, "Trk �M");
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(3, "Dist nm");
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(4, "TAS kts");
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(5, "Wind ");
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(6, "Hdg �M");
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(7, "GS kts");
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(8, "Time");
  if (g_MapWnd.m_Units == 0)
    g_MapWnd.magneticTable->horizontalHeader()->setLabel(9, "Fuel lit");
  else
    g_MapWnd.magneticTable->horizontalHeader()->setLabel(9, "Fuel usg");

  g_MapWnd.magneticTable->horizontalHeader()->setLabel(10, "  "); // empty
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(11, "  "); // empty
  g_MapWnd.magneticTable->horizontalHeader()->setLabel(12, "  "); // empty

  connect(pMagneticTable, SIGNAL(clicked(int, int, int, const QPoint&)), this, SLOT(magneticTableClicked(int, int, int, const QPoint&)));
  tabs->addTab(pMagneticTable, "Result - Magnetic");

  autofit();
 
  //MassBalanceGadget *massBalance = new MassBalanceGadget();
  //tabs->addTab(massBalance, "Mass & Balance");

  // Add a massbalance gadget inside a scrollview to
  // the tabs.
  QScrollView *sv = new QScrollView(tabs);
  m_massBalanceGadget = new MassBalanceGadget(sv->viewport());
  sv->addChild(m_massBalanceGadget);
  sv->setResizePolicy(QScrollView::AutoOneFit);  // very nifty this :-)
  tabs->addTab(sv, "Mass & Balance");

  //
  // Register interest in the aircraftChanged() signal. The signal
  // is emitted when a new aircraft is chosen.
  //
  connect(m_massBalanceGadget, SIGNAL(aircraftChanged()), this, SLOT(aircraftChanged()));
}


void ApplicationWindow::updateMRU(const char *szfileName)
{
  char sKrt[256];
  //QString kv0, kv1;
  int i;

  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iniFile.SetValue("Prefs", "LastUsedKrt", szfileName);
  
  for (i = 5; i > 0; i--) {
    QString kv0 = QString("%1").arg(i);
    QString kv1 = QString("%1").arg(i+1);
    
    iniFile.GetValueS(sKrt, "MRU", kv0.latin1(), " ");
    iniFile.SetValue("MRU", kv1.latin1(), sKrt);
  }
  iniFile.SetValue("MRU", "1", szfileName);
  iniFile.WriteFile();
}


//-----------------------------------------------------------------------------
// Make "our" version of setCaption called setTitle. For convenience
// we will create the composite string with the required "Qt" as well
// as the stuff we want; Current filename (.krt) and aircraft definition (.ad)
//
void ApplicationWindow::setTitle()
{
  char sLastAd[256];
  char sLastKrt[256];
 
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iniFile.GetValueS(sLastAd, "Prefs", "LastUsedAd", " ");
  iniFile.GetValueS(sLastKrt, "Prefs", "LastUsedKrt", " ");

  setCaption(QString("%1 - %2 - %3").arg(SZ_APPNAME).arg(sLastAd).arg(sLastKrt));
}

//
// Menu slots
//

//-----------------------------------------------------------------------------
// File|New handler
//
void ApplicationWindow::fileNew()
{
  if (testChange() == 1)
    return;

  g_MapWnd.clearRoute();     // Clear any existing file
  m_filename = (QString) NULL;

  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iniFile.SetValue("Prefs", "LastUsedKrt", "<none>");
  iniFile.WriteFile();

  pMapWidget->repaint(true); //erase back
  setTitle();
}

//----------------------------------------------------------------------------->
// File|Open handler
//
void ApplicationWindow::fileOpen()
{
  // DEMO VERSION
  QMessageBox::information(this, SZ_PRGNAME,
                           "This feature is unfortunately not available in "
                           "the demonstration version");
  // DEMO VERSION
}


void ApplicationWindow::fileLoad(const char *szfileName)
{
  // DEMO VERSION
  QMessageBox::information(this, SZ_PRGNAME,
                           "This feature is unfortunately not available in "
                           "the demonstration version");
  // DEMO VERSION
}

//-----------------------------------------------------------------------------
// File | Save handler
//
void ApplicationWindow::fileSave()
{
  // DEMO VERSION
  QMessageBox::information(this, SZ_PRGNAME,
                           "This feature is unfortunately not available in "
                           "the demonstration version");
  // DEMO VERSION
}

//-----------------------------------------------------------------------------
// File | saveAs handler
//
void ApplicationWindow::saveAs()
{
  // DEMO VERSION
  QMessageBox::information(this, SZ_PRGNAME,
                           "This feature is unfortunately not available in "
                           "the demonstration version");
  // DEMO VERSION
}



//-----------------------------------------------------------------------------
// Action | Expand handler
//
void ApplicationWindow::expanddatabase()
{
  expandAllDatabaseTree();
}

//-----------------------------------------------------------------------------
// Action | Collapse handler
//
void ApplicationWindow::collapsedatabase()
{
  collapseAllDatabaseTree();
}


//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::helper_print_text(GraphicPainter *p, QString fileName)
{
  printf("ApplicationWindow::print_text\n");

  QFile file(fileName);
  file.open(IO_ReadOnly);
  QTextStream fdata(&file);

  QFont font("Courier", 9);
  p->setFont(font);
  QFontMetrics fm = p->fontMetrics();
  int yPos    = 0;            // y position for each line

  while (!fdata.eof()) {
    yPos = yPos + fm.lineSpacing();
    //fdata.readLine(s, 255);
    QString s = fdata.readLine();
    printf(s);
    p->drawString(s, 200, yPos);
  }
  file.close();
}

/*
void ApplicationWindow::print_text(GraphicPainter *p)
{
  QString filename = QString("%1/flightlog~%2.txt").arg(getenv("TEMP")).arg(0);
  print_text(p, filename);
}
*/

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::helper_print_graphics(GraphicPainter *p)
{
	QPaintDeviceMetrics pdm(printer);
  int w = pdm.width();
  int h = pdm.height();
  g_MapWnd.Size(w, h);

	g_MapWnd.bPolygonActive = false;
  g_MapWnd.PaintMain(p);  // the whole echilada
	g_MapWnd.bPolygonActive = true;
}

void ApplicationWindow::helper_print_massbalance(GraphicPainter *p)
{
  QRect r = p->window();
  int wid = r.width();
 
  m_massBalanceGadget->m_envelopePanel->PaintMain(p);
 
  QFont font("Courier", 9);
  p->setFont(font);
  QFontMetrics fm = p->fontMetrics();
  int yPos = 0;            // y position for each line
  QString s;

  switch (m_massBalanceGadget->m_iUnits) {
    case 0:
      yPos = yPos + fm.lineSpacing();
      p->drawString(tr("ITEM"), wid*0.25, yPos);
      p->drawString(tr("ARM mm"), wid*0.50, yPos);
      p->drawString(tr("WEIGHT kg"), wid*0.75, yPos);
      break;

    case 1:
      yPos = yPos + fm.lineSpacing();
      p->drawString(tr("ITEM"), wid*0.25, yPos);
      p->drawString(tr("ARM inches"), wid*0.50, yPos);
      p->drawString(tr("WEIGHT lbs"), wid*0.75, yPos);
      break;
  }


  for (int i = 0; i < m_massBalanceGadget->m_iNrPoints; i++) {
    yPos = yPos + fm.lineSpacing();
    s = QString(m_massBalanceGadget->item[i]->text());
    p->drawString(s, wid*0.25, yPos);
    s = QString(m_massBalanceGadget->arm[i]->text());
    p->drawString(s, wid*0.50, yPos);
    s = QString(m_massBalanceGadget->weight[i]->text());
    p->drawString(s, wid*0.75, yPos);
  }
}


//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::print()
{
  g_MapWnd.m_bPrnActive = TRUE;

  TripKit dlg(this, "tripkit", " ", TRUE);
  dlg.setCaption(QString("%1 - %2").arg(SZ_APPNAME).arg("Trip Kit"));
  int rv = dlg.exec();
  if (rv == QDialog::Rejected)
    return;
 
  //-----------------------------------
  // Print prepartion
  //
  double _top;
  double _bottom;
  double _right;
  double _left;
  int _mag;
  int _mag2;
  int _screenWidth;
  int _screenHeight;
  double _convY;
  double _convX;

  _left    = g_MapWnd.left;
  _top     = g_MapWnd.top;
  _right   = g_MapWnd.right;
  _bottom  = g_MapWnd.bottom;
  _convX   = g_MapWnd.convX;
  _convY   = g_MapWnd.convY;
  _mag     = g_MapWnd.mag;
  _mag2    = g_MapWnd.mag2;

  _screenWidth = g_MapWnd.screenWidth;
  _screenHeight = g_MapWnd.screenHeight;

  g_MapWnd.bChange = true;

  //-----------------------------------
  // Print drawing
  //
  if (printer->setup(this)) {
    QPainter p(printer);

    bool bSomethingPrinted = false;

    // not quite sure why - but needs this otherwise auto zoomed
    // prints the first one wrong
    QPaintDeviceMetrics pdm(printer);
    int w = pdm.width();
    int h = pdm.height();
    g_MapWnd.Size(w, h);
    
    //
    // The current window
    //
    if (dlg.cbMapwin->isChecked()) {
      helper_print_graphics((GraphicPainter *) &p);
      bSomethingPrinted = true;
    }

    
    if (dlg.cbTripKit->isChecked()) {

      //
      // Full Route
      //
      if (dlg.cbRouteFull->isChecked()) {
        if (bSomethingPrinted)
          printer->newPage();

        g_MapWnd.ZoomToTrack(); 
        helper_print_graphics((GraphicPainter *) &p);
        bSomethingPrinted = true;
      }

      //
      // Route Legs
      //
      if (dlg.cbRouteLegs->isChecked()) {
        if (bSomethingPrinted)
          printer->newPage();

        double lat1, lon1; //, lat2, lon2;

        /* zoom to fit each leg fully on a page 
           ???
        for (int i = 0; i < g_MapWnd.RouteWpt - 1; i++) {
          if (g_MapWnd.RouteLst[i] != 0) {
            g_MapWnd.wpt.MoveFirst();
            g_MapWnd.wpt.Move(g_MapWnd.RouteLst[i]);
            lat1 = g_MapWnd.wpt.m_rLat;
            lon1 = g_MapWnd.wpt.m_rLon;
 
            g_MapWnd.wpt.Move(g_MapWnd.RouteLst[i+1]);
            lat2 = g_MapWnd.wpt.m_rLat;
            lon2 = g_MapWnd.wpt.m_rLon;

            g_MapWnd.ZoomToWorld();
            g_MapWnd.ZoomToRect(lat1, lat2, lon1, lon2);
            print_graphics((GraphicPainter *) &p);
            printer->newPage();
          }
        }
        */

        // Plot a 60nm radius (at least) around each vertex
        for (int i = 0; i < g_MapWnd.RouteWpt; i++) {
          if (g_MapWnd.RouteLst[i] != 0) {
            g_MapWnd.wpt.Move(g_MapWnd.RouteLst[i]);
            lat1 = g_MapWnd.wpt.m_rLat;
            lon1 = g_MapWnd.wpt.m_rLon;

            g_MapWnd.ZoomToRect(lat1-1.0, lat1+1.0, lon1-1.0, lon1+1.0);
            helper_print_graphics((GraphicPainter *) &p);
            printer->newPage();

            bSomethingPrinted = true;
          }
        }
      }

      //
      // Mass and balance
      //
      if (dlg.cbMassBalance->isChecked()) {
        if (bSomethingPrinted)
          printer->newPage();

        helper_print_massbalance((GraphicPainter *) &p);
        bSomethingPrinted = true;
      }

      //
      // Flightlog
      //
      if (dlg.cbFlightLog->isChecked()) {
        if (bSomethingPrinted)
          printer->newPage();

        // the text version
        // QString fileName = QString("%1/flightlog~%2.txt").arg(getenv("TEMP")).arg(0);
        // flightlogTxt(fileName);
        // helper_print_text((GraphicPainter *) &p, fileName);

        // the html version
        QDateTime dt = QDateTime::currentDateTime();
        QDateTime dt0;
        unsigned long timestamp = dt.secsTo(dt0);

        QString fileName = QString("%1/flightlog~%2.html").arg(getenv("TEMP")).arg(timestamp);
        flightlogHtml(fileName);
        html_print(fileName);
        bSomethingPrinted = true;
      }
    
      //
      // Planning Log
      //
      if (dlg.cbPlanningLog->isChecked()) {
        if (bSomethingPrinted)
          printer->newPage();

        // the text version
        // QString fileName = QString("%1/planninglog~%2.txt").arg(getenv("TEMP")).arg(0);
        // planninglogTxt(fileName);
        // helper_print_text((GraphicPainter *) &p, fileName);

        // the html version
        QDateTime dt = QDateTime::currentDateTime();
        QDateTime dt0;
        unsigned long timestamp = dt.secsTo(dt0);

        QString fileName = QString("%1/planninglog~%2.html").arg(getenv("TEMP")).arg(timestamp);
        planninglogHtml(fileName);
        html_print(fileName);
        bSomethingPrinted = true;
      }
    }
  }

  //-----------------------------------
  // Print cleanup
  //
  g_MapWnd.left   = _left;
  g_MapWnd.top    = _top;
  g_MapWnd.right  = _right;
  g_MapWnd.bottom = _bottom;
  g_MapWnd.convX  = _convX;
  g_MapWnd.convY  = _convY;
  g_MapWnd.mag    = _mag;
  g_MapWnd.mag2   = _mag2;

  g_MapWnd.screenWidth  = _screenWidth;
  g_MapWnd.screenHeight = _screenHeight;
  g_MapWnd.Size(_screenWidth, _screenHeight);

  g_MapWnd.bChange = true;
  g_MapWnd.m_bPrnActive = FALSE;

}

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::toolbar_apt()
{
  printf("ApplicationWindow::toolbar_apt\n");

  //if (apt_button->isOn()) {
  if ((g_wptMask & W_APT) != W_APT) {
    g_wptMask = g_wptMask | W_APT;
    view->setItemChecked(id_apt, true);
    apt_button->setOn(true);
  }
  else {
    g_wptMask = g_wptMask & (~W_APT);
    view->setItemChecked(id_apt, false);
    apt_button->setOn(false);
  }

  pMapWidget->repaint(true);
}

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::toolbar_nav()
{
  printf("ApplicationWindow::toolbar_nav\n");

  //if (nav_button->isOn()) {
  if ((g_wptMask & (W_VOR | W_NDB)) != (W_VOR | W_NDB)) {
    g_wptMask = g_wptMask | W_VOR | W_NDB;
    view->setItemChecked(id_nav, true);
    nav_button->setOn(true);
  }
  else {
    g_wptMask = g_wptMask & ( ~(W_VOR | W_NDB));
    view->setItemChecked(id_nav, false);
    nav_button->setOn(false);
  }

  pMapWidget->repaint(true);
}

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::toolbar_trk()
{
  printf("ApplicationWindow::toolbar_trk\n");

  g_MapWnd.bGpsTrkActive = !g_MapWnd.bGpsTrkActive;
  view->setItemChecked(id_trk, g_MapWnd.bGpsTrkActive);
  gpstrk_button->setOn(g_MapWnd.bGpsTrkActive);

  pMapWidget->repaint(true);
}

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::toolbar_air()
{
  printf("ApplicationWindow::toolbar_air\n");

  g_MapWnd.bAirActive = !g_MapWnd.bAirActive;
  view->setItemChecked(id_air, g_MapWnd.bAirActive);
  air_button->setOn(g_MapWnd.bAirActive);

  pMapWidget->repaint(true);
}


//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::zoom()
{
  g_MapWnd.bZoomActive = !g_MapWnd.bZoomActive;
 
  //
  // Reset bAddRteActive because we cannot
  // zoom or pan and add routes at the same time
  //
  if (g_MapWnd.bAddRteActive) {
    g_MapWnd.bAddRteActive = !g_MapWnd.bAddRteActive;
    route_button->toggle();
  }

  if (g_MapWnd.bZoomActive) {
    pMapWidget->setCursor(sizeAllCursor);
  }
  else {
    pMapWidget->setCursor(pointingHandCursor);
  }

  statusBar()->message(QString("RouteActive %1 ZoomActive %2").arg(g_MapWnd.bAddRteActive).arg(g_MapWnd.bZoomActive), 2000);
}

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::zoomall()
{
  printf("ZoomToWorld\n");
  g_MapWnd.ZoomToWorld();
  pMapWidget->repaint(true);
}

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::zoomtrack()
{
  printf("ZoomToTrack\n");
  g_MapWnd.ZoomToTrack();
  pMapWidget->repaint(true);
}


//-----------------------------------------------------------------------------
// Pop up the Mass and Balance dialog
//
void ApplicationWindow::massbalance()
{
  MassBalanceDlg dlg(this, "massbalance", " ", TRUE);
  dlg.resize( 450, 350 );
  dlg.setCaption(QString("%1 - %2").arg(SZ_APPNAME).arg("Mass & Balance"));
  //dlg.show();
  dlg.exec();

  // we may have chosen a new .ad so refresh the caption
  aircraftChanged();
}

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::e6bcomputer()
{
  E6BDlg dlg(this, "e6bcomputer", " ", TRUE);
  dlg.resize( 450, 350 );
  dlg.setCaption(QString("%1 - %2").arg(SZ_APPNAME).arg("e6b Computer"));
  //dlg.show();
  dlg.exec();
}

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::flightlog()
{
  printf("ApplicationWindow::flightlog\n");

  printf("calling the text function\n");
  QString fileName = QString("%1/flightlog~%2.txt").arg(getenv("TEMP")).arg(0);
  flightlogTxt(fileName);

  printf("calling the html function\n");
  QDateTime dt = QDateTime::currentDateTime();
  QDateTime dt0;
  unsigned long timestamp = dt.secsTo(dt0);

  fileName = QString("%1/flightlog~%2.html").arg(getenv("TEMP")).arg(timestamp);
  flightlogHtml(fileName);
  html_browse(fileName);
}

//-----------------------------------------------------------------------------
//
//
void header_line(QTextStream *fdata)
{
  *fdata << ("<!-- Header Line -->\n");
  *fdata << ("<table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" width=\"500\" bordercolor=\"#FF0000\">\n");
  *fdata << ("<tr>\n");
  *fdata << ("    <td valign=\"top\" colspan=\"3\" width=\"39%\" height=\"31\"> <p align=\"left\"><font size=\"5\"> <b>Reg:......-......</b></font></p> </td>\n");
  *fdata << ("    <td valign=\"top\" colspan=\"4\" width=\"61%\" height=\"31\"> <p align=\"right\"><font size=\"5\"> <b>Date:......-......-......</i></b></font></p> </td>\n");
  *fdata << ("</tr>\n");
  *fdata << ("</table>\n");
  *fdata << ("<!-- Header Line -->\n");
}


//-----------------------------------------------------------------------------
// Create a html flight (cockpit) log
//
void ApplicationWindow::flightlogHtml(QString fileName)
{
  int nItem;
  QString s;
  int nTotItem = g_MapWnd.RouteWpt;

  QFile file(fileName);
  file.open(IO_WriteOnly);
  QTextStream fdata(&file);

  fdata << ("<html>\n");
  fdata << ("<body>\n");

  header_line(&fdata);

  fdata << ("<br><!-- Log Headings -->\n");
  fdata << ("<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\">\n");
 
  //#define BGCOLOR "#1f1f1f"
  #define BGCOLOR "#0000ff"
 
  fdata << ("<tr>\n");
  fdata << ("    <td valign=\"top\" colspan=\"7\" width=\"39%\" bgcolor=\"" BGCOLOR  "\" height=\"32\"><font face=\"Arial\" color=\"#FFFFFF\" > <b><i>Check point &nbsp;<p>&nbsp;</p></i></b></font> </td>\n");
  fdata << ("</tr>\n");

  fdata << ("<tr>\n");
  fdata << ("    <td valign=\"top\" width=\"17%\" bgcolor=\"" BGCOLOR  "\" height=\"41\"><p align=\"center\"><font face=\"Arial\" color=\"#FFFFFF\" ><b><i>Hdg (M)</i></b></font></p></td>\n");
  fdata << ("    <td valign=\"top\" width=\"17%\" bgcolor=\"" BGCOLOR  "\" height=\"41\"><p align=\"center\"><font face=\"Arial\" color=\"#FFFFFF\" ><b><i>Dist (nm)</i></b></font></p></td>\n");
  fdata << ("    <td valign=\"top\" width=\"12%\" bgcolor=\"" BGCOLOR  "\" height=\"41\"><p align=\"center\"><font face=\"Arial\" color=\"#FFFFFF\" ><b><i>Fuel</i></b></font></p></td>\n");
  fdata << ("    <td valign=\"top\" width=\"12%\" bgcolor=\"" BGCOLOR  "\" height=\"41\"><p align=\"center\"><font face=\"Arial\" color=\"#FFFFFF\" ><b><i>GS (kt)</i></b></font></p></td>\n");
  fdata << ("    <td valign=\"top\" width=\"12%\" bgcolor=\"" BGCOLOR  "\" height=\"41\"><p align=\"center\"><font face=\"Arial\" color=\"#FFFFFF\" ><b><i>EET</i></b></font></p></td>\n");
  fdata << ("    <td valign=\"top\" width=\"12%\" bgcolor=\"" BGCOLOR  "\" height=\"41\"><p align=\"center\"><font face=\"Arial\" color=\"#FFFFFF\" ><b><i>FL</i></b></font></p></td>\n");
  fdata << ("</tr>\n");
  fdata << ("<!-- Log Headings -->\n");

  // The log itself
  for (nItem = 0; nItem < nTotItem; nItem++) {
    s = "<font face=\"Arial\"> <b><i>" + g_MapWnd.magneticTable->text(nItem, 0) + " - " + g_MapWnd.magneticTable->text(nItem, 1) + "</b></i></font>";
    // -- s += "  Other Info: ";
    // add the lat and lon in a smaller font
    s += "<br> <font size=\"2\">" + g_MapWnd.trueTable->text(nItem, 2);
    s += " " + g_MapWnd.trueTable->text(nItem, 3) +
         "  (" + g_MapWnd.trueTable->text(nItem, 4) + "ft) " + "</font>";

    fdata << ("<!-- Blanks -->\n");
    fdata << ("<tr>\n");
    fdata << ("    <td valign=\"top\" colspan=\"7\" width=\"39%\" bgcolor=\"#FFFFFF\" height=\"32\">&nbsp; " + s + "</p></i></b></font></td>\n");
    fdata << ("</tr>\n");

    // We need to stop before the last one
    if (nItem + 1 < nTotItem) {
      fdata << ("<tr>\n");
      // magnetic heading
      fdata << ("    <td valign=\"top\" width=\"17%\" height=\"32\"><font face=\"Courier\"><b>&nbsp;" + g_MapWnd.magneticTable->text(nItem+1, 6) + "&degM </b></td>\n");
      // distance in nm
      fdata << ("    <td valign=\"top\" width=\"10%\" height=\"32\"><font face=\"Courier\"><b>&nbsp;" + g_MapWnd.magneticTable->text(nItem+1, 3) + "nm</b></td>\n");
      // fuel in liters or us gallons
      if (g_MapWnd.m_Units == 0)
        fdata << ("    <td valign=\"top\" width=\"12%\" height=\"32\"><font face=\"Courier\"><b>&nbsp;" + g_MapWnd.magneticTable->text(nItem+1, 9) + "lit</b></td>\n");
      else
        fdata << ("    <td valign=\"top\" width=\"12%\" height=\"32\"><font face=\"Courier\"><b>&nbsp;" + g_MapWnd.magneticTable->text(nItem+1, 9) + "usg</b></td>\n");

      // ground speed in kt
      fdata << ("    <td valign=\"top\" width=\"10%\" height=\"32\"><font face=\"Courier\"><b>&nbsp;" + g_MapWnd.magneticTable->text(nItem+1, 7) + "kt</b></font></td>\n");
      // time ete
      fdata << ("    <td valign=\"top\" width=\"10%\" height=\"32\"><font face=\"Courier\"><b>&nbsp;" + g_MapWnd.magneticTable->text(nItem+1, 8) + "</b></font></td>\n");
 
      fdata << ("    <td valign=\"top\" width=\"12%\" height=\"32\">&nbsp;</td>\n");
      fdata << ("</tr>\n");
    }
    fdata << ("<!-- Blanks -->\n");
  }
  fdata << ("</table>\n");

  fdata << ("<!-- Totals -->\n");
  fdata << ("<p><table border=\"0\" cellpadding=\"2\" cellspacing=\"1\" width=\"500\" bordercolor=\"#FF0000\">\n");

  // Total
  fdata << ("<tr>\n");
  fdata << ("    <td valign=\"top\" width=\"10%\" height=\"32\"><font size=\"5\"><b> Summary </b></td>\n");
  fdata << ("</tr>\n");

  fdata << ("<tr>\n");
  // total distance
  fdata << ("    <td valign=\"top\" align=\"left\"  width=\"10%\" height=\"32\"><font face=\"Courier\"><b>Total Dist = " + g_MapWnd.magneticTable->text(nTotItem+1, 3) + "nm</b></font><br>\n");
  // total fuel
  if (g_MapWnd.m_Units == 0) {
    // metric
    fdata << ("    <font face=\"Courier\"><b>Total Fuel = " + g_MapWnd.magneticTable->text(nTotItem+1, 9) + "lit</b></font><br>\n");
  }
  else {
    // US
    fdata << ("    <font face=\"Courier\"><b>Total Fuel = " + g_MapWnd.magneticTable->text(nTotItem+1, 9) + "usg</b></font><br>\n");
  }

  // total time
  fdata << ("    <font face=\"Courier\"><b>Total Time = " + g_MapWnd.magneticTable->text(nTotItem+1, 8) + "</b></font></font></td>\n");
  fdata << ("</tr>\n");
 
  fdata << ("</table>\n");
  fdata << ("<!-- Totals -->\n");
  fdata << ("</body>\n");
  fdata << ("</html>\n");

  file.close();
}


void ApplicationWindow::planninglog()
{
  printf("ApplicationWindow::planninglog\n");

  printf("calling the text function\n");
  QString fileName = QString("%1/planninglog~%2.html").arg(getenv("TEMP")).arg(0);
  planninglogTxt(fileName);

  printf("calling the html function\n");

  QDateTime dt = QDateTime::currentDateTime();
  QDateTime dt0;
  unsigned long timestamp = dt.secsTo(dt0);
  fileName = QString("%1/planninglog~%2.html").arg(getenv("TEMP")).arg(timestamp);

  planninglogHtml(fileName);
  html_browse(fileName);
}

//-----------------------------------------------------------------------------
// Create a html planning (detailed) log
//
void ApplicationWindow::planninglogHtml(QString fileName)
{
  int i;
  int nItem;
  QString s;// = new String();
  int nTotItem = g_MapWnd.RouteWpt;

  /*
  QDateTime dt = QDateTime::currentDateTime();
  QDateTime dt0;
  unsigned long timestamp = dt.secsTo(dt0);
  QString fileName = QString("%1/flightlog~%2.html").arg(getenv("TEMP")).arg(timestamp);
  */

  QFile file(fileName);
  file.open(IO_WriteOnly);
  QTextStream fdata(&file);

  fdata << ("<html>\n");
  fdata << ("<body>\n");

  header_line(&fdata);
  fdata << ("<br>\n<!-- Log Headings -->\n");

  //fdata << ("<table align=left border=1 cellspacing=0 cellpadding=1>\n");
    fdata << ("<table border=\"1\" cellpadding=\"1\" cellspacing=\"0\">\n");

  fdata << ("<tr>\n");

  fdata << ("<td><b>ICAO</b></td>\n");
  fdata << ("<td><b>Info</b></td>\n");
  fdata << ("<td><b>Trk  <br><code>(M)....</b></td>\n");
  fdata << ("<td><b>Dist <br><code>(nm)...</b></td>\n");
  fdata << ("<td><b>TAS  <br><code>(kts)..</b></td>\n");
  fdata << ("<td><b>Wind <br><code>().....</b></td>\n");
  fdata << ("<td><b>Hdg  <br><code>(M)....</b></td>\n");
  fdata << ("<td><b>GS   <br><code>(kts)..</b></td>\n");
  fdata << ("<td><b>Time <br><code>(min)..</b></td>\n");
  fdata << ("<td><b>Fuel <br><code>(usg)..</b></td>\n");
  fdata << ("</tr>\n");
 
  fdata << ("<tr>\n");

  // The log itself
  for (nItem = 0; nItem < nTotItem; nItem++) {
    for (i = 0; i < 10; i++) {
        s = g_MapWnd.magneticTable->text(nItem, i);
        if (i == 1) {
          // add the lat and lon in a smaller font
          s += "<br> <font size=\"2\">" + g_MapWnd.trueTable->text(nItem, 2);
          s += " " + g_MapWnd.trueTable->text(nItem, 3) +
               "  (" + g_MapWnd.trueTable->text(nItem, 4) + "ft) " + "</font>";
        }
        if (i > 1) {
          fdata << ("<td align=right valign=middle >&nbsp;" + s);
        }
        else {
          fdata << ("<td>&nbsp;" + s);
        }
    }
    fdata << ("<tr>\n");
  }

  nItem = nTotItem + 1;
  // The totals line
  for (i = 0; i < 10; i++) {
    s = g_MapWnd.magneticTable->text(nItem, i);
    if (i > 1) {
      fdata << ("<td align=right valign=middle >&nbsp; <b>" + s);
    }
    else {
      fdata << ("<td>&nbsp;<b>" + s);
    }
  }
  fdata << ("</table>\n");
  fdata << ("</body>\n");
  fdata << ("</html>\n");

  file.close();
}


//-----------------------------------------------------------------------------
// Utility member to setup a string with some tab postions. We use this
// mainly to make up the tables for the text only logs.
//
QString SetBuff(int tab[], char c, char d)
{
  int i;
  QString st;

  for (i = 0; i < 96; i++) {
    st[i] = c;
  }

  for (i = 0; i < 11; i++) {
    st[tab[i]] = d;
  }
 
  //st[tab[10] + 1] = 0;
  st[84] = 0;
  return st;
}

void ApplicationWindow::flightlogTxt(QString fileName)
{
  printf("ApplicationWindow::flightlogTxt\n");

  QDateTime dt = QDateTime::currentDateTime();
  QDateTime dt0;
  unsigned long timestamp = dt.secsTo(dt0);

  QFile file(fileName);
  file.open(IO_WriteOnly);
  QTextStream fdata(&file);

  int i;
  QString s;
  QString st;
  int nTotItem = g_MapWnd.RouteWpt;
  int nHeading = 6;
  int nItem;
  const int iRowHgt = 9;// 14;

  //                 0       1       2      3      4      5       6      7     8       9
  //QString hdg[] = {"ICAO", "Info", "Trk", "Dis", "TAS", "Wind", "Hdg", "GS", "Time", "Fuel"};
  //int tab[] =     {0,      7,      31,    37,    43,    49,     57,    63,   69,     77,   83};

  //               6      3      9       7     8
  QString hdg[] = {"Hdg", "Dis", "Fuel", "GS", "EET", "FL", " ", " ", " ", " ", "E", "F"};
  //int tab[] =   {0,     6,     12,     19,   25,    33,   80,  80,  80,  80,  80,  80};
  int tab[] =     {0,     7,     16,     26,   35,    43,   80,  80,  80,  80,  80,  80};

  //
  // Headings and heading border
  //
  st = SetBuff(tab, '-', '+');
  st[81] = 0; // lob it off
  printf(st + "\n");
  fdata << (st + "\n");

  st = SetBuff(tab, ' ', '|');
  for (i = 0; i < 10; i++) {
    st.replace(tab[i]+2, hdg[i].length(), hdg[i]);
  }
  printf(st + "\n");
  fdata << (st + "\n");

  st = SetBuff(tab, '-', '+');
  st[81] = 0; // lob it off
  printf(st + "\n");
  fdata << (st + "\n");

  //
  // The log itself
  //
  for (nItem = 0; nItem < nTotItem; nItem++) {
    s = g_MapWnd.magneticTable->text(nItem, 0);
    if (s.contains("----")) {
      // Handle the last line
      st = SetBuff(tab, '-', '+');
      printf(st + "\n");
      fdata << (st + "\n");
    }
    else {
      // All the rest - the log proper

      st = SetBuff(tab, ' ', ' ');
      i = 0;
      st = g_MapWnd.magneticTable->text(nItem, 0) +  " - " +
           g_MapWnd.magneticTable->text(nItem, 1);

      //st.replace(tab[i]+2, s.length(), s);
      // st[tab[i]] = '|';

      printf("\n" + st + "\n");
      fdata << ("\n" + st + "\n");

      // add the lat and lon 
      st = g_MapWnd.trueTable->text(nItem, 2) + " " +
           g_MapWnd.trueTable->text(nItem, 3) +
          "  (" + g_MapWnd.trueTable->text(nItem, 4) + "ft)";

      printf(st + "\n\n");
      fdata << (st + "\n\n");

      //---------------------
      nItem++;

      st = SetBuff(tab, '-', '+');
      st[81] = 0; // lob it off
      printf(st + "\n");
      fdata << (st + "\n");

      st = SetBuff(tab, ' ', '|');
      printf(st + "\n");
      fdata << (st + "\n");
                                                              
      st = SetBuff(tab, ' ', '|');
      i = 0;
      s = g_MapWnd.magneticTable->text(nItem, 6) + "'";
      st.replace(tab[i]+2, s.length(), s);
      st[tab[i]] = '|';

      i = 1;
      s = g_MapWnd.magneticTable->text(nItem, 3) + "nm";
      st.replace(tab[i]+2, s.length(), s);
      st[tab[i]] = '|';

      i = 2;
      s = g_MapWnd.magneticTable->text(nItem, 9) + "usg";  // or lit
      st.replace(tab[i]+2, s.length(), s);
      st[tab[i]] = '|';

      i = 3;
      s = g_MapWnd.magneticTable->text(nItem, 7) + "kt";
      st.replace(tab[i]+2, s.length(), s);
      st[tab[i]] = '|';

      i = 4;
      s = g_MapWnd.magneticTable->text(nItem, 8);
      st.replace(tab[i]+2, s.length(), s);
      st[tab[i]] = '|';

      i = 5;
      s = "  ";
      st.replace(tab[i]+2, s.length(), s);
      st[tab[i]] = '|';

      st[81] = 0; // lob it off
      printf(st + "\n");
      fdata << (st + "\n");

      st = SetBuff(tab, ' ', '|');
      printf(st + "\n");
      fdata << (st + "\n");
      
      st = SetBuff(tab, '-', '+');

      st[81] = 0; // lob it off
      printf(st + "\n");
      fdata << (st + "\n");

      nItem--;
    }
  }
  //st = SetBuff(tab, '-', '+');
  //printf(st + "\n");
  //fdata << (st + "\n");

  file.close();
}

void ApplicationWindow::planninglogTxt(QString fileName)
{
  printf("ApplicationWindow::planninglogTxt\n");

  QDateTime dt = QDateTime::currentDateTime();
  QDateTime dt0;
  unsigned long timestamp = dt.secsTo(dt0);

  //QString fileName = QString("%1/planninglog~%2.txt").arg(getenv("TEMP")).arg(timestamp);
  //QString fileName = QString("%1/planninglog~%2.txt").arg(getenv("TEMP")).arg(0);
  QFile file(fileName);
  file.open(IO_WriteOnly);
  QTextStream fdata(&file);

  int i;
  QString s;
  QString st;
  int nTotItem = g_MapWnd.RouteWpt;
  int nHeading = 10;
  int nItem;
  const int iRowHgt = 9;// 14;
  int iRow = 65;
  int iCol = 10;

  QString hdg[] = {"ICAO", "Info", "Trk", "Dis", "TAS", "Wind", "Hdg", "GS", "Time", "Fuel"};
  int tab[] =     {0,      7,      31,    37,    43,    49,     57,    63,   69,     77,   83};

  //
  // Headings and heading border
  //
  st = SetBuff(tab, '-', '+');
  printf(st + "\n");
  fdata << (st + "\n");
  st = SetBuff(tab, ' ', '|');
  for (i = 0; i < 10; i++) {
    st.replace(tab[i]+2, hdg[i].length(), hdg[i]);
  }
  printf(st + "\n");
  fdata << (st + "\n");

  st = SetBuff(tab, '-', '+');
  printf(st + "\n");
  fdata << (st + "\n");

  //
  // The log itself
  //
  for (nItem = 0; nItem < nTotItem; nItem++) {
    s = g_MapWnd.magneticTable->text(nItem, 0);
    if (s.contains("----")) {
      // Handle the last line
      st = SetBuff(tab, '-', '+');
      printf(st + "\n");
      fdata << (st + "\n");
    }
    else {
      // All the rest - the log proper
      st = SetBuff(tab, ' ', '|');
      for (i = 0; i < 10; i++) {
          s = g_MapWnd.magneticTable->text(nItem, i);
          st.replace(tab[i]+2, s.length(), s);
      }
      // belt and braces
      for (i = 0; i < 10; i++) {
        st[tab[i]] = '|';
      }
      printf(st + "\n");
      fdata << (st + "\n");
    }
  }
  st = SetBuff(tab, '-', '+');
  printf(st + "\n");
  fdata << (st + "\n");
  file.close();
}

//-----------------------------------------------------------------------------
// Tools | Briefing handler
//
void ApplicationWindow::briefing()
{
  printf("ApplicationWindow::briefing\n");
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();

  std::string szPath;
  szPath = iniFile.GetValue("Online", "BriefingURL", "c:/knav/html/briefing.html");//, szPath, 255, "./mapwnd.ini");
  printf("szPath = %s\n", szPath.c_str());

  html_browse(szPath.c_str());
}

//-----------------------------------------------------------------------------
// Tools | Weather
//
void ApplicationWindow::weather()
{
  printf("ApplicationWindow::weather\n");
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();

  std::string szPath;
  szPath = iniFile.GetValue("Online", "MetURL", "c:/knav/html/met.html");//, szPath, 255, "./mapwnd.ini");
  printf("szPath = %s\n", szPath.c_str());

  html_browse(szPath.c_str());
}

//-----------------------------------------------------------------------------
// Tools | Duats/Naips
//
void ApplicationWindow::duats_naips()
{
  printf("ApplicationWindow::duats_naips\n");
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();

  std::string szPath;
  szPath = iniFile.GetValue("Online", "NaipsURL", "http://www.duats.com/");//, szPath, 255, "./mapwnd.ini");
  printf("szPath = %s\n", szPath.c_str());

  html_browse(szPath.c_str());
}

//-----------------------------------------------------------------------------
// Tools | Flight Plan
//
// We use a macro - I know we should'nt :-| but it does
// make the code a lot more readable.
#define zd(s) ( (s.contains("--")) ? QString::null : (s) )

void ApplicationWindow::flightplan()
{
  printf("ApplicationWindow::flightplan\n");

	/*
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  std::string szPath;
  szPath = iniFile.GetValue("Online", "FlightPlanURL", "c:/knav/html/flightplan.html");//, szPath, 255, "./mapwnd.ini");
  printf("szPath = %s\n", szPath.c_str());
  html_browse(szPath.c_str());
  */

  uiFlightNotificationDlg dlg(this, "FlightNotification", " ", true);

	char szNaipsPath[256];
	if (naips_path(szNaipsPath) == 0)	{
		// We have found what appears to be a NAIPS installation
		dlg.btnNaips->setEnabled(true);
	}
	else {
		dlg.btnNaips->setEnabled(false);
	}



	char szBuffer[256];
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();

	// Load and populate the dialog using most of the last settings
	// as defaults where most applicable
	/*
  iniFile.GetValueS(szBuffer, "FlightPlan", "AircraftId", "VH-ABC");
	dlg.AircraftId->setText(szBuffer);
	iniFile.GetValueS(szBuffer, "FlightPlan", "AircraftType", "PA28");
	dlg.AircraftType->setText(szBuffer);
	iniFile.GetValueS(szBuffer, "FlightPlan", "InitCruiseSpeed", "N125");
	dlg.InitCruiseSpeed->setText(szBuffer);
	iniFile.GetValueS(szBuffer, "FlightPlan", "InitCruiseLevel", "A075");
	dlg.InitCruiseLevel->setText(szBuffer);
	*/
	

  dlg.AircraftId->setText(iniFile.GetValueS(szBuffer, "FlightPlan", "AircraftId", "VH-ABC"));
	dlg.AircraftType->setText(iniFile.GetValueS(szBuffer, "FlightPlan", "AircraftType", "PA28"));
	dlg.InitCruiseSpeed->setText(iniFile.GetValueS(szBuffer, "FlightPlan", "InitCruiseSpeed", "N125"));
	dlg.InitCruiseLevel->setText(iniFile.GetValueS(szBuffer, "FlightPlan", "InitCruiseLevel", "A075"));
	dlg.PilotInCommand->setText(iniFile.GetValueS(szBuffer, "FlightPlan", "PilotInCommand", "SNOOPY"));
	dlg.ContactPhoneNumber->setText(iniFile.GetValueS(szBuffer, "FlightPlan", "ContactPhoneNumber", "040 123 4567"));
		 


  // Check if there are any routepoints, if so populate
  // what we can
  if (g_MapWnd.RouteWpt > 0) {
    int i = 1;

    dlg.Departure->setText(g_MapWnd.magneticTable->text(0, 0));
    dlg.DEP->setText(g_MapWnd.magneticTable->text(0, 0));
    dlg.Destination->setText(g_MapWnd.magneticTable->text(g_MapWnd.RouteWpt-1, 0));
    dlg.DEST->setText(g_MapWnd.magneticTable->text(g_MapWnd.RouteWpt-1, 0));
    dlg.TotalEET->setText(g_MapWnd.magneticTable->text(g_MapWnd.RouteWpt+1, 8));

    // note: we use ++i and not i++
    if (++i < g_MapWnd.RouteWpt) { /* default DCT */               dlg.sigPoint_0->setText(zd(g_MapWnd.magneticTable->text(1, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_1->setText("DCT"); dlg.sigPoint_1->setText(zd(g_MapWnd.magneticTable->text(2, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_2->setText("DCT"); dlg.sigPoint_2->setText(zd(g_MapWnd.magneticTable->text(3, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_3->setText("DCT"); dlg.sigPoint_3->setText(zd(g_MapWnd.magneticTable->text(4, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_4->setText("DCT"); dlg.sigPoint_4->setText(zd(g_MapWnd.magneticTable->text(5, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_5->setText("DCT"); dlg.sigPoint_5->setText(zd(g_MapWnd.magneticTable->text(6, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_6->setText("DCT"); dlg.sigPoint_6->setText(zd(g_MapWnd.magneticTable->text(7, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_7->setText("DCT"); dlg.sigPoint_7->setText(zd(g_MapWnd.magneticTable->text(8, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_8->setText("DCT"); dlg.sigPoint_8->setText(zd(g_MapWnd.magneticTable->text(9, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_9->setText("DCT"); dlg.sigPoint_9->setText(zd(g_MapWnd.magneticTable->text(10, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_10->setText("DCT"); dlg.sigPoint_10->setText(zd(g_MapWnd.magneticTable->text(11, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_11->setText("DCT"); dlg.sigPoint_11->setText(zd(g_MapWnd.magneticTable->text(12, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_12->setText("DCT"); dlg.sigPoint_12->setText(zd(g_MapWnd.magneticTable->text(13, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_13->setText("DCT"); dlg.sigPoint_13->setText(zd(g_MapWnd.magneticTable->text(14, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_14->setText("DCT"); dlg.sigPoint_14->setText(zd(g_MapWnd.magneticTable->text(15, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_15->setText("DCT"); dlg.sigPoint_15->setText(zd(g_MapWnd.magneticTable->text(16, 0)));  }
    if (++i < g_MapWnd.RouteWpt) { dlg.atsRoute_16->setText("DCT"); dlg.sigPoint_16->setText(zd(g_MapWnd.magneticTable->text(17, 0)));  }
  }

  int rv = dlg.exec();
  if (rv == QDialog::Accepted) {
  	QString s;
		char szBuffer[256];
		CIniFile iniFile("knav.ini"); iniFile.ReadFile();
		s = QDir::cleanDirPath(QString("file:%1/html/7233-4_1.html").arg(iniFile.GetValueS(szBuffer, "Initze", "szKnavRootPath", ".")));
		html_browse(s);
  }
  else {
  }
  
	iniFile.SetValue("FlightPlan", "AircraftId", dlg.AircraftId->text().latin1());
	iniFile.SetValue("FlightPlan", "AircraftType", dlg.AircraftType->text().latin1());
	iniFile.SetValue("FlightPlan", "InitCruiseSpeed", dlg.InitCruiseSpeed->text().latin1());
	iniFile.SetValue("FlightPlan", "InitCruiseLevel", dlg.InitCruiseLevel->text().latin1());
	iniFile.SetValue("FlightPlan", "PilotInCommand", dlg.PilotInCommand->text().latin1());
	iniFile.SetValue("FlightPlan", "ContactPhoneNumber", dlg.ContactPhoneNumber->text().latin1());


	
	iniFile.WriteFile();
}


//-----------------------------------------------------------------------------
// Tools | xxx
//
void ApplicationWindow::dl_all()
{
#ifdef WIN32_NOPE
 
  QMessageBox::information(this, SZ_PRGNAME,
                           "Please ensure that your Garmin GPS is set to\n\n"
                           "  GARMIN/GARMIN");

  if (FileExist("~~zz.tmp")) return;

  if (g_wp) fclose(g_wp);
  if (g_rt) fclose(g_rt);
  if (g_tr) fclose(g_tr);

  WinExec("g3\\g3-1.bat", /*SW_HIDE*/ SW_MAXIMIZE);
 
  Sleep(5000);
  while (FileExist("~~zz.tmp"))
    Sleep(1000);
    //ProcessMessage();

/*
  FILE *fin, *fout;
  char s[1024];
  char st[1024];
*/

  //pTreeWnd->UpdateTree(); - todo
#endif
}



//-----------------------------------------------------------------------------
// Download Tracks from the GPS unit
//
void ApplicationWindow::dl_track()
{
  QMessageBox::information(this, SZ_PRGNAME,
                           "Please ensure that your GPS is connected\n\n"
                           "to a serial port and set to GARMIN/GARMIN");

  int rv = execG3cmd("g3\\g7tow -%i -i G45T > g3\\g3.trk");

  if (rv == 0) {
    // tracks
    char s[256];
    char sz[256];
    int iTrkNr = 0;

    FILE *g_tr;
    g_tr = fopen("g3/g3.trk", "rt");

    if (g_tr) {
      rewind(g_tr);
      QFile file;
      //QFile gpsTrk("gps-trk.csv");
      //gpsTrk.open(IO_WriteOnly);

      while (fgets(s, 255, g_tr)) {
        QString qs;
        if (s[0] == 'N') {
          //
          // New track start
          //
          if (fgets(s, 255, g_tr)) {
            iTrkNr++;
            QString ss = QString(s);
            int iMon = month_str(ss.mid(32, 3).latin1());

            qs.sprintf("%s-%02d-%s.%02d", ss.mid(48, 4).latin1(),  //year
                                          iMon,                    //mon
                                          ss.mid(36, 2).latin1(),  //day
                                          iTrkNr);
            file.close();
            file.setName(QString("g3/%1.csv").arg(qs));
            file.open(IO_WriteOnly);
            qs = QString("%1").arg(strzaptail(s, '\n'));
            qs = QString("0, 0, 0\n");
            //gpsTrk.writeBlock(qs.latin1(), qs.length());
          }
        }
        if (s[0] == 'T') {
          double Lat, Lon;
          convLL(&Lat, strcut(sz, s, 3, 11));
          convLL(&Lon, strcut(sz, s, 15, 12));
          qs.sprintf("%d, %7.4f, %8.4f\n", 0, -Lat, Lon);
          file.writeBlock(qs.latin1(), qs.length());
          //gpsTrk.writeBlock(qs.latin1(), qs.length());
        }
      } //while
      fclose(g_tr);
    }
    delete(downloaddb);
    createDownloadTree();
    QMessageBox::information(this, SZ_PRGNAME, "Downloaded track from a GPS unit.\n");
  }
  else {
    QMessageBox::warning(this, SZ_PRGNAME, SZ_GPSERROR);
  }
}

//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::dl_route()
{
  QMessageBox::information(this, SZ_PRGNAME,
                           "Please ensure that your GPS is connected\n\n"
                           "to a serial port and set to GARMIN/GARMIN");

  //int rv = system("g3\\g7tow -1 -i G45R > g3\\g3.rte");
  int rv = execG3cmd("g3\\g7tow -%i -i G45R > g3\\g3.rte");
  if (rv == 0) {
    QMessageBox::information(this, SZ_PRGNAME, "Downloaded routes from a GPS unit.\n");
  }
  else {
    QMessageBox::warning(this, SZ_PRGNAME, SZ_GPSERROR);
  }
}

//-----------------------------------------------------------------------------
// Download Waypoints from the GPS unit
//
void ApplicationWindow::dl_waypt()
{
  QMessageBox::information(this, SZ_PRGNAME,
                           "Please ensure that your GPS is connected\n\n"
                           "to a serial port and set to GARMIN/GARMIN");

  //int rv = system("g3\\g7tow -1 -i G45W > g3\\g3.wpt");
  int rv = execG3cmd("g3\\g7tow -%i -i G45W > g3\\g3.wpt");
  //int rv = 0;
  if (rv == 0) {
    // tracks
    char s[256];
    char sz[256];
    int iWptNr = 0;

    FILE *g_wp;
    g_wp = fopen("g3/g3.wpt", "rt");

    if (g_wp) {
      rewind(g_wp);
      //QFile file;
      QFile gpsWptFile("gps-wpt.csv");
      gpsWptFile.open(IO_WriteOnly);

      while (fgets(s, 255, g_wp)) {
        QString qs;
        if (s[0] == 'W') {
          iWptNr++;
          QString ss = QString(s);
          double Lat, Lon;
          convLL(&Lat, strcut(sz, s, 19, 11));
          convLL(&Lon, strcut(sz, s, 31, 12));
          qs.sprintf("0, %s, %s, -1, %7.4f, %8.4f, 32\n", ss.mid( 3, 16).latin1(), 
                                                          ss.mid(69, 20).latin1(),        
                                                          -Lat, Lon);
          gpsWptFile.writeBlock(qs.latin1(), qs.length());
        } 
      } //while
      gpsWptFile.close();
      fclose(g_wp);
    }
    delete(downloaddb);
    createDownloadTree();

    QMessageBox::information(this, SZ_PRGNAME, "Downloaded waypoints from a GPS unit.\n");
  }
  else {
    QMessageBox::warning(this, SZ_PRGNAME, SZ_GPSERROR);
  }
}


//-----------------------------------------------------------------------------
// When the route toolbar button is pressed toggle the routeActive flag
// in the map window
//
void ApplicationWindow::route()
{
  g_MapWnd.bAddRteActive = !g_MapWnd.bAddRteActive;

  // Reset bZoomActive because
  // we cannot zoom or pan and add routes at the same time
  if (g_MapWnd.bZoomActive) {
    g_MapWnd.bZoomActive = !g_MapWnd.bZoomActive;
        zoom_button->toggle();
  }

  if (g_MapWnd.bAddRteActive) {
    pMapWidget->setCursor(crossCursor);
  }
  else {
    pMapWidget->setCursor(pointingHandCursor);
  }

  statusBar()->message(QString("RouteActive %1 ZoomActive %2").arg(g_MapWnd.bAddRteActive).arg(g_MapWnd.bZoomActive), 20000);
}


//-----------------------------------------------------------------------------
// Toggle display of the compass rose.
//
void ApplicationWindow::rose()
{
  g_MapWnd.bRoseActive = !g_MapWnd.bRoseActive;
  view->setItemChecked(id_rose, g_MapWnd.bRoseActive);

  pMapWidget->repaint(true);
}

//-----------------------------------------------------------------------------
// Toggle display of the node rose.
//
void ApplicationWindow::noderose()
{
  g_MapWnd.bNodeRoseActive = !g_MapWnd.bNodeRoseActive;
  view->setItemChecked(id_noderose, g_MapWnd.bNodeRoseActive);

  pMapWidget->repaint(true);
}

//-----------------------------------------------------------------------------
// Toggle between flat and globe display
//
void ApplicationWindow::globe()
{
  g_MapWnd.flatWorld = !g_MapWnd.flatWorld;
  view->setItemChecked(id_globe, !g_MapWnd.flatWorld);
  globe_button->setOn(!g_MapWnd.flatWorld);

  // if we are now a globe *and* shade relief
  // toggle the shade relief so that it will be off.
  // the call to relief() will take care of the repaint
  // as well
  if (!g_MapWnd.flatWorld && g_MapWnd.bShadeActive) {
    relief();
  }
  else {
    // call Size with the current width and height. This will
    // ensure that the aspect gets set correctly for the flat map
    // and force a repaint.
    g_MapWnd.Size(g_MapWnd.screenWidth, g_MapWnd.screenHeight);
    pMapWidget->repaint(true);
  }
}

//-----------------------------------------------------------------------------
// Toggle shaded relief on/off
// 
void ApplicationWindow::relief()
{
  g_MapWnd.bShadeActive = !g_MapWnd.bShadeActive;
  view->setItemChecked(id_relief, g_MapWnd.bShadeActive);
  shade_button->setOn(g_MapWnd.bShadeActive);

  // if we are now a globe *and* shade relief
  // toggle the globe so that it will be off.
  // the call to globe() will take care of the repaint
  // as well
  if (!g_MapWnd.flatWorld && g_MapWnd.bShadeActive) {
    globe();
  }
  else {
    pMapWidget->repaint(true);
  }
}


void ApplicationWindow::radar()
{
  g_MapWnd.bRadarActive = !g_MapWnd.bRadarActive;
  //view->setItemChecked(id_radar, g_MapWnd.bRadarActive);
  //radar_button->setOn(g_MapWnd.bRadarActive);

  if (g_MapWnd.bRadarActive)
    int rv = system("radar.bat");
  else
    pMapWidget->repaint(true);

}


void ApplicationWindow::preferences()
{
  TabDialog dlg(this, "preferences", " ", TRUE);
  //dlg.resize( 450, 350 );
  dlg.resize( 400, 300 );
  dlg.setCaption(QString("%1 - %2").arg(SZ_APPNAME).arg("Preferences"));
  //dlg.show();
  dlg.exec();
}

//
// Deprecated
//
void ApplicationWindow::startup()
{
  #ifdef WIN32
    WinExec("notepad.exe knav.ini", SW_SHOW);
  #else
    int rv = system("notepad.exe knav.ini");
  #endif //WIN32
}

void ApplicationWindow::refresh()
{
  pMapWidget->repaint(true); //erase background
}


//-----------------------------------------------------------------------------
// Attempt to display a html info page based on the ICAO id
// provided.
//
// Search through szHtmlDataPath 0 to 3 paths as specified in
// the .ini file. If nothing is found try an build up some info based
// on the data in the loaded wpt database.
//
// Failing that default to a pretty meaningless "default" page.
//
// @param sIcao ICAO designator
//
void ApplicationWindow::displayInfoHtml(QString sIcao)
{
  int i, n;
  QString sExt[3] = {"", ".html", ".htm"};
  QString sSec[4] = {"0", "1", "2", "3"};
  char s[256];

  m_htmlBrowser->setIcao(sIcao);
  m_htmlBrowser->m_sCurrentInfoHtml = "";
  CIniFile inifile("knav.ini"); inifile.ReadFile();

  //
  // szHtmlDataPath0 should always be pointing to
  // "<progdir>/airport/" This would usually
  // be set in the knav.ini file by the installer
  // but to make dead sure we harcode it here as
  // well.
  //
  QDir d;
  QString aptInfoPath = d.absPath() + "/airport/";
  inifile.SetValue("Initze", "szHtmlDataPath0", aptInfoPath.latin1());

  // Try all the paths specified in knav.ini
  // for all the possiable extensions.
  // Display and exit is found
  for (n = 0; n < 4; n++) {
    for (i = 0; i < 3; i++) {
      QString vn(inifile.GetValueS(s, "Initze", QString("szHtmlDataPath%1").arg(sSec[n]).latin1(), "./airport/"));
      QString sCurrentInfoHtml = QString("%1%2%3").arg(vn)
                                                  .arg(sIcao)
                                                  .arg(sExt[i]);

      printf("trying  %s\n", sCurrentInfoHtml.latin1());

      if (FileExist(sCurrentInfoHtml.latin1())) {
        printf("found %s\n", sCurrentInfoHtml.latin1());
        m_htmlBrowser->setSource(sCurrentInfoHtml);
        m_htmlBrowser->m_sCurrentInfoHtml = sCurrentInfoHtml;
        return;
      }
    }
  }
                 
  InfoHtml infoHtml;
  printf("displayInfoHtml - using what we have\n");
  
  //
  // infoHtml.createInfoHtml() will create a info.html in the
  // ./airport directory.
  //
  if (infoHtml.createInfoHtml(sIcao)) {
    //html->setSource(QString("%1/info.html").arg(getenv("TEMP")));
    if (m_reccnt == 0) {
      m_reccnt++;
      displayInfoHtml(sIcao);
      m_reccnt = 0;
    }
  }
  else {
    //
    // Always go to home first. This will force the
    // widget to correctly display info.htm when multiple
    // unknowns are clicked consecutively.
    //
    m_htmlBrowser->home();
    m_htmlBrowser->setSource(QString("%1/default.html").arg(getenv("TEMP")));
  }
}



//-----------------------------------------------------------------------------
// Misc slots
//
void ApplicationWindow::clickCatcher()
{
  printf("Click caught\n");
}

void ApplicationWindow::infoChanged()
{
	if (!g_MapWnd.inf_icao.isEmpty()) {
		char ss[256];
		strcpy(ss, g_MapWnd.inf_icao.latin1());

		if (!g_MapWnd.bZoomActive)
			displayInfoHtml(g_MapWnd.inf_icao);
	}
}


void ApplicationWindow::latlonChanged()
{
  statusBar()->message(g_MapWnd.m_sStatusInfo, 12000);
}

void ApplicationWindow::trueTableValueChanged(int row, int col)
{
  printf("MapWidget::trueTablevalueChanged\n");
  g_MapWnd.trueTableEdited(row, col);
  pMapWidget->repaint(true);
}


//-----------------------------------------------------------------------------
// Slots for actions mouse clicks on the true table
//
void ApplicationWindow::trueTableClicked(int row, int col, int button, const QPoint &mousePos)
{
  printf("MapWidget::trueTableClicked\n");

  if (button == LeftButton) {
  }

  if (button == RightButton) {
    QPopupMenu *trueTableMenu = new QPopupMenu(this);
      trueTableMenu->insertItem("Information", this, SLOT(popup_info()));
      trueTableMenu->insertItem("Insert", this, SLOT(popup_insert()));
      trueTableMenu->insertItem("Delete", this, SLOT(popup_delete()));
      trueTableMenu->insertSeparator();
      trueTableMenu->insertItem("&Auto Fit", this, SLOT(autofit()), CTRL+Key_A);
      trueTableMenu->insertItem("Fixe&d Fit", this, SLOT(fixedfit()), CTRL+Key_V);

     trueTableMenu->popup(g_MapWnd.trueTable->mapToGlobal(mousePos));
  }
}

//-----------------------------------------------------------------------------
// Slots for actions mouse clicks on the magnetic table
//
void ApplicationWindow::magneticTableClicked(int row, int col, int button, const QPoint &mousePos)
{
  printf("MapWidget::magneticTableClicked\n");

  if (button == LeftButton) {
  }

  if (button == RightButton) {
    QPopupMenu *magneticTableMenu = new QPopupMenu(this);
      magneticTableMenu->insertItem("&Auto Fit", this, SLOT(autofit()), CTRL+Key_A);
      magneticTableMenu->insertItem("Fixe&d Fit", this, SLOT(fixedfit()), CTRL+Key_V);

     magneticTableMenu->popup(g_MapWnd.magneticTable->mapToGlobal(mousePos));
  }
}


//-----------------------------------------------------------------------------
// Slots for actions performed on the "tree" yyy
//
void ApplicationWindow::lv_mouseButtonClicked(int button, QListViewItem *item, const QPoint &pos, int c)
{
  printf("ApplicationWindow::lv_mouseButtonClicked\n");

  g_Select.m_lisviewitem = item;
  if (item) {
    QListViewItem *child = item->firstChild();
    if (child)
      g_Select.m_idStr = child->text(0);
    else
      g_Select.m_idStr = item->text(0);

    int ctr = 0;
    while (child) {
      QString s = child->text(0);
      if (s.contains("coor:")) {
        g_Select.m_coor = s;
      }
      else if (s.contains("icao:")) {
        g_Select.m_icao = s;
      }
      else if (s.contains("iiid:")) {
        g_Select.m_iiid = s;
      }

      child = child->nextSibling();
      ctr++;
      if (ctr > 10)
        break;
    }  

    /*if (button == LeftButton)*/ {
      if (item) {
        printf("item = %s\n", item->text(0).latin1());
        QString qs0 = item->text(0);

        /*
        QString fromFileName = QString("%1/%2.%3").arg(getenv("TEMP")).arg(qs0).arg("csv");
        if (FileCopy("gps-trk-tmp.csv", fromFileName.latin1())) {
          g_MapWnd.loadGpsTrack();
        }
        */
        QString fromFileName = QString("g3/%1").arg(qs0);
        if (FileCopy("gps-trk-tmp.csv", fromFileName.latin1())) {
          g_MapWnd.loadGpsTrack();
        }

        printf("child = %s\n", g_Select.m_idStr.latin1());
        //pMapWidget->repaint(true);
      }
    }

    if (button == RightButton) {
      printf("RightButton\n");
      QListViewItem *parent = item->parent();
      if (parent) {
        //
        // It really only makes sense to pop up a menu
        // from stuff that may be able to do something with it
        //
        QPopupMenu *treeMenu = new QPopupMenu(this);
          int id_popup_route  = treeMenu->insertItem("Route", this, SLOT(popup_route()));
          treeMenu->insertItem("Locate", this, SLOT(popup_locate()));
          treeMenu->insertSeparator();
          int id_popup_edit   = treeMenu->insertItem("Edit", this, SLOT(popup_edit()));
          int id_popup_remove = treeMenu->insertItem("Remove", this, SLOT(popup_remove()));
          /*
          treeMenu->insertSeparator();
          treeMenu->insertItem("&Auto Fit", this, SLOT(autofit()), CTRL+Key_A);
          treeMenu->insertItem("&Fixed Fit", this, SLOT(fixedfit()), CTRL+Key_V);*/
          //zzxx edit->setItemEnabled(id_redo, false);
          treeMenu->setItemEnabled(id_popup_route, false);
          treeMenu->setItemEnabled(id_popup_edit,  false);
          treeMenu->setItemEnabled(id_popup_remove, false);

          if (parent->text(0) == SZ_AIRPORT) { 
            treeMenu->setItemEnabled(id_popup_route, true);
            treeMenu->setItemEnabled(id_popup_edit, true);
          }
          
          if (parent->text(0) == SZ_TRACK)  
            treeMenu->setItemEnabled(id_popup_remove, true);


        treeMenu->popup(pos);
      }
    }
  }
}


/*
 we dont really need this one do we ?
void ApplicationWindow::lv_rightButtonClicked(QListViewItem *item, const QPoint &mousePos, int column)
{
    QPopupMenu *treeMenu = new QPopupMenu(this);
      treeMenu->insertItem("Information", this, SLOT(popup_info()));
      treeMenu->insertItem("Insert", this, SLOT(popup_insert()));
      treeMenu->insertItem("Delete", this, SLOT(popup_delete()));
      treeMenu->insertSeparator();
      treeMenu->insertItem("&Auto Fit", this, SLOT(autofit()), CTRL+Key_A);
      treeMenu->insertItem("&Fixed Fit", this, SLOT(fixedfit()), CTRL+Key_V);

  treeMenu->popup(mousePos);
 
}
*/




//-----------------------------------------------------------------------------
// A slot that fits the colums of the route and magnetic tables to
// the actual content. This will allow for display of the most information
//
void ApplicationWindow::autofit()
{
  g_MapWnd.autoFitTableColums(-1);
}

//-----------------------------------------------------------------------------
// A slot that fits the colums of the route and magnetic tables to
// a fixed default size.
//
void ApplicationWindow::fixedfit()
{
  g_MapWnd.autoFitTableColums(64);
}

//-----------------------------------------------------------------------------
// Popup | Info action performed
//
void ApplicationWindow::popup_info()
{
  int idx;
  QString sIcao;
  long id = 0;

  idx = g_MapWnd.trueTable->currentRow();
  printf(QString("Popup | Info on row %1").arg(idx));
  sIcao = g_MapWnd.trueTable->text(idx, 0);

  displayInfoHtml(sIcao);
}

//-----------------------------------------------------------------------------
// Popup | Insert action performed
//
void ApplicationWindow::popup_insert()
{
  int idx, i;

  idx = g_MapWnd.trueTable->currentRow();
  printf(QString("Popup | Info on row %1").arg(idx));

  g_MapWnd.RouteWpt++;

  for (i = g_MapWnd.RouteWpt; i > idx ; i--)
    g_MapWnd.RouteLst[i] = g_MapWnd.RouteLst[i-1];

  if (g_MapWnd.RouteWpt < 2) {
    //g_MapWnd.wpt.MoveFirst();
    g_MapWnd.RouteLst[idx] = 1;  // we assume of course that there is at
                                 // least 1 entry in wpt.
  }

  pMapWidget->repaint(true); // do erase
}

//-----------------------------------------------------------------------------
// Popup | Delete action performed
//
void ApplicationWindow::popup_delete()
{
  int idx;//, i;

  idx = g_MapWnd.trueTable->currentRow();
  printf(QString("Popup | Info on row %1").arg(idx));

  /*
  //
  // We 'delete' an entry by overwriting
  // that particular entry by shifting all
  // subsequent entries down one place
  //
  g_MapWnd.pushUndoBuffer();

  for (i = idx; i < g_MapWnd.RouteWpt; i++)
    g_MapWnd.RouteLst[i] = g_MapWnd.RouteLst[i+1];

  // There is now one less waypoint
  g_MapWnd.RouteWpt--;
  */

  g_MapWnd.removeRouteWpt(idx);

  pMapWidget->repaint(true); // do erase
}

//-----------------------------------------------------------------------------
// Popup | Route action performed
//
void ApplicationWindow::popup_route()
{
  QString siiid = g_Select.m_iiid.mid(5);
  int iiid = siiid.toInt();
  g_MapWnd.appendRouteWpt(iiid);

  pMapWidget->repaint(false); //no erase background
}

//-----------------------------------------------------------------------------
// Popup | Locate action performed
//
void ApplicationWindow::popup_locate()
{
  // display any information we may have in the info area
  QString sIcao(g_Select.m_icao.mid(5, 4));
  if (sIcao.length() != 0)
    displayInfoHtml(sIcao);

  QString coor = g_Select.m_idStr;

  if (coor.contains("coor:")) {
    QString slat = coor.mid(5, 7);
    QString slon = coor.mid(13, 7);
    slat.latin1();
    slon.latin1();

    double rlat = slat.toDouble();
    double rlon = slon.toDouble();
    g_MapWnd.setCrossHair(rlat, rlon);

    //---------
    // to be moved to GenMapWnd when we get a pan center
    // that takes lat/lon args
    int x, y;
    g_MapWnd.LtoC(rlon, rlat, x, y);
 
    //QPoint pt = mapFromGlobal(e->globalPos());
    POINT point = {x, y};
    g_MapWnd.PanCenter(point);
    //---------
  }
  else {
    g_MapWnd.clearCrossHair();
  }
  pMapWidget->repaint(true); //erase background
}


//-----------------------------------------------------------------------------
// Popup | Edit action performed
//
void ApplicationWindow::popup_edit()
{
  QString sIcao(g_Select.m_icao.mid(5, 4));
  displayInfoHtml(sIcao);

  AptInfoEditorForm dlg(this, "aptinfoeditor", " ", TRUE);
  dlg.setCaption(QString("%1 - %2").arg(SZ_APPNAME).arg("Airport Information Editor"));

  dlg.Init(sIcao);
  int rv = dlg.exec();
  if (rv == QDialog::Rejected)
    return;
}

//-----------------------------------------------------------------------------
// Popup | Remove action performed
//
void ApplicationWindow::popup_remove()
{
  QFile f(QString("g3/%1").arg(g_Select.m_idStr));
  int rv = f.remove();

  // delete the tree entry as well
  // Qt takes care of all the cosmetics and gb, all we need to
  // di is zap the pointer, neat :-)
  delete g_Select.m_lisviewitem;
}


void ApplicationWindow::test()
{
  printf("test o matic\n");
}


//-----------------------------------------------------------------------------
// Prompt for save, cancel or abandon and perform actions required
//
int ApplicationWindow::testChange()
{
  if (g_MapWnd.bRouteSaved) {
    return 2;  // same as abandon changes
  }

  int rv = QMessageBox::information(this, SZ_PRGNAME,
                                    "The document has been changed since "
                                    "the last save.",
                                    "Save Now", "Cancel", "Abandon",
                                    0, 1);
  switch (rv) {
    case 0:  // save now
      fileSave();
      break;
    case 1: // cancel
    default: // just for sanity
      break;
    case 2: // abandon
      g_MapWnd.bRouteSaved = true; // somewhat of a hack - maybe use fileNew?
      break;
  }
  return rv;
}


//-----------------------------------------------------------------------------
// Capture the close event and check to see if we need to save, cancel or 
// abandon
//
void ApplicationWindow::closeEvent(QCloseEvent* ce)
{
  if (g_MapWnd.bRouteSaved) {
    ce->accept();
    return;
  }

  int rv = testChange();
  switch (rv) {
    case 0:
      //fileSave();
      ce->accept();
      break;
    case 1:
    default: // just for sanity
      ce->ignore();
      break;
    case 2:
      ce->accept();
      break;
  }
}

//-----------------------------------------------------------------------------
// Capture the keypress event 
//
void ApplicationWindow::keyPressEvent(QKeyEvent *e)
{
  printf("ApplicationWindow::keyPressEvent\n");
  
  if (e->key() == QKeyEvent::Key_F9) {
    if (!e->isAutoRepeat()) {
      printf("-> F9\n");
      g_MapWnd.m_bTextActive = false;
      pMapWidget->repaint(true);
    }
  }
}

//-----------------------------------------------------------------------------
// Capture the keyrelease event 
//
void ApplicationWindow::keyReleaseEvent(QKeyEvent *e)
{
  printf("ApplicationWindow::keyReleaseEvent\n");
  
  if (e->key() == QKeyEvent::Key_F9) {
    if (!e->isAutoRepeat()) {
      printf("<- F9\n");
      g_MapWnd.m_bTextActive = true;
      pMapWidget->repaint(false);
    }
  }
}


//-----------------------------------------------------------------------------
// About box containing version and user information
//
void ApplicationWindow::about()
{
  //
  // Read the ini file for some last used settings.
  // Any of these can be overridden by paramaters
  // specified on the command line
  //
  char szSerialNum[256];
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iniFile.GetValueS(szSerialNum, "Prefs", "SerialNum", "");

  QString s(QString(SZ_PRGNAME "\n"
                    SZ_VERSION  "\n\n"
                    "Serial: %1\n\n"
                    "(c) Player One 2003\n"
                    "All rights reserved").arg(szSerialNum));

  QMessageBox::about(this, "About", s);
}

//-----------------------------------------------------------------------------
// 
//
void ApplicationWindow::index()
{
  //html_browse("help/main1.htm");
  chdir("./help");
  //win32_shell_browse("main1.1.pdf");
  ShellExecute(NULL, L"open", L"main1.1.pdf", NULL, NULL, SW_SHOWNORMAL);
  chdir("../");
}


//-----------------------------------------------------------------------------
// 
//
void ApplicationWindow::aboutQt()
{
  QMessageBox::aboutQt(this, "Qt About");
}



//-----------------------------------------------------------------------------
// Create and populate the database tree
//
void ApplicationWindow::createDatabaseTree()
{
  printf("ApplicationWindow::createDatabaseTree\n");

  // The following assumptions about the data source, wpt.csv are made:
  // 1) The ID is sequential and unique
  // 2) The info field is sorted alphabetically by region
  //    Not vital but there is no sorting done so
  //    what you've got is what you'll get
  // 3) ICAO of _rgn denotes a region
  // 4) ICAO of _cnt denotes a country
  //
  CWptSet *wpt = &(g_MapWnd.wpt);
  wpt->MoveFirst();
  //QListViewItem *wptdb = new QListViewItem(m_top, "Database");
  wptdb = new QListViewItem(m_top, "Database");
    QListViewItem *region;
    QListViewItem *country;
      QListViewItem *apt;
      QListViewItem *nav;

  int i = 0;
  while (wpt->IsEOF() == false) {
    if (wpt->m_ICAO.compare("_rgn") == 0) {
      // adding a region
      printf("+ %s\n", wpt->m_INFO.c_str());
      region = new QListViewItem(wptdb, wpt->m_INFO.c_str());

      i++;
      wpt->MoveNext();
      continue;
    }

    if (wpt->m_ICAO.compare("_cnt") == 0) {
      // adding a country
      printf("  - %s\n", wpt->m_INFO.c_str());
      country = new QListViewItem(region, wpt->m_INFO.c_str());
        apt  = new QListViewItem(country, SZ_AIRPORT); //"Airport"
        nav  = new QListViewItem(country, "Navaid");

      i++;
      wpt->MoveNext();
      continue;
    }

    switch ((int) wpt->m_TYPE) {
      QListViewItem *entry;
        QListViewItem *icao;
        QListViewItem *info;
        QListViewItem *rlatlon;
        QListViewItem *relev;
        QListViewItem *iiid;

      case W_APT:
        entry   = new QListViewItem(apt,   wpt->m_INFO.c_str());
        icao    = new QListViewItem(entry, QString("icao:%1").arg(wpt->m_ICAO.c_str()));
        info    = new QListViewItem(entry, QString("info:%1").arg(wpt->m_INFO.c_str()));
        rlatlon = new QListViewItem(entry, QString("coor:%1 %2").arg(wpt->m_rLat, 7).arg(wpt->m_rLon, 7));
        relev   = new QListViewItem(entry, QString("elev:%1").arg(wpt->m_rElev));
        iiid    = new QListViewItem(entry, QString("iiid:%1").arg(i));
        break;
      case W_VOR:
      case W_NDB:
        entry   = new QListViewItem(nav,   wpt->m_ICAO.c_str());
        icao    = new QListViewItem(entry, QString("icao:%1").arg(wpt->m_ICAO.c_str()));
        info    = new QListViewItem(entry, QString("info:%1").arg(wpt->m_INFO.c_str()));
        rlatlon = new QListViewItem(entry, QString("coor:%1 %2").arg(wpt->m_rLat).arg(wpt->m_rLon));
        iiid    = new QListViewItem(entry, QString("iiid:%1").arg(i));
        break;
    }

    i++;
    wpt->MoveNext();
  }
}


//-----------------------------------------------------------------------------
// Helper function to recurse the database tree
//
// This is a tricky little piece of code that uses recursion to get to 
// all the items in the tree down to the specified 'depth'
//
//int level = 0; // only during debugging
void ApplicationWindow::recurseAllTree(QListViewItem * it, bool bOpen, int depth)
{
  static int level = 0; // must be static, due to recursion
  level++;

  if (level < depth) {
    it->setOpen(bOpen);

    QListViewItem *it1 = it->firstChild();
    while (it1) {
      recurseAllTree(it1, bOpen, depth);

      it1->setOpen(bOpen);
      it1 = it1->nextSibling(); 
    }
  }
  level--;
}


//-----------------------------------------------------------------------------
// Expand all the children in the database tree
//
void ApplicationWindow::expandAllDatabaseTree()
{
  QListViewItem *it = m_top->firstChild();
  m_top->setCurrentItem(it);
  recurseAllTree(it, true, 4);
}


//-----------------------------------------------------------------------------
// Collapse all the children in the database tree
//
void ApplicationWindow::collapseAllDatabaseTree()
{
  QListViewItem *it = m_top->firstChild();
  m_top->setCurrentItem(it);
  recurseAllTree(it, false, 99);
}



//-----------------------------------------------------------------------------
//
//
void ApplicationWindow::createDownloadTree()
{
  //return;

  printf("ApplicationWindow::createDownloadTree\n");

  //QListViewItem *downloaddb = new QListViewItem(m_top, "Download");
  downloaddb = new QListViewItem(m_top, "Download");
    QListViewItem *rtedl = new QListViewItem(downloaddb, "Route");
    QListViewItem *trkdl = new QListViewItem(downloaddb, SZ_TRACK);
    QListViewItem *wptdl = new QListViewItem(downloaddb, "Waypoint");
      QListViewItem *entry;
        //QListViewItem *icao;
        QListViewItem *info;
        QListViewItem *rlatlon;
        //QListViewItem *relev;
        //QListViewItem *iiid;


  // routes
  /*
  entry = new QListViewItem(rtedl, "YSEN-YAUG");
  entry = new QListViewItem(rtedl, "YSEN-YESP");
  */


  // GPS tracks
  g_MapWnd.unloadGpsAllTrack();

  QFile gpsTrkFile("gps-trk.csv");
  gpsTrkFile.remove();

  QDir d("g3", "*.csv");
  d.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
  //d.setSorting( QDir::Size | QDir::Reversed );
  const QFileInfoList *list = d.entryInfoList();
  if (list) {
  QFileInfoListIterator it(*list);        // create list iterator
  QFileInfo *fi;                          // pointer for traversing
  printf( "     BYTES FILENAME\n" );      // print header
  while ( (fi=it.current()) ) {           // for each file...
    printf( "%10li %s\n", fi->size(), fi->fileName().data() );
    entry = new QListViewItem(trkdl, fi->fileName().data());

    gpsTrkFile.open(IO_Append | IO_WriteOnly);
    QString qs = QString("0, 0, 0\n");
    gpsTrkFile.writeBlock(qs.latin1(), qs.length());
    gpsTrkFile.close();

    FileAppend("gps-trk.csv", QString("g3/%1").arg(fi->fileName().data()));

    ++it;                                 // goto next list element
  }

  g_MapWnd.loadGpsAllTrack();

  // GPS waypoints
  g_MapWnd.gpsWpt.MoveFirst();
  while (g_MapWnd.gpsWpt.IsEOF() == false) {
    //g_MapWnd.gpsWpt.m_rLon
    //g_MapWnd.gpsWpt.m_rLat

    QString qs = QString( );
    //entry = new QListViewItem(wptdl, QString("%1").arg(g_MapWnd.gpsWpt.m_ICAO.c_str()));
    entry = new QListViewItem(wptdl, g_MapWnd.gpsWpt.m_ICAO.c_str());
      //icao    = new QListViewItem(entry, QString("icao:%1").arg(g_MapWnd.gpsWpt.m_ICAO.c_str()));
      info    = new QListViewItem(entry, QString("info:%1").arg(g_MapWnd.gpsWpt.m_INFO.c_str()));
      rlatlon = new QListViewItem(entry, QString("coor:%1 %2").arg(g_MapWnd.gpsWpt.m_rLat, 7).arg(g_MapWnd.gpsWpt.m_rLon, 7));
      //relev   = new QListViewItem(entry, QString("elev:%1").arg(g_MapWnd.gpsWpt.m_rElev));
      //iiid    = new QListViewItem(entry, QString("iiid:%1").arg(i));


    g_MapWnd.gpsWpt.MoveNext();
  }
  }
}


//-----------------------------------------------------------------------------
// restore RouteWpt and RouteLst from the undo buffer
//
void ApplicationWindow::editUndo()
{
  if (g_MapWnd._RouteWpt != 0) {
    long tmpRouteWpt = g_MapWnd.RouteWpt;
    g_MapWnd.RouteWpt = g_MapWnd._RouteWpt;
    g_MapWnd._RouteWpt = tmpRouteWpt;

    long tmpRouteLst[NR_LEGS];
    memcpy(tmpRouteLst, g_MapWnd.RouteLst, sizeof(g_MapWnd.RouteLst));
    memcpy(g_MapWnd.RouteLst, g_MapWnd._RouteLst, sizeof(g_MapWnd.RouteLst));
    memcpy(g_MapWnd._RouteLst, tmpRouteLst, sizeof(g_MapWnd.RouteLst));

    //edit->setItemEnabled(id_undo, false);
    //--edit->setItemEnabled(id_redo, true);
    pMapWidget->repaint(true);
  }
}

//-----------------------------------------------------------------------------
// "undo" the last undo
//
void ApplicationWindow::editRedo()
{
  long tmpRouteWpt = g_MapWnd._RouteWpt;
  g_MapWnd._RouteWpt = g_MapWnd.RouteWpt;
  g_MapWnd.RouteWpt = tmpRouteWpt;

  long tmpRouteLst[NR_LEGS];
  memcpy(tmpRouteLst, g_MapWnd._RouteLst, sizeof(g_MapWnd.RouteLst));
  memcpy(g_MapWnd._RouteLst, g_MapWnd.RouteLst, sizeof(g_MapWnd.RouteLst));
  memcpy(g_MapWnd.RouteLst, tmpRouteLst, sizeof(g_MapWnd.RouteLst));

  //edit->setItemEnabled(id_undo, true);
  edit->setItemEnabled(id_redo, false);
  pMapWidget->repaint(true);
}


//-----------------------------------------------------------------------------
// Handler for the aircraftChanged() signal. The signal
// is emitted when a new aircraft is chosen.
//
void ApplicationWindow::aircraftChanged()
{
  printf("ApplicationWindow::aircraftChanged()\n");
  g_MapWnd.loadAircraftDefaults();
  g_MapWnd.populateTrue();
  g_MapWnd.populateMagnetic();

  //m_massBalanceGadget->ReLoad();
  //m_massBalanceGadget->ReCalc();

  if (g_MapWnd.m_Units == 0) {
    g_MapWnd.trueTable->horizontalHeader()->setLabel(12, "FF lit/hr");
    g_MapWnd.magneticTable->horizontalHeader()->setLabel(9, "Fuel lit");
  }
  else {
    g_MapWnd.trueTable->horizontalHeader()->setLabel(12, "FF usg/hr");
    g_MapWnd.magneticTable->horizontalHeader()->setLabel(9, "Fuel usg");
  }

  // update the title bar to reflect the new AC
  setTitle();
}

