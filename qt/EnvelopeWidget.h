
#include <qwidget.h>

#ifndef __GRAPHICPAINTER_H
#include "GraphicPainter.h"
#endif



/*
  final static BasicStroke basicStroke  = new BasicStroke(1.0f);
  final static BasicStroke wideStroke   = new BasicStroke(3.0f);
  final static float dash1[] = {2.0f};
  final static BasicStroke dashedStroke = new BasicStroke(1.0f,
                                                          BasicStroke.CAP_BUTT,
                                                          BasicStroke.JOIN_MITER,
                                                          10.0f, dash1, 0.0f);
*/
#define BORDER 30

class EnvelopeWidget : public QWidget
{
public:
  double m_Arm;
  double m_Weight;
  double m_ZeroFuelArm;
  double m_ZeroFuelWeight;

  double MaxX; //= -9999;
  double MinX; //= 9999;
  double MaxY; //= -9999;
  double MinY; //= 9999;

  /**
   * Default constructor
   */
  EnvelopeWidget(QWidget *parent=0, const char *name=0, WFlags f=0);

  void cvtPt(int *x, int *y, double rx, double ry, QRect r);
  //int m_x;
  //int m_y;
  //void cvtPt(double rx, double ry, double width, double height);

  int iNrPoints;

  //-----------------------------------------------------------------------------
  // Paint - the main paint method
  //
  void paintEvent(QPaintEvent* e);
  void PaintMain(GraphicPainter *gc);


};


/*

public:
  double Arm;
  double Weight;
  double ZFArm;
  double ZFWeight;

// Operations
public:
  double MaxX;
  double MinX;
  double MaxY;
  double MinY;



// WhiteStatic.cpp : implementation file
//

#include "stdafx.h"
#include "WhiteStatic.h"
#include "ucolor.h"
#include "MapWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWhiteStatic

CWhiteStatic::CWhiteStatic()
{
  MaxX = -9999;
  MinX = 9999;
  MaxY = -9999;
  MinY = 9999;
}

CWhiteStatic::~CWhiteStatic()
{
}


BEGIN_MESSAGE_MAP(CWhiteStatic, CStatic)
  //{{AFX_MSG_MAP(CWhiteStatic)
  ON_WM_ERASEBKGND()
  ON_WM_PAINT()
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWhiteStatic message handlers


BOOL CWhiteStatic::OnEraseBkgnd(CDC* pDC)
{
  CRect rect;

  GetClientRect(&rect);
  pDC->PatBlt(0, 0, rect.Width(), rect.Height(), WHITENESS);

  return TRUE;  // message handled
  //return CWnd::OnEraseBkgnd(pDC);
}



typedef struct {
  double x;
  double y;
} dPoint;

#define BORDER 30

void CWhiteStatic::cvtPt(int *x, int *y, double rx, double ry, CRect r)
{
  *x = (r.right-r.left-(BORDER*2))/(MaxX-MinX)*(rx-MinX) + BORDER;
  *y = (r.bottom-r.top-(BORDER*2))/(MaxY-MinY)*(ry-MinY) + BORDER;
}

void CWhiteStatic::OnPaint()
{
  CPaintDC gc(this); // device context for painting
  CRect r;
  CFont Font, *OldFont;

  // Twin Commanche
  dPoint tbl[] = {{86.5, 2650},
                  {86.5, 3600},
                  {87.6, 3700},
                  {88.3, 3800},
                  {89.0, 3900},
                  {90.5, 3900},
                  {91.0, 3800},
                  {91.4, 3700},
                  {92.0, 3650},
                  {92.0, 2650},
                  {86.5, 2650}};
  int iNrPoints = 11;
  //Arm = 90.0;
  //Weight = 3425.0;

  int x, y;


  SetupFont(&Font, 14);
  OldFont = (CFont*) gc.SelectObject(&Font);


  char ss[256];
  char szData[256];
  char szItem[4];
  int i = 0;
  char *sp;
  int rv;

  double arm, weight;

  i = 0;
  while (TRUE) {
    itoa(i, szItem, 10);
    rv = GetPrivateProfileString("CGEnvelope", szItem, "", szData, 255, ".\\current.ad.tmp");
    if (rv <= 0)
        break;

    strcpy(ss, szData);
    sp = strtok(ss, ",");
    arm = atof(sp);
    sp = strtok(NULL, ",");
    weight = atof(sp);
    tbl[i].x = arm;
    tbl[i].y = weight;

    if (tbl[i].x > MaxX) MaxX = tbl[i].x;
    if (tbl[i].x < MinX) MinX = tbl[i].x;
    if (tbl[i].y > MaxY) MaxY = tbl[i].y;
    if (tbl[i].y < MinY) MinY = tbl[i].y;
    i++;
  }

  iNrPoints = i;

  GetClientRect(&r);

  gc.SetMapMode(MM_ANISOTROPIC);
  CSize ext = gc.GetViewportExt();
  gc.SetViewportExt(1, -1);
  gc.SetViewportOrg(0, r.bottom);

  // Envelope
  {
    CBrush Brush(RGB_LTGRAY);
    CPen   Pen(PS_SOLID, 1, RGB_LTGRAY);
    gc.SelectObject(Brush);
    gc.SelectObject(Pen);
    CPoint pts[255];

    for (i = 0; i < iNrPoints; i++) {
      cvtPt(&x, &y, tbl[i].x, tbl[i].y, r);
      pts[i].x = x;
      pts[i].y = y;
    }
    gc.Polygon(pts, iNrPoints);
  }

  // Point - Zero Fuel
  {
    CBrush Brush(RGB_LTRED);
    CPen   Pen2(PS_SOLID, 2, RGB_BLACK);
    CPen   Pen1(PS_SOLID, 0, RGB_BLACK);
    gc.SelectObject(Brush);
    gc.SelectObject(Pen2);

    cvtPt(&x, &y, MinX, ZFWeight, r);
    gc.MoveTo(x, y);
    cvtPt(&x, &y, ZFArm, ZFWeight, r);
    gc.LineTo(x, y);
    cvtPt(&x, &y, ZFArm, MinY, r);
    gc.LineTo(x, y);

    gc.SelectObject(Pen1);
    cvtPt(&x, &y, ZFArm, ZFWeight, r);
    gc.Ellipse(x-7, y-7, x+7, y+7);

  }

  // Point - Loaded
  {
    CBrush Brush(RGB_LTGREEN);
    CPen   Pen2(PS_SOLID, 2, RGB_BLACK);
    CPen   Pen1(PS_SOLID, 0, RGB_BLACK);
    gc.SelectObject(Brush);
    gc.SelectObject(Pen2);

    cvtPt(&x, &y, MinX, Weight, r);
    gc.MoveTo(x, y);
    cvtPt(&x, &y, Arm, Weight, r);
    gc.LineTo(x, y);
    cvtPt(&x, &y, Arm, MinY, r);
    gc.LineTo(x, y);

    gc.SelectObject(Pen1);
    cvtPt(&x, &y, Arm, Weight, r);
    gc.Ellipse(x-7, y-7, x+7, y+7);

  }


  // Label the axis
  cvtPt(&x, &y, MinX, MinY, r);
  sprintf(ss, "%.1f", MinX);
  gc.TextOut(x, y-5, ss);

  cvtPt(&x, &y, MaxX, MinY, r);
  sprintf(ss, "%.1f", MaxX);
  gc.TextOut(x, y-5, ss);

  cvtPt(&x, &y, MinX, MinY, r);
  sprintf(ss, "%.0f", MinY);
  gc.TextOut(x+5, y+15, ss);

  cvtPt(&x, &y, MinX, MaxY, r);
  sprintf(ss, "%.0f", MaxY);
  gc.TextOut(x+5, y+15, ss);


}

Long EZ
          Weight Station Moment
Empty     730    111.7  81541
Oil       8      140.0  1120
Fuel      150    104.5  15675
Pilot     210    59.0   12390
Pax       210    103.0  21630
Baggage   15     90.0   1350
Total     1323   101.06 133706


1  97  900
2  97 1325
3 104 1325
4 104  900
5  97  900

6  97 1425
7 104 1425
8 104  900
9  97  900


*/
