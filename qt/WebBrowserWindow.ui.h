//-----------------------------------------------------------------------------
// $Id: WebBrowserWindow.ui.h,v 1.1 2006/04/25 11:58:49 player Exp $
//
// ui.h extension file, included from the uic-generated form implementation.
//
// If you want to add, delete, or rename functions or slots, use
// Qt Designer to update this file, preserving your code.
//
// You should not define a constructor or destructor in this file.
// Instead, write your code in functions called init() and destroy().
// These will automatically be called by the form's constructor and
// destructor.
//
//-----------------------------------------------------------------------------
//
// http://msdn.microsoft.com/library/default.asp?url=/workshop/browser/webbrowser/reference/methods/goback.asp
//

#include <qaxwidget.h>
#include <qmessagebox.h>
#include <qprogressbar.h>
#include <qstatusbar.h>

#include <DOCOBJ.H> // Microsoft specific

#include "wings.xpm"

void WebBrowserWindow::init()
{
	QPixmap wingsIcon = QPixmap(wings);
	setIcon(wingsIcon);

	
	pb = new QProgressBar( statusBar() );
    pb->setPercentageVisible( FALSE );
    pb->hide();
    statusBar()->addWidget( pb, 0, TRUE );

    connect( WebBrowser, SIGNAL(ProgressChange(int,int)), this, SLOT(setProgress(int,int)) );
    connect( WebBrowser, SIGNAL(StatusTextChange(const QString&)), statusBar(), SLOT(message(const QString&)) );

    WebBrowser->dynamicCall( "GoHome()" );
}

void WebBrowserWindow::go()
{
    actionStop->setEnabled( TRUE );
    WebBrowser->dynamicCall( "Navigate(const QString&)", addressEdit->text() );
}

void WebBrowserWindow::setTitle( const QString &title )
{
    //setCaption( "Qt WebBrowser - " + title );
    setCaption(title);
}

void WebBrowserWindow::setProgress( int a, int b )
{
  if ( a <= 0 || b <= 0 ) {
    pb->hide();
    return;
  }
  pb->show();
  pb->setTotalSteps( b );
  pb->setProgress( a );
}


void WebBrowserWindow::setCommandState( int cmd, bool on )
{
    switch ( cmd ) {
    case 1:
  actionForward->setEnabled( on );
  break;
    case 2:
  actionBack->setEnabled( on );
  break;
    }
}

void WebBrowserWindow::navigateBegin()
{
  actionStop->setEnabled( TRUE );
}

void WebBrowserWindow::navigateComplete()
{
  actionStop->setEnabled( FALSE );
}

void WebBrowserWindow::newWindow()
{
  WebBrowserWindow *window = new WebBrowserWindow;
  window->show();
  if ( addressEdit->text().isEmpty() )
    return;

  window->addressEdit->setText( addressEdit->text() );
  window->actionStop->setEnabled( TRUE );
  window->go();
}


void WebBrowserWindow::pageSetupSlot()
{
  WebBrowser->dynamicCall("ExecWB(long cmdId, long cmdExecOpt)", OLECMDID_PAGESETUP, OLECMDEXECOPT_PROMPTUSER);
}

void WebBrowserWindow::printSlot()
{
  WebBrowser->dynamicCall("ExecWB(long cmdId, long cmdExecOpt)", OLECMDID_PRINT, OLECMDEXECOPT_PROMPTUSER);
}

void WebBrowserWindow::printPreviewSlot()
{
  WebBrowser->dynamicCall("ExecWB(long cmdId, long cmdExecOpt)", OLECMDID_PRINTPREVIEW, OLECMDEXECOPT_DONTPROMPTUSER);
}
 

void WebBrowserWindow::aboutSlot()
{
/*
  QMessageBox::about(this, tr("About WebBrowser"),
    tr("This Example has been created using the ActiveQt integration into Qt Designer.\n"
       "It demonstrates the use of QAxWidget to embed the Internet Explorer ActiveX\n"
       "control into a Qt application."));
       */
}

void WebBrowserWindow::aboutQtSlot()
{
  /*
    QMessageBox::aboutQt(this, tr("About Qt"));
    */
/*
  Sub print_wb()
    WebBrowser1.ExecWB OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER
  End Sub
*/
}


/*
typedef  enum OLECMDID  { 
  OLECMDID_OPEN = 1,
  OLECMDID_NEW  = 2,
  OLECMDID_SAVE = 3,
  OLECMDID_SAVEAS = 4,
  OLECMDID_SAVECOPYAS = 5,
  OLECMDID_PRINT  = 6,
  OLECMDID_PRINTPREVIEW = 7,
  OLECMDID_PAGESETUP  = 8,
  OLECMDID_SPELL  = 9,
  OLECMDID_PROPERTIES = 10,
  OLECMDID_CUT  = 11,
  OLECMDID_COPY = 12,
  OLECMDID_PASTE  = 13,
  OLECMDID_PASTESPECIAL = 14,
  OLECMDID_UNDO = 15,
  OLECMDID_REDO = 16,
  OLECMDID_SELECTALL  = 17,
  OLECMDID_CLEARSELECTION = 18,
  OLECMDID_ZOOM = 19,
  OLECMDID_GETZOOMRANGE = 20,
  OLECMDID_UPDATECOMMANDS = 21,
  OLECMDID_REFRESH  = 22,
  OLECMDID_STOP = 23,
  OLECMDID_HIDETOOLBARS = 24,
  OLECMDID_SETPROGRESSMAX = 25,
  OLECMDID_SETPROGRESSPOS = 26,
  OLECMDID_SETPROGRESSTEXT  = 27,
  OLECMDID_SETTITLE = 28,
  OLECMDID_SETDOWNLOADSTATE = 29,
  OLECMDID_STOPDOWNLOAD = 30,
  OLECMDID_ONTOOLBARACTIVATED = 31,
  OLECMDID_FIND = 32,
  OLECMDID_DELETE = 33,
  OLECMDID_HTTPEQUIV  = 34,
  OLECMDID_HTTPEQUIV_DONE = 35,
  OLECMDID_ENABLE_INTERACTION = 36,
  OLECMDID_ONUNLOAD = 37,
  OLECMDID_PROPERTYBAG2 = 38,
  OLECMDID_PREREFRESH = 39
} OLECMDID;

  
typedef enum OLECMDEXECOPT {  
  OLECMDEXECOPT_DODEFAULT = 0,
  OLECMDEXECOPT_PROMPTUSER  = 1,
  OLECMDEXECOPT_DONTPROMPTUSER  = 2,
  OLECMDEXECOPT_SHOWHELP  = 3
} OLECMDEXECOPT;

*/
