#ifdef _MOTIF_

#undef WIN32

#include <stdio.h>

#include <Xm/Xm.h>
#include <Xm/MainW.h>
#include <Xm/CascadeB.h>
#include <Xm/Label.h>

#include <Xm/DrawingA.h>
#include <Xm/PushBG.h>
#include <Xm/PushB.h>
#include <Xm/RowColumn.h>
#include <Xm/ScrolledW.h>
#include <Xm/Form.h>

#include "drawing.h"
#include "MapWnd.h"

/* arbitrary width and height values */
#define WIDTH  300
#define HEIGHT 200


/* callback prototypes */
void inputCallback(Widget widget, XtPointer client_data, XtPointer call_data);
void resizeCallback(Widget widget, XtPointer client_data, XtPointer call_data);
void exposeCallback(Widget widget, XtPointer client_data, XtPointer call_data);

/* callback prototypes */
void clear_it(Widget widget, XtPointer client_data, XtPointer call_data);

void set_color(Widget widget, XtPointer client_data, XtPointer call_data);
void exit(int) ;


extern XtAppContext   app;

CMapWnd MapWnd;


/* XLIB Data */
Display *display;
Screen  *screen_ptr;
Window   window;
Widget   drawing_a;

GC     gc     = (GC) 0 ;
Pixmap pixmap = (Pixmap) 0 ;

bool   dirty  = true;

/* dimensions of drawing area (pixmap) */
Dimension width, height;

/** <!------------------------------------------------------------------------->
 *  Create and initialise the main drawing area (map)
 */
Widget create_draw_form(Widget parent)
{
//  Widget         toplevel, form, pb;
  Widget         form;
  XGCValues      gcv;
  Arg            args[12];
  int            i, n;
  XtActionsRec   actions;

  /* for the DrawingArea widget */
  /* ManagerGadget* functions are necessary for DrawingArea widgets
   * that steal away button events from normal translation tables.
   */
  String translations = "<Btn1Motion>: DrawingAreaInput() ManagerGadgetButtonMotion()\n \
                         <Btn1Down>:   DrawingAreaInput() ManagerGadgetArm()\n \
                         <Btn1Up>:     DrawingAreaInput() ManagerGadgetActivate()\n \
                         <Btn2Down>:   DrawingAreaInput() ManagerGadgetArm()\n \
                         <Btn2Up>:     DrawingAreaInput() ManagerGadgetActivate()\n \
                         <Btn3Down>:   DrawingAreaInput() ManagerGadgetArm()\n \
                         <Btn3Up>:     DrawingAreaInput() ManagerGadgetActivate()\n \
                         ";

  XtTranslations parsed_translations = XtParseTranslationTable (translations);
 
  /* Create a MainWindow to contain the drawing area */
  //form = XmCreateForm(parent, "form", NULL, 0);
  form = parent;

  /* Create a GC for drawing (callback).  Used a lot -- make global */
  gcv.foreground = WhitePixelOfScreen(XtScreen(form));
  gc = XCreateGC(XtDisplay(form), RootWindowOfScreen(XtScreen(form)), GCForeground, &gcv);
 
 
  /* Create a DrawingArea widget.  Make it 5 inches wide by 6 inches tall.
   * Attach it to the top, bottom, left and right so that resize messages
         * are generated when the parent window is resized.
   */
  //n = 0;
  //XtSetArg(args[n], XmNunitType, Xm1000TH_INCHES); n++;
  //XtSetArg(args[n], XmNwidth, 5000); n++; /* 5 inches */
  //XtSetArg(args[n], XmNheight, 6000); n++; /* 6 inches */
  //XtSetArg(args[n], XmNresizePolicy, XmNONE); n++;  /* remain this a fixed size */
  //XtSetArg(args[n], XmNtranslations, parsed_translations); n++;
  n = 0;
  XtSetArg(args[n], XmNunitType, XmPIXELS); n++; /* in pixel units */
  XtSetArg(args[n], XmNwidth, WIDTH); n++;
  XtSetArg(args[n], XmNheight, HEIGHT); n++;
  XtSetArg(args[n], XmNresizePolicy, XmRESIZE_ANY); n++;  /* allow resize */
  XtSetArg(args[n], XmNtopAttachment, XmATTACH_FORM); n++;
  XtSetArg(args[n], XmNbottomAttachment, XmATTACH_FORM); n++;
  XtSetArg(args[n], XmNleftAttachment, XmATTACH_WIDGET); n++;
  XtSetArg(args[n], XmNrightAttachment, XmATTACH_FORM); n++;
  XtSetArg(args[n], XmNtranslations, parsed_translations); n++; /* override the translations so we can get motion <Btn1Motion> */
  drawing_a = XmCreateDrawingArea(form, "drawing_a", args, n);

  //XtOverrideTranslations (drawing_a, XtParseTranslationTable (translations));

  /* Create a DrawingArea widget. */
  //drawing_a = XmCreateDrawingArea(form, "drawing_a", NULL, 0);

  /* add callback for all mouse and keyboard input events */
  XtAddCallback(drawing_a, XmNinputCallback, inputCallback, NULL);
  XtAddCallback(drawing_a, XmNexposeCallback, exposeCallback, NULL);
  XtAddCallback(drawing_a, XmNresizeCallback, resizeCallback, NULL);

  // to resize - XtVaSetValues(drawing_a, XmNwidth, 100, XmNheight, 100, NULL);

  XtManageChild(drawing_a);
 
  // /* Pushing the clear button clears the drawing area widget */
  // XtAddCallback (pb, XmNactivateCallback, clear_it, (XtPointer) drawing_a);
 
  /* convert drawing area back to pixels to get its width and height */
  /* this could be useful - inches to pix */
  XtVaSetValues(drawing_a, XmNunitType, XmPIXELS, NULL);
  XtVaGetValues(drawing_a, XmNwidth, &width, XmNheight, &height, NULL);
 
  /* create a pixmap the same size as the drawing area. */
  pixmap = XCreatePixmap(XtDisplay(drawing_a), RootWindowOfScreen(XtScreen(drawing_a)),
                         width, height, DefaultDepthOfScreen(XtScreen(drawing_a)));

  /* clear pixmap with white */
  set_color(drawing_a, "White", NULL);
  XFillRectangle(XtDisplay(drawing_a), pixmap, gc, 0, 0, width, height);
 
  // The pen color ?
  set_color(drawing_a, "Purple", NULL);

  // set the maps starting width and height
  MapWnd.OnSize(WIDTH, HEIGHT);
  //MapWnd.ZoomToFit();
 
  return form;
}


/** <!------------------------------------------------------------------------->
 * Clear the window by clearing the pixmap and calling XCopyArea()
 */
void clear_it(Widget pb, XtPointer client_data, XtPointer call_data)
{
  Widget drawing_a = (Widget) client_data;
  XmPushButtonCallbackStruct *cbs = (XmPushButtonCallbackStruct *) call_data;
 
  /* clear pixmap with white */
  XSetForeground(XtDisplay(drawing_a), gc, WhitePixelOfScreen(XtScreen(drawing_a)));

  /* this clears the pixmap */
  XFillRectangle(XtDisplay(drawing_a), pixmap, gc, 0, 0, width, height);

  /* drawing is now done using black; change the gc */
  XSetForeground(XtDisplay(drawing_a), gc, BlackPixelOfScreen(XtScreen(drawing_a)));

  /* render the newly cleared pixmap onto the window */
  XCopyArea(cbs->event->xbutton.display, pixmap, XtWindow(drawing_a), gc, 0, 0, width, height, 0, 0);
}

/** <!------------------------------------------------------------------------->
 * callback routine for when any of the color tiles are pressed.
 * This general function may also be used to set the global gc's
 * color directly.  Just provide a widget and a color name.
 */
void set_color(Widget widget, XtPointer client_data, XtPointer call_data)
{
  String   color = (String) client_data;
  Display *dpy = XtDisplay(widget);
  Colormap cmap = DefaultColormapOfScreen(XtScreen(widget));
  XColor   col, unused;
 
  if (!XAllocNamedColor(dpy, cmap, color, &col, &unused)) {
          char buf[32];
          sprintf(buf, "Can't alloc %s", color);
          XtWarning(buf);
          return;
  }
 
  XSetForeground(dpy, gc, col.pixel);
}



/** <!------------------------------------------------------------------------->
 * exposeCallback is called whenever all or portions of the drawing area is
 * exposed.  This includes newly exposed portions of the widget resulting
 * from the user's interaction with the scrollbars.
 */
void exposeCallback(Widget drawing_a, XtPointer client_data, XtPointer call_data)
{
  printf("drawing.c - exposeCallback\n");

  /* for some reason, resizing the window generates many "expose" requests :-(
   * using a "dirty" flag, we van work around the problem somewhat.
   */
  if (dirty) {
    XmDrawingAreaCallbackStruct *cbs = (XmDrawingAreaCallbackStruct *) call_data;
    XCopyArea(cbs->event->xexpose.display, pixmap, cbs->window, gc, 0, 0, width, height, 0, 0);
 
    /*XDrawRectangle(cbs->event->xexpose.display, cbs->window, gc, 10, 10, 100, 100);*/
    GraphicPainter gcontext;
    gcontext.m_Display = cbs->event->xexpose.display;
    gcontext.m_Window = cbs->window;
    gcontext.m_GC = gc;
 
    MapWnd.PaintMain(&gcontext);  // the whole echilada
 
    MapWnd.drawWpt(&gcontext);
    //MapWnd.DrawRoute(&gcontext);
    //MapWnd.DrawMovements(&gcontext);
    dirty = false;
  }
}



/** <!------------------------------------------------------------------------->
 * Callback routine for DrawingArea's input (mouse & kbd) callbacks
 * as well as the PushButton's activate callback.  Determine which
 * it is by testing the cbs->reason field.
 */
void inputCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
  printf("drawing.c - inputCallback\n");

  static Position x, y;
  XmDrawingAreaCallbackStruct *cbs = (XmDrawingAreaCallbackStruct *) call_data;
  XEvent  *event = cbs->event;
  Display *dpy = event->xany.display;
 
  printf("button=%d  type=%d reason=%d\n", event->xbutton.button, event->xany.type, cbs->reason);

  if (cbs->reason == XmCR_INPUT) {
      /* activated by DrawingArea input event -- draw lines.
       * Button Down events anchor the initial point and Button
       * Up draws from the anchor point to the button-up point.
       */
      POINT point = {event->xbutton.x, event->xbutton.y};

      if (event->xany.type == ButtonPress) {
        if (event->xbutton.button == Button1) {
          printf("drawing.c - inputCallback - ButtonPress 1\n");
          MapWnd.mouseLeftButtonDown(point);
        }
        if (event->xbutton.button == Button2) {
          printf("drawing.c - inputCallback - ButtonPress 2\n");
          MapWnd.mouseMiddleButtonDown(point);
        }
        if (event->xbutton.button == Button3) {
          printf("drawing.c - inputCallback - ButtonPress 3\n");
          MapWnd.mouseRightButtonDown(point);
        }
      }

      if (event->xany.type == ButtonRelease) {
        if (event->xbutton.button == Button1) {
          printf("drawing.c - inputCallback - ButtonRelease 1\n");
          MapWnd.mouseLeftButtonUp(point);
        }
        if (event->xbutton.button == Button2) {
          printf("drawing.c - inputCallback - ButtonRelease 2\n");
          //MapWnd.mouseMiddleButtonUp(point);
        }
        if (event->xbutton.button == Button3) {
          printf("drawing.c - inputCallback - ButtonRelease 3\n");
          MapWnd.mouseRightButtonUp(point);
        }

        /* mouse interaction with the map area done,
         * force a repaint
         */
        dirty = true;
        exposeCallback(widget, client_data, call_data);
      }

      if (event->xany.type == MotionNotify) {
          printf("drawing.c - inputCallback - MotionNotify\n");

          GraphicPainter gcontext;
          gcontext.m_Display = cbs->event->xexpose.display;
          gcontext.m_Window = cbs->window;
          gcontext.m_GC = gc;

          MapWnd.mouseDragged(&gcontext, point);
      }
  }
}


/** <!------------------------------------------------------------------------->
 * This function handles both expose and resize (configure) events.
 * For XmCR_EXPOSE, just call redraw() and return. For resizing,
 * we must calculate the new size of the viewable area and possibly
 * reposition the pixmap's display and position offsets. Since we
 * are also responsible for the ScrollBars, adjust them accordingly.
 */
void resizeCallback(Widget widget, XtPointer client_data, XtPointer call_data)
{
  printf("drawing.c - resizeCallback\n");

  Dimension  new_width, new_height, oldw, oldh;
  Boolean    do_clear = False;
  XmDrawingAreaCallbackStruct *cbs = (XmDrawingAreaCallbackStruct *) call_data;
 
  if (cbs->reason == XmCR_EXPOSE) {
      //redraw (cbs->window);
      //return;
  }

  oldw = width;
  oldh = height;

  /* Unfortunately, the cbs->event field is NULL, so we have to have
   * get the size of the drawing area manually. A mis-design of
   * the DrawingArea widget--not a bug (technically).
   */
  XtVaGetValues (drawing_a, XmNwidth, &width, XmNheight, &height, NULL);
  printf("width=%d height=%d\n", width, height);
  MapWnd.OnSize(width, height);
  dirty = true;

#if 0

    /* Get the size of the viewable area in "units lengths" where
    ** each unit is the cell size for each dimension. This prevents
    ** rounding error for the pix_voffset and pix_hoffset values later.
    */
    new_width = view_width / cell_width;
    new_height = view_height / cell_height;

    /* When the user resizes the frame bigger, expose events are generated,
    ** so that's not a problem, since the expose handler will repaint the
    ** whole viewport. However, when the window resizes smaller, no
    ** expose event is generated. The window does not need to be
    ** redisplayed if the old viewport was smaller than the pixmap.
    ** (The existing image is still valid--no redisplay is necessary.)
    ** The window WILL need to be redisplayed if:
    ** 1) new view size is larger than pixmap (pixmap needs to be centered).
    ** 2) new view size is smaller than pixmap, but the OLD view size was
    **     larger than pixmap.
    */
    if ((int) new_height >= rows) {
        /* The height of the viewport is taller than the pixmap, so set
        ** pix_voffset = 0, so the top origin of the pixmap is shown,
        ** and the pixmap is centered vertically in viewport.
        */
 
        pix_voffset = 0;
        sw_voffset = (view_height - rows * cell_height)/2;
 
        /* Case 1 above */
        do_clear = True;
 
        /* scrollbar is maximum size */
        new_height = rows;
    }
    else {
        /* Pixmap is larger than viewport, so viewport will be completely
        ** redrawn on the redisplay. (So, we don't need to clear window.)
        ** Make sure upper side has origin of a cell (bitmap).
        */
 
        pix_voffset = min (pix_voffset, (rows-new_height) * cell_height);
        sw_voffset = 0; /* no centering is done */

        /* Case 2 above */
        if (oldh > rows * cell_height)
            do_clear = True;
    }
 
    XtVaSetValues(vsb, XmNsliderSize, max (new_height, 1),
                XmNvalue, pix_voffset / cell_height,
                XmNpageIncrement, max (new_height-1, 1),
                NULL);
 
    /* identical to vertical case above */
    if ((int) new_width >= cols) {
        /* The width of the viewport is wider than the pixmap, so set
        ** pix_hoffset = 0, so the left origin of the pixmap is shown,
        ** and the pixmap is centered horizontally in viewport.
        */
 
        pix_hoffset = 0;
        sw_hoffset = (view_width - cols * cell_width)/2;

        /* Case 1 above */
        do_clear = True;
 
        /* scrollbar is maximum size */
        new_width = cols;
    }
    else {
        /* Pixmap is larger than viewport, so viewport will be completely
        ** redrawn on the redisplay. (So, we don't need to clear window.)
        ** Make sure left side has origin of a cell (bitmap).
        */
 
        pix_hoffset = min (pix_hoffset, (cols-new_width)*cell_width);
        sw_hoffset = 0;
 
        /* Case 2 above */
        if (oldw > cols * cell_width)
            do_clear = True;
    }
 
    XtVaSetValues(hsb, XmNsliderSize, max (new_width, 1),
                XmNvalue, pix_hoffset / cell_width,
                XmNpageIncrement, max (new_width-1, 1),
                NULL);

    if (do_clear) {
        /* XClearWindow() doesn't generate an ExposeEvent */
        XClearArea (dpy, cbs->window, 0, 0, 0, 0, True); /* all 0's means the whole window */
    }
#endif //0

}


#endif //_MOTIF_
