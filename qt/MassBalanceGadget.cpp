//-----------------------------------------------------------------------------
// $Id: MassBalanceGadget.cpp,v 1.22 2006/04/23 11:00:10 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the 
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786


#include "MassBalanceGadget.h"
#include "MassBalanceGadget.moc"

#include <qiconview.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qfiledlg.h>
#include <qmessagebox.h>

#include "iniFile.h"
#include "version.h"

//int g_iUnits;

//-----------------------------------------------------------------------------
// Subclass QLineEdit to handle focus lost et al
// 
QLineEditSubClass::QLineEditSubClass(QWidget *parent, const char *name)
  : QLineEdit(parent, name)
{
}

//-----------------------------------------------------------------------------
// Extend focusOutEvent to emit a focusOut() signal
// 
void QLineEditSubClass::focusOutEvent(QFocusEvent* i_pEvent)
{
  QLineEdit::focusOutEvent(i_pEvent);

  if (i_pEvent->reason() != QFocusEvent::ActiveWindow &&
      i_pEvent->reason() != QFocusEvent::Popup &&
      edited()) {
    //Do the checks you want to do when focus is lost
    printf("QLineEditSubClass::focusOutEvent - focus lost\n");
  }
  emit focusOut();
}

//-----------------------------------------------------------------------------
// Extend focusInEvent to emit a focusIn() signal
//
void QLineEditSubClass::focusInEvent(QFocusEvent *e)
{
  QLineEdit::focusInEvent(e);
  emit focusIn();
}

//-----------------------------------------------------------------------------
// Extend keyPressEvent to emit a textFixed() signal
//
// The signal emission is supressed for now.
// 
void QLineEditSubClass::keyPressEvent(QKeyEvent *e)
{
  QLineEdit::keyPressEvent(e);
  if (e->key() == Key_Enter || e->key() == Key_Return) {
    //emit textFixed();
    //emit textFixed(text());
  }
}

static const char* const image0_data[] = {
"32 32 10 1",
". c None",
"# c #000000",
"b c #00c000",
"e c #303030",
"h c #400000",
"d c #585858",
"g c #a0a0a4",
"f c #c0c0c0",
"a c #c0ffc0",
"c c #ffffff",
"...###..........................",
"...#aa##........................",
".###baaa##......................",
".#cd##baaa##....................",
".#cccd##baaa##..##e.............",
".#cccccd##baaa##aaa##...........",
".#cccccccd##baaaaaaaa##.........",
".#cccccccccd##baaaaaaa#.........",
".#cccccfcffgg#bbbbaaaaa#........",
".#ccccccfcfffd#bbbbbbaa#........",
".#cccfcfcfcffgd##bh#bbba#.......",
".#ccccfcfffffffgd#dd##ba#.......",
".#cfcfcfcfffffffffgfgd#bb#......",
".#ccfcfffffffffgfgfgfgd#b#......",
".#cfcfcfffffffffgfgfggd#b#......",
".#fcfffffffffgfgfgggggd#b#......",
".#cfcfffffffffgfgfggggd#b#......",
".#fffffffffgfgfgggggggdhb#......",
".#ffffffffffgfgfggggggd#b#......",
".#ggfffffgfgfgggggggggd#b#......",
".#ddggffffgfgfggggggggd#b#......",
"..##ddggfgfgggggggggggd#b#......",
"....##ddgggfggggggggggd#b#......",
"......##ddggggggggggggd#b#......",
"........##ddggggggggggd#b#......",
"..........##ddggggggggd#b#......",
"............##ddggggggd#b###....",
"..............##ddggggd#b#####..",
"................##ddggd#b######.",
"..................##ddd#b#####..",
"....................##d#b###....",
"......................####......"};

static const char* const image1_data[] = {
"15 15 3 1",
"# c #000000",
"a c #00ff00",
". c #c0c0c0",
"......##.......",
"...a##aa##a....",
"..##aaaaaa##...",
".a#aaaaaaaaa#..",
".#aaaaaaaaaa#..",
".#aaaaaaaaaaa#.",
"#aaaaaaaaaaaa#.",
"#aaaaaaaaaaaaa#",
"#aaaaaaaaaaaa#.",
".#aaaaaaaaaa#..",
".#aaaaaaaaaa#..",
"..#aaaaaaaa#...",
"...##aaaaa#....",
".....##a##.....",
".......#......."};

static const char* const image2_data[] = {
"15 14 3 1",
"# c #000000",
". c #c0c0c0",
"a c #ff0000",
"......##.......",
"...a##aa##a....",
"..##aaaaaa##...",
".a#aaaaaaaaa#..",
".#aaaaaaaaaa#..",
".#aaaaaaaaaaa#.",
"#aaaaaaaaaaaa#.",
"#aaaaaaaaaaaaa#",
"#aaaaaaaaaaaa#.",
".#aaaaaaaaaa#..",
".#aaaaaaaaaa#..",
"..#aaaaaaaa#...",
"...##aaaaa#....",
".....##a##....."};


//-----------------------------------------------------------------------------
//  Construct a MassBalanceGadget which is a child of 'parent', with the
//  name 'name' and widget flags set to 'f'
//
MassBalanceGadget::MassBalanceGadget(QWidget *parent, const char *name, WFlags f)
    :QWidget(parent, name, f)

{
  QPixmap image0((const char**) image0_data);
  QPixmap image1((const char**) image1_data);
  QPixmap image2((const char**) image2_data);
  if (!name)
    setName("MassBalanceGadget");

  resize(638, 440);
 
  MyDialogLayout = new BorderLayout(this);
  MyDialogLayout->setSpacing(6);
  MyDialogLayout->setMargin(11);
 
  Layout20 = new QVBoxLayout;
  Layout20->setSpacing(6);
  Layout20->setMargin(0);
 
  Layout17 = new QGridLayout;
  Layout17->setSpacing(6);
  Layout17->setMargin(0);

  TextLabel1 = new QLabel(this, "TextLabel1");
  QFont font(TextLabel1->font());
  font.setBold(true);
  TextLabel1->setText(tr("ITEM"));
  TextLabel1->setFont(font);
  Layout17->addWidget(TextLabel1, 0, 0);
 
  TextLabel2 = new QLabel(this, "TextLabel2");
  TextLabel2->setText(tr("ARM"));
  TextLabel2->setFont(font);
  Layout17->addWidget(TextLabel2, 0, 1);

 
  TextLabel4 = new QLabel(this, "TextLabel4");
  TextLabel4->setText(tr("MAX"));
  TextLabel4->setFont(font);
  Layout17->addWidget(TextLabel4, 0, 2);
 
 

  TextLabel3 = new QLabel(this, "TextLabel3");
  TextLabel3->setText(tr("WEIGHT"));
  TextLabel3->setFont(font);
  Layout17->addWidget(TextLabel3, 0, 3);

  for (int i = 0; i < MAX_LINE; i++) {
    item[i] = new QLabel(this, "item_2");
    item[i]->setText(tr("item"));
    Layout17->addWidget(item[i], i+1, 0);
    item[i]->setText(tr("item1"));
    item[i]->setText(tr("item2"));

    arm[i] = new QLabel(this, "arm");
    QFont arm_font(arm[i]->font());
    arm_font.setFamily("Courier");
    arm[i]->setFont(arm_font);
    arm[i]->setAlignment(Qt::AlignRight);
    arm[i]->setText(tr("arm"));
    Layout17->addWidget(arm[i], i+1, 1);


    max[i] = new QLabel(this, "max");
    max[i]->setAlignment(Qt::AlignRight);
    max[i]->setText(tr("max"));
    max[i]->setEnabled(false);
    Layout17->addWidget(max[i], i+1, 2);

    weight[i] = new QLineEditSubClass(this, "weight");
    QFont weight_font(weight[i]->font());
    weight_font.setFamily("Courier");
    weight[i]->setFont(weight_font);
    weight[i]->setAlignment(Qt::AlignRight);
    connect(weight[i], SIGNAL(focusOut()), this, SLOT(focusOut()));
    Layout17->addWidget(weight[i], i+1, 3);
  }


  Layout20->addLayout(Layout17);
  Layout22 = new QHBoxLayout;
  Layout22->setSpacing(6);
  Layout22->setMargin(0);
  Layout22->addLayout(Layout20);

  m_envelopePanel = new EnvelopeWidget(this, "EnvelopePanel");
  m_envelopePanel->setMinimumSize(QSize(250, 150));
  Layout22->addWidget(m_envelopePanel);

  MyDialogLayout->add(Layout22, BorderLayout::Center);


  //---------------------------------
  // Create horizontal layout for the AC button
  // and the legend pixmaps and labels
  //
  Layout19 = new QHBoxLayout;
  Layout19->setSpacing(6);
  Layout19->setMargin(0);

  activeAircraftButton = new QPushButton(this, "activeAircraftButton");
  activeAircraftButton->setText(tr("SelectAircraft"));
  Layout19->addWidget(activeAircraftButton);

  QSpacerItem* spacer_2 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  Layout19->addItem(spacer_2);

  TextLabel5 = new QLabel(this, "TextLabel5");
  TextLabel5->setText(tr(""));
  TextLabel5->setPixmap(image1);
  Layout19->addWidget(TextLabel5);

  TextLabel6 = new QLabel(this, "TextLabel6");
  TextLabel6->setText(tr("As Loaded"));
  Layout19->addWidget(TextLabel6);

  TextLabel8 = new QLabel(this, "TextLabel8");
  TextLabel8->setText(tr(""));
  TextLabel8->setPixmap(image2);
  Layout19->addWidget(TextLabel8);

  TextLabel7 = new QLabel(this, "TextLabel7");
  TextLabel7->setText(tr("Zero Fuel"));
  Layout19->addWidget(TextLabel7);

  MyDialogLayout->add(Layout19, BorderLayout::South);

  // signals and slots connections
  connect(activeAircraftButton, SIGNAL(clicked()), this, SLOT(onAircraftButton()));

  // load and work the data
  ReLoad();
  ReCalc();
}

//-----------------------------------------------------------------------------
//  Destroy the object and frees any allocated resources
// 
MassBalanceGadget::~MassBalanceGadget()
{
    // no need to delete child widgets, Qt does it all for us
}

/*     
 *  Main event handler. Reimplemented to handle application
 *  font changes
 */
 /*
bool MassBalanceGadget::event(QEvent* ev)
{
    bool ret = QDialog::event(ev); 
    if (ev->type() == QEvent::ApplicationFontChange) {
    QFont arm_font(arm->font());
    arm_font.setFamily("Courier");
    arm->setFont(arm_font); 
    QFont weight_font(weight->font());
    weight_font.setFamily("Courier");
    weight->setFont(weight_font); 
    }
    return ret;
}

*/


//-----------------------------------------------------------------------------
// Reload the data
// 
void MassBalanceGadget::ReLoad()
{
  char ss[256];
  std::string szData;
  std::string szItem;
  int i = 0;

  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  std::string sLastAd = iniFile.GetValue("Prefs", "LastUsedAd", "");
  //?? if (sLastAd != "") {
  if (!sLastAd.empty()) {
    CIniFile ad_iniFile(sLastAd); ad_iniFile.ReadFile();

    while (true) {
      szItem = QString("%1").arg(i).latin1();
      szData = ad_iniFile.GetValue("LoadingSheet", szItem, "");
      if (szData.length() == 0) {
        break;
      }

      strcpy(ss, szData.c_str());
      char *sp = strtok(ss, ",");
      item[i]->setText(QString(sp).stripWhiteSpace());
      sp = strtok(NULL, ",");
      arm[i]->setText(QString(sp).stripWhiteSpace());
      sp = strtok(NULL, ",");
      max[i]->setText(QString(sp).stripWhiteSpace());
      weight[i]->setText(QString(sp).stripWhiteSpace());

      // Identify the entry with fuel
      QString qs(szData.c_str());
      if (qs.find("FUEL", 0, FALSE) > 0) {
        m_bFuel = i; 
      }

      i++;
    }
    m_iNrPoints = i;

    if (m_iNrPoints > MAX_LINE) {
      QMessageBox::critical(this, SZ_PRGNAME,
                            "Maximum allowable Item stations exceeded");
    }

    //QString acName = QString(ad_iniFile.GetValue("Options", "Name", "").c_str());
    //if (acName != "") {
    std::string acName = ad_iniFile.GetValue("Options", "Name", "");
    if (!acName.empty()) {
      //activeAircraftButton->setText(acName);
        activeAircraftButton->setText(acName.c_str());
    }

    // Clear the rest
    for (i = m_iNrPoints; i < MAX_LINE; i++) {
      item[i]->setText("");
      arm[i]->setText("");
      max[i]->setText("");
      weight[i]->setText("");
    }

    m_iUnits = ad_iniFile.GetValueI("Options", "Units", 1);
    switch (m_iUnits) {
      case 0:
        TextLabel1->setText(tr("ITEM"));
        TextLabel2->setText(tr("ARM mm"));
        TextLabel3->setText(tr("WEIGHT kg"));
        //m_rbMetric.SetCheck(BST_CHECKED);
        //m_rbUS.SetCheck(BST_UNCHECKED);
        break;

      case 1:
        TextLabel1->setText(tr("ITEM"));
        TextLabel2->setText(tr("ARM inch"));
        TextLabel3->setText(tr("WEIGHT lbs"));
        //m_rbUS.SetCheck(BST_CHECKED);
        //m_rbMetric.SetCheck(BST_UNCHECKED);
        break;
    }
  }

  m_envelopePanel->MaxX = -9999;
  m_envelopePanel->MinX =  9999;
  m_envelopePanel->MaxY = -9999;
  m_envelopePanel->MinY =  9999;
  m_envelopePanel->iNrPoints = m_iNrPoints;
}

//-----------------------------------------------------------------------------
// Recalculate the totals
//
void MassBalanceGadget::ReCalc()
{
  double TotWeight = 0;
  static double _TotMoment;
  double TotMoment = 0;
  double TotArm = 0;
  double ZeroFuelMoment = 0;
  double ZeroFuelWeight = 0;
  double ZeroFuelArm = 0;
  int i;

  for (i = 0; i < m_iNrPoints; i++) {
    QString qs = weight[i]->text();//.toDouble();

    TotWeight += weight[i]->text().toDouble();
    TotMoment += weight[i]->text().toDouble() * arm[i]->text().toDouble();

    if (i != m_bFuel) {
      ZeroFuelWeight += weight[i]->text().toDouble();
      ZeroFuelMoment += weight[i]->text().toDouble() * arm[i]->text().toDouble();
    }
  }

  
  if ((TotWeight * ZeroFuelWeight) != 0) {
    TotArm = TotMoment / TotWeight;
    m_envelopePanel->m_Arm    = TotArm;
    m_envelopePanel->m_Weight = TotWeight;

    ZeroFuelArm = ZeroFuelMoment / ZeroFuelWeight;
    m_envelopePanel->m_ZeroFuelArm    = ZeroFuelArm;
    m_envelopePanel->m_ZeroFuelWeight = ZeroFuelWeight;
  }
  else 
    QMessageBox::critical(this, SZ_PRGNAME,
                          "Total weight or zero fuel weight is zero.\n"
                          "Ensure the aircraft has a FUEL entry");


  _TotMoment = TotMoment;
  m_envelopePanel->repaint(true);
}



//-----------------------------------------------------------------------------
// Handle the button press
//
void MassBalanceGadget::onAircraftButton()
{
  printf("MassBalanceGadget::onAircraftButton()\n");

  QString fn = QFileDialog::getOpenFileName("./aircraft", "*.ad"); // , this); -- bug ?
  //QString fn = QFileDialog::getOpenFileName(QString::null, "*.ad", this);

  if (!fn.isEmpty()) {
    printf(QString("You chose to open this file: %1 \n").arg(fn));

    CIniFile iniFile("knav.ini"); iniFile.ReadFile();
    iniFile.SetValue("Prefs", "LastUsedAd", fn.latin1());
    iniFile.WriteFile();
    ReLoad();
    ReCalc();

    emit aircraftChanged();
  }
}

//-----------------------------------------------------------------------------
// Slots
//
void MassBalanceGadget::_textChanged()
{
  printf("MassBalanceGadget::textChanged\n");
}

void MassBalanceGadget::focusOut()
{
  printf("MassBalanceGadget::focusOut()\n"); 
  ReCalc();
}

void MassBalanceGadget::test()
{
  printf("test test test\n");
}


