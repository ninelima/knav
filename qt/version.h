#ifndef __VERSION_H
#define __VERSION_H

//-----------------------------------------------------------------------------
// Revision history
//
// 0.18 Update printing of node rose
// 0.19 Add mass & balance printing
// 0.20 Add route leg printing
// 0.21 Set route leg printing to constant scale (1 deg extent zoom)
// 0.22 Minor changes to ZoomToRect, ZoomToTrack
// 0.23 Add Layout option to knav.ini
// 0.25 Rework ZoomToRect to work for globe view
// 0.26 Reg info to tabdlg and move knav.ini to tabdlg
//      Startup edit to tabdlg
// 0.27 Fixup the szOwner to Owner
// 0.28 Add dirty file check and handling
// 0.29 Rework scale ruler
//      Add database tree expand and collapse
// 0.30 Fix an annoying little bug with file/new and edits
//      Change tree collapse to Ctrl-E
// 0.31 Improve "over the edge" handling with big polygons
//      Remove redundent code in _drawArea/Buffer
// 0.32 Add Disclaimer to startup
// 0.33 Add functionality to edit apt info html files
//      Remove dependancy on amaya
// 0.34 Update to Qt 332
//      Add support for other common graphic file formats
//      gif, jpg etc
// 0.35 Change node rose key to Ctrl-D to resolve confict with File/New
//      Ctrl-N
//      Fix bug in tripkit printing
// 0.36 Fix some control key conflicts
//      Worksheet popup is now Crtl+A and Ctrl+V
//      FlightLog and Planning log, F7 and F8
//      Remove dependancy on system browser
//      Fix a bug in the planning log html generation code
// 0.37 Roll back the built-in browser idea. It causes more problems
//      then it solves :-(
// 0.38 Remove printing of "text" flight and planning logs
//      Fix up the sync problem between the mass & balance
//      worksheet and dialog
// 1.00 Release version - Hoo Haa!
// 1.01 Fix the dc to gc bug in 0.25 deg shader
// 1.02 Various cosmetic changes to AptEditor
//      Produce a 'special' version of qt-mt332.dll that
//      allows a 4 sec timeout for tree keypresses
// 1.03 Minor change to 'locate' off tree, now looks up
//      html info as well
// 1.04 Fix lat/lat bug in InfoHtml::buildHtml
// 1.05 Fix an bug related to undo/redo
// 1.06 Add option to disable background fill when printing
//      tripkits - useful for color printers
// 1.07 Make a start on partially automated ICAO flight plan
//      form production
// 1.08 Fix Right Click Zoomout Bug (crash)
// 1.09 Add production of ICAO flight plan forms
//      Provide better integration with Microsoft IE
// 1.10 Fix icon loading sequence to allow menu icons
// 1.11 Add error handling to AptInfoEditor file save
// 1.12 Add icons to apt nav air and gpstrk entries
// 1.13 Mass & Balance bug with change of aircraft
// 1.14 Add max weight colomn to Mass & Balance
// 1.15 Fix kg/LB in E6B
//      Add m/ft to E6B
//      Fairly major re-work source to allow for Linux
//      build. Improve ANSI complience in the process :-)
//      Linux currently in alpha
// 1.16 Enhanced playback and flight analysis tools
// 1.17 Add TSI
// 1.18 Change swath color to pale gray
// 1.19 Major Re-work
//      Support CrossView and KwikNav from same source
//      base via the conditional #ifdef BUILD_CROSSVIEW
//      All tracking and playback moved from csv based
//      to SQLite database
// 1.20 Fixup Apt button filter to support APT | IFR | SLD
//      TODO: make individually selectable
// 1.21 Create trips under assets for cvii
// 1.22 Add data comms panel for cvii
//      Add about to dataman
//      Add versioning information to .exe's
// 1.23 Fix bugs related to removing the note field from
//      ff_movement
// 1.24 Minor cosmetic change to ASI and Altimeter
// 1.25 Addition of comms and geo fence functionality
//      - NOTE: currently in alpha
//        01  Toolbar eye candy added
//        02  Toolbar colors experimentation
//            Comms window re-worked
//            Click on LED and Search Filter handler stubs
//        03  First cut of the scrolling text and clickable LEDS
//        04  Div 0 checking added to MassBalanceGadget
//        05  Make scrollinfo font scalable
//        06  Work on some open issues
//        07  Add depart arrive logic to dataman
//        08  Add comms window logging
//        09  Minor bugfix with .krt file load
// 1.26 Native support for ESRI shapefiles added
// 1.27 Add eta and dme to mimic panel
// 1.28 Cosmetic changes to mimic panel and code cleanup
// 1.29 Finish off the <TBD>'s in Comms panel
// 1.30 Drop SQLite totally. It is simply too slow and unreliable
//      Add the ff_message table
//      Add text message support to DataMan
//      Add support for country names
//      Fix a bug with polygon rendering on globe view
//      Put a ugly workaround in for the Iridium Sectrac ,| issue
// 1.31 Fix Dataman bug - Performance
//      Add initial "status" support to Comms panel
//      Add buffered drawing support and option
//      Enable registration code checking
//      Add online version checking functionality
// 1.32 Rework POP engine with libpopc, now much faster :-)
//      and supports sectrac.co.za pop better
//      Add version info to validator
// 1.33 Add a more graceful error handling to POP3
//      incorrect settings
//      Add application exit forcing
//      Fix bug with auto asset adding
// 1.34 Allow Dataman settings change to take
//      immediate effect
// 1.35 Add Active Tracking worksheet
//        01  Small bugfix when switching from trail back to
//            "track all" sometimes did not work correctly
//        02  Plug a memory leak
//        03  Rework track-while-scan to be a lot less CPU intensive
//        04  Plug another memory leak
//        05  And another in DataMan this time
// 1.36 Make the SixPack optional for tracking and playback
//      Auto save/restore window and scrollbar sizes
//        01 Fix Asset update bug
//        02 Minor code cleanup
//        03 Beta Version of dataman that only support new DS tracker device
//        04 Improved flashing in trackall
// 1.37 Add onscreen data to targets
//        01 Make little tracking tail persistant
//        02 Update tree entry only after "edit asset" operation
//        03 Allow both buggy (:) and correct (,) RMC strings in dataman
//        04 Fix a minor bug where LCD data is incorrect for "follow me"
//        05 Add 1 deg buffer zone for shp drawing (less discriminating)
//

#define VER_MAJOR 1
#define VER_MINOR 37
#define VER_BUILD 5
#define SZ_VERSION "Build 01.37.05"

#ifdef BUILD_CROSSVIEW
  #define SZ_PRGNAME "CrossView II - Tracker"
  #define SZ_APPNAME SZ_PRGNAME
#else
  #define SZ_PRGNAME "Kwik Navigator"
  #define SZ_APPNAME SZ_PRGNAME
#endif

#endif //__VERSION_H
