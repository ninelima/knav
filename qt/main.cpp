//-----------------------------------------------------------------------------
// $Id: main.cpp,v 1.41 2007/03/19 01:50:37 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786


// Include the windows headers on the windows
// platform so we can do some windows stuff.
#ifdef WIN32
  #include <windows.h>
  #include <fcntl.h>
  #include <io.h>
#endif //WIN32

#include <qapplication.h>
#include "version.h"
#include "MainWindow.h"

// the styles
#include <qmotifstyle.h>
#include <qcdestyle.h>
#include <qwindowsstyle.h>
#include <qplatinumstyle.h>
#include <qsgistyle.h>
#include <qjpegio.h>
#include <qinputdialog.h>
#include <qmessagebox.h>

#include <qsqldatabase.h>
#include <qdatatable.h>
#include <qsqlselectcursor.h>


#include <stdlib.h>
#include <stdio.h>
#include "ustring.h"
#include "iniFile.h"
#include "misc.h"
#include "dbconf.h"
#include "Validator.h"


QSqlSelectCursor *g_AllMvm = NULL;    //("ff_movement");
QSqlSelectCursor *g_AllDevice = NULL; //("ff_device");
QSqlSelectCursor *g_AllAsset = NULL;  //("ff_asset");

QSqlSelectCursor *g_Mvm = NULL;       //("ff_movement");
QSqlSelectCursor *g_Device = NULL;    //("ff_device");
QSqlSelectCursor *g_Asset = NULL;    //("ff_asset");



//-----------------------------------------------------------------------------
// int main(int argc, char **argv)
//
int __main__(int argc, char *argv[])
{
  char szFileName[256];
  int iStyle = -1;

  //
  //  start_console();
  //

  printf("\n\n"
         "Qt Kwik Naviation Planner\n"
         SZ_VERSION "\n\n"
         "(c) Player One 2003\n"
         "All rights reserved"
         "\n\n");
 
  //
  // Read the ini file for some last used settings.
  // Any of these can be overridden by paramaters
  // specified on the command line
  //
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iStyle = iniFile.GetValueI("Prefs", "LookAndFeel", -1);
  //iniFile.GetValueS("Prefs", "LastUsedKrtFile", "none.krt");

  szFileName[0] = '\0';

  //
  // Parse the command line for any useful
  // tidbits
  //
  for (int i = 0; i < argc; i++) {
    puts(argv[i]);

    if (strcmp(argv[i], "-f") == 0)
      strcpy(szFileName, argv[i+1]);

    if (strcmp(argv[i], "-s") == 0)
      iStyle = atoi(argv[i+1]);

    if ((strcmp(argv[i], "-h") == 0) ||
        (strcmp(argv[i], "-?") == 0) ||
        (strcmp(argv[i], "--h") == 0) ||
        (strcmp(argv[i], "--?") == 0)
       ) {

      printf("\n\nKwik Navigator Qt - (c) 2000 Player One\n\n");
      printf("usage:\n\n");
      printf("qtnav.exe <-option [arg]>\n\n");
      printf("with option:\n");
      printf("  -f filename\n");
      printf("  -s style\n");
      printf("     with style:\n");
      printf("        0 - MotifStyle\n");
      printf("        1 - CDEStyle\n");
      printf("        2 - WindowsStyle\n");
      printf("        3 - PlatinumStyle\n");
      printf("        4 - SGIStyle\n");
      printf("  -h help\n");
      printf("  \n");
      printf("example:\n");
      printf("  qtnav.exe -f navex.krt -s 3\n");
      printf("  \n");
      printf("  Open navex.krt on startup and use the Platinum style user interface.");
      char c = getchar();
      return 0;
    }
  }



  //
  // Create our one and only application object
  //
  QApplication app(argc, argv);

#ifdef BUILD_CROSSVIEW
  Validator validator;
#endif // BUILD_CROSSVIEW
   
  //
  // We need to create an instance of the WebBrowser IExplore activeX
  // before we do anything else. This is a kludge to stop the GP in XP
  // that occurs if a FileOpen Dialog from M&B is followed by a webbrowse .
  // Exactly why this works is somewhat of a mystery.
  //
  html_browse(NULL);

  //
  // Open the database
  //
  QSqlDatabase *db = QSqlDatabase::addDatabase(DRIVER);
  db->setDatabaseName(DATABASE);

  db->setUserName(USER);
  db->setPassword(PASSWORD);
  db->setHostName(HOST);

  if (!db->open()) {
		QString err = "Debug : \n" + db->lastError().driverText() +
			",\n " + db->lastError().databaseText() +
			",\n " + QString("%1").arg(db->lastError().number()) +
			",\n " + db->lastError().text();

		qDebug(err);
    QMessageBox::critical(NULL, "Error", err);
    return -1;
  }
                                          
  //g_AllMvm    = new QSqlSelectCursor();  g_AllMvm->exec("select * from ff_movement");
  g_AllMvm    = new QSqlSelectCursor();  g_AllMvm->exec("select top 1000 * from ff_movement order by dtg desc");

  g_AllDevice = new QSqlSelectCursor();  g_AllDevice->exec("select * from ff_device");
  g_AllAsset  = new QSqlSelectCursor();  g_AllAsset->exec("select * from ff_asset");

  g_Mvm    = new QSqlSelectCursor();
  g_Device = new QSqlSelectCursor();
  g_Asset  = new QSqlSelectCursor();

  ApplicationWindow *mw = new ApplicationWindow();

  #ifndef _DEBUG
  mw->setCaption(SZ_APPNAME /*" - <none>"*/);
  #else
  mw->setCaption(SZ_APPNAME " - <debug>");
  #endif

  //
  // If a filename is supplied via -f assume
  // it is valid and attempt to load it
  //
  if (szFileName[0] != '\0')
    mw->fileLoad(szFileName);
 
  if (iStyle > -1) {
    switch (iStyle) {
      case 0:
        app.setStyle(new QMotifStyle);
        break;
      case 1:
        app.setStyle(new QCDEStyle);
        break;
      case 2:
        app.setStyle(new QWindowsStyle);
        break;
      case 3:
        app.setStyle(new QPlatinumStyle);
        break;
      case 4:
        app.setStyle(new QSGIStyle);
        break;
    }
  }

  if (!check_disclaimer()) {
    return -1;
  }

  ///*
  #ifdef BUILD_CROSSVIEW
  if (!check_serial_num()) {
    set_demo_version();
  }
  #endif // BUILD_CROSSVIEW
  //*/

  mw->show();
  app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
	
	return app.exec();
	
	/*int rv =  app.exec();
  delete g_AllMvm;   
  delete g_AllDevice;
  delete g_AllAsset; 
  delete g_Mvm;    
  delete g_Device; 
  delete g_Asset;
	return rv;*/
}


#ifdef WINDOWS_CONSOLE
  #pragma message ("#\n# WINDOWS_CONSOLE defined\n#\n")
  #pragma message ("#\n# main - console enabled\n#\n")

  int main(int argc, char *argv[])
  {
    // Pass through all the command line options
    return __main__(argc, argv);
  }
#else
  #pragma message ("#\n# WINDOWS_CONSOLE not defined\n#\n")
  #pragma message ("#\n# WinMain - console disabled\n#\n")

  int WINAPI WinMain(
    HINSTANCE hInstance,      // handle to current instance
    HINSTANCE hPrevInstance,  // handle to previous instance
    char *lpCmdLine,          // command line
    int nCmdShow              // show state
  )
  {
    // We do not support any command line options
    // under windows
    return __main__(0, NULL);
  }
#endif

