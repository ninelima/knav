//-----------------------------------------------------------------------------
// $Id: GenMapWnd.cpp,v 1.54 2007/06/23 06:55:09 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>       

#include <string>
#include <fstream>

#ifndef _MOTIF_
  #include <qimage.h>
  #include <qfile.h>
#endif

#include "utypes.h"
#include "math.h"
#include "umath.h"
#include "utrig.h"

#include "GraphicPainter.h"
#include "GenMapWnd.h"

#include "math.h"
#include "ustring.h"
#include "ucolor.h"
#include "umath.h"
#include "ufile.h"
#include "uvalues.h"
#include "iniFile.h"
#include "MagField.h"

#include "GreatCircle.h"
#include "misc.h"               

#ifdef _MOTIF_
  extern Widget  drawing_a;
#endif

#define IDC_CURSOR2 131
#define IDC_CURSOR3 132

extern bool bForecastBusy;
extern bool bMapActive;

#define FILE_SIZE 2000000
#ifdef WIN32
  #define MAX_GDI_EXTENT 30000
#else
  #define MAX_GDI_EXTENT 3000
#endif //WIN32

//#define MAX_POLY 4000
//#define MAX_POLY 16000
#define MAX_POLY 128000
//#define BUFSIZE 85000*2
#define BUFNUM  24


// Macros
#define root2    1.414213562
#define pi       3.1415946536
#define degToRad     (pi / 180.0)
#define degToMeters  (10001750.0 / 90.0)
#define earthRadius  (180 * degToMeters / pi)


//-----------------------------------------------------------------------------
//  GenMapPanel Constructor
//
CGenMapWnd::CGenMapWnd()
{
  Init();
}

void CGenMapWnd::Init()
{
  printf("CGenMapWnd::Init\n");
  ci = 0;

  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  // X globals
  // Aspect ration for X/Y
  convY = 1.0;
  convX = 1.0;
  // end X globals

  //char szPath[256];
  std::string ssPath;

  //
  // The data paths
  //
  ssPath = iniFile.GetValue("Initze", "szVectorDataPath", "./vector/");
  strcpy(m_sBinPath, ssPath.c_str());
  ssPath = iniFile.GetValue("Initze", "szQuadDataPath", "./QuadData/");
  strcpy(m_sQuadPath, ssPath.c_str());
  ssPath = iniFile.GetValue("Initze", "szReliefDataPath", "./relief/");
  strcpy(m_sShadePath, ssPath.c_str());

  // Read threshold settings
  span1000 = iniFile.GetValueI("SpanThreshold", "1000",  1);
  span0100 = iniFile.GetValueI("SpanThreshold", "0100",  4);
  span0025 = iniFile.GetValueI("SpanThreshold", "0025",  8);

  flatWorld = iniFile.GetValueI("Spatial", "flatWorld", 0);
  nZoomMode = iniFile.GetValueI("Spatial", "ZoomMode", 1);
  bDisplayDMS = iniFile.GetValueI("Spatial", "bDisplayDMS", 1);
  bShadeActive = iniFile.GetValueI("Spatial", "ShadeActive", 0);
  bRoseActive  = iniFile.GetValueI("Spatial", "RoseActive", 0);
  // depreciated - iBackGround  = iniFile.GetValueI("Spatial", "BackGround", 0);

  // Read zoom params
  top    = iniFile.GetValueF("ZoomState", "top", 90.0);
  bottom = iniFile.GetValueF("ZoomState", "bottom", -90.0);
  left   = iniFile.GetValueF("ZoomState", "left", -180.0);
  right  = iniFile.GetValueF("ZoomState", "right", 180);
  convX  = iniFile.GetValueF("ZoomState", "convX", 1.0);
  convY  = iniFile.GetValueF("ZoomState", "convY", 1.0);
  mag    = iniFile.GetValueI("ZoomState", "mag", 1);
  mag2   = iniFile.GetValueI("ZoomState", "mag2", 1);

  // Read pan params
  centerLon  = iniFile.GetValueF("ZoomState", "centerLon", 0.0);
  centerLat  = iniFile.GetValueF("ZoomState", "centerLat", 0.0);

  bZoomActive = FALSE;
  bPolygonActive = TRUE;

  OffsetPt.x = 0; OffsetPt.y = 0;
  M = 6;

  m_bLeftButtonDown = FALSE;
  nTargetGate = 80;
  bTextActive = FALSE;
 
  memset(nrpts,     0, sizeof(nrpts));
  memset(nrpbuftxt, 0, sizeof(nrpbuftxt));
 
  HiLat = 0.0;
  HiLong = 0.0;
  bAnimate = FALSE;
  //bChange = TRUE;
  Mready = FALSE;
  bAbort = FALSE;
  screenWidth =  1600;
  screenHeight = 600;

#ifndef _MOTIF_
  m_sStatusInfo = QString("0 0");
#endif
  bCrossHairActive = false;
  LoadData();

  // Temp
  // Utility(NULL);
  // ExitProcess(0);
  // Temp
}

CGenMapWnd::~CGenMapWnd()
{
  printf("CGenMapWnd::~CGenMapWnd\n");

  CIniFile iniFile( "knav.ini");
  iniFile.ReadFile();
 
  // Save the view params
  iniFile.SetValueI("Spatial", "flatWorld", flatWorld);
  iniFile.SetValueI("Spatial", "ShadeActive", bShadeActive);
  iniFile.SetValueI("Spatial", "RoseActive", bRoseActive);
  // depreciated -   iniFile.SetValueI("Spatial", "BackGround", iBackGround);

  // Save the Zoom params
  iniFile.SetValueF("ZoomState", "top", top);
  iniFile.SetValueF("ZoomState", "bottom", bottom);
  iniFile.SetValueF("ZoomState", "left", left);
  iniFile.SetValueF("ZoomState", "right", right);
  iniFile.SetValueF("ZoomState", "convX", convX);
  iniFile.SetValueF("ZoomState", "convY", convY);
  iniFile.SetValueI("ZoomState", "mag", mag);
  iniFile.SetValueI("ZoomState", "mag2", mag2);

  // Save the Pan params
  iniFile.SetValueF("ZoomState", "centerLon", centerLon);
  iniFile.SetValueF("ZoomState", "centerLat", centerLat);

  iniFile.WriteFile();

  UnLoadData();
}

//-----------------------------------------------------------------------------
// Create and load the static datasets as defined in the .ini file
// Note:
//  Dataset 0 is general purpose and used for grids
//
void CGenMapWnd::LoadData()
{
  printf("CGenMapWnd::LoadData\n");

  CIniFile iniFile( "./knav.ini");
  iniFile.ReadFile();
  QFile file;
  uint32_t len;
  int n;                              
  char szFileName[256];
  char szSection[256];
  int32_t nPenStyle;
  int32_t nWidth;
  int32_t crColor;
  int32_t crFill;
  int32_t bPolygon;
  int32_t iMagTH = 1;
  int32_t iMagTT = 255;
  int32_t iActive = true;

  // Our general purpose buffer
  // We need 720 + 2 + 2 minimum for grid
  buff[0]  = (float64_t *) malloc(MAX_POLY * sizeof(float64_t)); 

  //---------------------------------
  // Load the binary vector data sets
  //
  // Note: 0 is for special use
  //
  for (n = 1; n < NR_BUF; n++) {
    sprintf(szSection, "StaticVector_%d", n);
    iMagTH = iniFile.GetValueI(szSection, "magTH",    0);
    iMagTT = iniFile.GetValueI(szSection, "magTT",  255);
    iActive = iniFile.GetValueI(szSection, "Active",  0);
    if (!iActive) {
      buff[n]  = (float64_t *) malloc(1);  // placeholder
      continue;
    }

    nPenStyle = iniFile.GetValueI(szSection, "nPenStyle", 0);
    nWidth    = iniFile.GetValueI(szSection, "nWidth", 0);
    crColor   = iniFile.GetValueI(szSection, "crColor", 0);
    crFill    = iniFile.GetValueI(szSection, "crFill", 0);
    bPolygon  = iniFile.GetValueI(szSection, "IsPolygon", 0);
 
    std::string sPath;
    sPath = iniFile.GetValue(szSection, "FileName", " ");
    strcpy(szFileName, sPath.c_str());

    strcpy(descriptor[n].FileName, szFileName);
    descriptor[n].MagTH = iMagTH;
    descriptor[n].MagTT = iMagTH;
    descriptor[n].nPenStyle = nPenStyle;
    descriptor[n].nWidth    = nWidth;
    descriptor[n].crColor   = crColor;
    descriptor[n].crFill    = crFill;
    descriptor[n].bPolygon  = bPolygon;

    char s[256];
    strcpy(s, m_sBinPath); strcat(s, szFileName);
    printf("n=%d, section=%s\n", n, szSection);

    file.setName(s);
    if (!file.open(IO_ReadOnly)) {
      printf("CGenMapWnd::LoadData - file bad - %s\n", s);
      buff[n]  = (float64_t *) malloc(1);  // placeholder
      nrpts[n] = 0;
      file.close();
      continue;
    }

    len = file.size();
    buff[n] = (float64_t *) malloc(len);
    file.at(0);
    file.readBlock((char *) buff[n], len);

    nrpts[n] = len / (1*sizeof(float64_t));
    file.close();

    printf("CGenMapWnd::LoadData - %s [%d bytes]\n", s, len);
  }
}

//---------------------------------
// UnLoad the binary vector data sets
//
void CGenMapWnd::UnLoadData()
{
  for (int n = 0; n < NR_BUF; n++) {
    free(buff[n]);
    nrpts[n] = 0;
  }
}


//-----------------------------------------------------------------------------
// Return the magnetic variation for a given lat/long
//
float64_t CGenMapWnd::getMagVar(float64_t lat, float64_t lon)
{
  int ivar = (int) rad_to_deg(SGMagVar(deg_to_rad(lat),
                                       deg_to_rad(lon),
                                       0,  //height in km
                                       yymmdd_to_julian_days(02,01,01),
                                       7)); //model
  return -ivar;
}

void CGenMapWnd::LocationBar()
{
  // update status bar with location information
  char  buffer[120];
  char  dirx, diry;
  float64_t cx, cy;
  if (flatWorld) {
    cx = (left+right)/2.0;
    cy = (top+bottom)/2.0;
  } else {
    cx = centerLon;
    cy = centerLat;
  }
  if (cx < 0.)
    dirx = 'W';
  else
    dirx = 'E';
  if (cy < 0.)
    diry = 'S';
  else
    diry = 'N';
  float64_t cxx = fabs(cx);
  int cxd = (int) (cxx);
  int cxm = (int) ((cxx-cxd) * 60);
  int cxs = (int) (((cxx-cxd)*60-cxm)*60);
  float64_t cyy = fabs(cy);
  int cyd = (int) (cyy);
  int cym = (int) ((cyy-cyd) * 60);
  int cys = (int) (((cyy-cyd)*60-cym)*60);
  int chars;
  if (flatWorld)
    chars = sprintf(buffer, "center:% 8.6f, % 8.6f (%d %d' %d\"%c, %d %d' %d\"%c),  size: %d by %d nm", cy, cx, cyd, cym, cys, diry, cxd, cxm, cxs, dirx,
      (int)((right-left)*degToMeters/1852), (int)((top-bottom)*degToMeters/1852));
  else
    chars = sprintf(buffer, "center:% 8.6f, % 8.6f (%d %d' %d\"%c, %d %d' %d\"%c)", cy, cx, cyd, cym, cys, diry, cxd, cxm, cxs, dirx);

  /*((CMainFrame *) AfxGetMainWnd())->SetStatusBar(buffer);*/  //!TODO!
 
  //bChange = TRUE;
}


//-----------------------------------------------------------------------------
// Spherical / Cartesian conversions
//

// update viewpoint rotation matrix
void CGenMapWnd::Mcalc()
{
  M11 =  cos(-centerLon * degToRad);
  M12 =  0;
  M13 =  sin(-centerLon * degToRad);
  M22 =  cos(-centerLat * degToRad);
  M32 = -sin(-centerLat * degToRad);
  M21 =  M32*M13;
  M23 = -M11*M32;
  M31 = -M22*M13;
  M33 =  M11*M22;
  Mready = TRUE;
}

// spherical to cartesian conversion (y=top, x=right, z=out-of-screen)
void CGenMapWnd::StoC(float64_t &x, float64_t &y, float64_t &z, float64_t lat, float64_t lon, float64_t r)
{
  z = r * cos(lon * degToRad) * sin((90 - lat) * degToRad);
  x = r * sin(lon * degToRad) * sin((90 - lat) * degToRad);
  y = r * cos((90 - lat) * degToRad);
}

// spherical to cartesian conversion for normalized radius (y=top, x=right, z=out-of-screen)
void CGenMapWnd::StoCe(float64_t &x, float64_t &y, float64_t &z, float64_t lat, float64_t lon)
{
  z = cos(lon * degToRad) * sin((90 - lat) * degToRad);
  x = sin(lon * degToRad) * sin((90 - lat) * degToRad);
  y = cos((90 - lat) * degToRad);
}

// cartesian to spherical conversion (y=top, x=right, z=out-of-screen)
void CGenMapWnd::CtoS(float64_t x, float64_t y, float64_t z, float64_t &lat, float64_t &lon, float64_t &r)
{
  lon = atan2(x, z) / degToRad;
  r = sqrt(x*x + y*y + z*z);
  lat = 90 - acos(y/r) / degToRad;
}

// cartesian to spherical conversion for normalized radius  (y=top, x=right, z=out-of-screen)
void CGenMapWnd::CtoSe(float64_t x, float64_t y, float64_t z, float64_t &lat, float64_t &lon)
{
  lon = atan2(x, z) / degToRad;
  lat = 90 - acos(y) / degToRad;
}


//-----------------------------------------------------------------------------
// Lat/Long X/Y conversions
//

//protected boolean LtoC(float64_t rlong, float64_t rlat) {
int CGenMapWnd::LtoC(float64_t rlon, float64_t rlat, int &xc, int &yc)
{
  if (flatWorld) {
    xc = (int) ((rlon - left)/(right-left)*screenWidth);
    yc = (int) ((top - rlat)/(top-bottom)*screenHeight);

    if (xc < -MAX_GDI_EXTENT) xc = -MAX_GDI_EXTENT;
    if (yc < -MAX_GDI_EXTENT) yc = -MAX_GDI_EXTENT;
    if (xc > MAX_GDI_EXTENT) xc = MAX_GDI_EXTENT;
    if (yc > MAX_GDI_EXTENT) yc = MAX_GDI_EXTENT;

    if ((rlon < left) || (rlon > right) || (rlat < bottom) || (rlat > top)) {
      return false;
    }
 
    return true;
  }
  else {
    if (!Mready)
      Mcalc();
    float64_t xo, yo, zo, xs, ys, zs;
    //StoCe(xo, yo, zo, yl, xl);      // convert to normalized cartesian
    StoCe(xo, yo, zo, rlat, rlon);   // convert to normalized cartesian
    //xo = m_rx; yo = m_ry; zo = m_rz;

    zs = xo*M31 + yo*M32 + zo*M33;    // rotate to viewpoint
    xs = xo*M11 + zo*M13;             // (M12 skipped, should always be 0)
    xs *= convX * screenWidth/2;
    xc = screenWidth/2  + (int) (xs);
    //--> xc = convX * screenWidth/2  + (int) (xs);
    ys = xo*M21 + yo*M22 + zo*M23;
    ys *= convY * screenHeight/2;
    yc = screenHeight/2 - (int) (ys);
    //--> yc = convY * screenHeight/2 - (int) (ys);
    if (zs < 0)
      return FALSE;                 // dark side of the earth

    if (xc < -MAX_GDI_EXTENT) xc = -MAX_GDI_EXTENT;
    if (yc < -MAX_GDI_EXTENT) yc = -MAX_GDI_EXTENT;
    if (xc > MAX_GDI_EXTENT) xc = MAX_GDI_EXTENT;
    if (yc > MAX_GDI_EXTENT) yc = MAX_GDI_EXTENT;

    return TRUE;
  }
}

int CGenMapWnd::CtoL(int xc, int yc, float64_t &xl, float64_t &yl)
{
  if (flatWorld) {
    xl = left+((float64_t) xc)/screenWidth*(right-left);
    yl = top-((float64_t) yc)/screenHeight*(top - bottom);
      return true;
  }
  else {
    if (!Mready)
      Mcalc();
    float64_t xo,yo,zo,xs,ys,zs,t;
    float64_t nx = xc - screenWidth/2;    // center screen is 0,0
    float64_t ny = screenHeight/2 - yc;
    xs = nx / (convX * screenWidth/2);
    ys = ny / (convY * screenHeight/2);
    t = xs*xs+ys*ys;
    if (t > 1)                         // invalid point in orbit
        return false;
    zs = sqrt(1 - t);                  // positive root selects this side of earth
    xo = xs*M11 + ys*M21 + zs*M31;
    yo =          ys*M22 + zs*M32;     // (M12 skipped, should always be 0)
    zo = xs*M13 + ys*M23 + zs*M33;
    CtoSe(xo,yo,zo,yl,xl);             // convert from normalized cartesian
    fixup(xl, yl);                     // normalize angles
      return true;
  }
}

// fixup wrap-around
void CGenMapWnd::fixup(float64_t &x, float64_t &y)
{
  if (y > 90) {
    y -= (y - 90) * 2;
    x += 180;
  } else if (y < -90) {
    y += (y + 90) * 2;
    x += 180;
  }
  while (x > 180)
    x -= 360;
  while (x < -180)
    x += 360;
}


//-----------------------------------------------------------------------------
// Draw an line
//
//  Centered at lat, lon with radius r (in nm).
//  Clockwise starting at a1 and terminating at a2 (angles are measured as per
//  navigation ie N = top = 0 = 360 deg
//
/*void CGenMapWnd::drawLine(GraphicPainter *gc, float64_t lat1, float64_t lon1, float64_t lat2, float64_t lon2)
{
  int a, phi, i;

  i = 0;
  buff[0][i] = 9999;
  i++;
  buff[0][i] = 9999;
  i++;

  buff[0][i] =  lon1;
  i++;
  buff[0][i] = -lat1;
  i++;
  buff[0][i] =  lon2;
  i++;
  buff[0][i] = -lat2;
  i++;

  nrpts[0] = i;
  drawBuffer(gc, 0);
}*/

//void GreatCircle::getIntermediatePoint(float64_t *plat, float64_t *plon, float64_t lat1, float64_t lon1, float64_t lat2, float64_t lon2)

void CGenMapWnd::drawLine(GraphicPainter *gc, float64_t lat1, float64_t lon1, float64_t lat2, float64_t lon2)
{
  int i;

  if (lon1 > lon2) {
    float64_t t;
    t = lat1; lat1 = lat2; lat2 = t;
    t = lon1; lon1 = lon2; lon2 = t;
  }

  i = 0;
  buff[0][i] = 9999;
  i++;
  buff[0][i] = 9999;
  i++;

  buff[0][i] =  lon1;
  i++;
  buff[0][i] = -lat1;
  i++;

  float64_t Lat, Lon;
  float64_t step = fabs(lon2 - lon1) / 50;  // / 256 arbitrarily chosen value

  // If step == 0 is the special case where the line
  // is directly N/S
  if (step != 0.0) {
    for (Lon = lon1; Lon <= lon2; Lon += step) {
      GreatCircle::getIntermediatePoint(&Lat, &Lon, lat1, lon1, lat2, lon2);
      buff[0][i] =  Lon;
      i++;
      buff[0][i] = -Lat;
      i++;
    }
  }
 
  buff[0][i] =  lon2;
  i++;
  buff[0][i] = -lat2;
  i++;

  nrpts[0] = i;
  drawBuffer(gc, 0);
}


//-----------------------------------------------------------------------------
// Draw an arc
//
//  Centered at lat, lon with radius r (in nm).
//  Clockwise starting at a1 and terminating at a2 (angles are measured as per
//  navigation ie N = top = 0 = 360 deg
//
// Note:
//  This version simply draws an arc independant of the projection (or using
//  a xy projection if you wish). It is therefore not a accurate depiction
//  an arc on the surface of the earth, but it appears to be the correct
//  one to use for map making.
//
/*void CGenMapWnd::drawArc(GraphicPainter *gc, float64_t lat, float64_t lon, float64_t r, int a1, int a2)
{
  float64_t Lat=0, Lon=0;
  int a, phi, i;

  while (a1 > a2)
    a2 += 360;

  i = 0;
  buff[0][i] = 9999;
  i++;
  buff[0][i] = 9999;
  i++;
  for (a = a1; a <= a2; a++) {
    phi = 90 - a;
    Lon = lon +  r * icos(phi) / 60;
    Lat = lat +  r * isin(phi) / 60;

    buff[0][i] =  Lon;
    i++;
    buff[0][i] = -Lat;
    i++;

    nrpts[0] = i;
  }
  drawBuffer(gc, 0);
}
*/

//-----------------------------------------------------------------------------
// Draw an arc
//
//  Centered at lat, lon with radius r (in nm).
//  Clockwise starting at a1 and terminating at a2 (angles are measured as per
//  navigation ie N = top = 0 = 360 deg
//
void CGenMapWnd::drawArc(GraphicPainter *gc, float64_t lat, float64_t lon, float64_t r, int a1, int a2)
{
  float64_t Lat=0, Lon=0;
  int a, phi, i;

  while (a1 > a2)
    a2 += 360;

  i = 0;
  buff[0][i] = 9999;
  i++;
  buff[0][i] = 9999;
  i++;

  // convert from nm and use the "magic" number 1.05
  // "magic" number to work around a error, rounding perhaps?
  float64_t cor = 1.05/3600.0;
  
  for (a = a1; a <= a2; a++) {
    phi = a;
    GreatCircle::getRadialDmePoint(&Lat, &Lon, lat, lon, r*cor, -phi);
    buff[0][i] =  Lon;
    i++;
    buff[0][i] = -Lat;
    i++;
    nrpts[0] = i;
  }
  drawBuffer(gc, 0);
  float64_t dme = GreatCircle::getDistance(Lat, Lon, lat, lon);
}




//-----------------------------------------------------------------------------
// Draw the Lat and long grid lines
//
void CGenMapWnd::drawGrid(GraphicPainter *gc)
{
  int x, y;
  int ox, oy;
  int ex, ey;
  float64_t Lat=0, Lon=0;
  float64_t Incr, Sepr;

#ifndef _MOTIF_
    gc->setPen(Qt::gray);
#endif

  int FontHeight = 10;
  if (FontHeight <= MINFONTSIZE) FontHeight = 1;
  if (FontHeight > MAXFONTSIZE ) FontHeight = 12;

  char szfmtLat[24];
  char szfmtLon[24];

  Incr = 0.250;
  Incr = 30.0;
  if (mag > 3) Incr = 15.0;
  Sepr = Incr * 8;

  if (false) {
    LtoC(-180, 90, ox, oy);
    LtoC(180, -90, ex, ey);

    if (ox < 0) ox = 0;
    if (ex > screenWidth) ex = screenWidth;
    if (oy < 0 ) oy = 0;
    if (ey > screenHeight) ey = screenHeight;


    for (Lat = -90; Lat <= 90; Lat = Lat + Incr) {
      LtoC(Lon, -Lat, x, y);
      sprintf(szfmtLat, "%02.0f%c", fabs(Lat), -Lat > 0 ? 'N' : 'S');
      gc->drawString(szfmtLat, 5, y - 5);

      gc->moveTo(ox, y);
      gc->lineTo(ex, y);
    }

    for (Lon = -180; Lon <= 180; Lon = Lon + Incr) {
      LtoC(Lon, -Lat, x, y);
      sprintf(szfmtLon, "%03.0f%c", fabs(Lon), Lon > 0 ? 'E' : 'W');
      gc->drawString(szfmtLon, x + 5, MAXFONTSIZE);

      gc->moveTo(x, oy);
      gc->lineTo(x, ey);
    }
  }
  else {
    LtoC(-180, -90, ox, oy);
    LtoC(180, 90, ex, ey);

    int i;
    for (Lat = -90; Lat < 90; Lat = Lat + Incr) {
      i = 0;
      buff[0][i] = 9999;
      i++;
      buff[0][i] = 9999;
      i++;
      for (Lon = -180; Lon <= 180; Lon = Lon + 1.0) {
        buff[0][i] = Lon;
        i++;
        buff[0][i] = Lat;
        i++;
        nrpts[0] = i;
      }
      drawBuffer(gc, 0);
    }
    for (Lon = -180; Lon < 180; Lon = Lon + Incr) {
      i = 0;
      buff[0][i] = 9999;
      i++;
      buff[0][i] = 9999;
      i++;
      for (Lat = -90; Lat <= 90; Lat = Lat + 1.0) {
        buff[0][i] = Lon;
        i++;
        buff[0][i] = Lat;
        i++;
        nrpts[0] = i;
      }
      drawBuffer(gc, 0);
    }
  }
  gc->setStroke(1/*basicStroke*/);
}


//-----------------------------------------------------------------------------
// Draw a scale ruler legend
//
void CGenMapWnd::drawScaleRuler(GraphicPainter *gc)
{
  // Local vars
  int x, y;
  int x1, y1;
  int x2, y2;
  QString info;

  /*
  LtoC(left, bottom, x, y);
  x1 = x; y1 = y;
 
  if (mag < 5) {
    LtoC(left, bottom - 10, x, y);
    info = "600nm";
  }
  else if (mag < 10) {
    LtoC(left, bottom - 1, x, y);
    info = "60nm";
  }
  else if (mag < 15) {
    LtoC(left, bottom - 0.1, x, y);
    info = "6nm";
  }
  else if (mag < 20) {
    LtoC(left, bottom - 0.01, x, y);
    info = "0.6nm";
  }
  else {
    info = "overzoom";
    x = x1; y = y1;
  }

  x2 = x; y2 = y;
  int nm = abs(y2 - y1);
  */


  float64_t mult = 10000;
  float64_t nm = 9999;

  while (nm > screenWidth * 0.5) {
    mult /= 10;
    if (mult < 0.001) {
      info = "overzoom";
      nm = 0;
      break;
    }

    LtoC(left, bottom, x, y);
    x1 = x; y1 = y;

    LtoC(left, bottom - mult/60, x, y);
    x2 = x; y2 = y;
    nm = abs(y2 - y1);
    info = QString("%1nm").arg(mult);
  }



  #ifndef _MOTIF_
  gc->setPen(Qt::red);
  #endif

  int fmHeight = gc->fontMetrics().height();

  gc->moveTo(fmHeight, screenHeight - fmHeight);
  gc->lineTo(fmHeight + nm, screenHeight - fmHeight);

  gc->moveTo(fmHeight, screenHeight - fmHeight);
  gc->lineTo(fmHeight, screenHeight - 1.5*fmHeight);
  gc->moveTo(fmHeight + nm, screenHeight - fmHeight);
  gc->lineTo(fmHeight + nm, screenHeight - 1.5*fmHeight);
  gc->drawString(info, 1.2*fmHeight + nm, screenHeight - 2.0*fmHeight);
}

//-----------------------------------------------------------------------------
// Draw a compass rose with magnetic variation
//
void CGenMapWnd::drawRose(GraphicPainter *gc, int x, int y, int r)
{
  //int x, y;
  int isize;
  int a, b;
  int iVar = (int) getMagVar(centerLat, centerLon);

  #ifndef _MOTIF_
  gc->setPen(Qt::magenta);
  #endif

  isize = r;

  for (int i = 0; i < 360; i++) {
    int phi = (90+i+iVar);
    if (i % 90 == 0) {
      a = (int) uround(0.7*isize*cos(phi*M_PI/180.0));
      b = (int) uround(0.7*isize*sin(phi*M_PI/180.0));
      gc->radLine(x+a, y-b, (int) (0.3*isize), phi);

      // the middle cross
      gc->radLine(x, y, (int) (0.1*isize), phi);

      a = (int) uround(0.6*isize*cos((phi)*M_PI/180.0));
      b = (int) uround(0.6*isize*sin((phi)*M_PI/180.0));
      #ifndef _MOTIF_
      //gc->drawString(QString("%1").arg(360-i), x+a, y-b);
      gc->drawText(x+a-250, y-b-250, 500, 500, Qt::AlignCenter | Qt::AlignHCenter, QString("%1").arg(360-i));
      #endif
    }
    else if (i % 30 == 0) {
      a = (int) uround(0.9*isize*cos(phi*M_PI/180.0));
      b = (int) uround(0.9*isize*sin(phi*M_PI/180.0));
      gc->radLine(x+a, y-b, (int) (0.1*isize), phi);

      a = (int) uround(0.8*isize*cos((phi)*M_PI/180.0));
      b = (int) uround(0.8*isize*sin((phi)*M_PI/180.0));
      #ifndef _MOTIF_
      gc->drawText(x+a-250, y-b-250, 500, 500, Qt::AlignCenter | Qt::AlignHCenter, QString("%1").arg(360-i));
      #endif
    }
    else if (i % 10 == 0) {
      a = (int) uround(0.95*isize*cos(phi*M_PI/180.0));
      b = (int) uround(0.95*isize*sin(phi*M_PI/180.0));
      gc->radLine(x+a, y-b, (int) (0.05*isize), phi);

      a = (int) uround(0.85*isize*cos((phi)*M_PI/180.0));
      b = (int) uround(0.85*isize*sin((phi)*M_PI/180.0));
      #ifndef _MOTIF_
      gc->drawText(x+a-250, y-b-250, 500, 500, Qt::AlignCenter | Qt::AlignHCenter, QString("%1").arg(360-i));
      #endif
    }
    else {
      a = (int) uround(0.98*isize*cos(phi*M_PI/180.0));
      b = (int) uround(0.98*isize*sin(phi*M_PI/180.0));
      gc->radLine(x+a, y-b, (int) (0.02*isize), phi);
    }
  }
}

#if 0
  // A much simpler version
  void drawCourseRose(Graphics2D g2d) {
    LineRad2D l2r = new LineRad2D();
    // temp
    int x, y;
    int isize;
    int a, b;
    int iVar = (int) getMagVar(centerLat, centerLon);

    g2d.setStroke(basicStroke);
    g2d.setColor(Color.magenta);

    x = screenWidth/2;
    y = screenHeight/2;
    isize = screenHeight/2-10;

    Ellipse2D e2d = new Ellipse2D.Double();
    e2d.setFrame(x - isize, y - isize, isize*2, isize*2);
    g2d.draw(e2d);

    for (int i = 0; i < 360; i += 30) {
      int phi = (90+i+iVar);
      if (i % 90 == 0) {
        a = (int) Math.uround(0.7*isize*Math.cos(phi*M_PI/180.0));
        b = (int) Math.uround(0.7*isize*Math.sin(phi*M_PI/180.0));
        l2r.radLine(x+a, y-b, (int) (0.3*isize), phi);
        g2d.draw(l2r);

        // the middle cross
        l2r.radLine(x, y, (int) (0.1*isize), phi);
        g2d.draw(l2r);

        Font font = new Font("SansSerif", Font.PLAIN, 22*isize/255); //22
        g2d.setFont(font);
        a = (int) Math.uround(0.6*isize*Math.cos((phi)*M_PI/180.0));
        b = (int) Math.uround(0.6*isize*Math.sin((phi)*M_PI/180.0));
        g2d.drawString(new Integer(360-i).toString(), x+a, y-b);
      }
      else {
        a = (int) Math.uround(0.9*isize*Math.cos(phi*M_PI/180.0));
        b = (int) Math.uround(0.9*isize*Math.sin(phi*M_PI/180.0));
        l2r.radLine(x+a, y-b, (int) (0.1*isize), phi);
        g2d.draw(l2r);

        Font font = new Font("SansSerif", Font.PLAIN, 16*isize/255); //12
        g2d.setFont(font);
        a = (int) Math.uround(0.8*isize*Math.cos((phi)*M_PI/180.0));
        b = (int) Math.uround(0.8*isize*Math.sin((phi)*M_PI/180.0));
        g2d.drawString(new Integer(360-i).toString(), x+a, y-b);
      }
    }
  }
#endif

void CGenMapWnd::drawRose(GraphicPainter *gc)
{
  int x, y, r;

  x = screenWidth/2;
  y = screenHeight/2;
  r = screenHeight/2-10;

  int FontHeight;
  FontHeight = (int) (mag /* * FONT_SCALER*/);
  if (FontHeight <= MINFONTSIZE) FontHeight = MINFONTSIZE; //1;
  if (FontHeight >  MAXFONTSIZE)  FontHeight = MAXFONTSIZE;
  QFont font("Arial", FontHeight);
  gc->setFont(font);

  drawRose(gc, x, y, r);
}

//-----------------------------------------------------------------------------
// CrossHair functionality
//

//
// Set a crosshair
//
void CGenMapWnd::setCrossHair(float64_t lat, float64_t lon)
{
  xhlat = lat;
  xhlon = lon;
  bCrossHairActive = true;
}

//
// Clear a crosshair
//
void CGenMapWnd::clearCrossHair()
{
  bCrossHairActive = false;
}

//
// Draw a crosshair
//
void CGenMapWnd::drawCrossHair(GraphicPainter *gc)
{
  int x, y;
 
  QRect rect = gc->window();

  int cx = rect.width();
  int cy = rect.height();

  LtoC(xhlon, xhlat, x, y);
  gc->setStroke(1/*basicStroke*/);

  gc->setPen(Qt::magenta);

  gc->moveTo(x-15, y);
  gc->lineTo(0, y);

  gc->moveTo(x+15,  y);
  gc->lineTo(cx, y);

  gc->moveTo(x, y-15);
  gc->lineTo(x, 0);

  gc->moveTo(x, y+15);                                                    
  gc->lineTo(x, cy);
}


#include "GenMapWnd_drawBuffer.hpp"               
#include "GenMapWnd_drawArea.hpp"
#include "GenMapWnd_drawShpfile.hpp"
#include "GenMapWnd_drawShade.hpp"



/*
  //524 x 564  Pixels
  const int cBM_HEIGHT = 564;
  rSpan = 2.304;  //256 km = 2.304 deg lat
*/


void CGenMapWnd::Utility(GraphicPainter *gc)
{
/*
  float64_t latf, lonf;
  float64_t latf0, lonf0;
  float64_t latf1, lonf1;
  int idx = 2;  // note!

  QFile file("output.bin");
  file.open(IO_WriteOnly);
  //QTextStream fdata(&file);
  //fdata << ("<html>\n");

  QDataStream fdata(&file);
  fdata.setByteOrder(QDataStream::LittleEndian);

  for (int i = 0; i < nrpts[idx]; i++) {
    lonf = buff[idx][i];
    i++;
    latf = buff[idx][i];

    if ((latf == 9999) &&
        (lonf == 9999)) {
      lonf1 = buff[idx][i+1];
      latf1 = buff[idx][i+2];
    }

    lonf0 = lonf;
    latf0 = latf;

    fdata << lonf;
    fdata << latf;
  }
  file.close();
*/
}


//-----------------------------------------------------------------------------
// Paint - the main paint method
//
void CGenMapWnd::PaintMain(GraphicPainter *gc)
{
  if (bShadeActive)
    drawShadeR(gc, -top, left, -bottom, right);

  //---------------------------------
  // drawBuffer and drawBinPolygon
  //
  for (int i = 1; i < BUFNUM; i++) {
    #ifndef _MOTIF_
    gc->setPen(QPen(descriptor[i].crColor, descriptor[i].nWidth));
    #endif
    // we may want to add a check for iActive here ?
    if (mag >= descriptor[i].MagTH) {
      if ((descriptor[i].bPolygon == 0) || (bPolygonActive == 0))  {
        drawBuffer(gc, i);
      }
      else {
        if (!bShadeActive) {
          gc->setBrush(QColor(descriptor[i].crFill));
          drawBinPolygon(gc, i);
          gc->setBrush(Qt::NoBrush);
        }
      }
    }                              
  }

  //---------------------------------
  // drawShpfiles
  //
  CIniFile iniFile( "./knav.ini");
  iniFile.ReadFile();
  QFile file;
  //uint32_t len;
  char szFileName[256];
  char szSection[256];
  int32_t nPenStyle;
  int32_t nWidth;
  int32_t nTxtField;
  double rFSize;
  int32_t crColor;
  int32_t crFill;
  int32_t bPolygon;
  int32_t iMagTH = 1;
  int32_t iMagTT = 255;
  int32_t iActive = true;

  for (int n = 1; n < NR_BUF; n++) {
    std::string sPath;

    sprintf(szSection, "Esri_%d", n);
    
    m_descriptor.MagTH      = iMagTH    = iniFile.GetValueI(szSection, "magTH",      0);
    m_descriptor.MagTT      = iMagTT    = iniFile.GetValueI(szSection, "magTT",    255);
    m_descriptor.nPenStyle  = nPenStyle = iniFile.GetValueI(szSection, "nPenStyle",  0);
    m_descriptor.nWidth     = nWidth    = iniFile.GetValueI(szSection, "nWidth",     0);
    m_descriptor.nTxtField  = nTxtField = iniFile.GetValueI(szSection, "nTxtField", -1);
    m_descriptor.rFontScale = iniFile.GetValueF(szSection, "rFontScale",  1.0);
    m_descriptor.crColor    = crColor   = iniFile.GetValueI(szSection, "crColor",    0);
    m_descriptor.crFill     = crFill    = iniFile.GetValueI(szSection, "crFill",     0);
    m_descriptor.bPolygon   = bPolygon  = iniFile.GetValueI(szSection, "IsPolygon",  0);

    iActive   = iniFile.GetValueI(szSection, "Active",    0);
    sPath     = iniFile.GetValue(szSection,  "FileName", "~~~");
    strcpy(szFileName, sPath.c_str());
    strcpy(m_descriptor.FileName, szFileName);

    #ifndef _MOTIF_
    gc->setPen(QPen(crColor, nWidth));
    #endif

    if ((mag >= iMagTH) && iActive) {
      if ((bPolygon == 0) || (bPolygonActive == 0))  {
        drawShpfile(gc, szFileName); //, nField, rFSize);
      }
      else {
        if (!bShadeActive) {
          gc->setBrush(QColor(crColor));
          drawShpfile(gc, szFileName); //, nField, rFSize);
          gc->setBrush(Qt::NoBrush);
        }
      }
    }
  }

  drawGrid(gc);
  drawScaleRuler(gc);
  bIsDirty = false;
}


//-----------------------------------------------------------------------------
// Centre the current drawing on the point specified as the argument.
//
void CGenMapWnd::PanCenter(POINT point)
{
  float64_t rx, ry;

  if (!CtoL(point.x, point.y, rx, ry))
    return;

  float64_t yd = (top + bottom)/2.0 - ry;
  float64_t xd = (right + left)/2.0 - rx;
  left -= xd;
  right -= xd;
  top -= yd;
  bottom -= yd;

  centerLat = ry;
  centerLon = rx;

  LocationBar();
  Mready = FALSE;
  bIsDirty = true;
}

//-----------------------------------------------------------------------------
// Zoom to fit the whole world in the panel
//
// note:
//   renamed from ZoomToFit to ZoomToWorld
//
void CGenMapWnd::ZoomToWorld()
{
  float64_t minLat, maxLat, maxLon, minLon;
  mag = 1;
  mag2 = 1;
  minLat =  90.;
  bottom = -90.;
  maxLat = -90.;
  top =     90.;
  minLon =  180.;
  left =   -180.;
  maxLon = -180.;
  right =   180.;

  Size(screenWidth, screenHeight);                    

  // set new center
  float64_t rx, ry;//, xd, yd;
  rx = (minLon+maxLon)/2;
  ry = (minLat+maxLat)/2;
  left   += rx;
  right  += rx;
  top    += ry;
  bottom += ry;

  LocationBar();
  Mready = FALSE;
  return;
}

//-----------------------------------------------------------------------------
// Zoom to fit the specified rectangle in the panel
//
/*
void CGenMapWnd::ZoomToRect(float64_t minLat = +999,
                            float64_t maxLat = -999,
                            float64_t minLon = +999,
                            float64_t maxLon = -999)
{
  int x, y;
  float64_t Lat, Lon;

  ZoomToWorld();

  //
  // Pan to the middle of the
  // zoom rectangle
  //
  Lat = (minLat + maxLat) / 2;
  Lon = (minLon + maxLon) / 2;
  LtoC(Lon, +Lat, x, y);
  POINT p = {x, y};
  PanCenter(p);

  //
  // Zoom in until all the corner points
  // are within the rectangle
  //
  while ((minLon > left) &&
         (minLat > bottom) &&
         (maxLon < right) &&
         (maxLat < top)) {

    ZoomIn();
    if (mag > 15)
      break;
  }

  //
  // torque it :-)
  //
  ZoomOut();
 
  //
  // Fine tune the pan position
  //
  Lat = (top + bottom) / 2;
  Lon = (left + right) / 2;
  LtoC(Lon, Lat, x, y);
  p.x = x;
  p.y = y;
  PanCenter(p);
}
*/

void CGenMapWnd::ZoomToRect(float64_t minLat = +999,
                            float64_t maxLat = -999,
                            float64_t minLon = +999,
                            float64_t maxLon = -999)
{
  int x, y;
  float64_t Lat, Lon;
  POINT p;

  int iminLon;
  int imaxLon;
  int iminLat;
  int imaxLat;


  ZoomToWorld();

  //
  // Pan to the middle of the
  // zoom rectangle
  //
  Lat = (minLat + maxLat) / 2;
  Lon = (minLon + maxLon) / 2;
  LtoC(Lon, +Lat, x, y);
  p.x = x;
  p.y = y;
  PanCenter(p);

  LtoC(maxLon, maxLat, imaxLon, imaxLat);
  LtoC(minLon, minLat, iminLon, iminLat);

  //
  // Zoom in until any of the corner points
  // are within the rectangle
  //
  while ((iminLon > 0) &&
         (imaxLat > 0) &&
         (imaxLon < screenWidth) &&
         (iminLat < screenHeight)) {

    ZoomIn();

    //
    // Fine tune the pan position
    //
    Lat = (minLat + maxLat) / 2;
    Lon = (minLon + maxLon) / 2;
    LtoC(Lon, Lat, x, y);
    p.x = x;
    p.y = y;
    PanCenter(p);

    LtoC(maxLon, maxLat, imaxLon, imaxLat);
    LtoC(minLon, minLat, iminLon, iminLat);

    if (mag > 15)
      break;
  }

  //
  // torque it :-)
  //
  ZoomOut();
 
  //
  // Fine tune the pan position
  //
  Lat = (minLat + maxLat) / 2;
  Lon = (minLon + maxLon) / 2;
  LtoC(Lon, Lat, x, y);
  p.x = x;
  p.y = y;
  PanCenter(p);
}


//-----------------------------------------------------------------------------
// Increase the drawing scale by a factor of 2 (magnify or zoom in)
//
void CGenMapWnd::ZoomIn()
{
  ZoomIn2X();
}

void CGenMapWnd::ZoomIn2X()
{
  float64_t xd = (right - left) / 4.0;
  float64_t yd = (top - bottom) / 4.0;

  right -= xd;
  left += xd;
  top -= yd;
  bottom += yd;

  convX *= 2.0;
  convY *= 2.0;

  mag += 1.0;
  mag2 *= 2.0;
  LocationBar();
  Mready = false;
  bIsDirty = true;

  printf("CGenMapWnd::ZoomIn - mag = %f\n", mag);
}


//-----------------------------------------------------------------------------
// Decrease the drawing scale by a factor of 2 (reduce or zoom out)
//
void CGenMapWnd::ZoomOut()
{
  ZoomOut2X();
}

void CGenMapWnd::ZoomOut2X()
{
  if (mag > 1) {
    float64_t xd = (right - left) / 2.0;
    float64_t yd = (top - bottom) / 2.0;

    right += xd;
    left -= xd;
    top += yd;
    bottom -= yd;

    convX /= 2.0;
    convY /= 2.0;

    mag -= 1.0;
    mag2 /= 2.0;
    LocationBar();
    Mready = false;
    bIsDirty = true;
  }
  printf("CGenMapWnd::ZoomOut - mag = %d\n", mag);
}



//-----------------------------------------------------------------------------
// Helper function to handle changes to the compnents x y dimensions
//
void CGenMapWnd::Size(int cx, int cy)
{
  if ((cx == 0) && (cy == 0)) // happens during initialization
    return;

  convX = convY = mag2;

  if (cx > cy)
    convX *= cy / (float64_t) cx;
  else
    convY *= cx / (float64_t) cy;

  if (flatWorld) {
    float64_t xcent = (right + left)/2.0;
    float64_t ycent = (top + bottom)/2.0;
    float64_t ratio = (float64_t) cx / cy;
   
    // proportion correctly
    if (ratio > 2) {
      top = 90;
      bottom = -90;
      left = -top * ratio;
      right = top * ratio;
    }
    else {
      left = -180;
      right = 180;
      top = right / ratio;
      bottom = -right / ratio;
    }
    centerLat = ycent;
    centerLon = xcent;
 
    // scale
    for (int i=1; i<mag; i++) {
      float64_t xd = (right - left) / 4.;
      float64_t yd = (top - bottom) / 4.;
      right -= xd;
      left += xd;
      top -= yd;
      bottom += yd;
    }
 
    // center
    float64_t xd = (right + left) / 2.;
    float64_t yd = (top + bottom) / 2.;
    right += xcent-xd;
    left += xcent-xd;
    top += ycent-yd;
    bottom += ycent-yd;
  }

  //??
  screenWidth = cx;
  screenHeight = cy;
}


void CGenMapWnd::OnSize(int cx, int cy)
{
  Size(cx, cy);
}


//-----------------------------------------------------------------------------
// Discrete mouse button implementations
//

//-----------------------------------------------------------------------------
// Helper function to handle a left mouse button depress
// 
void CGenMapWnd::mouseLeftButtonDown(/*CPoint*/ POINT point)
{
  bAbort = true;

  //Point point = new Point(e.getX(), e.getY());
  if (bZoomActive) {
    if (nZoomMode == 0) {
      PanCenter(point);
      ZoomIn();
    }
  }
  else
    PanCenter(point);

  // new
  if (!m_bLeftButtonDown) {
    m_bLeftButtonDown = true;
    iMouseDownX = point.x;
    iMouseDownY = point.y;

  }
  // end new
}



//-----------------------------------------------------------------------------
// Helper function to handle a left mouse button release.
// Check if we are in "rectangle" zooming mode and handle that.
//
void CGenMapWnd::mouseLeftButtonUp(/*CPoint*/ POINT point)
{
  int x, y;
  float64_t CenterLat, CenterLong;

  // new
  //Point point = new Point(e.getX(), e.getY());
  if (m_bLeftButtonDown) {
    m_bLeftButtonDown = false;
    if (bZoomActive && (nZoomMode == 1)) {
      POINT p;
      float64_t mleft, mright, mtop, mbot;

      // NB:
      // Do the CtoL conversion *before*
      // we pan as iMouseDownN and point
      // will be meaningless after that.
      // The resultant Lat/Lon will of course
      // remain consistant.
      //
      CtoL(iMouseDownX, iMouseDownY, mleft, mtop);
      CtoL(point.x, point.y, mright, mbot);

      //
      // If the rect was not drawn from
      // top left to bottom right
      // deal with it
      //
      if (mright < mleft) {
        swapval(&mleft, &mright);
      }
      if (mtop < mbot) {
        swapval(&mtop, &mbot);
      }

      //
      // Pan to the middle of the
      // zoom rectangle
      //
      p.x = (point.x + iMouseDownX) / 2;
      p.y = (point.y + iMouseDownY) / 2;
      PanCenter(p);


      while ((left < mleft) && 
             (top > mtop) &&
             (bottom < mbot) &&
             (right > mright) &&
             (mag < 16)) {
        ZoomIn();
      }

      //
      // torque it :-)
      //
      ZoomOut();

      //
      // Fine tune the pan position
      //
      CenterLat = (top + bottom) / 2;
      CenterLong = (left + right) / 2;
      LtoC(CenterLong, CenterLat, x, y);
      p.x = x;
      p.y = y;
      PanCenter(p);

    }
  }
  // end new
  // Reset the last drag postions
  iMouseDragX = 0;
  iMouseDragY = 0;

  /*sw.reset();
  System.out.print("GenMapPanel mouse click cycle ");
  System.out.println(sw.getElaps());*/
}




//-----------------------------------------------------------------------------
// Helper function to handle a middle mouse button depress
//
void CGenMapWnd::mouseMiddleButtonDown(/*CPoint*/ POINT point)
{
  bAbort = true;
  PanCenter(point);
}

//-----------------------------------------------------------------------------
// Helper function to handle a middle mouse button release
//
void CGenMapWnd::mouseMiddleButtonUp(/*CPoint*/ POINT point)
{
}




//-----------------------------------------------------------------------------
// Helper function to handle a right mouse button depress
//
void CGenMapWnd::mouseRightButtonDown(/*CPoint*/ POINT point)
{
  bAbort = true;

  if (bZoomActive) {
    ZoomOut();
    if (nZoomMode == 1) {
      ZoomOut();
    }
  }
}

//-----------------------------------------------------------------------------
// Helper function to handle a right mouse button release
//
void CGenMapWnd::mouseRightButtonUp(/*CPoint*/ POINT point)
{
  // Reset the last drag postions
  iMouseDragX = 0;
  iMouseDragY = 0;
}

//-----------------------------------------------
// Mouse movement event handlers
//
void CGenMapWnd::mouseMoved(POINT point)
{
  float64_t rlat, rlon;
  CtoL(point.x, point.y, rlon, rlat);
#ifndef _MOTIF_

  if (bDisplayDMS)
    m_sStatusInfo = QString().sprintf("%02d %02d %02d %c %03d %02d %02d %c (%i) - %d",
                                                                utrunc(fabs(rlat)),
                                                                utrunc(60*ufract(fabs(rlat))),
                                                                utrunc(60*ufract(60*ufract(fabs(rlat)))),
                                                                rlat>0?'N':'S',
                                                                utrunc(fabs(rlon)),
                                                                utrunc(60*ufract(fabs(rlon))),
                                                                utrunc(60*ufract(60*ufract(fabs(rlon)))),
                                                                rlon>0?'E':'W',
                                                                (int) getMagVar(rlat, rlon),
                                                                (int) mag);
  else
    m_sStatusInfo = QString().sprintf("%06.4f%c %07.4f%c (%i) - %d", rlat, rlat>0?'N':'S',
                                                                rlon, rlon>0?'E':'W',
                                                                (int) getMagVar(rlat, rlon),
                                                                (int) mag);

#endif
}


#ifdef _MOTIF_
  #define ROP_XOR GXinvert
  #define ROP_CPY GXcopy

  #define LINEONOFFDASH LineOnOffDash
  #define LINESOLID     LineSolid

  #define CAPROUND      CapRound
  #define JOINROUND     JoinRound
#else
  #define ROP_XOR R2_NOTXORPEN
  #define ROP_CPY R2_COPYPEN

  #define LINEONOFFDASH 0
  #define LINESOLID     0

  #define CAPROUND      0
  #define JOINROUND     0
#endif

void CGenMapWnd::mouseDragged(GraphicPainter *gc, POINT point)
{
  // printf("CGenMapWnd::mouseDragged\n");
  GraphicPainter *g = gc;
  int x = iMouseDownX;
  int y = iMouseDownY;
  int width;
  int height;

  if (bZoomActive && (nZoomMode == 1)) {
    if (iMouseDragX && iMouseDragY) {
    #ifndef _MOTIF_
      gc->setPen(QPen(Qt::black, 0, Qt::DotLine /* Qt::DashLine*/));
    #endif
 
      /* undraw last box */
      gc->setLineAttributes(1, LINEONOFFDASH, CAPROUND, JOINROUND);
    #ifdef _MOTIF_
      gc->SetROP2(GXinvert);
    #else
      gc->SetROP2(Qt::NotXorROP);
    #endif
      width  = iMouseDragX - x;
      height = iMouseDragY - y;
      gc->drawRectangle(x, y, width, height);

      /* draw new box */
      width  = point.x - x;
      height = point.y - y;
      gc->drawRectangle(x, y, width, height);
      gc->setLineAttributes(1, LINESOLID, CAPROUND, JOINROUND);
      gc->SetROP2(ROP_CPY);
    }
  }
  iMouseDragX = point.x;
  iMouseDragY = point.y;
}




//-----------------------------------------------------------------------------
// CGenMapWnd drawing helpers
//
void CGenMapWnd::DrawOnTheFly(GraphicPainter *pDC)
{
/*
  char szOTFSet[1024];
  char *spFileName;
  char FileName[256];
  CPen Pen(PS_SOLID, 1, RGB_RED);
  char szSection[256];
  long nPenStyle;
  long nWidth;
  long crColor;

  int iMagTH = 1;
  int iMagTT = 255;
  int n = 1;

  for (n = 1; n < 11; n++) {
    sprintf(szSection, "OnTheFly_%d", n);
    iMagTH = GetPrivateProfileInt(szSection, "magTH",    0, "./mapwnd.ini");
    iMagTT = GetPrivateProfileInt(szSection, "magTT",  255, "./mapwnd.ini");
    if (!iMagTH) continue;
    if ((mag > iMagTH) && (mag < iMagTT)) {
            nPenStyle = GetPrivateProfileLong(szSection, "nPenStyle", 0, "./mapwnd.ini");
      nWidth = GetPrivateProfileLong(szSection,    "nWidth", 0, "./mapwnd.ini");
      crColor = GetPrivateProfileLong(szSection,   "crColor", 0, "./mapwnd.ini");

      Pen.DeleteObject();
      Pen.CreatePen(nPenStyle, nWidth, crColor);

      GetPrivateProfileString(szSection, "FileName", NULL, szOTFSet, 1023, "./mapwnd.ini");
      spFileName = strtok(szOTFSet, ",");
      while (spFileName) {
        sprintf(FileName, "%s%s", sBinPath, spFileName);
        //DrawLargeData(pDC, FileName, &Pen);
        DrawLargeData(pDC, FileName);
        spFileName = strtok(NULL, ",");
      }
    }
  }
*/
}

void CGenMapWnd::DrawHiLite(GraphicPainter *gc, float64_t Lat, float64_t Long, char *szName)
{
/*
  int x, y;
  CBrush *oldBrush;
  CBrush Brush(RGB_LTCYAN);
  CPen *oldPen;

  if (LtoC(Long, -Lat, x, y)) {
    oldPen = (CPen *) gc->SelectStockObject(NULL_PEN);
    oldBrush = (CBrush *) gc->SelectObject(&Brush);
    gc->Ellipse(x-12, y-12, x+12, y+12);
    gc->SelectObject(oldBrush);
    gc->SelectObject(oldPen);
  }
*/
}


//-----------------------------------------------------------------------------
// This routine is a draw while read type of plot
// typically used with very large data files not suited to
// direct memory loading
//
void CGenMapWnd::DrawLargeData(GraphicPainter *gc, char *szFileName)
{
  /*
  CFile cfiledata2;
  if (!cfiledata2.Open(szFileName, CFile::modeRead)) return;

  float64_t *pbufdata2;
  pbufdata2 = (float64_t *)malloc(FILE_SIZE);

  UINT nBytesRead = FILE_SIZE;
  UINT nNumRowsData2;

  while (nBytesRead == FILE_SIZE) {
    nBytesRead = cfiledata2.Read( pbufdata2, FILE_SIZE);
    nNumRowsData2 = nBytesRead / (2*sizeof(float64_t));
    drawBuffer(gc, Pen, pbufdata2, nNumRowsData2);
  } //while
 
  cfiledata2.Close();
  free(pbufdata2);
  */

  /*
  QFile file;
  int n = NR_BUF; // use the last buffer as general purpose

  file.setName(szFileName);
  if (!file.open(IO_ReadOnly)) {
    printf("CGenMapWnd::DrawLargeData - file bad - %s\n", szFileName);
    buff[n]  = (float64_t *) malloc(1);  // placeholder
    nrpts[n] = 0;
    file.close();
    return;
  }

  int len = file.size();
  buff[n] = (float64_t *) malloc(len);
  file.at(0);
  file.readBlock((char *) buff[n], len);

  nrpts[n] = len / (1*sizeof(float64_t));
  file.close();

  drawArea(gc, n);
  free(buff[n]);
  */

  // We use this as our testbed to draw shapefiles

}

 
//-----------------------------------------------------------------------------
// This routine to draw scaled names
// typically used for TOWN and PLACE names
//
void CGenMapWnd::DrawNames(GraphicPainter *gc, char *p, long loop)
{
/*
  char *sp;
  int x, y;
  float64_t rLat, rLong;
  char szName[40];
  int nSize;
  UINT row = 0;

  int LogPixY = gc->GetDeviceCaps(LOGPIXELSY);
  CFont Font, *OldFont;
  int FontHeight = (int) (2 * mag * LogPixY / m_LogPixY);
  if (FontHeight <= 4) FontHeight = 1;
  gc->SetupFont(&Font, FontHeight);
  OldFont = (CFont*) gc->SelectObject(&Font);

  if (bShadeActive)
    gc->SetTextColor(RGB_BLACK);
  else
    gc->SetTextColor(RGB_DKGRAY);

  gc->SetBkMode(TRANSPARENT);
  sp =  p;
  while (row <  loop) {
    sp[30] = '\0';
    strzaptail(sp, ' ');
    strcpy(szName, sp);

    sp = sp + 31;
    sp[1] = '\0';
    nSize = atoi(sp);

    sp = sp + 8;
    sp[9] = '\0';
    rLat = atof(sp);

    sp = sp + 11;
    sp[9] = '\0';
    rLong = atof(sp);
 
    sp = sp + 11;
    row++;

    if (rLong == 0)
      rLong = 1;
    if (rLat == 0)
      rLat = 1;

    if ((mag <= 6) && (nSize > 2))
      continue;
    if ((mag <= 5) && (nSize > 1))
      continue;

    //if (LtoC(rLong, -rLat, x, y)) {
    LtoC(rLong, -rLat, x, y); {
      if (mag > 1) {
        gc->TextOut(x, y, szName);
      }
    }
  }
  gc->SelectObject(OldFont);
*/
}


void CGenMapWnd::DrawStreetNames(GraphicPainter *gc, char *p, long loop)
{
/*
  char *sp;
  int x, y;
  float64_t rLat, rLong;
  char szName[40];
  int nSize;
  int row = 0;
  CFont Font, *OldFont;

  int LogPixY = gc->GetDeviceCaps(LOGPIXELSY);

  int FontHeight = (int) (0.75 * mag * LogPixY / m_LogPixY);

  gc->SetupFont(&Font, FontHeight);
  OldFont = (CFont*) gc->SelectObject(&Font);
  sp =  p;
  while (row <  loop) {
    sp[30] = '\0';
    strzaptail(sp, ' ');
    strcpy(szName, sp);

    sp = sp + 31;
    sp[1] = '\0';
    nSize = atoi(sp);

    sp = sp + 8;
    sp[9] = '\0';
    rLat = atof(sp);

    sp = sp + 11;
    sp[9] = '\0';
    rLong = atof(sp);
 
    sp = sp + 11;
    row++;

    if (rLong == 0)
      rLong = 1;
    if (rLat == 0)
      rLat = 1;

    if (LtoC(rLong, -rLat, x, y)) {
    //LtoC(rLong, -rLat, x, y); {
      if (mag > 1) {
        gc->TextOut(x, y, szName);
        gc->Rectangle(x-2, y-2, x+2, y+2);
      }
    }
  }
  gc->SelectObject(OldFont);
*/
}

void CGenMapWnd::DrawRoadNames(GraphicPainter *gc, char *p, long loop)
{
/*
  char *sp;
  int x, y;
  float64_t rLat, rLong;
  char szName[40];
  int nSize;
  int row = 0;
  CFont Font, *OldFont;
  int i;
  POINT ppoint[6] = {1,1, 2,0, 1,-1, -1,-1, -2,0, -1,1};
  POINT point[6];
  CBrush *oldBrush;
  CBrush Brush(RGB_GREEN);
  CPen *oldPen;

  int LogPixY = gc->GetDeviceCaps(LOGPIXELSY);

  int FontHeight = (int) (1 * mag * LogPixY / m_LogPixY);
  if (FontHeight <= 4) FontHeight = 1;
  int FontHeight2 = FontHeight / 2;

  gc->SetTextColor(RGB_DKGRAY);
  UINT iAlign = gc->SetTextAlign(TA_CENTER);
 
  gc->SetupFont(&Font, FontHeight);
  OldFont = (CFont*) gc->SelectObject(&Font);
  sp =  p;
  while (row <  loop) {
    sp[30] = '\0';
    strzaptail(sp, ' ');
    strcpy(szName, sp);

    sp = sp + 31;
    sp[1] = '\0';
    nSize = atoi(sp);

    sp = sp + 8;
    sp[9] = '\0';
    rLat = atof(sp);

    sp = sp + 11;
    sp[9] = '\0';
    rLong = atof(sp);
 
    sp = sp + 11;
    row++;

    if (rLong == 0)
      rLong = 1;
    if (rLat == 0)
      rLat = 1;

    if (LtoC(rLong, -rLat, x, y)) {
    //LtoC(rLong, -rLat, x, y); {
      if (mag > 1) {
        for (i = 0; i < 6; i++) {
          point[i].x = (2+FontHeight2)*ppoint[i].x + x;
          point[i].y = (2+FontHeight2)*ppoint[i].y + y;
        }
        gc->Polygon((LPPOINT)&point, 6);
        gc->TextOut(x, y - (FontHeight2), szName);
      }
    }
  }
  gc->SetTextAlign(iAlign);
  gc->SelectObject(OldFont);
*/
}
 

void CGenMapWnd::drawLargeArea(GraphicPainter *gc, char *szFileName, COLORREF color)
{
/*
  QFile cfiledata2;
  if (!cfiledata2.Open(szFileName, CFile::modeRead)) return;

  float64_t *pbufdata2;
  pbufdata2 = (float64_t *)malloc(FILE_SIZE);

  UINT nBytesRead = FILE_SIZE;
  UINT nNumRowsData2;

  while (nBytesRead == FILE_SIZE) {
    nBytesRead = cfiledata2.Read( pbufdata2, FILE_SIZE);
    nNumRowsData2 = nBytesRead / (2*sizeof(float64_t));
    void CGenMapWnd::DrawAreas(GraphicPainter* gc, COLORREF color, float64_t *p, long loop)
  } //while
 
  cfiledata2.Close();
  free(pbufdata2);
*/
}


//-----------------------------------------------------------------------------
// Helper functions
//


//-----------------------------------------------------------------------------
// Projection member functions
//
void CGenMapWnd::ProjectGetXY(float64_t Lat, float64_t Long, int *x, int *y)
{
  //CRect r;
  //GetClientRect(&r);

  *x = (int) (M*Long + OffsetPt.x);
  *y = (int) (M*Lat + OffsetPt.y);
}

void CGenMapWnd::ProjectGetLatLong(int x, int y, float64_t *Lat, float64_t *Long)
{
  //CRect r;
  //GetClientRect(&r);

  *Long = (x - OffsetPt.x)/M;
  *Lat = (y - OffsetPt.y)/M;
}


bool CGenMapWnd::EraseBkgnd(GraphicPainter* pDC)
{
#ifdef WIN32
/*
  CBrush backBrush;

  if (bShadeActive)
    backBrush.CreateSolidBrush(RGB_BLACK);
  else
    backBrush.CreateSolidBrush(RGB_WHITE);
 
  // Save the old brush
  CBrush* pOldBrush = pDC->SelectObject(&backBrush);
 
  // Get the current clipping boundary
  CRect rect;
  pDC->GetClipBox(&rect);
 
  // Erase the area needed
  pDC->PatBlt(rect.left, rect.top, rect.Width(), rect.Height(), PATCOPY);
 
  pDC->SelectObject(pOldBrush); // Select the old brush back
  return TRUE;  // message handled
  // return CWnd::OnEraseBkgnd(pDC);
*/
  return TRUE;  // message handled
#else
  //todo
  return TRUE;  // message handled
#endif
}


/*
int CGenMapWnd::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
  if (CWnd::OnCreate(lpCreateStruct) == -1)
    return -1;
 
  SetTimer(3, 5000, NULL);       //tick every 5 seconds
  //ZoomToFit();
  LoadData();
 
  return 0;
}
*/


//-----------------------------------------------------------------------------
// Mouse and mouse button handlers
//
/*
void CGenMapWnd::OnMouseMove(UINT nFlags, CPoint point)
{
  float64_t rLat, rLong;
  char sLat[256], sLong[256];
  char s[256];

  if (!CtoL(point.x, point.y, rLong, rLat))
    return;

  if (bLButtonDown) {
    GraphicPainter *TheDC = GetDC();
    CBrush *OldBrush = (CBrush *) TheDC->SelectObject(GetStockObject(NULL_BRUSH));
    //CPen *OldPen     = (CPen *)   TheDC->SelectObject(GetStockObject(BLACK_PEN));
    TheDC->SetROP2(R2_NOT);
    if (oLx != -1) {
      if (bZoomActive && (nZoomMode == 1)) {
        TheDC->Rectangle(iMouseDownX, iMouseDownY, oLx, oLy);
        TheDC->Rectangle(iMouseDownX, iMouseDownY,  point.x, point.y);
      }
    }
    oLx = point.x;  oLy = point.y;
    //TheDC->SelectObject(OldPen);
    //TheDC->SelectObject(OldBrush);
    ReleaseDC(TheDC);
  }
  // end new

  CWnd::OnMouseMove(nFlags, point);
}
*/





//-----------------------------------------------------------------------------
// CNavView drawing


 
// returns TRUE if abort was detected
/*
bool CGenMapWnd::drawMessageLoop()
{
  return FALSE;

  MSG msg;
  while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
    if (msg.message == WM_QUIT) {
      PostQuitMessage(0);
      TranslateMessage(&msg);
      DispatchMessage(&msg);
      ExitProcess(0);     // Whoa boy !!!! ???
    }
    if ((msg.message == WM_PAINT) && (bAbort == TRUE)) {
      // abort this and start over
      bAbort = FALSE;
      bIsDirty = TRUE;
      bIsBusy = FALSE;
      return TRUE;
    }
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }
  return FALSE;
}
*/



void CGenMapWnd::OnViewFlatworld()
{
  flatWorld = !flatWorld;
  Size(screenWidth, screenHeight);
  Mready = FALSE;
  //-->Redraw();
}

void CGenMapWnd::OnViewShade()
{
  bShadeActive = !bShadeActive;
  //-->Redraw();
}

/*
// depreciated
void CGenMapWnd::OnViewBackgroundNone()
{
  iBackGround = 0;
  //-->Redraw();
}

// depreciated
void CGenMapWnd::OnViewBackgroundGreyscale()
{
  iBackGround = 1;
  bDibFileLoaded = FALSE;
  //-->Redraw();
}

// depreciated
void CGenMapWnd::OnViewBackgroundColor()
{
  iBackGround = 2;
  bDibFileLoaded = FALSE;
  //-->Redraw();
}
*/

/*
void CGenMapWnd::OnTimer(UINT nIDEvent)
{
  if (nIDEvent != 3) return;
  //if (!bMapActive) return;
  //if (bForecastBusy) return;
  if (bIsBusy) return;

  if (bIsDirty) {
    bIsDirty = FALSE;
    bAbort = FALSE;
    //EnterShips();      // contents of the FCset
    //EnterMovement();      //

    //Invalidate(FALSE); // Rect(NULL, FALSE);
  }
 
  CWnd::OnTimer(nIDEvent);
}
*/


