#ifndef __INFOHTML_H
#define __INFOHTML_H

#include <qstring.h>
#include "WptSet.h"


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

class InfoHtml
{

public:
  QString sRegion;
  QString sCountry;

  // Declare and open the InfoSet as final outside the inner class TreeSelectionListener
  CWptSet *wpt;
 
  InfoHtml();
  double getMagVar(double lat, double lon);
  void buildAirportHtml(QString icao);
  void buildAirportDefaultHtml(void);

  bool createAirportInfoHtml(QString icao);
  //void createCommsInfoHtml(QString msg);
  void createCommsInfoHtml(QString sFilename, QString sRegistration, QString sDateTime, QString sType, QString msg);
};


#endif // __INFOHTML_H
