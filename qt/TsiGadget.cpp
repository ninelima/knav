//-----------------------------------------------------------------------------
// $Id: TsiGadget.cpp,v 1.5 2005/10/30 10:07:13 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <qrect.h>
#include <math.h>
#include "TsiGadget.h"
#include "umath.h"
#include "uvalues.h"

//#define min(a,b)            (((a) < (b)) ? (a) : (b))

//-----------------------------------------------------------------------------
// Default constructor
//
TsiGadget::TsiGadget(QWidget *parent, const char *name, WFlags f)
    //:QWidget(parent, name, f)
    :PanelGadget(parent, name, f)
{
  // capture mouse movements only if buttons are pressed
  // ie mouse dragged
  //setMouseTracking(false);
  // not!
  setMouseTracking(true);
  m_bMouseLeftButtonDown = false;
  m_bMouseRightButtonDown = false;
	m_roll = 0;
}



void TsiGadget::drawBlankFace(GraphicPainter *gc, int x, int y, int size)
{
  int fontheight = gc->fontMetrics().height();
  int fontwidth = gc->fontMetrics().width('0');

  gc->setPen(Qt::gray);
  gc->setBrush(Qt::gray);
  gc->drawRect(0, 0, 150*m_scale, 150*m_scale);

  gc->setPen(Qt::black);
  gc->setBrush(Qt::black);
  gc->drawEllipse(0, 0, 150*m_scale, 150*m_scale);
}


void TsiGadget::drawFace(GraphicPainter *gc)
{

  QString ts;
  int a, b;
  int x = 75;
  int y = 75;
  int ang;

/*
{***************************************************************}
{*  Turn Coordinator and Turn and Slip indicator               *}
{***************************************************************}
procedure TSI(roll :integer);
var
  x,y : integer;
  a,b : integer;
  ang : integer;

begin
  y := 380;
  x := R*0 +R div 2;

  blank(x,y,65);

  setcolor(white);
  hexagon(x,y,65);

  {little plane body}
  circle(x,y,8);
  setfillstyle(solidfill,white);
  floodfill(x,y,white);
  {little plane wings}
  RadLine(x,y,25,90+roll);
  RadLine(x,y,45,0+roll);
  RadLine(x,y,45,180+roll);

  ang := 90-roll; { bank }
  a := round(8*cos(ang*pi/180));
  b := round(8*sin(ang*pi/180));
  radline(x-a,y-b,20,(180+roll));
  radline(x-a,y-b,20,+roll);

  { rate turn lines}
  setlinestyle(solidln,0,ThickWidth);
  line(x-65,y,x-50,y);
  line(x+65,y,x+50,y);
  calibxlong(x,y,50,-15);
  calibxlong(x,y,50,+15+180);
  setlinestyle(solidln,0,NormWidth);

  {slip indicator}
  arc(x,y-50,250,290, 85);
  arc(x,y-50,253,287,100);
  line(x-30,y+30,x-30,y+45);
  line(x+30,y+30,x+30,y+45);
  floodfill(x,y+35+5,white);

  setcolor(black);
  line(x-10,y+37,x-10,y+47);
  line(x+10,y+37,x+10,y+47);
  { ball }
  circle(x,y+43,5);

  setfillstyle(solidfill,black);
  floodfill(x,y+43,black);
  setcolor(white);
  setfillstyle(solidfill,white);
end;
*/
  //gc->setPen(QPen(Qt::yellow, 3*m_scale));
  gc->setPen(QPen(Qt::white, 1*m_scale));
  //gc->setBrush(Qt::gray);
  gc->setBrush(Qt::white);

  // {little plane body}
  //circle(x,y,8);
  gc->drawEllipse((x-8)*m_scale, (y-8)*m_scale, 17*m_scale, 17*m_scale);

  gc->setPen(QPen(Qt::white, 2*m_scale));
  
  //{little plane wings}
  gc->radLine(x*m_scale, y*m_scale, m_scale*25,  90 + m_roll);
  gc->radLine(x*m_scale, y*m_scale, m_scale*45,   0 + m_roll);
  gc->radLine(x*m_scale, y*m_scale, m_scale*45, 180 + m_roll);

  ang = 90 - m_roll; //{ bank }
  a = uround(8*cos(ang*M_PI/180));
  b = uround(8*sin(ang*M_PI/180));
  gc->radLine((x-a)*m_scale, (y-b)*m_scale, 20*m_scale, (180+m_roll));
  gc->radLine((x-a)*m_scale, (y-b)*m_scale, 20*m_scale, +m_roll);
	
	
  // { rate turn lines}
  gc->setPen(QPen(Qt::white, 3*m_scale));

  gc->drawLine((x-65)*m_scale, y*m_scale, (x-50)*m_scale , y*m_scale);
  gc->drawLine((x+65)*m_scale, y*m_scale, (x+50)*m_scale , y*m_scale);

  //calibxlong(x,y,50,-15);
  int off;
  int phi;
  
	off = 50;
  phi = -15;
  a = uround(off*cos(phi*M_PI/180));
  b = uround(off*sin(phi*M_PI/180));
  gc->radLine((x+a)*m_scale, (y-b)*m_scale, m_scale*15, phi);

  //calibxlong(x,y,50,+15+180);
  off = 50;
  phi = +15+180;
  a = uround(off*cos(phi*M_PI/180));
  b = uround(off*sin(phi*M_PI/180));
  gc->radLine((x+a)*m_scale, (y-b)*m_scale, m_scale*15, phi);

  gc->setPen(QPen(Qt::white, 1*m_scale));
	/*
	todo
  {slip indicator}
  arc(x,y-50,250,290, 85);
  arc(x,y-50,253,287,100);
  line(x-30,y+30,x-30,y+45);
  line(x+30,y+30,x+30,y+45);
  floodfill(x,y+35+5,white);

  setcolor(black);
  line(x-10,y+37,x-10,y+47);
  line(x+10,y+37,x+10,y+47);
  { ball }
  circle(x,y+43,5);

  setfillstyle(solidfill,black);
  floodfill(x,y+43,black);
  setcolor(white);
  setfillstyle(solidfill,white);
  */


}

void TsiGadget::setRot(int rot)
{
	if (m_roll != rot) {
	  m_roll = rot*5;
		//m_roll = 3*5;
		repaint(false);
	}
}

void TsiGadget::mousePressEvent(QMouseEvent *e)
{
}

