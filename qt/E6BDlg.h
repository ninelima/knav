#ifndef __E6BDLG_H
#define __E6BDLG_H

#include <qvariant.h>
#include <qdialog.h>

#include "E6BGadget.h"

class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QIconView;
class QIconViewItem;
class QLabel;
class QLineEdit;
class QPushButton;
class QButtonGroup;
class QRadioButton;

class E6BDlg : public QDialog
{ 
    Q_OBJECT

public:
    E6BDlg(QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~E6BDlg();


    E6BGadget* e6bGadget;

    QButtonGroup *buttonGroup; 
    QRadioButton *button10;
    QRadioButton *button60;
    QPushButton  *buttonCancel;

protected:
    QVBoxLayout* E6BDlgLayout;
    QHBoxLayout* Layout22;
    QVBoxLayout* Layout20;
    QGridLayout* Layout17;
    QHBoxLayout* Layout19;
    QHBoxLayout* Layout1;
//    bool event( QEvent* );

private slots:
    void    radioButtonClicked( int );

};

#endif // __E6BDLG_H
