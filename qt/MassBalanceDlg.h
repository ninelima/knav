/****************************************************************************
** Form interface generated from reading ui file 'mydialog.ui'
**
** Created: Mon Jun 2 20:11:47 2003
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#ifndef __MASSBALANCEDLG_H
#define __MASSBALANCEDLG_H

#include <qvariant.h>
#include <qdialog.h>

#ifndef __MASSBALANCEGADGET_H
#include "MassBalanceGadget.h"
#endif 


class QVBoxLayout; 
class QHBoxLayout; 
class QGridLayout; 
class QIconView;
class QIconViewItem;
class QLabel;
class QLineEdit;
class QPushButton;

class MassBalanceDlg : public QDialog
{ 
    Q_OBJECT

public:
    MassBalanceDlg( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~MassBalanceDlg();

		MassBalanceGadget* massBalanceGadget;

    QPushButton *buttonEdit;
    QPushButton *buttonPrint;
    QPushButton *buttonCancel;

protected:
    QVBoxLayout* MassBalanceDlgLayout;
    QHBoxLayout* Layout22;
    QVBoxLayout* Layout20;
    QGridLayout* Layout17;
    QHBoxLayout* Layout19;
    QHBoxLayout* Layout1;
//    bool event( QEvent* );

private slots:
  void edit();
  void aircraftChanged();
  void print();


signals:

};

#endif //__MASSBALANCEDLG_H
                        