//-----------------------------------------------------------------------------
// $Id: HudGadget.cpp,v 1.9 2006/04/14 14:29:17 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <qrect.h>
#include <math.h>
#include "HudGadget.h"
#include "umath.h"
#include "uvalues.h"

#define min(a,b)            (((a) < (b)) ? (a) : (b))

int sign(int a)
{
  if (a < 0) return -1;
  if (a > 0) return +1;
  return 0;
}

//-----------------------------------------------------------------------------
// Default constructor
//
HudGadget::HudGadget(QWidget *parent, const char *name, WFlags f)
    :PanelGadget(parent, name, f)
{

  g_Pitch = 0;//8;
  g_Bank = 20;//30;
  g_Hdg = 0;//35;
	g_Alt = 0;
	g_Gs = 0;
}


void HudGadget::drawBlankFace(GraphicPainter *gc, int x, int y, int size)
{
  gc->setPen(Qt::gray);
  gc->setBrush(Qt::gray);
  gc->drawRect((x-75)*m_scale, (y-75)*m_scale, 150*m_scale, 150*m_scale);

  gc->setPen(Qt::black);
  gc->setBrush(Qt::black);
  gc->drawRect((x-75)*m_scale, (y-75)*m_scale, 150*m_scale, 150*m_scale);

  gc->setPen(Qt::white);
}

void HudGadget::cropFace(GraphicPainter *gc, int x, int y, int size)
{
  gc->setPen(Qt::gray);
  gc->setBrush(Qt::gray);

  gc->drawRect((x-75)*m_scale, (y+75)*m_scale, 150*m_scale, 150*m_scale); // bottom 
  gc->drawRect((x+75)*m_scale, (y-75)*m_scale, 150*m_scale, 150*m_scale); // right

  gc->setPen(Qt::white);
}


void HudGadget::drawFace(GraphicPainter *gc)
{
  int x = 75;
  int y = 75;
  int LINE = 8;												
  int LMARGIN = x + 29;

  char s[256];// = " N   010  020  030  040  050  060  070  080   E  100 110 120 130 140 150 160 170  S  190 200 210 220 230 240 250 260  W  280 290 300 310 320 330 340 350";
  char r[] =       " �    �    �    �    �    �    �    �    �    �    �    �    �    �    �   "; 
  //char r[] =       " � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � � "; 
  char z[16];

  int theta;
  int l = x/2;
  int a, b;
  int c, d;
  int i;
  int idot = 10*2;
  int step;
  int mHdg, mAlt, mGs;

  int size = m_size;
  drawBlankFace(gc, 75, 75, size);

  QFont font1("Arial", 7*m_scale); 
  gc->setFont(font1);

  mHdg = g_Hdg / 10;
  s[0] = 0;

  for (i = mHdg-4; i <= mHdg+5; i++) {
    sprintf(z, "%02d0   ", i < 0 ? i+36 : i >= 36 ? i-36 : i);
    strcat(s, z);
  }

	// Little box for compass ribbon -- really only useful for debug
  //gc->drawRect((x-55)*m_scale, 0, 110*m_scale, 3*LINE);

	gc->drawText(10 - uround(g_Hdg * 2.4) % 24, 5+0*LINE, 150*m_scale, 2*LINE, Qt::AlignHCenter, s);
	gc->drawText(10 - uround(g_Hdg * 2.4) % 24, 5+1*LINE, 150*m_scale, 2*LINE, Qt::AlignHCenter, r);

	// Clip the sides of the box text
  gc->setPen(Qt::black);
  gc->setBrush(Qt::black);
  gc->drawRect(0, 0, (x-55)*m_scale, 3*LINE);
  gc->drawRect((x+55)*m_scale, 0, 21*m_scale, 3*LINE);

	// reset the pen back to white
  gc->setPen(Qt::white);

	// Alt and Gs
  gc->drawString(QString().sprintf("%04dft", uround(g_Alt)), (LMARGIN+5)*m_scale, y*m_scale);
  gc->drawString(QString().sprintf("%03dkt", uround(g_Gs)),  0,                   y*m_scale);

  // Hdg bug
	gc->moveTo(x*m_scale,       19*m_scale);
	gc->lineTo((x+3)*m_scale,  (19+3)*m_scale);
	gc->lineTo((x-3)*m_scale,  (19+3)*m_scale);
	gc->lineTo(x*m_scale,       19*m_scale);

  //Calc theta
  theta = g_Bank;
  step  = g_Pitch;

  // The gull wing que
	/*
  gc->setPen(QPen(Qt::white, 2));
  gc->drawPoint((x - 1)*m_scale, y*m_scale);
  gc->drawPoint((x + 1)*m_scale, x*m_scale);
  gc->drawPoint(x*m_scale,   (x - 1)*m_scale);
  gc->drawPoint(x*m_scale,   (x + 1)*m_scale);
  gc->drawPoint(x*m_scale,    x*m_scale);

  gc->moveTo((x + 30)*m_scale, y*m_scale);
  gc->lineTo((x +  9)*m_scale,  y*m_scale);
  gc->lineTo((x +  9)*m_scale,  (y + 6)*m_scale);
  gc->moveTo((x - 30)*m_scale, y*m_scale);
  gc->lineTo((x -  9)*m_scale,  y*m_scale);
  gc->lineTo((x -  9)*m_scale,  (y + 6)*m_scale);
	*/

  // F16 style que
  gc->drawEllipse((x-3)*m_scale, (y-3)*m_scale, 8*m_scale, 8*m_scale);
  gc->moveTo((x + 3)*m_scale,   y*m_scale);
  gc->lineTo((x + 10)*m_scale,  y*m_scale);
  gc->moveTo((x - 3)*m_scale,   y*m_scale);
  gc->lineTo((x - 10)*m_scale,  y*m_scale);
  gc->moveTo(x*m_scale,  (y-3)*m_scale);
  gc->lineTo(x*m_scale,  (y-10)*m_scale);
  
	// Pitch Bank ladder
  QFont font2("Arial", 5*m_scale); //, fontsize);
  gc->setFont(font2);
  
	for (step = -4; step <=4; step += 1) {
    if (step % 2 == 0)
      l = x/2;
    else
      l = x/4;
    
    if (step == 0)
      l = x;//-10;

    if (step < 0)
      gc->setPen(QPen(Qt::white, 1, Qt::DotLine));
    else
      gc->setPen(QPen(Qt::white, 1, Qt::SolidLine));


    if (step == 0) {
      // Horizon
      a = uround((uround(step*idot-g_Pitch*2))*cos((theta+90)*M_PI/180.0));
      b = uround((uround(step*idot-g_Pitch*2))*sin((theta+90)*M_PI/180.0));
      c = uround((l/4)*cos((theta)*M_PI/180.0));
      d = uround((l/4)*sin((theta)*M_PI/180.0));
      
      gc->radLine((x+a+c)*m_scale, (y-b-d)*m_scale, (l-2)*m_scale, theta); 
      gc->radLine((x+a-c)*m_scale, (y-b+d)*m_scale, (l-2)*m_scale, theta+180); 
    }
    else {
      // Pitch / bank steps
      a = uround((uround(step*idot-g_Pitch*2))*cos((theta+90)*M_PI/180.0));
      b = uround((uround(step*idot-g_Pitch*2))*sin((theta+90)*M_PI/180.0));
      c = uround((l)*cos((theta)*M_PI/180.0));
      d = uround((l)*sin((theta)*M_PI/180.0));

      if (step % 2 == 0) {
        gc->radLine((x+a+c)*m_scale, (y-b-d)*m_scale, 5*m_scale, theta + sign(step)*270); // the little ears
		    gc->drawText((x+a+(c*1.2))*m_scale, (y-b-(d*1.2))*m_scale, 20*m_scale, 20, Qt::AlignLeft, QString("%1").arg(step*5)); 

        gc->radLine((x+a-c)*m_scale, (y-b+d)*m_scale, 5*m_scale, theta + sign(step)*270); // the little ears
		    gc->drawText((x+a-(c*1.6))*m_scale, (y-b+(d*1.6))*m_scale, 20*m_scale, 20, Qt::AlignLeft, QString("%1").arg(step*5)); 
      }  
      gc->radLine((x+a)*m_scale, (y-b)*m_scale, (l+1)*m_scale, theta); 
      gc->radLine((x+a)*m_scale, (y-b)*m_scale, (l+2)*m_scale, theta+180); 
    }
  }

  cropFace(gc, 75, 75, size);
}


void HudGadget::setHdg(int Hdg)
{
	if (g_Hdg != Hdg) {
	  g_Hdg = Hdg;
		repaint(false);
	}
}

void HudGadget::setAlt(int Alt)
{
	if (g_Alt != Alt) {
	  g_Alt = Alt;
		repaint(false);
	}
}

void HudGadget::setGs(int Gs)
{
	if (g_Gs != Gs) {
	  g_Gs = Gs;
		repaint(false);
	}
}

void HudGadget::setPitch(int Pitch)
{
  if (g_Pitch != Pitch) {
    g_Pitch = Pitch;
		repaint(false);
  }
}


void HudGadget::setBank(int Bank)
{
  if (g_Bank != Bank) {
    g_Bank = Bank;
		repaint(false);
  }
}



#if 0
//-----------------------------------------------------------------------------
// This virtual method is called whenever the window is resized. We
// use it to make sure that the off-screen buffer is always the same
// size as the window.
// To retain the original drawing, it is first copied
// to a temporary buffer. After the main buffer has been resized and
// filled with white, the image is copied from the temporary buffer to
// the main buffer.
//
void HudGadget::resizeEvent(QResizeEvent* event)
{
  pixbuffer.resize(event->size());
  pixbuffer.fill(white);
}

void HudGadget::mousePressEvent(QMouseEvent *e)
{
  if (e->button() == LeftButton) {
    m_bMouseLeftButtonDown = true;
  }

  if (e->button() == RightButton) {
    m_bMouseRightButtonDown = true;
  }
}

void HudGadget::mouseReleaseEvent(QMouseEvent *e)
{
  if (e->button() == LeftButton) {
    m_bMouseLeftButtonDown = false;
  }

  if (e->button() == RightButton) {
    m_bMouseRightButtonDown = false;
  }
}

void HudGadget::mouseMoveEvent(QMouseEvent *e)
{
  int x = rect.width()/2;
  int y = rect.height()/2;
  int dx = e->x()-x;
  int dy = e->y()-y;
  int theta = (int) (180/M_PI*atan2(dy, dx));

  QWidget::mouseMoveEvent(e);
}

#endif
