//-----------------------------------------------------------------------------
// $Id: PanelGadget.cpp,v 1.3 2005/09/29 14:03:26 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <qrect.h>
#include <math.h>
#include "PanelGadget.h"
#include "umath.h"
#include "uvalues.h"


//-----------------------------------------------------------------------------
// Default constructor
//
PanelGadget::PanelGadget(QWidget *parent, const char *name, WFlags f)
    :QWidget(parent, name, f)
{
  // capture mouse movements only if buttons are pressed
  // ie mouse dragged
  //setMouseTracking(false);
  // not!
  setMouseTracking(true);
  m_bMouseLeftButtonDown = false;
  m_bMouseRightButtonDown = false;
}


//-----------------------------------------------------------------------------
// Paint method the panel
//
void PanelGadget::paintEvent(QPaintEvent* e)
{
  QPainter p(this);
  QPainter b(&pixbuffer);
  GraphicPainter *gc = (GraphicPainter *) &p;

  rect = p.window();
  int screenWidth  = rect.width();
  int screenHeight = rect.height();
 
  m_size = min(screenWidth, screenHeight)/2;
  m_scale = (double) m_size / 75.0;

  int x = 75;
  int y = 75;
  drawBlankFace((GraphicPainter *) &b, x, y, m_size);

  drawFace((GraphicPainter *) &b);
  // copy the image from the buffer pixmap to the window
  bitBlt(this, 0, 0, &pixbuffer);
}

#if 0
void PanelGadget::drawBlankFace(GraphicPainter *gc, int x, int y, int size)
{
}


void PanelGadget::drawFace(GraphicPainter *gc)
{
}
#endif

//-----------------------------------------------------------------------------
// This virtual method is called whenever the window is resized. We
// use it to make sure that the off-screen buffer is always the same
// size as the window.
// To retain the original drawing, it is first copied
// to a temporary buffer. After the main buffer has been resized and
// filled with white, the image is copied from the temporary buffer to
// the main buffer.
//
void PanelGadget::resizeEvent(QResizeEvent* event)
{
  //QPixmap save(pixbuffer);
  pixbuffer.resize(event->size());
  //pixbuffer.fill(white);
  pixbuffer.fill(Qt::gray);
  //bitBlt(&pixbuffer, 0, 0, &save);
}

void PanelGadget::mousePressEvent(QMouseEvent *e)
{
  if (e->button() == LeftButton) {
    m_bMouseLeftButtonDown = true;
  }

  if (e->button() == RightButton) {
    m_bMouseRightButtonDown = true;
  }
}

void PanelGadget::mouseReleaseEvent(QMouseEvent *e)
{
  if (e->button() == LeftButton) {
    m_bMouseLeftButtonDown = false;
  }

  if (e->button() == RightButton) {
    m_bMouseRightButtonDown = false;
  }
}

void PanelGadget::mouseMoveEvent(QMouseEvent *e)
{
  int x = rect.width()/2;
  int y = rect.height()/2;
  int dx = e->x()-x;
  int dy = e->y()-y;
  int theta = (int) (180/M_PI*atan2((double)dy, (double)dx));

  /*
  if (m_bMouseLeftButtonDown) {
    // we are dragging with the left mouse button
    setE6B(-theta-90-hotspot); //the -80 is to set the hotspot to the 60 "arrow"
    repaint(false);
  }

  if (m_bMouseRightButtonDown) {
    // we are dragging with the right mouse button
    setE6Blubber(-theta);
    repaint(false);
  }
  */

  QWidget::mouseMoveEvent(e);
}


