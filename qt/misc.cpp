//-----------------------------------------------------------------------------
// $Id: misc.cpp,v 1.21 2006/11/04 13:11:59 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786


// Include the windows headers on the windows
// platform so we can do some windows stuff.
#ifdef WIN32
  #include <windows.h>
  #include <fcntl.h>
  #include <io.h>
#endif //WIN32

#include <qstring.h>
#include <qlineedit.h>
#include <qinputdialog.h>
#include <qmessagebox.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "misc.h"
#include "ustring.h"
#include "umath.h"
#include "uvalues.h"
#include "iniFile.h"
#include "HelpWindow.h"

#ifdef WIN32
  #include "WebBrowserWindow.uih"
#endif //WIN32

#include "version.h"



//-----------------------------------------------------------------------------
// Read the Windows registry to find if there
// is a NAIPS installed
// "HKEY_LOCAL_MACHINE\SOFTWARE\Airservices Australia\NAIPS For Windows"
// "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\NICS.exe\Path"
int naips_path(char *szNaipsPath)
{
#ifdef WIN32
  BYTE buff1[256];
  DWORD buff2[256];
  LPBYTE lpData = buff1;
  LPDWORD lpcbData = buff2;
  HKEY hKey;
  HKEY hSubKey;
  RegOpenKey(HKEY_LOCAL_MACHINE, L"SOFTWARE",        &hKey);
  RegOpenKey(hKey,               L"Microsoft",       &hSubKey);
  RegOpenKey(hSubKey,            L"Windows",         &hKey);
  RegOpenKey(hKey,               L"CurrentVersion",  &hSubKey);
  RegOpenKey(hSubKey,            L"App Paths",       &hKey);
  RegOpenKey(hKey,               L"NICS.exe",        &hSubKey);
  int rv = RegQueryValueEx(
    hSubKey,
    L"Path",
    NULL,
    NULL,
    lpData,
    lpcbData
  ); // == ERROR_SUCCESS )

  if (rv == ERROR_SUCCESS)
    //unicodetoansi(szNaipsPath, (const wchar_t *) lpData);
    unicodetoansi(szNaipsPath, (const unsigned short *) lpData);
  else
    szNaipsPath = NULL;

  return rv;
#endif //WIN32
}

int naips_execute()
{
#ifdef WIN32
  char szCommand[256];
  char szNaipsPath[256];

  BYTE buff1[256];
  DWORD buff2[256];
  LPBYTE lpData = buff1;
  LPDWORD lpcbData = buff2;
  HKEY hKey;
  HKEY hSubKey;
  RegOpenKey(HKEY_LOCAL_MACHINE, L"SOFTWARE",        &hKey);
  RegOpenKey(hKey,               L"Microsoft",       &hSubKey);
  RegOpenKey(hSubKey,            L"Windows",         &hKey);
  RegOpenKey(hKey,               L"CurrentVersion",  &hSubKey);
  RegOpenKey(hSubKey,            L"App Paths",       &hKey);
  RegOpenKey(hKey,               L"NICS.exe",        &hSubKey);
  int rv = RegQueryValueEx(
    hSubKey,
    NULL,  // get the default value
    NULL,
    NULL,
    lpData,
    lpcbData
  ); // == ERROR_SUCCESS )

  if (rv == ERROR_SUCCESS) {
    //sprintf(szCommand, "%s", unicodetoansi(szNaipsPath, (const wchar_t *) lpData));
    sprintf(szCommand, "%s", unicodetoansi(szNaipsPath, (const unsigned short *) lpData));
    WinExec(szCommand, SW_SHOW | SW_SHOWNORMAL);
    return true;
  }
  else
    return false;
#endif //WIN32
}

//-----------------------------------------------------------------------------
// Fire up a browser for local html files. In this case we can use out very own
// built in browser. (It is not capable of online yet
//
void html_browse(const char *sztarget)
{
  //if (strstr(sztarget, "http:") ||
  //    strstr(sztarget, "file:") ) {

  if (0) {
  #ifdef WIN32
    // We can use the windows default browser
    //wchar_t wctarget[256];
    unsigned short wctarget[256];
    ansitounicode(wctarget, sztarget);
    //ShellExecute(NULL, L"open", wctarget, NULL, NULL, SW_SHOWNORMAL);
    ShellExecute(NULL, L"open", (LPCWSTR) wctarget, NULL, NULL, SW_SHOWNORMAL);
  #else
    CIniFile iniFile("knav.ini"); iniFile.ReadFile();

    std::string szBrowser;
    szBrowser = iniFile.GetValue("Prefs", "HtmlBrowser", "webbrowser.exe");
    char szCommand[256];
    printf("HtmlBrowser = %s\n", szBrowser.c_str());
    sprintf(szCommand, "%s %s", szBrowser.c_str(), sztarget);
    system(szCommand);
  #endif //WIN32

  }
  else {
    QString home = sztarget;
 
  #ifdef WIN32
    WebBrowserWindow *w = new WebBrowserWindow();
    if (!home.isNull()) {
      w->addressEdit->setText(home);
      w->go();
      w->setCaption(SZ_APPNAME);
      w->show();
    }
    else
      delete w;
  #else
    /*
    HelpWindow *help = new HelpWindow(home, ".", 0, "help viewer");
    help->setSourceFile(home);
    help->setCaption(SZ_APPNAME);
    help->show();
    */
  #endif //WIN32


  }
}


//-----------------------------------------------------------------------------
// Fire up the Windows "system" browser to print the specified target
//
void html_print(const char *sztarget)
{
/*
#ifdef WIN32
  // We can use the windows default browser     ERROR_FILE_NOT_FOUND    SE_ERR_SHARE
  wchar_t wctarget[256];
  ansitounicode(wctarget, sztarget);
  int rv = (int) ShellExecute(NULL, L"print", wctarget, NULL, NULL, SW_SHOWNORMAL);
#endif //WIN32
*/

  QString home = sztarget;
  HelpWindow *help = new HelpWindow(home, ".", 0, "help viewer");
  help->setCaption(SZ_APPNAME);
  help->hardcopy();
}



//-----------------------------------------------------------------------------
// Fire up the our HTML editor with the specified target
//
void html_edit(const char *sztarget)
{
  char szCommand[256];
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();

  std::string szBrowser;
  szBrowser = iniFile.GetValue("Prefs", "HtmlEditor", "notepad.exe");
  printf("HtmlEditor = %s\n", szBrowser.c_str());

  sprintf(szCommand, "%s \"%s\"", szBrowser.c_str(), sztarget);

#ifdef WIN32
  WinExec(szCommand, SW_SHOW);
#else
  system(szCommand);
#endif //WIN32
}

//-----------------------------------------------------------------------------
// Execute a G3 command, typically for exchanging data with the Garmin unit
//
// @param sFmtCmd Specify a "printf" style format string. If a %i is used
//                the function will use that field as the comm port number
//                attempting both COM1 and 2. The succesful port (if any)
//                is stored in the ini file
//
// Example of usage:
// int rv = execG3cmd("g3\\g7tow -%i -i G45T >> g3\\g3.trk");
//
int execG3cmd(const char *sFmtCmd)
{
  char szCommand[256];
  int rv;
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  int iSerialPort = iniFile.GetValueI("Prefs", "iSerialPort", 1);

  sprintf(szCommand, sFmtCmd, iSerialPort);
  rv = system(szCommand);

  if (rv != 0) {
    // try the other serial port
    if (iSerialPort == 1)
      iSerialPort = 2;
    else
      iSerialPort = 1;

    sprintf(szCommand, sFmtCmd, iSerialPort);
    rv = system(szCommand);
  }

  if (rv == 0) {
    iniFile.SetValueI("Prefs", "iSerialPort", iSerialPort);
    iniFile.WriteFile();
  }

  return rv;
}

/*
char *gen_code(char *szbuff)
{
  int a, b, c;

  a = 0;
  b = 2000;
  c = 0;

  while ((b > 999) ||
         (a*c == 0)) {
    a = randomInt(999);
    c = randomInt(999);
    b = (a^c);
  }

  sprintf(szbuff, "%03d-%03d-%03d", a, b, c);
  return szbuff;
}

bool check_code(const char *szcode)
{
  char szbuff[256];
  int a, b, c;

  strcpy(szbuff, szcode);

  char *sp = strtok(szbuff, "-");
  if (sp) a = atoi(sp);

  sp = strtok(NULL, "-");
  if (sp) b = atoi(sp);

  sp = strtok(NULL, "-");
  if (sp) c = atoi(sp);
  if (a*c == 0) return false;
  if (a == c) return false;
  if (b == (a^c))
    return true;
  else
    return false;
}
*/


//-----------------------------------------------------------------------------
// Add a console window (that will allow for printf, puts etc).
// This is only useful when the subsystem is forced as WINDOWS
// during link (/subsystem:windows).
//
//
// We do not really need this little trick. It is
// only useful when the subsystem is forced as WINDOWS
// during link. Since Qt uses main and not WinMain
// our printf's should work just fine.
//
#ifdef WIN32
void start_console()
{
  // stdout
  int hCrt;
  FILE *hf;    AllocConsole();
  hCrt = _open_osfhandle((long) GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
  hf = _fdopen(hCrt, "w" );
  *stdout = *hf;
  int i = setvbuf(stdout, NULL, _IONBF, 0);


  // stderr
  int hCrt1;
  FILE *hf1;    AllocConsole();
  hCrt1 = _open_osfhandle((long) GetStdHandle(STD_ERROR_HANDLE), _O_TEXT);
  hf1 = _fdopen(hCrt1, "w" );
  *stderr = *hf1;
  int j = setvbuf(stderr, NULL, _IONBF, 0);
}
#endif //WIN32



bool check_code(const char *szcode, const char *name)
{
  char szbuff[256];
  int a, b, c;

  int aa = 1;
  for (size_t i = 0; i < strlen(name); i++) {
    aa *= name[i];
    aa %= 998;
    aa += 1;
  }

  strcpy(szbuff, szcode);

  char *sp = strtok(szbuff, "-");
  if (sp) a = atoi(sp);
  if (a != aa) return false;

  sp = strtok(NULL, "-");
  if (sp) b = atoi(sp);

  sp = strtok(NULL, "-");
  if (sp) c = atoi(sp);

  if (a*c == 0) return false;
  if (a == c) return false;
  if (b == (a^c))
    return true;
  else
    return false;
}


bool g_demo_version = false;

void set_demo_version()
{
  /*
  QMessageBox::information(NULL, SZ_PRGNAME,
                           "      DEMONSTRATION MODE\n\n"
                           "Some program functionality is disabled."
                           );
  */
  g_demo_version = true;
}

bool check_demo_version()
{
  if (g_demo_version == true) {
    // DEMO VERSION
    QMessageBox::information(NULL, SZ_PRGNAME,
                             "This feature is unfortunately not available in "
                             "the demonstration version");
    // DEMO VERSION
    return true;
  }
  else
    return false;
}


//-----------------------------------------------------------------------------
// Check for a serial number and prompt for username and serial
// number if none is found
//
bool check_serial_num()
{
  bool ok = false;
  bool check = false;
  char szSerialNum[256];
  char szOwner[256];

  //
  // Read the ini file for some last used settings.
  // Any of these can be overridden by paramaters
  // specified on the command line
  //
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iniFile.GetValueS(szSerialNum, "Prefs", "SerialNum", "");
  iniFile.GetValueS(szOwner, "Prefs", "Owner", "");

  if (check_code(szSerialNum, szOwner)) {
    g_demo_version = false;
    return true;
  }

  int rv = QMessageBox::information(NULL, SZ_PRGNAME,
                           "      DEMONSTRATION MODE\n\n"
                           "Some program functionality is disabled.",
                           "Enter registration details",
                           "Continue with demo version");
  if (rv == 1) {
    g_demo_version = true;
    return false;
  }


  //
  // Prompt for a owner name
  //
  QString text;
  text = QInputDialog::getText(SZ_APPNAME,
                               "Please enter your name\n",
                               QLineEdit::Normal,
                               "", &ok, NULL );
  if (ok && !text.isEmpty()) {
    strcpy(szOwner, text.latin1());
    iniFile.SetValue("Prefs", "Owner", szOwner);
    iniFile.WriteFile();
  }
  else {
    // user entered nothing or pressed cancel
    check = false;
  }

  //
  // Prompt for a serial number
  //
  text = QInputDialog::getText(SZ_APPNAME,
                               "Please enter your serial number - format: nnn-nnn-nnn\n",
                               QLineEdit::Normal,
                               "111-222-333", &ok, NULL );
  if (ok && !text.isEmpty()) {
    check = check_code(text.latin1(), szOwner);// user entered something and pressed ok
    strcpy(szSerialNum, text.latin1());
  }
  else {
    // user entered nothing or pressed cancel
    check = false;
  }

  if (check) {
    g_demo_version = false;
    QMessageBox::information(NULL, SZ_PRGNAME,
                             "PRODUCT REGISTERED\n\n"
                             "Full program functionality is available."
                             );

    iniFile.SetValue("Prefs", "SerialNum", szSerialNum);
    iniFile.WriteFile();
  }
  else {
    g_demo_version = true;
    QMessageBox::information(NULL, SZ_PRGNAME,
                             "      DEMONSTRATION MODE\n\n"
                             "Some program functionality is disabled."
                             );
  }

  return check;
}

//-----------------------------------------------------------------------------
// Display the disclaimer and get confirmation
//
bool check_disclaimer()
{
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  int iDisclaimer = iniFile.GetValueI("Prefs", "Disclaimer", 1);
  if (iDisclaimer == 0)
    return true;

  int rv = QMessageBox::information(NULL, SZ_PRGNAME,
    "This program is supplied for educational and entertainment puposes \n"
    "only, without representation or warranty of any kind. \n\n"
 
    "The author therefore assume no responsibility and shall have no \n"
    "liability, consequential or otherwise, of any kind arising from the \n"
    "use of this program or any part thereof.\n\n"
 
    "Click the Reject button if you DISAGREE.\n\n"
 
    "Click the Accept button if you UNDERSTAND and ACCEPT\n"
    "these conditions.\n\n",
    "Accept", "Reject",
    0, 1);
 
  switch (rv) {
    case 0:  // accept
      return true;
      break;
    case 1:  // reject
    default: // just for sanity
      break;
    case 2:  // abandon
      break;
  }
  return false;
}




#define MIN(x,y) (x < y ? x : y)
#define MAX(x,y) (x > y ? x : y)
#define INSIDE 0
#define OUTSIDE 1

typedef struct {
   double x,y;
} Point;

int InsidePolygon(Point *polygon,int N,Point p)
{
  int counter = 0;
  int i;
  double xinters;
  Point p1,p2;

  p1 = polygon[0];
  for (i=1;i<=N;i++) {
    p2 = polygon[i % N];
    if (p.y > MIN(p1.y,p2.y)) {
      if (p.y <= MAX(p1.y,p2.y)) {
        if (p.x <= MAX(p1.x,p2.x)) {
          if (p1.y != p2.y) {
            xinters = (p.y-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)+p1.x;
            if (p1.x == p2.x || p.x <= xinters)
              counter++;
          }
        }
      }
    }
    p1 = p2;
  }

  if (counter % 2 == 0)
    return(OUTSIDE);
  else
    return(INSIDE);
}



//-----------------------------------------------------------------------------
// Utility function to convert a lat or long string
// into a double value.
// The format for the input string (sc) is as follows:
// hddmm  for lat
// hdddmm for long
//
// NB - Duplicate in qt/MapWnd
//
void convLL(double *fc, char *sc)
{
  double sign;
  char st[256];
  char *sp;
  double d, m;//, s;

  sp = sc;
  if ((sp[0] == 'S') || (sp[0] == 'E'))
    sign = 1;
  else
    sign = -1;

  if ((sp[0] == 'N') || (sp[0] == 'S')) {
    d = atof(strcut(st, sp, 1, 2));
    sp += 4;
  }
  else {
    d = atof(strcut(st, sp, 1, 3));
    sp += 5;
  }
  //m = atof(strcut(st, sp, 0, 2));
  m = atof(sp);

  *fc = sign * (d + (m/60));
}
       

//
// Calculate a approximate track (heading)
//
int calc_hdg(double lat1, double lon1,
             double lat2, double lon2,
             long delta_sec)
{
  int hdg;
  float d;
  float depy = lat2 - lat1;
  float depx = lon2 - lon1;

  d = sqrt(sqr(depy) + sqr(depx));
  if (d < 0.001) return 999;

  // Note: depx and depy - CORRECT - we measure angles from north clockwise
  hdg = (atan2(depx, depy)*180.0/M_PI);
  while (hdg < 0) hdg += 360;
  hdg = (hdg + 360) % 360;
  return hdg;

  /* - no atan2 support
  if (depy < 0)
    hdg = (atan(depx/depy)*180.0/M_PI) + 180.0;
  else
    hdg = (atan(depx/depy)*180.0/M_PI);
  */
}


int runavg_hdg(int a, int nr)
{
  static int abuff[1024];
  int i;
  int isum = 0;

  if (nr > 1000) nr = 1000;

  for (i = nr; i > 0; i--) abuff[i] = abuff[i-1];
  abuff[0] = a;
  for (i = 0; i < nr; i++) isum += abuff[i];
  return isum / nr;
}


int avg_hdg(double lat, double lon, long sec)
{
  static double lat1 = 0;
  static double lon1 = 0;
  static long sec1 = 0;
  int hdg = 0;

  if (lat1*lon1 != 0)
    hdg = calc_hdg(lat1, lon1, lat, lon, sec-sec1+1); // +1 sec to help with div 0

  lat1 = lat;
  lon1 = lon;
  sec1 = sec;

  //int avhdg = runavg_hdg(hdg, 10);
  //return avhdg;
  return hdg;
}


//
// Calculate a approximate speed
//
double calc_speed(double lat1, double lon1,
                  double lat2, double lon2,
                  long delta_sec)
{
  double dSpeed;
  double R = sqrt (sqr(lat2 - lat1) + sqr(lon2 - lon1));

  dSpeed =  (R*60*3600 / delta_sec); // * 1.85; for km
  if ((dSpeed < 0) || (dSpeed > 900)) dSpeed = 0;

  return dSpeed;
}


int runavg_speed(int a, int nr)
{
  static int abuff[1024];
  int i;
  int isum = 0;

  if (nr > 1000) nr = 1000;

  for (i = nr; i > 0; i--) abuff[i] = abuff[i-1];
  abuff[0] = a;
  for (i = 0; i < nr; i++) isum += abuff[i];
  return isum / nr;
}


double avg_speed(double lat, double lon, long sec)
{
  static double lat1 = 0;
  static double lon1 = 0;
  static long sec1 = 0;
  double speed = 0;

  if (lat1*lon1 != 0)
    speed = calc_speed(lat, lon, lat1, lon1, sec-sec1+1);  // +1 sec to help with div 0

  lat1 = lat;
  lon1 = lon;
  sec1 = sec;

  //int avspeed = runavg_speed(speed, 10);
  //return avspeed;
  return speed;
}


//
// Calculate a approximate Rate of Turn
//
double calc_rot(int hdg1,
                int hdg2,
                long delta_sec)
{
  // tas/10+7 = bank at 3 deg/sec rot (rate 1)
  // typical GA AC rate 1 ~ 20 deg
 
  double iRot = (double)(hdg2 - hdg1)/delta_sec;
  return iRot;
}


double avg_rot(int hdg, long sec)
{
  static int hdg1 = 0;
  static long sec1 = 0;

  double rot = calc_rot(hdg1, hdg, sec-sec1+1);  // +1 sec to help with div 0

  hdg1 = hdg;
  sec1 = sec;

  return rot;
}


//
// Calculate a approximate bank
//
double calc_bank(int hdg1,
                 int hdg2,
                 long delta_sec)
{
  // tas/10+7 = bank at 3 deg/sec rot (rate 1)
  // typical GA AC rate 1 ~ 20 deg
 
  double iRot = (double)(hdg1 - hdg2)/delta_sec;
  double iBank = (double) (iRot / 3.0) * 20.0;

  return iBank;
}


double avg_bank(int hdg, long sec)
{
  static int hdg1 = 0;
  static long sec1 = 0;

  int bank = calc_bank(hdg1, hdg, sec-sec1+1);   // +1 sec to help with div 0

  hdg1 = hdg;
  sec1 = sec;

  //int avspeed = runavg_speed(speed, 10);
  //return avspeed;
  return bank;
}




//
// Calculate a approximate Rate of Climb fpm
//
double calc_roc(int alt1,
                int alt2,
                long delta_sec)
{
  double iroc = (double)(alt2 - alt1)*60/delta_sec;
  return iroc;
}


double avg_roc(int alt, long sec)
{
  static int alt1 = 0;
  static long sec1 = 0;

  double roc = calc_roc(alt1, alt, sec-sec1+1);  // +1 sec to help with div 0

  alt1 = alt;
  sec1 = sec;

  return roc;
}

bool isInsideRect(double top, double left, double bottom, double right, double lat, double lon)
{
  if ( (lat < top) && (lat > bottom) &&
       (lon < right) && (lon > left) ) {

    return true;
  }
  else
    return false;
}
