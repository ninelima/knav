//-----------------------------------------------------------------------------
// $Id: tabdialog.cpp,v 1.17 2006/04/25 11:58:49 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786

#ifdef WIN32
  //
  // Include the windows headers on the windows
  // platform so we can do some windows stuff.
  //
  #include <windows.h>
  #include <direct.h>

#endif //WIN32

#include "tabdialog.h"
#include "tabdialog.moc"


#include <qvbox.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qdatetime.h>
#include <qbuttongroup.h>
#include <qcheckbox.h>
#include <qapplication.h>
#include <qcombobox.h>
#include <qpushbutton.h>
#include <qdir.h>
#include <qmessagebox.h>

#include "iniFile.h"
#include "MapWnd.h"
#include "version.h"

//
// "standard" template includes
//
#include <string>

int g_MouseWheel;
extern CMapWnd g_MapWnd;

TabDialog::TabDialog(QWidget *parent, const char *name, bool modal, WFlags f)
      : QTabDialog(parent, name, modal, f)
{
  setSizeGripEnabled(true);

  setupTab1();
  setupTab2();
  setupTab3();

  // signals and slots connections
  //connect(this, SIGNAL(applyButtonPressed()), qApp, SLOT(quit()) );
  //connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
  //connect(buttonEdit, SIGNAL(clicked()), this, SLOT(edit()));
}

void TabDialog::setupTab1()
{
  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  int iStyle       = iniFile.GetValueI("Prefs", "LookAndFeel", -1);
  int iBrowser     = iniFile.GetValueI("Prefs", "iBrowser", 0);
  int iMouseWheel  = iniFile.GetValueI("Prefs", "MouseWheel", -1);
  int iLatLonStyle = iniFile.GetValueI("Spatial", "bDisplayDMS", 1);
  int iAirPenWide  = iniFile.GetValueI("Prefs", "bAirPenWide", 0);
  int iPaintCache  = iniFile.GetValueI("Prefs", "iPaintCache", 0); 

  QVBox *tab1 = new QVBox(this);
  tab1->setMargin(5);

  (void) new QLabel("Decoration Style:", tab1);
  QComboBox *cbStyle = new QComboBox(false, tab1, "Style");
  cbStyle->insertItem("0 - Motif");
  cbStyle->insertItem("1 - CDE");
  cbStyle->insertItem("2 - Windows");
  cbStyle->insertItem("3 - Platinum");
  cbStyle->insertItem("4 - SGI");
  cbStyle->setCurrentItem(iStyle);
  connect(cbStyle, SIGNAL(activated(int)), this, SLOT(cbStyle_activated(int)));

  /*
  (void) new QLabel("Browser:", tab1);
  QComboBox *cbBrowser = new QComboBox(false, tab1, "LatLonStyle");
  cbBrowser->insertItem("0 - Built In");
  cbBrowser->insertItem("1 - System default");
  cbBrowser->insertItem("2 - User Specified");
  cbBrowser->setCurrentItem(iBrowser);
  connect(cbBrowser, SIGNAL(activated(int)), this, SLOT(cbBrowser_activated(int)));
  */

  (void) new QLabel("Lat/Lon Style:", tab1);
  QComboBox *cbLatLonStyle = new QComboBox(false, tab1, "LatLonStyle");
  cbLatLonStyle->insertItem("0 - Decimal deg");
  cbLatLonStyle->insertItem("1 - DDD MM SS H");
  cbLatLonStyle->setCurrentItem(iLatLonStyle);
  connect(cbLatLonStyle, SIGNAL(activated(int)), this, SLOT(cbLatLonStyle_activated(int)));

  (void) new QLabel("Airspace Line Style:", tab1);
  QComboBox *cbAirspaceLineStyle = new QComboBox(false, tab1, "LatLonStyle");
  cbAirspaceLineStyle->insertItem("0 - Thin");
  cbAirspaceLineStyle->insertItem("1 - Wide");
  cbAirspaceLineStyle->setCurrentItem(iAirPenWide);
  connect(cbAirspaceLineStyle, SIGNAL(activated(int)), this, SLOT(cbAirspaceLineStyle_activated(int)));

  (void) new QLabel("MouseWheel:", tab1);
  QComboBox *cbMouseWheel = new QComboBox(false, tab1, "MouseWheel");
  cbMouseWheel->insertItem("0 - Disabled");
  cbMouseWheel->insertItem("1 - Zoom");
  cbMouseWheel->insertItem("2 - Pan & Zoom");
  cbMouseWheel->setCurrentItem(iMouseWheel);
  connect(cbMouseWheel, SIGNAL(activated(int)), this, SLOT(cbMouseWheel_activated(int)));

  (void) new QLabel("Drawing:", tab1);
  QComboBox *cbDrawingStyle = new QComboBox(false, tab1, "DrawingStyle");
  cbDrawingStyle->insertItem("0 - Direct");
  cbDrawingStyle->insertItem("1 - Bufferd");
  cbDrawingStyle->setCurrentItem(iPaintCache);
  connect(cbDrawingStyle, SIGNAL(activated(int)), this, SLOT(cbDrawingStyle_activated(int)));

  addTab(tab1, "General");
}

void TabDialog::setupTab2()
{
  QVBox *tab2 = new QVBox(this);
  tab2->setMargin(5);

  QButtonGroup *bg = new QButtonGroup(1, QGroupBox::Horizontal, "Scripts/Permissions", tab2);
  QPushButton *but3 = new QPushButton("Startup script", bg);
  QPushButton *but4 = new QPushButton("Empty", bg);
  QPushButton *but5 = new QPushButton("Empty", bg);

  char szCheck[256];
  const char *szKey = "verysecretserialnumber";

  char szSerialNum[256];
  char szOwner[256];
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iniFile.GetValueS(szSerialNum, "Prefs", "SerialNum", "");
  iniFile.GetValueS(szOwner, "Prefs", "Owner", "Owner");

  //-------------------
  for (int i = 0; i < 9; i++) {
    char a = ((szOwner[i] ^ szKey[i]) & 0x1f) + 0x40;
    szCheck[i] = a;
  }
  //-------------------

  QButtonGroup *bg2 = new QButtonGroup(2, QGroupBox::Horizontal, "Owner", tab2);
 
  (void)new QLabel("Owner", bg2);
  //QLineEdit *owner = new QLineEdit("Owner", bg2);
  QLabel *owner = new QLabel("Owner", bg2);
  owner->setFrameStyle(QFrame::Panel | QFrame::Sunken);
  owner->setText(szOwner);

  (void)new QLabel("Serial Number", bg2);
  QLabel *serial = new QLabel(fileinfo.group(), bg2);
  serial->setFrameStyle(QFrame::Panel | QFrame::Sunken);
  serial->setText(szSerialNum);

  addTab(tab2, "Configuration");
  connect(but3, SIGNAL(clicked()), this, SLOT(but3_clicked()));
}


//-----------------------------------------------------------------------------
// The user can choose which vector sets to display
//
void TabDialog::setupTab3()
{
  QHBox *tab3 = new QHBox(this);
  tab3->setMargin(3);
  tab3->setSpacing(3);

  std::string sPath;
  //char szPath[256];
  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();

  //-----------------------------------
  QVBox *vb6 = new QVBox(tab3);
    (void)new QLabel("Available:", vb6);
    listAvailable = new QListBox(vb6);

  //-----------------------------------
  QVBox *tab5 = new QVBox(tab3);
    QPushButton *but2 = new QPushButton(">", tab5);
    QPushButton *but1 = new QPushButton("<", tab5);
    but1->setMaximumWidth(30);
    but2->setMaximumWidth(30);

  //-----------------------------------
  QVBox *vb4 = new QVBox(tab3);
    (void)new QLabel("Selected:", vb4);
    listSelected = new QListBox(vb4);

  // Now that we have a available *and* a selected
  // control  populate them
  char szSection[256];
  int n;
  // Static Vector sets
  for (n = 1; n < NR_BUF; n++) {
    sprintf(szSection, "StaticVector_%d", n);
    sPath = iniFile.GetValue(szSection, "FileName", "");
    QString s2(sPath.c_str());

    int iActive = iniFile.GetValueI(szSection, "Active", 0);
    if (!s2.isEmpty()) {
      if (!iActive) {
        // this entry is currently disabled
        listAvailable->insertItem(QString().sprintf("%02d: %s", n, sPath.c_str()));
      }
      else {
        listSelected->insertItem(QString().sprintf("%02d: %s", n, sPath.c_str()));
      }
    }
  }

  // Shpfile Vector sets
  for (n = 1; n < NR_BUF; n++) {
    sprintf(szSection, "Esri_%d", n);
    sPath = iniFile.GetValue(szSection, "FileName", "");
    QString s2(sPath.c_str());

    int iActive = iniFile.GetValueI(szSection, "Active", 0);
    if (!s2.isEmpty()) {
      if (!iActive) {
        // this entry is currently disabled
        listAvailable->insertItem(QString().sprintf("%02d: %s", NR_BUF + n, sPath.c_str()));
      }
      else {
        listSelected->insertItem(QString().sprintf("%02d: %s", NR_BUF + n, sPath.c_str()));
      }
    }
  }


  // re-sort both lists
  listAvailable->sort();
  listSelected->sort();

  addTab(tab3, "Vector Data");
  connect(but1, SIGNAL(clicked()), this, SLOT(but1_clicked()));
  connect(but2, SIGNAL(clicked()), this, SLOT(but2_clicked()));
}



void TabDialog::staticVector()
{
  /*
  std::string sPath;
  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();

  for (int i=0; i < listSelected->count(); i++) {
      char szSection[256];
      for (int n = 1; n < NR_BUF; n++) {
        sprintf(szSection, "StaticVector_%d", n);
        sPath = iniFile.GetValue(szSection, "FileName", "~~~");
        QString s2(sPath.c_str());

        if (s2 == " ") {
          // empty slot found
          //sPath = iniFile.GetValue(szSection, "FileName", "~~~");
          iniFile.SetValue(szSection, "FileName", std::string(listSelected->item(i)->text()));
          iniFile.SetValueI(szSection, "magTH", 1);
 
          break;
        }
      }
  }

  iniFile.WriteFile();
  */
}

void TabDialog::but3_clicked()
{
  #ifdef WIN32
    WinExec("notepad.exe knav.ini", SW_SHOW);
  #else
    int rv = system("notepad.exe knav.ini");
  #endif //WIN32
}


//-----------------------------------------------------------------------------
// Move the selected item from "available" to "selected"
// right to left  ->
//
void TabDialog::but2_clicked()
{
  printf("TabDialog::but1_clicked\n");
  if (listAvailable->currentItem() == -1)
    return;

  puts(listAvailable->currentText());
 
  // add the item to the selected list
  listSelected->insertItem(listAvailable->currentText());

  // add the same item to the knav.ini file
  std::string sPath;
  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();

  //
  // Find the filename in the static vector list
  //
  int n;
  bool bFound = false;
  char szSection[256];
  for (n = 1; n < NR_BUF; n++) {
    sprintf(szSection, "StaticVector_%d", n);
    sPath = iniFile.GetValue(szSection, "FileName", "~~~~");
    QString s2(sPath.c_str());
    if (s2.isEmpty())
      continue;

    if (listAvailable->currentText().contains(s2)) {
      // found
      iniFile.SetValueI(szSection, "Active", 1);
      break;
    }
  }

  //
  // Find the filename in the static esri list
  //
  for (n = 1; n < NR_BUF; n++) {
    sprintf(szSection, "Esri_%d", n);
    sPath = iniFile.GetValue(szSection, "FileName", "~~~~");
    QString s2(sPath.c_str());
    if (s2.isEmpty())
      continue;

    if (listAvailable->currentText().contains(s2)) {
      // found
      iniFile.SetValueI(szSection, "Active", 1);
      break;
    }
  }
  
  iniFile.WriteFile();

  // remove the item from the avaliable list
  listAvailable->removeItem(listAvailable->currentItem());

  // re-sort both lists
  listAvailable->sort();
  listSelected->sort();

  g_MapWnd.UnLoadData();
  g_MapWnd.LoadData();
}

//-----------------------------------------------------------------------------
// Move the selected item from "selected" to "available"
// left to right   <-
//
void TabDialog::but1_clicked()
{
  printf("TabDialog::but2_clicked\n");
  if (listSelected->currentItem() == -1)
    return;

  puts(listSelected->currentText());
  // add the item to the available list
  listAvailable->insertItem(listSelected->currentText());

  // add the same item to the knav.ini file
  std::string sPath;
  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();

  //
  // Find the filename in the static vector list
  //
  int n;
  bool bFound = false;
  char szSection[256];
  for (n = 1; n < NR_BUF; n++) {
    sprintf(szSection, "StaticVector_%d", n);
    sPath = iniFile.GetValue(szSection, "FileName", "~~~~");
 
    QString s1(listSelected->currentText());
    QString s2(sPath.c_str());
    if (s2.isEmpty())
      continue;

    s1.latin1();
    s2.latin1();
    if (s1.contains(s2)) {
      // empty slot found
      iniFile.SetValueI(szSection, "Active", 0);
      break;
    }
  }

  //
  // Find the filename in the esri vector list
  //
  for (n = 1; n < NR_BUF; n++) {
    sprintf(szSection, "Esri_%d", n);
    sPath = iniFile.GetValue(szSection, "FileName", "~~~~");
 
    QString s1(listSelected->currentText());
    QString s2(sPath.c_str());
    if (s2.isEmpty())
      continue;

    s1.latin1();
    s2.latin1();
    if (s1.contains(s2)) {
      // empty slot found
      iniFile.SetValueI(szSection, "Active", 0); 
      break;
    }
  }

  iniFile.WriteFile();

  // remove the item from the selected list
  listSelected->removeItem(listSelected->currentItem());

  // re-sort both lists
  listAvailable->sort();
  listSelected->sort();

  g_MapWnd.UnLoadData();
  g_MapWnd.LoadData();
}

//-----------------------------------------------------------------------------
// Respond to changes in the style drop down combo
//
void TabDialog::cbStyle_activated(int sel)
{
  printf("TabDialog::cbStyle_change()\n");
  printf("sel = %d\n", sel);

  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  iniFile.SetValueI("Prefs", "LookAndFeel", sel);
  iniFile.WriteFile();

  QMessageBox::information(this, SZ_PRGNAME,
                           "Style will become active next \n "
                           "time the application is started");
}

//-----------------------------------------------------------------------------
// Respond to changes in the browser drop down combo
//
/*
void TabDialog::cbBrowser_activated(int sel)
{
  printf("TabDialog::cbBrowser_activated\n");
  printf("sel = %d\n", sel);

  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  iniFile.SetValueI("Prefs", "iBrowser", sel);
  iniFile.WriteFile();
}
*/


//-----------------------------------------------------------------------------
// Respond to changes in the MouseWheel drop down combo
//
void TabDialog::cbMouseWheel_activated(int sel)
{
  printf("TabDialog::cbMouseWheel_activated()\n");
  printf("sel = %d\n", sel);

  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  iniFile.SetValueI("Prefs", "MouseWheel", sel);
  iniFile.WriteFile();

  g_MouseWheel = sel;
}


//-----------------------------------------------------------------------------
// Respond to changes in the LatLonStyle drop down combo
//
void TabDialog::cbLatLonStyle_activated(int sel)
{
  printf("TabDialog::cbLatLonStyle_activated()\n");
  printf("sel = %d\n", sel);

  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  iniFile.SetValueI("Spatial", "bDisplayDMS", sel);
  iniFile.WriteFile();
}

//-----------------------------------------------------------------------------
// Respond to changes in the cbAirspaceLineStyle drop down combo
//
void TabDialog::cbAirspaceLineStyle_activated(int sel)
{
  printf("TabDialog::cbAirspaceLineStyle_activated()\n");
  printf("sel = %d\n", sel);

  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  iniFile.SetValueI("Prefs", "bAirPenWide", sel);
  iniFile.WriteFile();
}

//-----------------------------------------------------------------------------
// Respond to changes in the cbDrawingStyle drop down combo
//
void TabDialog::cbDrawingStyle_activated(int sel)
{
  printf("TabDialog::cbAirspaceLineStyle_activated()\n");
  printf("sel = %d\n", sel);

  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  iniFile.SetValueI("Prefs", "iPaintCache", sel);
  iniFile.WriteFile();
}

