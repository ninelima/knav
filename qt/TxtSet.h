#ifndef __TXTSET_H
#define __TXTSET_H

#include "DelimRecordSet.h"

// -> #define _NO_TXT_CACHE_
//#define _NO_TXT_CACHE_


#ifdef _NO_TXT_CACHE_

#pragma message ("fix cp for no cache")
 
//-----------------------------------------------------------------------------
// CTxtSet Delimited recordset
//
class CTxtSet : public CDelimRecordSet
{
public:
  // long cp;// = 0;
  CTxtSet();
  ~CTxtSet();

  void Bind();
 
  QString m_NAME;
  QString m_COUNTRY;
  double  m_rLat;
  double  m_rLon;
  long    m_TYPE;
};


#else //_NO_TXT_CACHE_
    
//-----------------------------------------------------------------------------
         
#define TXT_TOTAL 124000

class QString;

class TxtRec
{
  public:          
    QString m_NAME;
    //QString m_COUNTRY;
    double  m_rLat;
    double  m_rLon;
    long    m_TYPE;
    long    m_nFields;
};


class CTxtSet : public CDelimRecordSet
{

public:
  // We can use vectors, lists or whatever
  // but speed is king. A simple static array
  // is the best.
  TxtRec tbl[TXT_TOTAL+1];
  int num;

private:
  int cp;

public:
  QString m_NAME;
  QString m_COUNTRY;
  double  m_rLat;
  double  m_rLon;
  long    m_TYPE;
  long    m_nFields;

  CTxtSet();
  ~CTxtSet();

  void TxtSetInit();
  bool Open(const char *sFileName);
  bool MoveFirst();
  bool MoveNext();
  bool Move(int idx);
  bool IsEOF();
  void Clear();
  void Bind();
  void Store();
  TxtRec GetRecord();

  long GetCP();

};

#endif // _NO_TXT_CACHE_
#endif // __WPTSET_H
