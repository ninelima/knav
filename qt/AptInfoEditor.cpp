
#include "AptInfoEditor.h"
#include "AptInfoEditor.moc"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qlineedit.h>
#include <qlabel.h>
#include <qmultilineedit.h>
#include <qlistbox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

#include <qfile.h>
#include <qtextstream.h>
#include <qfiledialog.h>
#include <qregexp.h>
#include <qmessagebox.h>

#include "version.h"
                               
//
//  Constructs a AptInfoEditorForm as a child of 'parent', with the
//  name 'name' and widget flags set to 'f'.
//
//  The dialog will by default be modeless, unless you set 'modal' to
//  TRUE to construct a modal dialog.
// 
AptInfoEditorForm::AptInfoEditorForm( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
      setName( "Form1" );

    setSizeGripEnabled( TRUE );

    Form1Layout = new QVBoxLayout( this, 11, 6, "Form1Layout");
    Layout33 = new QVBoxLayout( 0, 0, 6, "Layout33");
    Layout6 = new QHBoxLayout( 0, 0, 6, "Layout6");
    Layout4 = new QGridLayout( 0, 1, 1, 0, 6, "Layout4");
    Layout8 = new QHBoxLayout( 0, 0, 6, "Layout8");
    Layout2 = new QVBoxLayout( 0, 0, 6, "Layout2"); 

    ICAOTextLabel = new QLabel( this, "ICAOTextLabel" );
    CountryTextLabel = new QLabel( this, "CountryTextLabel" );
    RegionTextLabel = new QLabel( this, "RegionTextLabel" );
    NameTextLabel = new QLabel( this, "NameTextLabel" );

    ICAOLineEdit = new QLineEdit( this, "ICAOLineEdit" );
    NameLineEdit = new QLineEdit( this, "NameLineEdit" );
    RegionLineEdit = new QLineEdit( this, "RegionLineEdit" );
    CountryLineEdit = new QLineEdit( this, "CountryLineEdit" );

    Layout4->addWidget( ICAOTextLabel, 0, 0);
    Layout4->addWidget( ICAOLineEdit,  0, 1);
    Layout4->addWidget( NameTextLabel, 0, 2);
    Layout4->addWidget( NameLineEdit,  0, 3);
    Layout4->addWidget( CountryTextLabel, 1, 0);
    Layout4->addWidget( CountryLineEdit,  1, 1);
    Layout4->addWidget( RegionTextLabel, 1, 2);
    Layout4->addWidget( RegionLineEdit,  1, 3);

    Layout6->addLayout( Layout4 );
    Layout33->addLayout( Layout6 );

    GeographicTextLabel = new QLabel( this, "GeographicTextLabel" );
    FrequenciesTextLabel = new QLabel( this, "FrequenciesTextLabel" );
    RunwaysTextLabel = new QLabel( this, "RunwaysTextLabel" );
    GeographicMultiLineEdit = new QMultiLineEdit( this, "GeographicMultiLineEdit" );
    FreqMultiLineEdit = new QMultiLineEdit( this, "FreqMultiLineEdit" );
    RwyMultiLineEdit = new QMultiLineEdit( this, "RwyMultiLineEdit" );

    Layout2->addWidget( GeographicTextLabel );
    Layout2->addWidget( GeographicMultiLineEdit );
    Layout2->addWidget( FrequenciesTextLabel );
    Layout2->addWidget( FreqMultiLineEdit );
    Layout2->addWidget( RunwaysTextLabel );
    Layout2->addWidget( RwyMultiLineEdit );
    Layout8->addLayout( Layout2 );

    Layout1 = new QVBoxLayout( 0, 0, 6, "Layout1"); 

    CommentsTextLabel = new QLabel( this, "CommentsTextLabel" );
    CommentMultiLineEdit = new QMultiLineEdit( this, "CommentMultiLineEdit" );
    FuelTextLabel = new QLabel( this, "FuelTextLabel" );
    FuelMultiLineEdit = new QMultiLineEdit( this, "FuelMultiLineEdit" );
    OperatorTextLabel = new QLabel( this, "OperatorTextLabel" );
    OprMultiLineEdit = new QMultiLineEdit( this, "OprMultiLineEdit" );

    Layout1->addWidget( CommentsTextLabel );
    Layout1->addWidget( CommentMultiLineEdit );
    Layout1->addWidget( FuelTextLabel );
    Layout1->addWidget( FuelMultiLineEdit );
    Layout1->addWidget( OperatorTextLabel );
    Layout1->addWidget( OprMultiLineEdit );
    Layout8->addLayout( Layout1 );
    Layout33->addLayout( Layout8 );

    Layout32 = new QHBoxLayout( 0, 0, 6, "Layout32"); 
    Layout12 = new QVBoxLayout( 0, 0, 6, "Layout12"); 

    ImageTextLabel = new QLabel( this, "ImageTextLabel" );
    Layout12->addWidget( ImageTextLabel );

    ImgListBox = new QListBox( this, "ImgListBox" );
    Layout12->addWidget( ImgListBox );
    Layout32->addLayout( Layout12 );

    Layout31 = new QVBoxLayout( 0, 0, 6, "Layout31"); 

    AddButton = new QPushButton( this, "AddButton" );
    Layout31->addWidget( AddButton );

    RemoveButton = new QPushButton( this, "RemoveButton" );
    Layout31->addWidget( RemoveButton );
    Spacer4 = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    Layout31->addItem( Spacer4 );

    CancelButton = new QPushButton( this, "CancelButton" );
    OKButton     = new QPushButton( this, "OKButton" );
    Layout31->addWidget( OKButton );
    Layout31->addWidget( CancelButton );

    Layout32->addLayout( Layout31 );
    Layout33->addLayout( Layout32 );
    Form1Layout->addLayout( Layout33 );
    languageChange();
    resize( QSize(550, 550).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );

    // tab order
    setTabOrder( ICAOLineEdit, NameLineEdit );
    setTabOrder( NameLineEdit, CountryLineEdit );
    setTabOrder( CountryLineEdit, GeographicMultiLineEdit );
    setTabOrder( GeographicMultiLineEdit, CommentMultiLineEdit );
    setTabOrder( CommentMultiLineEdit, FreqMultiLineEdit );
    setTabOrder( FreqMultiLineEdit, FuelMultiLineEdit );
    setTabOrder( FuelMultiLineEdit, RwyMultiLineEdit );
    setTabOrder( RwyMultiLineEdit, OprMultiLineEdit );
    setTabOrder( OprMultiLineEdit, AddButton );
    setTabOrder( AddButton, RemoveButton );
    setTabOrder( RemoveButton, OKButton );
    setTabOrder( OKButton, ImgListBox );

    // signals and slots connections
    //connect(OKButton,     SIGNAL( clicked() ), this, SLOT( reject() ) );

    connect(AddButton, SIGNAL(clicked()), this, SLOT(add_clicked()));
    connect(RemoveButton, SIGNAL(clicked()), this, SLOT(remove_clicked()));

    connect(OKButton, SIGNAL(clicked()), this, SLOT(ok_clicked()));
    connect(CancelButton, SIGNAL( clicked() ), this, SLOT( reject() ) );

}

//
//  Destroys the object and frees any allocated resources
// 
AptInfoEditorForm::~AptInfoEditorForm()
{
  // no need to delete child widgets, Qt does it all for us
}


/*
Function stripHTML(strHTML)
'Strips the HTML tags from strHTML

  Dim objRegExp, strOutput
  Set objRegExp = New Regexp

  objRegExp.IgnoreCase = True
  objRegExp.Global = True
  objRegExp.Pattern = "<(.|\n)+?>"

  'Replace all HTML tag matches with the empty string
  strOutput = objRegExp.Replace(strHTML, "")
  
  'Replace all < and > with &lt; and &gt;
  strOutput = Replace(strOutput, "<", "&lt;")
  strOutput = Replace(strOutput, ">", "&gt;")
  
  stripHTML = strOutput    'Return the value of strOutput

  Set objRegExp = Nothing
End Function
*/


//
// Remove all html tags from the string
//
QString AptInfoEditorForm::stripHTMLTags(QString htmlStr)
{
  QString strOutput(htmlStr);
  QRegExp objRegExp("<(.|\n)+?>", false, true);

  // Replace all HTML tag matches with the empty string
  //strOutput.replace(objRegExp, "");
  strOutput.replace(QRegExp("<[^>]*>"), "");
  // Remove the newline at the end
  //strOutput.replace(QRegExp("$\n"), "");

  // Strip off any trailing newline
  int len = strOutput.length();
  QChar c = strOutput.at(len);   
  if (c == '\n') {
    strOutput.remove(len, 1);
  }
  c = strOutput.at(len-1);   
  if (c == '\n') {
    strOutput.remove(len-1, 1);
  }
  return strOutput;    //Return the value of strOutput
}
 

void AptInfoEditorForm::Init(QString sIcao)
{
  m_sFilename = QString("airport\\%1.html").arg(sIcao).latin1();
  //m_sFilename = sIcao; //a hack for now
  readHtmlInfo(m_sFilename);
}

//
// Parse the html file looking for the XML style comments
// Extract the info into the relevant member variables
// and set the corresponding controls
//
void AptInfoEditorForm::readHtmlInfo(QString sFilename)
{
  QString s;
  QFile aptFile(m_sFilename);
  if (aptFile.open(IO_ReadOnly)) {
    while (!aptFile.atEnd()) {
      aptFile.readLine(s, 255);

      if (s.contains("<!--ICAO-->")) {
        aptFile.readLine(sICAO, 255);
        ICAOLineEdit->setText(stripHTMLTags(sICAO));
      }
      else if (s.contains("<!--INFO-->")) {
        aptFile.readLine(sINFO, 255);
        NameLineEdit->setText(stripHTMLTags(sINFO));
      }
      else if (s.contains("<!--COUNTRY-->")) {
        aptFile.readLine(sCOUNTRY, 255);
        CountryLineEdit->setText(stripHTMLTags(sCOUNTRY));
      }
      else if (s.contains("<!--REGION-->")) {
        aptFile.readLine(sREGION, 255);
        RegionLineEdit->setText(stripHTMLTags(sREGION));
      }
      else if (s.contains("<!--SPATIAL-->")) {
        aptFile.readLine(s, 255);
        while (!s.contains("<!--/SPATIAL-->")) {
          if (aptFile.atEnd()) break;
          sSPATIAL.append(s);
          aptFile.readLine(s, 255);
        }
        GeographicMultiLineEdit->setText(stripHTMLTags(sSPATIAL));
      }
      else if (s.contains("<!--FREQ-->")) {
        aptFile.readLine(s, 255);
        while (!s.contains("<!--/FREQ-->")) {
          if (aptFile.atEnd()) break;
          sFREQ.append(s);
          aptFile.readLine(s, 255);
        }
        FreqMultiLineEdit->setText(stripHTMLTags(sFREQ));
      }
      // <!--RWY-->
      else if (s.contains("<!--RWY-->")) {
        aptFile.readLine(s, 255);
        while (!s.contains("<!--/RWY-->")) {
          if (aptFile.atEnd()) break;
          sRWY.append(s);
          aptFile.readLine(s, 255);
        }
        RwyMultiLineEdit->setText(stripHTMLTags(sRWY));
      }
      // <!--COMMENT-->
      else if (s.contains("<!--COMMENT-->")) {
        aptFile.readLine(s, 255);
        while (!s.contains("<!--/COMMENT-->")) {
          if (aptFile.atEnd()) break;
          sCOMMENT.append(s);
          aptFile.readLine(s, 255);
        }
        CommentMultiLineEdit->setText(stripHTMLTags(sCOMMENT));
      }
      // <!--FUEL-->
      else if (s.contains("<!--FUEL-->")) {
        aptFile.readLine(s, 255);
        while (!s.contains("<!--/FUEL-->")) {
          if (aptFile.atEnd()) break;
          sFUEL.append(s);
          aptFile.readLine(s, 255);
        }
        FuelMultiLineEdit->setText(stripHTMLTags(sFUEL));
      }
      // <!--OPERATOR-->
      else if (s.contains("<!--OPERATOR-->")) {
        aptFile.readLine(s, 255);
        while (!s.contains("<!--/OPERATOR-->")) {
          if (aptFile.atEnd()) break;
          sOPERATOR.append(s);
          aptFile.readLine(s, 255);
        }
        OprMultiLineEdit->setText(stripHTMLTags(sOPERATOR));
      }
      // <!--IMAGE-->
      else if (s.contains("<!--IMAGE-->")) {
        aptFile.readLine(s, 255);
        while (!s.contains("<!--/IMAGE-->")) {
          if (aptFile.atEnd()) break;
          //sOPERATOR.append(s);
          //<br><a href="./img/YSEN.png"><img src="./img/YSEN.png" width=240 height=150 border=0>
          //"<[^>]*>"
          //"<[^>]+>"
          QRegExp r("\"[^\"]+\""); // match a " than anything not " until we get a " again
          int len;
          int rv = r.match(s, 0, &len);
          QString sImgFileName(s.mid(rv+1, len-2));
          ImgListBox->insertItem(sImgFileName);
          aptFile.readLine(s, 255);
        }
        OprMultiLineEdit->setText(stripHTMLTags(sOPERATOR));
      }

    }
  }
  aptFile.close();
}


//-----------------------------------------------------------------------------
//  Set the strings of the subwidgets using the current language.
// 
void AptInfoEditorForm::languageChange()
{
    setCaption( tr( "Form1" ) );
    ICAOTextLabel->setText( tr( "ICAO" ) );
    NameTextLabel->setText( tr( "Name" ) );
    CountryTextLabel->setText( tr( "Country" ) );
    RegionTextLabel->setText( tr( "Region" ) );
    GeographicTextLabel->setText( tr( "Geographic info" ) );
    FrequenciesTextLabel->setText( tr( "Frequencies" ) );
    RunwaysTextLabel->setText( tr( "Runways" ) );
    CommentsTextLabel->setText( tr( "Comments" ) );
    FuelTextLabel->setText( tr( "Fuel" ) );
    OperatorTextLabel->setText( tr( "Operator" ) );
    ImageTextLabel->setText( tr( "Images" ) );
    ImgListBox->clear();
    //ImgListBox->insertItem( tr( "New Item" ) );
    AddButton->setText( tr( "Add" ) );
    RemoveButton->setText( tr( "Remove" ) );
    OKButton->setText( tr( "OK" ) );
    CancelButton->setText( tr( "Cancel" ) );
}

void AptInfoEditorForm::add_clicked()
{
  QString fn = QFileDialog::getOpenFileName("airport/img", "Images (*.gif *.jpg *.png *.xpm)", this);

  if (!fn.isEmpty()) {
    printf(QString("You chose to open this file: %1 \n").arg(fn));
    ImgListBox->insertItem(fn);
  }
}

void AptInfoEditorForm::remove_clicked()
{
  ImgListBox->removeItem(ImgListBox->currentItem());
}

void AptInfoEditorForm::ok_clicked()
{
  printf("AptInfoEditorForm::ok_clicked\n");

  // Can do all this for us
  // InfoHtml infoHtml;
  // use it!

  QFile aptFile(m_sFilename);
  if (aptFile.open(IO_WriteOnly)) {
    QTextStream ts(&aptFile);

    ts << "<html>\n";
    ts << "<body>\n";
    ts << "<!--TITLE-->\n";
    ts << "<hr>";
    ts << "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
    ts << "<tr valign=\"middle\" bgcolor=\"#CCCCFF\"> ";
    ts << "<td nowrap align=\"center\"><font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"5\"><b>\n";
    ts << "<!--ICAO-->\n";
    ts << ICAOLineEdit->text() << "\n";
    ts << "<!--/ICAO-->\n";
    ts << "</b></font></td>";
    ts << "<td width=\"99%\" bgcolor=\"#FFFFFF\"> <font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"+1\"><b>\n";
    ts << "<!--INFO-->\n";
    ts << NameLineEdit->text() << "\n";
    ts << "<!--/INFO-->\n";
    ts << "</b><br>\n";
    ts << "<!--COUNTRY-->\n";
    ts << CountryLineEdit->text() << "\n";
    ts << "<!--/COUNTRY-->\n";
    ts << ",\n";
    ts << "<!--REGION-->\n";
    ts << RegionLineEdit->text() << "\n";
    ts << "<!--/REGION-->\n";
    ts << "</font> </td> ";
    ts << "<td nowrap bgcolor=\"#FFFFFF\"></td> ";
    ts << "</tr> ";
    ts << "</table> ";
    ts << "<hr>\n";
    ts << "<!--SPATIAL-->\n";
    ts << GeographicMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
    ts << "<!--/SPATIAL-->\n";
    ts << "<p><b>Frequencies</b><br>\n";
    ts << "<!--FREQ-->\n";
    ts << FreqMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
    ts << "<!--/FREQ-->\n";
    ts << "<p><b>Runways</b><br>\n";
    ts << "<!--RWY-->\n";
    ts << RwyMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
    ts << "<!--/RWY-->\n";
    ts << "<p><b>Comments</b><br>\n";
    ts << "<!--COMMENT-->\n";
    ts << CommentMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
    ts << "<!--/COMMENT-->\n";
    ts << "<p><b>Fuel</b><br>\n";
    ts << "<!--FUEL-->\n";
    ts << FuelMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
    ts << "<!--/FUEL-->\n";
    ts << "<p><b>Operator</b><br>\n";
    ts << "<!--OPERATOR-->\n";
    ts << OprMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
    ts << "<!--/OPERATOR-->\n";
    ts << "<p>\n";
    ts << "<!--IMAGE-->\n";
    // <br><a href="./img/YPJT.png"><img src="./img/YPJT.png" width=240 height=150 border=0></p>
    for (unsigned int i = 0; i < ImgListBox->count(); i++) {
      ts << "<br><a href=\"" << ImgListBox->text(i) << "\"> "
         << "<img src=\"" << ImgListBox->text(i) 
         << "\" width=240 border=0> \n";
    }
    ts << "<!--/IMAGE-->\n";
    ts << "</body>\n";
    ts << "</html>\n";
  }
	else {
    //QMessageBox::information(this, SZ_PRGNAME, QString("Cannot write to file - %1").arg(m_sFilename));
    QMessageBox::warning(this, SZ_PRGNAME, QString("Cannot write to file - %1").arg(m_sFilename));
	}
  aptFile.close();
  reject();
}

