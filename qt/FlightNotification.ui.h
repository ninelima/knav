//-----------------------------------------------------------------------------
// $Id: FlightNotification.ui.h,v 1.5 2005/01/17 15:20:47 player Exp $
//
// ui.h extension file, included from the uic-generated form implementation.
//
// If you want to add, delete, or rename functions or slots, use
// Qt Designer to update this file, preserving your code.
//
// You should not define a constructor or destructor in this file.
// Instead, write your code in functions called init() and destroy().
// These will automatically be called by the form's constructor and
// destructor.
//
//-----------------------------------------------------------------------------

#include <qmessagebox.h>
#include <qpixmap.h>
#include <qpainter.h>
#include <qfile.h>
#include <qfiledialog.h>
#include "MapWnd.h"
#include "misc.h"
//#include "iniFile.h"

extern CMapWnd g_MapWnd;



QString FormatRouteWaypoint(QLineEdit *atsRoute,
                            QLineEdit *sigPoint,
                            QLineEdit *newSpeed,
                            QLineEdit *newFlLev,
                            QLineEdit *newRules)
{
  QString s;

  if (!atsRoute->text().isEmpty()) { s.append(atsRoute->text()); s.append(" "); }
  if (!sigPoint->text().isEmpty()) { s.append(sigPoint->text()); }
  if (!newSpeed->text().isEmpty()) { s.append("/"); s.append(newSpeed->text()); }
  if (!newFlLev->text().isEmpty()) { s.append(newFlLev->text()); }  s.append(" ");
  if (!newRules->text().isEmpty()) { s.append(newRules->text()); s.append(" "); }

  return s;
}



//-----------------------------------------------------------------------------
// Produce a ICAO flightplan and load it into the system browser
//
void uiFlightNotificationDlg::printSlot()
{
  QPixmap pixIcao;
  if (cbAntiAlias->isChecked()) {
    pixIcao.load("./html/7233-4_1.png");
  }
	else {
    pixIcao.load("./html/7233-4_0.png");
	}
  QPainter b(&pixIcao);
 
  //QFont font("Courier New", 25, QFont::Normal); // - good
  QFont font("Comic Sans MS", 25, QFont::Normal); // - better :-)
  font.setStretch(QFont::Expanded);
  font.setFixedPitch(true);

  b.setFont(font);
  b.drawText(354,  384, 210, 30, Qt::AlignLeft | Qt::AlignVCenter, AircraftId->text()); // 7  Aircraft Id
  b.drawText(766,  387,  30, 30, Qt::AlignLeft | Qt::AlignVCenter, FlightRules->currentText());      // 8  Flight rules
  b.drawText(1002, 387,  30, 30, Qt::AlignLeft | Qt::AlignVCenter, TypeOfFlight->currentText());      //    Type of flight

  b.drawText(86,  446,  60, 30, Qt::AlignLeft | Qt::AlignVCenter, NumberOfAircraft->text());      // 9  Number
  b.drawText(294, 446, 120, 30, Qt::AlignLeft | Qt::AlignVCenter, AircraftType->text());    //    Type
  b.drawText(644, 449,  30, 30, Qt::AlignLeft | Qt::AlignVCenter, WakeTurbulenceCategory->currentText());       //    Wake turbulence cat

  QString sBuffer;
  if (NavComEquipN->isChecked()) sBuffer.append("N");
  if (NavComEquipS->isChecked()) sBuffer.append("S");
  if (NavComEquipD->isChecked()) sBuffer.append("D");
  if (NavComEquipF->isChecked()) sBuffer.append("F");
  if (NavComEquipG->isChecked()) sBuffer.append("G");
  if (NavComEquipH->isChecked()) sBuffer.append("H");
  if (NavComEquipI->isChecked()) sBuffer.append("I");
  if (NavComEquipJ->isChecked()) sBuffer.append("J");
  if (NavComEquipL->isChecked()) sBuffer.append("L");
  if (NavComEquipO->isChecked()) sBuffer.append("O");
  if (NavComEquipR->isChecked()) sBuffer.append("R");
  if (NavComEquipT->isChecked()) sBuffer.append("T");
  if (NavComEquipU->isChecked()) sBuffer.append("U");
  if (NavComEquipV->isChecked()) sBuffer.append("V");
  if (NavComEquipW->isChecked()) sBuffer.append("W");
  if (NavComEquipZ->isChecked()) sBuffer.append("Z");
  b.drawText(862,  446, 144, 30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);   // 10 Equipment
  b.drawText(1004, 446,  30, 30, Qt::AlignLeft | Qt::AlignVCenter, SSREquip->currentText());  // SSR

  b.drawText(172, 506, 120, 30, Qt::AlignLeft | Qt::AlignVCenter, Departure->text());  // 13 Departure aerodrome
  b.drawText(414, 506, 180, 30, Qt::AlignLeft | Qt::AlignVCenter, ETD->text());        // Time

  b.drawText(85,  567, 150, 30, Qt::AlignLeft | Qt::AlignVCenter, InitCruiseSpeed->text());  // 15 Cruising speed
  b.drawText(264, 567, 150, 30, Qt::AlignLeft | Qt::AlignVCenter, InitCruiseLevel->text());  // 13 Level

  char *spr = "12345678901234567890123456789012345678901234567890"; // 50
  //char *sp1 = "DCT YMMG DCT YABC F065/N155 DCT YRAV DCT YSEN";  // 50
  //char sbuf[128];
  //char *sp = " ";
 

  int i, pos;
  QString s;
	//
	// Build up the route and store it as string s
	//
  // 0 - 16
	s.truncate(0);
  s  = FormatRouteWaypoint(atsRoute_0,  sigPoint_0,  newSpeed_0,  newFlLev_0,  newRules_0);
  s += FormatRouteWaypoint(atsRoute_1,  sigPoint_1,  newSpeed_1,  newFlLev_1,  newRules_1);
  s += FormatRouteWaypoint(atsRoute_2,  sigPoint_2,  newSpeed_2,  newFlLev_2,  newRules_2);
  s += FormatRouteWaypoint(atsRoute_3,  sigPoint_3,  newSpeed_3,  newFlLev_3,  newRules_3);
  s += FormatRouteWaypoint(atsRoute_4,  sigPoint_4,  newSpeed_4,  newFlLev_4,  newRules_4);
  s += FormatRouteWaypoint(atsRoute_5,  sigPoint_5,  newSpeed_5,  newFlLev_5,  newRules_5);
  s += FormatRouteWaypoint(atsRoute_6,  sigPoint_6,  newSpeed_6,  newFlLev_6,  newRules_6);
  s += FormatRouteWaypoint(atsRoute_7,  sigPoint_7,  newSpeed_7,  newFlLev_7,  newRules_7);
  s += FormatRouteWaypoint(atsRoute_8,  sigPoint_8,  newSpeed_8,  newFlLev_8,  newRules_8);
  s += FormatRouteWaypoint(atsRoute_9,  sigPoint_9,  newSpeed_9,  newFlLev_9,  newRules_9);
  s += FormatRouteWaypoint(atsRoute_10, sigPoint_10, newSpeed_10, newFlLev_10, newRules_10);
  s += FormatRouteWaypoint(atsRoute_11, sigPoint_11, newSpeed_11, newFlLev_11, newRules_11);
  s += FormatRouteWaypoint(atsRoute_12, sigPoint_12, newSpeed_12, newFlLev_12, newRules_12);
  s += FormatRouteWaypoint(atsRoute_13, sigPoint_13, newSpeed_13, newFlLev_13, newRules_13);
  s += FormatRouteWaypoint(atsRoute_14, sigPoint_14, newSpeed_14, newFlLev_14, newRules_14);
  s += FormatRouteWaypoint(atsRoute_15, sigPoint_15, newSpeed_15, newFlLev_15, newRules_15);
  s += FormatRouteWaypoint(atsRoute_16, sigPoint_16, newSpeed_16, newFlLev_16, newRules_16);
 

  //
  // Enter the 5 route lines
  //

	// line 1
  pos = 0;
  sBuffer.truncate(0);
  for (i = 32; i > 0; i--) {
    if (s[i] == ' ') {
      sBuffer = s.mid(pos, i);
      pos = i;
      break;
    }
  }            
  b.drawText(440, 567, 736,  30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);  //    Route top line 32 char

	// line 2
  sBuffer.truncate(0);
  for (i = 32+49; i > pos; i--) {
    if (s[i] == ' ') {
      sBuffer = s.mid(pos, i-32+2);
      pos = i;
      break;
    }
  }
  b.drawText(55,  598, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);  //    Route 2nd line 49 char

	// line 3
  sBuffer.truncate(0);
  for (i = 32+49+49; i > pos; i--) {
    if (s[i] == ' ') {
      sBuffer = s.mid(pos, i-32-49+2);
      pos = i;
      break;
    }
  }
  b.drawText(55,  628, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);    //    Route 3rd line 49 char

	// line 4
  sBuffer.truncate(0);
  for (i = 32+49+49+49; i > pos; i--) {
    if (s[i] == ' ') {
      sBuffer = s.mid(pos, i-32-49-49+2);
      pos = i;
      break;
    }
  }
  b.drawText(55,  659, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);    //    Route 4th line 49 char

	// line 5
  sBuffer.truncate(0);
  for (i = 32+49+49+49+49; i > pos; i--) {
    if (s[i] == ' ') {
      sBuffer = s.mid(pos, i-32-49-49-49+2);
      pos = i;
      break;
    }
  }
  b.drawText(55,  689, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);    //    Route 5th line 49 char

  b.drawText(172, 782, 120, 30, Qt::AlignLeft | Qt::AlignVCenter, Destination->text()); // 16 Destination
  //b.drawText(436, 782,  60, 30, Qt::AlignLeft | Qt::AlignVCenter, "02");              //    Total EET hrs
  //b.drawText(496, 782,  60, 30, Qt::AlignLeft | Qt::AlignVCenter, "11");              //    Total EET min
  b.drawText(436, 782, 120, 30, Qt::AlignLeft | Qt::AlignVCenter, TotalEET->text());    //    Total EET 
  b.drawText(680, 782, 120, 30, Qt::AlignLeft | Qt::AlignVCenter, Alternate->text());   //    Alternate 
  b.drawText(916, 782, 120, 30, Qt::AlignLeft | Qt::AlignVCenter, " ");                 //    2nd Alternate 


	//
	// Build up the 'OTHER INFORMATION' and store it as string s
	//
	s.truncate(0);
	if (!REG->text().isEmpty()) s += "REG/" + REG->text() + " "; 
	// todo PER/
	if (!DLA->text().isEmpty()) s += "DLA/" + DLA->text() + " "; 
	if (!RMK->text().isEmpty()) s += "RMK/" + RMK->text() + " "; 
	if (!EET->text().isEmpty()) s += "EET/" + EET->text() + " "; 

	if (!DEP->text().isEmpty())  s += "DEP/"  + DEP->text()  + " "; 
	if (!DEST->text().isEmpty()) s += "DEST/" + DEST->text() + " "; 
	if (!NAV->text().isEmpty())  s += "NAV/"  + NAV->text()  + " "; 
	if (!STS->text().isEmpty())  s += "STS/"  + STS->text()  + " "; 
	
	// SARTIME
	if (!DTG->text().isEmpty())  {
		s += "SARTIME " + DTG->text() + " ";
		s += ToAtnsUnit->text() + " ";
		s += "FOR " + ForNNNat->currentText() + " AT ";
		s += ForNNNatMMMM->text() + " ";
	}

	
	if (!CODE->text().isEmpty()) s += "CODE/" + CODE->text() + " "; 
	if (!DAT->text().isEmpty())  s += "DAT/"  + DAT->text()  + " "; 
	if (!COM->text().isEmpty())  s += "COM/"  + COM->text()  + " "; 
	if (!TYP->text().isEmpty())  s += "TYP/"  + TYP->text()  + " "; 
	if (!ALTN->text().isEmpty()) s += "ALTN/" + ALTN->text() + " "; 
	if (!RALT->text().isEmpty()) s += "RALT/" + RALT->text() + " "; 
	if (!SEL->text().isEmpty())  s += "SEL/"  + SEL->text()  + " "; 
	if (!OPR->text().isEmpty())  s += "OPR/"  + OPR->text()  + " "; 

	if (!RIF->text().isEmpty())  s += "RIF/"  + RIF->text()  + " "; 

  //
  // Enter the 4 '18 OTHER INFORMATION' lines
  //
	
	// line 1
  pos = 0;
  sBuffer.truncate(0);
  for (i = 48; i > 0; i--) {
    if (s[i] == ' ') {
      sBuffer = s.mid(pos, i);
      pos = i;
      break;
    }
  }            
  b.drawText(85, 841, 1090, 30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);  // 18 Other Information top line

	// line 2
  sBuffer.truncate(0);
  for (i = 48+49; i > pos; i--) {
    if (s[i] == ' ') {
      sBuffer = s.mid(pos, i-48+2);
      pos = i;
      break;
    }
  }
  b.drawText(55, 871, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);  //    Other  2nd line 49 char
  
	// line 3
  sBuffer.truncate(0);
  for (i = 48+49+49; i > pos; i--) {
    if (s[i] == ' ') {
      sBuffer = s.mid(pos, i-48-49+2);
      pos = i;
      break;
    }
  }
  b.drawText(55, 899, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);    //    Other  3rd line 49 char

	// line 4
  sBuffer.truncate(0);
  for (i = 48+49+49+49; i > pos; i--) {
    if (s[i] == ' ') {
      sBuffer = s.mid(pos, i-48-49-49+2);
      pos = i;
      break;
    }
  }
  b.drawText(55, 926, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sBuffer);    //    Other 4th line 49 char

	//strcpy(sbuf, spr);
  //b.drawText(85, 841, 1090, 30, Qt::AlignLeft | Qt::AlignVCenter, sbuf);   // 18 Other Information top line
  //b.drawText(55, 871, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sp);     //    Route  2nd line 49 char
  //b.drawText(55, 899, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sp);     //    Route  3rd line 49 char
  //b.drawText(55, 926, 1120, 30, Qt::AlignLeft | Qt::AlignVCenter, sp);     //    Route  4th line 49 char





  //b.drawText(148,  1038, 60, 30, Qt::AlignLeft | Qt::AlignVCenter, "04");    // 19 Endurance hrs
  //b.drawText(208,  1038, 60, 30, Qt::AlignLeft | Qt::AlignVCenter, "30");    //    Endurance min
  b.drawText(148,  1038, 120, 30, Qt::AlignLeft | Qt::AlignVCenter, Endurance->text());    // 19 Endurance 
  b.drawText(412,  1043, 90, 30, Qt::AlignLeft | Qt::AlignVCenter, PersonsOnBoard->text()); // Persons on board
  
  if (EmerRadioUHF->isChecked())  b.drawText(832,  1038, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");  // Emer UHF
  if (EmerRadioVHF->isChecked())  b.drawText(920,  1038, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");  // Emer UHF
  if (EmerRadioELBA->isChecked()) b.drawText(1012, 1038, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");  // Emer ELBA
  
  // TODO
#if 0
  b.drawText(147,  1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");      // Survival 
  b.drawText(234,  1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");      // Polar 
  b.drawText(324,  1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");      // Deser 
  b.drawText(416,  1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");      // Maritim 
  b.drawText(496,  1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");      // Jungle 
#endif
  
  if (LifeJacketsLight->isChecked()) b.drawText(648,  1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");  // Jackets 
  if (LifeJacketsLight->isChecked()) b.drawText(742,  1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");  // Light 
  if (LifeJacketsFluore->isChecked()) b.drawText(832,  1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X"); // Fluore 
  if (LifeJacketsUHF->isChecked()) b.drawText(922,  1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");    // UHF 
  if (LifeJacketsVHF->isChecked()) b.drawText(1008, 1128, 30, 30, Qt::AlignLeft | Qt::AlignVCenter, "X");    // VHF 


  if (DinghiesNumber->text().toLong() > 0 ) {
    b.drawText(90,   1215, 30,  30, Qt::AlignLeft | Qt::AlignVCenter, "X");      // Dinghies
    b.drawText(148,  1216, 60,  30, Qt::AlignLeft | Qt::AlignVCenter, DinghiesNumber->text());      // Number
    b.drawText(238,  1216, 90,  30, Qt::AlignLeft | Qt::AlignVCenter, DinghiesCapacity->text());      // Capacity
    if (DinghiesCovered->currentText().contains("YES"))  b.drawText(354,  1216, 90,  30, Qt::AlignLeft | Qt::AlignVCenter, "X");  // Covered
    b.drawText(413,  1218, 274, 30, Qt::AlignLeft | Qt::AlignVCenter, DinghiesColor->text()); // Color
  }

  b.drawText(147,  1273, 1030, 30, Qt::AlignLeft | Qt::AlignVCenter, AircraftColorAndMarkings->text() ); //  Aircraft color & markings

  //b.drawText(90,   1333, 30,  30, Qt::AlignLeft | Qt::AlignVCenter, "X");      // N Remarks
  b.drawText(147,  1333, 913, 30, Qt::AlignLeft | Qt::AlignVCenter, SuppRemarks->text()); //  Remarks
  b.drawText(147,  1392, 531, 30, Qt::AlignLeft | Qt::AlignVCenter, PilotInCommand->text()); //  Pilot-in-command

  pixIcao.save("./html/7233-4_out.png", "PNG");

  emit accept();
  //html_browse("file:./7233-4_1.html");
}



QString FormatNaipsWaypoint(int iSegment,
                            QLineEdit *atsRoute, 
                            QLineEdit *sigPoint,  
                            QLineEdit *newSpeed,
                            QLineEdit *newFlLev,
                            QLineEdit *newRules)
{
  QString s;
  s.append(QString("SEG=%1\n").arg(iSegment));
  s.append(QString("ATSCODE=%1\n").arg(atsRoute->text()));
  s.append(QString("SIGPT=%1\n").arg(sigPoint->text()));
  s.append(QString("NSPEED=%1\n").arg(newSpeed->text()));
  s.append(QString("NLEVEL=%1\n").arg(newFlLev->text()));
  s.append(QString("NRULES=%1\n").arg(newRules->text()));
  return s;
}


void uiFlightNotificationDlg::naipsSlot()
{
  char sz[256];
  naips_path(sz);
  QString sPath(QString("%1\\details\\knav.dtl").arg(sz));

  QString fn = QFileDialog::getSaveFileName(/*QString::null*/sPath, "*.dtl", this);
  if (fn.isEmpty()) {
    return;
  }


  QFile file(/*sPath*/fn);
  if (file.open(IO_WriteOnly | IO_Translate)) {
    QTextStream stream( &file );
    stream << "Details Ver2 nfw_3.3.1" << "\n";
    stream << "FNT=ICAO" << "\n";

    stream << "STAGE=1" << "\n";
    stream << "ACFTID=" << AircraftId->text() << "\n";
    stream << "RULES=" << FlightRules->currentText() << "\n";
    stream << "FTYPE=" << TypeOfFlight->currentText() << "\n";
    stream << "NAC=" << NumberOfAircraft->text() << "\n";
    stream << "ACFTTYPE=" << AircraftType->text() << "\n";
    stream << "WAKE=" << WakeTurbulenceCategory->currentText() << "\n";
// todo EQUIP=S
    stream << "SSR=" << SSREquip->currentText() << "\n";
    stream << "DEP=" << Departure->text() << "\n";
    stream << "ETD=" << ETD->text() << "\n";
    stream << "SPEED=" << InitCruiseSpeed->text() << "\n";
    stream << "LEVEL=" << InitCruiseLevel->text() << "\n";
    stream << "DEST=" << Destination->text() << "\n";
    stream << "EET=" << TotalEET->text() << "\n";
    stream << "ALT=" << Alternate->text() << "\n";

    int i = 0;
    // note: we use i++ and not ++i
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_0,  sigPoint_0,  newSpeed_0,  newFlLev_0,  newRules_0 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_1,  sigPoint_1,  newSpeed_1,  newFlLev_1,  newRules_1 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_2,  sigPoint_2,  newSpeed_2,  newFlLev_2,  newRules_2 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_3,  sigPoint_3,  newSpeed_3,  newFlLev_3,  newRules_3 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_4,  sigPoint_4,  newSpeed_4,  newFlLev_4,  newRules_4 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_5,  sigPoint_5,  newSpeed_5,  newFlLev_5,  newRules_5 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_6,  sigPoint_6,  newSpeed_6,  newFlLev_6,  newRules_6 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_7,  sigPoint_7,  newSpeed_7,  newFlLev_7,  newRules_7 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_8,  sigPoint_8,  newSpeed_8,  newFlLev_8,  newRules_8 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_9,  sigPoint_9,  newSpeed_9,  newFlLev_9,  newRules_9 ); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_10, sigPoint_10, newSpeed_10, newFlLev_10, newRules_10); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_11, sigPoint_11, newSpeed_11, newFlLev_11, newRules_11); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_12, sigPoint_12, newSpeed_12, newFlLev_12, newRules_12); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_13, sigPoint_13, newSpeed_13, newFlLev_13, newRules_13); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_14, sigPoint_14, newSpeed_14, newFlLev_14, newRules_14); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_15, sigPoint_15, newSpeed_15, newFlLev_15, newRules_15); }
    if (++i < g_MapWnd.RouteWpt) { stream << FormatNaipsWaypoint(i, atsRoute_16, sigPoint_16, newSpeed_16, newFlLev_16, newRules_16); }
 
    /*
    COVDINGHY=N
    */
    stream << "PIC=" << PilotInCommand->text() << "\n";
    stream << "PHONE=" << ContactPhoneNumber->text() << "\n";
    stream << "END=" << Endurance->text() << "\n";
    stream << "POB=" << PersonsOnBoard->text() << "\n";

    file.close();     
  }

  //naips_execute();
}
