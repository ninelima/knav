#include <stdlib.h>
#include <qstring.h>
#include <qtimer.h>
#include <qsqldatabase.h>
#include <qdatatable.h>
#include <qsqlquery.h>
#include <qsqlselectcursor.h> 
#include <qmessagebox.h>
#include <qfiledialog.h>
#include <qhttp.h>
#include <qurl.h>

#include <qhttp.h>
#include <qsocket.h>

#include "Validator.h"
#include "Validator.moc"
#include "iniFile.h"
#include "../qt/version.h"


Validator::Validator(QWidget *parent, const char *name, WFlags f)
  : QWidget(parent, name, f) 
{																					  
  m_articleFetcher = new QHttp();
	connect(m_articleFetcher, SIGNAL(done(bool)), this, SLOT(fetchDone(bool)));

  QString home("http://members.iinet.net.au/~ninelima/addr.txt");
  QUrl u(home);
  m_articleFetcher->setHost(u.host());
  m_articleFetcher->get(home);
}

Validator::~Validator()
{
	delete m_articleFetcher;
}

void Validator::fetchDone(bool error)
{
  m_serverIP = QString(m_articleFetcher->readAll());
  sendStr(SZ_VERSION);
}

void Validator::sendStr(QString str)
{
  // First check if we have a valid IP address to send to
  if (m_serverIP.isEmpty()) 
    return;

  char szSerialNum[256];
  char szOwner[256];

  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iniFile.GetValueS(szSerialNum, "Prefs", "SerialNum", "");
  iniFile.GetValueS(szOwner, "Prefs", "Owner", "");

  m_socket = new QSocket( this );            // create the socket and connect various of its signals
  m_socket->connectToHost(m_serverIP, 4242); // and connect to the server

  m_buffer = QString("%1 %2 %3\n").arg(szSerialNum).arg(szOwner).arg(str);
  int rv = m_socket->writeBlock(m_buffer.latin1(), m_buffer.length());
  m_socket->close();
}
