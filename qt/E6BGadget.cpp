//-----------------------------------------------------------------------------
// $Id: E6BGadget.cpp,v 1.6 2005/03/01 10:16:27 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <qrect.h>
#include <math.h>
#include "E6BGadget.h"
#include "umath.h"
#include "uvalues.h"


//-----------------------------------------------------------------------------
// Default constructor
//
E6BGadget::E6BGadget(QWidget *parent, const char *name, WFlags f)
    :QWidget(parent, name, f)
{
  hotspot = 0;
  offsetE6B = 0;
  // capture mouse movements only if buttons are pressed
  // ie mouse dragged
  //setMouseTracking(false);
  // not!
  setMouseTracking(true);
  m_bMouseLeftButtonDown = false;
  m_bMouseRightButtonDown = false;
}



//-----------------------------------------------------------------------------
// Paint method the panel
//
void E6BGadget::paintEvent(QPaintEvent* e)
{
  QPainter p(this);
  QPainter b(&pixbuffer);
  GraphicPainter *gc = (GraphicPainter *) &p;

  drawE6B((GraphicPainter *) &b);

  // copy the image from the buffer pixmap to the window
  bitBlt(this, 0, 0, &pixbuffer);

  int x, y;
  rect = p.window();
  int screenWidth  = rect.width();
  int screenHeight = rect.height();
  x = screenWidth/2;
  y = screenHeight/2;
  int isize = screenHeight/2*8/10;

  // draw a lubber line -------------
  gc->setPen(Qt::red);
  gc->radLine(x, y, (int) (1.3*isize), lubberE6B);
}


 void E6BGadget::incE6B() 
{
  offsetE6B++;
}

 void E6BGadget::decE6B() 
{
  offsetE6B--;
}

 void E6BGadget::setE6B(int a) 
{
  offsetE6B = a;
}

 void E6BGadget::setE6Blubber(int a)
{
  lubberE6B = a;
}

void E6BGadget::drawE6B(GraphicPainter *gc)
{
  int x, y;
  int isize;
  int a, b;
  rect = gc->window();
  int screenWidth  = rect.width();
  int screenHeight = rect.height();

  x = screenWidth/2;
  y = screenHeight/2;
  isize = screenHeight/2*8/10;

  // set a color for the outer slide
  gc->setPen(Qt::yellow);
  gc->setBrush(Qt::yellow);
  gc->Ellipse(x - isize*1.2, y - isize*1.2, x+isize*1.2, y+isize*1.2);

  // set a color for the inner slide
  gc->setPen(Qt::white);
  gc->setBrush(Qt::white);
  gc->Ellipse(x - isize, y - isize, x+isize, y+isize);

  int fontsize = 16*isize/255;
  if (fontsize > 10) fontsize = 10;
  int adjust = fontsize/2;

  QFont font1("Arial", fontsize);
  QFont font2("Arial", fontsize*9/10);
  QFont font3("Arial", fontsize*9/10);
  for (int i = 1; i < 260; i++) {
    gc->setFont(font1);
    double s = log10((double)i);
    int phi = 90 - (int) (360*s);

    // outer scale ------------------------
    // draw the lines
    gc->setPen(Qt::green);

    // draw the lines
    a = (int) uround(isize*cos(phi*M_PI/180.0));
    b = (int) uround(isize*sin(phi*M_PI/180.0));
    if (i < /*100*/50) {
      gc->radLine(x+a, y-b, (int) (0.05*isize), phi);
    }
    else if (i % 10 == 0) {
      gc->radLine(x+a, y-b, (int) (0.05*isize), phi);
    }

    // draw the text
    if (i % 10 == 0) {
      a = (int) uround(1.1*isize*cos((phi)*M_PI/180.0));
      b = (int) uround(1.1*isize*sin((phi)*M_PI/180.0));
      if ( i < 100)
        gc->drawString(QString("%1").arg(i), x+a-adjust, y-b);
      else
        gc->drawString(QString("%1").arg(i/10), x+a-adjust, y-b);
    }

    if (i == 1) {
      a = (int) uround(1.1*isize*cos((phi)*M_PI/180.0));
      b = (int) uround(1.1*isize*sin((phi)*M_PI/180.0));
      gc->setBrush(Qt::blue);
      gc->Ellipse(x+a-isize*0.06, y-b-isize*0.06, x+a+isize*0.06, y-b+isize*0.06);
    }

    // inner scale ------------------------
    gc->setPen(Qt::magenta);
    phi += offsetE6B;

    // draw the lines
    a = (int) uround(isize*cos(phi*M_PI/180.0));
    b = (int) uround(isize*sin(phi*M_PI/180.0));
    if (i < /*100*/50) {
      gc->radLine(x+a, y-b, (int) (0.05*isize), phi+180);
    }
    else if (i % 10 == 0){
      gc->radLine(x+a, y-b, (int) (0.05*isize), phi+180);
    }

    // draw the text
    if (i % 10 == 0) {
      a = (int) uround(0.9*isize*cos((phi)*M_PI/180.0));
      b = (int) uround(0.9*isize*sin((phi)*M_PI/180.0));
      if ( i < 100)
        gc->drawString(QString("%1").arg(i), x+a-adjust, y-b);
      else
        gc->drawString(QString("%1").arg(i/10), x+a-adjust, y-b);

      // time calibrated scale inner text
      // for greater than 10 hrs - 60 in the scale
      if ((i > 60) && (i < 120) ||
          (i == 150) || (i == 210) ) {
        gc->setFont(font2);
        a = (int) uround(0.7*isize*cos((phi)*M_PI/180.0));
        b = (int) uround(0.7*isize*sin((phi)*M_PI/180.0));
        // we cheat using the int to auto truncate
        gc->drawString(QString().sprintf("%02d:%02d", i/60, i % 60), x+a-adjust*2, y-b);
      }
    }

    // time calibrated scale inner text
    if (i % 6 == 0) {
      gc->setFont(font2);
      a = (int) uround(0.7*isize*cos((phi)*M_PI/180.0));
      b = (int) uround(0.7*isize*sin((phi)*M_PI/180.0));
      if ( i < 60) {
        //DecimalFormat fmtTime = new DecimalFormat("00");
        // we cheat using the int to auto truncate
        gc->drawString(QString().sprintf("%02d:%02d", i/6, 0), x+a-adjust*2, y-b);
      }
    }

    if (i == 1) {
      a = (int) uround(0.9*isize*cos((phi)*M_PI/180.0));
      b = (int) uround(0.9*isize*sin((phi)*M_PI/180.0));
      gc->setBrush(Qt::blue);
      gc->Ellipse(x+a-isize*0.06, y-b-isize*0.06, x+a+isize*0.06, y-b+isize*0.06);
    }

    if (i == 6) {
      QPointArray ptArray(3);
 
      a = (int) uround(1.0*isize*cos((phi)*M_PI/180.0));
      b = (int) uround(1.0*isize*sin((phi)*M_PI/180.0));
      ptArray[0] = QPoint(x+a, y-b);

      a = (int) uround(0.8*isize*cos((phi-5)*M_PI/180.0));
      b = (int) uround(0.8*isize*sin((phi-5)*M_PI/180.0));
      ptArray[1] = QPoint(x+a, y-b);

      a = (int) uround(0.8*isize*cos((phi+5)*M_PI/180.0));
      b = (int) uround(0.8*isize*sin((phi+5)*M_PI/180.0));
      ptArray[2] = QPoint(x+a, y-b);

      gc->setPen(blue);
      gc->setBrush(blue);
      gc->drawPolygon(ptArray);//, 0, 3);
    }
  }

  /*
  This is done in paintEvent directly onto the painter
  That way we need not worry about leaving tracks

  // draw a lubber line -------------
  gc->setPen(Qt::red);
  gc->moveTo(x, y);
  gc->lineTo(10, 10);
  gc->radLine(x, y, (int) (1.3*isize), lubberE6B);
  */

  // draw the que lines -------------
  double r = 0.22;
  // USG / LBS - avgas
  gc->setFont(font3);
  double s = log10(167.0);
  int phi = 90 - (int) (360*s);

  gc->setPen(Qt::blue);
  a = (int) uround(isize*cos(phi*M_PI/180.0));
  b = (int) uround(isize*sin(phi*M_PI/180.0));
  gc->radLine(x+a, y-b, (int) (r*isize), phi);

  // draw the text
  a = (int) uround(1.25*isize*cos((phi)*M_PI/180.0));
  b = (int) uround(1.25*isize*sin((phi)*M_PI/180.0));
  gc->drawString("USG / LBS", x+a/*-adjust*/, y-b);

  // usg / lit ------------------------
  s = log10(265.0);
  phi = 90 - (int) (360*s);

  gc->setPen(Qt::blue);
  a = (int) uround(isize*cos(phi*M_PI/180.0));
  b = (int) uround(isize*sin(phi*M_PI/180.0));
  gc->radLine(x+a, y-b, (int) (r*isize), phi);

  // draw the text
  a = (int) uround(1.25*isize*cos((phi)*M_PI/180.0));
  b = (int) uround(1.25*isize*sin((phi)*M_PI/180.0));
  gc->drawString("USG / LIT", x+a/* *adjust */, y-b);

  // LIT / KG - avgas -----------------
  s = log10(142.0);
  phi = 90 - (int) (360*s);

  gc->setPen(Qt::blue);
  a = (int) uround(isize*cos(phi*M_PI/180.0));
  b = (int) uround(isize*sin(phi*M_PI/180.0));
  gc->radLine(x+a, y-b, (int) (r*isize), phi);

  // draw the text
  a = (int) uround(1.25*isize*cos((phi)*M_PI/180.0));
  b = (int) uround(1.25*isize*sin((phi)*M_PI/180.0));
  gc->drawString("LIT / KG", x+a/*-adjust*/, y-b);



  // m / ft -------------------------
  s = log10(305.0);
  phi = 90 - (int) (360*s);

  gc->setPen(Qt::blue);
  a = (int) uround(isize*cos(phi*M_PI/180.0));
  b = (int) uround(isize*sin(phi*M_PI/180.0));
  gc->radLine(x+a, y-b, (int) (r*isize), phi);

  // draw the text
  a = (int) uround(1.25*isize*cos((phi)*M_PI/180.0));
  b = (int) uround(1.25*isize*sin((phi)*M_PI/180.0));
  gc->drawString("M / FT", x+a/*-adjust*/, y-b);




  // kg / LBS -------------------------
  s = log10(455.0);
  phi = 90 - (int) (360*s);

  gc->setPen(Qt::blue);
  a = (int) uround(isize*cos(phi*M_PI/180.0));
  b = (int) uround(isize*sin(phi*M_PI/180.0));
  gc->radLine(x+a, y-b, (int) (r*isize), phi);

  // draw the text
  a = (int) uround(1.25*isize*cos((phi)*M_PI/180.0));
  b = (int) uround(1.25*isize*sin((phi)*M_PI/180.0));
  gc->drawString("KG / LB", x+a-7*adjust, y-b);

  // nm / km --------------------------
  s = log10(540.0);
  phi = 90 - (int) (360*s);

  gc->setPen(Qt::blue);
  a = (int) uround(isize*cos(phi*M_PI/180.0));
  b = (int) uround(isize*sin(phi*M_PI/180.0));
  gc->radLine(x+a, y-b, (int) (r*isize), phi);

  // draw the text
  a = (int) uround(1.25*isize*cos((phi)*M_PI/180.0));
  b = (int) uround(1.25*isize*sin((phi)*M_PI/180.0));
  gc->drawString("NM / KM", x+a-7*adjust, y-b);

  // sm / km --------------------------
  s = log10(625.0);
  phi = 90 - (int) (360*s);

  gc->setPen(Qt::blue);
  a = (int) uround(isize*cos(phi*M_PI/180.0));
  b = (int) uround(isize*sin(phi*M_PI/180.0));
  gc->radLine(x+a, y-b, (int) (r*isize), phi);

  // draw the text
  a = (int) uround(1.25*isize*cos((phi)*M_PI/180.0));
  b = (int) uround(1.25*isize*sin((phi)*M_PI/180.0));
  gc->drawString("SM / KM", x+a-7*adjust, y-b);
}


//-----------------------------------------------------------------------------
// This virtual method is called whenever the window is resized. We
// use it to make sure that the off-screen buffer is always the same
// size as the window.
// To retain the original drawing, it is first copied
// to a temporary buffer. After the main buffer has been resized and
// filled with white, the image is copied from the temporary buffer to
// the main buffer.
//
void E6BGadget::resizeEvent(QResizeEvent* event)
{
  //QPixmap save(pixbuffer);
  pixbuffer.resize(event->size());
  pixbuffer.fill(white);
  //bitBlt(&pixbuffer, 0, 0, &save);
}

void E6BGadget::mousePressEvent(QMouseEvent *e)
{
  if (e->button() == LeftButton) {
    m_bMouseLeftButtonDown = true;
  }

  if (e->button() == RightButton) {
    m_bMouseRightButtonDown = true;
  }
}

void E6BGadget::mouseReleaseEvent(QMouseEvent *e)
{
  if (e->button() == LeftButton) {
    m_bMouseLeftButtonDown = false;
  }

  if (e->button() == RightButton) {
    m_bMouseRightButtonDown = false;
  }
}

void E6BGadget::mouseMoveEvent(QMouseEvent *e)
{
  int x = rect.width()/2;
  int y = rect.height()/2;
  int dx = e->x()-x;
  int dy = e->y()-y;
  int theta = (int) (180/M_PI*atan2((double)dy, (double)dx));

  if (m_bMouseLeftButtonDown) {
    // we are dragging with the left mouse button
    setE6B(-theta-90-hotspot); //the -80 is to set the hotspot to the 60 "arrow"
    repaint(false);
  }

  if (m_bMouseRightButtonDown) {
    // we are dragging with the right mouse button
    setE6Blubber(-theta);
    repaint(false);
  }

  QWidget::mouseMoveEvent(e);
}




