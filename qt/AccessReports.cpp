#include "AccessReports.h"
//#include "AccessReportsSet.h"
#include "resource.h"

IMPLEMENT_DYNAMIC(CAccessReports, CMapStringToOb)

#define HTML_EXTENSION _T(".html")

/******************************************************************************
 ******************************************************************************/
CAccessReports::CAccessReports(CString const& strDatabaseName, BOOL bLoadAllReports)
{
  m_strDatabaseName = strDatabaseName;

  m_pAccess = NULL;
  m_pDoCmd = NULL;

  m_pAccess = new _Application;
  if (m_pAccess) {
    m_pAccess->CreateDispatch(L"Access.Application");
    m_pAccess->OpenCurrentDatabase(strDatabaseName, FALSE);

    LPDISPATCH lpDispDoCmd = m_pAccess->GetDoCmd();
    if (lpDispDoCmd) {
      m_pDoCmd = new DoCmd(lpDispDoCmd);
      if (m_pDoCmd) {

        if (TRUE == bLoadAllReports) {
          LoadAllReports();
        }
      }
    }
  }
}


/******************************************************************************
 ******************************************************************************/
void CAccessReports::LoadAllReports()
{
#if 0

  ASSERT(m_pAccess && m_pDoCmd);
  if (m_pAccess && m_pDoCmd) {
    CDaoWorkspace ws;
    CDaoDatabase* pDb;
    CAccessReportsSet* pRs;
    try
    {
      ws.Create(_T("FX"), _T("Admin"), _T(""));

      pDb = new CDaoDatabase(&ws);
      if (pDb) {
        pDb->Open(m_strDatabaseName);

        pRs = new CAccessReportsSet(pDb);
        if (pRs) {
          pRs->Open();

          while (!pRs->IsEOF()) {
            m_reports.Add(pRs->m_strName);
            pRs->MoveNext();
          }

          pRs->Close();
          delete pRs;

          pDb->Close();
          delete pDb;

          ws.Close();
        }
      }
    }
    catch(CDaoException* pe) {
      // close mfc dao db objects
      if (ws.IsOpen()) {
        ws.Close();
        if (pDb) {
          if (pDb->IsOpen()) {
            pDb->Close();

            if (pRs) {
              if (pRs->IsOpen()) {
                pRs->Close();
              }

              delete pRs;
            }

            delete pDb;
          }
        }
      }

      AfxMessageBox(pe->m_pErrorInfo->m_strDescription, MB_ICONEXCLAMATION);
      pe->Delete();
    }
  }
#endif // 0
}


/******************************************************************************
 ******************************************************************************/
CAccessReports::~CAccessReports()
{
  if (m_pDoCmd) {
    delete m_pDoCmd;
    m_pDoCmd = NULL;
  }

  if (m_pAccess) {
    m_pAccess->Quit(0 /* acPrompt */);
    delete m_pAccess;
    m_pAccess = NULL;
  }
}


/******************************************************************************
 ******************************************************************************/
void CAccessReports::RunReport(CString const& strReportName)
{
  if (m_pAccess && m_pDoCmd) {
    m_pDoCmd->OpenReport(
      COleVariant(strReportName),
      2,                                          // acViewPreview
      COleVariant(),
      COleVariant());

    m_pAccess->SetVisible(TRUE);
  }
}


/******************************************************************************
 ******************************************************************************/
void CAccessReports::Print(CString const& strReportName)
{
  if (m_pAccess && m_pDoCmd) {
    RunReport(strReportName);
    m_pDoCmd->PrintOut(0,                         // acPrintAll
      COleVariant(),
      COleVariant(),
      0,                                          // acHigh
      COleVariant((short)1, VT_I2),
      COleVariant((short)-1, VT_I2));
  }
}


/******************************************************************************
 ******************************************************************************/
void CAccessReports::SaveAsHtml(CString const& strReportName, CString const& strDestinationFolder)
{    
  CString ss = strReportName;

  if (m_pAccess && m_pDoCmd) {
    CString strHtmlFileName = strDestinationFolder + strReportName + HTML_EXTENSION;

    m_pDoCmd->OutputTo(
      3,                                          // asOutputReport
      COleVariant(strReportName),                 // <report name>
      COleVariant(L"HTML (*.html)"),              // acFormatHTML
      COleVariant(strHtmlFileName),               // <report name> + .html
      COleVariant((short)0, VT_I2),               // autostart ?
      COleVariant(L""));
  }
}
