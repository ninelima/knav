
#ifndef TRIPKIT_H
#define TRIPKIT_H

#include <qvariant.h>
#include <qdialog.h>
#include "TripKitDlg.uih"

class TripKit : public uiTripKitDlg
{
  Q_OBJECT

  public:
    TripKit( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~TripKit();

protected slots:
    void tripkit_toggle();

};

#endif // TRIPKIT_H
