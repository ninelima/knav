#ifndef __DBCONF_H
#define __DBCONF_H

/*
#define BUILD_CROSSVIEW
#ifdef BUILD_CROSSVIEW
  #define DRIVER       "MYSQL"                     // see the Qt SQL documentation for a list of available drivers
  #define DATABASE     "cvii"                      // the name of your database
  #define USER         "root"                      // user name with appropriate rights
  #define PASSWORD     ""                          // password for USER
  #define HOST         "castor"                    // host on which the database is running
#else
  #define DRIVER       "QSQLITE"                   // see the Qt SQL documentation for a list of available drivers
  #define DATABASE     "knav.db"                   // the name of your database
  #define USER         ""                          // user name with appropriate rights
  #define PASSWORD     ""                          // password for USER
  #define HOST         ""                          // host on which the database is running
#endif //BUILD_CROSSVIEW
*/


/*
#ifdef BUILD_CROSSVIEW
  #define DRIVER       "QSQLITE"                   // see the Qt SQL documentation for a list of available drivers
  #define DATABASE     "cvii.db"                   // the name of your database
  #define USER         ""                          // user name with appropriate rights
  #define PASSWORD     ""                          // password for USER
  #define HOST         ""                          // host on which the database is running
#else
  #define DRIVER       "QSQLITE"                   // see the Qt SQL documentation for a list of available drivers
  #define DATABASE     "knav.db"                   // the name of your database
  #define USER         ""                          // user name with appropriate rights
  #define PASSWORD     ""                          // password for USER
  #define HOST         ""                          // host on which the database is running
#endif //BUILD_CROSSVIEW

*/

#ifdef BUILD_CROSSVIEW
  #define DRIVER       "QODBC3"                    // see the Qt SQL documentation for a list of available drivers
  #define DATABASE     "DRIVER={Microsoft Access Driver (*.mdb)};FIL={MS Access};DBQ=cvii.mdb"   // the name of your database
  #define USER         ""                          // user name with appropriate rights
  #define PASSWORD     ""                          // password for USER
  #define HOST         ""                          // host on which the database is running
#else
  #define DRIVER       "QODBC3"                    // see the Qt SQL documentation for a list of available drivers
  #define DATABASE     "DRIVER={Microsoft Access Driver (*.mdb)};FIL={MS Access};DBQ=knav.mdb"   // the name of your database
  #define USER         ""                          // user name with appropriate rights
  #define PASSWORD     ""                          // password for USER
  #define HOST         ""                          // host on which the database is running
#endif //BUILD_CROSSVIEW


#endif // __DBCONF_H
