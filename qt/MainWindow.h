/****************************************************************************
** $Id: MainWindow.h,v 1.21 2007/03/19 01:50:37 player Exp $
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/
#ifndef __APPLICATION_H
#define __APPLICATION_H

#include <qmainwindow.h>
#include <qtoolbutton.h>
#include <qlistview.h>
#include <qtextbrowser.h>
#include <qtabwidget.h>
#include <qpixmap.h>
#include <qtable.h>
#include <qsplitter.h>

#include "UTextBrowser.h"

#ifndef __GRAPHICPAINTER_H
#include "GraphicPainter.h"
#endif

#ifndef __MASSBALANCEGADGET_H
#include "MassBalanceGadget.h"
#endif

#ifndef PLAYCONTROL_H
#include "PlayControl.uih"
#endif


#ifndef DATACOMMS_H
#ifdef BUILD_CROSSVIEW
#include "DataComms.uih"
#endif
#endif



//-----------------------------------------------------------------------------
// MapWidget definition
//
class MapWidget : public QWidget
{
  Q_OBJECT

public:
  bool m_bPaintCache;

  MapWidget(QWidget *parent=0, const char *name=0, WFlags f=0);
  ~MapWidget() {}

  void animateMe();
  void followMe();
  void flashPoint(double rlat, double rlon);
  void flash(QColor color, double rlat, double rlon, bool keeponscreen);
  void targetInfo();
  void targetInfoAll();

private:
  QPixmap m_pix;        // Pixmap for double-buffering
  //QPixmap m_raw;        // Pixmap for raw double-buffering
  POINT point;// = {e->x(), e->y()};

protected:
  void paintCache(QPaintEvent* e);
  void paintDirect(QPaintEvent* e);

  void paintEvent(QPaintEvent *e);
  void resizeEvent(QResizeEvent *e);
  void mouseMoveEvent(QMouseEvent *e);
  void mousePressEvent(QMouseEvent *e);
  void mouseReleaseEvent(QMouseEvent *e);
  void wheelEvent(QWheelEvent *e);

        
//public:
private slots:

signals:
  void infoChanged();
  void latlonChanged();

//protected:
};


//-----------------------------------------------------------------------------
// ApplicationWindow definition
//
class QMultiLineEdit;
class QToolBar;
class QPopupMenu;

class ApplicationWindow: public QMainWindow
{
    Q_OBJECT
public:
  MapWidget *pMapWidget;
  UTextBrowser *m_htmlBrowser;

  ApplicationWindow();
  ~ApplicationWindow();

  QPixmap openIcon, saveIcon, printIcon, wingsIcon;
  QPixmap zoomIcon, routeIcon;
  QPixmap globeIcon, shadeIcon;

  QPixmap aptIcon, navIcon, airIcon, gpsTrkIcon;
  QPixmap redLedOnIcon, redLedOffIcon, greenLedOnIcon, greenLedOffIcon;

  void createMenu();
  void createToolbar();
  void createWorkspace();

 
protected:
  void closeEvent(QCloseEvent *);
  void keyPressEvent(QKeyEvent *e);
  void keyReleaseEvent(QKeyEvent *e);


private:
  int m_reccnt;

  int id_relief;
  int id_rose;
  int id_noderose;
  int id_globe;
  //----------
  int id_apt;
  int id_nav;
  int id_air;
  int id_trk;

  int id_undo;
  int id_redo;

  int id_open; 
  int id_save; 
  int id_save_as;
  int id_print;


  QPopupMenu *view;// = new QPopupMenu(this);
  QPopupMenu *edit;

  void displayInfoHtml(QString sIcao); 
  void dl_all();
  void report_helper(QString sReport);


public: // slots:
  void fileLoad(const char *szfileName);

  void helper_print_text(GraphicPainter *p, QString filename);
  //void print_text(GraphicPainter *p);
  void helper_print_graphics(GraphicPainter *p);
  void helper_print_massbalance(GraphicPainter *p);


  //void setTitle(const QString &caption);
  void setTitle();


private slots:
  //
  // Menu slots
  //
  void fileNew();
  void fileOpen();
  //  void fileLoad(const char *fileName);
  void fileSave();
  void saveAs();
  void print();
  //-----------
  void editUndo();
  void editRedo();
  //-----------
  void zoom();
  void route();

  void ack_redled();
  void ack_greenled();

  //-----------
  void toolbar_apt();
  void toolbar_nav();
  void toolbar_trk();
  void toolbar_air();

  //-----------
  void rose();
  void noderose();

  void globe();
  void relief();
  void radar();

  void preferences();
  void startup();
  void refresh();

  //-----------
  void zoomall();
  void zoomtrack();

  void expanddatabase();
  void collapsedatabase();

  //-----------
  void massbalance();
  void e6bcomputer();
  void flightlog();
  void planninglog();
  //void flightlogTxt(GraphicPainter g);
  void flightlogTxt(QString fileName);
  void planninglogTxt(QString fileName);

  void flightlogHtml(QString fileName);
  void planninglogHtml(QString fileName);

  //-----------
  void briefing();
  void weather();
  void duats_naips();
  void flightplan();

  //-----------
  void dl_track();
  void dl_route();
  void dl_waypt();
  //-----------
  void report_1();
  void report_2();
  void report_3();
  void report_4();
  void report_5();
  void report_6();

  //-----------
  void about();
  void index();
  void aboutQt();

  //-----------

  //
  // miscellaneous slots
  //
  void clickCatcher();
  void infoChanged();
  void latlonChanged();
  /*
  void latlonChanged();
  */
  //void routeTablevalueChanged();
  //void trueTablevalueChanged();
  void trueTableValueChanged(int row, int col);
  //void clicked(int row, int col, int button, const QPoint &mousePos)
  void trueTableClicked(int row, int col, int button, const QPoint &mousePos);
  void magneticTableClicked(int row, int col, int button, const QPoint &mousePos);
  void trackTableClicked(int row, int col, int button, const QPoint &mousePos);


  //
  // Slots for actions performed on the ListView "tree"
  //
  void lv_mouseButtonClicked( int button, QListViewItem * item, const QPoint & pos, int c );
  //void lv_rightButtonClicked(QListViewItem *, const QPoint &, int);


  //
  // Popup menu slots
  //
  void autofit();
  void fixedfit();
  void popup_info();
  void popup_insert();
  void popup_delete();

  void popup_route();
  void popup_locate();
  void popup_trail();
  void popup_animate();
  void popup_followme();
  void popup_edit();
  void popup_remove();

  void popup_track_locate();


  void test();
  int testChange();

  //connect(massBalanceGadget, SIGNAL(aircraftChanged()), this, SLOT(aircraftChanged()));
  void aircraftChanged();

  void pollTimerDone();
  void scrollTimerDone();

private:
  QTabWidget* tabs;

  QPrinter *m_printer;
  MassBalanceGadget *m_massBalanceGadget;
  PlayControl *m_playControlDlg;
  QTimer *PollTimer;
  QTimer *ScrollTimer;

  QSplitter *splitH;  // = new QSplitter(QSplitter::Horizontal, central, "splitter");
  QSplitter *splitV0; // = new QSplitter(QSplitter::Vertical, leftPanel, "splitter");
  QSplitter *splitV1; // = new QSplitter(QSplitter::Vertical, rightPanel, "splitter");

  #ifdef BUILD_CROSSVIEW

    QString m_sHtml_Notification;
    QString m_sHtml_Exception;
    QString m_sHtml_Text;

    DataComms *m_dataCommsDlg;
    QToolButton *radar_button;

    QTable *m_TrackTable;//    = new QTable(NR_LEGS + 2, MAXCOLOM, tabs, "track table");

    bool scrollActive;
    QLabel *scrollinfo;

    QToolButton *redled_button;
    QToolButton *greenled_button;
    // or ...
    //QLabel *redled_label;
    //QLabel *greenled_label;
 
    QToolButton *ack_button;
  #endif // BUILD_CROSSVIEW

  QWidget  *central;
  QToolBar *fileTools;
  QToolBar *actionTools;
  QToolBar *filterTools;
  QToolBar *commsTools;

  QString m_filename;

  QToolButton *zoom_button;
  QToolButton *route_button;
  QToolButton *globe_button;
  QToolButton *shade_button;

  QToolButton *apt_button;
  QToolButton *nav_button;
  QToolButton *gpstrk_button;
  QToolButton *air_button;

  QListView *m_top;
  QListViewItem *activedb;
  QListViewItem *wptdb;
  QListViewItem *downloaddb;

 
  void updateMRU(const char *szfileName);
  
  void createDatabaseTree();
  void expandAllDatabaseTree();
  void collapseAllDatabaseTree();
  void recurseAllTree(QListViewItem * it, bool bOpen, int depth);


  void createDownloadTree();
  void createActiveTree();
  void reCreateActiveTree();

};


#endif // __APPLICATION_H
