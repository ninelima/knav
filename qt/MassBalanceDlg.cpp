//-----------------------------------------------------------------------------
// $Id: MassBalanceDlg.cpp,v 1.9 2006/04/23 10:58:06 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the 
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786


#ifdef WIN32
  //
  // Include the windows headers on the windows
  // platform so we can do some windows stuff.
  //
  #include "windows.h"
#endif //WIN32

#include <stdio.h>
#include <stdlib.h>
#include "MassBalanceDlg.h"
#include "MassBalanceDlg.moc"

#include <qiconview.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>
#include <qdir.h>
#include "iniFile.h"           

//-----------------------------------------------------------------------------
//  Constructs a MassBalanceDlg which is a child of 'parent', with the 
//  name 'name' and widget flags set to 'f' 
//
//  The dialog will by default be modeless, unless you set 'modal' to
//  TRUE to construct a modal dialog.
// 
MassBalanceDlg::MassBalanceDlg(QWidget* parent,  const char* name, bool modal, WFlags fl)
    : QDialog(parent, name, modal, fl)
{
  if (!name)
    setName("MassBalanceDlg");

  resize(638, 440); 
  setCaption(tr("MassBalanceDlg"));
  setSizeGripEnabled(TRUE);
  MassBalanceDlgLayout = new QVBoxLayout(this); 
  MassBalanceDlgLayout->setSpacing(6);
  MassBalanceDlgLayout->setMargin(11);

  /*
  Layout22 = new QHBoxLayout; 
  Layout22->setSpacing(6);
  Layout22->setMargin(0);

  Layout20 = new QVBoxLayout; 
  Layout20->setSpacing(6);
  Layout20->setMargin(0);

  Layout17 = new QGridLayout; 
  Layout17->setSpacing(6);
  Layout17->setMargin(0);
  */

  massBalanceGadget = new MassBalanceGadget(this, "mb");
  MassBalanceDlgLayout->addWidget(massBalanceGadget);

  //
  // Register interest in the aircraftChanged() signal. The signal
  // is emitted when a new aircraft is chosen.
  //
  connect(massBalanceGadget, SIGNAL(aircraftChanged()), this, SLOT(aircraftChanged()));


  Layout1 = new QHBoxLayout; 
  Layout1->setSpacing(6);
  Layout1->setMargin(12);       

  buttonEdit = new QPushButton(this, "buttonEdit");
  buttonEdit->setText(tr("Edit"));
  buttonEdit->setAutoDefault(TRUE);
  Layout1->addWidget(buttonEdit);

  /*
  buttonPrint = new QPushButton(this, "buttonPrint");
  buttonPrint->setText(tr("Print"));
  buttonPrint->setAutoDefault(TRUE);
  Layout1->addWidget(buttonPrint);
  */
  
  QSpacerItem* spacer_3 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
  Layout1->addItem(spacer_3);

  buttonCancel = new QPushButton(this, "buttonCancel");
  buttonCancel->setText(tr("Dismiss"));
  buttonCancel->setAutoDefault(TRUE);
  Layout1->addWidget(buttonCancel);
  MassBalanceDlgLayout->addLayout(Layout1);

  // signals and slots connections
  connect(buttonCancel, SIGNAL(clicked()), this, SLOT(reject()));
  connect(buttonEdit, SIGNAL(clicked()), this, SLOT(edit()));
  /*
  connect(buttonPrint, SIGNAL(clicked()), this, SLOT(print()));
  */
}

//-----------------------------------------------------------------------------
//  Destroys the object and frees any allocated resources
// 
MassBalanceDlg::~MassBalanceDlg()
{
  // no need to delete child widgets, Qt does it all for us
}

//  
//  Main event handler. Reimplemented to handle application
//  font changes
// 
/*
bool MassBalanceDlg::event(QEvent* ev)
{
    bool ret = QDialog::event(ev); 
    if (ev->type() == QEvent::ApplicationFontChange) {
    QFont arm_font(arm->font());
    arm_font.setFamily("Courier");
    arm->setFont(arm_font); 
    QFont weight_font(weight->font());
    weight_font.setFamily("Courier");
    weight->setFont(weight_font); 
    }                    
    return ret;
}
*/


void MassBalanceDlg::edit()
{
  char szLastAd[256];

  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iniFile.GetValueS(szLastAd, "Prefs", "LastUsedAd", " ");

  printf("MassBalanceDlg::edit\n");
  QString sLastAd = QDir::convertSeparators(szLastAd);

  #ifdef WIN32
    WinExec(QString("notepad.exe %1").arg(sLastAd), SW_SHOW);
  #else
    int rv = system(QString("notepad.exe %1").arg(sLastAd));
  #endif //WIN32
}


void MassBalanceDlg::print()
{
  printf("MassBalanceDlg::print\n");

  /*
  if (printer->setup(this)) {
    QPainter p(printer);
  
    //print_graphics((GraphicPainter *) &p);
  
    QPaintDeviceMetrics pdm(printer);
    int w = pdm.width();
    int h = pdm.height();
    g_MapWnd.Size(w, h);
    g_MapWnd.PaintMain(p);  // the whole echilada
  



  massBalanceGadget->envelopePanel->PaintMain();
  */
}



void MassBalanceDlg::aircraftChanged()
{
  printf("MassBalanceDlg::aircraftChanged\n");
}
