//-----------------------------------------------------------------------------
// $Id: AltGadget.cpp,v 1.6 2005/12/21 03:57:11 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <qrect.h>
#include <math.h>
#include "AltGadget.h"
#include "umath.h"
#include "uvalues.h"

//#define min(a,b)            (((a) < (b)) ? (a) : (b))

//-----------------------------------------------------------------------------
// Default constructor
//
AltGadget::AltGadget(QWidget *parent, const char *name, WFlags f)
    //:QWidget(parent, name, f)
    :PanelGadget(parent, name, f)
{
  // capture mouse movements only if buttons are pressed
  // ie mouse dragged
  //setMouseTracking(false);
  // not!
  setMouseTracking(true);
  m_bMouseLeftButtonDown = false;
  m_bMouseRightButtonDown = false;

  m_alt = 1234;
}


//-----------------------------------------------------------------------------
// Paint method the panel
//

void AltGadget::drawBlankFace(GraphicPainter *gc, int x, int y, int size)
{
  QString ts;
  int m;
  double scale = m_scale;
  size = m_size;

  int fontheight = gc->fontMetrics().height();
  int fontwidth = gc->fontMetrics().width('0');

  gc->setPen(Qt::gray);
  gc->setBrush(Qt::gray);
  gc->drawRect(0, 0, 150*scale, 150*scale);
  gc->setPen(Qt::black);
  gc->setBrush(Qt::black);
  gc->drawEllipse(0, 0, 150*scale, 150*scale);
  gc->drawEllipse(55*scale, 55*scale, 80*scale, 80*scale);

  QFont font1("Arial", 8*scale);
  gc->setFont(font1);
  gc->setPen(Qt::white);

  for (m = 50; m >= 1; m--) {
    ts.sprintf("%d", (50-m)*2 / 10);

    // Short calib lines
    int off = 40;
    int phi = m*360 / 36+90;
    int a = uround(off*cos(phi*M_PI/180));
    int b = uround(off*sin(phi*M_PI/180));
    gc->radLine((x+a)*scale, (y-b)*scale, scale*5, phi);

    if ( (-(50-m) % 5) == 0) {
      int off = 55;
      int phi = m*360 / 50+90;
      int a = uround(off*cos(phi*M_PI/180));
      int b = uround(off*sin(phi*M_PI/180));

      switch (phi) {
        case 450 :  b=b+2; a=a-2; break;  // 0
        case 414 :  b=b+2; a=a-2; break;  // 1
        case 378 :  b=b+3; a=a-3; break;  // 2
        case 342 :  b=b+5; a=a-3; break;  // 3
        case 306 :  b=b+5; a=a-3; break;  // 4
        case 270 :  b=b+5; a=a-3; break;  // 5
        case 234 :  b=b+5; a=a-0; break;  // 6
        case 198 :  b=b+5; a=a-0; break;  // 7
        case 162 :  b=b+5; a=a-0; break;  // 8
        case 126 :  b=b+5; a=a-0; break;  // 9
      } //switch

      //a = a - fontwidth;
      b = b - fontheight/2;
 
      gc->drawString(ts, (x+a)*scale, (y-b)*scale);

    }
  }
}


void AltGadget::drawFace(GraphicPainter *gc)
{
  QString ts;
  int i;
  int a, b;
  int x = 75;
  int y = 75;

  gc->setPen(QPen(Qt::yellow, 3*m_scale));

  a = uround(-0.36*m_alt+90);
  gc->radLine(x*m_scale, y*m_scale, 50*m_scale, a); 

  a = uround(-0.036*m_alt+90);
  gc->radLine(x*m_scale, y*m_scale, 35*m_scale, a);

  a = uround(-0.0036*m_alt+90);
  gc->radLine(x*m_scale, y*m_scale, 15*m_scale, a);
}


void AltGadget::setAlt(int alt)
{
  if (m_alt != alt) {
    m_alt = alt;
    repaint(false);
  }
}

