//-----------------------------------------------------------------------------
// MapWnd.h : header file
// $Id: MapWnd.h,v 1.36 2007/03/10 03:50:46 player Exp $
//

#ifndef __MAPWND_H
#define __MAPWND_H


#ifndef __GENMAPWND_H
#include "GenMapWnd.h"
#endif

#ifndef __WPTSET_H
#include "WptSet.h"
#endif

#ifndef __TXTSET_H
#include "TxtSet.h"
#endif

#ifndef __GPSTRKSET_H
#include "GpsTrkSet.h"
#endif

#ifndef __AIRSPACESET_H
#include "AirspaceSet.h"
#endif

#ifndef _MOTIF_
#ifndef QTABLE_H
#include <qtable.h>
#endif

#ifndef QLISTVIEW_H
#include <qlistview.h>
#endif

#include <qsqlcursor.h>

#include "PlayControl.uih"


#endif //_MOTIF_

// The waypoint database constant
// must be a single bit in an integer
// ie 1,2,4,8,16 etc.
#define W_APT  1
#define W_VOR  2
#define W_NDB  4
#define W_USR  8
#define W_IFR 16
#define W_SLD 32
// W_IFR | W_SLD = 48

#define NR_LEGS 28
#define MINFONTSIZE  6
#define MAXFONTSIZE 14
//
// sensible range for FONT_SCALER 1.0 - 1.5
//
//#define MAXCOLOM    12
#define MAXCOLOM    13
#define THIN_PEN    1
#define WIDE_PEN    3
#define FAT_PEN     11

#define SZ_ALL_DEVICE "* All Devices"
#define SZ_ALL_ASSET  "* All Assets"
//#define SZ_ASSET      "Asset"


// may need to be in uselect.h
class USelect
{
public:
  int m_activebranch;
  QListViewItem *m_lisviewitem;
  QString m_idStr;
  QString m_coor;
  QString m_icao;
  QString m_iiid;
  QString m_tripid;
  double m_minLat;
  double m_maxLat;
  double m_minLon;
  double m_maxLon;

  QString m_devid;
  bool m_poll;
};




void convLL(double *fc, char *sc);


//-----------------------------------------------------------------------------
// TableItem Class
//
class TableItem : public QTableItem
{
public:
  TableItem(QTable *t, EditType et, const QString &txt);
    //: QTableItem( t, et, txt );
  void paint(QPainter *p, const QColorGroup &cg, const QRect &cr, bool selected);
};     

//-----------------------------------------------------------------------------
// CMapWnd window
//
class CMapWnd : public CGenMapWnd
{
// Construction
public:
  CMapWnd();

// Attributes
public:

#ifndef _MOTIF_
  // data needed for a info html
  long inf_id;
  QString inf_name;
  QString inf_icao;
#endif
  
  int m_TAS, m_FuelFlow, m_Units;

  bool wptReady;
  CWptSet wpt;

  bool txtReady;
  CTxtSet txt;
  CTxtSet streetTxt;

  bool airReady;
  CAirspaceSet air;

  bool gpsTrkAnimate;
  bool gpsTrkFollowMe;
  bool gpsTrkReady;
  CGpsTrkSet gpsTrk;

  bool gpsAllTrkReady;
  CGpsTrkSet gpsAllTrk;


  //GpsWptSet gpsWpt;
  CWptSet gpsWpt;
  bool gpsWptReady;

  bool m_bPrnActive;
  bool m_bTextActive;
  bool bNodeRoseActive;
  bool bRadarActive;

  bool bAddRteActive;
  //  int  iInsertAt;

  bool bGpsTrkActive;
  bool bAirActive;
  bool bAirPenWide;

  // Mouse variables
  POINT mousepoint;
  BOOL _bLbuttonDown;
  bool _bDragged;
  int _iWpt;

  int iInsertAt;
  int iSeg, lastSeg;

  QString fromFileName;

#ifdef _MOTIF_
#else

  QTable *trueTable;
  QTable *magneticTable;

  PlayControl *playControlDlg;

#endif // _MOTIF_

  // Route variable
  long  _RouteLst[NR_LEGS];
  long   RouteLst[NR_LEGS];
  long  TasLst[NR_LEGS];
  long  FuelFlowLst[NR_LEGS];
  long  WindLst[NR_LEGS];
  long  _RouteWpt;
  //long  RouteWpt;
  Q_INT32 RouteWpt;
  bool  bRouteSaved;


// Operations
public:
  //void EnterMovement();
  void populateTrue();
  void populateMagnetic();
  void autoFitTableColums(int width = -1);

  void trueTableEdited(int row, int col);

  void PaintMain(GraphicPainter *gc);

  void EnterSingleMovement();
  //void DrawMovements(GraphicPainter *gc);
  void DrawSingleMovement(GraphicPainter *gc);
  void DrawNewNames(GraphicPainter *gc);

  void choosePen(GraphicPainter *gc, long iType);
  
  void drawAllWpt(GraphicPainter *gc);
  void drawWpt(GraphicPainter *gc);
  void drawTxt(GraphicPainter *gc);
  void drawAirspace(GraphicPainter *gc);

  void drawRoute(GraphicPainter *gc);
  void clearRoute();
  void saveRoute(const char *szFileName);
  bool loadRoute(const char *szFileName);

  void drawAllRadar(GraphicPainter *gc);

  void drawGpsAllTrk(GraphicPainter *gc);
  void drawGpsTrk(GraphicPainter *gc);
  void animateGpsTrk(GraphicPainter *gc);
  
  bool flash(GraphicPainter *gc, QColor color, double rLat, double rLon, bool bKeepOnscreen = false);
  void flashPoint(GraphicPainter *gc, double Lat, double Lon, bool bKeepOnscreen = false);
  void followGpsTrk(GraphicPainter *gc);

  void animateDatabaseTrk(GraphicPainter *gc);
  void followDatabaseTrk(GraphicPainter *gc);
  
  void drawDatabaseAllTrk(GraphicPainter *gc);
  void drawDatabaseTrk(GraphicPainter *gc);


  void drawGpsWpt(GraphicPainter *gc);

  void InitPlayControl(PlayControl *dlg = NULL);

  void loadAircraftDefaults();
  void mapPanelInit();
  void playControlDlgInit();

  void loadTxt();
  void loadWpt();
  void loadAir();
  void loadGpsWpt();
  void loadGpsTrack();
  //void reLoadGpsTrack();

  void loadDatabaseTrack();

  void loadGpsAllTrack();
  void unloadGpsAllTrack();

  void pushUndoBuffer();
  void appendRouteWpt(int iWpt);
  void insertRouteWpt(int iWpt, int iAt);
  void removeRouteWpt(int iAt);

  void higlightNearestWaypoint(GraphicPainter *gc, double rLat, double rLong);
  int higlightNearestSegment(GraphicPainter *gc);

  int findIcaoWaypoint(const char *sIcao);
  int findInfoWaypoint(const char *sInfo);

  int findNearestWaypoint(double rLat, double rLong);
  int findNearestRouteSegment(double rLat, double rLon);


  //void drawShadeT(GraphicPainter *pDC, double rtop, double rleft, double rbottom, double rright, const char *szFileName, const double cLat, const double cLon, const double nSpan);
  void drawShadeT(GraphicPainter *pDC, const char *szFileName, const double cLat, const double cLon, const double rSpan);

  void mouseLeftButtonDown(POINT point);
  void mouseLeftButtonUp(POINT point);
  void mouseRightButtonDown(POINT point);
  void mouseMoved(GraphicPainter *gc, POINT point);
  void mouseDragged(GraphicPainter *gc, POINT point);

public:
    void ZoomToTrack();
  int nLineCtr;
  virtual ~CMapWnd();


protected:

private:
  void autoSetPen(GraphicPainter *gc);

};


#endif //__MAPWND_H
