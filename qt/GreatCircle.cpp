//-----------------------------------------------------------------------------
//
// Title:        GreatCircle
//
// Description:  Module to calculate departure (12), arrival (21) angles
//               and great circle distance
//               Great Circle Calculator using ellipsoidal earth models
//
//               Copyright (C) 2000  Edward A Williams <Ed_Williams@compuserve.com>
//
// History:      07/15/2002 Dawie Botes (ninelima@yahoo.com)
//                          Adapted from Javascript version
//
// Copyright:    Copyright (C) 2000  Edward A Williams <Ed_Williams@compuserve.com>
//
//               This program is free software; you can redistribute it and/or
//               modify it under the terms of the GNU General Public License as
//               published by the Free Software Foundation; either version 2 of the
//               License, or (at your option) any later version.
//
//               This program is distributed in the hope that it will be useful, but
//               WITHOUT ANY WARRANTY; without even the implied warranty of
//               MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//               General Public License for more details.
//
//               You should have received a copy of the GNU General Public License
//               along with this program; if not, write to the Free Software
//               Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Company:
//
// @author       Dawie Botes
// @version 1.0
//

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "uvalues.h"
#include "umath.h"
#include "GreatCircle.h"

//
// Functions declared with the static modifier by definition have file scope.
// Static variables have the same limitation. Trying to access any static variables
// from outside of the file in which they are declared can result in a compile error
// or an unresolved external depending on the specific coding of the source files.
//
static double g_d, g_crs12, g_crs21;

//-----------------------------------------------------------------------------
// Compute course and distance (spherical model)
//
// @param lat1 the start latitude  - N positive
// @param lon1 the start longitude - E positive
// @param lat2 the end latitude  - N positive
// @param lon2 the end longitude - E positive
//
void GreatCircle::crsdist(double lat1, double lon1, double lat2, double lon2)
{ 
  double argacos;

  // convert angles to radians
  lat1 = (M_PI/180)*lat1;
  lat2 = (M_PI/180)*lat2;
  lon1 = (M_PI/180)*lon1;
  lon2 = (M_PI/180)*lon2;

  if ((lat1+lat2==0.) &&
      (fabs(lon1-lon2) == M_PI) &&
      (fabs(lat1) != (M_PI/180)*90.)) {
    //alert("Course between antipodal points is undefined");
    printf("Course between antipodal points is undefined\n");
  }

  g_d = acos(sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(lon1-lon2));
  if ((g_d==0.) || (lat1==-(M_PI/180)*90.)) {
    g_crs12=2*M_PI;
  }
  else if (lat1 == (M_PI/180)*90.) {
    g_crs12 = M_PI;
  }
  else {
    argacos = (sin(lat2) - sin(lat1) * cos(g_d))/(sin(g_d) * cos(lat1));
    if (sin(lon2-lon1) < 0) {
      g_crs12 = acosf(argacos);
    }
    else {
      g_crs12 = 2*M_PI-acosf(argacos);
    }
  }
  if ((g_d == 0.0) || (lat2 == -(M_PI/180)*90.0)) {
     g_crs21=0.0;
  }
  else if (lat2 == (M_PI/180)*90.0) {
    g_crs21 = M_PI;
  }
  else {
    argacos=(sin(lat1) - sin(lat2) * cos(g_d))/(sin(g_d) * cos(lat2));
    if (sin(lon1 - lon2)<0) {
      g_crs21 = acosf(argacos);
    }
    else {
      g_crs21 = 2*M_PI - acosf(argacos);
    }
  }

  //out=new MakeArray(0)
  //out.d=d
  //out.crs12=crs12
  //out.crs21=crs21
  //return out

  // Convert to degrees
  g_crs12=g_crs12*(180/M_PI);
  g_crs21=g_crs21*(180/M_PI);
  g_d=g_d*(180/M_PI)*60*1.0;  // go to physical units - nm = 1.0
}

/* protect against rounding error on input argument */
double GreatCircle::acosf(double x)
{
  if (fabs(x) > 1) {
    x /= fabs(x);
  }
  return acos(x);
}


//-----------------------------------------------------------------------------
// Compute course from 1 to 2 (spherical model)
// aka departure course
//
// @param lat1 the start latitude  - N positive
// @param lon1 the start longitude - E positive
// @param lat2 the end latitude  - N positive
// @param lon2 the end longitude - E positive
//
double GreatCircle::getCrs12(double lat1, double lon1, double lat2, double lon2)
{
  crsdist(lat1, lon1, lat2, lon2);
  return -g_crs12;
}

//-----------------------------------------------------------------------------
// Compute course from 2 to 1 (spherical model)
// aka arrival course
//
// @param lat1 the start latitude  - N positive
// @param lon1 the start longitude - E positive
// @param lat2 the end latitude  - N positive
// @param lon2 the end longitude - E positive
//
double GreatCircle::getCrs21(double lat1, double lon1, double lat2, double lon2)
{
  crsdist(lat1, lon1, lat2, lon2);
  return -g_crs21;
}

//-----------------------------------------------------------------------------
// Compute distance between 1 and 2 (spherical model)
//
// @param lat1 the start latitude  - N positive
// @param lon1 the start longitude - E positive
// @param lat2 the end latitude  - N positive
// @param lon2 the end longitude - E positive
//
double GreatCircle::getDistance(double lat1, double lon1, double lat2, double lon2)
{
  crsdist(lat1, lon1, lat2, lon2);
  return g_d;
}

//
//  Latitude of point on GC
//
//  Intermediate points {lat,lon} lie on the great circle connecting
//  points 1 and 2 when:
//
//  lat = atan((sin(lat1)*cos(lat2)*sin(lon-lon2)
//        -sin(lat2)*cos(lat1)*sin(lon-lon1))
//        /(cos(lat1)*cos(lat2)*sin(lon1-lon2)))
//
void GreatCircle::getIntermediatePoint(double *plat, double *plon, double lat1, double lon1, double lat2, double lon2)
{
  // convert angles to radians
  double lat = (M_PI/180)*(*plat);
  double lon = (M_PI/180)*(*plon);
  lat1 = (M_PI/180)*lat1;
  lat2 = (M_PI/180)*lat2;
  lon1 = (M_PI/180)*lon1;
  lon2 = (M_PI/180)*lon2;

  lat = atan((sin(lat1)*cos(lat2)*sin(lon-lon2)
        -sin(lat2)*cos(lat1)*sin(lon-lon1))
        /(cos(lat1)*cos(lat2)*sin(lon1-lon2)));

  *plat = (180/M_PI)*lat;
  *plon = (180/M_PI)*lon;
}

//
//  Lat/lon given radial and distance
//
//  A point {lat,lon} is a distance d out on the tc radial from point 1 if:
//
//     lat=asin(sin(lat1)*cos(d)+cos(lat1)*sin(d)*cos(tc))
//     IF (cos(lat)=0)
//        lon=lon1      // endpoint a pole
//     ELSE
//        lon=mod(lon1-asin(sin(tc)*sin(d)/cos(lat))+pi,2*pi)-pi
//     ENDIF
//
//  This algorithm is limited to distances such that dlon <pi/2, i.e those that extend 
//  around less than one quarter of the circumference of the earth in longitude. 
//  A completely general, but more complicated algorithm is necessary if greater distances 
//  are allowed: 
//
//     lat =asin(sin(lat1)*cos(d)+cos(lat1)*sin(d)*cos(tc))
//     dlon=atan2(sin(tc)*sin(d)*cos(lat1),cos(d)-sin(lat1)*sin(lat))
//     lon=mod( lon1-dlon +pi,2*pi )-pi
//
void GreatCircle::getRadialDmePoint(double *plat, double *plon, double lat1, double lon1, double dme, double radial)
{
  // convert angles to radians
  lat1 = (M_PI/180)*lat1;
  lon1 = (M_PI/180)*lon1;
  radial = (M_PI/180)*radial;

  double lat, lon;
  double dlon;

  
  // general version
  lat  = asin(sin(lat1)*cos(dme)+cos(lat1)*sin(dme)*cos(radial));
  dlon = atan2(sin(radial)*sin(dme)*cos(lat1), cos(dme)-sin(lat1)*sin(lat));
  lon  = mod(lon1 - dlon + M_PI, 2*M_PI) - M_PI;
    
  /*
  //simple version
  lat = asin(sin(lat1)*cos(dme)+cos(lat1)*sin(dme)*cos(radial));
  if (cos(lat) == 0)
    lon = lon1;      // endpoint a pole
  else
    lon = mod(lon1 - asin(sin(radial)*sin(dme)/cos(lat))+M_PI, 2*M_PI)-M_PI;
  */  
     
  *plat = (180/M_PI)*lat;
  *plon = (180/M_PI)*lon;
}




