//-----------------------------------------------------------------------------
// $Id: EnvelopeWidget.cpp,v 1.10 2005/03/01 10:16:27 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the 
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786


//#include "EnvelopeWidget.h"
#include <stdlib.h>
#include <qwidget.h>
#include <qpainter.h>
#include <qstring.h>
#include <math.h>

#include "EnvelopeWidget.h"
#include "iniFile.h"
#include "umath.h"


class RPoint
{
public:
  RPoint(void) {}
  RPoint(double xpos, double ypos) { m_x = xpos; m_y = ypos; }
  double x() { return m_x; }
  double y() { return m_y; }

private:
  double m_x;
  double m_y;
};

//-----------------------------------------------------------------------------
// Default constructor
//
EnvelopeWidget::EnvelopeWidget(QWidget *parent, const char *name, WFlags f)
    :QWidget(parent, name, f)
{
  MaxX = -9999;
  MinX =  9999;
  MaxY = -9999;
  MinY =  9999;
  setBackgroundMode(PaletteLight);
}

void EnvelopeWidget::cvtPt(int *x, int *y, double rx, double ry, QRect r)
{
  //*x = (int)(0 +          (r.width() - (BORDER*2))/(MaxX - MinX)*(rx - MinX) + BORDER);
  //*y = (int)(r.height() - (r.height()- (BORDER*2))/(MaxY - MinY)*(ry - MinY) - BORDER);
  *x = (int)(r.left() +   (r.width() - (BORDER*2))/(MaxX - MinX)*(rx - MinX) + BORDER);
  *y = (int)(r.bottom() - (r.height()- (BORDER*2))/(MaxY - MinY)*(ry - MinY) - BORDER);
  
  /*
  int a;
  a = r.height();
  a = r.bottom();
  a = r.top();
  a = r.left();
  */
}

int iNrPoints;

//-----------------------------------------------------------------------------
// Paint - the main paint method
//
void EnvelopeWidget::paintEvent(QPaintEvent* e)
{
  QPainter p(this);
  p.setClipRect(e->rect());
  PaintMain((GraphicPainter *) &p);  // the whole echilada
}

void EnvelopeWidget::PaintMain(GraphicPainter *gc)
{
  int fmHeight = gc->fontMetrics().height();
  int fmHeight2 = fmHeight / 2;

  QRect r = gc->window();

  //r.setHeight(r.height() - 8*fmHeight);
  //r.setWidth(r.width() - 8*fmHeight);         

  if (r.width() > 500) {
    int wid = r.width();
    r.setLeft(wid/4);
    r.setWidth(wid/2);
    //r.setRight(r.width() * 3/4);

    r.setTop(wid/4);
    r.setHeight(wid/2);
    //r.setBottom(r.width() * 3/4);
  }

  //r.setRight(170);

  // just so we van see
  /*p.setPen(Qt::white);
  p.setBrush(Qt::white);
  p.drawRect(r);*/

  RPoint tbl[256];

  int x, y;
  int i = 0;

  // Reading the profile file - todo
  char ss[256];
  std::string szData;
  std::string szItem;
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  std::string sLastAd = iniFile.GetValue("Prefs", "LastUsedAd", "");
  if (sLastAd.empty())
    return;

  CIniFile ad_iniFile(sLastAd); ad_iniFile.ReadFile();

  double rArm, rWeight;
  i = 0;
  while (true) {
    szItem = QString("%1").arg(i).latin1();
    szData = ad_iniFile.GetValue("CGEnvelope", szItem, "");

    if (szData.length() == 0)
      break;

    strcpy(ss, szData.c_str());
    char *sp = strtok(ss, ",");
    rArm = atof(sp);
    sp = strtok(NULL, ",");
    if (sp) {
      rWeight = atof(sp);
      tbl[i] = RPoint(rArm, rWeight);

      if (rArm > MaxX) MaxX = rArm;
      if (rArm < MinX) MinX = rArm;
      if (rWeight > MaxY) MaxY = rWeight;
      if (rWeight < MinY) MinY = rWeight;
    }
    i++;
  }
  iNrPoints = i;


  // Envelope
  {
    QPointArray ptArray(iNrPoints);

    for (i = 0; i < iNrPoints; i++) {
      cvtPt(&x, &y, tbl[i].x(), tbl[i].y(), r);
      ptArray[i] = QPoint(x, y);
    }
    //Polygon polygon = new Polygon(xPoints, yPoints, iNrPoints);

    /*
    // If the rArm/rWeight is outside the polygon use dark gray
    // otherwise use light gray for the envelope
    cvtPt(&x, &y, rArm, rWeight, r);
    if (polygon.contains(x, y)) {
      g2d.setColor(Color.lightGray);
      p.setBrush(Qt::green);
    }
    else {
      g2d.setColor(Color.darkGray);
    }
    */
    //p.drawPolygon(ptArray, 0, iNrPoints);
    gc->drawPolygon(ptArray);//, 0, iNrPoints);

    // g2d.fill(polygon);
    // g2d.setColor(Color.black/*lightGray*/);
    // g2d.draw(polygon);
  }

  // Point - Zero Fuel
  {
    gc->setPen(QPen(Qt::black, 3));

    cvtPt(&x, &y, MinX, m_ZeroFuelWeight, r);
    gc->moveTo(x, y);
    cvtPt(&x, &y, m_ZeroFuelArm, m_ZeroFuelWeight, r);
    gc->lineTo(x, y);
    cvtPt(&x, &y, m_ZeroFuelArm, MinY, r);
    gc->lineTo(x, y);

    gc->setPen(QPen(Qt::black, 1));
    gc->setBrush(Qt::red);
    cvtPt(&x, &y, m_ZeroFuelArm, m_ZeroFuelWeight, r);
    
    //gc->drawEllipse(x-7, y-7, 14, 14);
    gc->drawEllipse(x-fmHeight2, y-fmHeight2, fmHeight, fmHeight);
  }

  // Point - Loaded
  {
    gc->setPen(QPen(Qt::black, 3));

    cvtPt(&x, &y, MinX, m_Weight, r);
    gc->moveTo(x, y);
    cvtPt(&x, &y, m_Arm, m_Weight, r);
    gc->lineTo(x, y);

    cvtPt(&x, &y, m_Arm, MinY, r);
    gc->lineTo(x, y);

    cvtPt(&x, &y, m_Arm, m_Weight, r);
    gc->setPen(QPen(Qt::black, 1));
    gc->setBrush(Qt::green);
    cvtPt(&x, &y, m_Arm, m_Weight, r);

    //gc->drawEllipse(x-7, y-7, 14, 14);
    gc->drawEllipse(x-fmHeight2, y-fmHeight2, fmHeight, fmHeight);
  }


  // Label the axis
  gc->setPen(QPen(Qt::black, 1));
  cvtPt(&x, &y, MinX, MinY, r);
  gc->drawText(x+2, y-5, QString("%1").arg(MinX));

  cvtPt(&x, &y, MaxX, MinY, r);
  gc->drawText(x+2, y-5, QString("%1").arg(MaxX));

  cvtPt(&x, &y, MinX, MinY, r);
  //gc->drawText(x+5, y+15, QString("%1").arg(MinY));
  gc->drawText(x+5, y+fmHeight, QString("%1").arg(MinY));

  cvtPt(&x, &y, MinX, MaxY, r);
  //gc->drawText(x+5, y-15, QString("%1").arg(MaxY));
  gc->drawText(x+5, y-fmHeight, QString("%1").arg(MaxY));

  // Print the totals
  cvtPt(&x, &y, MinX, MinY, r);
  //gc->drawText(x+50,  y+28, QString("Weight = %1     Arm = %1").arg(utrunc(m_Weight)).arg(utrunc(m_Arm)));
  gc->drawText(x+fmHeight*4,  y+fmHeight*2, QString("Weight = %1     Arm = %1").arg(utrunc(m_Weight)).arg(utrunc(m_Arm)));

}















/*
  final static BasicStroke basicStroke  = new BasicStroke(1.0f);
  final static BasicStroke wideStroke   = new BasicStroke(3.0f);
  final static float dash1[] = {2.0f};
  final static BasicStroke dashedStroke = new BasicStroke(1.0f,
                                                          BasicStroke.CAP_BUTT,
                                                          BasicStroke.JOIN_MITER,
                                                          10.0f, dash1, 0.0f);

  double rArm;
  double rWeight;
  double ZFArm;
  double ZFWeight;

  double MaxX = -9999;
  double MinX = 9999;
  double MaxY = -9999;
  double MinY = 9999;
  */



/*

public:
  double rArm;
  double rWeight;
  double ZFArm;
  double ZFWeight;

// Operations
public:
  double MaxX;
  double MinX;
  double MaxY;
  double MinY;



// WhiteStatic.cpp : implementation file
//

#include "stdafx.h"
#include "WhiteStatic.h"
#include "ucolor.h"
#include "MapWnd.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CWhiteStatic

CWhiteStatic::CWhiteStatic()
{
  MaxX = -9999;
  MinX = 9999;
  MaxY = -9999;
  MinY = 9999;
}

CWhiteStatic::~CWhiteStatic()
{
}


BEGIN_MESSAGE_MAP(CWhiteStatic, CStatic)
  //{{AFX_MSG_MAP(CWhiteStatic)
  ON_WM_ERASEBKGND()
  ON_WM_PAINT()
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CWhiteStatic message handlers


BOOL CWhiteStatic::OnEraseBkgnd(CDC* pDC)
{
  CRect rect;

  GetClientRect(&rect);
  pDC->PatBlt(0, 0, rect.Width(), rect.Height(), WHITENESS);

  return TRUE;  // message handled
  //return CWnd::OnEraseBkgnd(pDC);
}



typedef struct {
  double x;
  double y;
} dPoint;

#define BORDER 30

void CWhiteStatic::cvtPt(int *x, int *y, double rx, double ry, CRect r)
{
  *x = (r.right-r.left-(BORDER*2))/(MaxX-MinX)*(rx-MinX) + BORDER;
  *y = (r.bottom-r.top-(BORDER*2))/(MaxY-MinY)*(ry-MinY) + BORDER;
}

void CWhiteStatic::OnPaint()
{
  CPaintDC gc(this); // device context for painting
  CRect r;
  CFont Font, *OldFont;

  // Twin Commanche
  dPoint tbl[] = {{86.5, 2650},
                  {86.5, 3600},
                  {87.6, 3700},
                  {88.3, 3800},
                  {89.0, 3900},
                  {90.5, 3900},
                  {91.0, 3800},
                  {91.4, 3700},
                  {92.0, 3650},
                  {92.0, 2650},
                  {86.5, 2650}};
  int iNrPoints = 11;
  //rArm = 90.0;
  //rWeight = 3425.0;

  int x, y;


  SetupFont(&Font, 14);
  OldFont = (CFont*) gc.SelectObject(&Font);


  char ss[256];
  char szData[256];
  char szItem[4];
  int i = 0;
  char *sp;
  int rv;

  double arm, weight;

  i = 0;
  while (TRUE) {
    itoa(i, szItem, 10);
    rv = GetPrivateProfileString("CGEnvelope", szItem, "", szData, 255, ".\\current.ad.tmp");
    if (rv <= 0)
        break;

    strcpy(ss, szData);
    sp = strtok(ss, ",");
    arm = atof(sp);
    sp = strtok(NULL, ",");
    weight = atof(sp);
    tbl[i].x = arm;
    tbl[i].y = weight;

    if (tbl[i].x > MaxX) MaxX = tbl[i].x;
    if (tbl[i].x < MinX) MinX = tbl[i].x;
    if (tbl[i].y > MaxY) MaxY = tbl[i].y;
    if (tbl[i].y < MinY) MinY = tbl[i].y;
    i++;
  }

  iNrPoints = i;

  GetClientRect(&r);

  gc.SetMapMode(MM_ANISOTROPIC);
  CSize ext = gc.GetViewportExt();
  gc.SetViewportExt(1, -1);
  gc.SetViewportOrg(0, r.bottom);

  // Envelope
  {
    CBrush Brush(RGB_LTGRAY);
    CPen   Pen(PS_SOLID, 1, RGB_LTGRAY);
    gc.SelectObject(Brush);
    gc.SelectObject(Pen);
    CPoint pts[255];

    for (i = 0; i < iNrPoints; i++) {
      cvtPt(&x, &y, tbl[i].x, tbl[i].y, r);
      pts[i].x = x;
      pts[i].y = y;
    }
    gc.Polygon(pts, iNrPoints);
  }

  // Point - Zero Fuel
  {
    CBrush Brush(RGB_LTRED);
    CPen   Pen2(PS_SOLID, 2, RGB_BLACK);
    CPen   Pen1(PS_SOLID, 0, RGB_BLACK);
    gc.SelectObject(Brush);
    gc.SelectObject(Pen2);

    cvtPt(&x, &y, MinX, ZFWeight, r);
    gc.MoveTo(x, y);
    cvtPt(&x, &y, ZFArm, ZFWeight, r);
    gc.LineTo(x, y);
    cvtPt(&x, &y, ZFArm, MinY, r);
    gc.LineTo(x, y);

    gc.SelectObject(Pen1);
    cvtPt(&x, &y, ZFArm, ZFWeight, r);
    gc.Ellipse(x-7, y-7, x+7, y+7);

  }

  // Point - Loaded
  {
    CBrush Brush(RGB_LTGREEN);
    CPen   Pen2(PS_SOLID, 2, RGB_BLACK);
    CPen   Pen1(PS_SOLID, 0, RGB_BLACK);
    gc.SelectObject(Brush);
    gc.SelectObject(Pen2);

    cvtPt(&x, &y, MinX, rWeight, r);
    gc.MoveTo(x, y);
    cvtPt(&x, &y, rArm, rWeight, r);
    gc.LineTo(x, y);
    cvtPt(&x, &y, rArm, MinY, r);
    gc.LineTo(x, y);

    gc.SelectObject(Pen1);
    cvtPt(&x, &y, rArm, rWeight, r);
    gc.Ellipse(x-7, y-7, x+7, y+7);

  }


  // Label the axis
  cvtPt(&x, &y, MinX, MinY, r);
  sprintf(ss, "%.1f", MinX);
  gc.TextOut(x, y-5, ss);

  cvtPt(&x, &y, MaxX, MinY, r);
  sprintf(ss, "%.1f", MaxX);
  gc.TextOut(x, y-5, ss);

  cvtPt(&x, &y, MinX, MinY, r);
  sprintf(ss, "%.0f", MinY);
  gc.TextOut(x+5, y+15, ss);

  cvtPt(&x, &y, MinX, MaxY, r);
  sprintf(ss, "%.0f", MaxY);
  gc.TextOut(x+5, y+15, ss);


}

Long EZ
          rWeight Station Moment
Empty     730    111.7  81541
Oil       8      140.0  1120
Fuel      150    104.5  15675
Pilot     210    59.0   12390
Pax       210    103.0  21630
Baggage   15     90.0   1350
Total     1323   101.06 133706


1  97  900
2  97 1325
3 104 1325
4 104  900
5  97  900

6  97 1425
7 104 1425
8 104  900
9  97  900


*/
