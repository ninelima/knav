/****************************************************************************
** Form implementation generated from reading ui file 'mydialog.ui'
**
** Created: Sun Jun 22 00:56:37 2003
**      by:  The User Interface Compiler (uic)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "mydialog.h"

#include <qiconview.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>
#include <qimage.h>
#include <qpixmap.h>

static const char* const image0_data[] = { 
"32 32 10 1",
". c None",
"# c #000000",
"b c #00c000",
"e c #303030",
"h c #400000",
"d c #585858",
"g c #a0a0a4",
"f c #c0c0c0",
"a c #c0ffc0",
"c c #ffffff",
"...###..........................",
"...#aa##........................",
".###baaa##......................",
".#cd##baaa##....................",
".#cccd##baaa##..##e.............",
".#cccccd##baaa##aaa##...........",
".#cccccccd##baaaaaaaa##.........",
".#cccccccccd##baaaaaaa#.........",
".#cccccfcffgg#bbbbaaaaa#........",
".#ccccccfcfffd#bbbbbbaa#........",
".#cccfcfcfcffgd##bh#bbba#.......",
".#ccccfcfffffffgd#dd##ba#.......",
".#cfcfcfcfffffffffgfgd#bb#......",
".#ccfcfffffffffgfgfgfgd#b#......",
".#cfcfcfffffffffgfgfggd#b#......",
".#fcfffffffffgfgfgggggd#b#......",
".#cfcfffffffffgfgfggggd#b#......",
".#fffffffffgfgfgggggggdhb#......",
".#ffffffffffgfgfggggggd#b#......",
".#ggfffffgfgfgggggggggd#b#......",
".#ddggffffgfgfggggggggd#b#......",
"..##ddggfgfgggggggggggd#b#......",
"....##ddgggfggggggggggd#b#......",
"......##ddggggggggggggd#b#......",
"........##ddggggggggggd#b#......",
"..........##ddggggggggd#b#......",
"............##ddggggggd#b###....",
"..............##ddggggd#b#####..",
"................##ddggd#b######.",
"..................##ddd#b#####..",
"....................##d#b###....",
"......................####......"};

static const char* const image1_data[] = { 
"15 15 3 1",
"# c #000000",
"a c #00ff00",
". c #c0c0c0",
"......##.......",
"...a##aa##a....",
"..##aaaaaa##...",
".a#aaaaaaaaa#..",
".#aaaaaaaaaa#..",
".#aaaaaaaaaaa#.",
"#aaaaaaaaaaaa#.",
"#aaaaaaaaaaaaa#",
"#aaaaaaaaaaaa#.",
".#aaaaaaaaaa#..",
".#aaaaaaaaaa#..",
"..#aaaaaaaa#...",
"...##aaaaa#....",
".....##a##.....",
".......#......."};

static const char* const image2_data[] = { 
"15 14 3 1",
"# c #000000",
". c #c0c0c0",
"a c #ff0000",
"......##.......",
"...a##aa##a....",
"..##aaaaaa##...",
".a#aaaaaaaaa#..",
".#aaaaaaaaaa#..",
".#aaaaaaaaaaa#.",
"#aaaaaaaaaaaa#.",
"#aaaaaaaaaaaaa#",
"#aaaaaaaaaaaa#.",
".#aaaaaaaaaa#..",
".#aaaaaaaaaa#..",
"..#aaaaaaaa#...",
"...##aaaaa#....",
".....##a##....."};


/* 
 *  Constructs a MyDialog which is a child of 'parent', with the 
 *  name 'name' and widget flags set to 'f' 
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
MyDialog::MyDialog( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    QPixmap image0( ( const char** ) image0_data );
    QPixmap image1( ( const char** ) image1_data );
    QPixmap image2( ( const char** ) image2_data );
    if ( !name )
	setName( "MyDialog" );
    resize( 634, 440 ); 
    setCaption( tr( "MyDialog" ) );
    setSizeGripEnabled( TRUE );
    MyDialogLayout = new QVBoxLayout( this ); 
    MyDialogLayout->setSpacing( 6 );
    MyDialogLayout->setMargin( 11 );

    Layout22 = new QHBoxLayout; 
    Layout22->setSpacing( 6 );
    Layout22->setMargin( 0 );

    Layout20 = new QVBoxLayout; 
    Layout20->setSpacing( 6 );
    Layout20->setMargin( 0 );

    Layout17 = new QGridLayout; 
    Layout17->setSpacing( 6 );
    Layout17->setMargin( 0 );

    item_2 = new QLabel( this, "item_2" );
    item_2->setText( tr( "item" ) );

    Layout17->addWidget( item_2, 1, 0 );

    arm_2 = new QLabel( this, "arm_2" );
    arm_2->setText( tr( "arm" ) );

    Layout17->addWidget( arm_2, 2, 1 );

    TextLabel2 = new QLabel( this, "TextLabel2" );
    TextLabel2->setText( tr( "Arm" ) );

    Layout17->addWidget( TextLabel2, 0, 3 );

    item_2_2 = new QLabel( this, "item_2_2" );
    item_2_2->setText( tr( "item" ) );

    Layout17->addWidget( item_2_2, 2, 0 );

    arm = new QLabel( this, "arm" );
    QFont arm_font(  arm->font() );
    arm_font.setFamily( "Courier" );
    arm->setFont( arm_font ); 
    arm->setText( tr( "arm" ) );

    Layout17->addWidget( arm, 1, 1 );

    TextLabel1 = new QLabel( this, "TextLabel1" );
    TextLabel1->setText( tr( "Item" ) );

    Layout17->addMultiCellWidget( TextLabel1, 0, 0, 0, 2 );

    TextLabel3 = new QLabel( this, "TextLabel3" );
    TextLabel3->setText( tr( "Weight" ) );

    Layout17->addWidget( TextLabel3, 0, 4 );

    weight_2 = new QLineEdit( this, "weight_2" );

    Layout17->addMultiCellWidget( weight_2, 2, 2, 2, 4 );

    weight = new QLineEdit( this, "weight" );
    QFont weight_font(  weight->font() );
    weight_font.setFamily( "Courier" );
    weight->setFont( weight_font ); 

    Layout17->addMultiCellWidget( weight, 1, 1, 2, 4 );
    Layout20->addLayout( Layout17 );
    QSpacerItem* spacer = new QSpacerItem( 20, 20, QSizePolicy::Minimum, QSizePolicy::Expanding );
    Layout20->addItem( spacer );
    Layout22->addLayout( Layout20 );

    IconView1 = new QIconView( this, "IconView1" );
    (void) new QIconViewItem( IconView1, tr( "New Item" ), image0 );
    IconView1->setMinimumSize( QSize( 150, 150 ) );
    Layout22->addWidget( IconView1 );
    MyDialogLayout->addLayout( Layout22 );

    Layout19 = new QHBoxLayout; 
    Layout19->setSpacing( 6 );
    Layout19->setMargin( 0 );

    PushButton61 = new QPushButton( this, "PushButton61" );
    PushButton61->setText( tr( "SelectAircraft" ) );
    Layout19->addWidget( PushButton61 );
    QSpacerItem* spacer_2 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout19->addItem( spacer_2 );

    TextLabel5 = new QLabel( this, "TextLabel5" );
    TextLabel5->setText( tr( "" ) );
    TextLabel5->setPixmap( image1 );
    Layout19->addWidget( TextLabel5 );

    TextLabel6 = new QLabel( this, "TextLabel6" );
    TextLabel6->setText( tr( "As Loaded" ) );
    Layout19->addWidget( TextLabel6 );

    TextLabel4 = new QLabel( this, "TextLabel4" );
    TextLabel4->setText( tr( "" ) );
    TextLabel4->setPixmap( image2 );
    Layout19->addWidget( TextLabel4 );

    TextLabel7 = new QLabel( this, "TextLabel7" );
    TextLabel7->setText( tr( "Zero Fuel" ) );
    Layout19->addWidget( TextLabel7 );
    MyDialogLayout->addLayout( Layout19 );

    Layout1 = new QHBoxLayout; 
    Layout1->setSpacing( 6 );
    Layout1->setMargin( 0 );

    buttonHelp = new QPushButton( this, "buttonHelp" );
    buttonHelp->setText( tr( "Edit" ) );
    buttonHelp->setAutoDefault( TRUE );
    Layout1->addWidget( buttonHelp );
    QSpacerItem* spacer_3 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout1->addItem( spacer_3 );

    buttonCancel = new QPushButton( this, "buttonCancel" );
    buttonCancel->setText( tr( "Dismiss" ) );
    buttonCancel->setAutoDefault( TRUE );
    Layout1->addWidget( buttonCancel );
    MyDialogLayout->addLayout( Layout1 );

    // signals and slots connections
    connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
    connect( weight, SIGNAL( textChanged(const QString&) ), weight, SLOT( setFocus() ) );
}

/*  
 *  Destroys the object and frees any allocated resources
 */
MyDialog::~MyDialog()
{
    // no need to delete child widgets, Qt does it all for us
}

/*  
 *  Main event handler. Reimplemented to handle application
 *  font changes
 */
bool MyDialog::event( QEvent* ev )
{
    bool ret = QDialog::event( ev ); 
    if ( ev->type() == QEvent::ApplicationFontChange ) {
	QFont arm_font(  arm->font() );
	arm_font.setFamily( "Courier" );
	arm->setFont( arm_font ); 
	QFont weight_font(  weight->font() );
	weight_font.setFamily( "Courier" );
	weight->setFont( weight_font ); 
    }
    return ret;
}

