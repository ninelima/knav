#ifndef __WPTSET_H
#define __WPTSET_H

#include "DelimRecordSet.h"
#include <string>

// WptSet.h : header file
//

// -> #define _NO_WPT_CACHE_
//#define _NO_WPT_CACHE_

#if _MOTIF_
#define _NO_WPT_CACHE_
#endif

 
#ifdef _NO_WPT_CACHE_

#pragma message ("fix cp for no cache")
 
//-----------------------------------------------------------------------------
// CWptSet Delimited recordset
//
class CWptSet : public CDelimRecordSet
{
public:
  //long cp;// = 0;
  CWptSet();
  ~CWptSet();

  void Bind();
 
  long  m_ID;
  QString m_ICAO;
  QString m_INFO;
  double  m_rElev;
  double  m_rLat;
  double  m_rLon;
  long  m_TYPE;
};


#else //_NO_WPT_CACHE_

//-----------------------------------------------------------------------------

//#define WPT_TOTAL 24000
#define WPT_TOTAL 15000

class QString;


class WptRec
//struct WptRec
{
public:

  long  m_ID;
  QString m_ICAO;
  QString m_INFO;
  double m_rElev;
  double m_rLat;
  double m_rLon;
  long   m_TYPE;
  long   m_nFields;

 
  /*
  WptRec()
  {
    // Field initialisation
    m_ID    = 0;
    m_ICAO  = "";
    m_INFO  = "";
    m_rElev = 0.0;
    m_rLat  = 0.0;
    m_rLon = 0.0;
    m_TYPE  = 0;

    // Number of fields
    m_nFields = 8;
  }
  */
};


class CWptSet// : public CDelimRecordSet
{

public:
  // We can use vectors, lists or whatever
  // but speed is king. A simple static array
  // is the best.
  WptRec tbl[WPT_TOTAL+1];

  int num;

private:
  int cp;


public:
  long   m_ID;
  QString m_ICAO;
  QString m_INFO;
  QString m_LAT;
  QString m_LONG;
  double m_rElev;
  double m_rLat;
  double m_rLon;
  long   m_TYPE;

  unsigned int m_nCurRec;

  CWptSet();
  ~CWptSet();

  void WptSetInit();
  bool Open(const char *sFileName);
  bool MoveFirst();
  bool MoveNext();
  bool Move(int idx);
  bool IsEOF();
  void Clear();
  void Bind();
  void Store();
  void Close() {};
  WptRec GetRecord();

  /*
  long GetCP();
  */

};


#endif //_MOTIF_


#endif // __WPTSET_H
