#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "../../ulib/umath.h"

#define LOWORD(l)  ((unsigned short)(l))
#define HIWORD(l)  ((unsigned short)(((unsigned long)(l) >> 16) & 0xFFFF))

#define LOBYTE(w)  ((unsigned char)(w))
#define HIBYTE(w)  ((unsigned char)(((unsigned short)(w) >> 8) & 0xFF))

char *gen(char *szbuff, char *name)
{
  int a, b, c;

  int aa = 1;
  for (int i = 0; i < strlen(name); i++) {
    aa *= name[i];
    aa %= 998;
    aa += 1;
  }

  a = aa;
  b = 2000;
  c = 0;

  while ((b > 999) ||
         (a*c == 0) ||
         (a == c)) {
    //a = randomInt(999);
    c = randomInt(999);
    b = (a^c);
  }

  sprintf(szbuff, "%03d-%03d-%03d", a, b, c);
  return szbuff;
}


int main(int argc, char ** argv)
{
  char sn[256];

  srand((unsigned)time(NULL));

  if (argc != 2) {  
    printf("\nUsage:\n codegen.exe <owner name>\n\n");
    return 0;
  }

  gen(sn, argv[1]);
  puts(sn);    

  return 0;
}
