
/**
 * Title:        @(#)
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

class WindTriangle
{
public:
#if 0
  /** Track also known as Course */
  static double m_TRK;
  /** Heading */
  static double m_HD;
  /** Wind direction (from) */
  static double m_WD;
  /** True airpeed */
  static double m_TAS;
  /** Groundspeed */
  static double m_GS;
  /** Wind speed */
  static double m_WS;
#endif

 
  static bool calcWdWs(void);
  static bool calcHdGs(double trk, double tas, double wd, double ws);
  static bool calcHdGs(double trk, double tas, int wind, double *hd, double *gs);
  static bool calcHdGs(double trk, double tas, double wd, double ws, double *hd, double *gs);
  static bool calcHdGs(double trk, double tas, int wind, int *hd, int *gs);

 
  /*
  static bool calcWdWs(void);
  static bool calcHdGs(double trk, double tas, double wd, double ws);
  static bool calcHdGs(double trk, double tas, int wind, double *hd, double *gs);
  static bool calcHdGs(double trk, double tas, double wd, double ws, double *hd, double *gs);
  */
};


