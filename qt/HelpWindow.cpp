/****************************************************************************
 ** $Id: HelpWindow.cpp,v 1.5 2006/04/23 10:52:31 player Exp $
 **
 ** Copyright (C) 1992-2002 Trolltech AS.  All rights reserved.
 **
 ** This file is part of an example program for Qt.  This example
 ** program may be used, distributed and modified without limitation.
 **
 *****************************************************************************/

#include "HelpWindow.h"
#include "HelpWindow.moc"

#include <qstatusbar.h>
#include <qpixmap.h>
#include <qpopupmenu.h>
#include <qmenubar.h>                 
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qiconset.h>
#include <qfile.h>
#include <qtextstream.h>
#include <qstylesheet.h>
#include <qmessagebox.h>
#include <qfiledialog.h>
#include <qapplication.h>
#include <qcombobox.h>
#include <qevent.h>
#include <qlineedit.h>
#include <qobjectlist.h>
#include <qfileinfo.h>
#include <qfile.h>
#include <qdatastream.h>
#include <qprinter.h>
#include <qsimplerichtext.h>
#include <qpainter.h>
#include <qpaintdevicemetrics.h>

#include <ctype.h>

#include <qhttp.h>
#include <qurl.h>

#include "back.xpm"
#include "forward.xpm"
#include "home.xpm"



HelpWindow::HelpWindow(const QString& sHome, const QString &sPath,
QWidget *parent, const char *name)
: QMainWindow( parent, name, WDestructiveClose ),
pathCombo( 0 )
{
	readHistory();
	readBookmarks();

	textbrowser = new QTextBrowser( this );

	textbrowser->mimeSourceFactory()->setFilePath( sPath );
	textbrowser->setFrameStyle( QFrame::Panel | QFrame::Sunken );
	connect( textbrowser, SIGNAL( sourceChanged(const QString& ) ),
		this, SLOT( sourceChanged( const QString&) ) );

	setCentralWidget( textbrowser );

	if ( !sHome.isEmpty() )
		textbrowser->setSource( sHome );

	connect( textbrowser, SIGNAL( highlighted( const QString&) ),
		statusBar(), SLOT( message( const QString&)) );

	resize( 640, 480 );

	QPopupMenu* file = new QPopupMenu( this );
	file->insertItem( tr("&New Window"), this, SLOT( newWindow() ), CTRL+Key_N );
	file->insertItem( tr("&Open File"), this, SLOT( openFile() ), CTRL+Key_O );
	file->insertItem( tr("&Print"), this, SLOT( print() ), CTRL+Key_P );
	file->insertSeparator();
	file->insertItem( tr("&Close"), this, SLOT( close() ), CTRL+Key_Q );
	file->insertItem( tr("E&xit"), qApp, SLOT( closeAllWindows() ), CTRL+Key_X );

	// The same three icons are used twice each.
	QIconSet icon_back( QPixmap("back.xpm") );
	QIconSet icon_forward( QPixmap("forward.xpm") );
	QIconSet icon_home( QPixmap("home.xpm") );
	/*QIconSet icon_back( QPixmap(back_xpm) );
	QIconSet icon_forward( QPixmap(forward_xpm) );
	QIconSet icon_home( QPixmap(home_xpm) );*/

	QPopupMenu* go = new QPopupMenu( this );
	backwardId = go->insertItem( icon_back,
		tr("&Backward"), textbrowser, SLOT( backward() ),
		CTRL+Key_Left );
	forwardId = go->insertItem( icon_forward,
		tr("&Forward"), textbrowser, SLOT( forward() ),
		CTRL+Key_Right );
	go->insertItem( icon_home, tr("&Home"), textbrowser, SLOT( home() ) );

	//QPopupMenu* help = new QPopupMenu( this );
	//help->insertItem( tr("&About"), this, SLOT( about() ) );
	//help->insertItem( tr("About &Qt"), this, SLOT( aboutQt() ) );

	hist = new QPopupMenu( this );
	QStringList::Iterator it = history.begin();
	for ( ; it != history.end(); ++it )
		mHistory[ hist->insertItem( *it ) ] = *it;
	connect( hist, SIGNAL( activated( int ) ),
		this, SLOT( histChosen( int ) ) );

	bookm = new QPopupMenu( this );
	bookm->insertItem( tr( "Add Bookmark" ), this, SLOT( addBookmark() ) );
	bookm->insertSeparator();

	QStringList::Iterator it2 = bookmarks.begin();
	for ( ; it2 != bookmarks.end(); ++it2 )
		mBookmarks[ bookm->insertItem( *it2 ) ] = *it2;
	connect( bookm, SIGNAL( activated( int ) ),
		this, SLOT( bookmChosen( int ) ) );

	menuBar()->insertItem( tr("&File"), file );
	menuBar()->insertItem( tr("&Go"), go );
	menuBar()->insertItem( tr( "History" ), hist );
	menuBar()->insertItem( tr( "Bookmarks" ), bookm );
	menuBar()->insertSeparator();
	//menuBar()->insertItem( tr("&Help"), help );

	menuBar()->setItemEnabled( forwardId, FALSE);
	menuBar()->setItemEnabled( backwardId, FALSE);
	connect( textbrowser, SIGNAL( backwardAvailable( bool ) ), this, SLOT( setBackwardAvailable( bool ) ) );
	connect( textbrowser, SIGNAL( forwardAvailable( bool ) ), this, SLOT( setForwardAvailable( bool ) ) );

	QToolBar* toolbar = new QToolBar( this );
	addToolBar( toolbar, "Toolbar");
	QToolButton* button;

	button = new QToolButton( icon_back, tr("Backward"), "", textbrowser, SLOT(backward()), toolbar );
	connect( textbrowser, SIGNAL( backwardAvailable(bool) ), button, SLOT( setEnabled(bool) ) );
	button->setEnabled( FALSE );
	button = new QToolButton( icon_forward, tr("Forward"), "", textbrowser, SLOT(forward()), toolbar );
	connect( textbrowser, SIGNAL( forwardAvailable(bool) ), button, SLOT( setEnabled(bool) ) );
	button->setEnabled( FALSE );
	button = new QToolButton( icon_home, tr("Home"), "", textbrowser, SLOT(home()), toolbar );

	toolbar->addSeparator();

	pathCombo = new QComboBox( TRUE, toolbar );
	connect( pathCombo, SIGNAL( activated( const QString & ) ), this, SLOT( pathSelected( const QString & ) ) );
	toolbar->setStretchableWidget( pathCombo );
	setRightJustification( TRUE );
	setDockEnabled( DockLeft, FALSE );
	setDockEnabled( DockRight, FALSE );

	pathCombo->insertItem( sHome );
	textbrowser->setFocus();

	connect(&articleFetcher, SIGNAL(done(bool)), this, SLOT(fetchDone(bool)));
}


void HelpWindow::setBackwardAvailable( bool b)
{
	menuBar()->setItemEnabled( backwardId, b);
}


void HelpWindow::setForwardAvailable( bool b)
{
	menuBar()->setItemEnabled( forwardId, b);
}


void HelpWindow::sourceChanged( const QString& url )
{
	if ( textbrowser->documentTitle().isNull() )
		setCaption( "Qt Example - Helpviewer - " + url );
	else
		setCaption( "Qt Example - Helpviewer - " + textbrowser->documentTitle() ) ;

	if ( !url.isEmpty() && pathCombo ) {
		bool exists = FALSE;
		int i;
		for ( i = 0; i < pathCombo->count(); ++i ) {
			if ( pathCombo->text( i ) == url ) {
				exists = TRUE;
				break;
			}
		}
		if ( !exists ) {
			pathCombo->insertItem( url, 0 );
			pathCombo->setCurrentItem( 0 );
			mHistory[ hist->insertItem( url ) ] = url;
		} else
		pathCombo->setCurrentItem( i );
	}

	//setSourceUrl(url);

}


HelpWindow::~HelpWindow()
{
	history =  mHistory.values();

	QFile f( QDir::currentDirPath() + "/.history" );
	f.open( IO_WriteOnly );
	QDataStream s( &f );
	s << history;
	f.close();

	bookmarks = mBookmarks.values();

	QFile f2( QDir::currentDirPath() + "/.bookmarks" );
	f2.open( IO_WriteOnly );
	QDataStream s2( &f2 );
	s2 << bookmarks;
	f2.close();
}


void HelpWindow::about()
{
	QMessageBox::about( this, "HelpViewer Example",
		"<p>This example implements a simple HTML help viewer "
		"using Qt's rich text capabilities</p>"
		"<p>It's just about 400 lines of C++ code, so don't expect too much :-)</p>"
		);
}


void HelpWindow::aboutQt()
{
	QMessageBox::aboutQt( this, "QBrowser" );
}


void HelpWindow::openFile()
{
#ifndef QT_NO_FILEDIALOG
	QString fn = QFileDialog::getOpenFileName( QString::null, QString::null, this );
	if ( !fn.isEmpty() )
		textbrowser->setSource( fn );
#endif
}


void HelpWindow::setSourceFile(const QString& name)
{
	textbrowser->setSource(name);
}
                           

// same as "navigate"
void HelpWindow::setSourceUrl(const QString& name)
{
	QUrl u(name);
	articleFetcher.setHost(u.host());
	articleFetcher.get(name);
}


void HelpWindow::fetchDone( bool error )
{
	if (error) {
		QMessageBox::critical(this, "Error fetching",
			"An error occurred when fetching this document: "
			+ articleFetcher.errorString(),
			QMessageBox::Ok, QMessageBox::NoButton);
	}
	else {
		textbrowser->setText(articleFetcher.readAll());
	}
}


void HelpWindow::newWindow()
{
	( new HelpWindow(textbrowser->source(), "qbrowser") )->show();
}


void HelpWindow::hardcopy()
{
	print();
}


void HelpWindow::print()
{
#ifndef QT_NO_PRINTER
	QPrinter printer( QPrinter::HighResolution );
	printer.setFullPage(TRUE);
	if ( printer.setup( this ) ) {
		QPainter p( &printer );
		if( !p.isActive() )														// starting printing failed
			return;
		QPaintDeviceMetrics metrics(p.device());
		int dpiy = metrics.logicalDpiY();
		int margin = (int) ( (2/2.54)*dpiy );					// 2 cm margins
		QRect body( margin, margin, metrics.width() - 2*margin, metrics.height() - 2*margin );
		QSimpleRichText richText( textbrowser->text(),
			QFont(),
			textbrowser->context(),
			textbrowser->styleSheet(),
			textbrowser->mimeSourceFactory(),
			body.height() );
		richText.setWidth( &p, body.width() );
		QRect view( body );
		int page = 1;
		do {
			richText.draw( &p, body.left(), body.top(), view, colorGroup() );
			view.moveBy( 0, body.height() );
			p.translate( 0 , -body.height() );
			p.drawText( view.right() - p.fontMetrics().width( QString::number(page) ),
				view.bottom() + p.fontMetrics().ascent() + 5, QString::number(page) );
			if ( view.top()  >= richText.height() )
				break;
			printer.newPage();
			page++;
		} while (TRUE);
	}
#endif
}


void HelpWindow::pathSelected( const QString &path )
{
	textbrowser->setSource( path );
	if ( mHistory.values().contains(path) )
		mHistory[ hist->insertItem( path ) ] = path;
}


void HelpWindow::readHistory()
{
	if ( QFile::exists( QDir::currentDirPath() + "/.history" ) ) {
		QFile f( QDir::currentDirPath() + "/.history" );
		f.open( IO_ReadOnly );
		QDataStream s( &f );
		s >> history;
		f.close();
		while ( history.count() > 20 )
			history.remove( history.begin() );
	}
}


void HelpWindow::readBookmarks()
{
	if ( QFile::exists( QDir::currentDirPath() + "/.bookmarks" ) ) {
		QFile f( QDir::currentDirPath() + "/.bookmarks" );
		f.open( IO_ReadOnly );
		QDataStream s( &f );
		s >> bookmarks;
		f.close();
	}
}


void HelpWindow::histChosen( int i )
{
	if ( mHistory.contains( i ) )
		textbrowser->setSource( mHistory[ i ] );
}


void HelpWindow::bookmChosen( int i )
{
	if ( mBookmarks.contains( i ) )
		textbrowser->setSource( mBookmarks[ i ] );
}


void HelpWindow::addBookmark()
{
	mBookmarks[ bookm->insertItem( caption() ) ] = textbrowser->context();
}
