/****************************************************************************
** ui.h extension file, included from the uic-generated form implementation.
**
** If you want to add, delete, or rename functions or slots, use
** Qt Designer to update this file, preserving your code.
**
** You should not define a constructor or destructor in this file.
** Instead, write your code in functions called init() and destroy().
** These will automatically be called by the form's constructor and
** destructor.
*****************************************************************************/


void PlayControl::init()
{
#ifdef BUILD_RAILVIEW
  HudGadget1->hide();
  tsiGadget1->hide();
  vsiGadget1->hide();
#endif // BUILD_RAILVIEW
}


void PlayControl::pushButtonRew_clicked()
{																								
  m_Rewind = 1;
}


void PlayControl::pushButtonBack_clicked()
{
  m_Play = 0;
  m_Back = 1;
}


void PlayControl::pushButtonPlay_clicked()
{
  if (pushButtonPlay->state() == QButton::On)
    m_Play = 1;
  else
    m_Play = 0;
}


void PlayControl::pushButtonFwd_clicked()
{
  m_Fwd = 1;
}


void PlayControl::pushButtonPause_clicked()
{
  m_Play = 0;
  m_Closed = 1;
}


void PlayControl::reject()
{
  m_Closed = 1;
  QDialog::reject();
}


void PlayControl::hideButtons()
{
  pushButtonRew->hide();
  pushButtonBack->hide();
  pushButtonPlay->hide();
  pushButtonFwd->hide();
  pushButtonExit->hide();
  //slider1->hide();
}


void PlayControl::showButtons()
{
  pushButtonRew->show();
  pushButtonBack->show();
  pushButtonPlay->show();
  pushButtonFwd->show();
  pushButtonExit->show();
  //slider1->show();

  m_Closed = 0;
}

void PlayControl::checkBoxSixPack_clicked()
{
  if (checkBoxSixPack->isChecked()) {
    showSixPack();
  }
  else {
    hideSixPack();
  }
}


void PlayControl::hideSixPack()
{
  frameSixPack->hide();
}

void PlayControl::showSixPack()
{
  frameSixPack->show();
}
