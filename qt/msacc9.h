//
// StdAfx.h imports
//
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN    // Exclude rarely-used stuff from Windows headers

#include <afxdisp.h>        // MFC Automation classes
#ifndef _AFX_NO_AFXCMN_SUPPORT
#endif // _AFX_NO_AFXCMN_SUPPORT
#define WM_NEWREPORT (WM_USER + 1)





// Machine generated IDispatch wrapper class(es) created with ClassWizard
/////////////////////////////////////////////////////////////////////////////
// DoCmd wrapper class

class DoCmd : public COleDispatchDriver
{
public:
  DoCmd() {}    // Calls COleDispatchDriver default constructor
  DoCmd(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  DoCmd(const DoCmd& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  void AddMenu(const VARIANT& MenuName, const VARIANT& MenuMacroName, const VARIANT& StatusBarText);
  void ApplyFilter(const VARIANT& FilterName, const VARIANT& WhereCondition);
  void Beep();
  void CancelEvent();
  void Close(long ObjectType, const VARIANT& ObjectName, long Save);
  void CopyObject(const VARIANT& DestinationDatabase, const VARIANT& NewName, long SourceObjectType, const VARIANT& SourceObjectName);
  void DoMenuItem(const VARIANT& MenuBar, const VARIANT& MenuName, const VARIANT& Command, const VARIANT& Subcommand, const VARIANT& Version);
  void Echo(const VARIANT& EchoOn, const VARIANT& StatusBarText);
  void FindNext();
  void FindRecord(const VARIANT& FindWhat, long Match, const VARIANT& MatchCase, long Search, const VARIANT& SearchAsFormatted, long OnlyCurrentField, const VARIANT& FindFirst);
  void GoToControl(const VARIANT& ControlName);
  void GoToPage(const VARIANT& PageNumber, const VARIANT& Right, const VARIANT& Down);
  void GoToRecord(long ObjectType, const VARIANT& ObjectName, long Record, const VARIANT& Offset);
  void Hourglass(const VARIANT& HourglassOn);
  void Maximize();
  void Minimize();
  void MoveSize(const VARIANT& Right, const VARIANT& Down, const VARIANT& Width, const VARIANT& Height);
  void OpenForm(const VARIANT& FormName, long View, const VARIANT& FilterName, const VARIANT& WhereCondition, long DataMode, long WindowMode, const VARIANT& OpenArgs);
  void OpenQuery(const VARIANT& QueryName, long View, long DataMode);
  void OpenTable(const VARIANT& TableName, long View, long DataMode);
  void PrintOut(long PrintRange, const VARIANT& PageFrom, const VARIANT& PageTo, long PrintQuality, const VARIANT& Copies, const VARIANT& CollateCopies);
  void Quit(long Options);
  void Requery(const VARIANT& ControlName);
  void RepaintObject(long ObjectType, const VARIANT& ObjectName);
  void Rename(const VARIANT& NewName, long ObjectType, const VARIANT& OldName);
  void Restore();
  void RunMacro(const VARIANT& MacroName, const VARIANT& RepeatCount, const VARIANT& RepeatExpression);
  void RunSQL(const VARIANT& SQLStatement, const VARIANT& UseTransaction);
  void SelectObject(long ObjectType, const VARIANT& ObjectName, const VARIANT& InDatabaseWindow);
  void SetWarnings(const VARIANT& WarningsOn);
  void ShowAllRecords();
  void OpenReport(const VARIANT& ReportName, long View, const VARIANT& FilterName, const VARIANT& WhereCondition);
  void TransferDatabase(long TransferType, const VARIANT& DatabaseType, const VARIANT& DatabaseName, long ObjectType, const VARIANT& Source, const VARIANT& Destination, const VARIANT& StructureOnly, const VARIANT& StoreLogin);
  void TransferSpreadsheet(long TransferType, long SpreadsheetType, const VARIANT& TableName, const VARIANT& FileName, const VARIANT& HasFieldNames, const VARIANT& Range, const VARIANT& UseOA);
  void TransferText(long TransferType, const VARIANT& SpecificationName, const VARIANT& TableName, const VARIANT& FileName, const VARIANT& HasFieldNames, const VARIANT& HTMLTableName, const VARIANT& CodePage);
  void OutputTo(long ObjectType, const VARIANT& ObjectName, const VARIANT& OutputFormat, const VARIANT& OutputFile, const VARIANT& AutoStart, const VARIANT& TemplateFile);
  void DeleteObject(long ObjectType, const VARIANT& ObjectName);
  void OpenModule(const VARIANT& ModuleName, const VARIANT& ProcedureName);
  void SendObject(long ObjectType, const VARIANT& ObjectName, const VARIANT& OutputFormat, const VARIANT& To, const VARIANT& Cc, const VARIANT& Bcc, const VARIANT& Subject, const VARIANT& MessageText, const VARIANT& EditMessage, 
    const VARIANT& TemplateFile);
  void ShowToolbar(const VARIANT& ToolbarName, long Show);
  void Save(long ObjectType, const VARIANT& ObjectName);
  void SetMenuItem(const VARIANT& MenuIndex, const VARIANT& CommandIndex, const VARIANT& SubcommandIndex, const VARIANT& Flag);
  void RunCommand(long Command);
  void OpenDataAccessPage(const VARIANT& DataAccessPageName, long View);
  void OpenView(const VARIANT& ViewName, long View, long DataMode);
  void OpenDiagram(const VARIANT& DiagramName);
  void OpenStoredProcedure(const VARIANT& ProcedureName, long View, long DataMode);
};
/////////////////////////////////////////////////////////////////////////////
// _RecordsetEvents wrapper class

class _RecordsetEvents : public COleDispatchDriver
{
public:
  _RecordsetEvents() {}   // Calls COleDispatchDriver default constructor
  _RecordsetEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _RecordsetEvents(const _RecordsetEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  void WillChangeField(long cFields, const VARIANT& Fields, long* adStatus, LPUNKNOWN pRecordset);
  void FieldChangeComplete(long cFields, const VARIANT& Fields, LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset);
  void WillChangeRecord(long adReason, long cRecords, long* adStatus, LPUNKNOWN pRecordset);
  void RecordChangeComplete(long adReason, long cRecords, LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset);
  void WillChangeRecordset(long adReason, long* adStatus, LPUNKNOWN pRecordset);
  void RecordsetChangeComplete(long adReason, LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset);
  void WillMove(long adReason, long* adStatus, LPUNKNOWN pRecordset);
  void MoveComplete(long adReason, LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset);
  void EndOfRecordset(short* fMoreData, long* adStatus, LPUNKNOWN pRecordset);
  void FetchProgress(long Progress, long MaxProgress, long* adStatus, LPUNKNOWN pRecordset);
  void FetchComplete(LPUNKNOWN pError, long* adStatus, LPUNKNOWN pRecordset);
};
/////////////////////////////////////////////////////////////////////////////
// _AccessProperty wrapper class

class _AccessProperty : public COleDispatchDriver
{
public:
  _AccessProperty() {}    // Calls COleDispatchDriver default constructor
  _AccessProperty(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _AccessProperty(const _AccessProperty& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetProperties();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
  short GetType();
  void SetType(short nNewValue);
  BOOL GetInherited();
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  short GetCategory();
};
/////////////////////////////////////////////////////////////////////////////
// Properties wrapper class

class Properties : public COleDispatchDriver
{
public:
  Properties() {}   // Calls COleDispatchDriver default constructor
  Properties(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Properties(const Properties& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetItem(const VARIANT& Index);
  long GetCount();
};
/////////////////////////////////////////////////////////////////////////////
// _FormatCondition wrapper class

class _FormatCondition : public COleDispatchDriver
{
public:
  _FormatCondition() {}   // Calls COleDispatchDriver default constructor
  _FormatCondition(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _FormatCondition(const _FormatCondition& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  long GetForeColor();
  void SetForeColor(long nNewValue);
  long GetBackColor();
  void SetBackColor(long nNewValue);
  BOOL GetFontBold();
  void SetFontBold(BOOL bNewValue);
  BOOL GetFontItalic();
  void SetFontItalic(BOOL bNewValue);
  BOOL GetFontUnderline();
  void SetFontUnderline(BOOL bNewValue);
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  long GetType();
  long GetOperator();
  CString GetExpression1();
  CString GetExpression2();
  void Modify(long Type, long Operator, const VARIANT& Expression1, const VARIANT& Expression2);
  void Delete();
};
/////////////////////////////////////////////////////////////////////////////
// FormatConditions wrapper class

class FormatConditions : public COleDispatchDriver
{
public:
  FormatConditions() {}   // Calls COleDispatchDriver default constructor
  FormatConditions(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  FormatConditions(const FormatConditions& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetItem(const VARIANT& Index);
  long GetCount();
  LPDISPATCH Add(long Type, long Operator, const VARIANT& Expression1, const VARIANT& Expression2);
  void Delete();
};
/////////////////////////////////////////////////////////////////////////////
// _ItemsSelected wrapper class

class _ItemsSelected : public COleDispatchDriver
{
public:
  _ItemsSelected() {}   // Calls COleDispatchDriver default constructor
  _ItemsSelected(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _ItemsSelected(const _ItemsSelected& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  long GetItem(const VARIANT& Index);
  long GetCount();
};
/////////////////////////////////////////////////////////////////////////////
// Children wrapper class

class Children : public COleDispatchDriver
{
public:
  Children() {}   // Calls COleDispatchDriver default constructor
  Children(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Children(const Children& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetItem(const VARIANT& Index);
  long GetCount();
};
/////////////////////////////////////////////////////////////////////////////
// _AccessField wrapper class

class _AccessField : public COleDispatchDriver
{
public:
  _AccessField() {}   // Calls COleDispatchDriver default constructor
  _AccessField(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _AccessField(const _AccessField& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
};
/////////////////////////////////////////////////////////////////////////////
// _Hyperlink wrapper class

class _Hyperlink : public COleDispatchDriver
{
public:
  _Hyperlink() {}   // Calls COleDispatchDriver default constructor
  _Hyperlink(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Hyperlink(const _Hyperlink& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  CString GetSubAddress();
  void SetSubAddress(const VARIANT& newValue);
  CString GetAddress();
  void SetAddress(const VARIANT& newValue);
  void AddToFavorites();
  void Follow(BOOL NewWindow, BOOL AddHistory, const VARIANT& ExtraInfo, long Method, LPCTSTR HeaderInfo);
  CString GetEmailSubject();
  void SetEmailSubject(const VARIANT& newValue);
  CString GetScreenTip();
  void SetScreenTip(const VARIANT& newValue);
  CString GetTextToDisplay();
  void SetTextToDisplay(const VARIANT& newValue);
  void CreateNewDocument(LPCTSTR FileName, BOOL EditNow, BOOL Overwrite);
};
/////////////////////////////////////////////////////////////////////////////
// Pages wrapper class

class Pages : public COleDispatchDriver
{
public:
  Pages() {}    // Calls COleDispatchDriver default constructor
  Pages(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Pages(const Pages& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetItem(const VARIANT& Index);
  long GetCount();
  LPDISPATCH Add(const VARIANT& Before);
  void Remove(const VARIANT& Item);
};
/////////////////////////////////////////////////////////////////////////////
// _Control wrapper class

class _Control : public COleDispatchDriver
{
public:
  _Control() {}   // Calls COleDispatchDriver default constructor
  _Control(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Control(const _Control& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  void Undo();
  void Dropdown();
  VARIANT GetColumn(long Index, const VARIANT& Row);
  long GetSelected(long lRow);
  void SetSelected(long lRow, long nNewValue);
  VARIANT GetOldValue();
  LPDISPATCH GetForm();
  LPDISPATCH GetReport();
  VARIANT GetItemData(long Index);
  LPDISPATCH GetObject();
  CString GetObjectVerbs(long Index);
  LPDISPATCH GetProperties();
  void Requery();
  void SizeToFit();
  LPDISPATCH GetItemsSelected();
  void SetFocus();
  LPDISPATCH GetPages();
  LPDISPATCH GetControls();
  LPDISPATCH GetHyperlink();
};
/////////////////////////////////////////////////////////////////////////////
// Controls wrapper class

class Controls : public COleDispatchDriver
{
public:
  Controls() {}   // Calls COleDispatchDriver default constructor
  Controls(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Controls(const Controls& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetItem(const VARIANT& Index);
  long GetCount();
};
/////////////////////////////////////////////////////////////////////////////
// _Label wrapper class

class _Label : public COleDispatchDriver
{
public:
  _Label() {}   // Calls COleDispatchDriver default constructor
  _Label(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Label(const _Label& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetProperties();
  void SizeToFit();
  LPDISPATCH GetHyperlink();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetCaption();
  void SetCaption(LPCTSTR lpszNewValue);
  CString GetHyperlinkAddress();
  void SetHyperlinkAddress(LPCTSTR lpszNewValue);
  CString GetHyperlinkSubAddress();
  void SetHyperlinkSubAddress(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetVertical();
  void SetVertical(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  long GetForeColor();
  void SetForeColor(long nNewValue);
  CString GetFontName();
  void SetFontName(LPCTSTR lpszNewValue);
  short GetFontSize();
  void SetFontSize(short nNewValue);
  short GetFontWeight();
  void SetFontWeight(short nNewValue);
  BOOL GetFontItalic();
  void SetFontItalic(BOOL bNewValue);
  BOOL GetFontUnderline();
  void SetFontUnderline(BOOL bNewValue);
  // method 'GetTextAlign' not emitted because of invalid return type or parameter type
  // method 'SetTextAlign' not emitted because of invalid return type or parameter type
  short GetFontBold();
  void SetFontBold(short nNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetSection();
  void SetSection(short nNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  // method 'GetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'SetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'GetNumeralShapes' not emitted because of invalid return type or parameter type
  // method 'SetNumeralShapes' not emitted because of invalid return type or parameter type
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
  short GetLeftMargin();
  void SetLeftMargin(short nNewValue);
  short GetTopMargin();
  void SetTopMargin(short nNewValue);
  short GetLineSpacing();
  void SetLineSpacing(short nNewValue);
  short GetRightMargin();
  void SetRightMargin(short nNewValue);
  short GetBottomMargin();
  void SetBottomMargin(short nNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _Rectangle wrapper class

class _Rectangle : public COleDispatchDriver
{
public:
  _Rectangle() {}   // Calls COleDispatchDriver default constructor
  _Rectangle(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Rectangle(const _Rectangle& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetProperties();
  void SizeToFit();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _Line wrapper class

class _Line : public COleDispatchDriver
{
public:
  _Line() {}    // Calls COleDispatchDriver default constructor
  _Line(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Line(const _Line& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetProperties();
  void SizeToFit();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  BOOL GetLineSlant();
  void SetLineSlant(BOOL bNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _Image wrapper class

class _Image : public COleDispatchDriver
{
public:
  _Image() {}   // Calls COleDispatchDriver default constructor
  _Image(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Image(const _Image& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  VARIANT GetOldValue();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  LPDISPATCH GetHyperlink();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetPicture();
  void SetPicture(LPCTSTR lpszNewValue);
  // method 'GetPictureType' not emitted because of invalid return type or parameter type
  // method 'SetPictureType' not emitted because of invalid return type or parameter type
  VARIANT GetPictureData();
  void SetPictureData(const VARIANT& newValue);
  // method 'GetSizeMode' not emitted because of invalid return type or parameter type
  // method 'SetSizeMode' not emitted because of invalid return type or parameter type
  // method 'GetPictureAlignment' not emitted because of invalid return type or parameter type
  // method 'SetPictureAlignment' not emitted because of invalid return type or parameter type
  BOOL GetPictureTiling();
  void SetPictureTiling(BOOL bNewValue);
  CString GetHyperlinkAddress();
  void SetHyperlinkAddress(LPCTSTR lpszNewValue);
  CString GetHyperlinkSubAddress();
  void SetHyperlinkSubAddress(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetSection();
  void SetSection(short nNewValue);
  VARIANT GetObjectPalette();
  void SetObjectPalette(const VARIANT& newValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  long GetImageHeight();
  void SetImageHeight(long nNewValue);
  long GetImageWidth();
  void SetImageWidth(long nNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _CommandButton wrapper class

class _CommandButton : public COleDispatchDriver
{
public:
  _CommandButton() {}   // Calls COleDispatchDriver default constructor
  _CommandButton(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _CommandButton(const _CommandButton& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  VARIANT GetOldValue();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  LPDISPATCH GetHyperlink();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetCaption();
  void SetCaption(LPCTSTR lpszNewValue);
  CString GetPicture();
  void SetPicture(LPCTSTR lpszNewValue);
  // method 'GetPictureType' not emitted because of invalid return type or parameter type
  // method 'SetPictureType' not emitted because of invalid return type or parameter type
  VARIANT GetPictureData();
  void SetPictureData(const VARIANT& newValue);
  BOOL GetTransparent();
  void SetTransparent(BOOL bNewValue);
  BOOL GetDefault();
  void SetDefault(BOOL bNewValue);
  BOOL GetCancel();
  void SetCancel(BOOL bNewValue);
  BOOL GetAutoRepeat();
  void SetAutoRepeat(BOOL bNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  CString GetOnPush();
  void SetOnPush(LPCTSTR lpszNewValue);
  CString GetHyperlinkAddress();
  void SetHyperlinkAddress(LPCTSTR lpszNewValue);
  CString GetHyperlinkSubAddress();
  void SetHyperlinkSubAddress(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  long GetForeColor();
  void SetForeColor(long nNewValue);
  CString GetFontName();
  void SetFontName(LPCTSTR lpszNewValue);
  short GetFontSize();
  void SetFontSize(short nNewValue);
  short GetFontWeight();
  void SetFontWeight(short nNewValue);
  BOOL GetFontItalic();
  void SetFontItalic(BOOL bNewValue);
  BOOL GetFontUnderline();
  void SetFontUnderline(BOOL bNewValue);
  short GetFontBold();
  void SetFontBold(short nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  VARIANT GetObjectPalette();
  void SetObjectPalette(const VARIANT& newValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  // method 'GetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'SetReadingOrder' not emitted because of invalid return type or parameter type
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _OptionButton wrapper class

class _OptionButton : public COleDispatchDriver
{
public:
  _OptionButton() {}    // Calls COleDispatchDriver default constructor
  _OptionButton(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _OptionButton(const _OptionButton& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  VARIANT GetOldValue();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  long GetOptionValue();
  void SetOptionValue(long nNewValue);
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  CString GetDefaultValue();
  void SetDefaultValue(LPCTSTR lpszNewValue);
  CString GetValidationRule();
  void SetValidationRule(LPCTSTR lpszNewValue);
  CString GetValidationText();
  void SetValidationText(LPCTSTR lpszNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  BOOL GetTripleState();
  void SetTripleState(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  BOOL GetHideDuplicates();
  void SetHideDuplicates(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetColumnWidth();
  void SetColumnWidth(short nNewValue);
  short GetColumnOrder();
  void SetColumnOrder(short nNewValue);
  BOOL GetColumnHidden();
  void SetColumnHidden(BOOL bNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  // method 'GetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'SetReadingOrder' not emitted because of invalid return type or parameter type
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _Checkbox wrapper class

class _Checkbox : public COleDispatchDriver
{
public:
  _Checkbox() {}    // Calls COleDispatchDriver default constructor
  _Checkbox(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Checkbox(const _Checkbox& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  void Undo();
  VARIANT GetOldValue();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  long GetOptionValue();
  void SetOptionValue(long nNewValue);
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  CString GetDefaultValue();
  void SetDefaultValue(LPCTSTR lpszNewValue);
  CString GetValidationRule();
  void SetValidationRule(LPCTSTR lpszNewValue);
  CString GetValidationText();
  void SetValidationText(LPCTSTR lpszNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  BOOL GetTripleState();
  void SetTripleState(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  BOOL GetHideDuplicates();
  void SetHideDuplicates(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetColumnWidth();
  void SetColumnWidth(short nNewValue);
  short GetColumnOrder();
  void SetColumnOrder(short nNewValue);
  BOOL GetColumnHidden();
  void SetColumnHidden(BOOL bNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  // method 'GetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'SetReadingOrder' not emitted because of invalid return type or parameter type
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _OptionGroup wrapper class

class _OptionGroup : public COleDispatchDriver
{
public:
  _OptionGroup() {}   // Calls COleDispatchDriver default constructor
  _OptionGroup(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _OptionGroup(const _OptionGroup& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  void Undo();
  VARIANT GetOldValue();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  CString GetDefaultValue();
  void SetDefaultValue(LPCTSTR lpszNewValue);
  CString GetValidationRule();
  void SetValidationRule(LPCTSTR lpszNewValue);
  CString GetValidationText();
  void SetValidationText(LPCTSTR lpszNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  BOOL GetHideDuplicates();
  void SetHideDuplicates(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetColumnWidth();
  void SetColumnWidth(short nNewValue);
  short GetColumnOrder();
  void SetColumnOrder(short nNewValue);
  BOOL GetColumnHidden();
  void SetColumnHidden(BOOL bNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _BoundObjectFrame wrapper class

class _BoundObjectFrame : public COleDispatchDriver
{
public:
  _BoundObjectFrame() {}    // Calls COleDispatchDriver default constructor
  _BoundObjectFrame(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _BoundObjectFrame(const _BoundObjectFrame& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  VARIANT GetOldValue();
  LPDISPATCH GetObject();
  CString GetObjectVerbs(long Index);
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  // method 'GetSizeMode' not emitted because of invalid return type or parameter type
  // method 'SetSizeMode' not emitted because of invalid return type or parameter type
  CString GetClass();
  void SetClass(LPCTSTR lpszNewValue);
  CString GetSourceDoc();
  void SetSourceDoc(LPCTSTR lpszNewValue);
  CString GetSourceItem();
  void SetSourceItem(LPCTSTR lpszNewValue);
  short GetAutoActivate();
  void SetAutoActivate(short nNewValue);
  BOOL GetDisplayType();
  void SetDisplayType(BOOL bNewValue);
  short GetUpdateOptions();
  void SetUpdateOptions(short nNewValue);
  long GetVerb();
  void SetVerb(long nNewValue);
  // method 'GetOLETypeAllowed' not emitted because of invalid return type or parameter type
  // method 'SetOLETypeAllowed' not emitted because of invalid return type or parameter type
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetColumnWidth();
  void SetColumnWidth(short nNewValue);
  short GetColumnOrder();
  void SetColumnOrder(short nNewValue);
  BOOL GetColumnHidden();
  void SetColumnHidden(BOOL bNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  VARIANT GetObjectPalette();
  void SetObjectPalette(const VARIANT& newValue);
  long GetLpOleObject();
  void SetLpOleObject(long nNewValue);
  long GetObjectVerbsCount();
  void SetObjectVerbsCount(long nNewValue);
  short GetAction();
  void SetAction(short nNewValue);
  // method 'GetScaling' not emitted because of invalid return type or parameter type
  // method 'SetScaling' not emitted because of invalid return type or parameter type
  // method 'GetOLEType' not emitted because of invalid return type or parameter type
  // method 'SetOLEType' not emitted because of invalid return type or parameter type
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnUpdated();
  void SetOnUpdated(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _Textbox wrapper class

class _Textbox : public COleDispatchDriver
{
public:
  _Textbox() {}   // Calls COleDispatchDriver default constructor
  _Textbox(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Textbox(const _Textbox& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  void Undo();
  VARIANT GetOldValue();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  LPDISPATCH GetHyperlink();
  LPDISPATCH GetFormatConditions();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  CString GetFormat();
  void SetFormat(LPCTSTR lpszNewValue);
  // method 'GetDecimalPlaces' not emitted because of invalid return type or parameter type
  // method 'SetDecimalPlaces' not emitted because of invalid return type or parameter type
  CString GetInputMask();
  void SetInputMask(LPCTSTR lpszNewValue);
  CString GetDefaultValue();
  void SetDefaultValue(LPCTSTR lpszNewValue);
  BOOL GetIMEHold();
  void SetIMEHold(BOOL bNewValue);
  CString GetFuriganaControl();
  void SetFuriganaControl(LPCTSTR lpszNewValue);
  CString GetPostalAddress();
  void SetPostalAddress(LPCTSTR lpszNewValue);
  CString GetValidationRule();
  void SetValidationRule(LPCTSTR lpszNewValue);
  CString GetValidationText();
  void SetValidationText(LPCTSTR lpszNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetEnterKeyBehavior();
  void SetEnterKeyBehavior(BOOL bNewValue);
  BOOL GetAllowAutoCorrect();
  void SetAllowAutoCorrect(BOOL bNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetVertical();
  void SetVertical(BOOL bNewValue);
  BOOL GetFELineBreak();
  void SetFELineBreak(BOOL bNewValue);
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  // method 'GetFilterLookup' not emitted because of invalid return type or parameter type
  // method 'SetFilterLookup' not emitted because of invalid return type or parameter type
  BOOL GetAutoTab();
  void SetAutoTab(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  // method 'GetScrollBars' not emitted because of invalid return type or parameter type
  // method 'SetScrollBars' not emitted because of invalid return type or parameter type
  BOOL GetHideDuplicates();
  void SetHideDuplicates(BOOL bNewValue);
  BOOL GetCanGrow();
  void SetCanGrow(BOOL bNewValue);
  BOOL GetCanShrink();
  void SetCanShrink(BOOL bNewValue);
  // method 'GetRunningSum' not emitted because of invalid return type or parameter type
  // method 'SetRunningSum' not emitted because of invalid return type or parameter type
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  long GetForeColor();
  void SetForeColor(long nNewValue);
  CString GetFontName();
  void SetFontName(LPCTSTR lpszNewValue);
  short GetFontSize();
  void SetFontSize(short nNewValue);
  short GetFontWeight();
  void SetFontWeight(short nNewValue);
  BOOL GetFontItalic();
  void SetFontItalic(BOOL bNewValue);
  BOOL GetFontUnderline();
  void SetFontUnderline(BOOL bNewValue);
  // method 'GetTextAlign' not emitted because of invalid return type or parameter type
  // method 'SetTextAlign' not emitted because of invalid return type or parameter type
  short GetFontBold();
  void SetFontBold(short nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetColumnWidth();
  void SetColumnWidth(short nNewValue);
  short GetColumnOrder();
  void SetColumnOrder(short nNewValue);
  BOOL GetColumnHidden();
  void SetColumnHidden(BOOL bNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  CString GetText();
  void SetText(LPCTSTR lpszNewValue);
  CString GetSelText();
  void SetSelText(LPCTSTR lpszNewValue);
  short GetSelStart();
  void SetSelStart(short nNewValue);
  short GetSelLength();
  void SetSelLength(short nNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnChange();
  void SetOnChange(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  // method 'GetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'SetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'GetKeyboardLanguage' not emitted because of invalid return type or parameter type
  // method 'SetKeyboardLanguage' not emitted because of invalid return type or parameter type
  // method 'GetAllowedText' not emitted because of invalid return type or parameter type
  // method 'SetAllowedText' not emitted because of invalid return type or parameter type
  // method 'GetScrollBarAlign' not emitted because of invalid return type or parameter type
  // method 'SetScrollBarAlign' not emitted because of invalid return type or parameter type
  // method 'GetNumeralShapes' not emitted because of invalid return type or parameter type
  // method 'SetNumeralShapes' not emitted because of invalid return type or parameter type
  long GetIMEMode();
  void SetIMEMode(long nNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
  long GetIMESentenceMode();
  void SetIMESentenceMode(long nNewValue);
  short GetLeftMargin();
  void SetLeftMargin(short nNewValue);
  short GetTopMargin();
  void SetTopMargin(short nNewValue);
  short GetLineSpacing();
  void SetLineSpacing(short nNewValue);
  short GetRightMargin();
  void SetRightMargin(short nNewValue);
  short GetBottomMargin();
  void SetBottomMargin(short nNewValue);
  BOOL GetIsHyperlink();
  void SetIsHyperlink(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _ListBox wrapper class

class _ListBox : public COleDispatchDriver
{
public:
  _ListBox() {}   // Calls COleDispatchDriver default constructor
  _ListBox(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _ListBox(const _ListBox& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  void Undo();
  VARIANT GetColumn(long Index, const VARIANT& Row);
  long GetSelected(long lRow);
  void SetSelected(long lRow, long nNewValue);
  VARIANT GetOldValue();
  VARIANT GetItemData(long Index);
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  LPDISPATCH GetItemsSelected();
  LPDISPATCH GetHyperlink();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  CString GetRowSourceType();
  void SetRowSourceType(LPCTSTR lpszNewValue);
  CString GetRowSource();
  void SetRowSource(LPCTSTR lpszNewValue);
  short GetColumnCount();
  void SetColumnCount(short nNewValue);
  BOOL GetColumnHeads();
  void SetColumnHeads(BOOL bNewValue);
  CString GetColumnWidths();
  void SetColumnWidths(LPCTSTR lpszNewValue);
  long GetBoundColumn();
  void SetBoundColumn(long nNewValue);
  CString GetDefaultValue();
  void SetDefaultValue(LPCTSTR lpszNewValue);
  BOOL GetIMEHold();
  void SetIMEHold(BOOL bNewValue);
  CString GetValidationRule();
  void SetValidationRule(LPCTSTR lpszNewValue);
  CString GetValidationText();
  void SetValidationText(LPCTSTR lpszNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  // method 'GetMultiSelect' not emitted because of invalid return type or parameter type
  // method 'SetMultiSelect' not emitted because of invalid return type or parameter type
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  BOOL GetHideDuplicates();
  void SetHideDuplicates(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  long GetForeColor();
  void SetForeColor(long nNewValue);
  CString GetFontName();
  void SetFontName(LPCTSTR lpszNewValue);
  short GetFontSize();
  void SetFontSize(short nNewValue);
  short GetFontWeight();
  void SetFontWeight(short nNewValue);
  BOOL GetFontItalic();
  void SetFontItalic(BOOL bNewValue);
  BOOL GetFontUnderline();
  void SetFontUnderline(BOOL bNewValue);
  short GetFontBold();
  void SetFontBold(short nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetColumnWidth();
  void SetColumnWidth(short nNewValue);
  short GetColumnOrder();
  void SetColumnOrder(short nNewValue);
  BOOL GetColumnHidden();
  void SetColumnHidden(BOOL bNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  long GetListCount();
  void SetListCount(long nNewValue);
  long GetListIndex();
  void SetListIndex(long nNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  // method 'GetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'SetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'GetScrollBarAlign' not emitted because of invalid return type or parameter type
  // method 'SetScrollBarAlign' not emitted because of invalid return type or parameter type
  // method 'GetTextAlign' not emitted because of invalid return type or parameter type
  // method 'SetTextAlign' not emitted because of invalid return type or parameter type
  // method 'GetNumeralShapes' not emitted because of invalid return type or parameter type
  // method 'SetNumeralShapes' not emitted because of invalid return type or parameter type
  long GetIMEMode();
  void SetIMEMode(long nNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
  long GetIMESentenceMode();
  void SetIMESentenceMode(long nNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _Combobox wrapper class

class _Combobox : public COleDispatchDriver
{
public:
  _Combobox() {}    // Calls COleDispatchDriver default constructor
  _Combobox(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Combobox(const _Combobox& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  void Undo();
  void Dropdown();
  VARIANT GetColumn(long Index, const VARIANT& Row);
  VARIANT GetOldValue();
  VARIANT GetItemData(long Index);
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  LPDISPATCH GetHyperlink();
  LPDISPATCH GetFormatConditions();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  CString GetFormat();
  void SetFormat(LPCTSTR lpszNewValue);
  // method 'GetDecimalPlaces' not emitted because of invalid return type or parameter type
  // method 'SetDecimalPlaces' not emitted because of invalid return type or parameter type
  CString GetInputMask();
  void SetInputMask(LPCTSTR lpszNewValue);
  CString GetRowSourceType();
  void SetRowSourceType(LPCTSTR lpszNewValue);
  CString GetRowSource();
  void SetRowSource(LPCTSTR lpszNewValue);
  short GetColumnCount();
  void SetColumnCount(short nNewValue);
  BOOL GetColumnHeads();
  void SetColumnHeads(BOOL bNewValue);
  CString GetColumnWidths();
  void SetColumnWidths(LPCTSTR lpszNewValue);
  long GetBoundColumn();
  void SetBoundColumn(long nNewValue);
  short GetListRows();
  void SetListRows(short nNewValue);
  CString GetListWidth();
  void SetListWidth(LPCTSTR lpszNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetLimitToList();
  void SetLimitToList(BOOL bNewValue);
  BOOL GetAutoExpand();
  void SetAutoExpand(BOOL bNewValue);
  CString GetDefaultValue();
  void SetDefaultValue(LPCTSTR lpszNewValue);
  BOOL GetIMEHold();
  void SetIMEHold(BOOL bNewValue);
  CString GetValidationRule();
  void SetValidationRule(LPCTSTR lpszNewValue);
  CString GetValidationText();
  void SetValidationText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  BOOL GetAllowAutoCorrect();
  void SetAllowAutoCorrect(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  BOOL GetHideDuplicates();
  void SetHideDuplicates(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  long GetForeColor();
  void SetForeColor(long nNewValue);
  CString GetFontName();
  void SetFontName(LPCTSTR lpszNewValue);
  short GetFontSize();
  void SetFontSize(short nNewValue);
  short GetFontWeight();
  void SetFontWeight(short nNewValue);
  BOOL GetFontItalic();
  void SetFontItalic(BOOL bNewValue);
  BOOL GetFontUnderline();
  void SetFontUnderline(BOOL bNewValue);
  // method 'GetTextAlign' not emitted because of invalid return type or parameter type
  // method 'SetTextAlign' not emitted because of invalid return type or parameter type
  short GetFontBold();
  void SetFontBold(short nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetColumnWidth();
  void SetColumnWidth(short nNewValue);
  short GetColumnOrder();
  void SetColumnOrder(short nNewValue);
  BOOL GetColumnHidden();
  void SetColumnHidden(BOOL bNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  CString GetText();
  void SetText(LPCTSTR lpszNewValue);
  CString GetSelText();
  void SetSelText(LPCTSTR lpszNewValue);
  short GetSelStart();
  void SetSelStart(short nNewValue);
  short GetSelLength();
  void SetSelLength(short nNewValue);
  long GetListCount();
  void SetListCount(long nNewValue);
  long GetListIndex();
  void SetListIndex(long nNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnChange();
  void SetOnChange(LPCTSTR lpszNewValue);
  CString GetOnNotInList();
  void SetOnNotInList(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  // method 'GetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'SetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'GetKeyboardLanguage' not emitted because of invalid return type or parameter type
  // method 'SetKeyboardLanguage' not emitted because of invalid return type or parameter type
  // method 'GetAllowedText' not emitted because of invalid return type or parameter type
  // method 'SetAllowedText' not emitted because of invalid return type or parameter type
  // method 'GetScrollBarAlign' not emitted because of invalid return type or parameter type
  // method 'SetScrollBarAlign' not emitted because of invalid return type or parameter type
  // method 'GetNumeralShapes' not emitted because of invalid return type or parameter type
  // method 'SetNumeralShapes' not emitted because of invalid return type or parameter type
  long GetIMEMode();
  void SetIMEMode(long nNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
  long GetIMESentenceMode();
  void SetIMESentenceMode(long nNewValue);
  BOOL GetIsHyperlink();
  void SetIsHyperlink(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _ObjectFrame wrapper class

class _ObjectFrame : public COleDispatchDriver
{
public:
  _ObjectFrame() {}   // Calls COleDispatchDriver default constructor
  _ObjectFrame(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _ObjectFrame(const _ObjectFrame& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  VARIANT GetOldValue();
  LPDISPATCH GetObject();
  CString GetObjectVerbs(long Index);
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  // method 'GetSizeMode' not emitted because of invalid return type or parameter type
  // method 'SetSizeMode' not emitted because of invalid return type or parameter type
  CString GetOLEClass();
  void SetOLEClass(LPCTSTR lpszNewValue);
  CString GetItem();
  void SetItem(LPCTSTR lpszNewValue);
  CString GetRowSourceType();
  void SetRowSourceType(LPCTSTR lpszNewValue);
  CString GetRowSource();
  void SetRowSource(LPCTSTR lpszNewValue);
  CString GetLinkChildFields();
  void SetLinkChildFields(LPCTSTR lpszNewValue);
  CString GetLinkMasterFields();
  void SetLinkMasterFields(LPCTSTR lpszNewValue);
  short GetAutoActivate();
  void SetAutoActivate(short nNewValue);
  BOOL GetDisplayType();
  void SetDisplayType(BOOL bNewValue);
  short GetUpdateOptions();
  void SetUpdateOptions(short nNewValue);
  long GetVerb();
  void SetVerb(long nNewValue);
  // method 'GetOLEType' not emitted because of invalid return type or parameter type
  // method 'SetOLEType' not emitted because of invalid return type or parameter type
  // method 'GetOLETypeAllowed' not emitted because of invalid return type or parameter type
  // method 'SetOLETypeAllowed' not emitted because of invalid return type or parameter type
  CString GetSourceObject();
  void SetSourceObject(LPCTSTR lpszNewValue);
  CString GetClass();
  void SetClass(LPCTSTR lpszNewValue);
  CString GetSourceDoc();
  void SetSourceDoc(LPCTSTR lpszNewValue);
  CString GetSourceItem();
  void SetSourceItem(LPCTSTR lpszNewValue);
  short GetColumnCount();
  void SetColumnCount(short nNewValue);
  BOOL GetColumnHeads();
  void SetColumnHeads(BOOL bNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  short GetUpdateMethod();
  void SetUpdateMethod(short nNewValue);
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  VARIANT GetObjectPalette();
  void SetObjectPalette(const VARIANT& newValue);
  long GetLpOleObject();
  void SetLpOleObject(long nNewValue);
  long GetObjectVerbsCount();
  void SetObjectVerbsCount(long nNewValue);
  short GetAction();
  void SetAction(short nNewValue);
  // method 'GetScaling' not emitted because of invalid return type or parameter type
  // method 'SetScaling' not emitted because of invalid return type or parameter type
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetOnUpdated();
  void SetOnUpdated(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _PageBreak wrapper class

class _PageBreak : public COleDispatchDriver
{
public:
  _PageBreak() {}   // Calls COleDispatchDriver default constructor
  _PageBreak(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _PageBreak(const _PageBreak& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetProperties();
  void SizeToFit();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  short GetSection();
  void SetSection(short nNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _ToggleButton wrapper class

class _ToggleButton : public COleDispatchDriver
{
public:
  _ToggleButton() {}    // Calls COleDispatchDriver default constructor
  _ToggleButton(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _ToggleButton(const _ToggleButton& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  void Undo();
  VARIANT GetOldValue();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  CString GetCaption();
  void SetCaption(LPCTSTR lpszNewValue);
  CString GetPicture();
  void SetPicture(LPCTSTR lpszNewValue);
  // method 'GetPictureType' not emitted because of invalid return type or parameter type
  // method 'SetPictureType' not emitted because of invalid return type or parameter type
  VARIANT GetPictureData();
  void SetPictureData(const VARIANT& newValue);
  long GetOptionValue();
  void SetOptionValue(long nNewValue);
  CString GetDefaultValue();
  void SetDefaultValue(LPCTSTR lpszNewValue);
  CString GetValidationRule();
  void SetValidationRule(LPCTSTR lpszNewValue);
  CString GetValidationText();
  void SetValidationText(LPCTSTR lpszNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  BOOL GetTripleState();
  void SetTripleState(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  BOOL GetHideDuplicates();
  void SetHideDuplicates(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  long GetForeColor();
  void SetForeColor(long nNewValue);
  CString GetFontName();
  void SetFontName(LPCTSTR lpszNewValue);
  short GetFontSize();
  void SetFontSize(short nNewValue);
  short GetFontWeight();
  void SetFontWeight(short nNewValue);
  BOOL GetFontItalic();
  void SetFontItalic(BOOL bNewValue);
  BOOL GetFontUnderline();
  void SetFontUnderline(BOOL bNewValue);
  short GetFontBold();
  void SetFontBold(short nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetColumnWidth();
  void SetColumnWidth(short nNewValue);
  short GetColumnOrder();
  void SetColumnOrder(short nNewValue);
  BOOL GetColumnHidden();
  void SetColumnHidden(BOOL bNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  VARIANT GetObjectPalette();
  void SetObjectPalette(const VARIANT& newValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  // method 'GetReadingOrder' not emitted because of invalid return type or parameter type
  // method 'SetReadingOrder' not emitted because of invalid return type or parameter type
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _PaletteButton wrapper class

class _PaletteButton : public COleDispatchDriver
{
public:
  _PaletteButton() {}   // Calls COleDispatchDriver default constructor
  _PaletteButton(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _PaletteButton(const _PaletteButton& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  VARIANT GetOldValue();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  long GetOptionValue();
  void SetOptionValue(long nNewValue);
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  CString GetDefaultValue();
  void SetDefaultValue(LPCTSTR lpszNewValue);
  CString GetValidationRule();
  void SetValidationRule(LPCTSTR lpszNewValue);
  CString GetValidationText();
  void SetValidationText(LPCTSTR lpszNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  BOOL GetTripleState();
  void SetTripleState(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  BOOL GetHideDuplicates();
  void SetHideDuplicates(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  long GetBackColor();
  void SetBackColor(long nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetColumnWidth();
  void SetColumnWidth(short nNewValue);
  short GetColumnOrder();
  void SetColumnOrder(short nNewValue);
  BOOL GetColumnHidden();
  void SetColumnHidden(BOOL bNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _SubForm wrapper class

class _SubForm : public COleDispatchDriver
{
public:
  _SubForm() {}   // Calls COleDispatchDriver default constructor
  _SubForm(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _SubForm(const _SubForm& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetForm();
  LPDISPATCH GetReport();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void SetFocus();
  void Requery();
  LPDISPATCH GetControls();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetSourceObject();
  void SetSourceObject(LPCTSTR lpszNewValue);
  CString GetLinkChildFields();
  void SetLinkChildFields(LPCTSTR lpszNewValue);
  CString GetLinkMasterFields();
  void SetLinkMasterFields(LPCTSTR lpszNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  BOOL GetCanGrow();
  void SetCanGrow(BOOL bNewValue);
  BOOL GetCanShrink();
  void SetCanShrink(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  BOOL GetAutoLabel();
  void SetAutoLabel(BOOL bNewValue);
  BOOL GetAddColon();
  void SetAddColon(BOOL bNewValue);
  short GetLabelX();
  void SetLabelX(short nNewValue);
  short GetLabelY();
  void SetLabelY(short nNewValue);
  // method 'GetLabelAlign' not emitted because of invalid return type or parameter type
  // method 'SetLabelAlign' not emitted because of invalid return type or parameter type
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _SubReport wrapper class

class _SubReport : public COleDispatchDriver
{
public:
  _SubReport() {}   // Calls COleDispatchDriver default constructor
  _SubReport(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _SubReport(const _SubReport& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetForm();
  LPDISPATCH GetReport();
};
/////////////////////////////////////////////////////////////////////////////
// _CustomControl wrapper class

class _CustomControl : public COleDispatchDriver
{
public:
  _CustomControl() {}   // Calls COleDispatchDriver default constructor
  _CustomControl(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _CustomControl(const _CustomControl& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  VARIANT GetOldValue();
  LPDISPATCH GetObject();
  CString GetObjectVerbs(long Index);
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  CString GetOLEClass();
  void SetOLEClass(LPCTSTR lpszNewValue);
  long GetVerb();
  void SetVerb(long nNewValue);
  CString GetClass();
  void SetClass(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetLocked();
  void SetLocked(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'GetOldBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetOldBorderStyle' not emitted because of invalid return type or parameter type
  long GetBorderColor();
  void SetBorderColor(long nNewValue);
  // method 'GetBorderWidth' not emitted because of invalid return type or parameter type
  // method 'SetBorderWidth' not emitted because of invalid return type or parameter type
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  VARIANT GetObjectPalette();
  void SetObjectPalette(const VARIANT& newValue);
  long GetLpOleObject();
  void SetLpOleObject(long nNewValue);
  long GetObjectVerbsCount();
  void SetObjectVerbsCount(long nNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetOnUpdated();
  void SetOnUpdated(LPCTSTR lpszNewValue);
  CString GetOnEnter();
  void SetOnEnter(LPCTSTR lpszNewValue);
  CString GetOnExit();
  void SetOnExit(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  BOOL GetDefault();
  void SetDefault(BOOL bNewValue);
  BOOL GetCancel();
  void SetCancel(BOOL bNewValue);
  CString GetCustom();
  void SetCustom(LPCTSTR lpszNewValue);
  CString GetAbout();
  void SetAbout(LPCTSTR lpszNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _TabControl wrapper class

class _TabControl : public COleDispatchDriver
{
public:
  _TabControl() {}    // Calls COleDispatchDriver default constructor
  _TabControl(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _TabControl(const _TabControl& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  VARIANT GetOldValue();
  LPDISPATCH GetProperties();
  void SizeToFit();
  LPDISPATCH GetPages();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  BOOL GetTabStop();
  void SetTabStop(BOOL bNewValue);
  short GetTabIndex();
  void SetTabIndex(short nNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  // method 'GetBackStyle' not emitted because of invalid return type or parameter type
  // method 'SetBackStyle' not emitted because of invalid return type or parameter type
  BOOL GetMultiRow();
  void SetMultiRow(BOOL bNewValue);
  // method 'GetStyle' not emitted because of invalid return type or parameter type
  // method 'SetStyle' not emitted because of invalid return type or parameter type
  short GetTabFixedHeight();
  void SetTabFixedHeight(short nNewValue);
  short GetTabFixedWidth();
  void SetTabFixedWidth(short nNewValue);
  CString GetFontName();
  void SetFontName(LPCTSTR lpszNewValue);
  short GetFontSize();
  void SetFontSize(short nNewValue);
  short GetFontWeight();
  void SetFontWeight(short nNewValue);
  BOOL GetFontItalic();
  void SetFontItalic(BOOL bNewValue);
  BOOL GetFontUnderline();
  void SetFontUnderline(BOOL bNewValue);
  short GetFontBold();
  void SetFontBold(short nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetOnChange();
  void SetOnChange(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _Page wrapper class

class _Page : public COleDispatchDriver
{
public:
  _Page() {}    // Calls COleDispatchDriver default constructor
  _Page(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Page(const _Page& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetProperties();
  void SizeToFit();
  void Requery();
  void SetFocus();
  LPDISPATCH GetControls();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetControlType' not emitted because of invalid return type or parameter type
  // method 'SetControlType' not emitted because of invalid return type or parameter type
  CString GetCaption();
  void SetCaption(LPCTSTR lpszNewValue);
  CString GetPicture();
  void SetPicture(LPCTSTR lpszNewValue);
  // method 'GetPictureType' not emitted because of invalid return type or parameter type
  // method 'SetPictureType' not emitted because of invalid return type or parameter type
  short GetPageIndex();
  void SetPageIndex(short nNewValue);
  CString GetStatusBarText();
  void SetStatusBarText(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  BOOL GetEnabled();
  void SetEnabled(BOOL bNewValue);
  short GetLeft();
  void SetLeft(short nNewValue);
  short GetTop();
  void SetTop(short nNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  CString GetControlTipText();
  void SetControlTipText(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetSection();
  void SetSection(short nNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  BOOL GetIsVisible();
  void SetIsVisible(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  VARIANT GetPictureData();
  void SetPictureData(const VARIANT& newValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _Section wrapper class

class _Section : public COleDispatchDriver
{
public:
  _Section() {}   // Calls COleDispatchDriver default constructor
  _Section(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Section(const _Section& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetProperties();
  LPDISPATCH GetControls();
  CString GetEventProcPrefix();
  void SetEventProcPrefix(LPCTSTR lpszNewValue);
  // method 'GetForceNewPage' not emitted because of invalid return type or parameter type
  // method 'SetForceNewPage' not emitted because of invalid return type or parameter type
  // method 'GetNewRowOrCol' not emitted because of invalid return type or parameter type
  // method 'SetNewRowOrCol' not emitted because of invalid return type or parameter type
  BOOL GetKeepTogether();
  void SetKeepTogether(BOOL bNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  // method 'GetDisplayWhen' not emitted because of invalid return type or parameter type
  // method 'SetDisplayWhen' not emitted because of invalid return type or parameter type
  BOOL GetCanGrow();
  void SetCanGrow(BOOL bNewValue);
  BOOL GetCanShrink();
  void SetCanShrink(BOOL bNewValue);
  BOOL GetRepeatSection();
  void SetRepeatSection(BOOL bNewValue);
  short GetHeight();
  void SetHeight(short nNewValue);
  long GetBackColor();
  void SetBackColor(long nNewValue);
  // method 'GetSpecialEffect' not emitted because of invalid return type or parameter type
  // method 'SetSpecialEffect' not emitted because of invalid return type or parameter type
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  CString GetOnFormat();
  void SetOnFormat(LPCTSTR lpszNewValue);
  CString GetOnPrint();
  void SetOnPrint(LPCTSTR lpszNewValue);
  CString GetOnRetreat();
  void SetOnRetreat(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  BOOL GetHasContinued();
  void SetHasContinued(BOOL bNewValue);
  BOOL GetWillContinue();
  void SetWillContinue(BOOL bNewValue);
  BOOL GetInSelection();
  void SetInSelection(BOOL bNewValue);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// _GroupLevel wrapper class

class _GroupLevel : public COleDispatchDriver
{
public:
  _GroupLevel() {}    // Calls COleDispatchDriver default constructor
  _GroupLevel(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _GroupLevel(const _GroupLevel& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetProperties();
  CString GetControlSource();
  void SetControlSource(LPCTSTR lpszNewValue);
  BOOL GetSortOrder();
  void SetSortOrder(BOOL bNewValue);
  BOOL GetGroupHeader();
  void SetGroupHeader(BOOL bNewValue);
  BOOL GetGroupFooter();
  void SetGroupFooter(BOOL bNewValue);
  short GetGroupOn();
  void SetGroupOn(short nNewValue);
  long GetGroupInterval();
  void SetGroupInterval(long nNewValue);
  // method 'GetKeepTogether' not emitted because of invalid return type or parameter type
  // method 'SetKeepTogether' not emitted because of invalid return type or parameter type
};
/////////////////////////////////////////////////////////////////////////////
// Module wrapper class

class Module : public COleDispatchDriver
{
public:
  Module() {}   // Calls COleDispatchDriver default constructor
  Module(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Module(const Module& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  void InsertText(LPCTSTR Text);
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
  void AddFromString(LPCTSTR String);
  void AddFromFile(LPCTSTR FileName);
  CString GetLines(long Line, long NumLines);
  long GetCountOfLines();
  void InsertLines(long Line, LPCTSTR String);
  void DeleteLines(long StartLine, long Count);
  void ReplaceLine(long Line, LPCTSTR String);
  long GetProcStartLine(LPCTSTR ProcName, long ProcKind);
  long GetProcCountLines(LPCTSTR ProcName, long ProcKind);
  long GetProcBodyLine(LPCTSTR ProcName, long ProcKind);
  CString GetProcOfLine(long Line, long* pprockind);
  long GetCountOfDeclarationLines();
  long CreateEventProc(LPCTSTR EventName, LPCTSTR ObjectName);
  BOOL Find(LPCTSTR Target, long* StartLine, long* StartColumn, long* EndLine, long* EndColumn, BOOL WholeWord, BOOL MatchCase, BOOL PatternSearch);
  long GetType();
};
/////////////////////////////////////////////////////////////////////////////
// Modules wrapper class

class Modules : public COleDispatchDriver
{
public:
  Modules() {}    // Calls COleDispatchDriver default constructor
  Modules(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Modules(const Modules& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetItem(const VARIANT& Index);
  long GetCount();
};
/////////////////////////////////////////////////////////////////////////////
// _Form wrapper class

class _Form : public COleDispatchDriver
{
public:
  _Form() {}    // Calls COleDispatchDriver default constructor
  _Form(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Form(const _Form& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  CString GetRecordSource();
  void SetRecordSource(LPCTSTR lpszNewValue);
  CString GetFilter();
  void SetFilter(LPCTSTR lpszNewValue);
  BOOL GetFilterOn();
  void SetFilterOn(BOOL bNewValue);
  CString GetOrderBy();
  void SetOrderBy(LPCTSTR lpszNewValue);
  BOOL GetOrderByOn();
  void SetOrderByOn(BOOL bNewValue);
  BOOL GetAllowFilters();
  void SetAllowFilters(BOOL bNewValue);
  CString GetCaption();
  void SetCaption(LPCTSTR lpszNewValue);
  // method 'GetDefaultView' not emitted because of invalid return type or parameter type
  // method 'SetDefaultView' not emitted because of invalid return type or parameter type
  // method 'GetViewsAllowed' not emitted because of invalid return type or parameter type
  // method 'SetViewsAllowed' not emitted because of invalid return type or parameter type
  BOOL GetAllowEdits();
  void SetAllowEdits(BOOL bNewValue);
  BOOL GetAllowDeletions();
  void SetAllowDeletions(BOOL bNewValue);
  BOOL GetAllowAdditions();
  void SetAllowAdditions(BOOL bNewValue);
  BOOL GetDataEntry();
  void SetDataEntry(BOOL bNewValue);
  // method 'GetRecordsetType' not emitted because of invalid return type or parameter type
  // method 'SetRecordsetType' not emitted because of invalid return type or parameter type
  // method 'GetRecordLocks' not emitted because of invalid return type or parameter type
  // method 'SetRecordLocks' not emitted because of invalid return type or parameter type
  // method 'GetScrollBars' not emitted because of invalid return type or parameter type
  // method 'SetScrollBars' not emitted because of invalid return type or parameter type
  BOOL GetRecordSelectors();
  void SetRecordSelectors(BOOL bNewValue);
  BOOL GetNavigationButtons();
  void SetNavigationButtons(BOOL bNewValue);
  BOOL GetDividingLines();
  void SetDividingLines(BOOL bNewValue);
  BOOL GetAutoResize();
  void SetAutoResize(BOOL bNewValue);
  BOOL GetAutoCenter();
  void SetAutoCenter(BOOL bNewValue);
  BOOL GetPopUp();
  void SetPopUp(BOOL bNewValue);
  BOOL GetModal();
  void SetModal(BOOL bNewValue);
  // method 'GetBorderStyle' not emitted because of invalid return type or parameter type
  // method 'SetBorderStyle' not emitted because of invalid return type or parameter type
  BOOL GetControlBox();
  void SetControlBox(BOOL bNewValue);
  // method 'GetMinMaxButtons' not emitted because of invalid return type or parameter type
  // method 'SetMinMaxButtons' not emitted because of invalid return type or parameter type
  BOOL GetCloseButton();
  void SetCloseButton(BOOL bNewValue);
  BOOL GetWhatsThisButton();
  void SetWhatsThisButton(BOOL bNewValue);
  short GetWidth();
  void SetWidth(short nNewValue);
  CString GetPicture();
  void SetPicture(LPCTSTR lpszNewValue);
  // method 'GetPictureType' not emitted because of invalid return type or parameter type
  // method 'SetPictureType' not emitted because of invalid return type or parameter type
  // method 'GetPictureSizeMode' not emitted because of invalid return type or parameter type
  // method 'SetPictureSizeMode' not emitted because of invalid return type or parameter type
  // method 'GetPictureAlignment' not emitted because of invalid return type or parameter type
  // method 'SetPictureAlignment' not emitted because of invalid return type or parameter type
  BOOL GetPictureTiling();
  void SetPictureTiling(BOOL bNewValue);
  // method 'GetCycle' not emitted because of invalid return type or parameter type
  // method 'SetCycle' not emitted because of invalid return type or parameter type
  CString GetMenuBar();
  void SetMenuBar(LPCTSTR lpszNewValue);
  CString GetToolbar();
  void SetToolbar(LPCTSTR lpszNewValue);
  BOOL GetShortcutMenu();
  void SetShortcutMenu(BOOL bNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  short GetGridX();
  void SetGridX(short nNewValue);
  short GetGridY();
  void SetGridY(short nNewValue);
  BOOL GetLayoutForPrint();
  void SetLayoutForPrint(BOOL bNewValue);
  BOOL GetFastLaserPrinting();
  void SetFastLaserPrinting(BOOL bNewValue);
  CString GetHelpFile();
  void SetHelpFile(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  short GetRowHeight();
  void SetRowHeight(short nNewValue);
  CString GetDatasheetFontName();
  void SetDatasheetFontName(LPCTSTR lpszNewValue);
  short GetDatasheetFontHeight();
  void SetDatasheetFontHeight(short nNewValue);
  short GetDatasheetFontWeight();
  void SetDatasheetFontWeight(short nNewValue);
  BOOL GetDatasheetFontItalic();
  void SetDatasheetFontItalic(BOOL bNewValue);
  BOOL GetDatasheetFontUnderline();
  void SetDatasheetFontUnderline(BOOL bNewValue);
  // method 'GetDatasheetGridlinesBehavior' not emitted because of invalid return type or parameter type
  // method 'SetDatasheetGridlinesBehavior' not emitted because of invalid return type or parameter type
  long GetDatasheetGridlinesColor();
  void SetDatasheetGridlinesColor(long nNewValue);
  // method 'GetDatasheetCellsEffect' not emitted because of invalid return type or parameter type
  // method 'SetDatasheetCellsEffect' not emitted because of invalid return type or parameter type
  long GetDatasheetForeColor();
  void SetDatasheetForeColor(long nNewValue);
  long GetDatasheetBackColor();
  void SetDatasheetBackColor(long nNewValue);
  long GetHwnd();
  void SetHwnd(long nNewValue);
  short GetCount();
  void SetCount(short nNewValue);
  long GetPage();
  void SetPage(long nNewValue);
  short GetPages();
  void SetPages(short nNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  BOOL GetPainting();
  void SetPainting(BOOL bNewValue);
  VARIANT GetPrtMip();
  void SetPrtMip(const VARIANT& newValue);
  VARIANT GetPrtDevMode();
  void SetPrtDevMode(const VARIANT& newValue);
  VARIANT GetPrtDevNames();
  void SetPrtDevNames(const VARIANT& newValue);
  short GetFrozenColumns();
  void SetFrozenColumns(short nNewValue);
  VARIANT GetBookmark();
  void SetBookmark(const VARIANT& newValue);
  CString GetPaletteSource();
  void SetPaletteSource(LPCTSTR lpszNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  VARIANT GetPaintPalette();
  void SetPaintPalette(const VARIANT& newValue);
  CString GetOnMenu();
  void SetOnMenu(LPCTSTR lpszNewValue);
  VARIANT GetOpenArgs();
  void SetOpenArgs(const VARIANT& newValue);
  CString GetOnCurrent();
  void SetOnCurrent(LPCTSTR lpszNewValue);
  CString GetOnInsert();
  void SetOnInsert(LPCTSTR lpszNewValue);
  CString GetBeforeInsert();
  void SetBeforeInsert(LPCTSTR lpszNewValue);
  CString GetAfterInsert();
  void SetAfterInsert(LPCTSTR lpszNewValue);
  CString GetBeforeUpdate();
  void SetBeforeUpdate(LPCTSTR lpszNewValue);
  CString GetAfterUpdate();
  void SetAfterUpdate(LPCTSTR lpszNewValue);
  CString GetOnDirty();
  void SetOnDirty(LPCTSTR lpszNewValue);
  CString GetOnDelete();
  void SetOnDelete(LPCTSTR lpszNewValue);
  CString GetBeforeDelConfirm();
  void SetBeforeDelConfirm(LPCTSTR lpszNewValue);
  CString GetAfterDelConfirm();
  void SetAfterDelConfirm(LPCTSTR lpszNewValue);
  CString GetOnOpen();
  void SetOnOpen(LPCTSTR lpszNewValue);
  CString GetOnLoad();
  void SetOnLoad(LPCTSTR lpszNewValue);
  CString GetOnResize();
  void SetOnResize(LPCTSTR lpszNewValue);
  CString GetOnUnload();
  void SetOnUnload(LPCTSTR lpszNewValue);
  CString GetOnClose();
  void SetOnClose(LPCTSTR lpszNewValue);
  CString GetOnActivate();
  void SetOnActivate(LPCTSTR lpszNewValue);
  CString GetOnDeactivate();
  void SetOnDeactivate(LPCTSTR lpszNewValue);
  CString GetOnGotFocus();
  void SetOnGotFocus(LPCTSTR lpszNewValue);
  CString GetOnLostFocus();
  void SetOnLostFocus(LPCTSTR lpszNewValue);
  CString GetOnClick();
  void SetOnClick(LPCTSTR lpszNewValue);
  CString GetOnDblClick();
  void SetOnDblClick(LPCTSTR lpszNewValue);
  CString GetOnMouseDown();
  void SetOnMouseDown(LPCTSTR lpszNewValue);
  CString GetOnMouseMove();
  void SetOnMouseMove(LPCTSTR lpszNewValue);
  CString GetOnMouseUp();
  void SetOnMouseUp(LPCTSTR lpszNewValue);
  CString GetOnKeyDown();
  void SetOnKeyDown(LPCTSTR lpszNewValue);
  CString GetOnKeyUp();
  void SetOnKeyUp(LPCTSTR lpszNewValue);
  CString GetOnKeyPress();
  void SetOnKeyPress(LPCTSTR lpszNewValue);
  BOOL GetKeyPreview();
  void SetKeyPreview(BOOL bNewValue);
  CString GetOnError();
  void SetOnError(LPCTSTR lpszNewValue);
  CString GetOnFilter();
  void SetOnFilter(LPCTSTR lpszNewValue);
  CString GetOnApplyFilter();
  void SetOnApplyFilter(LPCTSTR lpszNewValue);
  CString GetOnTimer();
  void SetOnTimer(LPCTSTR lpszNewValue);
  long GetTimerInterval();
  void SetTimerInterval(long nNewValue);
  BOOL GetDirty();
  void SetDirty(BOOL bNewValue);
  short GetWindowWidth();
  void SetWindowWidth(short nNewValue);
  short GetWindowHeight();
  void SetWindowHeight(short nNewValue);
  short GetCurrentView();
  void SetCurrentView(short nNewValue);
  short GetCurrentSectionTop();
  void SetCurrentSectionTop(short nNewValue);
  short GetCurrentSectionLeft();
  void SetCurrentSectionLeft(short nNewValue);
  long GetSelLeft();
  void SetSelLeft(long nNewValue);
  long GetSelTop();
  void SetSelTop(long nNewValue);
  long GetSelWidth();
  void SetSelWidth(long nNewValue);
  long GetSelHeight();
  void SetSelHeight(long nNewValue);
  long GetCurrentRecord();
  void SetCurrentRecord(long nNewValue);
  VARIANT GetPictureData();
  void SetPictureData(const VARIANT& newValue);
  long GetInsideHeight();
  void SetInsideHeight(long nNewValue);
  long GetInsideWidth();
  void SetInsideWidth(long nNewValue);
  VARIANT GetPicturePalette();
  void SetPicturePalette(const VARIANT& newValue);
  BOOL GetHasModule();
  void SetHasModule(BOOL bNewValue);
  // method 'GetOrientation' not emitted because of invalid return type or parameter type
  // method 'SetOrientation' not emitted because of invalid return type or parameter type
  BOOL GetAllowDesignChanges();
  void SetAllowDesignChanges(BOOL bNewValue);
  CString GetServerFilter();
  void SetServerFilter(LPCTSTR lpszNewValue);
  BOOL GetServerFilterByForm();
  void SetServerFilterByForm(BOOL bNewValue);
  long GetMaxRecords();
  void SetMaxRecords(long nNewValue);
  CString GetUniqueTable();
  void SetUniqueTable(LPCTSTR lpszNewValue);
  CString GetResyncCommand();
  void SetResyncCommand(LPCTSTR lpszNewValue);
  CString GetInputParameters();
  void SetInputParameters(LPCTSTR lpszNewValue);
  BOOL GetMaxRecButton();
  void SetMaxRecButton(BOOL bNewValue);
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  short GetNewRecord();
  void Undo();
  LPDISPATCH GetActiveControl();
  LPDISPATCH GetDefaultControl(long ControlType);
  LPDISPATCH GetRecordsetClone();
  LPDISPATCH GetRecordset();
  void SetRefRecordset(LPDISPATCH newValue);
  LPDISPATCH GetSection(const VARIANT& Index);
  LPDISPATCH GetForm();
  LPDISPATCH GetModule();
  LPDISPATCH GetProperties();
  void Recalc();
  void Requery();
  void Refresh();
  void Repaint();
  void GoToPage(long PageNumber, long Right, long Down);
  void SetFocus();
  LPDISPATCH GetControls();
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
  short GetSubdatasheetHeight();
  void SetSubdatasheetHeight(short nNewValue);
  BOOL GetSubdatasheetExpanded();
  void SetSubdatasheetExpanded(BOOL bNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// Forms wrapper class

class Forms : public COleDispatchDriver
{
public:
  Forms() {}    // Calls COleDispatchDriver default constructor
  Forms(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Forms(const Forms& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetItem(const VARIANT& Index);
  long GetCount();
};
/////////////////////////////////////////////////////////////////////////////
// _Report wrapper class

class _Report : public COleDispatchDriver
{
public:
  _Report() {}    // Calls COleDispatchDriver default constructor
  _Report(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Report(const _Report& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  CString GetRecordSource();
  void SetRecordSource(LPCTSTR lpszNewValue);
  CString GetFilter();
  void SetFilter(LPCTSTR lpszNewValue);
  BOOL GetFilterOn();
  void SetFilterOn(BOOL bNewValue);
  CString GetOrderBy();
  void SetOrderBy(LPCTSTR lpszNewValue);
  BOOL GetOrderByOn();
  void SetOrderByOn(BOOL bNewValue);
  CString GetServerFilter();
  void SetServerFilter(LPCTSTR lpszNewValue);
  CString GetCaption();
  void SetCaption(LPCTSTR lpszNewValue);
  // method 'GetRecordLocks' not emitted because of invalid return type or parameter type
  // method 'SetRecordLocks' not emitted because of invalid return type or parameter type
  // method 'GetPageHeader' not emitted because of invalid return type or parameter type
  // method 'SetPageHeader' not emitted because of invalid return type or parameter type
  // method 'GetPageFooter' not emitted because of invalid return type or parameter type
  // method 'SetPageFooter' not emitted because of invalid return type or parameter type
  // method 'GetDateGrouping' not emitted because of invalid return type or parameter type
  // method 'SetDateGrouping' not emitted because of invalid return type or parameter type
  // method 'GetGrpKeepTogether' not emitted because of invalid return type or parameter type
  // method 'SetGrpKeepTogether' not emitted because of invalid return type or parameter type
  short GetWidth();
  void SetWidth(short nNewValue);
  CString GetPicture();
  void SetPicture(LPCTSTR lpszNewValue);
  // method 'GetPictureType' not emitted because of invalid return type or parameter type
  // method 'SetPictureType' not emitted because of invalid return type or parameter type
  // method 'GetPictureSizeMode' not emitted because of invalid return type or parameter type
  // method 'SetPictureSizeMode' not emitted because of invalid return type or parameter type
  // method 'GetPictureAlignment' not emitted because of invalid return type or parameter type
  // method 'SetPictureAlignment' not emitted because of invalid return type or parameter type
  BOOL GetPictureTiling();
  void SetPictureTiling(BOOL bNewValue);
  // method 'GetPicturePages' not emitted because of invalid return type or parameter type
  // method 'SetPicturePages' not emitted because of invalid return type or parameter type
  CString GetMenuBar();
  void SetMenuBar(LPCTSTR lpszNewValue);
  CString GetToolbar();
  void SetToolbar(LPCTSTR lpszNewValue);
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  short GetGridX();
  void SetGridX(short nNewValue);
  short GetGridY();
  void SetGridY(short nNewValue);
  BOOL GetLayoutForPrint();
  void SetLayoutForPrint(BOOL bNewValue);
  BOOL GetFastLaserPrinting();
  void SetFastLaserPrinting(BOOL bNewValue);
  CString GetHelpFile();
  void SetHelpFile(LPCTSTR lpszNewValue);
  long GetHelpContextId();
  void SetHelpContextId(long nNewValue);
  long GetHwnd();
  void SetHwnd(long nNewValue);
  short GetCount();
  void SetCount(short nNewValue);
  long GetPage();
  void SetPage(long nNewValue);
  short GetPages();
  void SetPages(short nNewValue);
  long GetHasData();
  void SetHasData(long nNewValue);
  long GetLeft();
  void SetLeft(long nNewValue);
  long GetTop();
  void SetTop(long nNewValue);
  long GetHeight();
  void SetHeight(long nNewValue);
  BOOL GetPrintSection();
  void SetPrintSection(BOOL bNewValue);
  BOOL GetNextRecord();
  void SetNextRecord(BOOL bNewValue);
  BOOL GetMoveLayout();
  void SetMoveLayout(BOOL bNewValue);
  short GetFormatCount();
  void SetFormatCount(short nNewValue);
  short GetPrintCount();
  void SetPrintCount(short nNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  BOOL GetPainting();
  void SetPainting(BOOL bNewValue);
  VARIANT GetPrtMip();
  void SetPrtMip(const VARIANT& newValue);
  VARIANT GetPrtDevMode();
  void SetPrtDevMode(const VARIANT& newValue);
  VARIANT GetPrtDevNames();
  void SetPrtDevNames(const VARIANT& newValue);
  long GetForeColor();
  void SetForeColor(long nNewValue);
  float GetCurrentX();
  void SetCurrentX(float newValue);
  float GetCurrentY();
  void SetCurrentY(float newValue);
  float GetScaleHeight();
  void SetScaleHeight(float newValue);
  float GetScaleLeft();
  void SetScaleLeft(float newValue);
  short GetScaleMode();
  void SetScaleMode(short nNewValue);
  float GetScaleTop();
  void SetScaleTop(float newValue);
  float GetScaleWidth();
  void SetScaleWidth(float newValue);
  short GetFontBold();
  void SetFontBold(short nNewValue);
  short GetFontItalic();
  void SetFontItalic(short nNewValue);
  CString GetFontName();
  void SetFontName(LPCTSTR lpszNewValue);
  short GetFontSize();
  void SetFontSize(short nNewValue);
  short GetFontUnderline();
  void SetFontUnderline(short nNewValue);
  short GetDrawMode();
  void SetDrawMode(short nNewValue);
  short GetDrawStyle();
  void SetDrawStyle(short nNewValue);
  short GetDrawWidth();
  void SetDrawWidth(short nNewValue);
  long GetFillColor();
  void SetFillColor(long nNewValue);
  short GetFillStyle();
  void SetFillStyle(short nNewValue);
  CString GetPaletteSource();
  void SetPaletteSource(LPCTSTR lpszNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  VARIANT GetPaintPalette();
  void SetPaintPalette(const VARIANT& newValue);
  CString GetOnMenu();
  void SetOnMenu(LPCTSTR lpszNewValue);
  CString GetOnOpen();
  void SetOnOpen(LPCTSTR lpszNewValue);
  CString GetOnClose();
  void SetOnClose(LPCTSTR lpszNewValue);
  CString GetOnActivate();
  void SetOnActivate(LPCTSTR lpszNewValue);
  CString GetOnDeactivate();
  void SetOnDeactivate(LPCTSTR lpszNewValue);
  CString GetOnNoData();
  void SetOnNoData(LPCTSTR lpszNewValue);
  CString GetOnPage();
  void SetOnPage(LPCTSTR lpszNewValue);
  CString GetOnError();
  void SetOnError(LPCTSTR lpszNewValue);
  BOOL GetDirty();
  void SetDirty(BOOL bNewValue);
  long GetCurrentRecord();
  void SetCurrentRecord(long nNewValue);
  VARIANT GetPictureData();
  void SetPictureData(const VARIANT& newValue);
  VARIANT GetPicturePalette();
  void SetPicturePalette(const VARIANT& newValue);
  BOOL GetHasModule();
  void SetHasModule(BOOL bNewValue);
  // method 'GetOrientation' not emitted because of invalid return type or parameter type
  // method 'SetOrientation' not emitted because of invalid return type or parameter type
  CString GetInputParameters();
  void SetInputParameters(LPCTSTR lpszNewValue);
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetActiveControl();
  LPDISPATCH GetDefaultControl(long ControlType);
  void Circle(short flags, float X, float Y, float radius, long color, float start, float end, float aspect);
  void Line(short flags, float x1, float y1, float x2, float y2, long color);
  void PSet(short flags, float X, float Y, long color);
  void Scale(short flags, float x1, float y1, float x2, float y2);
  float TextWidth(LPCTSTR Expr);
  float TextHeight(LPCTSTR Expr);
  void Print(LPCTSTR Expr);
  LPDISPATCH GetSection(const VARIANT& Index);
  LPDISPATCH GetGroupLevel(long Index);
  LPDISPATCH GetReport();
  LPDISPATCH GetModule();
  LPDISPATCH GetProperties();
  LPDISPATCH GetControls();
  CString GetName();
  void SetName(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// Reports wrapper class

class Reports : public COleDispatchDriver
{
public:
  Reports() {}    // Calls COleDispatchDriver default constructor
  Reports(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Reports(const Reports& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetItem(const VARIANT& Index);
  long GetCount();
};
/////////////////////////////////////////////////////////////////////////////
// Screen wrapper class

class Screen : public COleDispatchDriver
{
public:
  Screen() {}   // Calls COleDispatchDriver default constructor
  Screen(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Screen(const Screen& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetActiveDatasheet();
  LPDISPATCH GetActiveControl();
  LPDISPATCH GetPreviousControl();
  LPDISPATCH GetActiveForm();
  LPDISPATCH GetActiveReport();
  short GetMousePointer();
  void SetMousePointer(short nNewValue);
  LPDISPATCH GetActiveDataAccessPage();
};
/////////////////////////////////////////////////////////////////////////////
// _Application wrapper class

class _Application : public COleDispatchDriver
{
public:
  _Application() {}   // Calls COleDispatchDriver default constructor
  _Application(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Application(const _Application& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetCodeContextObject();
  void NewCurrentDatabase(LPCTSTR filepath);
  void OpenCurrentDatabase(LPCTSTR filepath, BOOL Exclusive);
  CString GetMenuBar();
  void SetMenuBar(LPCTSTR lpszNewValue);
  long GetCurrentObjectType();
  CString GetCurrentObjectName();
  VARIANT GetOption(LPCTSTR OptionName);
  void SetOption(LPCTSTR OptionName, const VARIANT& Setting);
  void Echo(short EchoOn, LPCTSTR bstrStatusBarText);
  void CloseCurrentDatabase();
  void Quit(long Option);
  LPDISPATCH GetForms();
  LPDISPATCH GetReports();
  LPDISPATCH GetScreen();
  LPDISPATCH GetDoCmd();
  CString GetShortcutMenuBar();
  void SetShortcutMenuBar(LPCTSTR lpszNewValue);
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  BOOL GetUserControl();
  void SetUserControl(BOOL bNewValue);
  VARIANT SysCmd(long Action, const VARIANT& Argument2, const VARIANT& Argument3);
  LPDISPATCH CreateForm(const VARIANT& Database, const VARIANT& FormTemplate);
  LPDISPATCH CreateReport(const VARIANT& Database, const VARIANT& ReportTemplate);
  LPDISPATCH CreateControl(LPCTSTR FormName, long ControlType, long Section, const VARIANT& Parent, const VARIANT& ColumnName, const VARIANT& Left, const VARIANT& Top, const VARIANT& Width, const VARIANT& Height);
  LPDISPATCH CreateReportControl(LPCTSTR ReportName, long ControlType, long Section, const VARIANT& Parent, const VARIANT& ColumnName, const VARIANT& Left, const VARIANT& Top, const VARIANT& Width, const VARIANT& Height);
  void DeleteControl(LPCTSTR FormName, LPCTSTR ControlName);
  void DeleteReportControl(LPCTSTR ReportName, LPCTSTR ControlName);
  long CreateGroupLevel(LPCTSTR ReportName, LPCTSTR Expression, short Header, short Footer);
  VARIANT DMin(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DMax(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DSum(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DAvg(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DLookup(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DLast(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DVar(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DVarP(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DStDev(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DStDevP(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DFirst(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT DCount(LPCTSTR Expr, LPCTSTR Domain, const VARIANT& Criteria);
  VARIANT Eval(LPCTSTR StringExpr);
  CString CurrentUser();
  VARIANT DDEInitiate(LPCTSTR Application, LPCTSTR Topic);
  void DDEExecute(const VARIANT& ChanNum, LPCTSTR Command);
  void DDEPoke(const VARIANT& ChanNum, LPCTSTR Item, LPCTSTR Data);
  CString DDERequest(const VARIANT& ChanNum, LPCTSTR Item);
  void DDETerminate(const VARIANT& ChanNum);
  void DDETerminateAll();
  LPDISPATCH GetDBEngine();
  LPDISPATCH CurrentDb();
  LPDISPATCH CodeDb();
  CString BuildCriteria(LPCTSTR Field, short FieldType, LPCTSTR Expression);
  LPDISPATCH DefaultWorkspaceClone();
  void RefreshTitleBar();
  long hWndAccessApp();
  VARIANT Run(LPCTSTR Procedure, VARIANT* Arg1, VARIANT* Arg2, VARIANT* Arg3, VARIANT* Arg4, VARIANT* Arg5, VARIANT* Arg6, VARIANT* Arg7, VARIANT* Arg8, VARIANT* Arg9, VARIANT* Arg10, VARIANT* Arg11, VARIANT* Arg12, VARIANT* Arg13, 
    VARIANT* Arg14, VARIANT* Arg15, VARIANT* Arg16, VARIANT* Arg17, VARIANT* Arg18, VARIANT* Arg19, VARIANT* Arg20, VARIANT* Arg21, VARIANT* Arg22, VARIANT* Arg23, VARIANT* Arg24, VARIANT* Arg25, VARIANT* Arg26, VARIANT* Arg27, 
    VARIANT* Arg28, VARIANT* Arg29, VARIANT* Arg30);
  VARIANT Nz(const VARIANT& Value, const VARIANT& ValueIfNull);
  LPDISPATCH LoadPicture(LPCTSTR FileName);
  VARIANT AccessError(const VARIANT& ErrorNumber);
  VARIANT StringFromGUID(const VARIANT& Guid);
  VARIANT GUIDFromString(const VARIANT& String);
  LPDISPATCH GetCommandBars();
  LPDISPATCH GetAssistant();
  void FollowHyperlink(LPCTSTR Address, LPCTSTR SubAddress, BOOL NewWindow, BOOL AddHistory, const VARIANT& ExtraInfo, long Method, LPCTSTR HeaderInfo);
  void AddToFavorites();
  void RefreshDatabaseWindow();
  LPDISPATCH GetReferences();
  LPDISPATCH GetModules();
  LPDISPATCH GetFileSearch();
  BOOL GetIsCompiled();
  void RunCommand(long Command);
  CString HyperlinkPart(const VARIANT& Hyperlink, long Part);
  BOOL GetHiddenAttribute(long ObjectType, LPCTSTR ObjectName);
  void SetHiddenAttribute(long ObjectType, LPCTSTR ObjectName, BOOL fHidden);
  LPDISPATCH GetVbe();
  LPDISPATCH GetDataAccessPages();
  LPDISPATCH CreateDataAccessPage(const VARIANT& FileName, BOOL CreateNewFile);
  LPDISPATCH GetCurrentProject();
  LPDISPATCH GetCurrentData();
  LPDISPATCH GetCodeProject();
  LPDISPATCH GetCodeData();
  void NewAccessProject(LPCTSTR filepath, const VARIANT& Connect);
  void OpenAccessProject(LPCTSTR filepath, BOOL Exclusive);
  void CreateAccessProject(LPCTSTR filepath, const VARIANT& Connect);
  CString GetProductCode();
  LPDISPATCH GetCOMAddIns();
  CString GetName();
  LPDISPATCH GetDefaultWebOptions();
  LPDISPATCH GetLanguageSettings();
  LPDISPATCH GetAnswerWizard();
  long GetFeatureInstall();
  void SetFeatureInstall(long nNewValue);
  double EuroConvert(double Number, LPCTSTR SourceCurrency, LPCTSTR TargetCurrency, const VARIANT& FullPrecision, const VARIANT& TriangulationPrecision);
};
/////////////////////////////////////////////////////////////////////////////
// Reference wrapper class

class Reference : public COleDispatchDriver
{
public:
  Reference() {}    // Calls COleDispatchDriver default constructor
  Reference(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  Reference(const Reference& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetCollection();
  CString GetName();
  CString GetGuid();
  long GetMajor();
  long GetMinor();
  CString GetFullPath();
  BOOL GetBuiltIn();
  BOOL GetIsBroken();
  long GetKind();
};
/////////////////////////////////////////////////////////////////////////////
// _References wrapper class

class _References : public COleDispatchDriver
{
public:
  _References() {}    // Calls COleDispatchDriver default constructor
  _References(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _References(const _References& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetParent();
  LPDISPATCH Item(const VARIANT& var);
  long GetCount();
  LPUNKNOWN _NewEnum();
  LPDISPATCH AddFromGuid(LPCTSTR Guid, long Major, long Minor);
  LPDISPATCH AddFromFile(LPCTSTR FileName);
  void Remove(LPDISPATCH Reference);
};
/////////////////////////////////////////////////////////////////////////////
// _References_Events wrapper class

class _References_Events : public COleDispatchDriver
{
public:
  _References_Events() {}   // Calls COleDispatchDriver default constructor
  _References_Events(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _References_Events(const _References_Events& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  void ItemAdded(LPDISPATCH Reference);
  void ItemRemoved(LPDISPATCH Reference);
};
/////////////////////////////////////////////////////////////////////////////
// _Dummy wrapper class

class _Dummy : public COleDispatchDriver
{
public:
  _Dummy() {}   // Calls COleDispatchDriver default constructor
  _Dummy(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _Dummy(const _Dummy& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
};
/////////////////////////////////////////////////////////////////////////////
// _DataAccessPage wrapper class

class _DataAccessPage : public COleDispatchDriver
{
public:
  _DataAccessPage() {}    // Calls COleDispatchDriver default constructor
  _DataAccessPage(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _DataAccessPage(const _DataAccessPage& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  CString GetName();
  BOOL GetVisible();
  void SetVisible(BOOL bNewValue);
  CString GetTag();
  void SetTag(LPCTSTR lpszNewValue);
  long GetWindowWidth();
  long GetWindowHeight();
  short GetCurrentView();
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetDocument();
  void ApplyTheme(LPCTSTR ThemeName);
  LPDISPATCH GetWebOptions();
  CString GetConnectionString();
  void SetConnectionString(LPCTSTR lpszNewValue);
};
/////////////////////////////////////////////////////////////////////////////
// DataAccessPages wrapper class

class DataAccessPages : public COleDispatchDriver
{
public:
  DataAccessPages() {}    // Calls COleDispatchDriver default constructor
  DataAccessPages(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  DataAccessPages(const DataAccessPages& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetItem(const VARIANT& var);
  long GetCount();
};
/////////////////////////////////////////////////////////////////////////////
// AllObjects wrapper class

class AllObjects : public COleDispatchDriver
{
public:
  AllObjects() {}   // Calls COleDispatchDriver default constructor
  AllObjects(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  AllObjects(const AllObjects& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetItem(const VARIANT& var);
  long GetCount();
};
/////////////////////////////////////////////////////////////////////////////
// AccessObjectProperty wrapper class

class AccessObjectProperty : public COleDispatchDriver
{
public:
  AccessObjectProperty() {}   // Calls COleDispatchDriver default constructor
  AccessObjectProperty(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  AccessObjectProperty(const AccessObjectProperty& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  CString GetName();
  VARIANT GetValue();
  void SetValue(const VARIANT& newValue);
};
/////////////////////////////////////////////////////////////////////////////
// AccessObjectProperties wrapper class

class AccessObjectProperties : public COleDispatchDriver
{
public:
  AccessObjectProperties() {}   // Calls COleDispatchDriver default constructor
  AccessObjectProperties(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  AccessObjectProperties(const AccessObjectProperties& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  LPDISPATCH GetItem(const VARIANT& Index);
  long GetCount();
  void Add(LPCTSTR PropertyName, const VARIANT& Value);
  void Remove(const VARIANT& Item);
};
/////////////////////////////////////////////////////////////////////////////
// _CurrentProject wrapper class

class _CurrentProject : public COleDispatchDriver
{
public:
  _CurrentProject() {}    // Calls COleDispatchDriver default constructor
  _CurrentProject(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _CurrentProject(const _CurrentProject& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetAllForms();
  LPDISPATCH GetAllReports();
  LPDISPATCH GetAllMacros();
  LPDISPATCH GetAllModules();
  LPDISPATCH GetAllDataAccessPages();
  long GetProjectType();
  CString GetBaseConnectionString();
  BOOL GetIsConnected();
  void OpenConnection(const VARIANT& BaseConnectionString, const VARIANT& UserID, const VARIANT& Password);
  void CloseConnection();
  CString GetName();
  CString GetPath();
  CString GetFullName();
  LPDISPATCH GetConnection();
  LPDISPATCH GetProperties();
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
};
/////////////////////////////////////////////////////////////////////////////
// _CurrentData wrapper class

class _CurrentData : public COleDispatchDriver
{
public:
  _CurrentData() {}   // Calls COleDispatchDriver default constructor
  _CurrentData(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _CurrentData(const _CurrentData& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetAllTables();
  LPDISPATCH GetAllQueries();
  LPDISPATCH GetAllViews();
  LPDISPATCH GetAllStoredProcedures();
  LPDISPATCH GetAllDatabaseDiagrams();
};
/////////////////////////////////////////////////////////////////////////////
// AccessObject wrapper class

class AccessObject : public COleDispatchDriver
{
public:
  AccessObject() {}   // Calls COleDispatchDriver default constructor
  AccessObject(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  AccessObject(const AccessObject& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetParent();
  CString GetName();
  long GetType();
  LPDISPATCH GetProperties();
  BOOL GetIsLoaded();
  CString GetFullName();
};
/////////////////////////////////////////////////////////////////////////////
// _WizHook wrapper class

class _WizHook : public COleDispatchDriver
{
public:
  _WizHook() {}   // Calls COleDispatchDriver default constructor
  _WizHook(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _WizHook(const _WizHook& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
};
/////////////////////////////////////////////////////////////////////////////
// _DefaultWebOptions wrapper class

class _DefaultWebOptions : public COleDispatchDriver
{
public:
  _DefaultWebOptions() {}   // Calls COleDispatchDriver default constructor
  _DefaultWebOptions(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _DefaultWebOptions(const _DefaultWebOptions& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  long GetHyperlinkColor();
  void SetHyperlinkColor(long nNewValue);
  long GetFollowedHyperlinkColor();
  void SetFollowedHyperlinkColor(long nNewValue);
  BOOL GetUnderlineHyperlinks();
  void SetUnderlineHyperlinks(BOOL bNewValue);
  BOOL GetOrganizeInFolder();
  void SetOrganizeInFolder(BOOL bNewValue);
  BOOL GetUseLongFileNames();
  void SetUseLongFileNames(BOOL bNewValue);
  BOOL GetCheckIfOfficeIsHTMLEditor();
  void SetCheckIfOfficeIsHTMLEditor(BOOL bNewValue);
  BOOL GetDownloadComponents();
  void SetDownloadComponents(BOOL bNewValue);
  CString GetLocationOfComponents();
  void SetLocationOfComponents(LPCTSTR lpszNewValue);
  long GetEncoding();
  void SetEncoding(long nNewValue);
  BOOL GetAlwaysSaveInDefaultEncoding();
  void SetAlwaysSaveInDefaultEncoding(BOOL bNewValue);
  CString GetFolderSuffix();
};
/////////////////////////////////////////////////////////////////////////////
// _WebOptions wrapper class

class _WebOptions : public COleDispatchDriver
{
public:
  _WebOptions() {}    // Calls COleDispatchDriver default constructor
  _WebOptions(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  _WebOptions(const _WebOptions& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
  LPDISPATCH GetApplication();
  LPDISPATCH GetParent();
  BOOL GetOrganizeInFolder();
  void SetOrganizeInFolder(BOOL bNewValue);
  BOOL GetUseLongFileNames();
  void SetUseLongFileNames(BOOL bNewValue);
  BOOL GetDownloadComponents();
  void SetDownloadComponents(BOOL bNewValue);
  CString GetLocationOfComponents();
  void SetLocationOfComponents(LPCTSTR lpszNewValue);
  long GetEncoding();
  void SetEncoding(long nNewValue);
  CString GetFolderSuffix();
  void UseDefaultFolderSuffix();
};
