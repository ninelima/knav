//-----------------------------------------------------------------------------
// $Id: WindTriangle.cpp,v 1.4 2004/08/19 13:08:56 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//-----------------------------------------------------------------------------

#include <math.h>
#include <stdlib.h>
#include "uvalues.h"
#include "WindTriangle.h"


//-----------------------------------------------------------------------------
// Default constructor.
//
// <p> Note:
// <br> Units of the speed is irrelevant as long as they are consistent.
// <br> Angles are measured in degrees, oriented for a compass rose.
//
/*void WindTriangle()
{
} */

bool WindTriangle::calcWdWs(void)
{
  /*
  (1) Unknown Wind:
  WS=sqrt( (TAS-GS)^2+ 4*TAS*GS*(sin((HD-TRK)/2))^2 )
  WD=TRK + atan2(TAS*sin(HD-TRK), TAS*cos(HD-TRK)-GS)  (**)
  IF (WD<0) THEN WD=WD+2*pi
  IF (WD>2*pi) THEN WD=WD-2*pi
     ( (**) assumes atan2(y,x), reverse arguments if your implementation
  has atan2(x,y) )
  */
   return false;
}

//-----------------------------------------------------------------------------
// calculate the heading (HD) and groundspeed (GS)
//
// @param trk the track
// @param tas the true airspeed
// @param wd the wind direction (from)
// @param ws the wind speed
// @param *hd the heading (out)
// @param *gs the ground speed (out)
//
// @return true for success or false for failure or impossible calculation
//
bool WindTriangle::calcHdGs(double trk, double tas, double wd, double ws,
                            double *hd, double *gs)
{
  double m_HD, m_GS;

  double TRK   = trk * M_PI / 180.0;
  double m_TAS = tas;
  double WD    = wd * M_PI / 180.0;
  double m_WS  = ws;

 //(2) Find HD, GS
  double SWC = (m_WS/m_TAS) * sin(WD-TRK);
  if (fabs(SWC) > 1.0) {
    //"course cannot be flown-- wind too strong"
    return false;
  }
  else {
    m_HD=TRK + asin(SWC);
    if (m_HD < 0) m_HD = m_HD + 2 * M_PI;
    if (m_HD > 2 * M_PI) m_HD = m_HD - 2 * M_PI;
    m_GS = m_TAS * sqrt(1 - SWC*SWC)-m_WS * cos(WD - TRK);

    // convert to deg
    m_HD = m_HD * 180.0 / M_PI;

    *hd = m_HD;
    *gs = m_GS;
    return true;
  }
}

//-----------------------------------------------------------------------------
// calculate the heading (HD) and groundspeed (GS)
//
// @param trk the track
// @param tas the true airspeed
// @param wind the wind direction and speed <i>dddss</i>
//
// @return true for success or false for failure or impossible calculation
//
bool WindTriangle::calcHdGs(double trk, double tas, int wind,
                            double *hd, double *gs)
{
  int ws, wd;

  wd = wind / 100;
  ws = wind - (wd * 100);

  return WindTriangle::calcHdGs(trk, tas, wd, ws, hd, gs);
}

//-----------------------------------------------------------------------------
// calculate the heading (HD) and groundspeed (GS)
//
// @param trk the track
// @param tas the true airspeed
// @param wind the wind direction and speed dddss
//
// @return true for success or false for failure or impossible calculation
//
bool WindTriangle::calcHdGs(double trk, double tas, int wind,
                            int *hd, int *gs)
{
  double rhd, rgs;

  bool rv = WindTriangle::calcHdGs(trk, tas, wind, &rhd, &rgs);

  *hd = (int) rhd;
  *gs = (int) rgs;
  return rv;
}

/*
(3) Find CRS, GS
  GS=sqrt(WS^2 + TAS^2 - 2*WS*TAS*cos(HD-WD))
  WCA=atan2(WS*sin(HD-WD),TAS-WS*cos(HD-WD))  (*)
  CRS=MOD(HD+WCA,2*pi)
(*) WCA=asin((WS/GS)*sin(HD-WD)) works if the wind correction
angle is less than 90 degrees, which will always be the case if
WS < TAS. The listed formula works in the general case
*/


