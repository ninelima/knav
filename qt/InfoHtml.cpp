//-----------------------------------------------------------------------------
// $Id: InfoHtml.cpp,v 1.14 2006/03/18 16:17:17 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <stdlib.h>
#include <math.h>
#include <qfile.h>
#include <qdatetime.h>
#include <qtextstream.h>
#include "InfoHtml.h"
#include "MagField.h"
#include "ufile.h"


//-----------------------------------------------------------------------------
// Return the magnetic variation for a given lat/long
// 
double InfoHtml::getMagVar(double lat, double lon)
{
  int ivar = (int) rad_to_deg(SGMagVar(deg_to_rad(lat),
                                       deg_to_rad(lon),
                                       0,  //height in km
                                       yymmdd_to_julian_days(02,01,01),
                                       7)); //model
  return -ivar;
}


//-----------------------------------------------------------------------------
//
// 
InfoHtml::InfoHtml()
{
  //wpt = new CWptSet();
  //wpt->Open("wpt.csv");
}

//-----------------------------------------------------------------------------
//
// 
void InfoHtml::buildAirportHtml(QString icao)
{
  QString buffer;
  buffer = QString().sprintf("%04.2f%c %05.2f%c <br>\n %ift (%i)", fabs(wpt->m_rLat), wpt->m_rLat>0?'N':'S',
                                                                   fabs(wpt->m_rLon), wpt->m_rLon>0?'E':'W',
                                                                   (int)wpt->m_rElev,
                                                                   (int)getMagVar(wpt->m_rLat, wpt->m_rLon));

  printf("InfoHtml::buildAirportHtml - creating airport/%s.html\n", icao.latin1());
  QFile file(QString("airport/%1.html").arg(icao));

  // Perform a sanity check
  if (file.exists())
    return;

  file.open(IO_WriteOnly);
  QTextStream ts(&file);

  ts << "<html>\n";
  ts << "<body>\n";
  ts << "<!--TITLE-->\n";
  ts << "<hr>\n";
  ts << "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">\n";
  ts << "  <tr valign=\"middle\" bgcolor=\"#FFFF00\">\n";
  ts << "    <td nowrap align=\"center\"><font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"5\"><b>";
  ts << "<!--ICAO-->\n";
  ts << wpt->m_ICAO << "\n";
  ts << "<!--/ICAO-->\n";
  ts << "</b></font></td>\n";
  ts << "    <td width=\"99%\" bgcolor=\"#FFFFFF\"> <font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"+1\"><b>\n";
  ts << "<!--INFO-->\n";
  ts << wpt->m_INFO << "\n";
  ts << "<!--/INFO-->\n";
  ts << "</b><br>\n";
  ts << "<!--COUNTRY-->\n";
  ts << sCountry << "\n";
  ts << "<!--/COUNTRY-->\n";
  ts << "\n, \n";
  ts << "<!--REGION-->\n";
  ts << sRegion << "\n";
  ts << "<!--/REGION-->\n";
  ts << "</font> </td>\n";
  ts << "    <td nowrap bgcolor=\"#FFFFFF\"></td>\n";
  ts << "  </tr>\n";
  ts << "</table>\n";
  ts << "<hr>\n";

  ts << "<pre>\n";
  ts << "<!--SPATIAL-->\n";
  ts << buffer + "\n";
  ts << "<!--/SPATIAL-->\n";
  ts << "</pre>\n";
  //ts << "<p><b>Frequencies</b><br>\n";
  ts << "<!--FREQ-->\n";
  //ts << FreqMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
  ts << "<!--/FREQ-->\n";

  ts << "<!--RWY-->\n";
  //ts << RwyMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
  ts << "<!--/RWY-->\n";
  //ts << "<p><b>Comments</b><br>\n";
  ts << "<!--COMMENT-->\n";
  //ts << CommentMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
  ts << "<!--/COMMENT-->\n";
  //ts << "<p><b>Fuel</b><br>\n";
  ts << "<!--FUEL-->\n";
  //ts << FuelMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
  ts << "<!--/FUEL-->\n";
  //ts << "<p><b>Operator</b><br>\n";
  ts << "<!--OPERATOR-->\n";
  //ts << OprMultiLineEdit->text().replace(QRegExp("\n"), "<br>\n") << "\n";
  ts << "<!--/OPERATOR-->\n";
  ts << "<!--IMAGE-->\n";
  // <br><a href="./img/YPJT.png"><img src="./img/YPJT.png" width=240 height=150 border=0></p>
  //for (int i = 0; i < ImgListBox->count(); i++) {
  //  ts << "<br><a href=\"" << ImgListBox->text(i) << "\"> "
  //     << "<img src=\"" << ImgListBox->text(i)
  //     << "\" width=240 height=150 border=0> \n";
  //}
  ts << "<!--/IMAGE-->\n";
  ts << "<font face=\"Verdana, Arial, Helvetica, sans-serif\" size=\"1\">\n";
  ts << "<br>System generated - No additional information found.\n";
  ts << "</font>";

  ts << "</body>\n";
  ts << "</html>\n";
  file.close();
}

//-----------------------------------------------------------------------------
//
// 
void InfoHtml::buildAirportDefaultHtml()
{
  printf("InfoHtml::buildAirportHtml - creating <TEMP>/default.html for 0\n");
  QFile file(QString("%1/%2").arg(getenv("TEMP")).arg("default.html"));
  file.open(IO_WriteOnly);
  QTextStream ts(&file);

  ts << ("<html>\n");
  ts << ("<body>\n");
  ts << ("    <b>\n");
  ts << ("      Default Information\n");
  ts << ("    </b>\n");
  ts << ("    <p>\n");
  ts << ("      No item is currently selected or no information\n");
  ts << ("      is available on the selected item.\n");
  ts << ("    </p>\n");
  ts << ("    <!--\n");
  ts << ("    <img src=\"knav_small_logo.png\" alt=\"knav logo\" width=\"185\" height=\"114\" name=\"Graphic1\">\n");
  ts << ("    -->\n");
  ts << ("  </body>\n");
  ts << ("</html>\n");

  file.close();
}


//-----------------------------------------------------------------------------
// Create a Airport info html file
// 
bool InfoHtml::createAirportInfoHtml(QString icao)
{
  wpt = new CWptSet();
  wpt->Open("wpt.csv");

  if (icao != "0") {
    wpt->MoveFirst();
    // First we find the corresponding entry
    while (wpt->IsEOF() == false) {
      if (wpt->m_ICAO == "_rgn") {
        sRegion = wpt->m_INFO;
      }

      if (wpt->m_ICAO == "_cnt") {
        sCountry = wpt->m_INFO;
      }

      if (wpt->m_ICAO == icao) {
        // build up the html page (wpt derived) for display in the viewer
        buildAirportHtml(icao);
        return true;
      }
      wpt->MoveNext();
    }
  }
  else {
    // build up the html page (default) for display in the viewer
    buildAirportDefaultHtml();
    return false;
  }

  // we will only reach this far if the icao string was 
  // not found in the wpt database
  buildAirportDefaultHtml();
  return false;
}


//-----------------------------------------------------------------------------
// Create a Data Comms info html file
// 

void InfoHtml::createCommsInfoHtml(QString sFilename, QString sRegistration, QString sDateTime, QString sType, QString msg)
{
  printf("InfoHtml::buildAirportHtml - creating html/notification_data.txt\n");
  //QString sDateTime = QDateTime::currentDateTime(Qt::UTC).toString("ddd, d-MMMM-yyyy hh:mm");
  QFile file(QString("html/" + sFilename + ".txt"));
  file.open(IO_WriteOnly | IO_Append);
  QTextStream ts(&file);
  ts << "<tr>\n";
  ts << "  <td>" + sRegistration + "</td>\n";
  ts << "  <td>" + sDateTime + "</td>\n";
  ts << "  <td>" + sType + "</td>\n";
  ts << "  <td>" + msg + "</td>\n";
  ts << "</tr>\n";
  file.close();

  file.setName(QString("html/" + sFilename + ".html"));
  file.open(IO_WriteOnly);
  ts.setDevice(&file);

  // Headings
  ts << "<html>\n";
  ts << "<body>\n";
  ts << "<div align=\"left\">\n";
  ts << "  <table border=\"0\">\n";
  ts << "    <tr bgcolor=\"#CCCCCC\">\n";
  ts << "      <td><span>Reg </span></td>\n";
  ts << "      <td><span>Date Time</span></td>\n";
  ts << "      <td><span>Type</span></td>\n";
  ts << "      <td><span>Description</span></td>\n";
  ts << "    </tr>\n";
  ts << "<!------------------->\n";
  file.close();

  FileAppend(QString("html/" + sFilename + ".html").latin1(), 
             QString("html/" + sFilename + ".txt").latin1());

  file.open(IO_WriteOnly | IO_Append);
  ts.setDevice(&file);
  ts << "<!------------------->\n";
  ts << "  </table>\n";
  ts << "</div>\n";
  ts << "</body>\n";
  ts << "</html>\n";
  file.close();
}


