//
// $Id: E6BGadget.h,v 1.4 2004/04/16 15:19:43 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//

#include <qwidget.h>
#include <qpixmap.h>

#ifndef __GRAPHICPAINTER_H
  #include "GraphicPainter.h"
#endif // __GRAPHICPAINTER_H

class E6BGadget : public QWidget
{
public:
  int hotspot; /** 80 sets hotspot to the arrow */
  QRect rect;

  E6BGadget(QWidget *parent=0, const char *name=0, WFlags f=0);

  void paintEvent(QPaintEvent* e);
  bool bE6BActive;
  int offsetE6B;
  int lubberE6B;
  void incE6B(void); 
  void decE6B(void);
  void setE6B(int a); 
  void setE6Blubber(int a); 
  void drawE6B(GraphicPainter *gc);

  void mousePressEvent(QMouseEvent *e);
  void mouseReleaseEvent(QMouseEvent *e);
  void mouseMoveEvent(QMouseEvent *e);
  void resizeEvent(QResizeEvent* event);

private:
  QPixmap pixbuffer;

    bool m_bMouseLeftButtonDown;
    bool m_bMouseRightButtonDown;
};
