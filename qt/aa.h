/****************************************************************************
** Form interface generated from reading ui file 'FlightNotification_3.ui'
**
** Created: Sun Dec 5 13:06:55 2004
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.3   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef FLIGHT_NOTIFICATION_H
#define FLIGHT_NOTIFICATION_H

#include <qvariant.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QTabWidget;
class QWidget;
class QLineEdit;
class QComboBox;
class QLabel;
class QButtonGroup;
class QCheckBox;
class QPushButton;

class Flight_Notification : public QDialog
{
    Q_OBJECT

public:
    Flight_Notification( QWidget* parent = 0, const char* name = 0, bool modal = FALSE, WFlags fl = 0 );
    ~Flight_Notification();

    QTabWidget* tabWidget2;
    QWidget* tab;
    QLineEdit* lineEdit1;
    QComboBox* comboBox2;
    QLabel* textLabel1_2;
    QLabel* textLabel2;
    QLineEdit* lineEdit3;
    QLineEdit* lineEdit2;
    QComboBox* comboBox1;
    QLabel* textLabel2_2;
    QLabel* textLabel3_2;
    QLabel* textLabel1;
    QLabel* textLabel3;
    QComboBox* comboBox4;
    QButtonGroup* buttonGroup1;
    QLabel* textLabel5;
    QCheckBox* checkBox9;
    QCheckBox* checkBox3;
    QCheckBox* checkBox10;
    QCheckBox* checkBox7;
    QCheckBox* checkBox4;
    QCheckBox* checkBox2;
    QCheckBox* checkBox1;
    QCheckBox* checkBox5;
    QCheckBox* checkBox6;
    QCheckBox* checkBox8;
    QCheckBox* checkBox11;
    QCheckBox* checkBox16;
    QCheckBox* checkBox15;
    QCheckBox* checkBox14;
    QCheckBox* checkBox13;
    QCheckBox* checkBox12;
    QLabel* textLabel3_3;
    QLabel* textLabel2_3;
    QComboBox* comboBox3;
    QLabel* textLabel7;
    QLineEdit* lineEdit5;
    QLabel* textLabel8;
    QLabel* textLabel5_2;
    QLineEdit* lineEdit4;
    QLabel* textLabel4;
    QLineEdit* lineEdit7;
    QLabel* textLabel6;
    QLineEdit* lineEdit8;
    QLabel* textLabel1_3;
    QLineEdit* lineEdit6;
    QWidget* tab_2;
    QLineEdit* lineEdit9;
    QLineEdit* lineEdit10;
    QLineEdit* lineEdit12;
    QLineEdit* lineEdit13;
    QLabel* textLabel7_2_2;
    QLineEdit* lineEdit11;
    QLineEdit* lineEdit9_2;
    QLineEdit* lineEdit10_2;
    QLineEdit* lineEdit13_2;
    QLineEdit* lineEdit12_2;
    QLabel* textLabel7_2_2_2;
    QLineEdit* lineEdit11_2;
    QLineEdit* lineEdit13_3;
    QLabel* textLabel7_2_2_3;
    QLineEdit* lineEdit10_3;
    QLineEdit* lineEdit9_3;
    QLineEdit* lineEdit12_3;
    QLineEdit* lineEdit11_3;
    QLineEdit* lineEdit10_4;
    QLineEdit* lineEdit9_4;
    QLineEdit* lineEdit13_4;
    QLineEdit* lineEdit12_4;
    QLabel* textLabel7_2_2_4;
    QLineEdit* lineEdit11_4;
    QLineEdit* lineEdit13_5;
    QLineEdit* lineEdit9_5;
    QLineEdit* lineEdit10_5;
    QLineEdit* lineEdit12_5;
    QLineEdit* lineEdit11_5;
    QLabel* textLabel7_2_2_5;
    QLabel* textLabel1_4;
    QLabel* textLabel2_4;
    QLabel* textLabel3_4;
    QLabel* textLabel5_3;
    QLabel* textLabel4_2;
    QLabel* textLabel7_2;
    QLineEdit* lineEdit9_6;
    QLineEdit* lineEdit10_6;
    QLabel* textLabel7_2_2_6;
    QLineEdit* lineEdit11_6;
    QLineEdit* lineEdit12_6;
    QLineEdit* lineEdit13_6;
    QLineEdit* lineEdit11_6_3;
    QLineEdit* lineEdit10_6_3;
    QLineEdit* lineEdit13_6_3;
    QLineEdit* lineEdit9_6_3;
    QLabel* textLabel7_2_2_6_3;
    QLineEdit* lineEdit12_6_3;
    QLabel* textLabel7_2_2_6_4;
    QLineEdit* lineEdit13_6_4;
    QLineEdit* lineEdit12_6_4;
    QLineEdit* lineEdit10_6_4;
    QLineEdit* lineEdit11_6_4;
    QLineEdit* lineEdit9_6_4;
    QLineEdit* lineEdit12_6_5;
    QLineEdit* lineEdit11_6_5;
    QLineEdit* lineEdit9_6_5;
    QLabel* textLabel7_2_2_6_5;
    QLineEdit* lineEdit10_6_5;
    QLineEdit* lineEdit13_6_5;
    QLabel* textLabel7_2_2_6_6;
    QLineEdit* lineEdit11_6_6;
    QLineEdit* lineEdit12_6_6;
    QLineEdit* lineEdit9_6_6;
    QLineEdit* lineEdit13_6_6;
    QLineEdit* lineEdit10_6_6;
    QLineEdit* lineEdit13_6_7;
    QLineEdit* lineEdit9_6_7;
    QLineEdit* lineEdit12_6_7;
    QLineEdit* lineEdit11_6_7;
    QLineEdit* lineEdit10_6_7;
    QLabel* textLabel7_2_2_6_7;
    QLineEdit* lineEdit12_6_8;
    QLineEdit* lineEdit13_6_8;
    QLineEdit* lineEdit9_6_8;
    QLineEdit* lineEdit11_6_8;
    QLineEdit* lineEdit10_6_8;
    QLabel* textLabel7_2_2_6_8;
    QLabel* textLabel7_2_2_6_9;
    QLineEdit* lineEdit11_6_9;
    QLineEdit* lineEdit13_6_9;
    QLineEdit* lineEdit12_6_9;
    QLineEdit* lineEdit9_6_9;
    QLineEdit* lineEdit10_6_9;
    QLineEdit* lineEdit13_6_10;
    QLineEdit* lineEdit12_6_10;
    QLineEdit* lineEdit11_6_10;
    QLineEdit* lineEdit10_6_10;
    QLabel* textLabel7_2_2_6_10;
    QLineEdit* lineEdit9_6_10;
    QLineEdit* lineEdit10_6_11;
    QLineEdit* lineEdit11_6_11;
    QLineEdit* lineEdit12_6_11;
    QLineEdit* lineEdit9_6_11;
    QLineEdit* lineEdit13_6_11;
    QLabel* textLabel7_2_2_6_11;
    QLineEdit* lineEdit10_6_12;
    QLineEdit* lineEdit12_6_12;
    QLabel* textLabel7_2_2_6_12;
    QLineEdit* lineEdit13_6_12;
    QLineEdit* lineEdit11_6_12;
    QLineEdit* lineEdit9_6_12;
    QWidget* TabPage;
    QWidget* TabPage_2;
    QPushButton* pushButton1;
    QPushButton* pushButton2;

protected:
    QVBoxLayout* tabLayout;
    QGridLayout* layout7;
    QGridLayout* buttonGroup1Layout;
    QGridLayout* layout6;
    QGridLayout* tabLayout_2;
    QHBoxLayout* layout9;
    QSpacerItem* spacer1;

protected slots:
    virtual void languageChange();

};

#endif // FLIGHT_NOTIFICATION_H
