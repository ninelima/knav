#pragma once

#include "msacc9.h" // access automation

class CAccessReports : public CObject
{
DECLARE_DYNAMIC(CAccessReports)

public:
	CAccessReports(CString const& strDatabaseName, BOOL bLoadAllReports);
	~CAccessReports();	

protected:
	CString m_strDatabaseName;

protected:
	_Application* m_pAccess;
	DoCmd* m_pDoCmd;

public:
	CStringArray m_reports;
public:
	void LoadAllReports();

public:
	void RunReport(CString const& strReportName);
	void Print(CString const& strReportName);
	void SaveAsHtml(CString const& strReportName, CString const& strDestinationFolder);
};	
