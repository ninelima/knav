//-----------------------------------------------------------------------------
//
// Copyright (c) Player One Inc 2001
//
// Module name: GpsTrkSet
// Module type: Implementation source file
// Compiler   : MSVC++ 6.0
// Environment: Microsoft Windows NT
//
// Description:
//  todo
//
// Note:
//  The pure virtual Bind(..) is implemented in this class
//
// Contents:
//
// Revision history:
//
//-----------------------------------------------------------------------------

#include <string>
#include "GpsTrkSet.h"

#ifdef _NO_GPSTRK_CACHE_
#pragma message ("#\n# CGpsTrkSet - caching disabled\n#")

//-----------------------------------------------------------------------------
// CGpsTrkSet
//
CGpsTrkSet::CGpsTrkSet()
  : CDelimRecordSet()
{
  m_DTG = 0;
  m_rLat = 0.0;
  m_rLong = 0.0;
	m_Date = 0;
	m_Time = 0;
  
	m_Gs = 0; 
	m_Trk = 0; 
	m_Alt = 0; 
	
	//m_nFields = 8; //5;
}


//ID,ICAO,INFO,VAR,rLat,rLong,TYPE
void CGpsTrkSet::Bind()
{
  CDelimRecordSet::Bind();

  m_DTG   = atoi(m_sField[0]);
  m_rLat  = atof(m_sField[1]);
  m_rLong = atof(m_sField[2]);
  m_Date = atof(m_sField[3]);
  m_Time = atof(m_sField[4]);

	m_Gs  = atof(m_sField[5]);
	m_Trk = atof(m_sField[6]);
	m_Alt = atof(m_sField[7]);
}


#else //-----------------------------------------------------------------------------
--> todo
/*
#pragma message ("#\n# CGpsTrkSet - caching enabled\n#")

#include <qlist.h>
#include <qstring.h>


CGpsTrkSet::CGpsTrkSet()
{
  cp = 0;
  num = 0;
  GpsTrkSetInit();
}

void CGpsTrkSet::GpsTrkSetInit()
{
}

//-----------------------------------------------------------------------------
// Open and read the entire .csv file, storing it in vector class tbl
//
bool CGpsTrkSet::Open(const char *sFileName)
{
  //Integer nn = new Integer(0);
  //Double  dd = new Double(0);
  CDelimRecordSet rs;// = new CDelimRecordSet();

  if (!rs.Open(sFileName))
    return false;
  rs.MoveFirst();
  rs.MoveNext();
  while (rs.IsEOF() != true) {
      if (rs.m_nFields > 6) {
        WptRec rec;
        rec.m_ID    = QString(rs.m_sField[0]).toLong();
        rec.m_ICAO  = QString(rs.m_sField[1]);
        rec.m_INFO  = QString(rs.m_sField[2]);
        rec.m_rElev = QString(rs.m_sField[3]).toDouble();
        rec.m_rLat  = QString(rs.m_sField[4]).toDouble();
        rec.m_rLong = QString(rs.m_sField[5]).toDouble();
        rec.m_TYPE  = QString(rs.m_sField[6]).toLong();
        tbl[num] = rec;
        num++;
    if (num > WPT_TOTAL)
      throw 1;
    }
    rs.MoveNext();
  }
  rs.Close();
  return true;
}

//-----------------------------------------------------------------------------
//  Move to the first entry in the set
//
bool CGpsTrkSet::MoveFirst()
{
  cp = 0;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
//  Move to the next entry in the set
//
bool CGpsTrkSet::MoveNext()
{
  cp++;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
//  Move to a specified in the set
//
bool CGpsTrkSet::Move(int idx)
{
  cp = idx;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
//  Check for the end of the set
//  returns true is the end is reached and
//  false if not
//
bool CGpsTrkSet::IsEOF()
{
  int size = num;// tbl.size();
  if (cp >= size - 1)
    return true;
  else
    return false;
}

//
//  Clear out the contents of the set
//
void CGpsTrkSet::Clear()
{
  //tbl.clear();
  cp = 0;
  num = 0;
}

void CGpsTrkSet::Bind()
{
  WptRec rec;
  rec = tbl[cp]; //elementAt(cp);
  m_ID    = rec.m_ID;
  m_ICAO  = rec.m_ICAO;
  m_INFO  = rec.m_INFO;
  m_rElev = rec.m_rElev;
  m_rLat  = rec.m_rLat;
  m_rLong = rec.m_rLong;
  m_TYPE  = rec.m_TYPE;
}

void CGpsTrkSet::Store()
{
  WptRec rec;// = new WptRec();
    rec.m_ID    = m_ID;
    rec.m_ICAO  = m_ICAO;
    rec.m_INFO  = m_INFO;
    rec.m_rElev = m_rElev;
    rec.m_rLat  = m_rLat;
    rec.m_rLong = m_rLong;
    rec.m_TYPE  = m_TYPE;
  tbl[cp] = rec;
}

WptRec CGpsTrkSet::GetRecord()
{
  WptRec rec;// = new WptRec();
    rec.m_ID    = m_ID;
    rec.m_ICAO  = m_ICAO;
    rec.m_INFO  = m_INFO;
    rec.m_rElev = m_rElev;
    rec.m_rLat  = m_rLat;
    rec.m_rLong = m_rLong;
    rec.m_TYPE  = m_TYPE;
  return rec;
}
*/
--> todo
#endif
