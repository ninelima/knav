class GreatCircle
{
public:

//-----------------------------------------------------------------------------
// True course/distance calculation
//

//private:

  //static double d, crs12, crs21;

  static void crsdist(double lat1, double lon1, double lat2, double lon2);
  static double acosf(double x); 

  static double getCrs12(double lat1, double lon1, double lat2, double lon2); 
  static double getCrs21(double lat1, double lon1, double lat2, double lon2);
  static double getDistance(double lat1, double lon1, double lat2, double lon2);

  static void getIntermediatePoint(double *lat, double *lon, double lat1, double lon1, double lat2, double lon2);
  static void getRadialDmePoint(double *plat, double *plon, double lat1, double lon1, double dme, double radial);


  
  
};
