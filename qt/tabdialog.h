/****************************************************************************
** $Id: tabdialog.h,v 1.9 2006/04/25 11:58:49 player Exp $
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#ifndef TABDIALOG_H
#define TABDIALOG_H

#include <qtabdialog.h>
#include <qstring.h>
#include <qfileinfo.h>
#include <qlistbox.h>

class TabDialog : public QTabDialog
{
    Q_OBJECT

public:

    //QTabDialog (QWidget *parent=0, const char *name=0, bool modal=FALSE, WFlags f=0)
    //TabDialog( QWidget *parent, const char *name, const QString &_filename );
    TabDialog(QWidget *parent=0, const char *name=0, bool modal=FALSE, WFlags f=0);

private:
  QListBox *listSelected;
  QListBox *listAvailable;
  void staticVector();

protected:
    QString filename;
    QFileInfo fileinfo;

    void setupTab1();
    void setupTab2();
    void setupTab3();


private slots:
  // tab 2
  void but3_clicked();
  // tab 3
  void but1_clicked();
  void but2_clicked();
  void cbStyle_activated(int sel);
  // void cbBrowser_activated(int sel);
  void cbMouseWheel_activated(int sel);
  void cbLatLonStyle_activated(int sel);
  void cbAirspaceLineStyle_activated(int sel);
  void cbDrawingStyle_activated(int sel);
  
 

signals:

};

#endif
