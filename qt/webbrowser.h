/****************************************************************************
** Form interface generated from reading ui file 'webbrowser.ui'
**
** Created: Sat Jan 1 14:27:10 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.3   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#ifndef WEBBROWSERWINDOW_H
#define WEBBROWSERWINDOW_H

#include <qvariant.h>
#include <qpixmap.h>
//#include <qmainwindow.h>
#include <qdialog.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QSpacerItem;
class QAction;
class QActionGroup;
class QToolBar;
class QPopupMenu;
class QFrame;
class QAxWidget;
class QLabel;
class QLineEdit;
class QProgressBar;

class WebBrowserWindow : public QMainWindow
{
    Q_OBJECT

public:
    WebBrowserWindow( QWidget* parent = 0, const char* name = 0, WFlags fl = WType_TopLevel );
    ~WebBrowserWindow();

    QFrame* Frame3;
    QAxWidget* WebBrowser;
    QLabel* lblAddress;
    QLineEdit* addressEdit;
    QMenuBar *menubar;
    QPopupMenu *PopupMenu;
    QPopupMenu *FileNewGroup_2;
    QPopupMenu *unnamed;
    QToolBar *Toolbar;
    QToolBar *Toolbar_2;
    QAction* actionGo;
    QAction* actionBack;
    QAction* actionForward;
    QAction* actionStop;
    QAction* actionRefresh;
    QAction* actionHome;
    QAction* actionFileClose;
    QActionGroup* FileNewGroup;
    QAction* actionNewWindow;
    QAction* actionSearch;
    QAction* actionAbout;
    QAction* actionAboutQt;

public slots:
    virtual void go();
    virtual void newWindow();
    virtual void setProgress( int a, int b );
    virtual void init();
    virtual void setTitle( const QString & title );
    virtual void setCommandState( int cmd, bool on );
    virtual void navigateComplete();
    virtual void navigateBegin();
    virtual void aboutSlot();
    virtual void aboutQtSlot();

protected:
    QProgressBar *pb;

    QHBoxLayout* WebBrowserWindowLayout;
    QVBoxLayout* Frame3Layout;

protected slots:
    virtual void languageChange();

private:
    QPixmap image0;
    QPixmap image1;
    QPixmap image2;
    QPixmap image3;
    QPixmap image4;
    QPixmap image5;
    QPixmap image6;

};

#endif // WEBBROWSERWINDOW_H
