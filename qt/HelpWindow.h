/****************************************************************************
** $Id: HelpWindow.h,v 1.4 2006/04/25 11:58:49 player Exp $
**
** Copyright (C) 1992-2000 Trolltech AS.  All rights reserved.
**
** This file is part of an example program for Qt.  This example
** program may be used, distributed and modified without limitation.
**
*****************************************************************************/

#ifndef HELPWINDOW_H
#define HELPWINDOW_H                          

#include <qmainwindow.h>
#include <qtextbrowser.h>
#include <qstringlist.h>
#include <qmap.h>
#include <qdir.h>
#include <qhttp.h>

class QComboBox;
class QPopupMenu;

class HelpWindow : public QMainWindow
{
    Q_OBJECT
public:

  
  HelpWindow(const QString& sHome, const QString& sPath, QWidget *parent = 0, const char *name=0);
    ~HelpWindow();
 
  void hardcopy();
  void setSourceFile(const QString& name);
  void setSourceUrl(const QString& name);


private slots:
    void setBackwardAvailable( bool );
    void setForwardAvailable( bool );

    void sourceChanged( const QString& );
    void about();
    void aboutQt();
    void openFile();
    void newWindow();
    void print();

    void pathSelected( const QString & );
    void histChosen( int );
    void bookmChosen( int );
    void addBookmark();

    void fetchDone( bool error );


private:
    void readHistory();
    void readBookmarks();

    QComboBox *pathCombo;
    int backwardId, forwardId;
    QStringList history, bookmarks;
    QMap<int, QString> mHistory, mBookmarks;
    QPopupMenu *hist, *bookm;


public:
    QTextBrowser *textbrowser;
    QHttp articleFetcher;

};

#endif

