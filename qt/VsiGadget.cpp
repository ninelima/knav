//-----------------------------------------------------------------------------
// $Id: VsiGadget.cpp,v 1.4 2005/12/16 16:04:22 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <qrect.h>
#include <math.h>
#include "VsiGadget.h"
#include "umath.h"
#include "uvalues.h"

//#define min(a,b)            (((a) < (b)) ? (a) : (b))

//-----------------------------------------------------------------------------
// Default constructor
//
VsiGadget::VsiGadget(QWidget *parent, const char *name, WFlags f)
    //:QWidget(parent, name, f)
    :PanelGadget(parent, name, f)
{
  // capture mouse movements only if buttons are pressed
  // ie mouse dragged
  //setMouseTracking(false);
  // not!
  setMouseTracking(true);
  m_bMouseLeftButtonDown = false;
  m_bMouseRightButtonDown = false;
}


//-----------------------------------------------------------------------------
// Paint method the panel
//
void VsiGadget::drawBlankFace(GraphicPainter *gc, int x, int y, int size)
{

/*
 
{***************************************************************}
{*  Vertical Speed Indicator                                   *}
{***************************************************************}
procedure CalibTextVSI(x,y,off,phi : integer;ts : string);
var
  a,b : integer;
begin
  calibshort(x,y,off+5,phi);
  a := round(off*cos(phi*pi/180));
  b := round(off*sin(phi*pi/180));
  case phi of
   540 : begin b := b+2; a:=a-2; end;  {0}
   504 : begin b := b+2; a:=a-2; end;  {5}
   468 : begin b := b+2; a:=a-3; end;  {10}
   432 : begin b := b+2; a:=a-3; end;  {15}
   396 : begin b := b+2; a:=a-6; end;  {20}
  -180 : begin b := b+2; a:=a-2; end;  {0}
  -144 : begin b := b+5; a:=a-0; end;  {5}
  -108 : begin b := b+8; a:=a-0; end;  {10}
  -72  : begin b := b+8; a:=a-3; end;  {15}
  -36  : begin b := b+8; a:=a-6; end;  {20}
  end; {case}
  OuttextXY(x+a,y-b,ts);
end;

procedure VSI(vertspeed :integer);
var
  x,y : integer;
  a : integer;
  sp : string;
  m : real;

begin
  y := 380;
  x := R*2 +R div 2;

  hexagon(x,y,65);
  for a := 50 downto 40 do
    begin
      str((50-a)*2 div 2,sp);
      CalibShort(x,y,60,a*360 div 50+180);
      if (-(50-a) mod 5) = 0 then  CalibTextVSI(x,y,50,a*360 div 50+180,sp);
    end;

  CalibShort(x,y,60,35*360 div 50+180);
  CalibTextVSI(x,y,50,35*360 div 50+180,'15');
  CalibShort(x,y,60,30*360 div 50+180);
  CalibTextVSI(x,y,50,30*360 div 50+180,'20');

  for a := 50 downto 40 do
    begin
      str((50-a)*2 div 2,sp);
      CalibShort(x,y,60,-a*360 div 50+180);
      if (-(50-a) mod 5) = 0 then  CalibTextVSI(x,y,50,-a*360 div 50+180,sp);
    end;

  CalibShort(x,y,60,-35*360 div 50+180);
  CalibTextVSI(x,y,50,-35*360 div 50+180,'15');
  CalibShort(x,y,60,-30*360 div 50+180);
  CalibTextVSI(x,y,50,-30*360 div 50+180,'20');

end;
*/

  QString ts;
  //int a, b;
  int m;
  double scale = m_scale;

  gc->setPen(Qt::gray);
  gc->setBrush(Qt::gray);
  gc->drawRect(0, 0, 150*scale, 150*scale);
  gc->setPen(Qt::black);
  gc->setBrush(Qt::black);
  gc->drawEllipse(0, 0, 150*scale, 150*scale);
  //circle(x,y,40);
  gc->drawEllipse(55*scale, 55*scale, 80*scale, 80*scale);

  QFont font1("Arial", 8*scale); //, fontsize);
  gc->setFont(font1);
  gc->setPen(Qt::white);
  for (m = 50; m >= 40; m--) {
    //str((50-a)*2 div 2,sp);
    ts.sprintf("%d", (50-m)*2 / 2 );

    //CalibShort(x,y,60,-a*360 div 50+180);
    // Short calib lines
      int off = 60;
      int phi = -m*360 / 50+180;
      int a = uround(off*cos(phi*M_PI/180));
      int b = uround(off*sin(phi*M_PI/180));
      gc->radLine((x+a)*scale, (y-b)*scale, scale*5,  phi);
      gc->radLine((x+a)*scale, (y+b)*scale, scale*5, -phi);

    //if (-(50-a) mod 5) = 0 then
    if ( (-(50-m) % 5) == 0) {
      //CalibTextVSI(x,y,50,-a*360 div 50+180,sp);

      int off = 50;
      int phi = m*360 / 50+180;
      int a = uround(off*cos(phi*M_PI/180));
      int b = uround(off*sin(phi*M_PI/180));

      switch (phi) {
        case 540 :  b = b+2; a=a-2; break;  // 0
        case 504 :  b = b+2; a=a-2; break;  // 5
        case 468 :  b = b+2; a=a-3; break;  // 10
        case 432 :  b = b+2; a=a-3; break;  // 15
        case 396 :  b = b+2; a=a-6; break;  // 20
        case -180:  b = b+2; a=a-2; break;  // 0
        case -144:  b = b+5; a=a-0; break;  // 5
        case -108:  b = b+8; a=a-0; break;  // 10
        case -72 :  b = b+8; a=a-3; break;  // 15
        case -36 :  b = b+8; a=a-6; break;  // 20
      } //switch
      gc->drawString(ts, (x+a)*scale,  (y+b)*scale);
      gc->drawString(ts, (x+a)*scale,  (y-b)*scale);
    }
  }
}


void VsiGadget::drawFace(GraphicPainter *gc)
{

  QString ts;
  int a;
  int x = 75;
  int y = 75;

/*
const
  T : integer = 23;
var
  x,y : integer;
  a,b : integer;
var
  DC,MemDC, PDC: HDC;
  Bitmap1, Bitmap2, HBits: HBitmap;
  ThePen,OldPen : HPen;

begin
  DC := GetDC(HWindow);
  PDC   := CreateCompatibleDC(DC);
  MemDC := CreateCompatibleDC(DC);
  HBits := CreateCompatibleBitmap(DC, bmWidth, bmHeight);

  Bitmap1 := SelectObject(MemDC, HBits);
  Bitmap2 := SelectObject(PDC, BitMapHandle);

  ThePen := CreatePen(ps_Solid, 2, EgaColor[Yellow]);
  OldPen := SelectObject(MemDC,ThePen);

  { The VSI }
  BitBlt(MemDC, 0, 0, bmWidth, bmHeight, PDC, 335, 295, srcCopy);
  x := 65;
  y := 65;

  a := round(-144/2000*VertSpeed+180);
  radline(MemDC, x,y,55,a);
  T := VertSpeed;
  BitBlt(DC, 335, 295, bmWidth, bmHeight, MemDC, 0, 0, srcCopy);
*/

  gc->setPen(QPen(Qt::yellow, 3*m_scale));

  a = uround(-144*m_vertspeed/2000+180);
  gc->radLine(x*m_scale, y*m_scale, 50*m_scale, a); //55

}

void VsiGadget::setRoc(int roc)
{
	if (m_vertspeed != roc) {
	  m_vertspeed = roc;
		repaint(false);
	}
}

void VsiGadget::mousePressEvent(QMouseEvent *e)
{
}

