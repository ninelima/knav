//-----------------------------------------------------------------------------
// $Id: AsiGadget.cpp,v 1.6 2007/03/03 07:56:14 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include <qrect.h>
#include <math.h>
#include "AsiGadget.h"
#include "version.h"
#include "umath.h"
#include "uvalues.h"

//#define min(a,b)    (((a) < (b)) ? (a) : (b))

//-----------------------------------------------------------------------------
// Default constructor
//
AsiGadget::AsiGadget(QWidget *parent, const char *name, WFlags f)
    //:QWidget(parent, name, f)
    :PanelGadget(parent, name, f)
{
  // capture mouse movements only if buttons are pressed
  // ie mouse dragged
  //setMouseTracking(false);
  // not!
  setMouseTracking(true);
  m_bMouseLeftButtonDown = false;
  m_bMouseRightButtonDown = false;
}


//-----------------------------------------------------------------------------
// Paint method the panel
//
/*
void AsiGadget::paintEvent(QPaintEvent* e)
{
  QPainter p(this);
  QPainter b(&pixbuffer);
  GraphicPainter *gc = (GraphicPainter *) &p;

  rect = p.window();
  int screenWidth  = rect.width();
  int screenHeight = rect.height();
 
  m_size = min(screenWidth, screenHeight)/2;
  m_scale = (double) m_size / 75.0;

  drawFace((GraphicPainter *) &b);
  // copy the image from the buffer pixmap to the window
  bitBlt(this, 0, 0, &pixbuffer);
}
*/

void AsiGadget::drawBlankFace(GraphicPainter *gc, int x, int y, int size)
{

/*
 
procedure CalibTextASI(x,y,off,phi : integer;ts : string);
var
  a,b : integer;
begin
  a := round(off*cos(phi*pi/180));
  b := round(off*sin(phi*pi/180));
  case phi of
   450 : b := b+10;                     { 0}
   410 : b := b+10;                     {20}
   370 : b := b+5;                      {40}
   330 : b := b;                        {60}
   290 : b := b;                        {80}
   250 : a := a-5;                      {100}
   210 : a := a-10;                     {120}
   170 : begin a := a-12; b:=b+5 end;   {140}
   130 : begin a := a-10; b:=b+10 end;  {160}
  end;
  OuttextXY(x+a,y-b,ts);
end;

procedure ASI(speed :integer);
var
  x,y : integer;
  a : integer;
  sp : string;

begin
  y := 230;
  x := R*0 +R div 2;

  hexagon(x,y,65);
  arc(x,y,130,90,40);
  setcolor(green);
  setlinestyle(solidln,0,thickwidth);
  arc(x,y,140,30,43);
  {setcolor(lightgray);}
  setcolor(lightgray);
  arc(x,y,230,40,38);
  setcolor(white);
  setlinestyle(solidln,0,Normwidth);
  arc(x,y,130,90,40);
  for a := 36 downto 4  do
    begin
      str((36-a)*10 div 2,sp);
      CalibShort(x,y,41,a*360 div 36+90);
      if (-(36-a) mod 4) = 0 then CalibTextASI(x,y,50,a*360 div 36+90,sp);
    end;
end;
*/



  QString ts;
  //int a, b;
  int m;
  double scale = m_scale;

  int fontheight = gc->fontMetrics().height();
  int fontwidth = gc->fontMetrics().width('0');

  gc->setPen(Qt::gray);
  gc->setBrush(Qt::gray);
  gc->drawRect(0, 0, 150*scale, 150*scale);

  gc->setPen(Qt::black);
  gc->setBrush(Qt::black);
  gc->drawEllipse(0, 0, 150*scale, 150*scale);

  gc->setPen(Qt::white);
  gc->setBrush(Qt::white);
  gc->drawArc(35*scale, 35*scale, 80*scale, 80*scale, 90*16, -320*16);

  gc->setPen(QPen(Qt::green, 3*scale));
  gc->drawArc(32*scale, 32*scale, 86*scale, 86*scale, 30*16, -110*16);

  gc->setPen(QPen(Qt::lightGray, 3*scale));
  gc->drawArc(37*scale, 37*scale, 76*scale, 76*scale, 40*16, -190*16);

  QFont font1("Arial", 8*scale); //, fontsize);
  gc->setFont(font1);
  gc->setPen(Qt::white);
 
#ifdef BUILD_RAILVIEW
    const int range = 18*1;  //80kts
#else
#ifdef BUILD_CROSSVIEW
  const int range = 36*2;  //320kts
#else
  const int range = 36*1;  //160kts
  //const int range = (36.0*1.5);  //240kts
#endif
#endif

  for (m = range; m >= 4; m--) {
    //str((36-a)*10 div 2,sp);
    ts.sprintf("%d", (range-m)*10 / 2);

    //CalibShort(x,y,41,a*360 div 36+90);
    // Short calib lines
      //int a, b;
      int off = 40;                                 
      int phi = m*360 / range+90;
      int a = uround(off*cos(phi*M_PI/180));
      int b = uround(off*sin(phi*M_PI/180));

      gc->radLine((x+a)*scale, (y-b)*scale, scale*5, phi);
                                                                       

    if ( (-(range-m) % 4) == 0) {
      int off = 60;//50;
      int phi = m*360 / range+90;
      int a = uround(off*cos(phi*M_PI/180));
      int b = uround(off*sin(phi*M_PI/180));

        /*
                        switch (phi) {
        case 450 : b=b+10; break;               //  0
        case 410 : b=b+10; break;               // 20
        case 370 : b=b+5;  break;               // 40
        case 330 : b=b;    break;               // 60
        case 290 : b=b;    break;               // 80
        case 250 : a=a-5;  break;               //100
        case 210 : a=a-10; break;               //120
        case 170 : a=a-12; b=b+5;  break;       //140
        case 130 : a=a-10; b=b+10; break;       //160
      }
                        */

      a = a - fontwidth;
      b = b - fontheight/4;

      gc->drawString(ts, (x+a)*scale, (y-b)*scale);
      //gc->drawText((x+a)*scale, (y-b)*scale, ts -1, Qt::AlignCenter);

    }
  }
}


void AsiGadget::drawFace(GraphicPainter *gc)
{
  QString ts;
  int i;
  int a, b;
  int x = 75;
  int y = 75;

/*
procedure DispASI(speed : integer);
const
  T : integer = 0;
var
  x,y : integer;
  a : integer;
  sp : string;

begin
  if T = speed then exit;
  T := speed;
  y := 100;
  x := R*0 +R div 2;

  { err := pcxFileDisplay('ASI.PCX',x-65,y-65,0); }
  err := pcxVirtualDisplay(ptrASI,0,0,x-65,y-65,x+65,y+65,0);
  if err <> 0 then error;
  a := -2*speed+90;
  radline(x,y,35,a);
end;
*/
  gc->setPen(QPen(Qt::yellow, 3*m_scale));

#ifdef BUILD_CROSSVIEW
  a = uround(-1*m_Gs+90); // for 360
#else
  a = uround(-2*m_Gs+90);  // for 180
  //a = uround(-1.5*(double)m_Gs+90.0);  // for 270
#endif

  gc->radLine(x*m_scale, y*m_scale, 35*m_scale, a);
}

void AsiGadget::setGs(int Gs)
{
        if (m_Gs != Gs) {
                m_Gs = Gs;
                repaint(false);
        }
}

