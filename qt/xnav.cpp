#ifdef _MOTIF_

#include <stdio.h>
#include <Xm/Xm.h>
#include <Xm/MainW.h>
#include <Xm/CascadeB.h>
#include <Xm/Label.h>
#include <Xm/PanedW.h>
#include <Xm/Notebook.h>
#include <Xm/RowColumn.h>
#include <Xm/PushB.h>
#include <Xm/Form.h>
#include <Xm/Text.h>
#include <Xm/PushBG.h>

#include "drawing.h"
#include "outline.h"
#include "detail.h"
#include "menu.h"
#include "toolbar.h"
#include "MapWnd.h"

extern CMapWnd MapWnd;

XtAppContext   app;

/*------------------------------------------------------------------------------
 *  Create and initialise the main work pane consisting of one horizontal
 *  and one vertical "splitter"
 *

   +---+-----------+
   |   |           |
   |   |           |
   |   |           |
   |   +-----------+
   |   |           |
   |   |           |
   +---+-----------+
 
 */

Widget CreatePaneGroup(Widget parent)
{
  Widget     paneH, child, toolbar;
  XmString   xms;
  Arg        args[6];
  int        n = 0;

  /*
   *  Create a form so that we can lay out items as we want to
   */
  Widget form = XtVaCreateWidget("Form", xmFormWidgetClass, parent, NULL);
  //XtManageChild(form);

  /*
   *  Create and initialise the toolbar at the top of the form
   */
  toolbar = create_toolbar(form);
  XtVaSetValues(toolbar,
                XmNtopAttachment, XmATTACH_FORM,
                XmNleftAttachment, XmATTACH_FORM,
                NULL);
  XtManageChild(toolbar);

  /*
   *  Create and initialise the horizontal pane widget
   */
  XtSetArg(args[n], XmNorientation, XmHORIZONTAL);
  n++;
  paneH = (Widget) XmCreatePanedWindow(form, "pane", args, n);

  n = 0;
  XtSetArg(args[n], XmNpaneMinimum, 60);
  n++;
  child = create_outline_container(paneH);
  XtManageChild(child);

  XtManageChild(paneH);

  XtVaSetValues(paneH,
                XmNtopAttachment, XmATTACH_WIDGET,
                // attach left to same x coordinate as left of "one"
                XmNtopWidget, toolbar,
                XmNleftAttachment, XmATTACH_OPPOSITE_WIDGET,
                XmNleftWidget, toolbar,
                XmNbottomAttachment, XmATTACH_FORM,
                XmNrightAttachment, XmATTACH_FORM,
                NULL);


  /*-------------------------------------------------------------------------*/

  /*
   *  Create and initialise the vertical pane widget
   */
  Widget   paneV, notebook, page, tab;

  n = 0;
  XtSetArg(args[n], XmNorientation, XmVERTICAL); n++;
  paneV = XmCreatePanedWindow(paneH, "pane", args, n);

  child = create_draw_form(paneV);
  XtManageChild(child);

  /* Create the Notebook */
  n = 0;
  XtSetArg(args[n], XmNpaneMinimum, 60); n++; //does this do anything?
  notebook = XmCreateNotebook (paneV, "notebook", args/*NULL*/, n);

  /* Create the "pages" */
  for (int i = 0; i < 3; i++ ){
    char         buffer[32];
    Arg          args[4];

    page = XmCreateRowColumn (notebook, "page", NULL, 0);
    /* add stuff to the page */
    n = 0;
    XtSetArg(args[n], XmNpaneMinimum, 60); n++; //60
    //child = XmCreateLabel(page, "A label on the page", args, n);
    child = create_detail_container(notebook);
    XtManageChild(child);


    /* add tabs for the "pages" */
    n = 0;
    //XtSetArg(args[n], XmNnotebookChildType, XmMAJOR_TAB); n++;
    //sprintf(buffer, "%s %d", "Major", i);
    XtSetArg(args[n], XmNnotebookChildType, XmMINOR_TAB); n++;
    sprintf(buffer, "%s %d", "Minor", i);
    tab = XmCreatePushButton (notebook, buffer, args, n);

    XtManageChild(tab);
    XtManageChild(page);
  }
  XtManageChild(notebook);

  /* a little work around to remove the "page scroller" that
   * was automatically added when the notebook widget was
   * created
   */
  //extern Widget notebook;
  Widget scroller;
  scroller = XtNameToWidget (notebook, "PageScroller");
  XtUnmanageChild (scroller);

  XtManageChild(paneV);
  //return paneV;
  return form;
}



/*------------------------------------------------------------------------------
 *  Program entry point
 */
int main(int argc, char **argv)
{
    Widget form;/*A, paneB;*/
    Widget top_wid, main_w, help;
    Widget  menubar, menu, toolbar;
 
    int n = 0;
    Arg args[12];

    XtSetLanguageProc(NULL, NULL, NULL);
 
    /* Initialize toolkit */
    top_wid = XtVaAppInitialize(&app, "Demos", NULL, 0, &argc, argv, NULL, NULL);
 
    XtVaSetValues (top_wid, XmNtitle, "XNav - <none>", NULL);

    /* main window will contain a MenuBar and a Label  */
    main_w = XtVaCreateManagedWidget("main_window",
                                      xmMainWindowWidgetClass,   top_wid,
                                      XmNwidth, 300,
                                      XmNheight, 300,
                                      NULL);

    /* Initialize the menu and return the menu bar */
    menubar = initmenu(main_w);
    XtManageChild(menubar);
 
    form = CreatePaneGroup(main_w);
    XtManageChild(form);

 
    XtRealizeWidget(top_wid);
    XtAppMainLoop(app);
    return 0;
}

#endif //_MOTIF_
