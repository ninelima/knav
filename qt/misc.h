#ifndef __MISC_H
#define __MISC_H

#ifndef LOWORD
  #define LOWORD(l)  ((unsigned short)(l))
#endif

#ifndef HIWORD
  #define HIWORD(l)  ((unsigned short)(((unsigned long)(l) >> 16) & 0xFFFF))
#endif

#ifndef LOBYTE
  #define LOBYTE(w)  ((unsigned char)(w))
#endif

#ifndef HIWORD
  #define LOBYTE(w)  ((unsigned char)(((unsigned short)(w) >> 8) & 0xFF))
#endif

#define STATUS_NONE                      0
#define STATUS_BATTERY_DISCONNECT        1
#define STATUS_CIRCUIT_CLOSE             2
#define STATUS_CIRCUIT_OPEN              3
#define STATUS_CONTINUOUS_PANIC          4
#define STATUS_DENIED_DRIVER             5
#define STATUS_DRIVER_SERVICE_MODE       6
#define STATUS_ENTER_AREA                7
#define STATUS_EXCESS_FUEL_USAGE         8
#define STATUS_EXCESS_IDLING             9
#define STATUS_EXIT_AREA                10
#define STATUS_GPS_UNLOCK               11
#define STATUS_GPS_UNLOCK_ALARM         12
#define STATUS_HARSH_BRAKING            13
#define STATUS_IDLING                   14
#define STATUS_IGNITION_OFF             15
#define STATUS_IGNITION_ON              16
#define STATUS_IMMOBILISED              17
#define STATUS_IN_NO_GO_AREA            18
#define STATUS_MOVING                   19
#define STATUS_OUT_OF_COVERAGE          20
#define STATUS_OUTSIDE_PREFERRED_AREA   21
#define STATUS_OVERHEATING              22
#define STATUS_PANIC                    23
#define STATUS_SERVICE_DUE              24
#define STATUS_SPEEDING                 25
#define STATUS_STOPPED                  26
                                         
#define STATUS_ARRIVE   101              
#define STATUS_DEPART   102               
#define STATUS_ENROUTE  103
#define STATUS_OFFTRCK  104
#define STATUS_PROHIBIT 105
#define STATUS_MESSAGE  110


int naips_path(char *szNaipsPath);
int naips_execute();

void html_browse(const char *sztarget);
void html_print(const char *sztarget);
void html_edit(const char *sztarget);

int execG3cmd(const char *sFmtCmd);

void start_console();


//char *gen_code(char *szcode);
bool check_code(const char *szcode, const char *name);

void set_demo_version();
bool check_demo_version();

bool check_disclaimer();
bool check_serial_num();

void convLL(double *fc, char *sc);

int calc_hdg(double lat1, double lon1, double lat2, double lon2);
int avg_hdg(double lat, double lon, long sec);

double calc_speed(double lat1, double lon1, double lat2, double lon2, long delta_sec);
double avg_speed(double lat, double lon, long sec);

double avg_bank(int hdg, long sec);
double avg_rot(int hdg, long sec);
double avg_roc(int alt, long sec);

bool isInsideRect(double top, double left, double bottom, double right, double lat, double lon);


#endif //__MISC_H
