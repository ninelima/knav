#ifndef __DELIMRECORDSET_H
#define __DELIMRECORDSET_H

// DelimRecordSet.h : header file
//
#ifndef _INC_STDIO
#include "stdio.h"
#endif

#define BUF_SIZE 2048
#define MAX_FIELD_SIZE 512
#define MAX_FIELD_NUM 64

//-----------------------------------------------------------------------------
// CDelimRecordSet
//
class CDelimRecordSet
{
  // Construction
  public:
    CDelimRecordSet();
    ~CDelimRecordSet();
 
  // Attributes
  public:
 
  // Operations
  public:
 
 
  // Implementation
  public:
    bool IsEOF();
    char m_szToken[256];
    unsigned int m_nFields;
    unsigned int m_nCurRec;
    char m_sField[MAX_FIELD_NUM][MAX_FIELD_SIZE];
 
    bool Open(const char *szFileName);
    bool Close();

    bool MoveFirst();
    bool MoveNext();
    bool MoveLast();
    bool Move(unsigned int nRecNum);
   
    void Sort(int nField);
   
  private:
    bool m_EOF;
    char m_szBuffer[BUF_SIZE];
    FILE *m_DataFile;
   
  protected:
    virtual void Bind();
 
};


#endif // __DELIMRECORDSET_H
