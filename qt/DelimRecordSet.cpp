#pragma warning(disable : 4996)										// Disable warning messages 4996

//-----------------------------------------------------------------------------
// $Id: DelimRecordSet.cpp,v 1.16 2005/10/30 10:00:25 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include "stdio.h"
#include "string.h"
#include "DelimRecordSet.h"

//-----------------------------------------------------------------------------
// Constructor
//
CDelimRecordSet::CDelimRecordSet()
{
  m_DataFile = NULL;
  m_nCurRec    = 0;
  m_nFields    = 0;
  strcpy(m_szToken, ",");
}

CDelimRecordSet::~CDelimRecordSet()
{
  printf("CDelimRecordSet::~CDelimRecordSet\n");

  if (m_DataFile) {
    Close();
  }
}

//-----------------------------------------------------------------------------
// CDelimRecordSet message handlers
//
bool CDelimRecordSet::Open(const char *szFileName)
{
	memset(m_sField, 0x0, sizeof(m_sField));
  m_nCurRec    = 0;
  m_nFields    = 0;

  m_DataFile = fopen(szFileName, "rt");
  if (!m_DataFile) {
    printf("Invalid File - %s\n", szFileName);
    return false;
  }

  return true;
}

//-----------------------------------------------------------------------------
// Close the source file
//
bool CDelimRecordSet::Close()
{
  if (m_DataFile) {
    fclose(m_DataFile);
    m_DataFile = NULL;
  }

  return true;
}


//-----------------------------------------------------------------------------
// Move to the first record
//
bool CDelimRecordSet::MoveFirst()
{
  char *sp;

  if (m_DataFile) {
    rewind(m_DataFile);
    sp = fgets(m_szBuffer, BUF_SIZE, m_DataFile);
    m_nCurRec = 0;
    Bind();
    m_EOF = false;
    return true;
  }
  else {
    m_EOF = true;
    return false;
  }
}

//-----------------------------------------------------------------------------
// Move to the next record
//
bool CDelimRecordSet::MoveNext()
{
  char *sp;

  if (m_DataFile) {
    sp = fgets(m_szBuffer, BUF_SIZE, m_DataFile);
    m_nCurRec++;

    if (sp) {
      Bind();
      m_EOF = false;
      return true;
    }
    else {
      m_EOF = true;
      return false;
    }
  }
  else {
    m_EOF = true;
    return false;
  }

}
//-----------------------------------------------------------------------------
// Move to the last record
//
bool CDelimRecordSet::MoveLast()
{
  if (MoveFirst()) {
		while (MoveNext());
  }
	else 
		return false;

  return true;
}

//-----------------------------------------------------------------------------
// Move to a specific record
//
bool CDelimRecordSet::Move(unsigned int nRecNum)
{
  bool rv;
  char *sp;

  // Sanity check
  if (nRecNum < 0) {
    printf("Invalid RecNum - %d\n", nRecNum);
    return false;
  }

  // If the record we are seeking is already
  // behind us, start from the beginning
  if (nRecNum < m_nCurRec)
    rv = MoveFirst();

  while (m_nCurRec + 1 < nRecNum)  {
    sp = fgets(m_szBuffer, BUF_SIZE, m_DataFile);
    m_nCurRec++;
  }
  MoveNext();

  return true;
}


//-----------------------------------------------------------------------------
// Prepares for adding a new record. Call Update to complete the addition.
//
void AddNew()
{
  // todo
}

//-----------------------------------------------------------------------------
// Prepares for changes to the current record. Call Update to complete the edit.
//
void Edit()
{
  // todo
}

//-----------------------------------------------------------------------------
// Completes an AddNew or Edit operation by saving the new or edited data on
// the data source.
//
bool Update()
{
  // todo
  return false;
}

//-----------------------------------------------------------------------------
// Bind to the generic fields
//
void CDelimRecordSet::Bind()
{
  char *sp;

  // it is a comment line
  if (m_szBuffer[0] == '#')
    return;


  m_nFields = 0;
  sp = strtok(m_szBuffer, m_szToken);
  while (sp) {
    // While there are tokens in m_szBuffer
    strcpy(m_sField[m_nFields], sp);
    m_nFields++;

    // Get next token:
    sp = strtok(NULL, m_szToken);
  }
}

/*
line = stream.readLine();
QStringList list = QStringList::split(",", line, true);
*/



//-----------------------------------------------------------------------------
//
//
void CDelimRecordSet::Sort(int nField)
{
  /* Not completed ---
  FILE *fpSorted;
  char szCurField[MAX_FIELD_SIZE];

  fpSorted = fopen("~.~", "wt");
  */
}

//-----------------------------------------------------------------------------
//
//
bool CDelimRecordSet::IsEOF()
{
  return m_EOF;
}


