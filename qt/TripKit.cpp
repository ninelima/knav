//-----------------------------------------------------------------------------
// $Id: TripKit.cpp,v 1.4 2006/04/23 11:04:26 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the 
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786

#include <qcheckbox.h>
#include <qpushbutton.h>
#include <qframe.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

#include "TripKit.h"
#include "TripKit.moc"

#include "iniFile.h"

//-----------------------------------------------------------------------------
//  Construct a TripKit which is a child of 'parent', with the
//  name 'name' and widget flags set to 'f'
//
//  The dialog will by default be modeless, unless you set 'modal' to
//  TRUE to construct a modal dialog.
//
TripKit::TripKit(QWidget *parent,  const char *name, bool modal, WFlags fl)
    : uiTripKitDlg(parent, name, modal, fl)
{
  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
	cbTripKit->setChecked(iniFile.GetValueI("TripKit", "bTripKit", 0));
  cbRouteFull->setChecked(iniFile.GetValueI("TripKit", "bRouteFull", 0));
  cbRouteLegs->setChecked(iniFile.GetValueI("TripKit", "bRouteLegs", 0));
  cbFlightLog->setChecked(iniFile.GetValueI("TripKit", "bFlightLog", 0));
  cbPlanningLog->setChecked(iniFile.GetValueI("TripKit", "bPlanningLog", 0));
  cbMassBalance->setChecked(iniFile.GetValueI("TripKit", "bMassBalance", 0));
  cbPrintBackground->setChecked(iniFile.GetValueI("TripKit", "bPrintBackground", 0));
  
  // call the slot for toggle just so we get things in sync
  // as it were.
  tripkit_toggle();
}

//-----------------------------------------------------------------------------
// Destroy the object and frees any allocated resources
//
TripKit::~TripKit()
{
  CIniFile iniFile( "knav.ini"); iniFile.ReadFile();
  iniFile.SetValueI("TripKit", "bTripKit",   cbTripKit->isChecked());
  iniFile.SetValueI("TripKit", "bRouteFull",   cbRouteFull->isChecked());
  iniFile.SetValueI("TripKit", "bRouteLegs",   cbRouteLegs->isChecked());
  iniFile.SetValueI("TripKit", "bFlightLog",   cbFlightLog->isChecked());
  iniFile.SetValueI("TripKit", "bPlanningLog", cbPlanningLog->isChecked());
  iniFile.SetValueI("TripKit", "bMassBalance", cbMassBalance->isChecked());
  iniFile.SetValueI("TripKit", "bPrintBackground", cbPrintBackground->isChecked());
  iniFile.WriteFile();

  // no need to delete child widgets, Qt does it all for us
}



void TripKit::tripkit_toggle()
{
  if (cbTripKit->isChecked()) {
    cbRouteFull->setEnabled(true);
    cbRouteLegs->setEnabled(true);
    cbFlightLog->setEnabled(true);
    cbPlanningLog->setEnabled(true);
    cbMassBalance->setEnabled(true);
    //cbReserved->setEnabled(true);
  }
  else {
    cbRouteFull->setEnabled(false);
    cbRouteLegs->setEnabled(false);
    cbFlightLog->setEnabled(false);
    cbPlanningLog->setEnabled(false);
    cbMassBalance->setEnabled(false);
    //cbReserved->setEnabled(false);
  }
}