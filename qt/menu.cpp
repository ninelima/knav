/**
 * Motif specific
 */

#ifndef _MOTIF_

#include "menu.h"
#include <qpopupmenu.h>
#include <qkeycode.h>
#include <qapplication.h>
#include <qmessagebox.h>
#include <qpixmap.h>
#include <qpainter.h>

/* XPM */
static const char * p1_xpm[] = {
"16 16 3 1",
"   c None",
".  c #000000000000",
"X  c #FFFFFFFF0000",
"                ",
"                ",
"         ....   ",
"        .XXXX.  ",
" .............. ",
" .XXXXXXXXXXXX. ",
" .XXXXXXXXXXXX. ",
" .XXXXXXXXXXXX. ",
" .XXXXXXXXXXXX. ",
" .XXXXXXXXXXXX. ",
" .XXXXXXXXXXXX. ",
" .XXXXXXXXXXXX. ",
" .XXXXXXXXXXXX. ",
" .XXXXXXXXXXXX. ",
" .............. ",
"                "};

/* XPM */
static const char * p2_xpm[] = {
"16 16 3 1",
"   c None",
".  c #000000000000",
"X  c #FFFFFFFFFFFF",
"                ",
"   ......       ",
"   .XXX.X.      ",
"   .XXX.XX.     ",
"   .XXX.XXX.    ",
"   .XXX.....    ",
"   .XXXXXXX.    ",
"   .XXXXXXX.    ",
"   .XXXXXXX.    ",
"   .XXXXXXX.    ",
"   .XXXXXXX.    ",
"   .XXXXXXX.    ",
"   .XXXXXXX.    ",
"   .........    ",
"                ",
"                "};

/* XPM */
static const char * p3_xpm[] = {
"16 16 3 1",
"   c None",
".  c #000000000000",
"X  c #FFFFFFFFFFFF",
"                ",
"                ",
"   .........    ",
"  ...........   ",
"  ........ ..   ",
"  ...........   ",
"  ...........   ",
"  ...........   ",
"  ...........   ",
"  ...XXXXX...   ",
"  ...XXXXX...   ",
"  ...XXXXX...   ",
"  ...XXXXX...   ",
"   .........    ",
"                ",
"                "};


/*
  Auxiliary class to provide fancy menu items with different
  fonts. Used for the "bold" and "underline" menu items in the options
  menu.
 */
class MyMenuItem : public QCustomMenuItem
{
public:
    MyMenuItem( const QString& s, const QFont& f )
    : string( s ), font( f ){};
    ~MyMenuItem(){}

    void paint( QPainter* p, const QColorGroup& /*cg*/, bool /*act*/, bool /*enabled*/, int x, int y, int w, int h )
    {
      p->setFont ( font );
      p->drawText( x, y, w, h, AlignLeft | AlignVCenter | ShowPrefix | DontClip, string );
    }

    QSize sizeHint()
    {
      return QFontMetrics( font ).size( AlignLeft | AlignVCenter | ShowPrefix | DontClip,  string );
    }
private:
    QString string;
    QFont font;
};


MenuExample::MenuExample( QWidget *parent, const char *name )
    : QWidget( parent, name )
{
    QPixmap p1( p1_xpm );
    QPixmap p2( p2_xpm );
    QPixmap p3( p3_xpm );

    QPopupMenu *print = new QPopupMenu( this );
    CHECK_PTR( print );
    print->insertTearOffHandle();
    print->insertItem( "&Print to printer", this, SLOT(printer()) );
    print->insertItem( "Print to &file", this, SLOT(file()) );
    print->insertItem( "Print to fa&x", this, SLOT(fax()) );
    print->insertSeparator();
    print->insertItem( "Printer &Setup", this, SLOT(printerSetup()) );

    QPopupMenu *file = new QPopupMenu( this );
    CHECK_PTR( file );
    file->insertItem( p1, "&Open",  this, SLOT(open()), CTRL+Key_O );
    file->insertItem( p2, "&New", this, SLOT(news()), CTRL+Key_N );
    file->insertItem( p3, "&Save", this, SLOT(save()), CTRL+Key_S );
    file->insertItem( "&Close", this, SLOT(closeDoc()), CTRL+Key_W );
    file->insertSeparator();
    file->insertItem( "&Print", print, CTRL+Key_P );
    file->insertSeparator();
    file->insertItem( "E&xit",  qApp, SLOT(quit()), CTRL+Key_Q );

    QPopupMenu *edit = new QPopupMenu( this );
    CHECK_PTR( edit );
    int undoID = edit->insertItem( "&Undo", this, SLOT(undo()) );
    int redoID = edit->insertItem( "&Redo", this, SLOT(redo()) );
    edit->setItemEnabled( undoID, FALSE );
    edit->setItemEnabled( redoID, FALSE );

    QPopupMenu* options = new QPopupMenu( this );
    CHECK_PTR( options );
    options->insertTearOffHandle();
    options->setCaption("Options");
    options->insertItem( "&Normal Font", this, SLOT(normal()) );
    options->insertSeparator();

    options->polish(); // adjust system settings
    QFont f = options->font();
    f.setBold( TRUE );
    boldID = options->insertItem( new MyMenuItem( "&Bold", f ) );
    options->setAccel( CTRL+Key_B, boldID );
    options->connectItem( boldID, this, SLOT(bold()) );
    f = font();
    f.setUnderline( TRUE );
    underlineID = options->insertItem( new MyMenuItem( "&Underline", f ) );
    options->setAccel( CTRL+Key_U, underlineID );
    options->connectItem( underlineID, this, SLOT(underline()) );

    isBold = FALSE;
    isUnderline = FALSE;
    options->setCheckable( TRUE );


    QPopupMenu *help = new QPopupMenu( this );
    CHECK_PTR( help );
    help->insertItem( "&About", this, SLOT(about()), CTRL+Key_H );
    help->insertItem( "About &Qt", this, SLOT(aboutQt()) );

    menu = new QMenuBar( this );
    CHECK_PTR( menu );
    menu->insertItem( "&File", file );
    menu->insertItem( "&Edit", edit );
    menu->insertItem( "&Options", options );
    menu->insertSeparator();
    menu->insertItem( "&Help", help );
    menu->setSeparator( QMenuBar::InWindowsStyle );

    
        label = new QLabel( this );
    CHECK_PTR( label );
    label->setGeometry( 20, rect().center().y()-20, width()-40, 40 );
    label->setFrameStyle( QFrame::Box | QFrame::Raised );
    label->setLineWidth( 1 );
    label->setAlignment( AlignCenter );

    connect( this,  SIGNAL(explain(const QString&)), label, SLOT(setText(const QString&)) );

    setMinimumSize( 100, 80 );
}


void MenuExample::open()
{
    emit explain( "File/Open selected" );
}


void MenuExample::news()
{
    emit explain( "File/New selected" );
}

void MenuExample::save()
{
    emit explain( "File/Save selected" );
}


void MenuExample::closeDoc()
{
    emit explain( "File/Close selected" );
}


void MenuExample::undo()
{
    emit explain( "Edit/Undo selected" );
}


void MenuExample::redo()
{
    emit explain( "Edit/Redo selected" );
}


void MenuExample::normal()
{
    isBold = FALSE;
    isUnderline = FALSE;
    menu->setItemChecked( boldID, isBold );
    menu->setItemChecked( underlineID, isUnderline );
    emit explain( "Options/Normal selected" );
}


void MenuExample::bold()
{
    isBold = !isBold;
    menu->setItemChecked( boldID, isBold );
    emit explain( "Options/Bold selected" );
}


void MenuExample::underline()
{
    isUnderline = !isUnderline;
    menu->setItemChecked( underlineID, isUnderline );
    emit explain( "Options/Underline selected" );
}


void MenuExample::about()
{
    QMessageBox::about( this, "Qt Menu Example",
            "This example demonstrates simple use of Qt menus.\n"
            "You can cut and paste lines from it to your own\n"
            "programs." );
}


void MenuExample::aboutQt()
{
    QMessageBox::aboutQt( this, "Qt Menu Example" );
}


void MenuExample::printer()
{
    emit explain( "File/Printer/Print selected" );
}

void MenuExample::file()
{
    emit explain( "File/Printer/Print To File selected" );
}

void MenuExample::fax()
{
    emit explain( "File/Printer/Print To Fax selected" );
}

void MenuExample::printerSetup()
{
    emit explain( "File/Printer/Printer Setup selected" );
}


void MenuExample::resizeEvent( QResizeEvent * )
{
    label->setGeometry( 20, rect().center().y()-20, width()-40, 40 );
}


int __main( int argc, char ** argv )
{
    QApplication a( argc, argv );
    MenuExample m;

    a.setMainWidget( &m );
    m.setCaption("Qt Examples - Menus");
    m.show();
    return a.exec();
}



//-----------------------------------------------------------------------------
#else   //#ifndef _MOTIF_
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>

#include <Xm/Xm.h>
#include <Xm/MainW.h>
#include <Xm/CascadeB.h>
#include <Xm/Label.h>

#include "menu.h"
#include "GenMapWnd.h"

extern CGenMapWnd MapWnd;


/* prototype callback functions */
void file_call(Widget widget, XtPointer client_data, XtPointer call_data);
void quit_call(Widget widget, XtPointer client_data, XtPointer call_data);
void menu_call(Widget widget, XtPointer client_data, XtPointer call_data);
void help_call(Widget widget, XtPointer client_data, XtPointer call_data);

/* global variables */ 
Widget label;
String food[] = { "Chicken",  "Beef", "Pork", "Lamb", "Cheese"};


/*------------------------------------------------------------------------------
 *  Create and initialise a menu bar associated with the supplied window 
 *  widget 
 */
Widget initmenu(Widget main_w)
{
    Widget top_wid, help;
    Widget  menubar, menu, widget;
    XtAppContext app;
    XColor back, fore, spare;
    XmString  quit_str, file_str, view_str,  zoom_str, tool_str, chicken, beef, pork,
              lamb, cheese, label_str, new_str, open_str, save_str;

    int n = 0;
    Arg args[2];

    /* Create a simple MenuBar that contains three menus */
    file_str = XmStringCreateLocalized("File");
    view_str = XmStringCreateLocalized("View");
    zoom_str = XmStringCreateLocalized("Zoom");
    tool_str = XmStringCreateLocalized("Tools");
    menubar = XmVaCreateSimpleMenuBar(main_w, "menubar",
                                      XmVaCASCADEBUTTON, file_str, 'F',
                                      XmVaCASCADEBUTTON, view_str, 'V',
                                      XmVaCASCADEBUTTON, zoom_str, 'Z',
                                      XmVaCASCADEBUTTON, tool_str, 'Z',
                                      NULL);
    /* finished with this so free */
    XmStringFree(tool_str);
    XmStringFree(zoom_str);
    XmStringFree(view_str);
    XmStringFree(file_str);
 
    /* First menu is the file menu -- callback is file_call() */
    new_str = XmStringCreateLocalized("New");
    open_str = XmStringCreateLocalized("Open");
    save_str = XmStringCreateLocalized("Save");
    quit_str = XmStringCreateLocalized("Quit");

    XmVaCreateSimplePulldownMenu(menubar, "file_menu", 0, file_call,
                                 XmVaPUSHBUTTON, new_str, 'N', NULL, NULL,
                                 XmVaPUSHBUTTON, open_str, 'O', NULL, NULL,
                                 XmVaPUSHBUTTON, save_str, 'S', NULL, NULL,
                                 XmVaPUSHBUTTON, quit_str, 'F', NULL, NULL,
                                 NULL);

                                 
    XmStringFree(new_str);
    XmStringFree(open_str);
    XmStringFree(save_str);
    XmStringFree(quit_str);

#if 0
    /* First menu is the quit menu -- callback is quit_call() */
    XmVaCreateSimplePulldownMenu(menubar, "quit_menu", 0, quit_call,
                                 XmVaPUSHBUTTON, quit, 'Q', NULL, NULL,
                                 NULL);
    XmStringFree(quit);
#endif

    /* Second menu is the food menu -- callback is menu_call() */
    chicken = XmStringCreateLocalized(food[0]);
    beef = XmStringCreateLocalized(food[1]);
    pork = XmStringCreateLocalized(food[2]);
    lamb = XmStringCreateLocalized(food[3]);
    cheese = XmStringCreateLocalized(food[4]);
 
    menu = XmVaCreateSimplePulldownMenu(menubar, "edit_menu", 1, menu_call,
                                        XmVaRADIOBUTTON, chicken, 'C', NULL, NULL,
                                        XmVaRADIOBUTTON, beef, 'B', NULL, NULL,
                                        XmVaRADIOBUTTON, pork, 'P', NULL, NULL,
                                        XmVaRADIOBUTTON, lamb, 'L', NULL, NULL,
                                        XmVaRADIOBUTTON, cheese, 'h', NULL, NULL,
                                        /* RowColumn resources to enforce */
                                        XmNradioBehavior, True,
                                        /* select radio behavior in Menu */
                                        XmNradioAlwaysOne, True,
                                        NULL);

    XmStringFree(chicken);
    XmStringFree(beef);
    XmStringFree(pork);
    XmStringFree(lamb);
    XmStringFree(cheese);

    /* Initialize menu so that "chicken" is selected. */
    if (widget = XtNameToWidget(menu, "button_1")) {
      XtSetArg(args[n],XmNset, True);
      n++;
      XtSetValues(widget, args, n);
    }
    n=0; /* reset n */
 
    /* get help widget ID to add callback */
    help = XtVaCreateManagedWidget("Help",
                                   xmCascadeButtonWidgetClass, menubar,
                                   XmNmnemonic, 'H',
                                   NULL);
    XtAddCallback(help, XmNactivateCallback, help_call, NULL);

    /* Tell the menubar which button is the help menu  */
    XtSetArg(args[n], XmNmenuHelpWidget, help);
    n++;
    XtSetValues(menubar, args, n);
    n=0; /* reset n */

    return menubar;
}

/*------------------------------------------------------------------------------
 *  Menu item callback functions
 */

/* Any item the user selects from the File menu calls this function.
   It will "Quit" (item_no == 0).  */
 
//void file_call(Widget w, int item_no)
void file_call(Widget widget, XtPointer client_data, XtPointer call_data)
  /* w = menu item that was selected
      item_no = the index into the menu */
{
  int item_no = (int) client_data;
 
  printf("File Item# %d\n", item_no);
  if (item_no == 0) {

        //CRect r;
        //GetClientRect(&r);
        //pMapWnd->OnSize(r.Width(), r.Height());
        MapWnd.ZoomToWorld();
        //Invalidate();
  
    }

  if (item_no == 1) {
  }

  if (item_no == 4)
    /* the "quit" item */
    exit(0);
}
 
/* Any item the user selects from the File menu calls this function.
   It will "Quit" (item_no == 0).  */
 
//void quit_call(Widget w, int item_no)
void quit_call(Widget widget, XtPointer client_data, XtPointer call_data)
  /* w = menu item that was selected
      item_no = the index into the menu */
{
  int item_no = (int) client_data;
  
  if (item_no == 0)
    /* the "quit" item */
    exit(0);
}
 
/* Called from any of the food "Menu" items.
   Change the XmNlabelString of the label widget.
   Note: we have to use dynamic setting with setargs(). */

//void  menu_call(Widget w, int item_no)
void  menu_call(Widget widget, XtPointer client_data, XtPointer call_data)
{
  int n =0;
  Arg args[1];
  int item_no = (int) client_data;
 
 
  XmString   label_str;
 
  label_str = XmStringCreateLocalized(food[item_no]);
 
  XtSetArg(args[n],XmNlabelString, label_str);
  ++n;
  XtSetValues(label, args, n);
}

//void help_call()
void help_call(Widget widget, XtPointer client_data, XtPointer call_data)
{
  int item_no = (int) client_data;
  
  printf("Sorry, I'm Not Much Help\n");
}


#endif //#ifndef _MOTIF_

