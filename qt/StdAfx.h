// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__46A0F2A7_27EA_11D2_8DC5_204C4F4F5020__INCLUDED_)
#define AFX_STDAFX_H__46A0F2A7_27EA_11D2_8DC5_204C4F4F5020__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN    // Exclude rarely-used stuff from Windows headers

//#include <afxwin.h>         // MFC core and standard components
//#include <afxext.h>         // MFC extensions
//#include <afxcview.h>
#include <afxdisp.h>        // MFC Automation classes
//#include <afxdtctl.h>   // MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
//#include <afxcmn.h>     // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
//#include <afxhtml.h>      // MFC HTML view support


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#define WM_NEWREPORT (WM_USER + 1)

#endif // !defined(AFX_STDAFX_H__46A0F2A7_27EA_11D2_8DC5_204C4F4F5020__INCLUDED_)
