//-----------------------------------------------------------------------------
//
// Copyright (c) Player One 2001
//
// Module name:
// Module type: Implementation source file
// Compiler   : MSVC++ 6.0
// Environment: Microsoft Windows NT
//
// Description:
//  todo
//
// Note:
//  The pure virtual Bind(..) is implemented in this class
//
// Contents:
//
// Revision history:
//
//-----------------------------------------------------------------------------

#include <string>
#include "AirspaceSet.h"

#ifdef _NO_AIRSPACE_CACHE_
#pragma message ("#\n# CAirspaceSet - caching disabled\n#")

//-----------------------------------------------------------------------------
// CAirspaceSet
//
CAirspaceSet::CAirspaceSet()
  : CDelimRecordSet()
{
  m_rLat1 = 0;
  m_rLon1 = 0;
  m_rLat2 = 0;
  m_rLon2 = 0;
  m_radius = 0;
  m_a1     = 0;
  m_a2     = 0;
  m_alt1 = 0;
  m_alt2 = 0;
  m_INFO = "";

  m_nFields = 10;
}

void CAirspaceSet::Bind()
{
  CDelimRecordSet::Bind();

  m_rLat1  = atof(m_sField[0]);
  m_rLon1  = atof(m_sField[1]);
  m_rLat2  = atof(m_sField[2]);
  m_rLon2  = atof(m_sField[3]);
  m_radius = atoi(m_sField[4]);
  m_a1     = atoi(m_sField[5]);
  m_a2     = atoi(m_sField[6]);
  m_alt1   = atoi(m_sField[7]);
  m_alt2   = atoi(m_sField[8]);
  m_INFO   = m_sField[9]; 
}


#else //-----------------------------------------------------------------------------
#pragma message ("#\n# CAirspaceSet - caching enabled\n#")
todo

#include <qlist.h>
#include <qstring.h>


CAirspaceSet::CAirspaceSet()
{
  cp = 0;
  num = 0;
  AirspaceSetInit();
}

void CAirspaceSet::AirspaceSetInit()
{
}

//-----------------------------------------------------------------------------
// Open and read the entire .csv file, storing it in vector class tbl
//
bool CAirspaceSet::Open(const char *sFileName)
{
  //Integer nn = new Integer(0);
  //Double  dd = new Double(0);
  CDelimRecordSet rs;// = new CDelimRecordSet();

  if (!rs.Open(sFileName))
    return false;
  rs.MoveFirst();
  rs.MoveNext();
  while (rs.IsEOF() != true) {
      if (rs.m_nFields > 6) {
        AirRec rec;

        rec.m_rLat1  = atof(rs.m_sField[0]);
        rec.m_rLon1  = atof(rs.m_sField[1]);
        rec.m_rLat2  = atof(rs.m_sField[2]);
        rec.m_rLon2  = atof(rs.m_sField[3]);
        rec.m_radius = atoi(rs.m_sField[4]);
        rec.m_a1     = atoi(rs.m_sField[5]);
        rec.m_a2     = atoi(rs.m_sField[6]);


        tbl[num] = rec;
        num++;
    if (num > AIR_TOTAL)
      throw 1;
    }
    rs.MoveNext();
  }
  rs.Close();
  return true;
}

//-----------------------------------------------------------------------------
// Move to the first entry in the set
//
bool CAirspaceSet::MoveFirst()
{
  cp = 0;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
// Move to the next entry in the set
//
bool CAirspaceSet::MoveNext()
{
  cp++;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
// Move to a specified in the set
//
bool CAirspaceSet::Move(int idx)
{
  cp = idx;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
// Check for the end of the set
// returns true is the end is reached and
// false if not
//
bool CAirspaceSet::IsEOF()
{
  int size = num;// tbl.size();
  if (cp >= size - 1)
    return true;
  else
    return false;
}

//-----------------------------------------------------------------------------
// Clear out the contents of the set
//
void CAirspaceSet::Clear()
{
  cp = 0;
  num = 0;
}

//-----------------------------------------------------------------------------
//
//
void CAirspaceSet::Bind()
{
  AirRec rec;
  rec = tbl[cp];

  m_rLat1  =  rec.m_rLat1;
  m_rLon1  =  rec.m_rLon1;
  m_rLat2  =  rec.m_rLat2;
  m_rLon2  =  rec.m_rLon2;
  m_radius =  rec.m_radius;
  m_a1     =  rec.m_a1;
  m_a2     =  rec.m_a2;
}

//-----------------------------------------------------------------------------
//
//
void CAirspaceSet::Store()
{
  AirRec rec;
    rec.m_rLat1  =  m_rLat1;
    rec.m_rLon1  =  m_rLon1;
    rec.m_rLat2  =  m_rLat2;
    rec.m_rLon2  =  m_rLon2;
    rec.m_radius =  m_radius;
    rec.m_a1     =  m_a1;
    rec.m_a2     =  m_a2;

  tbl[cp] = rec;
}

//-----------------------------------------------------------------------------
//
//
AirRec CAirspaceSet::GetRecord()
{
  AirRec rec;
    rec.m_rLat1  =  m_rLat1;
    rec.m_rLon1  =  m_rLon1;
    rec.m_rLat2  =  m_rLat2;
    rec.m_rLon2  =  m_rLon2;
    rec.m_radius =  m_radius;
    rec.m_a1     =  m_a1;
    rec.m_a2     =  m_a2;

  return rec;
}

#endif
