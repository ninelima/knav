//
// $Id: AltGadget.h,v 1.4 2005/11/21 10:34:16 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//
#ifndef __ALTGADGET_H
#define __ALTGADGET_H

#include <qwidget.h>
#include <qpixmap.h>

#ifndef __GRAPHICPAINTER_H
  #include "GraphicPainter.h"
#endif // __GRAPHICPAINTER_H

#include "PanelGadget.h"

class AltGadget : public PanelGadget //QWidget //PanelGadget
{
public:
  int m_alt;
  //int hotspot; /** 80 sets hotspot to the arrow */
  QRect rect;

  AltGadget(QWidget *parent=0, const char *name=0, WFlags f=0);

  void drawBlankFace(GraphicPainter *gc, int x, int y, int size);
  void drawFace(GraphicPainter *gc);
 
  void setAlt(int alt);

private:
};

#endif // __ALTGADGET_H
