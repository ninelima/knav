/****************************************************************************
** Form implementation generated from reading ui file 'FlightNotification_3.ui'
**
** Created: Sun Dec 5 13:04:47 2004
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.3   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "FlightNotification.h"

#include <qvariant.h>
#include <qpushbutton.h>
#include <qtabwidget.h>
#include <qwidget.h>
#include <qlineedit.h>
#include <qcombobox.h>
#include <qlabel.h>
#include <qbuttongroup.h>
#include <qcheckbox.h>
#include <qlayout.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

/*
 *  Constructs a Flight_Notification as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
Flight_Notification::Flight_Notification( QWidget* parent, const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
	setName( "Flight_Notification" );

    tabWidget2 = new QTabWidget( this, "tabWidget2" );
    tabWidget2->setGeometry( QRect( 11, 11, 335, 583 ) );

    tab = new QWidget( tabWidget2, "tab" );
    tabLayout = new QVBoxLayout( tab, 11, 6, "tabLayout"); 

    layout7 = new QGridLayout( 0, 1, 1, 0, 6, "layout7"); 

    lineEdit1 = new QLineEdit( tab, "lineEdit1" );

    layout7->addWidget( lineEdit1, 0, 1 );

    comboBox2 = new QComboBox( FALSE, tab, "comboBox2" );

    layout7->addWidget( comboBox2, 5, 1 );

    textLabel1_2 = new QLabel( tab, "textLabel1_2" );
    textLabel1_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout7->addWidget( textLabel1_2, 3, 0 );

    textLabel2 = new QLabel( tab, "textLabel2" );
    textLabel2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout7->addWidget( textLabel2, 1, 0 );

    lineEdit3 = new QLineEdit( tab, "lineEdit3" );
    lineEdit3->setFrameShape( QLineEdit::LineEditPanel );
    lineEdit3->setFrameShadow( QLineEdit::Sunken );

    layout7->addWidget( lineEdit3, 4, 1 );

    lineEdit2 = new QLineEdit( tab, "lineEdit2" );

    layout7->addWidget( lineEdit2, 3, 1 );

    comboBox1 = new QComboBox( FALSE, tab, "comboBox1" );

    layout7->addWidget( comboBox1, 2, 1 );

    textLabel2_2 = new QLabel( tab, "textLabel2_2" );
    textLabel2_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout7->addWidget( textLabel2_2, 4, 0 );

    textLabel3_2 = new QLabel( tab, "textLabel3_2" );
    textLabel3_2->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout7->addWidget( textLabel3_2, 5, 0 );

    textLabel1 = new QLabel( tab, "textLabel1" );
    textLabel1->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout7->addWidget( textLabel1, 0, 0 );

    textLabel3 = new QLabel( tab, "textLabel3" );
    textLabel3->setAlignment( int( QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout7->addWidget( textLabel3, 2, 0 );

    comboBox4 = new QComboBox( FALSE, tab, "comboBox4" );

    layout7->addWidget( comboBox4, 1, 1 );
    tabLayout->addLayout( layout7 );

    buttonGroup1 = new QButtonGroup( tab, "buttonGroup1" );
    buttonGroup1->setColumnLayout(0, Qt::Vertical );
    buttonGroup1->layout()->setSpacing( 6 );
    buttonGroup1->layout()->setMargin( 11 );
    buttonGroup1Layout = new QGridLayout( buttonGroup1->layout() );
    buttonGroup1Layout->setAlignment( Qt::AlignTop );

    textLabel5 = new QLabel( buttonGroup1, "textLabel5" );

    buttonGroup1Layout->addWidget( textLabel5, 1, 0 );

    checkBox9 = new QCheckBox( buttonGroup1, "checkBox9" );

    buttonGroup1Layout->addWidget( checkBox9, 4, 0 );

    checkBox3 = new QCheckBox( buttonGroup1, "checkBox3" );

    buttonGroup1Layout->addWidget( checkBox3, 2, 0 );

    checkBox10 = new QCheckBox( buttonGroup1, "checkBox10" );

    buttonGroup1Layout->addWidget( checkBox10, 4, 1 );

    checkBox7 = new QCheckBox( buttonGroup1, "checkBox7" );

    buttonGroup1Layout->addWidget( checkBox7, 3, 1 );

    checkBox4 = new QCheckBox( buttonGroup1, "checkBox4" );

    buttonGroup1Layout->addWidget( checkBox4, 2, 1 );

    checkBox2 = new QCheckBox( buttonGroup1, "checkBox2" );

    buttonGroup1Layout->addMultiCellWidget( checkBox2, 0, 0, 1, 2 );

    checkBox1 = new QCheckBox( buttonGroup1, "checkBox1" );

    buttonGroup1Layout->addWidget( checkBox1, 0, 0 );

    checkBox5 = new QCheckBox( buttonGroup1, "checkBox5" );

    buttonGroup1Layout->addWidget( checkBox5, 2, 2 );

    checkBox6 = new QCheckBox( buttonGroup1, "checkBox6" );

    buttonGroup1Layout->addWidget( checkBox6, 3, 0 );

    checkBox8 = new QCheckBox( buttonGroup1, "checkBox8" );

    buttonGroup1Layout->addWidget( checkBox8, 3, 2 );

    checkBox11 = new QCheckBox( buttonGroup1, "checkBox11" );

    buttonGroup1Layout->addWidget( checkBox11, 4, 2 );

    checkBox16 = new QCheckBox( buttonGroup1, "checkBox16" );

    buttonGroup1Layout->addWidget( checkBox16, 6, 1 );

    checkBox15 = new QCheckBox( buttonGroup1, "checkBox15" );

    buttonGroup1Layout->addWidget( checkBox15, 6, 0 );

    checkBox14 = new QCheckBox( buttonGroup1, "checkBox14" );

    buttonGroup1Layout->addWidget( checkBox14, 5, 2 );

    checkBox13 = new QCheckBox( buttonGroup1, "checkBox13" );

    buttonGroup1Layout->addWidget( checkBox13, 5, 1 );

    checkBox12 = new QCheckBox( buttonGroup1, "checkBox12" );

    buttonGroup1Layout->addWidget( checkBox12, 5, 0 );
    tabLayout->addWidget( buttonGroup1 );

    layout6 = new QGridLayout( 0, 1, 1, 0, 6, "layout6"); 

    textLabel3_3 = new QLabel( tab, "textLabel3_3" );
    textLabel3_3->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout6->addWidget( textLabel3_3, 2, 0 );

    textLabel2_3 = new QLabel( tab, "textLabel2_3" );
    textLabel2_3->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout6->addWidget( textLabel2_3, 1, 0 );

    comboBox3 = new QComboBox( FALSE, tab, "comboBox3" );

    layout6->addWidget( comboBox3, 0, 1 );

    textLabel7 = new QLabel( tab, "textLabel7" );
    textLabel7->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout6->addMultiCellWidget( textLabel7, 6, 7, 0, 0 );

    lineEdit5 = new QLineEdit( tab, "lineEdit5" );

    layout6->addMultiCellWidget( lineEdit5, 2, 3, 1, 1 );

    textLabel8 = new QLabel( tab, "textLabel8" );
    textLabel8->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout6->addWidget( textLabel8, 8, 0 );

    textLabel5_2 = new QLabel( tab, "textLabel5_2" );
    textLabel5_2->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout6->addWidget( textLabel5_2, 4, 0 );

    lineEdit4 = new QLineEdit( tab, "lineEdit4" );

    layout6->addWidget( lineEdit4, 1, 1 );

    textLabel4 = new QLabel( tab, "textLabel4" );
    textLabel4->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout6->addWidget( textLabel4, 3, 0 );

    lineEdit7 = new QLineEdit( tab, "lineEdit7" );

    layout6->addMultiCellWidget( lineEdit7, 5, 6, 1, 1 );

    textLabel6 = new QLabel( tab, "textLabel6" );
    textLabel6->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout6->addWidget( textLabel6, 5, 0 );

    lineEdit8 = new QLineEdit( tab, "lineEdit8" );

    layout6->addMultiCellWidget( lineEdit8, 7, 8, 1, 1 );

    textLabel1_3 = new QLabel( tab, "textLabel1_3" );
    textLabel1_3->setAlignment( int( QLabel::WordBreak | QLabel::AlignVCenter | QLabel::AlignRight ) );

    layout6->addWidget( textLabel1_3, 0, 0 );

    lineEdit6 = new QLineEdit( tab, "lineEdit6" );

    layout6->addWidget( lineEdit6, 4, 1 );
    tabLayout->addLayout( layout6 );
    tabWidget2->insertTab( tab, QString("") );

    tab_2 = new QWidget( tabWidget2, "tab_2" );
    tabLayout_2 = new QGridLayout( tab_2, 1, 1, 11, 6, "tabLayout_2"); 

    lineEdit9 = new QLineEdit( tab_2, "lineEdit9" );

    tabLayout_2->addWidget( lineEdit9, 1, 0 );

    lineEdit10 = new QLineEdit( tab_2, "lineEdit10" );

    tabLayout_2->addWidget( lineEdit10, 1, 1 );

    lineEdit12 = new QLineEdit( tab_2, "lineEdit12" );

    tabLayout_2->addWidget( lineEdit12, 1, 4 );

    lineEdit13 = new QLineEdit( tab_2, "lineEdit13" );

    tabLayout_2->addWidget( lineEdit13, 1, 5 );

    textLabel7_2_2 = new QLabel( tab_2, "textLabel7_2_2" );

    tabLayout_2->addWidget( textLabel7_2_2, 1, 2 );

    lineEdit11 = new QLineEdit( tab_2, "lineEdit11" );

    tabLayout_2->addWidget( lineEdit11, 1, 3 );

    lineEdit9_2 = new QLineEdit( tab_2, "lineEdit9_2" );

    tabLayout_2->addWidget( lineEdit9_2, 2, 0 );

    lineEdit10_2 = new QLineEdit( tab_2, "lineEdit10_2" );

    tabLayout_2->addWidget( lineEdit10_2, 2, 1 );

    lineEdit13_2 = new QLineEdit( tab_2, "lineEdit13_2" );

    tabLayout_2->addWidget( lineEdit13_2, 2, 5 );

    lineEdit12_2 = new QLineEdit( tab_2, "lineEdit12_2" );

    tabLayout_2->addWidget( lineEdit12_2, 2, 4 );

    textLabel7_2_2_2 = new QLabel( tab_2, "textLabel7_2_2_2" );

    tabLayout_2->addWidget( textLabel7_2_2_2, 2, 2 );

    lineEdit11_2 = new QLineEdit( tab_2, "lineEdit11_2" );

    tabLayout_2->addWidget( lineEdit11_2, 2, 3 );

    lineEdit13_3 = new QLineEdit( tab_2, "lineEdit13_3" );

    tabLayout_2->addWidget( lineEdit13_3, 3, 5 );

    textLabel7_2_2_3 = new QLabel( tab_2, "textLabel7_2_2_3" );

    tabLayout_2->addWidget( textLabel7_2_2_3, 3, 2 );

    lineEdit10_3 = new QLineEdit( tab_2, "lineEdit10_3" );

    tabLayout_2->addWidget( lineEdit10_3, 3, 1 );

    lineEdit9_3 = new QLineEdit( tab_2, "lineEdit9_3" );

    tabLayout_2->addWidget( lineEdit9_3, 3, 0 );

    lineEdit12_3 = new QLineEdit( tab_2, "lineEdit12_3" );

    tabLayout_2->addWidget( lineEdit12_3, 3, 4 );

    lineEdit11_3 = new QLineEdit( tab_2, "lineEdit11_3" );

    tabLayout_2->addWidget( lineEdit11_3, 3, 3 );

    lineEdit10_4 = new QLineEdit( tab_2, "lineEdit10_4" );

    tabLayout_2->addWidget( lineEdit10_4, 4, 1 );

    lineEdit9_4 = new QLineEdit( tab_2, "lineEdit9_4" );

    tabLayout_2->addWidget( lineEdit9_4, 4, 0 );

    lineEdit13_4 = new QLineEdit( tab_2, "lineEdit13_4" );

    tabLayout_2->addWidget( lineEdit13_4, 4, 5 );

    lineEdit12_4 = new QLineEdit( tab_2, "lineEdit12_4" );

    tabLayout_2->addWidget( lineEdit12_4, 4, 4 );

    textLabel7_2_2_4 = new QLabel( tab_2, "textLabel7_2_2_4" );

    tabLayout_2->addWidget( textLabel7_2_2_4, 4, 2 );

    lineEdit11_4 = new QLineEdit( tab_2, "lineEdit11_4" );

    tabLayout_2->addWidget( lineEdit11_4, 4, 3 );

    lineEdit13_5 = new QLineEdit( tab_2, "lineEdit13_5" );

    tabLayout_2->addWidget( lineEdit13_5, 5, 5 );

    lineEdit9_5 = new QLineEdit( tab_2, "lineEdit9_5" );

    tabLayout_2->addWidget( lineEdit9_5, 5, 0 );

    lineEdit10_5 = new QLineEdit( tab_2, "lineEdit10_5" );

    tabLayout_2->addWidget( lineEdit10_5, 5, 1 );

    lineEdit12_5 = new QLineEdit( tab_2, "lineEdit12_5" );

    tabLayout_2->addWidget( lineEdit12_5, 5, 4 );

    lineEdit11_5 = new QLineEdit( tab_2, "lineEdit11_5" );

    tabLayout_2->addWidget( lineEdit11_5, 5, 3 );

    textLabel7_2_2_5 = new QLabel( tab_2, "textLabel7_2_2_5" );

    tabLayout_2->addWidget( textLabel7_2_2_5, 5, 2 );

    textLabel1_4 = new QLabel( tab_2, "textLabel1_4" );

    tabLayout_2->addWidget( textLabel1_4, 0, 0 );

    textLabel2_4 = new QLabel( tab_2, "textLabel2_4" );

    tabLayout_2->addWidget( textLabel2_4, 0, 1 );

    textLabel3_4 = new QLabel( tab_2, "textLabel3_4" );

    tabLayout_2->addWidget( textLabel3_4, 0, 3 );

    textLabel5_3 = new QLabel( tab_2, "textLabel5_3" );

    tabLayout_2->addWidget( textLabel5_3, 0, 5 );

    textLabel4_2 = new QLabel( tab_2, "textLabel4_2" );

    tabLayout_2->addWidget( textLabel4_2, 0, 4 );

    textLabel7_2 = new QLabel( tab_2, "textLabel7_2" );

    tabLayout_2->addWidget( textLabel7_2, 0, 2 );

    lineEdit9_6 = new QLineEdit( tab_2, "lineEdit9_6" );

    tabLayout_2->addWidget( lineEdit9_6, 6, 0 );

    lineEdit10_6 = new QLineEdit( tab_2, "lineEdit10_6" );

    tabLayout_2->addWidget( lineEdit10_6, 6, 1 );

    textLabel7_2_2_6 = new QLabel( tab_2, "textLabel7_2_2_6" );

    tabLayout_2->addWidget( textLabel7_2_2_6, 6, 2 );

    lineEdit11_6 = new QLineEdit( tab_2, "lineEdit11_6" );

    tabLayout_2->addWidget( lineEdit11_6, 6, 3 );

    lineEdit12_6 = new QLineEdit( tab_2, "lineEdit12_6" );

    tabLayout_2->addWidget( lineEdit12_6, 6, 4 );

    lineEdit13_6 = new QLineEdit( tab_2, "lineEdit13_6" );

    tabLayout_2->addWidget( lineEdit13_6, 6, 5 );

    lineEdit11_6_3 = new QLineEdit( tab_2, "lineEdit11_6_3" );

    tabLayout_2->addWidget( lineEdit11_6_3, 7, 3 );

    lineEdit10_6_3 = new QLineEdit( tab_2, "lineEdit10_6_3" );

    tabLayout_2->addWidget( lineEdit10_6_3, 7, 1 );

    lineEdit13_6_3 = new QLineEdit( tab_2, "lineEdit13_6_3" );

    tabLayout_2->addWidget( lineEdit13_6_3, 7, 5 );

    lineEdit9_6_3 = new QLineEdit( tab_2, "lineEdit9_6_3" );

    tabLayout_2->addWidget( lineEdit9_6_3, 7, 0 );

    textLabel7_2_2_6_3 = new QLabel( tab_2, "textLabel7_2_2_6_3" );

    tabLayout_2->addWidget( textLabel7_2_2_6_3, 7, 2 );

    lineEdit12_6_3 = new QLineEdit( tab_2, "lineEdit12_6_3" );

    tabLayout_2->addWidget( lineEdit12_6_3, 7, 4 );

    textLabel7_2_2_6_4 = new QLabel( tab_2, "textLabel7_2_2_6_4" );

    tabLayout_2->addWidget( textLabel7_2_2_6_4, 8, 2 );

    lineEdit13_6_4 = new QLineEdit( tab_2, "lineEdit13_6_4" );

    tabLayout_2->addWidget( lineEdit13_6_4, 8, 5 );

    lineEdit12_6_4 = new QLineEdit( tab_2, "lineEdit12_6_4" );

    tabLayout_2->addWidget( lineEdit12_6_4, 8, 4 );

    lineEdit10_6_4 = new QLineEdit( tab_2, "lineEdit10_6_4" );

    tabLayout_2->addWidget( lineEdit10_6_4, 8, 1 );

    lineEdit11_6_4 = new QLineEdit( tab_2, "lineEdit11_6_4" );

    tabLayout_2->addWidget( lineEdit11_6_4, 8, 3 );

    lineEdit9_6_4 = new QLineEdit( tab_2, "lineEdit9_6_4" );

    tabLayout_2->addWidget( lineEdit9_6_4, 8, 0 );

    lineEdit12_6_5 = new QLineEdit( tab_2, "lineEdit12_6_5" );

    tabLayout_2->addWidget( lineEdit12_6_5, 9, 4 );

    lineEdit11_6_5 = new QLineEdit( tab_2, "lineEdit11_6_5" );

    tabLayout_2->addWidget( lineEdit11_6_5, 9, 3 );

    lineEdit9_6_5 = new QLineEdit( tab_2, "lineEdit9_6_5" );

    tabLayout_2->addWidget( lineEdit9_6_5, 9, 0 );

    textLabel7_2_2_6_5 = new QLabel( tab_2, "textLabel7_2_2_6_5" );

    tabLayout_2->addWidget( textLabel7_2_2_6_5, 9, 2 );

    lineEdit10_6_5 = new QLineEdit( tab_2, "lineEdit10_6_5" );

    tabLayout_2->addWidget( lineEdit10_6_5, 9, 1 );

    lineEdit13_6_5 = new QLineEdit( tab_2, "lineEdit13_6_5" );

    tabLayout_2->addWidget( lineEdit13_6_5, 9, 5 );

    textLabel7_2_2_6_6 = new QLabel( tab_2, "textLabel7_2_2_6_6" );

    tabLayout_2->addWidget( textLabel7_2_2_6_6, 10, 2 );

    lineEdit11_6_6 = new QLineEdit( tab_2, "lineEdit11_6_6" );

    tabLayout_2->addWidget( lineEdit11_6_6, 10, 3 );

    lineEdit12_6_6 = new QLineEdit( tab_2, "lineEdit12_6_6" );

    tabLayout_2->addWidget( lineEdit12_6_6, 10, 4 );

    lineEdit9_6_6 = new QLineEdit( tab_2, "lineEdit9_6_6" );

    tabLayout_2->addWidget( lineEdit9_6_6, 10, 0 );

    lineEdit13_6_6 = new QLineEdit( tab_2, "lineEdit13_6_6" );

    tabLayout_2->addWidget( lineEdit13_6_6, 10, 5 );

    lineEdit10_6_6 = new QLineEdit( tab_2, "lineEdit10_6_6" );

    tabLayout_2->addWidget( lineEdit10_6_6, 10, 1 );

    lineEdit13_6_7 = new QLineEdit( tab_2, "lineEdit13_6_7" );

    tabLayout_2->addWidget( lineEdit13_6_7, 11, 5 );

    lineEdit9_6_7 = new QLineEdit( tab_2, "lineEdit9_6_7" );

    tabLayout_2->addWidget( lineEdit9_6_7, 11, 0 );

    lineEdit12_6_7 = new QLineEdit( tab_2, "lineEdit12_6_7" );

    tabLayout_2->addWidget( lineEdit12_6_7, 11, 4 );

    lineEdit11_6_7 = new QLineEdit( tab_2, "lineEdit11_6_7" );

    tabLayout_2->addWidget( lineEdit11_6_7, 11, 3 );

    lineEdit10_6_7 = new QLineEdit( tab_2, "lineEdit10_6_7" );

    tabLayout_2->addWidget( lineEdit10_6_7, 11, 1 );

    textLabel7_2_2_6_7 = new QLabel( tab_2, "textLabel7_2_2_6_7" );

    tabLayout_2->addWidget( textLabel7_2_2_6_7, 11, 2 );

    lineEdit12_6_8 = new QLineEdit( tab_2, "lineEdit12_6_8" );

    tabLayout_2->addWidget( lineEdit12_6_8, 12, 4 );

    lineEdit13_6_8 = new QLineEdit( tab_2, "lineEdit13_6_8" );

    tabLayout_2->addWidget( lineEdit13_6_8, 12, 5 );

    lineEdit9_6_8 = new QLineEdit( tab_2, "lineEdit9_6_8" );

    tabLayout_2->addWidget( lineEdit9_6_8, 12, 0 );

    lineEdit11_6_8 = new QLineEdit( tab_2, "lineEdit11_6_8" );

    tabLayout_2->addWidget( lineEdit11_6_8, 12, 3 );

    lineEdit10_6_8 = new QLineEdit( tab_2, "lineEdit10_6_8" );

    tabLayout_2->addWidget( lineEdit10_6_8, 12, 1 );

    textLabel7_2_2_6_8 = new QLabel( tab_2, "textLabel7_2_2_6_8" );

    tabLayout_2->addWidget( textLabel7_2_2_6_8, 12, 2 );

    textLabel7_2_2_6_9 = new QLabel( tab_2, "textLabel7_2_2_6_9" );

    tabLayout_2->addWidget( textLabel7_2_2_6_9, 13, 2 );

    lineEdit11_6_9 = new QLineEdit( tab_2, "lineEdit11_6_9" );

    tabLayout_2->addWidget( lineEdit11_6_9, 13, 3 );

    lineEdit13_6_9 = new QLineEdit( tab_2, "lineEdit13_6_9" );

    tabLayout_2->addWidget( lineEdit13_6_9, 13, 5 );

    lineEdit12_6_9 = new QLineEdit( tab_2, "lineEdit12_6_9" );

    tabLayout_2->addWidget( lineEdit12_6_9, 13, 4 );

    lineEdit9_6_9 = new QLineEdit( tab_2, "lineEdit9_6_9" );

    tabLayout_2->addWidget( lineEdit9_6_9, 13, 0 );

    lineEdit10_6_9 = new QLineEdit( tab_2, "lineEdit10_6_9" );

    tabLayout_2->addWidget( lineEdit10_6_9, 13, 1 );

    lineEdit13_6_10 = new QLineEdit( tab_2, "lineEdit13_6_10" );

    tabLayout_2->addWidget( lineEdit13_6_10, 14, 5 );

    lineEdit12_6_10 = new QLineEdit( tab_2, "lineEdit12_6_10" );

    tabLayout_2->addWidget( lineEdit12_6_10, 14, 4 );

    lineEdit11_6_10 = new QLineEdit( tab_2, "lineEdit11_6_10" );

    tabLayout_2->addWidget( lineEdit11_6_10, 14, 3 );

    lineEdit10_6_10 = new QLineEdit( tab_2, "lineEdit10_6_10" );

    tabLayout_2->addWidget( lineEdit10_6_10, 14, 1 );

    textLabel7_2_2_6_10 = new QLabel( tab_2, "textLabel7_2_2_6_10" );

    tabLayout_2->addWidget( textLabel7_2_2_6_10, 14, 2 );

    lineEdit9_6_10 = new QLineEdit( tab_2, "lineEdit9_6_10" );

    tabLayout_2->addWidget( lineEdit9_6_10, 14, 0 );

    lineEdit10_6_11 = new QLineEdit( tab_2, "lineEdit10_6_11" );

    tabLayout_2->addWidget( lineEdit10_6_11, 15, 1 );

    lineEdit11_6_11 = new QLineEdit( tab_2, "lineEdit11_6_11" );

    tabLayout_2->addWidget( lineEdit11_6_11, 15, 3 );

    lineEdit12_6_11 = new QLineEdit( tab_2, "lineEdit12_6_11" );

    tabLayout_2->addWidget( lineEdit12_6_11, 15, 4 );

    lineEdit9_6_11 = new QLineEdit( tab_2, "lineEdit9_6_11" );

    tabLayout_2->addWidget( lineEdit9_6_11, 15, 0 );

    lineEdit13_6_11 = new QLineEdit( tab_2, "lineEdit13_6_11" );

    tabLayout_2->addWidget( lineEdit13_6_11, 15, 5 );

    textLabel7_2_2_6_11 = new QLabel( tab_2, "textLabel7_2_2_6_11" );

    tabLayout_2->addWidget( textLabel7_2_2_6_11, 15, 2 );

    lineEdit10_6_12 = new QLineEdit( tab_2, "lineEdit10_6_12" );

    tabLayout_2->addWidget( lineEdit10_6_12, 16, 1 );

    lineEdit12_6_12 = new QLineEdit( tab_2, "lineEdit12_6_12" );

    tabLayout_2->addWidget( lineEdit12_6_12, 16, 4 );

    textLabel7_2_2_6_12 = new QLabel( tab_2, "textLabel7_2_2_6_12" );

    tabLayout_2->addWidget( textLabel7_2_2_6_12, 16, 2 );

    lineEdit13_6_12 = new QLineEdit( tab_2, "lineEdit13_6_12" );

    tabLayout_2->addWidget( lineEdit13_6_12, 16, 5 );

    lineEdit11_6_12 = new QLineEdit( tab_2, "lineEdit11_6_12" );

    tabLayout_2->addWidget( lineEdit11_6_12, 16, 3 );

    lineEdit9_6_12 = new QLineEdit( tab_2, "lineEdit9_6_12" );

    tabLayout_2->addWidget( lineEdit9_6_12, 16, 0 );
    tabWidget2->insertTab( tab_2, QString("") );

    TabPage = new QWidget( tabWidget2, "TabPage" );
    tabWidget2->insertTab( TabPage, QString("") );

    TabPage_2 = new QWidget( tabWidget2, "TabPage_2" );
    tabWidget2->insertTab( TabPage_2, QString("") );

    QWidget* privateLayoutWidget = new QWidget( this, "layout9" );
    privateLayoutWidget->setGeometry( QRect( 11, 600, 335, 28 ) );
    layout9 = new QHBoxLayout( privateLayoutWidget, 11, 6, "layout9"); 
    spacer1 = new QSpacerItem( 40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout9->addItem( spacer1 );

    pushButton1 = new QPushButton( privateLayoutWidget, "pushButton1" );
    layout9->addWidget( pushButton1 );

    pushButton2 = new QPushButton( privateLayoutWidget, "pushButton2" );
    layout9->addWidget( pushButton2 );
    languageChange();
    resize( QSize(448, 628).expandedTo(minimumSizeHint()) );
    clearWState( WState_Polished );
}

/*
 *  Destroys the object and frees any allocated resources
 */
Flight_Notification::~Flight_Notification()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void Flight_Notification::languageChange()
{
    setCaption( tr( "Flight Notification" ) );
    comboBox2->clear();
    comboBox2->insertItem( tr( "L - Light" ) );
    comboBox2->insertItem( tr( "M - Medium" ) );
    comboBox2->insertItem( tr( "H - Heavy" ) );
    textLabel1_2->setText( tr( "Number of Aircraft" ) );
    textLabel2->setText( tr( "Flight Rules" ) );
    comboBox1->clear();
    comboBox1->insertItem( tr( "S - Scheduled" ) );
    comboBox1->insertItem( tr( "N - Non Scheduled" ) );
    comboBox1->insertItem( tr( "G - General Aviation" ) );
    comboBox1->insertItem( tr( "M - Miltary" ) );
    comboBox1->insertItem( tr( "X - Other" ) );
    textLabel2_2->setText( tr( "Aircraft Type" ) );
    textLabel3_2->setText( tr( "Wake Turbulence Category" ) );
    textLabel1->setText( tr( "Aircraft Id" ) );
    textLabel3->setText( tr( "Type of Flight" ) );
    comboBox4->clear();
    comboBox4->insertItem( tr( "I - IFR" ) );
    comboBox4->insertItem( tr( "V - VFR" ) );
    comboBox4->insertItem( tr( "Y - IFR then VFR" ) );
    comboBox4->insertItem( tr( "Z - VFR then IFR" ) );
    buttonGroup1->setTitle( tr( "NAV/COM Equipment" ) );
    textLabel5->setText( tr( "and/or" ) );
    checkBox9->setText( tr( "L - ILS" ) );
    checkBox3->setText( tr( "D - DME" ) );
    checkBox10->setText( tr( "O - VOR" ) );
    checkBox7->setText( tr( "I - INS" ) );
    checkBox4->setText( tr( "F - ADF" ) );
    checkBox2->setText( tr( "S - VHF/ADF/ILS/VOR" ) );
    checkBox1->setText( tr( "N - Nil" ) );
    checkBox5->setText( tr( "G - GNSS" ) );
    checkBox6->setText( tr( "H - HF" ) );
    checkBox8->setText( tr( "J - Datalink" ) );
    checkBox11->setText( tr( "R - RNP" ) );
    checkBox16->setText( tr( "Z - Other" ) );
    checkBox15->setText( tr( "W - RVSM" ) );
    checkBox14->setText( tr( "V - VHF" ) );
    checkBox13->setText( tr( "U - UHF" ) );
    checkBox12->setText( tr( "T - Tacan" ) );
    textLabel3_3->setText( tr( "ETD" ) );
    textLabel2_3->setText( tr( "Departure" ) );
    comboBox3->clear();
    comboBox3->insertItem( tr( "N - None" ) );
    comboBox3->insertItem( tr( "A - Mode A" ) );
    comboBox3->insertItem( tr( "C - Mode A + C" ) );
    comboBox3->insertItem( tr( "S - Mode S" ) );
    textLabel7->setText( tr( "Total EET" ) );
    textLabel8->setText( tr( "Alternate" ) );
    textLabel5_2->setText( tr( "Initial Cruising Level" ) );
    textLabel4->setText( tr( "Initial Cruising Speed" ) );
    textLabel6->setText( tr( "Destination" ) );
    textLabel1_3->setText( tr( "SSR Equirpment" ) );
    tabWidget2->changeTab( tab, tr( "General" ) );
    textLabel7_2_2->setText( tr( "/" ) );
    textLabel7_2_2_2->setText( tr( "/" ) );
    textLabel7_2_2_3->setText( tr( "/" ) );
    textLabel7_2_2_4->setText( tr( "/" ) );
    textLabel7_2_2_5->setText( tr( "/" ) );
    textLabel1_4->setText( tr( "ATS Route" ) );
    textLabel2_4->setText( tr( "Significant Point" ) );
    textLabel3_4->setText( tr( "New Speed" ) );
    textLabel5_3->setText( tr( "New Rules" ) );
    textLabel4_2->setText( tr( "New FL" ) );
    textLabel7_2->setText( tr( "/" ) );
    textLabel7_2_2_6->setText( tr( "/" ) );
    textLabel7_2_2_6_3->setText( tr( "/" ) );
    textLabel7_2_2_6_4->setText( tr( "/" ) );
    textLabel7_2_2_6_5->setText( tr( "/" ) );
    textLabel7_2_2_6_6->setText( tr( "/" ) );
    textLabel7_2_2_6_7->setText( tr( "/" ) );
    textLabel7_2_2_6_8->setText( tr( "/" ) );
    textLabel7_2_2_6_9->setText( tr( "/" ) );
    textLabel7_2_2_6_10->setText( tr( "/" ) );
    textLabel7_2_2_6_11->setText( tr( "/" ) );
    textLabel7_2_2_6_12->setText( tr( "/" ) );
    tabWidget2->changeTab( tab_2, tr( "Route" ) );
    tabWidget2->changeTab( TabPage, tr( "Other" ) );
    tabWidget2->changeTab( TabPage_2, tr( "Supp" ) );
    pushButton1->setText( tr( "pushButton1" ) );
    pushButton2->setText( tr( "pushButton2" ) );
}

