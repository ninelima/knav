//-----------------------------------------------------------------------------
// $Id: E6BDlg.cpp,v 1.7 2006/04/23 10:54:50 player Exp $
//
// Copyright (C) 1992-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

#include "E6BDlg.h"
#include "E6BDlg.moc"

#include <stdio.h>

#include <qiconview.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qbuttongroup.h>
#include <qradiobutton.h>
#include <qlayout.h>
#include <qvariant.h>
#include <qtooltip.h>
#include <qwhatsthis.h>

//-----------------------------------------------------------------------------
//  Construct a E6BDlg which is a child of 'parent', with the
//  name 'name' and widget flags set to 'f'
//
//  The dialog will by default be modeless, unless you set 'modal' to
//  TRUE to construct a modal dialog.
//
E6BDlg::E6BDlg( QWidget* parent,  const char* name, bool modal, WFlags fl )
    : QDialog( parent, name, modal, fl )
{
    if ( !name )
      setName( "E6BDlg" );
    resize( 638, 440 ); 
    setCaption( tr( "E6BDlg" ) );
    setSizeGripEnabled( TRUE );
    E6BDlgLayout = new QVBoxLayout( this );
    E6BDlgLayout->setSpacing( 6 );
    E6BDlgLayout->setMargin( 11 );
/*
    Layout22 = new QHBoxLayout; 
    Layout22->setSpacing( 6 );
    Layout22->setMargin( 0 );

    Layout20 = new QVBoxLayout; 
    Layout20->setSpacing( 6 );
    Layout20->setMargin( 0 );

    Layout17 = new QGridLayout; 
    Layout17->setSpacing( 6 );
    Layout17->setMargin( 0 );
*/

    e6bGadget = new E6BGadget(this, "e6b");
    E6BDlgLayout->addWidget(e6bGadget);

    Layout1 = new QHBoxLayout;
    Layout1->setSpacing( 6 );
    Layout1->setMargin( 12 );       

    buttonGroup = new QButtonGroup(this, "radiogroup");
    //buttonGroup->setTitle( "hotspot" );

    Layout1->addWidget(buttonGroup);

    // Create a layout for the radio buttons
    QHBoxLayout *hbox = new QHBoxLayout(buttonGroup, 10);

    //button10 = new QRadioButton( this, "10" );
    button10 = new QRadioButton(buttonGroup);
    button10->setText( "10" );
    button10->setChecked(true);
    hbox->addWidget( button10 );

    //button60 = new QRadioButton( this, "60" );
    button60 = new QRadioButton(buttonGroup);
    button60->setText( "60" );
    hbox->addWidget( button60 );

    connect(buttonGroup, SIGNAL(clicked(int)), SLOT(radioButtonClicked(int)) );


    QSpacerItem* spacer_3 = new QSpacerItem( 20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    Layout1->addItem( spacer_3 );

    buttonCancel = new QPushButton( this, "buttonCancel" );
    buttonCancel->setText( tr( "Dismiss" ) );
    buttonCancel->setAutoDefault( TRUE );
    Layout1->addWidget( buttonCancel );
    E6BDlgLayout->addLayout( Layout1 );

    // signals and slots connections
    connect( buttonCancel, SIGNAL( clicked() ), this, SLOT( reject() ) );
}

//-----------------------------------------------------------------------------
// Destroy the object and frees any allocated resources
//
E6BDlg::~E6BDlg()
{
  // no need to delete child widgets, Qt does it all for us
}

void E6BDlg::radioButtonClicked( int id )
{
  printf(QString("Radio button #%1 clicked\n").arg(id));
  switch (id) {
    case 0:
      e6bGadget->hotspot = 0;
      break;
    case 1:
      e6bGadget->hotspot = 80;
      break;
  }
}
