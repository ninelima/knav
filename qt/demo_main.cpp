//-----------------------------------------------------------------------------
// $Id: demo_main.cpp,v 1.2 2005/01/13 12:52:36 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786

#ifdef WIN32
  #include <windows.h>
  #include <fcntl.h>
#endif //WIN32

#include <qapplication.h>
#include "version.h"
#include "MainWindow.h"

// the styles
#include <qmotifstyle.h>
#include <qcdestyle.h>
#include <qwindowsstyle.h>
#include <qplatinumstyle.h>
#include <qsgistyle.h>
#include <qjpegio.h>
#include <qinputdialog.h>
#include <qmessagebox.h>

#include <stdlib.h>
#include <stdio.h>
#include <io.h>
#include "ustring.h"
#include "iniFile.h"
#include "umisc.h"

// Include the windows headers on the windows
// platform so we can do some windows stuff.
/*
#ifdef WIN32
  #include <windows.h>
  #include <fcntl.h>
#endif //WIN32
*/

//

//-----------------------------------------------------------------------------
// Check for a serial number and prompt for username and serial
// number if none is found
//
bool check_serial_num()
{
  bool ok = false;
  bool check = false;
  char szSerialNum[256];
  char szOwner[256];

  //
  // Read the ini file for some last used settings.
  // Any of these can be overridden by paramaters
  // specified on the command line
  //
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iniFile.GetValueS(szSerialNum, "Prefs", "SerialNum", "");
  iniFile.GetValueS(szOwner, "Prefs", "Owner", "");

  if (strlen(szSerialNum) > 0) {
    return check_code(szSerialNum);
  }

  QString text;

  //
  // Prompt for a owner name
  //
  text = QInputDialog::getText(SZ_APPNAME,
                               "Please enter your name\n",
                               QLineEdit::Normal,
                               "", &ok, NULL );
  if (ok && !text.isEmpty()) {
    strcpy(szOwner, text.latin1());
    iniFile.SetValue("Prefs", "Owner", szOwner);
    iniFile.WriteFile();
  }
  else {
    // user entered nothing or pressed cancel
    check = false;
  }

  //
  // Prompt for a serial number
  //
  text = QInputDialog::getText(SZ_APPNAME,
                               "Please enter your serial number - format: nnn-nnn-nnn\n",
                               QLineEdit::Normal,
                               "111-222-333", &ok, NULL );
  if (ok && !text.isEmpty()) {
    check = check_code(text.latin1());// user entered something and pressed ok
    strcpy(szSerialNum, text.latin1());
  }
  else {
    // user entered nothing or pressed cancel
    check = false;
  }

  if (check) {
    iniFile.SetValue("Prefs", "SerialNum", szSerialNum);
    iniFile.WriteFile();
  }

  return check;
}

//-----------------------------------------------------------------------------
// Display the disclaimer and get confirmation
//
bool check_disclaimer()
{
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  int iDisclaimer = iniFile.GetValueI("Prefs", "Disclaimer", 1);
  if (iDisclaimer == 0)
    return true;

  int rv = QMessageBox::information(NULL, SZ_PRGNAME,
    "This program is supplied for educational and entertainment puposes \n"
    "only, without representation or warranty of any kind. \n\n"
 
    "The author therefore assume no responsibility and shall have no \n"
    "liability, consequential or otherwise, of any kind arising from the \n"
    "use of this program or any part thereof.\n\n"
 
    "Click the Reject button if you DISAGREE.\n\n"
 
    "Click the Accept button if you UNDERSTAND and ACCEPT\n"
    "these conditions.\n\n",
    "Accept", "Reject",
    0, 1);
 
  switch (rv) {
    case 0:  // accept
      return true;
      break;
    case 1:  // reject
    default: // just for sanity
      break;
    case 2:  // abandon
      break;
  }
  return false;
}

//
// int main(int argc, char **argv)
//
int __main__(int argc, char *argv[])
{
  char szFileName[256];
  int iStyle = -1;

  //
  //  addconsole();
  //

  printf("\n\n"
         "Qt Kwik Naviation Planner\n"
         SZ_VERSION "\n\n"
         "(c) Player One 2003\n"
         "All rights reserved"
         "\n\n");
 
  //
  // Read the ini file for some last used settings.
  // Any of these can be overridden by paramaters
  // specified on the command line
  //
  CIniFile iniFile("knav.ini"); iniFile.ReadFile();
  iStyle = iniFile.GetValueI("Prefs", "LookAndFeel", -1);
  //iniFile.GetValueS("Prefs", "LastUsedKrtFile", "none.krt");

  szFileName[0] = '\0';

  //
  // Parse the command line for any useful
  // tidbits
  //
  for (int i = 0; i < argc; i++) {
    puts(argv[i]);

    if (strcmp(argv[i], "-f") == 0)
      strcpy(szFileName, argv[i+1]);

    if (strcmp(argv[i], "-s") == 0)
      iStyle = atoi(argv[i+1]);

    if ((strcmp(argv[i], "-h") == 0) ||
        (strcmp(argv[i], "-?") == 0) ||
        (strcmp(argv[i], "--h") == 0) ||
        (strcmp(argv[i], "--?") == 0)
       ) {

      printf("\n\nKwik Navigator Qt - (c) 2000 Player One\n\n");
      printf("usage:\n\n");
      printf("qtnav.exe <-option [arg]>\n\n");
      printf("with option:\n");
      printf("  -f filename\n");
      printf("  -s style\n");
      printf("     with style:\n");
      printf("        0 - MotifStyle\n");
      printf("        1 - CDEStyle\n");
      printf("        2 - WindowsStyle\n");
      printf("        3 - PlatinumStyle\n");
      printf("        4 - SGIStyle\n");
      printf("  -h help\n");
      printf("  \n");
      printf("example:\n");
      printf("  qtnav.exe -f navex.krt -s 3\n");
      printf("  \n");
      printf("  Open navex.krt on startup and use the Platinum style user interface.");
      char c = getchar();
      return 0;
    }
  }

  //
  // Create our one and only application object
  //
  QApplication app(argc, argv);

  // qInitJpegIO(); // Explicit enabling of JPEG support
                    // mmmm ... this does not seem to work :-(

  ApplicationWindow *mw = new ApplicationWindow();

  #ifndef _DEBUG
  mw->setCaption(SZ_APPNAME " - <none>");
  #else
  mw->setCaption(SZ_APPNAME " - <debug>");
  #endif

  //
  // If a filename is supplied via -f assume
  // it is valid and attempt to load it
  //
  if (szFileName[0] != '\0')
    mw->fileLoad(szFileName);
 
  if (iStyle > -1) {
    switch (iStyle) {
      case 0:
        app.setStyle(new QMotifStyle);
        break;
      case 1:
        app.setStyle(new QCDEStyle);
        break;
      case 2:
        app.setStyle(new QWindowsStyle);
        break;
      case 3:
        app.setStyle(new QPlatinumStyle);
        break;
      case 4:
        app.setStyle(new QSGIStyle);
        break;
    }
  }


  if (!check_disclaimer())
    return -1;

  // DEMO VERSION
  QMessageBox::information(NULL, SZ_PRGNAME,
                           "      DEMONSTRATION VERSION\n\n"
                           "Some program functionality is disabled\n"
                           "and only very limited data is supplied.\n\n"
                           "A very small sample of data is provided in\n"
                           "the south west of Australia");

  // DEMO_VERSION

  mw->show();
  app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));
  return app.exec();
}


#ifdef WINDOWS_CONSOLE
#pragma message ("#\n# WINDOWS_CONSOLE defined\n#\n")
#pragma message ("#\n# main - console enabled\n#\n")

  int main(int argc, char *argv[])
  {
    return __main__(argc, argv);
  }
#else
#pragma message ("#\n# WINDOWS_CONSOLE not defined\n#\n")
#pragma message ("#\n# WinMain - console disabled\n#\n")

  int WINAPI WinMain(
    HINSTANCE hInstance,      // handle to current instance
    HINSTANCE hPrevInstance,  // handle to previous instance
    char *lpCmdLine,          // command line
    int nCmdShow              // show state
  )
  {
    /* Not so sure this is a good idea
    int argc = 0;
    char *argv[256];
    char szbuf[256];

    strcpy(szbuf, lpCmdLine);
    argv[argc] = (char *) malloc(24);
    strcpy(argv[argc], "appname.exe");
    argc++;
    char *sp = strtok(szbuf, " ");
    while (sp != NULL) {
      argv[argc] = (char *) malloc(strlen(sp));
      strcpy(argv[argc], sp);
      argc++;
      sp = strtok(NULL, " ");

      if (argc > 255)
        break;
    }

    return __main__(argc, argv);
    */
    return __main__(0, NULL);
  }
#endif

