// GraphicContext.h: interface for the GraphicPainter class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __GRAPHICPAINTER_H
#define __GRAPHICPAINTER_H

#ifndef __DEFS_H
#include "defs.h"
#endif //__DEFS_H

#ifdef _MOTIF_
#include <Xm/Xm.h>
#include <Xm/MainW.h>
#include <Xm/CascadeB.h>
#include <Xm/Label.h>
#include <Xm/DrawingA.h>
#include <Xm/PushBG.h>
#include <Xm/PushB.h>
#include <Xm/RowColumn.h>
#include <Xm/ScrolledW.h>
#include <Xm/Form.h>
#endif // _MOTIF_


//#include "Point.h"

#ifndef _MOTIF_
#include <qpainter.h>
class GraphicPainter : public QPainter
{
#else
class GraphicPainter
{
#endif //_MOTIF_

public:

  GraphicPainter();
  virtual ~GraphicPainter();

  BOOL SetupFont(unsigned long *Font, int height);

  void setColor(COLORREF crColor);
  void setStroke(int width);
  BOOL drawString(const char *str, int x, int y);
  BOOL drawRectangle(int x, int y, int width, int height);

#ifdef _MOTIF_
  POINT moveTo(int x, int y);
  BOOL lineTo(int x, int y);
#endif //_MOTIF_
  void radLine(int x, int y, int l, int phi);
  void radLineArrow(int x, int y, int l, int phi);

  void radMove(int x, int y, int l, int phi);


  void setLineAttributes(int width, int style, int cap, int join);



#ifdef _MOTIF_
  // To be
  //HDC m_hDC;   //?
  Display *m_Display;
  Window   m_Window;
  GC       m_GC;         
#endif //_MOTIF_
  

  int SetROP2(int nDrawMode);

  COLORREF GetPixel(int x, int y);
  void SetPixel(int x, int y, COLORREF color);

  //virtual BOOL TextOut(int x, int y, LPCTSTR lpszString, int nCount);
  //BOOL TextOut(int x, int y, const CString& str);
  BOOL TextOut(int x, int y, const char *);

#ifdef _MOTIF_
  //CPoint MoveTo(int x, int y); 
  //CPoint MoveTo(POINT point); 
  POINT MoveTo(int x, int y);
  POINT MoveTo(POINT point);

  BOOL LineTo(int x, int y); 
  BOOL LineTo(POINT point); 
#endif //_MOTIF_

  BOOL Ellipse(int x1, int y1, int x2, int y2);
  //BOOL Ellipse(RECT lpRect); 
  BOOL Ellipse(LPCRECT lpRect);

  BOOL Rectangle(int x1, int y1, int x2, int y2);
  //BOOL Rectangle(RECT lpRect);
  BOOL Rectangle(LPCRECT lpRect);

  UINT SetTextAlign(UINT nFlags);

  BOOL Polygon(LPPOINT lpPoints, int nCount);

  //virtual COLORREF SetTextColor(COLORREF crColor); 
  COLORREF SetTextColor(COLORREF crColor); 
  int SetBkMode(int nBkMode); 



        // Not to be
/*
  int GetDeviceCaps(int nIndex) const; 
  
        CPen* SelectObject( CPen* pPen );
  CBrush* SelectObject( CBrush* pBrush );
  virtual CFont* SelectObject( CFont* pFont );
  CBitmap* SelectObject( CBitmap* pBitmap );
  int SelectObject( CRgn* pRgn ); 
  CGdiObject* SelectObject( CGdiObject* pObject ); 
        
  //BOOL CreateCompatibleDC( CDC* pDC ); 
  BOOL CreateCompatibleDC(GraphicPainter* pDC );
  //BOOL BitBlt ( int x, int y, int nWidth, int nHeight, CDC* pSrcDC, int xSrc, int ySrc, DWORD dwRop ); 
  BOOL BitBlt ( int x, int y, int nWidth, int nHeight, GraphicPainter*  pSrcDC, int xSrc, int ySrc, DWORD dwRop );
  virtual CGdiObject* SelectStockObject (int nIndex ); 
        virtual int GetClipBox( LPRECT lpRect ) const; 
  BOOL PatBlt( int x, int y, int nWidth, int nHeight, DWORD dwRop ); 
*/

};

#endif // __GRAPHICCONTEXT_H
