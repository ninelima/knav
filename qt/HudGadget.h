//
// $Id: HudGadget.h,v 1.3 2005/09/29 14:02:06 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//

#include <qwidget.h>
#include <qpixmap.h>

#ifndef __GRAPHICPAINTER_H
  #include "GraphicPainter.h"
#endif // __GRAPHICPAINTER_H

#include "PanelGadget.h"

class HudGadget : public PanelGadget //QWidget //PanelGadget
{
public:
  int hotspot; /** 80 sets hotspot to the arrow */
  int g_Pitch;
  int g_Bank;
  int g_Gs;
  int g_Alt;
  int g_Hdg;


  QRect rect;

  HudGadget(QWidget *parent=0, const char *name=0, WFlags f=0);

  //void paintEvent(QPaintEvent* e);

  void drawBlankFace(GraphicPainter *gc, int x, int y, int size);
  void cropFace(GraphicPainter *gc, int x, int y, int size);

  void drawFace(GraphicPainter *gc);

  void setHdg(int Hdg);
  void setAlt(int Alt);
  void setGs(int Gs);

  void setPitch(int Pitch);
  void setBank(int Bank);


  /*
  void mousePressEvent(QMouseEvent *e);
  void mouseReleaseEvent(QMouseEvent *e);
  void mouseMoveEvent(QMouseEvent *e);
  void resizeEvent(QResizeEvent* event);
  */

private:
  /*QPixmap pixbuffer;
  int m_size;
  double m_scale;

  bool m_bMouseLeftButtonDown;
  bool m_bMouseRightButtonDown;*/
};
