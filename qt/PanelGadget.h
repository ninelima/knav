//
// $Id: PanelGadget.h,v 1.2 2005/07/08 07:40:14 player Exp $
//
// Copyright (C) 2001-2004 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//
#ifndef __PANELGADGET_H
#define __PANELGADGET_H

#include <qwidget.h>
#include <qpixmap.h>

#ifndef __GRAPHICPAINTER_H
  #include "GraphicPainter.h"
#endif // __GRAPHICPAINTER_H


#define min(a,b)            (((a) < (b)) ? (a) : (b))


class PanelGadget : public QWidget
{
public:
  //int m_Hdg;
  //int hotspot; /** 80 sets hotspot to the arrow */

  PanelGadget(QWidget *parent=0, const char *name=0, WFlags f=0);

  void paintEvent(QPaintEvent* e);

  virtual void drawBlankFace(GraphicPainter *gc, int x, int y, int size) = 0;
  virtual void drawFace(GraphicPainter *gc) = 0;

  virtual void mousePressEvent(QMouseEvent *e);
  virtual void mouseReleaseEvent(QMouseEvent *e);
  virtual void mouseMoveEvent(QMouseEvent *e);
  virtual void resizeEvent(QResizeEvent* event);

//private:
protected:
  QRect rect;

  QPixmap pixbuffer;
  int m_size;
  double m_scale;

  bool m_bMouseLeftButtonDown;
  bool m_bMouseRightButtonDown;
};

#endif //__PANELGADGET_H
