//-----------------------------------------------------------------------------
// $Id: UTextBrowser.cpp,v 1.3 2006/04/23 11:05:33 player Exp $
//
// Copyright (C) 1992-2000 Player One. All rights reserved.
//
// This file is part of Kwik Navigator Flight Planner. This source
// file may be used, distributed and modified without limitation.
//
//-----------------------------------------------------------------------------

//
//  Compiler Warning (level 1) C4786
//  'identifier' : identifier was truncated to 'number' characters in the 
//                 debug information
//
#pragma warning(disable : 4786)  // Disable warning messages 4786


#include "UTextBrowser.h"
#include "UTextBrowser.moc"

#include <qpopupmenu.h>
#include <stdio.h>
#include <stdlib.h>
#include "ustring.h"
#include "iniFile.h"
#include "misc.h"
#include "version.h"
#include "AptInfoEditor.h"


UTextBrowser::UTextBrowser(QWidget *parent, const char *name)
  : QTextBrowser(parent, name)
{
}

void UTextBrowser::setIcao(const QString icao)
{
  m_sIcao = icao;
}

void UTextBrowser::setSource(const QString &name)
{
  m_sCurrentInfoHtml = name;

  //
  // Call the parent
  //
  QTextBrowser::setSource(name);
}


//-----------------------------------------------------------------------------
// Grab the mouse press events in the "browser" window.
// We will handle them ourself by using the system browser.
//
void UTextBrowser::viewportMousePressEvent(QMouseEvent *e)
{
  if (e->button() == LeftButton) {
    popup_view();
  }

  if (e->button() == RightButton) {
    printf("RightButton\n");

    QPopupMenu *treeMenu = new QPopupMenu(this);
    
    /*
    /// aaah hell this is just all too hard ....
    int id_popup_route  = treeMenu->insertItem("Route", this, SLOT(popup_route()));
                          treeMenu->insertItem("Locate", this, SLOT(popup_locate()));
                          treeMenu->insertSeparator();*/
    int id_popup_view   = treeMenu->insertItem("View", this, SLOT(popup_view()));
    int id_popup_edit   = treeMenu->insertItem("Edit", this, SLOT(popup_edit()));
    //int id_popup_remove = treeMenu->insertItem("Remove", this, SLOT(popup_remove()));

    treeMenu->popup(e->globalPos());
  }

 

  /*
  printf("UTextBrowser::viewportMousePressEvent\n");

  if (!m_sCurrentInfoHtml.isEmpty())
    html_browse(m_sCurrentInfoHtml.latin1());
  else
    html_browse("default.html");

  //do not call: QTextBrowser::viewportMousePressEvent(e);
  */
}



//-----------------------------------------------------------------------------
// Popup | Edit action performed
//
void UTextBrowser::popup_edit()
{
  //html_edit(m_sCurrentInfoHtml);
  AptInfoEditorForm dlg(this, "aptinfoeditor", " ", TRUE);
  dlg.setCaption(QString("%1 - %2").arg(SZ_APPNAME).arg("Airport Information Editor"));
  //dlg.Init(m_sCurrentInfoHtml);
  dlg.Init(m_sIcao);
  int rv = dlg.exec();
  if (rv == QDialog::Rejected)
    return;
}


//-----------------------------------------------------------------------------
// Popup | Edit action performed
//
void UTextBrowser::popup_view()
{
  html_browse(m_sCurrentInfoHtml);
}
