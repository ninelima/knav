#ifndef __AIRSPACESET_H
#define __AIRSPACESET_H

#include "DelimRecordSet.h"
#include <string>

//
// AirspaceSet.h : header file
//

#define _NO_AIRSPACE_CACHE_

#if _MOTIF_
#define _NO_AIRSPACE_CACHE_
#endif

 
//#ifdef _MOTIF_
#ifdef _NO_AIRSPACE_CACHE_

#pragma message ("fix cp for no cache")
 
//-----------------------------------------------------------------------------
// CAirspaceSet Delimited recordset
//
class CAirspaceSet : public CDelimRecordSet
{
public:
  long cp;// = 0;
  CAirspaceSet();

  void Bind();
 
  double  m_rLat1;
  double  m_rLon1;
  double  m_rLat2;
  double  m_rLon2;
  long    m_radius;
  long    m_a1;
  long    m_a2;
  long    m_alt1;
  long    m_alt2;
  std::string m_INFO;
  long   m_nFields;
};


#else //_NO_AIRSPACE_CACHE_
todo

//-----------------------------------------------------------------------------

#define AIR_TOTAL 14000

class AirRec
{
public:

  double  m_rLat1;
  double  m_rLon1;
  double  m_rLat2;
  double  m_rLon2;
  long    m_radius;
  long    m_a1;
  long    m_a2;
  long    m_alt1;
  long    m_alt2;
  long    m_nFields;

  AirRec()
  {
    // Field initialisation
    m_rLat1 = 0;
    m_rLon1 = 0;
    m_rLat2 = 0;
    m_rLon2 = 0;
    m_radius = 0;
    m_a1 = 0;
    m_a2 = 0;
    m_alt1 = 0;
    m_alt2 = 0;

    // Number of fields
    m_nFields = 9;
  }
};


class CAirspaceSet : public CDelimRecordSet
{

public:
  // We can use vectors, lists or whatever
  // but speed is king. A simple static array
  // is the best.
  AirRec tbl[AIR_TOTAL];

  int cp;
  int num;


public:
  double  m_rLat1;
  double  m_rLon1;
  double  m_rLat2;
  double  m_rLon2;
  long    m_radius;
  long    m_a1;
  long    m_a2;

  CAirspaceSet();
  void AirspaceSetInit();
  bool Open(const char *sFileName);
  bool MoveFirst();
  bool MoveNext();
  bool Move(int idx);
  bool IsEOF();
  void Clear();
  void Bind();
  void Store();
  AirRec GetRecord();
};


#endif //_MOTIF_


#endif // __AIRSPACESET_H
