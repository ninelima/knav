//-----------------------------------------------------------------------------
//
// Copyright (c) Player One Inc 2001
//
// Module name: WPTSet
// Module type: Implementation source file
// Compiler   : MSVC++ 6.0
// Environment: Microsoft Windows NT
//
// Description:
//  This class implements the a CSV file handler derived from CDelimRecordSet
//  It's primary function is to define and bind member variables mapped to the
//  WPT database
//
// Note:
//  The pure virtual Bind(..) is implemented in this class
//
// Contents:
//
// Revision history:
//  24 Aug 2001 B2  Created
//  24 Aug 2001 B2  Add comment block
//
//-----------------------------------------------------------------------------

//#include <string>
#include <qstring.h>
#include "WptSet.h"

#ifdef _NO_WPT_CACHE_
#pragma message ("#\n# CWptSet - caching disabled\n#")

//-----------------------------------------------------------------------------
// CWptSet
//
CWptSet::CWptSet()
  : CDelimRecordSet()
{
  m_ID = 0;
  m_ICAO = "";
  m_INFO = "";
  m_rElev = 0.0;
  m_rLat = 0.0;
  m_rLon = 0.0;
  m_TYPE = 0;
  m_nFields = 7;
}

CWptSet::~CWptSet()
{
}

void CWptSet::Bind()
{
  CDelimRecordSet::Bind();

  m_ID    = atoi(m_sField[0]);
  m_ICAO  = m_sField[1];
  m_INFO  = m_sField[2];
  m_rElev = atof(m_sField[3]);
  m_rLat  = atof(m_sField[4]);
  m_rLon  = atof(m_sField[5]);
  m_TYPE  = atoi(m_sField[6]);
}


#else //-----------------------------------------------------------------------------
#pragma message ("#\n# CWptSet - caching enabled\n#")

#include <qstring.h>

CWptSet::CWptSet()
{
  cp = 0;
  num = 0;
  WptSetInit();
}

CWptSet::~CWptSet()
{
}


void CWptSet::WptSetInit()
{
}

//-----------------------------------------------------------------------------
//  Open and read the entire .csv file, storing it in vector class tbl
//
bool CWptSet::Open(const char *sFileName)
{
  CDelimRecordSet rs;// = new CDelimRecordSet();

  if (!rs.Open(sFileName))
    return false;
  rs.MoveFirst();
  //rs.MoveNext(); // only if we use the first line for field names
  while (rs.IsEOF() != true) {
    if (rs.m_nFields > 6) {
        WptRec rec;
        rec.m_ID    = QString(rs.m_sField[0]).toLong();
        rec.m_ICAO  = QString(rs.m_sField[1]);
        rec.m_INFO  = QString(rs.m_sField[2]);
        rec.m_rElev = QString(rs.m_sField[3]).toDouble();
        rec.m_rLat  = QString(rs.m_sField[4]).toDouble();
        rec.m_rLon  = QString(rs.m_sField[5]).toDouble();
        rec.m_TYPE  = QString(rs.m_sField[6]).toLong();
				rec.m_nFields = 8;

        tbl[num] = rec;									 
        num++;
        int size1 = sizeof (WptRec);
    }
    if (num >= WPT_TOTAL)
      break; //throw 1;

    rs.MoveNext();
  }
  rs.Close();
  return true;
}

//-----------------------------------------------------------------------------
//  Move to the first entry in the set
//
bool CWptSet::MoveFirst()
{
  cp = 0;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
// Move to the next entry in the set
//
bool CWptSet::MoveNext()
{
  cp++;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
// Move to a specified in the set
//
bool CWptSet::Move(int idx)
{
  cp = idx;
  Bind();
  return true;
}

//-----------------------------------------------------------------------------
//  Check for the end of the set
//  returns true is the end is reached and
//  false if not
//
bool CWptSet::IsEOF()
{
  int size = num;// tbl.size();
  //if (cp >= size - 1) if we use the first line as headings
  if (cp >= size)
    return true;
  else
    return false;
}

//-----------------------------------------------------------------------------
//  Clear out the contents of the set
//
void CWptSet::Clear()
{
  cp = 0;
  num = 0;
}

void CWptSet::Bind()
{

  m_ID    = tbl[cp].m_ID;
  m_ICAO  = tbl[cp].m_ICAO;
  m_INFO  = tbl[cp].m_INFO;
  m_rElev = tbl[cp].m_rElev;
  m_rLat  = tbl[cp].m_rLat;
  m_rLon  = tbl[cp].m_rLon;
  m_TYPE  = tbl[cp].m_TYPE;

  m_nCurRec = cp;
}

void CWptSet::Store() 
{

  tbl[cp].m_ID    = m_ID;
  tbl[cp].m_ICAO  = m_ICAO;
  tbl[cp].m_INFO  = m_INFO;
  tbl[cp].m_rElev = m_rElev;
  tbl[cp].m_rLat  = m_rLat;
  tbl[cp].m_rLon  = m_rLon;
  tbl[cp].m_TYPE  = m_TYPE;

}

WptRec CWptSet::GetRecord()
{
  WptRec rec;// = new WptRec();

  rec.m_ID    = m_ID;
  rec.m_ICAO  = m_ICAO;
  rec.m_INFO  = m_INFO;
  rec.m_rElev = m_rElev;
  rec.m_rLat  = m_rLat;
  rec.m_rLon  = m_rLon;
  rec.m_TYPE  = m_TYPE;

  return rec;
}

/*
long CWptSet::GetCP()
{
  return cp;
}
*/

#endif
