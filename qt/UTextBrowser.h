#ifndef __UTEXTBROWSER_H
#define __UTEXTBROWSER_H

#include <qtextbrowser.h>

class UTextBrowser : public QTextBrowser
{
    Q_OBJECT

public:

  QString m_sCurrentInfoHtml;
  QString m_sIcao;

  UTextBrowser(QWidget *parent=0, const char *name=0);
  void setSource(const QString &name);
  void setIcao(const QString icao);

protected:
  void viewportMousePressEvent(QMouseEvent *e);

protected:

private slots:
  void popup_edit();
  void popup_view();


};           

#endif // __UTEXTBROWSER_H

